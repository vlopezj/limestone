{ mkDerivation, base, fetchgit, mtl, stdenv, template-haskell }:
mkDerivation {
  pname = "geniplate-mirror";
  version = "0.7.4";
  src = fetchgit {
    url = "git://github.com/danr/geniplate.git";
    sha256 = "1di82hgp07n5bg3gpr4gzmwlh32vx6kvvfz5bacqppd47jrgf7f4";
    rev = "98da37d6a2eb467aa3642118f5d51e787e2a6f50";
  };
  libraryHaskellDepends = [ base mtl template-haskell ];
  homepage = "https://github.com/danr/geniplate";
  description = "Use Template Haskell to generate Uniplate-like functions";
  license = stdenv.lib.licenses.bsd3;
}
