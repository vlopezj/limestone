#!/bin/bash
cabal2nix git://github.com/jyp/Cake.git > cake.nix
cabal2nix git://github.com/vlopezj/MarXup.git > marxup.nix
cabal2nix git://github.com/jyp/polynomials-bernstein.git > polynomials-bernstein.nix
cabal2nix git://github.com/danr/geniplate.git > geniplate-mirror.nix
cabal2nix git://github.com/vlopezj/multiset.git > multiset.nix
cabal2nix git://github.com/vlopezj/genifunctors.git > genifunctors.nix
cabal2nix git://github.com/vlopezj/typography-geometry.git > typography-geometry.nix
cabal2nix cabal://singletons-2.1 > singletons.nix
cabal2nix cabal://is > is.nix
cabal2nix cabal://parsek > parsek.nix
cabal2nix cabal://cmdlib > cmdlib.nix
cabal2nix cabal://void > void.nix
cabal2nix cabal://sbv-4.4 > sbv.nix
cabal2nix git://github.com/vlopezj/vector.git > vector.nix
cabal2nix cabal://th-desugar-1.6 > th-desugar.nix
cabal2nix cabal://QuickCheck-2.8.2 > QuickCheck.nix
