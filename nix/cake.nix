{ mkDerivation, array, base, binary, bytestring, cmdargs
, containers, derive, directory, fetchgit, filepath, mtl, parsek
, process, pureMD5, regex-tdfa, split, stdenv
}:
mkDerivation {
  pname = "cake";
  version = "1.1.0.1";
  src = fetchgit {
    url = "git://github.com/jyp/Cake.git";
    sha256 = "0war274i03wgc81g6yqw6a2bgsgcv5pxfswzj5xdrd4in1y3mlp3";
    rev = "df0aad2cac6ae77907787dfe0928da5985967f0f";
  };
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base binary bytestring containers derive directory filepath mtl
    parsek process pureMD5 split
  ];
  executableHaskellDepends = [
    array base cmdargs directory filepath process regex-tdfa
  ];
  description = "A build-system library and driver";
  license = "GPL";
}
