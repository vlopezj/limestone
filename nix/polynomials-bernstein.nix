{ mkDerivation, base, fetchgit, stdenv, vector }:
mkDerivation {
  pname = "polynomials-bernstein";
  version = "1.1.2";
  src = fetchgit {
    url = "git://github.com/jyp/polynomials-bernstein.git";
    sha256 = "0s8qbfpc4fga88j2b5qj2yrzg1v5cj1pmxcc50i9m6n7h3n80mwi";
    rev = "7e9f62a6d38b23f8f97894b765d1556fde848d53";
  };
  libraryHaskellDepends = [ base vector ];
  description = "A solver for systems of polynomial equations in bernstein form";
  license = "GPL";
}
