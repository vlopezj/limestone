{ mkDerivation, base, containers, fetchgit, mtl, stdenv
, template-haskell
}:
mkDerivation {
  pname = "genifunctors";
  version = "0.3";
  src = fetchgit {
    url = "git://github.com/vlopezj/genifunctors.git";
    sha256 = "1jw1zpd233w2nrdn3pjh4fmrac0vidfl67652f79mdiipa4lg3f8";
    rev = "4677bb57423b1b380ce9b50cc3d765a5c49a957a";
  };
  libraryHaskellDepends = [ base containers mtl template-haskell ];
  testHaskellDepends = [ base containers mtl template-haskell ];
  homepage = "https://github.com/danr/genifunctors";
  description = "Generate generalized fmap, foldMap and traverse";
  license = stdenv.lib.licenses.bsd3;
}
