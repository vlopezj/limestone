{ mkDerivation, base, configurator, containers, directory, dlist
, fetchgit, filepath, glpk-hs, graphviz, haskell-src-exts
, labeled-tree, lens, mtl, parsek, polynomials-bernstein, pretty
, process, stdenv, text, typography-geometry, vector
}:
mkDerivation {
  pname = "marxup";
  version = "3.0.0.2";
  src = fetchgit {
    url = "git://github.com/vlopezj/MarXup.git";
    sha256 = "1539jdb58fv3jvzxw6q0cv7q7gxbs4nj0g8a61dbn180z6vxdk3c";
    rev = "a09f4e1f5d99e73201a404e9e75113817fc596aa";
  };
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base containers directory filepath glpk-hs graphviz
    haskell-src-exts labeled-tree lens mtl polynomials-bernstein
    process text typography-geometry vector
  ];
  executableHaskellDepends = [
    base configurator dlist parsek pretty
  ];
  description = "Markup language preprocessor for Haskell";
  license = stdenv.lib.licenses.gpl2;
}
