{ mkDerivation, base, stdenv }:
mkDerivation {
  pname = "parsek";
  version = "1.0.1.3";
  sha256 = "184cbw9gz3vv2jbr2wzkygv25y70jayxd8d76pgpvjcaps4qqxp7";
  libraryHaskellDepends = [ base ];
  description = "Parallel Parsing Processes";
  license = stdenv.lib.licenses.gpl3;
}
