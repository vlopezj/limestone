{ mkDerivation, base, deepseq, fetchgit, ghc-prim, primitive
, QuickCheck, random, stdenv, template-haskell, test-framework
, test-framework-quickcheck2, transformers
}:
mkDerivation {
  pname = "vector";
  version = "0.11.0.0";
  src = fetchgit {
    url = "git://github.com/vlopezj/vector.git";
    sha256 = "1f7yv2ivlkcmxgl6kjpb8y0p6m1d9hk43amn9frssm572syr60a9";
    rev = "aac207882aab9dd6fe0feacdf9f88fe559a4733d";
  };
  libraryHaskellDepends = [ base deepseq ghc-prim primitive ];
  testHaskellDepends = [
    base QuickCheck random template-haskell test-framework
    test-framework-quickcheck2 transformers
  ];
  homepage = "https://github.com/haskell/vector";
  description = "Efficient Arrays";
  license = stdenv.lib.licenses.bsd3;
}
