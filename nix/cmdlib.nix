{ mkDerivation, base, mtl, split, stdenv, syb, transformers }:
mkDerivation {
  pname = "cmdlib";
  version = "0.3.6";
  sha256 = "0mxk7yy3sglxc97my5lnphisg6fawifrbdbpz31h7ybiqccx4hsn";
  libraryHaskellDepends = [ base mtl split syb transformers ];
  description = "a library for command line parsing & online help";
  license = stdenv.lib.licenses.bsd3;
}
