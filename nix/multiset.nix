{ mkDerivation, base, containers, fetchgit, stdenv }:
mkDerivation {
  pname = "multiset";
  version = "0.2.3";
  src = fetchgit {
    url = "git://github.com/vlopezj/multiset.git";
    sha256 = "01q1g425zz1km2xbphycaisbndv4m6zjskn3gkis88gnrb63c8z4";
    rev = "8c610942394ca1f0cdd20d1d0b8fd626b4e7f10a";
  };
  libraryHaskellDepends = [ base containers ];
  description = "The Data.MultiSet container type";
  license = stdenv.lib.licenses.bsd3;
}
