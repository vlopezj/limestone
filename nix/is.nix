{ mkDerivation, base, stdenv, template-haskell }:
mkDerivation {
  pname = "is";
  version = "0.2";
  sha256 = "1ihrrpypdjhsr42nd9chyq730kllx239igjpa12m14458lnrcb2h";
  libraryHaskellDepends = [ base template-haskell ];
  testHaskellDepends = [ base template-haskell ];
  description = "Pattern predicates using TH";
  license = stdenv.lib.licenses.bsd3;
}
