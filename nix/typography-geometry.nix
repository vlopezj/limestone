{ mkDerivation, base, containers, fetchgit, parallel
, polynomials-bernstein, stdenv, vector
}:
mkDerivation {
  pname = "typography-geometry";
  version = "1.0.0";
  src = fetchgit {
    url = "git://github.com/vlopezj/typography-geometry.git";
    sha256 = "0ajggwry5djwh68cpp7qarm05chm6qzaash2zjrfqqy26ilh84hs";
    rev = "ee96374492db3d5c91d94c3bd1adff2723a8e405";
  };
  libraryHaskellDepends = [
    base containers parallel polynomials-bernstein vector
  ];
  description = "Drawings for printed text documents";
  license = "GPL";
}
