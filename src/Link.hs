{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DoAndIfThenElse #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
module Link where

import Fresh
import LL
import LL.Traversal
import Data.List
import Control.Monad.Except.Extra
import Control.Monad.State
import Control.Lens
import Control.Applicative
import Data.Generics.Geniplate
import Data.Function
import Data.Maybe
import Id

type LinkM = ExceptT String FreshM

derivDeps :: Deriv0 Id Id -> [Name]
derivDeps (D0 d) = [name | LLDeriv name <- $(genUniverseBi' [t| Seqn0 Id Id -> [What] |]) (d ^. derivSequent) ]

-- Sorts derivations in dependency order
topoSortDeriv :: [Deriv0 Id Id] -> [Deriv0 Id Id] -> LinkM [Deriv0 Id Id]
topoSortDeriv dd d_all = do
  let graph = [(d ^. derivName, derivDeps (D0 d)) | D0 d <- (dd ++ d_all)]
  case checkGraphEdges graph of
    Left v -> throwError$ "Invalid derivation name " ++ v ++ ".\n" ++
                          "Valid names: " ++ intercalate ", " [d ^. derivName | D0 d <- (dd ++ d_all)]
    Right () -> return ()
    
  let res = sortDeps [d ^. _Wrapping D0 . derivName | d <- dd]  graph
  case res of
    Left cycle -> throwError$ "Dependency cycle: " ++ intercalate "»" cycle ++ "."
    Right names   -> do
      let ds' = [fromJust$ find (\(D0 d) -> d ^. derivName == name) (dd ++ d_all) | name <- names] 
      return ds'

checkGraphEdges :: (Eq a) => [(a,[a])] -> Either a () 
checkGraphEdges graph =
  forM_ graph $ \(_, deps) ->
               forM_ deps $ \d ->
                 case lookup d graph of
                   Nothing -> throwError d
                   Just _  -> return ()

-- | Returns a list of derivations
linkDeriv :: [Deriv2 Id Id] -> LinkM [Deriv2 Id Id]
linkDeriv = foldM (\deps (D2 d) ->
                           do
                             d' <- d & derivSequent %%~ (go deps)
                             return $ (D2 d'):deps) []
  where
  go :: [Deriv2 Id Id] -> Seqn2 Id Id -> LinkM (Seqn2 Id Id)
  go deps (Seqn2 s) = Seqn2 <$> foldSeqn defSeqnFold{sseq} s
    where
    sseq _ s@What{ what = LLDeriv name, zVars } =
      case find ((== name) . view (_Wrapping D2 . derivName)) deps of
        Nothing -> error $ "Bug in linker depencency resolution. Derivation " ++ name ++ " not found.\n" ++
                           "Valid names: " ++ intercalate ", " [d ^. derivName | D2 d <- deps]
        Just d -> do
          D2 (Deriv { _derivCtx,
                      _derivSequent }) <-
            refreshDeriv2 d

          (Seqn2 seq') <- typedSubst
                          _derivCtx
                          [ty ::: kind | ElemIntuit ty kind <- zVars]
                          [ref | ElemLinear ref <- zVars]
                          _derivSequent
          return $ seq'

    sseq a s = pure $ Seqn a s

-- ^ Rename variables, checking that their types are actually equal
typedSubst :: (MonadError String m, MonadFresh m)
           => IdTypedCtx 
           -> [IdType ::: IdType] -- new types
           -> [Id     ::: IdSized] -- new linear variables
           -> Seqn2 Id Id -> m (Seqn2 Id Id)

typedSubst src dst_intuit dst_linear s = do
    flip catchError (throwError . ("Argument type mismatch\n." ++)) $
      matchTypedCtx (SeqCtx [unsafeId "dummy" ::: kind :^ 1 | _ ::: kind <- dst_intuit] dst_linear) src
    let t = renameList [(s,d) | (s ::: _) <- (src ^. linear)
                              | (d ::: _) <- (dst_linear)] s
    tySubstsSeq2 [(v,ty) | v ::: _ <- src ^. intuit
                         | ty ::: _ <- dst_intuit ] t


-- | Returns an ordered list of dependencies for the given node
--   The given node will always be last; and each of the nodes
--   in the list reference only
sortDeps :: forall a. Eq a => [a] -> [(a, [a])] -> Either [a] [a]
sortDeps goals graph = flip evalStateT [] $ go [] goals
  where
    go :: [a] -- ^ nodes recursed into
       -> [a] -- ^ goals
       -> StateT [a] (Either [a]) [a] -- ^ sorted deps
    go current goals = do
      fmap concat $ forM goals $ \x -> do
        finished <- get
        if x `elem` finished then
          return []
        else 
          case span (/= x) current of
            (_,[]) -> do 
              res <- go (x:current) (fromJust (lookup x graph))
              modify (x:)
              return (res ++ [x])
            (bread,_) -> throwError (reverse bread ++ [x]) -- cycle
        
              
