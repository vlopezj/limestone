{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeOperators #-}
module Op(CmpOp()
         ,pattern CmpFalse
         ,pattern Lt
         ,pattern Eq
         ,pattern Gt
         ,pattern CmpTrue
         ,pattern Ge
         ,pattern Le
         ,pattern Ne
         ,IntOp(..)
         ,haskellCmpOp
         ,negCmp,joinCmp,meetCmp
         ,flipCmp
         ,enumCmpOp
         ,Choice(..), choiceToBool, boolToChoice
         ,CutInfo(..)
         ,(:…)(..)
         ,mathCmpOp
         )
       where

import Control.Applicative
import Data.String

data IntOp = Add | Sub | Mul | Div | Mod
  deriving (Show,Read,Eq)


-- CmpBot and CmpTop make it into a lattice
data CmpOp = CmpOp { lt :: !Bool
                   , eq :: !Bool 
                   , gt :: !Bool } deriving (Ord, Eq)

enumCmpOp = CmpOp <$> [False,True] <*> [False,True] <*> [False,True]

instance Show CmpOp where
  showsPrec _ t s = case t of
    CmpFalse -> "CmpFalse" ++ s
    Gt -> "Gt" ++ s
    Eq -> "Eq" ++ s
    Lt -> "Lt" ++ s
    Ge -> "Ge" ++ s
    Le -> "Le" ++ s
    Ne -> "Ne" ++ s
    CmpTrue -> "CmpTrue" ++ s

instance Read CmpOp where
  readsPrec _ t = lex t >>= \(s,rest) -> map (,rest) $ case s of 
    "CmpFalse" -> [CmpFalse]
    "Gt"       -> [Gt]
    "Eq"       -> [Eq] 
    "Lt"       -> [Lt] 
    "Ge"       -> [Ge] 
    "Le"       -> [Le]
    "Ne"       -> [Ne]
    "CmpTrue"  -> [CmpTrue]
    _          -> []

-- False < True 
pattern CmpFalse = CmpOp False False False 
pattern Gt       = CmpOp False False True
pattern Eq       = CmpOp False True  False
pattern Lt       = CmpOp True  False False
pattern Ge       = CmpOp False True  True
pattern Le       = CmpOp True  True  False
pattern Ne       = CmpOp True  False True
pattern CmpTrue  = CmpOp True  True  True 

data CmpOpPrim = PLt | PGt | PGe

haskellCmpOp :: (Ord a) => CmpOp -> a -> a -> Bool
haskellCmpOp CmpFalse = const $ const $ False
haskellCmpOp Eq = (==)
haskellCmpOp Ne = (/=)
haskellCmpOp Lt = (<)
haskellCmpOp Le = (<=)
haskellCmpOp Gt = (>)
haskellCmpOp Ge = (>=)
haskellCmpOp CmpTrue = const $ const $ True

negCmp :: CmpOp -> CmpOp
negCmp (CmpOp a b c) = CmpOp (not a) (not b) (not c)

flipCmp :: CmpOp -> CmpOp
flipCmp CmpOp{eq = eq',
              lt = lt',
              gt = gt'} = CmpOp{eq = eq',
                                lt = gt',
                                gt = lt'}

-- Partial order
-- a `impliesCmp` b ⇒ a ≤ b
impliesCmp :: CmpOp -> CmpOp -> Bool
a `impliesCmp` b = lt a <= lt b &&
                   eq a <= eq b &&
                   gt a <= gt b

joinCmp :: CmpOp -> CmpOp -> CmpOp
a `joinCmp` b = CmpOp { lt = lt a || lt b
                      , eq = eq a || eq b
                      , gt = gt a || gt b
                      }

meetCmp :: CmpOp -> CmpOp -> CmpOp
a `meetCmp` b = CmpOp { lt = lt a && lt b
                      , eq = eq a && eq b
                      , gt = gt a && gt b
                      }

data Choice = Inl | Inr
  deriving (Show,Read,Eq)


choiceToBool :: Choice -> Bool
choiceToBool Inl = True
choiceToBool Inr = False

boolToChoice :: Bool -> Choice
boolToChoice True = Inl
boolToChoice False = Inr

data CutInfo = Desugar | Manual | Fuse | Polymorphic | NoCommute
  deriving (Show,Read,Eq,Ord)

data (:…) = (:…)
(…) = (:…)

instance Show (:…) where
  showsPrec _ (:…) s = "(…)" ++ s

instance Read (:…) where
  readsPrec _ ('(':'…':')':rest) = [((…), rest)]
  readsPrec _ _                = []
 

mathCmpOp :: (IsString s) => CmpOp -> s
mathCmpOp Eq = "="
mathCmpOp Ne = "≠"
mathCmpOp Ge = "≥"
mathCmpOp Le = "≤"
mathCmpOp Lt = "<"
mathCmpOp Gt = "<"
mathCmpOp CmpFalse = "⊥"
mathCmpOp CmpTrue  = "⊤"
