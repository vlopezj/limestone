{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveFunctor,TypeSynonymInstances,FlexibleInstances,MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell,ScopedTypeVariables,PatternGuards,QuasiQuotes #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UnicodeSyntax #-}
module Type(module Type,
            module Type.Symbol,
            module Binders) where

import Development.Placeholders

import Id
import MetaVar
import Control.Applicative
import Control.Monad.Reader
import Data.Function (on)
import Data.Traversable as T
import Data.Foldable (Foldable)
import Data.Generics.Geniplate.Extra
import Data.Generics.Genifunctors
import Fresh
import Data.Maybe
import Data.List
import Data.Bitraversable
import Control.Monad.State
import Data.Bifoldable
import Data.Bifunctor
import Binders
import Data.Default
import Control.Lens
import ContM

import Type.Core as Type
import Type.Size as Type
import Type.Symbol
import Type.Ctx
import Type.Traversal
import Type.Traversal as Type(foldType, foldType', defTypeFold, defTypeTraverse,
                       traverseType'', foldTypeWithSelf, foldTypeWithSelf',
                       zipWithTypeM , zipWithTypeM'')

import GHC.Exts

instance HasRef (Type' name ref) where
  type Ref (Type' name ref) = ref

instance EmbedSymbol (Type' name ref) where
  symb = T . Var []

instance IsString ref => IsString (Type' name ref) where fromString = fromStringEmbed
                                                         
deriving instance (Ord name, Ord ref) => Ord (Type' name ref)
deriving instance (Eq name, Ord ref) => Eq (Type' name ref)

instance Bifoldable Sized' where  bifoldMap = bifoldMapDefault
instance Bifunctor Sized' where  bimap = bimapDefault
instance Bitraversable Sized' where  bitraverse = $(genTraverse ''Sized')

pattern KindTy  = T TType :^ 1
pattern KindSz  = T TType :^ 1

unit :: (Ord ref) => Type' name ref -> Sized' name ref
unit = (:^ 1)

equals :: Eq a => [a] -> Bool
equals [] = error "equals called over empty list"
equals (x:xs) = all (==x) xs

-- | 
index :: Type' name ref
index = T (Index True)

-- | Simplify arrays of size 0 into a unit type.
arrayOp :: (Ord ref) => Polarity -> Size ref -> Type' name ref -> Type' name ref
arrayOp pol sz t = arrayOp' (arrayPol pol) sz t

arrayOp' :: (Ord ref) => ArrayPol -> Size ref -> Type' name ref -> Type' name ref
arrayOp' pol sz t | isZero sz = T$ Unit pol
arrayOp' pol sz t = T$ Array pol sz t

dualWhen :: Dual x => Bool -> x -> x
dualWhen f = if f then dual else id

instance Dual (Type' n r) where
  dual = neg

instance Polar (Sized' n r) where
  arrayPolarity (a :^ _) = arrayPolarity a

instance Dual (Sized' n r) where
  dual (a :^ b) = dual a :^ b

bimapType :: (a -> a') -> (b -> b') -> Type' a b -> Type' a' b'
bimapType = $(genFmap ''Type')

-- tySubst i t s == s[t/i]
tySubstsCore :: (MonadFresh m) => [(Id, Type' Id Id)] -> Type' Id Id -> m (Type' Id Id)
tySubstsCore su (T t0) = case t0 of
    Var _ (TVar pos i) | Just t <- lookup i su -> (if pos then id else neg) <$> refreshType t
    SizeT sz                                 -> tM$ SizeT <$> szSubsts [(i,s) | (i, sizeFromType -> Just s) <- su] sz
    _                                        -> tM$ return t0
            
tySubst :: (MonadFresh m) => Id -> Type' Id Id -> Type' Id Id -> m (Type' Id Id)
tySubst i t = tySubsts [(i,t)]

tySubsts :: (MonadFresh m) => [(Id,Type' Id Id)] -> Type' Id Id -> m (Type' Id Id)
tySubsts su = (fmap normType .) $ transformBiM (tySubstsCore su)

tySzSubst :: (MonadFresh m) => Id -> IdSize -> IdType -> m IdType
tySzSubst i sz = tySubst i (T (SizeT sz))
           
tyRename :: Eq a => a -> a -> Type' a a -> Type' a a
tyRename old new = tyRenameList [(old,new)]

normType :: (Eq ref, Eq name) => Type' name ref -> Type' name ref
normType = id

travType' :: (Type' name ref -> Type' name ref) -> Type' name ref -> Type' name ref
travType' = transformBi

tyRenameList :: forall a. Eq a => [(a,a)] -> Type' a a -> Type' a a
tyRenameList su = tr $ \ x -> fromMaybe x (lookup x su)
  where
    tr :: (a -> a) -> Type' a a -> Type' a a
    tr = $(genTransformBiT' [ [t|MetaVar a|] ] [t| (a -> a) -> Type' a a -> Type' a a |])
                -- Don't go inside metavariable's scope lists!


refreshType :: (MonadFresh m) => Type' Id Id -> m (Type' Id Id)
refreshType = refreshBinders

tyBinders :: Type' name ref -> [name]
tyBinders = names

tyRefs :: forall name ref. Type' name ref -> [ref] -- forall ref name . Type' name ref -> [ref]
tyRefs = $(genUniverseBiT' [ [t|MetaVar ref|] ] [t| Type' name ref -> [ref] |])
                       -- this means don't go inside MetaVar
                       -- we do this so we don't consider the scope of the
                       -- meta var

tyMetaRefs :: forall name ref. Type' name ref -> [MetaVarRef]
tyMetaRefs = $(genUniverseBi' [t| Type' name ref -> [MetaVarRef] |])

(•),(∙) :: Type' name ref -> Type' name ref
-- Type-construction helper functions
(&),(&:),(⅋),(⊕),(⊗),(⊸),(-⊸),(|:) :: Type' name ref -> Type' name ref -> Type' name ref
a ⅋ b = T$ a :|: b
a ⊗ b = T$ a :*: b
a ▹ b = T$ Product (arrayPol Neutral) a b
a ⊕ b = T$ a :+: b
a & b = T$ a :&: b
(&:) = (Type.&)
(|:) = (Type.⅋)
a ⊸ b = neg a ⅋ b
a -⊸ b = T$ a `Lollipop` b
(•) a = T$ Plupp a -- BULLET 2022 (TODO: remove)
(∙) a = T$ Plupp a -- BULLET OPERATOR 2219
a `lol` b = T$ a `Lollipop` b

infixr 4 -⊸
infixr 4 `lol`

tProp p = T$ TProp p
tSize = T TSize
exists v k t = T$ Exists v k t

bigTensor :: (Ord ref) => Size ref -> Type' name ref -> Type' name ref
bigTensor = arrayOp Positive
bigPar :: (Ord ref) => Size ref->  Type' name ref -> Type' name ref
bigPar = arrayOp Negative
bigSeq :: (Ord ref) => Size ref->  Type' name ref -> Type' name ref
bigSeq = arrayOp Neutral

bigSeqPlus, bigSeqMinus :: (Ord ref) => Size ref->  Type' name ref -> Type' name ref
bigSeqPlus = arrayOp' (ANeutral (PolConst True))
bigSeqMinus = arrayOp' (ANeutral (PolConst False))

bigSeqPol :: (MonadFresh m, Ord ref) => Size ref → Type' name ref → m (Type' name ref)
bigSeqPol sz t = do
  p <- freshFrom "p" 
  return$ arrayOp' (ANeutral (PolVar True p)) sz t

meta :: (EmbedSymbol t) => Name -> t
meta x = symb $ MetaSyntactic True True x

bool :: Type' name ref
bool = T$ (T One) :+: (T One)

one :: Type' name ref
one = T One

bottom :: Type' name ref
bottom = T Bot

zero :: Type' name ref
zero = T Zero

int :: Type' name ref
int = T$ TInt True

fixTy :: Type' name ref -> Type' name ref
fixTy ty = T$ (T$ Bang (neg ty)) :*: ty

{-
foldTy,unfoldTy :: name -> Type' name Int -> Type' name Int
foldTy u t = Forall u Type ((Bang (t ⊸ var 0)) ⊸ var 0)
unfoldTy u t = neg $ foldTy u (neg t)
-}
foldTy',unfoldTy' :: name -> Type' name name -> Type' name name
foldTy' u t = T$ Forall u (T TType) ((T$ Bang (t ⊸ var u)) ⊸ var u)
unfoldTy' u t = neg $ foldTy' u (neg t)

infixr 6 ⊗,&,▹
infixr 5 ⅋, ⊸, ⊕

-- | Linear negation
neg :: Type' name ref -> Type' name ref
neg = foldTypeWithSelf' defTypeFold{sty = \t' t -> pure . T $ neg0' t' t }

-- | Negate the top level only
neg0 :: Type' name ref -> Type' name ref
neg0 t'@(T t) = T$ neg0' t' t

neg0' :: Type' name ref -> Type'' (Type' name ref) (Size ref) name ref -> Type'' (Type' name ref) (Size ref) name ref 
neg0' (T t') t = case t of
            TType      -> error "neg: Type"
            (SizeT _)     -> error "neg: SizeT"
            (Index b) -> Index (not b)
            (Primitive b name)   -> Primitive (not b) name
            (x :*: y) -> x :|: y
            (x :|: y) -> x :*: y
            (x :+: y) -> x :&: y
            (x :&: y) -> x :+: y
            Zero -> Top
            Top  -> Zero
            Unit r -> Unit (dual r)
            (Var xs (TVar b x))     -> Var xs (TVar (not b) x)
            (Bang t)       -> Quest (t)
            (Quest t)      -> Bang (t)
            (Rec b n t)    -> Rec (dual b) n (t)
            (Array r sz t) -> Array (dual r) sz t
            (Var _ (MetaSyntactic b bs x)) | Var xs _ <- t' -> Var xs $ MetaSyntactic (not b) (not bs) x 


            (Var xs (Meta (MetaVar pos u sc i))) | Var xs _ <- t' -> Var xs (Meta (MetaVar (not pos) u sc i))

            -- Special cases: not all types are negated
            (Exists v _ t) | Exists _ s _ <- t' -> Forall v s (t)
            (Forall v _ t) | Forall _ s _ <- t' -> Exists v s (t)
            (Lollipop _ y) | Lollipop x _ <- t' -> x :*: y
            (MetaNeg _) | MetaNeg  (T x) <- t' -> x

            (RevNeg _) -> error "neg: RevNeg"
            Plupp{}    -> error "neg: Plupp"
            (UnpushedNeg t) -> UnpushedNeg (t) -- debatable


instance Polar (Type' name ref) where
  arrayPolarity :: Type' name ref -> ArrayPol
  arrayPolarity (T t) = case t of
    Zero -> APositive
    Top -> ANegative
    _ :*: _ -> APositive
    _ :&: _ -> ANegative
    _ :|: _ -> ANegative
    _ :+: _ -> APositive
    Bang _ -> APositive
    Quest _ -> ANegative 
    Exists _ _ _ -> APositive
    Forall _ _ _ -> ANegative
    Rec b _ _ | b == Mu -> APositive
    Rec b _ _ | b == Nu -> ANegative
    Array p _ _ -> p 
  {-
    -- error $ "Seq. array can't be made into a positive vector."
    -- ^ Use sign of internal type
    Array Neutral _ ty -> positiveType ty
  -}
    Var _ (MetaSyntactic b _ _ ) -> bool b -- strictly speaking, this is wrong since we do not know the instance of the meta-type.
    Var _ (TVar b _) -> bool b -- same remark.
    Unit p -> p 
    MetaNeg t -> dual $ arrayPolarity t
    RevNeg t -> dual $ arrayPolarity t
    Lollipop _ _ -> ANegative
    Index b -> bool b
    Primitive b _ -> bool b
    -- TODO: Put something here
    SizeT _ -> APositive
    TSize   -> APositive
    TProp _ -> APositive
    TType   -> APositive
    --Plupp _ -> $notImplemented
    UnpushedNeg t -> dual $ arrayPolarity t
    where 
      bool = arrayPolarity

-- | Classification of types as positive and negative
isPositive :: Polar a => a -> Bool
isPositive t = case polarity t of
  Positive -> True
  Negative -> False
  Neutral  -> False
  {- case t of
  One -> True
  Zero -> True
  _ :*: _ -> True
  _ :+: _ -> True
  Bang _ -> True
  Exists _ _ _ -> True
  Rec b _ _ -> b == Mu
  Array Positive _ _ -> True 
  -- error $ "Seq. array can't be made into a positive vector."
  -- ^ Use sign of internal type
  Array Neutral _ ty -> positiveType ty
  Var _ (MetaSyntactic b _ _ ) -> b -- strictly speaking, this is wrong since we do not know the instance of the meta-type.
  Var _ (TVar b _) -> b -- same remark.
  _ -> False
-}
-- | Negate if negative.
mkPositive :: (Dual a, Polar a) => a -> a
mkPositive t = if not $ isPositive t then dual t else t

positiveType :: Type' name ref -> Bool
positiveType = isPositive

---------------------------
-- Substitution machinery

-- | Type-variables substitutions
type Subst name = [Type' name Int]

subst0 :: Type' name Int -> Subst name
subst0 x = x:map var [0..]
{-

-- | Types which can be applied a 'Subst'
(∙) :: Subst name -> Type' name Int -> Type' name Int
(∙) = apply

(∙∙) :: Subst name -> [Type' name Int] -> [Type' name Int]
f ∙∙ xs = map (f ∙) xs
instance (Substitute a, Substitute b) => Substitute (a,b) where
  f ∙ (x,y) = (f∙x, f∙y)

instance (Substitute a) => Substitute [a] where
  -}

wk :: Subst name
wk = map var [1..]

wkN :: Int -> Subst name
wkN n = map var [n..]

contractN :: Int -> Subst name
contractN n = map var [-n..]

if_ :: Bool -> (a -> a) -> a -> a
if_ True  _ = id
if_ False f = f
{-
apply :: Subst name -> Type' name Int -> Type' name Int
apply f t0 = case t0 of
  Type -> Type
  x :+: y -> s x :+: s y
  x :&: y -> s x :&: s y
  x :|: y -> s x :|: s y
  x :*: y -> s x :*: s y
  Zero -> Zero
  One -> One
 Top -> Top
  Bot -> Bot
  TVar pol x -> if_ pol neg (atNote "apply" f x)
  Forall w t -> Forall w (s' t)
  Exists w t -> Exists w (s' t)
  Rec b w t -> Rec b w (s' t)
  Array d t -> Array d (s t)
  Bang t -> Bang (s t)
  Quest t -> Quest (s t)
  MetaSyntactic b x ns -> MetaSyntactic b x (f ∙∙ ns)
  MetaNeg t -> MetaNeg (f ∙ t)
  Lollipop u v -> Lollipop (f ∙ u) (f ∙ v)
  Meta{} -> error "apply Meta"
 where s = apply f
       s' = apply (var 0 : wk ∙∙ f)

matchType :: (Eq ref, Eq name) => Type' name ref -> Type' name ref -> Maybe [Type' name ref]
matchType t0 t1 = go t0 t1
  where
    go (a :*: b) (x :*: y) = (++) <$> go a x <*> go b y
    go (a :|: b) (x :|: y) = (++) <$> go a x <*> go b y
    go (a :+: b) (x :+: y) = (++) <$> go a x <*> go b y
    go (a :&: b) (x :&: y) = (++) <$> go a x <*> go b y
    go (Bang u)     (Bang v)     = go u v
    go (Quest u)    (Quest v)    = go u v
    go (Forall _ u) (Forall _ v) = go u v
    go (Exists _ u) (Exists _ v) = go u v
    go (Rec b1 _ u) (Rec b2 n v)  | b1 == b2  = (meta n:) <$> go u v
                                  | otherwise = mzero
    go Meta{}       t            = return [t]
    go u            v
        | u == v    = return []
        | otherwise = mzero
-}

class TypeUtil name where
    _dum  :: Type' name ref
    wild :: Type' name ref

instance TypeUtil Name where
    _dum = meta "dummy type"
    wild = meta "_"

instance TypeUtil Id where
    _dum = meta "dummy type"
    wild = meta "_"

-- | Replace all variables by their position in the list
idxType :: forall name . Eq name => Type' name name -> [name] -> Either name (Type' name Int)
idxType = runReaderT . foldType' defTypeFold{sty = const (pure . sty)}
  where
    --sty :: _ -> ReaderT [name] (Either name) (Type' name Int)
    sty t0 = T <$> case t0 of
        TType            -> return TType
        SizeT sz         -> SizeT <$> idxSize sz
        Index b          -> return (Index b)
        x :*: y          -> (:*:) <$> x <*> y
        x :|: y          -> (:|:) <$> x <*> y
        x :+: y          -> (:+:) <$> x <*> y
        x :&: y          -> (:&:) <$> x <*> y
        Top              -> return Top
        Zero             -> return Zero
        Unit d           -> return (Unit d)
        Lollipop x y     -> Lollipop <$> x <*> y
        Bang x           -> Bang <$> x
        Quest x          -> Quest <$> x
        Array d sz x     -> Array d <$> idxSize sz <*> x
        Forall x s t       -> (Forall x <$> s <*> local (x:) (t))
        Exists x s t       -> (Exists x <$> s <*> local (x:) (t))
        Rec r x t        -> local (x:) (Rec r x <$> t)
        MetaNeg t        -> MetaNeg <$> t
        Primitive b name -> return $ Primitive b name
        Plupp t          -> Plupp <$> t
        UnpushedNeg t    -> UnpushedNeg <$> t
        Var ts s         -> Var <$> T.sequence ts <*> idxSymbol s
        TSize            -> return TSize
    
instance (name ~ ref, Eq name, Ord ref) => EqType name ref where
    eqType t1 t2 = case (k t1,k t2) of
        (Right u1,Right u2) -> u1 `eqDeBruijn` u2
        _                   -> False
      where
        refs' = nub (tyRefs t1 ++ tyRefs t2)
        k :: Type' name ref -> Either ref (Type' name Int)
        k t = idxType t refs'

        eqDeBruijn = (==) `on` (deparamPolType . bimapType (const ()) id)


metaSyntactic :: Bool -> Bool -> Name -> [Type' name ref] -> Type' name ref
metaSyntactic b bs n ts = T$ Var ts (MetaSyntactic b bs n)

isArray :: Type' name ref -> Bool
isArray (T Array{}) = True
isArray _           = False

-- | This module annotates sequences with polarization variables
paramPolType :: (MonadFresh m) => Type' name ref -> m (Type' name ref)
paramPolType = foldType defTypeFold{sty}
  where
    sty _ (Array b sz ty) = do
      b' <- paramPol freshId b
      return $ T (Array b' sz ty)
    sty _ a = return $ T a

deparamPolType :: Type' name ref -> Type' name ref
deparamPolType = foldType' defTypeFold{sty = (pure.) . sty}
  where
    sty _ (Array b sz ty) =
      let b' = deparamPol b in
      T (Array b' sz ty)
    sty _ a = T a

predsFromCtx :: (MonadNIASize ref m) => SeqCtx (name ::: Sized' name ref) -> ContM_' m c
predsFromCtx ctx = sequenceContM_ [ sizePredAdd p | _ ::: T (TProp p) :^ 1 <- ctx ^. intuit ]

instance (Monad m) => TransformBiM m PolVar (Type' name ref) where
  transformBiM = $(genTransformBiM' [t| (PolVar -> m PolVar) -> (Type' name ref -> m (Type' name ref)) |])

-- | Transform only sizes
instance (Monad m) => TransformBiM m (Size ref) (Type' name ref) where
  transformBiM = 
      $(genTransformBiM' [t| (Size ref -> m (Size ref))
                           -> Type' name ref -> m (Type' name ref) |])

instance TransformBi (Type' name ref) (Type' name ref) where
  transformBi = transformBiDefault

-- | Transform sizes and types
instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Type' name ref) where
    transformBiM f = trTy (fSz >=> f)
      where                       
      trTy = $(genTransformBiM' [t| (Type' name ref -> m (Type' name ref))
                                  -> Type' name ref -> m (Type' name ref) |])

      fSz :: Type' name ref -> m (Type' name ref)
      fSz  = fmap T . traverseType'' defTypeTraverse{ tsz = sizeAsTypeUnsafe f } . unT

instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Sized' name ref) where
  transformBiM f (ty :^ sz) = (:^) <$> transformBiM f ty <*> sizeAsTypeUnsafe f sz

instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (TypedCtx name ref) where
  transformBiM f SeqCtx{_intuit = _intuit',_linear = _linear'} = do
    _intuit <- T.sequence [(name :::) `liftM` transformBiM f tysz | name ::: tysz <- _intuit']
    _linear <- T.sequence [(name :::) `liftM` transformBiM f tysz | name ::: tysz <- _linear']
    return SeqCtx{_intuit,_linear}

freshPolVar :: (MonadFresh m) => m (PolVar)
freshPolVar = do
  pid <- freshFrom "p"
  return$ PolVar True pid

