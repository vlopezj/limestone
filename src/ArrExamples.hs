-- | Examples of array computations, such as a stencil
module ArrExamples where

import CLL
import LL
import Type
import PrettyId
import Rules
{-
-- ⅋⅋n (⊗⊗₃ α) -> ⅋⅋n α
applyStencil :: Deriv' Id Id
applyStencil = mkDeriv "ApplyStencil"
               [("α",unit Type),("n",unit Type)]
               [("inp",unit (Array Negative (TVar True "n") (Array Positive (sizeLit 3) (TVar True "α"))))
               ,("out",unit (Array Positive (TVar True "n") (TVar False "α")))
               ,("stencil", (Array Negative (sizeLit 3) (TVar False "α") :|: TVar True "α") :^ TVar True "n")
               ] $
               BigTensor "elm" "out" $ -- elm :: α
               BigPar () "window" "inp" $ -- window :: ⊗⊗₃ α
               Branch "" $ Par False dum "x" "y" "stencil" (Ax dum "x" "window") (Ax dum "y" "elm")
-}
-- | Stencil operation over tensor?, using coslice
applyStencil α n inp out stencil =
  slice out $ \elm ->
  coslice inp n $ \window ->
  load stencil $ \f ->
  par f (axiom window) (axiom elm)

np1 = SizeAdd (TVar True "n") SizeOne

n = TVar True "n"
α = TVar True "α"
-- α = TVar False "α"

padStencil :: Deriv' Id Id
padStencil = mkDeriv "padStencil"
             [("α",unit Type),("n",unit Type)]
             [("inp",unit (Array Positive np1 (TVar True "α")))
             ,("out",unit (Array Positive np1 (Array Negative (sizeLit 3) (TVar False "α"))))
             ,("dup", unit (Array Positive np1 (TVar True "α") :|: (Array Positive np1 (TVar True "α") :*: Array Positive np1 (TVar True "α"))))
             ,("drop",unit (Array Positive np1 (TVar True "α") :|: Array Positive n (TVar True "α")))
             ,("zero",unit (TVar True "α"))
             ] $
             BigTensor "elms" "out" $ undefined
