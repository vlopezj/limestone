{-# LANGUAGE FlexibleContexts #-}
module Data.Generics.Geniplate.Extra(module G
                                    ,transformBiDefault)
       where

import Data.Generics.Geniplate as G

import Data.Functor.Identity

transformBiDefault :: TransformBiM Identity a b => (a -> a) -> b -> b
transformBiDefault = (runIdentity .) . transformBiM . (Identity .)

