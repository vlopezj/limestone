{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE TransformListComp #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
module LL.Para 
{-# DEPRECATED "Use LL.Traversal, LL.Fold and TypeCheck instead" #-}
where

import LL
import Fresh
import Control.Monad
import Control.Monad.State
import Control.Applicative
import qualified Data.Traversable as T
import qualified Data.Foldable as T
import Data.List (partition)
import Text.Show.Pretty (ppShow)
import GHC.Exts
import Control.Arrow ((&&&))
import Control.Lens hiding (Fold, Choice, snoc)
import Control.Monad.Except.Extra
import Type.Data

-- Paramorphism for sequents.

type SeqFinal t a = SeqFinal' Name t a
type SeqFinal' name t a = SeqFinal'' name Int t a
type IdSeqFinal t a = SeqFinal'' Id Id t a

type SeqFinal'' name ref t = SeqFinal''' name ref t t t

data SeqFinal''' name ref t sz tsz a = SeqFinal
    { sty         :: Ctx' name ref -> Type' name ref -> t
    , ssz         :: Ctx' name ref -> Size ref -> sz
    , stysz       :: Ctx' name ref -> Sized' name ref -> tsz
    , sax         :: name -> name -> t -> a
    , sncut       :: [(name,t,name,t)] -> a -> a -> a
    , ssync       :: ([(name,tsz)],a)
                  -> Maybe ([(name,tsz)],a)
                  -> Maybe ([(name,tsz)],a)
                  -> a
    , shalt       :: a
    , scross      :: Boson -> name -> name -> t -> name -> t -> a -> a
    , sSplit      :: name -> Size ref -> name -> Size ref -> name -> t -> a -> a
    , sBigTensor  :: name -> name -> t -> t -> Size ref -> a -> a
    , spar        :: Boson -> name -> name -> t -> name -> t -> a -> a -> a
    , sMerge      :: name -> name -> name -> a -> a
    , swith       :: Boson -> t -> LL.Choice -> name -> name -> t -> a -> a
    , splus       :: name -> name -> t -> name -> t -> a -> a -> a
    , stapp       :: Boson -> name -> t -> t -> name -> t -> a -> a
    -- ^ let x = u @ ty in s
    , stunpack    :: name -> name -> t -> name -> t -> a -> a
    , sbot        :: name -> a
    , szero       :: name -> [(name, t, sz)] -> a
    , sfold       :: name -> name -> t -> a -> a
    , sunfold     :: name -> name -> t -> a -> a
    , sone        :: Boson -> name -> a -> a
    , soffer      :: Boson -> name -> name -> t -> a -> a
    , sdemand     :: name ->  name -> t -> a -> a
    , signore     :: name -> t -> a -> a
    , salias      :: Boson -> name -> name -> t -> a -> a
    , swhat       :: Name -> [name] -> a
    , smem        :: t -> a -> a -> a
    , ssave       :: name -> t -> name -> a -> a
    , sload       :: name -> t -> name -> a -> a
    , sBigPar     :: name -> t -> [(name,a,Size ref)] -> a
    , sPermute    :: name -> sz -> sz -> a -> a
    , stile       :: name -> t -> a -> a
    , snatrec     :: name -> name -> name -> name -> name -> a -> a -> a   -- NatRec x0 xn y n z base ind
    , sfoldmap    :: [t] -> t -> a -> a -> a -> a
    , sfoldmap2   :: [t] -> t -> a -> a -> a -> a -> a

    , sintop      :: name -> IntOp -> name -> name -> a -> a
    , sintcmp     :: name -> CmpOp -> name -> name -> a -> a
    , sintlit     :: name -> Integer -> a -> a
    , sintcopy    :: name -> name -> name -> a -> a
    , sintignore  :: name -> a -> a
    , sfix        :: name -> t -> name -> t -> a -> a
    , sSizeCmp    :: sz -> sz -> [(CmpOp,a)] -> a
    }


foldIdSeqNoFresh :: (IdDeriv -> SeqFinal''' Id Id ty sz tysz a) -> Name -> IdCtx -> IdCtx -> IdSeq -> a
foldIdSeqNoFresh sf who ictx ctx s = runFreshMFrom s $ foldIdSeq sf who ictx ctx s

foldIdSeqFresh :: (IdDeriv -> SeqFinal''' Id Id ty sz tysz (FreshM a)) -> Name -> IdCtx -> IdCtx -> IdSeq -> a
foldIdSeqFresh sf who ictx ctx s = runFreshMFrom s $ join $ foldIdSeq sf who ictx ctx s

lookDel :: Eq a => a -> [(a,b)] -> Maybe (b,[(a,b)])
lookDel _ [] = Nothing
lookDel k0 (x@(k,v):xs) | k == k0 = Just (v,xs)
                        | otherwise = second (x:) <$> lookDel k0 xs

foldIdSeq :: forall ty sz tysz a m. (MonadFresh m) =>
       (Deriv' Id Id -> SeqFinal''' Id Id ty sz tysz a)
     -> Name
     -> Ctx' Id Id -- ^ "ty vars"
     -> Ctx' Id Id -- ^ vars
     -> Seq' Id Id
     -> m a
foldIdSeq sf who ts vs (Seqn0 s0) = go ts vs s0
  where
    go :: Ctx' Id Id -> Ctx' Id Id -> Seqn0' Id Id -> m a
    go ts vs (S' s0) = case s0 of
        Ax x y -> fetchVar x $ \vt ->
                    fetchVar y $ \vt' -> 
             if vt `eqType` neg vt'
                    then return $ sax x y (sty ts vt)
                    else oops $ "Incompatible types in axiom:\n" ++ show x ++ " : " ++ show vt ++ "\n" ++
                         show y ++ " : " ++ show vt'
          where fetchVar v k = case lookup v vs of
                          Nothing -> oops $ "ax: variable " ++ show v ++ " not found in " ++ show vs
                          Just (ty :^  1) -> k ty
                          Just (_  :^ sz) -> oops $ "ax: variable " ++ show v ++ " has wrong size: " ++ show sz
        NCut _ fan sz cs s t -> sncut [(v,fty (neg vt),v',fty vt) | (v,v',vt) <- cs]
                                  <$> go ts (demL vs_l ++ [(v,neg vt:^szL) | (v,_,vt) <- cs]) s
                                  <*> go ts ([(v,vt:^szR) | (_,v,vt) <- cs] ++ demR vs_r) t
          where
            (vs_l,vs_r) = partition ((`T.elem` (S s)) . fst) vs
            dem = demoteBy sz
            (szL,szR,demL,demR) = case fan of FanLeft -> (sizeOne,sz,dem,id); FanRight -> (sz,sizeOne,id,dem)

        Sync{..} -> do
          case forM syncIntr (_2 %%~ mkData) of
            Left  msg -> oops$ "Non-data type in sync: " ++ msg 
            Right syncIntr' ->
                let
                    ((vs_l,vs_r',vs_c'),_) = flip runState vs $ do
                        vs_l <- state $ partition ((`T.elem` (S contL)) . fst) 
                        vs_r' <- T.forM syncContR $ \syncContR -> 
                            state $ partition ((`T.elem` (S syncContR))  . fst)
                        vs_c' <- T.forM syncCont $ \syncCont -> 
                            state $ partition ((`T.elem` (S syncCont))  . fst)
                        return (vs_l, vs_r', vs_c')

                    upd = T.forM syncCont $ \syncCont -> do
                        let Just vs_c = vs_c'
                        let vsIntr    = [ (syncWIntr, (arrayOp Neutral syncSteps $ dataUpdate syncTy) :^ syncSz)
                                        | (syncSz,syncTy,_,Just SyncUpdate{..},_) <- syncIntr']
                        lift $ ([(v,ftysz tysz) | (v,tysz) <- vsIntr],) <$> go ts (vs_c ++ vsIntr) syncCont

                    rd = T.forM syncContR $ \syncContR -> do
                        let Just vs_r = vs_r'
                        let vsIntr = [ (syncYIntr, dataRead syncTy :^ syncCopies * syncSz)
                                     | (syncSz,syncTy,_,_,Just SyncRead{..}) <- syncIntr']
                        lift $ ([(v,ftysz tysz) | (v,tysz) <- vsIntr],) <$> go ts (vs_r ++ vsIntr) syncContR 
                in

                flip catchExceptT oops $ do
                        let vsIntr = [(xIntr, dataWrite syncTy :^ syncSz) | (syncSz, syncTy, xIntr, _, _) <- syncIntr']
                        ssync <$> lift (([(v,ftysz tysz) | (v,tysz) <- vsIntr],) <$> go ts (vs_l ++ vsIntr) contL)
                              <*> upd
                              <*> rd 
                

        Halt refs -> go vs refs
          where
            go vs [] = return shalt
            go vs (x:xs) =
              case rmlkupKs x vs of
                (_:^0,k) -> go (k []) xs
                (_:^_,_) -> error $ "Non empty variable in context with halt."

        Cross β x v v' t ->
          case rmlkupKs x vs of
            ((T (vt :*: vt')):^sz,k) -> scross β x v (fty vt) v' (fty vt') <$> go ts (k [(v,vt :^ sz),(v',vt':^sz)]) t
            (ty,_) -> error ("Wrong type for variable " ++ show x ++ " : " ++ show ty)

        Par β x v s v' t ->
          {-
           case fan of
             FanLeft ->
-}
               spar β x v (fty vt) v' (fty vt')
                                    <$> go ts (dem vs_ll ++ (v,vt:^sizeOne):dem vs_rl) s
                                    <*> go ts (vs_lr ++ (v',vt':^sz):vs_rr) t
    {-
             FanRight ->
               spar β x v (fty vt) v' (fty vt')
                                      <$> go ts (vs_ll ++ (v,vt:^sz):vs_rl) s
                                      <*> go ts (dem vs_lr ++ (v',vt':^sizeOne):dem vs_rr) t
-}
          where
            (~(T (vt :|: vt')):^sz,vsl,vsr) = rmlkuplr x vs
            (vs_ll,vs_lr) = partition ((`T.elem` (S s)) . fst) vsl
            (vs_rl,vs_rr) = partition ((`T.elem` (S s)) . fst) vsr
            dem = demoteBy sz

        Plus{..} -> splus zElim (branchName inl) (fty vt)
                                (branchName inr) (fty vt')
                                <$> (go ts (k (branchName inl, vt  :^sizeOne)) (branchProg inl))
                                <*> (go ts (k (branchName inr, vt' :^sizeOne)) (branchProg inr))
             where (~(T (vt :+: vt')):^ 1,k) = rmlkupK zElim vs

        With β x b v t -> swith β (fty (T (vt :&: vt'))) b x v (fty wt) <$> go ts (k (v,wt:^sz)) t
           where wt = choice b vt vt'
                 (~(T (vt :&: vt')):^sz,k) = rmlkupK x vs

        SBot x -> return $ sbot x
           where Just (~(T Bot) :^ 1) = lookup x vs
        SZero z xs -> return $ szero z [(x,fty xt,fsz xsz) | x <- xs, let Just (xt:^xsz) = lookup x vs' ]
           where Just (~(T Zero) :^ 1,vs') = lookDel z vs
        SOne β x t -> sone β x <$> go ts vs' t
          where Just (~(T One):^_sz,vs') = lookDel x vs
        TApp β x tyB v s -> do
            ty <- tySubst tv tyB tyA
            stapp β x (sty ts forall_ty) (sty ts ty) v (fty tyB) <$> go ts (k (v,ty:^sz)) s
          where (~forall_ty@(T (Forall tv _ tyA)):^sz,k) = rmlkupK x vs
        TUnpack{zElim = x, tyIntr = tw, xIntr = v, cont = s} ->
          stunpack tw x (sty ts (T (Exists tw_old ttw tyA))) v (sty ((tw,ttw:^sz):ts) new_ty) <$>
          go ((tw,ttw:^sizeOne):ts) (k (v,new_ty:^sz)) s
          where
            (~(T (Exists tw_old ttw tyA):^sz),k) = rmlkupK x vs
            new_ty = tyRename tw_old tw tyA

        Offer β x v s -> soffer β x v (fty tyA) <$> go ts [(v,tyA:^sizeOne)] s
          where Just ~(T (Quest tyA):^1) = lookup x vs
        Demand x v s -> sdemand x v (fty tyA) <$> go ts ((v,tyA:^sz):vs) s
          where Just ~(T (Bang tyA):^sz) = lookup x vs

        Load z sz x s -> sload z (fty ty) x <$> go ts (vs ++ [(x,ty :^ sz)]) s
          where Just (ty :^ 1) = lookup z ts

        Save z x s -> ssave z (fty tyA) x <$> go ((x,tyA:^sizeOne):ts) vs' s
          where Just (~(T (Bang tyA):^1),vs') = lookDel z vs

        -- Ignore _β x s -> signore x (fty tyA:^sz) <$> go ts vs s
        --   where Just ~(Bang tyA:^sz) = lookup x vs
        -- Alias β x w' s -> salias β x w' (fty tyA) <$> go ts ((w',Bang tyA:^sz):vs) s
        --   where Just ~(Bang tyA:^sz) = lookup x vs

        Fold x v s -> sfold v x (sty ts ty) <$> (go ts ((v,ty:^sz):vs) s)
          where
            Just ~(T (Rec Mu u tyR):^sz) = lookup x vs
            ty = foldTy' u tyR

        Unfold x v s -> sunfold v x (sty ts ty) <$> (go ts ((v,ty:^sz):vs) s)
          where
            Just ~(T (Rec Nu u tyR):^sz) = lookup x vs
            ty = unfoldTy' u tyR

        What (whatString -> x) ws _ -> return $ swhat x [w | ElemLinear w <- ws]
        -- Mem w w' ty _ n t u -> smem (fty ty) <$> (go ts ((w,neg ty):vs) t)
        --                                      <*> (go ts (replicate n (w',Bang ty) ++ vs) u)
          -- FIXME: allocate fresh names instead of (replicate n w')
        -- Tile x sz s -> stile x (sty ts x_ty) <$> (go ts ((x,x_ty):vs) s)
        --   where x_ty = Array Negative sz (Index True)
        BigPar {..} ->
          sBigPar zElim (fty tyA) <$> gol vs'L vs'R sz branches

                 -- <$> go ts (demoteBy splitSz vs_ll ++ (branchName handled,tyA:^sizeOne):demoteBy splitSz vs_rl) (branchProg handled)
                 -- <*> go ts (vs_lr ++ (branchName rest,bigPar szRest tyA:^sizeOne):vs_rr) (branchProg rest)
          where
            (T (Array ANegative sz tyA):^ sizeOne,vs'L,vs'R) = rmlkuplr zElim vs
            gol :: IdCtx -> IdCtx -> IdSize -> [(IdSize,Branch Id Id)] -> m [(Id,a,IdSize)]
            gol vsLocL vsLocR szLoc [] = return [] -- FIXME: check for empty context and zero size.
            gol vsLocL vsLocR szLoc ((splitSz,Branch name prog):bs) = do
               let (vsNowL,vsLaterL) = partition ((`T.elem` (S prog)) . fst) vsLocL
                   (vsNowR,vsLaterR) = partition ((`T.elem` (S prog)) . fst) vsLocR
                   szRest = szLoc - splitSz
               now <- (,,) <$> pure name <*> go ts (demoteBy splitSz vsNowL++(name,tyA:^sizeOne):demoteBy splitSz vsNowR) prog <*> pure splitSz
               later <- gol vsLaterL vsLaterR szRest bs
               return (now:later)

        BigTensor x z s ->
            case rmlkupK z vs of
              (tz@(T (Array APositive sz1 tyA)):^ sizeOne,k) -> sBigTensor x z (fty tz) (fty tyA) sz1 <$> go ts (k (x,tyA:^ sz1)) s
              _ -> return $ swhat "wrong big tensor" [z]

        Split z sz1 x y s -> case rmlkupKs z vs of
          (t0 :^  sz12,k) ->
            let tyA = t0 :^ sz1
                tyB = t0 :^ sz2
                sz2 = sz12 - sz1
            in  sSplit x sz1 y sz2 z (fty (T (Array APositive sz12 t0))) <$> go ts (k [(x,tyA),(y,tyB)]) s

        Merge x y _ z s -> do
          let (tz:^sz,vs') = rmlkup z vs
              (ty:^sy,vs'') = rmlkup y vs'
          when (tz /= ty) $ error "merging different types"
          sMerge x y z <$> go ts ((x,ty:^(sy + sz)):vs'') s
        Permute z sz' x s -> let (tz:^sz,vs') = rmlkup z vs in
                              sPermute x (fsz sz) (fsz sz') <$> go ts ((x,tz:^sz'):vs') s
    {-
        Foldmap {..} -> do
            k <- freshFrom "k"
            let tyMn = fmTyM
            tyM0 <- tySzSubst fmSz (sizeZero) fmTyM
            tyMk  <- tySzSubst fmSz (sizeVar k) fmTyM
            tyMk1 <- tySzSubst fmSz (sizeOne + sizeVar k) fmTyM

            pure (sfoldmap (map fty tyAs)
                           (fty fmTyM))
                  <*> go ts ((fmMapTrg,unit $ neg tyM0):demoteBy (sizePow $ sizeVar fmSz) zs) fmMap
                  <*> go ts ((fmMixL,unit tyMk):(fmMixR,unit tyMk):(fmMixTrg,unit $ neg tyMk1):[]) fmMix
                  <*> go ts ((fmMon,unit tyMn):vs') fmCont
           where (zs,vs') = partition (\(v,_) -> v `elem` fmArr) vs
                 tyAs = [tyA | (_,tyA :^ _sz') <- zs]
     -}
        Foldmap2 {..} -> do
            let tyM0 = fmTyM
            let tyMk = fmTyM

            pure (sfoldmap2 (map fty tyAs)
                            (fty fmTyM))
                  <*> go ts ((fmMapTrg,unit $ neg tyM0):demoteBy fmSize zs) fmMap
                  <*> go ts ((fmMixL,unit tyMk):(fmMixR,unit tyMk):(fmMixTrg,unit $ neg tyMk):[]) fmMix
                  <*> go ts ((fmUnitTrg,unit $ neg fmTyM):[]) fmUnit
                  <*> go ts ((fmMon,unit tyMk):vs') fmCont
           where (zs,vs') = partition (\(v,_) -> v `elem` fmArr) vs
                 tyAs = [tyA | (_,tyA :^ _sz') <- zs]
  
        IntOp op z1 z2 x s -> matchSizes sz sz' $ sintop x op z1 z2 <$> go ts ((x,int:^sz):vs2) s
          where
            (T (TInt True):^sz,vs1) = rmlkup z1 vs
            (T (TInt True):^sz',vs2) = rmlkup z2 vs1

        IntCmp cmp z1 z2 x s -> matchSizes sz sz' $ sintcmp x cmp z1 z2 <$> go ts ((x,bool:^sz):vs2) s
          where
            (T (TInt True):^sz,vs1) = rmlkup z1 vs
            (T (TInt True):^sz',vs2) = rmlkup z2 vs1


        IntLit x lit s -> sintlit x lit <$> go ts ((x,int:^sizeOne):vs) s

        IntCopy z x y s -> sintcopy x y z <$> go ts ((x,int:^sz):(y,int:^sz):vs1) s
          where
            (T (TInt True):^sz,vs1) = rmlkup z vs

        IntIgnore z s -> sintignore z <$> go ts vs1 s
          where
            (T (TInt True):^_sz,vs1) = rmlkup z vs

        SizeCmp sz1 sz2 ss ->
          sSizeCmp (fsz sz1) (fsz sz2) <$> T.sequenceA [(op,) <$> go ts vs s | (op,s) <- ss]

        -- Fix x z _ty s -> sfix x (Fty fix_ty') z (fty ty) <$> go ts ((x,fix_ty'):vs1) s
        --   where
        --     (ty,vs1) = rmlkup z vs
        --     fix_ty' = fixTy ty

     where
        oops msg = return $ swhat ("[" ++ msg ++ "]") (map fst vs)

        fty = sty ts
        fsz = ssz ts
        fsty (a :^ b) = (fty a, fsz b)
        ftysz = stysz ts
          
        -- fctx = map (second fty)
        SeqFinal{..} = sf (D0 $ Deriv who (seqctx ts vs) (S0 s0))
        matchSizes sz sz' k = if sz `eqSize` sz'
            then k
            else error "mismatching sizes"


        rmlkupKs v xs = let (t,l,r) = rmlkuplr v xs in (t,\ ys -> l++ys++r)
        rmlkupK v xs  = let (t,l,r) = rmlkuplr v xs in (t,\ x ->  l++x:r)

        rmlkuplr v xs = case break ((v ==) . fst) xs of
                          (l,(_,t):r) -> (t,l,r)
                          _ -> error $ "Unknown variable : " ++ show v ++
                                       "\n in context " ++ ppShow xs ++
                                       "\n for sequent: " ++ ppShow s0
        rmlkup v xs = let (t,l,r) = rmlkuplr v xs in (t,l++r)




foldIdDeriv :: (IdDeriv -> SeqFinal''' Id Id ty sz tysz a) -> IdDeriv -> a
foldIdDeriv sf (D0 (Deriv who (seqctx_ -> (ts, vs)) s)) = foldIdSeqNoFresh sf who ts vs s

seqRefs :: (Ord ref) => [SeqStep name ref] -> [ref]
seqRefs ss = [the ref | ref <- join
                                 [case t of
                                    SeqStep{..} -> [ref | (ref,_) <- seqChunk]
                                 | t <- ss],
                        then group by ref using groupWith]


data BranchCmp = BGEq | BEq | BLEq

{-
seqRefsUsedTraversal :: Seq' name ref -> [ref]
seqRefsUsedTraversal =
  $(do
       vname <- VarT <$> newName "name"
       vref <- VarT <$> newName "ref"
       tSize <- pure $ ConT ''Size
       tMetavar <- pure $ ConT ''MetaVar
       tType' <- pure $ ConT ''Type'
       tSeq' <- pure $ ConT ''Type'
       
       genUniverseBiT [   pure $ AppT tSize vref
-}
