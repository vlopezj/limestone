{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}
module LL.Traversal(module LL.Traversal) where

import LL.Core
import LL.Traversal.TH as LL.Traversal
import Data.Default
import Data.Generics.Genifunctors
import Control.Monad
import Control.Monad.Writer
import Control.Applicative
import Data.Bifunctor
import Data.Bifoldable
import Data.Traversable
import Data.Bitraversable
import Data.Functor.Identity
import Data.Generics.Geniplate.Extra
import Control.Lens

data SeqTraverse m seq  ty  sz  tysz  name  ref
                   seq' ty' sz' tysz' name' ref' =
  SeqTraverse {
    tseq  :: seq  -> m seq',
    tty   :: ty   -> m ty',
    tsz   :: sz   -> m sz',
    ttysz :: tysz -> m tysz',
    tname :: name -> m name',
    tref  :: ref  -> m ref' }

traverseSeq'' :: (Applicative f) => SeqTraverse f a₁ a₂ a₃ a₄ a₅ a₆ b₁ b₂ b₃ b₄ b₅ b₆
                                  -> Seq'' a₁ a₂ a₃ a₄ a₅ a₆
                                  -> f (Seq'' b₁ b₂ b₃ b₄ b₅ b₆)
traverseSeq'' SeqTraverse{..} x = genTraverseSeq'' tseq tty tsz ttysz tname tref x

traverseSeqStep' :: (Applicative f) => SeqTraverse f a₁ a₂ a₃ a₄ a₅ a₆ b₁ b₂ b₃ b₄ b₅ b₆
                                  -> SeqStep' a₁ a₃ a₅ a₆
                                  -> f (SeqStep' b₁ b₃ b₅ b₆)
traverseSeqStep' seqt = fmap (head . fence) . traverseSeq'' seqt . BigSeq . (:[])

traverseDeriv'' :: (Applicative m) => SeqTraverse m seq ty sz tysz name ref 
                                 seq' ty' sz' tysz' name' ref'
                -> Deriv'' seq name ref
                -> m (Deriv'' seq' name' ref')
traverseDeriv'' SeqTraverse{..} = genTraverseDeriv'' tseq tname tref

defSeqTraverse :: (Applicative m) => SeqTraverse m seq ty sz tysz name ref
                                                   seq ty sz tysz name ref
defSeqTraverse = SeqTraverse pure pure pure pure pure pure

defSeqTraverseVoid :: (Applicative m) => SeqTraverse m seq ty sz tysz name ref
                                                       ()  () () ()   ()   ()
defSeqTraverseVoid = SeqTraverse v v v v v v where v _ = pure () 

data SeqnFold m ann seq ty sz tysz name ref
                        ty' sz' tysz' name' ref' =
  SeqnFold {
      sseq  :: ann -> Seq'' seq ty sz tysz name ref -> m seq
     ,sty   :: ann -> ty' -> m ty
     ,ssz   :: ann -> sz' -> m sz
     ,stysz :: ann -> tysz' -> m tysz
     ,sname :: ann -> name' -> m name
     ,sref  :: ann -> ref'  -> m ref
    } 

defSeqnFold :: Applicative m => (SeqnFold m ann (Seqn ann ty sz tysz name ref)
                                          ty sz tysz name ref
                                          ty sz tysz name ref)
defSeqnFold = SeqnFold ((pure .) . Seqn) p p p p p where p _ = pure 

foldSeqn :: (Applicative m, Monad m)
         => SeqnFold m ann seq ty' sz' tysz' name' ref'
                               ty  sz  tysz  name  ref
         -> Seqn ann ty sz tysz name ref
         -> m seq
foldSeqn fold@SeqnFold{..} (Seqn ann seq) =
  do
    seq' <- traverseSeq'' SeqTraverse{..} seq
    sseq ann seq'
  where
    tname = sname ann
    tref  = sref ann
    tty   = sty ann
    tsz   = ssz ann
    ttysz = stysz ann
    tseq  = foldSeqn fold

foldSeqnWithSelf :: (Applicative m, Monad m)
         => SeqnFold m (Seqn ann ty sz tysz name ref)
                           seq ty' sz' tysz' name' ref'
                               ty  sz  tysz  name  ref
         -> Seqn ann ty sz tysz name ref
         -> m seq
foldSeqnWithSelf fold@SeqnFold{..} sn@(Seqn ann seq) =
  do
    seq' <- traverseSeq'' SeqTraverse{..} seq
    sseq sn seq'
  where
    tname = sname sn
    tref  = sref sn
    tty   = sty sn
    tsz   = ssz sn
    ttysz = stysz sn
    tseq  = foldSeqnWithSelf fold


-- | Names immediately mentioned by this sequent
names0Seq :: forall seq ty sz tysz name ref. Seq'' seq ty sz tysz name ref -> [name]
names0Seq x = execWriter $ traverseSeq'' defSeqTraverseVoid{tname,ttysz} x
            where
            tname :: name -> Writer [name] ()
            tname n = tell [n]

            ttysz :: tysz -> Writer [name] ()
            ttysz _ = return ()


seqs0Seq :: Seq'' seq ty sz tysz name ref -> [seq]
seqs0Seq x = execWriter $ traverseSeq'' defSeqTraverseVoid{tseq} x
            where
            tseq s = tell [s]


fmapSeq'' :: SeqFmap seq' ty' sz' tysz' name' ref'
                     seq  ty  sz  tysz  name  ref 
          -> Seq'' seq' ty' sz' tysz' name' ref' -> Seq'' seq ty sz tysz name ref
fmapSeq'' = (runIdentity.) . traverseSeq'' . seqTraverseFromFmap

seqTraverseFromFmap :: (Applicative m)
                    => SeqFmap seq  ty  sz  tysz  name  ref
                               seq' ty' sz' tysz' name' ref'
                    -> SeqTraverse m seq  ty  sz  tysz  name  ref
                                     seq' ty' sz' tysz' name' ref'
seqTraverseFromFmap SeqFmap{..} = SeqTraverse{..}
  where
    tseq = pure . fseq
    tty = pure . fty
    tsz = pure . fsz
    ttysz = pure . ftysz
    tname = pure . fname
    tref = pure . fref

defSeqFmap :: SeqFmap seq ty sz tysz name ref
                      seq ty sz tysz name ref
defSeqFmap = SeqFmap id id id id id id

data SeqFmap seq  ty  sz  tysz  name  ref
             seq' ty' sz' tysz' name' ref' =
  SeqFmap {
    fseq  :: seq  -> seq',
    fty   :: ty   -> ty',
    fsz   :: sz   -> sz',
    ftysz :: tysz -> tysz',
    fname :: name -> name',
    fref  :: ref  -> ref' }

fmapSeqStep' :: SeqFmap seq' ty' sz' tysz' name' ref'
                        seq  ty  sz  tysz  name  ref 
             -> SeqStep' seq' sz' name' ref' -> SeqStep' seq sz name ref
fmapSeqStep' = (runIdentity.) . traverseSeqStep' . seqTraverseFromFmap

transformBiMTypeSeq ::
  (Monad m
  ,Applicative m
  ,Wrapped s
  ,Rewrapped s s
  ,Unwrapped s ~ Seqn ann (Type' name ref) (Size ref) (Sized' name ref) a b) =>
  ((Type' name ref -> m (Type' name ref)) -> ann -> m ann) ->
  ((Type' name ref -> m (Type' name ref)) -> a   -> m a  ) ->
  ((Type' name ref -> m (Type' name ref)) -> b   -> m b  ) ->
  (Type' name ref -> m (Type' name ref)) ->
  s -> m s
transformBiMTypeSeq tann tname tref fty =
  _Wrapped $ foldSeqn SeqnFold{sseq, sty, ssz, stysz, sname, sref}
    where
      sseq ann s' = Seqn <$> tann fty ann <*> pure s'
      sty _ = transformBiM fty
      ssz _ = sizeAsTypeUnsafe fty
      stysz _ = transformBiM fty
      sname _ = tname fty 
      sref  _ = tref  fty 

  

instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Seqn0 name ref) where
  -- No types in the annotation
  transformBiM = transformBiMTypeSeq (\_ (ann :: ())  -> pure ()) (\_ (a :: name) -> pure a) (\_ (b :: ref) -> pure b)

instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Seqn1 name ref) where
  -- No types in the annotation
  transformBiM = transformBiMTypeSeq (\_ (a :: [ref]) -> pure a) (\_ (a :: name) -> pure a) (\_ (b :: ref) -> pure b)

instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Seqn2 name ref) where
  transformBiM = transformBiMTypeSeq transformBiM 
  -- Transform types in the type annotations too
                                     (tag . transformBiM)
                                     (tag . transformBiM)
      

transformBiMTypeDeriv ::
  (Monad m
  ,Applicative m
  ,Wrapped d
  ,Rewrapped d d
  ,Unwrapped d ~ Deriv'' seq name ref
  ,TransformBiM m (Type' name ref) seq) => (Type' name ref -> m (Type' name ref)) -> d -> m d
transformBiMTypeDeriv f = _Wrapped $ \d -> do
    let Deriv { _derivCtx = _derivCtx'
              , _derivSequent = _derivSequent'
              , _derivName
              } = d 
    _derivCtx <- transformBiM f _derivCtx'
    _derivSequent <- transformBiM f _derivSequent'
    return $ Deriv { _derivName
                   , _derivCtx
                   , _derivSequent
                   }

instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Deriv' name ref) where
  transformBiM = transformBiMTypeDeriv
 
instance TransformBi (Type' name ref) (Seq' name ref) where
  transformBi = transformBiDefault

instance TransformBi (Type' name ref) (Deriv' name ref) where
  transformBi = transformBiDefault


travDeriv :: (Type' name ref -> Type' name ref) -> Deriv' name ref -> Deriv' name ref
travDeriv = transformBi

travDerivM :: (Applicative m, Monad m) => (Type' name ref -> m (Type' name ref)) -> Deriv' name ref -> m (Deriv' name ref)
travDerivM = transformBiM

travSeq ::(Type' name ref -> Type' name ref) -> Seq' name ref -> Seq' name ref
travSeq = transformBi

travSeqM :: (Applicative m, Monad m) => (Type' name ref -> m (Type' name ref)) -> Seq' name ref -> m (Seq' name ref)
travSeqM = transformBiM



instance Bifunctor (Seqn ann ty sz tysz) where
  bimap = bimapDefault

instance Bifoldable (Seqn ann ty sz tysz) where
  bifoldMap = bifoldMapDefault
                                               
instance Bitraversable (Seqn ann ty sz tysz) where
  bitraverse f g (s@Seqn{_seq=_seq'}) = (\_seq -> s{_seq}) <$> traverseSeq'' defSeqTraverse{tseq = bitraverse f g
                                                                                           ,tname = f
                                                                                           ,tref  = g
                                                                                           } _seq'
                                               

  
