{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
-- | Sequents names and references should be traversed in the order in which
--   they come into scope. However, that's usually not the most convenient
--   way to represent them.
--
--   This module decouples representation from traversal order
module LL.Traversal.TH(genTraverseSeq''
                   ,genTraverseDeriv''
                   ) where
import Data.Proxy
import LL.Core
import Data.Bitraversable
import Data.Bifoldable
import Data.Bifunctor
import Data.Traversable as T
import Data.Foldable as T
import Control.Applicative
import Data.Generics.Genifunctors
import Data.Generics.Geniplate
import Data.Monoid
import Data.Maybe
import LL.Traversal.Wrappers
import Control.Lens

fmap6 f = fmap $ fmap $ fmap $ fmap $ fmap $ fmap f
fmap7 f = fmap $ fmap6 f
infixl 4 `fmap7`

-- traverse f t = sequence t . fmap f
traverse' :: (Traversable t, Applicative f) => (t a -> b) -> (t (f a) -> f b)
traverse' f t = fmap f $ sequenceA t
sequence7 x = (traverse' $ traverse' $ traverse' $
               traverse' $ traverse' $ traverse' $
               traverse' $ id) x
a `ap7` b = (liftA2 . liftA2 . liftA2 . liftA2 . liftA2 . liftA2 $ (<*>)) a b
infixl 4 `ap7`


alts as b = fromMaybe `fmap7` b `ap7` ((getFirst . mconcat . (map First)) `fmap7` sequence7 as)

genTraverseSeq'' :: (Applicative f)
              => (a₁ -> f b₁)
              -> (a₂ -> f b₂)
              -> (a₃ -> f b₃)
              -> (a₄ -> f b₄)
              -> (a₅ -> f b₅)
              -> (a₆ -> f b₆)
              -> (Seq'' a₁ a₂ a₃ a₄ a₅ a₆) -> f (Seq'' b₁ b₂ b₃ b₄ b₅ b₆)
genTraverseSeq'' = alts 
                  [wrapSeq `fmap6` ($(genTraverse ''TraverseBigSeq))
                  ,wrapSeq `fmap6` ($(genTraverse ''TraverseNCut))]
                  ($(genTraverse ''Seq''))

genTraverseDeriv'' :: (Applicative m) 
                => (seq -> m seq')
                -> (name -> m name')
                -> (ref -> m ref')
                -> Deriv'' seq name ref
                -> m (Deriv'' seq' name' ref')
genTraverseDeriv'' = $(genTraverse ''Deriv'') 

instance Bifunctor Seqn0 where bimap = bimapDefault                                           
instance Bifoldable Seqn0 where bifoldMap = bifoldMapDefault
instance Bitraversable Seqn0 where bitraverse = $(genTraverseT [(''Seq'', 'genTraverseSeq'')] ''Seqn0)

instance Bifunctor Seqn1 where bimap = bimapDefault                                           
instance Bifoldable Seqn1 where bifoldMap = bifoldMapDefault
instance Bitraversable Seqn1 where bitraverse = $(genTraverseT [(''Seq'', 'genTraverseSeq'')] ''Seqn1)

instance Bifunctor Seqn2 where bimap = bimapDefault                                           
instance Bifoldable Seqn2 where bifoldMap = bifoldMapDefault                                           
instance Bitraversable Seqn2 where bitraverse = $(genTraverseT [(''Seq'', 'genTraverseSeq'')] ''Seqn2)
                                           
instance Bifunctor Deriv0 where bimap = bimapDefault                                           
instance Bifoldable Deriv0 where bifoldMap = bifoldMapDefault
instance Bitraversable Deriv0 where bitraverse = $(genTraverseT [(''Seqn0, 'bitraverse)] ''Deriv0)

instance Bifunctor Deriv1 where bimap = bimapDefault                                           
instance Bifoldable Deriv1 where bifoldMap = bifoldMapDefault
instance Bitraversable Deriv1 where bitraverse = $(genTraverseT [(''Seqn1, 'bitraverse)] ''Deriv1)

instance Bifunctor Deriv2 where bimap = bimapDefault                                           
instance Bifoldable Deriv2 where bifoldMap = bifoldMapDefault                                           
instance Bitraversable Deriv2 where bitraverse = $(genTraverseT [(''Seqn2, 'bitraverse)] ''Deriv2)

instance Functor (Seqn0 a) where fmap = bimap id
instance Functor (Seqn1 a) where fmap = bimap id
instance Functor (Seqn2 a) where fmap = bimap id

instance Foldable (Seqn0 a) where foldMap = bifoldMap (const mempty)
instance Foldable (Seqn1 a) where foldMap = bifoldMap (const mempty)
instance Foldable (Seqn2 a) where foldMap = bifoldMap (const mempty)

instance Traversable (Seqn0 a) where traverse = bitraverse pure
instance Traversable (Seqn1 a) where traverse = bitraverse pure
instance Traversable (Seqn2 a) where traverse = bitraverse pure

instance Functor (Deriv0 a) where fmap = bimap id
instance Functor (Deriv1 a) where fmap = bimap id
instance Functor (Deriv2 a) where fmap = bimap id

instance Foldable (Deriv0 a) where foldMap = bifoldMap (const mempty)
instance Foldable (Deriv1 a) where foldMap = bifoldMap (const mempty)
instance Foldable (Deriv2 a) where foldMap = bifoldMap (const mempty)

instance Traversable (Deriv0 a) where traverse = bitraverse pure
instance Traversable (Deriv1 a) where traverse = bitraverse pure
instance Traversable (Deriv2 a) where traverse = bitraverse pure

