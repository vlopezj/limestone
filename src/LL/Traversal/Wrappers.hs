{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
-- | Sequents names and references should be traversed in the order in which
--   they come into scope. However, that's usually not the most convenient
--   way to represent them.
--
--   This module decouples representation from traversal order
module LL.Traversal.Wrappers where

import Data.Proxy
import LL.Core
import Data.Bitraversable
import Data.Bifoldable
import Data.Bifunctor
import Data.Traversable as T
import Data.Foldable as T
import Control.Applicative
import Data.Generics.Genifunctors
import Data.Generics.Geniplate
import Data.Monoid
import Data.Maybe

class WrapSeq τ where
  wrapSeq :: Functor f => (τ a₁ a₂ a₃ a₄ a₅ a₆ -> f (τ b₁ b₂ b₃ b₄ b₅ b₆))
          -> Seq'' a₁ a₂ a₃ a₄ a₅ a₆ -> Maybe (f (Seq'' b₁ b₂ b₃ b₄ b₅ b₆))

-- ^ Controls the order in which names and references in a fence are traversed.
data TraverseBigSeq seq ty sz tysz name ref = TraverseBigSeq {
      -- | Sizes, types for sync boundaries
      bigseq1 :: [Maybe sz]
     ,bigseq2 :: [[ref]]
     ,bigseq3 :: [Maybe ([name], seq)]
    }

instance WrapSeq TraverseBigSeq where
  wrapSeq f t = case t of
    BigSeq{} -> Just $ fmap unwrap $ f $ wrap t
    _        -> Nothing
    where
        wrap (BigSeq steps) = TraverseBigSeq{..}
         where
            bigseq1 = [seqStepSz t | t <- steps]
            bigseq2 = [seqStepRefs t | t <- steps]
            bigseq3 = [case step of
                         SeqStep{..} ->
                           Just (map snd seqChunk,
                                 seqProg)
                      | step <- steps]

        unwrap TraverseBigSeq{..} = BigSeq $
             [case step0 of
                 SeqStep{} -> 
                   let 
                     Just seqSz = step1
                     seqChunkRef = step2
                     Just (seqChunkName, seqProg) = step3
                     seqChunk = seqChunkRef `zip` seqChunkName
                   in
                    SeqStep{..}
             | step0 <- steps
             | step1 <- bigseq1 
             | step2 <- bigseq2
             | step3 <- bigseq3 ]
          where
            BigSeq steps = t

data TraverseNCut seq ty sz tysz name ref = TraverseNCut {
      ncut1 :: sz
     ,ncut2 :: [ty]
     ,ncut3 :: [name]
     ,ncut4 :: seq
     ,ncut5 :: [name]
     ,ncut6 :: seq
    }

instance WrapSeq TraverseNCut where
  wrapSeq f t = case t of
    NCut{} -> Just $ fmap unwrap $ f $ wrap t
    _      -> Nothing
    where
      wrap NCut{..} = TraverseNCut{..}
        where
          ncut1 = ncut_size
          ncut2 = [ty | (_,_,ty) <- ncut_cs]
          ncut3 = [l | (l,_,_)  <- ncut_cs]
          ncut4 = ncut_left
          ncut5 = [r | (_,r,_)  <- ncut_cs]
          ncut6 = ncut_right
      
      unwrap TraverseNCut{..} = t{
          ncut_size = ncut1
         ,ncut_cs = [(l,r,ty)
                    | l <- ncut3
                    | r <- ncut5
                    | ty <- ncut2 ]
         ,ncut_left = ncut4
         ,ncut_right = ncut6
        }

