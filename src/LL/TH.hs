module LL.TH where

import Id
import Type.Core
import LL.Core
import Type.TH (thOnly)

tr3 :: Monad m => (IdType -> m IdType) -> IdSeq -> m IdSeq
tr3 = thOnly

tr4 :: Monad m => (IdSeq -> m IdSeq) -> IdSeq -> m IdSeq
tr4 = thOnly
