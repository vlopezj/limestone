{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE GADTs #-}

module LL.Core (module LL.Core) where

import Type as LL.Core hiding ((&))
import Type.Ctx
import Id
import Data.Foldable as T
import Data.Traversable as T
import Data.Bitraversable
import Data.Bifoldable
import Data.Bifunctor
import Control.Applicative
import Data.List (inits, tails)
import Data.Semigroup hiding (Dual)
import Fresh
import GHC.Exts (Constraint, IsString(..))
import Data.Generics.Genifunctors
import Control.Lens(makeLenses,makeWrapped)
import Op as LL.Core
import Data.List
import Data.List.NonEmpty(NonEmpty(..))

data Deriv'' seq name ref = Deriv
    { _derivName    :: Name          -- for debugging
    , _derivCtx     :: TypedCtx name ref
    , _derivSequent :: seq
    }
  deriving (Show)


type Seq   = Seq' Name Int
type Deriv = Deriv' Name Int

type IdDeriv = Deriv' Id Id
type IdSeq   = Seq' Id Id
type IdSeq2  = Seqn2 Id Id
type IdSeqn0  = Seqn0 Id Id
type IdSeqn1  = Seqn1 Id Id
type IdSeqn2  = Seqn2 Id Id

type IdDeriv0 = Deriv0 Id Id
type IdDeriv1 = Deriv1 Id Id
type IdDeriv2 = Deriv2 Id Id

type IdSeqCtxElem = SeqCtxElem IdType IdSize Id

d0 _derivName _derivCtx _derivSequent =
  D0 (Deriv{..})

type IdSeqStep    = SeqStep Id Id
type IdSyncUpdate = SyncUpdate Id Id
type IdSyncRead   = SyncRead Id Id

newtype Deriv0 name ref = D0 { deriv0 :: Deriv'' (Seqn0 name ref) name ref } deriving (Show)
newtype Deriv1 name ref = D1 { deriv1 :: Deriv'' (Seqn1 name ref) name ref } deriving (Show)
newtype Deriv2 name ref = D2 { deriv2 :: Deriv'' (Seqn2 name ref) name ref } deriving (Show)

type Deriv' = Deriv0

type Boson = Bool

defaultBoson = False

data What = FFI FFI      -- ^ Call to target language program
          | LLDeriv Name -- ^ Embed a derivation
          | Huh Name     -- ^ Throw an error
          | Builtin BIF
          deriving (Show, Read)

instance IsString What where
  fromString = Huh

data FFI where
  Call    :: String -> FFI
  PrimLit :: String -> FFI
  Macro   :: String -> FFI
           deriving (Show, Read)

data BIF where
  BIFSchedSeq :: BIF -- [⨂_n \Bot ⊢]
  BIFSchedPar :: BIF -- [⨂_n \Bot ⊢]
  BIFIndex    :: BIF -- [|_n ∀i. ⊢]
  BIFCoalesce :: BIF -- [Seq_n (A ⊕ 1) , (Seq_n (A ⊕ 1))^⊥ ⊢ ]
           deriving (Show, Read)

whatString :: What -> String
whatString (Huh s) = s
whatString (LLDeriv s) = s
whatString (FFI (Call s)) = s
whatString (FFI (PrimLit s)) = s
whatString (FFI (Macro s)) = s
whatString (Builtin BIFSchedSeq) = "seq"
whatString (Builtin BIFSchedPar) = "par"

choice :: Choice -> a -> a -> a
choice Inl l _ = l
choice Inr _ r = r

data Fanning = FanLeft | FanRight
  deriving (Show,Read,Eq,Ord)

instance Dual Fanning where
  dual FanLeft = FanRight
  dual FanRight = FanLeft

data Branch' seq name ref = Branch {
  branchName :: name,
  branchProg :: seq
  } deriving (Show,Read)

type Branch name ref = Branch' (Seqn0' name ref) name ref

type IdBranch = Branch Id Id

type TypedId = Id ::: IdSized
type TypedIdSeq = Seqn2 Id Id
type TypedIdBranch = Branch' (TypedIdSeq) (TypedId) (TypedId)
type TypedIdDeriv = Deriv2 Id Id

pattern TyS p = Seqn2 p
pattern TyD p = D2 p

-- | Sequents. A 'Forced Type' is a type which is fully determined by
-- the context. They can be filled in by the function 'fillTypes' below.
data Seq'' seq ty sz tysz name ref
    = Ax { wElim :: ref,
           zElim :: ref } -- ^ Exactly 2 vars; will always be {0,1} in index notation

    | NCut {ncut_info :: CutInfo,
            ncut_fanning :: Fanning,
            ncut_size :: sz,
            ncut_cs :: [(name,name,ty)],  -- The second variable's type
            ncut_left :: seq,
            ncut_right :: seq }
    | Halt {zElims :: [ref]} -- ^ eliminate variables of size 0

    | Cross { β :: Boson
            , zElim :: ref
            , xIntr :: name
            , yIntr :: name
            , cont  :: seq
            }
    -- | Splits at given position.
    | Par { β :: Boson,
            zElim :: ref,
            xIntr :: name,
            contL :: seq,
            yIntr :: name,
            contR :: seq
          } 
    | Plus { zElim :: ref,
             inl,inr :: Branch' seq name ref
           }
    -- | Choose one of the alternatives for a "&" pair.
    | With {  β      :: Boson
           , zElim  :: ref
           , choose :: Choice 
           , xIntr  :: name
           , cont   :: seq
           } -- Rename to Choose

    -- | Discard a 1
    | SOne { β :: Boson,
             zElim :: ref,
             cont :: seq }      -- Rename to ...

    -- | Synchronize one or more poles
    | SPole { zElims :: [ref] }      -- Rename to ...


    -- these will need references (or names)!
    | SZero { zElim :: ref, elim :: [ref] }  -- Rename to Crash/Loop
    | SBot { zElim :: ref }            -- Rename to Terminate
    -- | 'meta' program. Used for typesetting, and FFI calls.
    | What { what         :: What
           , zVars   :: [SeqCtxElem ty sz ref]
           , disp    :: [Int]
           } {- ^ things consumed by this meta program -}

    -- | Polymorphic types
    | TApp { β :: Boson
           , zElim :: ref
           , ty    :: ty
           , xIntr :: name
           , cont :: seq
           }

    | TUnpack { zElim  :: ref
              , tyKind :: ty
              , tyIntr :: name
              , xIntr  :: name
              , cont   :: seq
              }
    -- | Produce and consume values following an exponential protocol
    | Offer { β :: Boson,
              zElim :: ref,
              xIntr :: name,
              cont :: seq
            } -- FIXME: no boson
    | Demand { zElim :: ref,
               xIntr :: name,
               cont :: seq }

    -- | Save and load names to/from the intuitionistic context
    | Save { zElim :: ref, xSave :: name, cont :: seq }
    | Load { zLoad :: ref, loadSize :: sz, xIntr :: name, cont :: seq}

    -- | Recursion
    | Fold {  zElim :: ref, xIntr :: name, cont :: seq }
    | Unfold { zElim :: ref, xIntr :: name, cont :: seq }
    -- | Weakening
    | Ignore { β :: Boson, zElim :: ref, cont :: seq }

    -- | Renaming
    | Alias { β :: Boson, zElim :: ref, xIntr :: name, cont :: seq }
    
    | Mem { zElim :: ref
          ,memTy    :: ty
          ,xIntr :: name, contL :: seq
          ,yIntr :: name, contR :: seq
          ,count :: Int
          }

    | Tile name sz seq -- TODO: remove; it can be implemented using Foldmap
      {-
    | Foldmap {
      fmSz :: ref,
      fmTyM :: ty, -- TODO: this one could depend on a size variable, which should be given here.
      fmArr :: [ref], {-^ a list of arrays folded in parallel -}
      fmMapTrg :: name,
      fmMap :: seq, -- ^ mapper
      fmMixL, fmMixR, fmMixTrg :: name,
      fmMix :: seq, -- ^ combiner
      fmMon :: name,  -- ^ a better name for this is "fmResult"
      fmCont ::  seq -- ^ result
      }
-}
    | Foldmap { 
      fmSz :: ref,
      fmArr :: [ref], {-^ a list of arrays folded in parallel -}
      fmTyM :: ty, -- TODO: this one could depend on a size variable, which should be given here.
      fmMapTrg :: name,
      fmMap :: seq, -- ^ mapper
      fmMixL, fmMixR, fmMixTrg :: name,
      fmMix :: seq, -- ^ combiner
      fmMon :: name,  -- ^ a better name for this is "fmResult"
      fmCont ::  seq -- ^ result
      }

    | Foldmap2 { -- version with constant monoid type
      fmArr :: [ref], {-^ a list of arrays folded in parallel -}
      fmSize :: sz,
      fmTyM :: ty,
      fmUnitTrg :: name,
      fmUnit :: seq,
      fmMapTrg :: name,
      fmMap :: seq, -- ^ mapper
      fmMixL,
      fmMixR,
      fmMixTrg :: name,
      fmMix :: seq, -- ^ combiner
      fmMon :: name,  -- ^ a better name for this is "fmResult"
      fmCont :: seq -- ^ result
      }
      
    -- | Freeze, also known as sync. Can also allocate a fold
    | Sync { syncIntr   :: [(sz,ty, name, Maybe (SyncUpdate' sz name ref), Maybe (SyncRead' sz name ref))]
           , contL      :: seq
           , syncCont   :: Maybe seq
           , syncContR  :: Maybe seq
           }
    -- | Sequential loop
    | BigSeq { fence :: [SeqStep' seq sz name ref] } 

{-
let n = fmSz:

  ~fmMapTrg:M[0],fmArr:A ⊢ fmMap
  fmMixL:M[k], fmMixR:M[k], fmMixTrg:~M[k+1] ⊢ fmMix
  Γ,fmMon:M[n] ⊢ fmCont
-------------------------------------------------------------------------------------------------- foldmap
                          Γ,fmArr:⊗(2^n)A ⊢

-}

    | BigPar {zElim :: ref
             ,branches :: [(sz,Branch' seq name ref)] -- ^ must be non-empty
             }
    | BigTensor { zElim :: ref, xIntr::name, cont::seq}
    | Split { zElim :: ref, splitSz :: sz, xIntr, yIntr ::  name, cont :: seq}
    | Merge { zElim, wElim :: ref, splitSz :: sz, xIntr :: name, cont :: seq} -- should 'go through' all rules. here for meta reasoning.
    | Permute { zElim :: ref, newSz :: sz, xIntr :: name, cont :: seq}

      -- | TODO: remove.
    | NatRec name --  zero
             name --  induction hyp.
             name --  final result
             ref --  what we do recursion on; coming from the intuit. ctx.
             ref --  the only variable whose type can depend the eliminatied size
             seq --  base case
             seq --  induction

             -- General form: NatRec x0 xn y n z base ind
      -- | Alternatively, one could reify the size and do an integer operation
      --   on it. The purpose of an specific construct is that the compiler
      --   could use the result of the comparison on a specific branch to
      --   eliminate dead branches further up the tree.
      --
      --   Notice that internally, the compiler has no support for 
    | SizeCmp { sz1 :: sz,
                sz2 :: sz,
                bCmp :: [(CmpOp, seq)] }
    | IntOp {  
               intOp  :: IntOp,
               zElim :: ref,
               wElim :: ref,
               xIntr :: name,
               cont :: seq }
    | IntCmp {
              op :: CmpOp,
               zElim :: ref,
               wElim :: ref,
               xIntr :: name,
               cont :: seq }
    | IntCopy {
                zElim :: ref,
                xIntr :: name,
                yIntr :: name,
                cont :: seq }
    | IntIgnore { zElim :: ref,
                  cont  :: seq }
    | IntLit { xIntr  :: name,
               intLit :: Integer,
               cont   :: seq }

    -- the logical abomination:
    | Fix name ref ty seq

    deriving (Show, Read)
{-

   C : f : (! ~ A) * A |- s
   -------------------------------       <- note: linear context empty, exponentials OK
   C ; x : A |- let f = fix x in s

-}

data SyncUpdate' sz name ref = SyncUpdate {
  syncSteps :: sz
 ,syncWIntr :: name
 } deriving (Show,Read)

data SyncRead' sz name ref = SyncRead {
  syncCopies :: sz
 ,syncYIntr  :: name
 } deriving (Show,Read)


  

data Seqn ann ty sz tysz name ref = Seqn { _ann :: ann,
                                           _seq :: Seq'' (Seqn ann ty sz tysz name ref) ty sz tysz name ref } deriving (Show, Read)

newtype Seqn0 name ref = Seqn0 { seqn0 :: Seqn0' name ref  } deriving (Show) 


type Seqn0' name ref = Seqn (EmptyCtx  name ref)
                         (Type' name ref)
                         (Size ref)
                         (Sized' name ref)
                         name ref 

type IdSeqn0' = Seqn0' Id Id

newtype Seqn1 name ref = Seqn1 { seqn1 :: Seqn (LinearCtx name ref)
                                            (Type' name ref)
                                            (Size ref)
                                            (Sized' name ref)
                                            name ref } deriving (Show)

type Seqn2'' name ref = Seq'' (Seqn2' name ref) 
                              (Type' name ref)
                              (Size ref)
                              (Sized' name ref)
                              (name ::: Sized' name ref)
                              (ref  ::: Sized' name ref) 

type Seqn2' name ref = Seqn (TypedCtx name ref)
                            (Type' name ref)
                            (Size ref)
                            (Sized' name ref)
                            (name ::: Sized' name ref)
                            (ref ::: Sized' name ref)

newtype Seqn2 name ref = Seqn2 { seqn2 :: Seqn (TypedCtx  name ref) 
                                            (Type' name ref)
                                            (Size ref)
                                            (Sized' name ref)
                                            (name ::: Sized' name ref)
                                            (ref  ::: Sized' name ref) } deriving (Show)

type Seq' = Seqn0

data SeqCtxElem ty sz ref = ElemLinear ref
                          | ElemIntuit ty ty deriving (Show, Read, Eq)


-- | Patterns and functions to help with common wrapping and unwrapping
pattern S'  p = Seqn EmptyCtx p
pattern S   p = Seqn0 p 
pattern S0  p = Seqn0 (Seqn EmptyCtx p)

r0 x = seqn0 x
rM x = fmap r0 x

s0 x = fmap S0 x
s' x = fmap S' x
sM x = fmap S  x

data SeqStep' seq sz name ref =
    SeqStep {
      --  conditions   :: SizeProp ref
      seqSz    :: sz -- ^ Size of the chunk
     ,seqChunk :: [(ref,name)] -- ^ Affected seqs
     ,seqProg  :: seq -- ^ Sequent to iterate
    }
  deriving (Show,Read)

--deriving instance (SigP Show s, Show name, Show ref) => Show (SeqStep' s name ref)
-- deriving (Show, Functor, Traversable, T.Foldable)
type SeqStep name ref = SeqStep' (Seqn0' name ref) (Size ref) name ref
type SyncUpdate name ref = SyncUpdate' (Size ref) name ref
type SyncRead name ref = SyncRead' (Size ref) name ref

instance HasRef (Seq' name ref) where
  type Ref (Seq' name ref) = ref

instance HasRef (Seqn0' name ref) where
  type Ref (Seqn0' name ref) = ref

instance HasRef (SeqStep' seq sz name ref) where
  type Ref (SeqStep' seq sz name ref) = ref

instance HasRef (SyncUpdate' sz name ref) where
  type Ref (SyncUpdate' sz name ref) = ref

instance HasRef (SyncRead' sz name ref) where
  type Ref (SyncRead' sz name ref) = ref

instance (MonadFresh m) => SizeActionM m IdSeq where
  -- TODO: write specialized version if size is a number
  0  `resizeByM` t = return $ S0 (Halt [])
  1  `resizeByM` t = return t
  sz `resizeByM` (S t) = do
    [x,x',y] <- freshIds 3
    return $ fuse1 x y (bigTensor sz (T Bot))
                                     (S0 $ BigPar x [(sz,Branch x' $ S'$ SOne False x' $ t)])
                                     (S0 $ What (Builtin BIFSchedSeq) [ElemLinear y] [])

instance (MonadFresh m) => SizeActionM m (Seqn0' Id Id) where
  sz `resizeByM` t = rM $ sz `resizeByM` (Seqn0 t)

instance (MonadFresh m) => SizeActionM m IdSeqStep where
  sz `resizeByM` step = do
    case step of
      SeqStep {seqProg = seqProg',..} -> do
        seqProg <- sz `resizeByM` seqProg'
        return $ SeqStep {..}
    --  The interval will now consume `sz` times more of each type of sequence
    --  and array.
    --  It will also write `sz` times more state.

{-
instance (MonadFresh m) => SizeActionM m IdSyncUpdate where
  sz `resizeByM` SyncUpdate{syncCont = syncCont',..} = do
    syncCont <- sz `resizeByM` syncCont'
    return $ SyncUpdate{..} 

instance (MonadFresh m) => SizeActionM m IdSyncRead where
  sz `resizeByM` SyncRead{syncContR = syncContR',..} = do
    syncContR <- sz `resizeByM` syncContR'
    return $ SyncRead{..} 
-}

swapCut (NCut ci side' sz' cv s t) = NCut ci (dual side') sz' [(y,x,dual tt') | (x,y,tt') <- cv] t s
fuseNr,cutNl,cutNr  :: Size ref -> name -> name -> Type' name ref ->  Seq' name ref -> Seq' name ref ->Seq' name ref
cut1ds,cut1 :: name -> name -> Type' name ref ->  Seq' name ref -> Seq' name ref ->Seq' name ref
fuseNr sz x y ty (S contL) (S contR) = S0$ NCut Fuse FanRight sz [(x,y,ty)] contL contR 
cutNr sz x y ty  (S contL) (S contR) = S0$ NCut Manual FanRight sz [(x,y,ty)] contL contR 
cutNl sz x y ty  (S contL) (S contR) = S0$ NCut Manual FanLeft  sz [(x,y,ty)] contL contR
fuseNl sz x y ty (S contL) (S contR) = S0$ NCut Fuse FanLeft  sz [(x,y,ty)] contL contR
cut1 = cutNr sizeOne
fuse1 = fuseNr sizeOne
cut1ds x y ty (S contL) (S contR) = S0$ NCut Desugar FanLeft sizeOne [(x,y,ty)] contL contR


mix (S a) (S b) = S0$ NCut Manual FanLeft sizeOne [] a b
halt = S0$ Halt []

halts = S0 . Halt

instance Semigroup (Seq' name ref) where
  (S0 (Halt [])) <> a  = a
  a <> (S0 (Halt []))  = a
  a <> b          = a `mix` b

instance Monoid (Seq' name ref) where
  mempty = halt
  mappend = (<>)
  mconcat [] = halt
  mconcat ss = T.foldl1 mix ss

$(T.concat <$> T.mapM makeLenses [''Deriv'', ''Seqn])
$(T.concat <$> T.mapM makeWrapped [''Deriv0, ''Deriv1, ''Deriv2,
                                   ''Seqn0, ''Seqn1, ''Seqn2])

seqStepSz :: SeqStep' seq sz name ref -> Maybe sz
seqStepSz t = case t of
  SeqStep{..} -> Just seqSz

seqStepSz0 :: SeqStep' a (Size ref) b c -> Size ref
seqStepSz0 = maybe sizeZero id . seqStepSz

seqStepRefs :: SeqStep' seq sz name ref -> [ref]
seqStepRefs t = case t of
  SeqStep{..}   -> map fst seqChunk 

seqStepNames :: SeqStep' seq sz name ref -> [name]
seqStepNames t = case t of
  SeqStep{..} -> map snd seqChunk 

seqStepSeq :: SeqStep' seq sz name ref -> Maybe seq
seqStepSeq t = case t of
  SeqStep{..} -> Just seqProg

cutInfo :: Functor f => (CutInfo -> f CutInfo) -> (Seq'' seq ty sz tysz name ref -> f (Seq'' seq ty sz tysz name ref))
cutInfo f NCut{ncut_info = ncut_info',..} = (\ncut_info -> NCut{..}) `fmap` f ncut_info'
  
-- | References to sequences without duplicates in all the steps
bigSeqRefs :: (Eq ref) => [SeqStep' seq sz name ref] -> [ref]
bigSeqRefs = nub . (>>= seqStepRefs)  

resizeSyncIntrBy :: (Num sz) => sz ->
                    (sz, ty, name, Maybe (SyncUpdate' sz name ref), Maybe (SyncRead' sz name ref)) ->
                    (sz, ty, name, Maybe (SyncUpdate' sz name ref), Maybe (SyncRead' sz name ref))  
resizeSyncIntrBy sz (syncSz, ty, xIntr, upd, rd) = (sz * syncSz, ty, xIntr, upd, rd)

