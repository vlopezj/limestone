{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DoAndIfThenElse #-}
module LL.Semantics.Static where

import LL
import Type
import Id
import LL.Semantics.Common hiding (Halt, Inl, Inr)
import Ritchie.Polar.Type hiding (SizedP)
import Control.Monad.Reader
import Fresh
import Control.Applicative
import Ritchie.Polar hiding (SizedP)
import Data.List
import Ritchie.Term (IdENameP, IdERefP)
import MarXup.Tex
import MarXup.Latex
import MarXup.Latex.Math
import MarXup.Math
import TexPretty
import Data.Monoid
import Data.Bifunctor
import Data.Traversable as T
import Data.Maybe
import Data.Default
import ContM

writeKey = (LC  "write" :@)
readKey = (LC "read" :@)
allocate :: IdLambdaF → IdLambdaF
allocate = (LC "allocate" :@)
schedule = (LC "schedule" :@)

-- | An important rule is that each recursive call has an even number of
--   SFN wrappers on top. That is, the translation is covariant in the type?
--
--   Also, this representation makes implementing axiom and cut trivial.
--
--   Decide whether to use less rich types
--
translateType' :: (Type' n r → SF n r) → Type' n r → Maybe (SF n r)
translateType' r (T t) = go t
  where
  go t = case t of

    Primitive True name   → j$ SFNN (SFAtom name)
    Primitive False name  → j$ SFN  (SFAtom name)

    Exists _ (T TSize) t  → j$ SFNN (SFNat :× r t)
    Forall _ (T TSize) t  → j$ SFN  (SFNat :× r t)

    Exists _ (T (TProp _)) t  → j$ r t
    Forall _ (T (TProp _)) t  → j$ r t

    t :*: u               → j$ SFNN (r t :× r u)
    t :|: u               → j$ SFN  (r (dual t) :× r (dual u))

    t :+: u               → j$ SFNN (r t :+ r u)
    t :&: u               → j$ SFN  (r (dual t) :+ r (dual u))

    One                   → j$ SFEff :→ SFEff
    Bot                   → j$ SFEff

    Zero                  → j$ SFNN (SFEmpty)
    Top                   → j$ SFN  (SFEmpty)

    ArrayTensor _ t       → j$ SFNN (SFNat :→ r t)
    ArrayPar    _ t       → j$ SFN  (SFNat :→ r (dual t))

    ArraySeqPos _ t       → j$ SFNN (SFNat :→ r t)
    ArraySeqNeg _ t       → j$ SFN  (SFNat :→ r (dual t))

    t                     → Nothing 

  j = Just

-- | Data types are effect-free
translateData' :: (Type' n r → Maybe (SF n r)) → Type' n r → Maybe (SF n r)
translateData' r (T t) = go t
  where
  go t = case t of

    Primitive True name   → j$ SFAtom name
    Exists _ (T TSize) t  → (SFNat :×) <$> r t

    t :*: u               → (:×) <$> r t <*> r u
    t :|: u               → (:×) <$> r t <*> r u

    t :+: u               → (:+) <$> r t <*> r u

    One                   → j$ SFUnit
    Bot                   → j$ SFUnit
    Zero                  → j$ SFVoid

    ArrayTensor sz t       → SFVec sz <$> r t
    ArrayPar    sz t       → SFVec sz <$> r t

    ArraySeqPos sz t       → SFVec sz <$> r t
    ArraySeqNeg sz t       → SFVec sz <$> r t

    _                     → Nothing 

  j = Just



translateData :: Type' n r → Maybe (SF n r) 
translateData = translateData' translateData 

translateType :: Type' n r → SF n r
translateType t = maybe (SFType t) id (translateType' translateType t)

translateSized :: Sized' n r → SF n r
translateSized (t :^ sz) = (SFNat :× (SFNat :→ translateType t))

translateSizedPol :: SizedP n r → SF n r
translateSizedPol (SizedP tsz _) = translateSized tsz

-- | Represents indexing into the binding.
--   TODO: Passing in the position in the environment
iota :: Id → LambdaF Id
iota (x) = (:@) (LConst Iota) (LVar x)

-- | Return for CSP, when the polarity is positive
--   (λ x. (λ κ. κ x))
shift :: SPol s → LambdaF a → LambdaF a
shift SPos x = x :∘ LShift
shift SNeg x = x

data TranslateSeqn = TranslateSeqn{
  showShifts   :: Bool
 ,showIndexing :: Bool
 }

instance Default TranslateSeqn where
  def = TranslateSeqn{showIndexing = True
                     ,showShifts = True
                     }

translateSeqn :: TranslateSeqn → IdSeqnPol → LambdaF Id
translateSeqn opts@TranslateSeqn{..} s@(SeqnPol _derivSequent) = runFreshMFrom s $ go _derivSequent
  where

  idx1 :: IdLambdaF → IdSize →  (IdLambdaF → FreshM IdLambdaF) → FreshM IdLambdaF
  idx1 x i cont =
    if showIndexing then do
        offset <- freshFrom "δ"
        array  <- freshFrom "μ"
        letTup2 (offset, array) x <$> cont (LVar array :@ (LSize (var offset + i)))  
    else
      case i of
        0 → cont x 
        _ → cont (x :@ LSize i)
             

  shifted :: SBool b → Id → FreshM (IdLambdaF) → FreshM (IdLambdaF)
  shifted SFalse x a = lam x <$> a
  shifted STrue  x a = do
    r <- a
    if showShifts then do
        κ   <- freshFrom "κ"
        u   <- freshFrom ignoreName
        x'  <- refreshId x
        if showIndexing then
          return$ lam x'$ let1 x (LTup (LSize 0) (lam u (lam κ (LVar κ :@ LVar x')))) r
        else
          return$ lam x'$ let1 x (lam κ (LVar κ :@ LVar x')) r
    else return$ lam x r

  uidx :: Id → FreshM IdLambdaF → (Id → IdLambdaF → FreshM a) → FreshM a
  uidx x l f | not showIndexing = l >>= f x
  uidx x l f = do
    l' <- l
    x' <- refreshId x 
    u  <- freshFrom ignoreName 
    f x' (let1 x (LTup (LSize 0) (lam u (LVar x'))) l')

  demote :: IdSize  -- index
          → [Id ::: IdSized] → FreshM IdLambdaF → FreshM IdLambdaF
  demote _  _ cont | not showIndexing = cont
  demote _ [] cont = cont
  demote 0 _  cont = cont
  demote i ((x ::: _ :^ sz):xs) cont = do
    offset <- freshFrom "δ"
    array  <- freshFrom "μ"
    (\b -> letTup2 (offset, array) (LVar x) $
           ((lam x b) :@ LTup (LSize$ var offset + i * sz) (LVar array))) <$>
           demote i xs cont 
       
  go :: SeqnPol' Id Id → FreshM (LambdaF Id)
  go s'@(Seqn ann s) = case s of
        Ax {wElim = SAr (sIsPos → STrue) (wElim' ::: _), zElim = SAr _ (zElim' ::: _)} →
           idx1 (LVar wElim') 0 $ \wElim'' -> 
           idx1 (LVar zElim') 0 $ \zElim'' -> 
           return$ LApp wElim'' zElim''

        Ax {wElim, zElim} → go$ Seqn ann$ Ax{wElim = zElim, zElim = wElim}

        Cross {zElim = SAr SPos (zElim ::: _), xIntr = SAn _ (xIntr ::: _), yIntr = SAn _ (yIntr ::: _),..} ->
          idx1 (LVar zElim) 0 $ \v → (v :@) <$> (  
              uidx xIntr (go cont) $ \xIntr' c' →
                uidx yIntr  (pure c')  $
                    \yIntr' c'' → return$ lamTup2 (xIntr', yIntr') c'')
    
        Halt _ → pure LNop

        SBot { zElim = SAr SNeg (zElim ::: _) } → idx1 (LVar zElim) 0 $ \zElim' -> pure zElim'
        SOne { zElim = SAr SPos (zElim ::: _), cont } → idx1 (LVar zElim) 0 $ \v -> (v :@) <$> go cont
        SZero { zElim = SAr SPos (zElim ::: _) } → 
          idx1 (LVar zElim) 0 $ \v → return v 

        NCut _ _ 1 [] a b → (:>>) <$> go a <*> go b 

        NCut _ _ 1 [(SAn (sIsPos → SFalse) (xIntr ::: _),SAn _ (yIntr ::: _),_)] a@(Seqn SeqCtx{_linear} _)  b → do
            y <- uidx xIntr (go a) ((pure.).lam)
            let1 yIntr (LTup (LSize 0) y) <$> go b


        NCut i fan 1 [(x,y,ty)] a b → go$ Seqn ann$ NCut i fan 1 [(y,x,dual ty)] b a 

        NCut i FanRight sz [(x,y,ty)] a b → go$ Seqn ann$ NCut i FanLeft sz [(y,x,dual ty)] b a 

        NCut _ FanLeft sz [(SAn (sIsPos → p) (xIntr ::: _),SAn _ (yIntr ::: _),_)] a@(Seqn SeqCtx{_linear} _)  b → do
            (i,isz) <- case sz of
                   1 → (,) <$> freshFrom ignoreName <*> pure 0
                   _ → ((,) <$> id <*> var) <$> freshFrom "i"
            s <- shifted p xIntr (demote isz [v | v@(vIntr ::: _) <- _linear, vIntr /= xIntr] (go a))
            let1 yIntr (LTup (LSize 0) (lam i s)) <$> go b

        Par { xIntr = SAn (sIsPos -> px) (xIntr ::: _)
            , yIntr = SAn (sIsPos -> py) (yIntr ::: _)
            , zElim = SAr SNeg (zElim ::: _)
            , ..} ->

          join$ liftM2 (\l r → idx1 (LVar zElim) 0 $ \v → return$ v :@ LTup l r)
                  (shifted px xIntr (go contL)) (shifted py yIntr (go contR)) 
                   
        Plus { zElim = SAr SPos (zElim ::: _)
             , inl = Branch (SAn _ (xIntr ::: _)) contL
             , inr = Branch (SAn _ (yIntr ::: _)) contR
             } ->
            uidx xIntr (go contL) $ \xIntr' l ->
            uidx yIntr (go contR) $ \yIntr' r ->
            idx1 (LVar zElim) 0 $ \v → return$ v :@ lamCase (xIntr', l) (yIntr', r)

        With { zElim = SAr SNeg (zElim ::: _)  
             , xIntr = SAn (sIsPos -> px)   (xIntr ::: _)
             , .. } ->
          idx1 (LVar zElim) 0 $ \v ->
          (\c → 
            v :@ 
            ((case choose of
                Inl → LInl
                Inr → LInr) :@ c)) <$> (shifted px xIntr (go cont))

        BigTensor { zElim = SAr SPos (zElim ::: _)
                  , xIntr = SAn _    (xIntr ::: _)
                  , .. } →
          idx1 (LVar zElim) 0 $ \v ->
          (\c → v :@ lam xIntr c) <$> go cont

        BigPar { zElim    = SAr SNeg (zElim ::: _)
               , branches 
               , .. } →
          do
            i <- freshFrom "i"
            let isz = var i
            let gob n = \case
                            []  → return ([],Nothing)
                            (sz,Branch (SAn (sIsPos -> p) (xIntr ::: _)) cont@(Seqn SeqCtx{_linear} _)):rest → do
                                    let n' = n + sz
                                    c'  <- demote (isz - n) [v | v@(vIntr ::: _) <- _linear, vIntr /= xIntr] (go cont)
                                    c'' <- shifted p xIntr (pure$ lam xIntr c')
                                    case rest of
                                        [] -> return ([], Just c'')
                                        bs -> do
                                                (bs',e') <- gob n' bs
                                                return (((LSize n \≤ LVar i) \∧ (LVar i \< LSize n'), 
                                                            c''):bs',
                                                            e')


            f <- gob 0 branches
            idx1 (LVar zElim) 0 $ \v ->
              return$ v :@ (LTup 
                (LSize (sum [sz | (sz,_) <- branches])) (lam i $ uncurry LIf f))


        Split { zElim = SAr _ (zElim ::: _ :^+ n)
              , xIntr = SAn _ (xIntr ::: _)
              , yIntr = SAn _ (yIntr ::: _)
              , splitSz
              , cont } →
          do
             n' <- freshFrom "n"
             z' <- refreshId zElim
             x' <- refreshId xIntr
             y' <- refreshId yIntr
             i  <- freshFrom "i"
             
             go cont >>= \c → pure$ 
               letTup2 (n', z') (LVar zElim) (
               let1    xIntr (LTup (LSize splitSz) (LVar z')) (
               let1    yIntr (LTup (LSize (n - splitSz)) (LVar z')) (
               c)))

        BigSeq [SeqStep {seqProg = seqProg@(Seqn SeqCtx{_linear} _),..}]  → do
            i <- freshFrom "i"
            let isz = var i
            let refs_pos = [(zElim, xIntr) | (SAr SApoP (zElim ::: _), SAn _ (xIntr ::: _)) ← seqChunk]
            let refs_neg = [(wElim, yIntr) | (SAr SApoN (wElim ::: _), yIntr) ← seqChunk] :: [(Id, SAName Id Id)] 
            recurse (\(zElim, xIntr) κ → idx1 (LVar zElim) 0 $ \v → κ (v,xIntr)) refs_pos $ \refs_pos' ->
                let pos_loop =  
                          foldr (\(zElim, xIntr) a → 
                            ((zElim :@ LVar i) :@) <$> (
                               uidx xIntr a ((pure.) . lam))) (demote isz [v | v@(vIntr ::: _) <- _linear, not$ vIntr `elem` [n | (_,SAn _ (n ::: _)) <- seqChunk]]
                               $ go seqProg) refs_pos' 

                in case refs_neg of
                    [(wElim,SAn (sIsPos -> p) (yIntr ::: _))] → 
                        idx1 (LVar wElim) 0 $ \v ->
                          (v :@) . lam i <$> shifted p yIntr pos_loop
                    [] → schedule . lam i <$> pos_loop

         -- Business as usual for the positive connectives
          -- Use the state translation for the negative ones
        Sync { syncIntr = [(sz, ty, SAn _ (xIntr ::: _), mu, mr)]
             , contL , syncCont, syncContR } →


          let rest :: (IdSize → FreshM IdLambdaF) → FreshM IdLambdaF
              rest d = do 
                    let b :: (IdLambdaF → IdLambdaF) → FreshM IdLambdaF
                        b f = case sz of
                          (sizeIsInteger → Just 1) → f <$> (d 0)
                          sz → do
                                 i ← freshFrom "i"
                                 d' ← d (var i)
                                 return$ LTup (LSize sz) (lam i (f d'))
                     
                    uc ← freshFrom "u"
                    rc ← freshFrom "r"
                    wc ← freshFrom "w"
                    uwc ← freshFrom "w"
                    unc ← freshFrom "o"
                    i_ ← freshFrom "i"
                    
                    
                    contL' ← go contL
                    syncCont' ← traverse go syncCont
                    syncContR' ← traverse go syncContR
                     

                    let w = do
                        bw ← b writeKey
                        return$ (lam xIntr contL') :@ bw

                    let u = case mu of
                              Nothing → return
                              Just (SyncUpdate{syncWIntr = SAn _ (wIntr ::: _), ..}) → (\prev → do
                                  bu ← b (\k →
                                      LTup (LSize syncSteps) (lam i_ ( 
                                          lam uc (
                                          readKey k :@ (lam rc (
                                          LVar uc :@ 
                                          LTup (LVar rc) (lam uc (lcase (LVar uc) (uwc, writeKey k) (unc, (LVar unc))))))))))

                                  return$ prev `andThenNL` ((lam wIntr (fromJust syncCont')) :@ bu))

                    let r = case mr of
                              Nothing → return
                              Just (SyncRead{syncYIntr = SAn _ (yIntr ::: _), ..}) → (\prev → do
                                br ← b readKey
                                return $ prev `andThenNL` ((lam yIntr (fromJust syncContR')) :@ br))

                    w >>= u >>= r
            
            in
               case sz of
                (sizeIsInteger → Just 1) → do
                    d ← freshFrom "d"
                    rest' ← rest (const$ return (LVar d))
                    return$ allocate (lam d (rest'))

                sz → do
                    d1  ← freshFromTeX (math ("d" <> (subscript (texSize 0 0))))
                    dsz ← freshFromTeX (math ("d" <> (subscript (texSize 0 (sz - 1)))))
                    rest' ← rest (\i → do
                                           a ← freshFromTeX$ math ("d" <> (subscript (texSize 0 i)))
                                           return$ LVar a)
                    return (allocate (lam d1  (
                            LElided :@ ( 
                            allocate (lam dsz (
                            rest'
                            ))))))

        TUnpack{zElim = SAr _ (zElim ::: _)
               ,xIntr = SAn _ (xIntr ::: _)
               ,tyIntr = SAn _ (tyIntr ::: _)
               ,..} → 
               
              idx1 (LVar zElim) 0 $ \v → 
              (v :@) <$> (uidx xIntr (go cont) $ \xIntr' c' → pure$ lamTup2 (tyIntr, xIntr') c')
        
        TApp{zElim = SAr _ (zElim ::: _)
            ,xIntr = SAn (sIsPos -> px) (xIntr ::: _)
            ,ty = TyA (TyPol _ ty)
            , ..} → do
           tyV <- freshFromTeX (texType 0 ty)
           idx1 (LVar zElim) 0 $ \v → 
              (\c -> v :@ LTup (LVar tyV) c) <$> (shifted px xIntr (go cont))

        Merge{zElim   = SAr _ (zElim ::: _) 
             ,wElim   = SAr _ (wElim ::: _)
             ,splitSz = sz
             ,xIntr   = SAn _ (xIntr ::: _)
             ,cont
             } → do
           let1 xIntr (mergeT (LVar wElim) (LVar zElim)) <$> go cont

        SizeCmp{sz1, sz2, bCmp} -> 
            LIf <$> T.sequence
                [ (lCmpOp op (LSize sz1) (LSize sz2),) <$> go cont 
                | (op, cont) <- init bCmp
                ] <*> (let (_,cont) = last bCmp in Just <$> go cont)

        What{ what, zVars, disp = _ } -> pure$ LTerm (whatString what) [(id_name v, LVar v) | ElemLinear (SAr _ (v ::: _)) <- zVars]

        _ → return (LProg (erase (SeqnPol s')))

mergeT x y = ("merge" :@ x) :@ y

freshFromTeX :: (MonadFresh m) => TeX → m Id
freshFromTeX a = freshFrom ("???" ++ renderSimple Plain a)
