{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}
module LL.Semantics.Common.Core where

import Op (CmpOp, mathCmpOp)
import Type
import Id
import LL(IdSeqn2,Seqn2,Seqn0)

import MarXup
import MarXup.Tex
import qualified MarXup.PrettyPrint as PP
import MarXup.Latex
import MarXup.Latex.Math
import Fresh

import Data.Generics.Genifunctors

import Data.List
import Data.Monoid
import Data.String
import Data.Default
import Data.Bitraversable
import Data.Foldable as F
import Data.Traversable as T
import Control.Applicative
import Prelude.Extras

import Control.Monad.State

import Bound
import Bound.Scope

import Data.Number.Fin.Integer
import Data.Char
import TexPretty hiding (keyword, indent, prn)
import qualified TexPretty as TP
import LL.Semantics.Common.Layout

data SFPrim where
  Nat    :: SFPrim
  Eff    :: SFPrim
  Empty  :: SFPrim
  PUnit  :: SFPrim
  PVoid  :: SFPrim
  Atom   :: String → SFPrim

-- Lower precedence bind tighter
data SF n r where
  SFPrim   :: SFPrim → SF n r            -- 7
  SFVar    :: r → SF n r                 -- 7
  SFTypeM  :: Maybe PolVar → Type' n r → SF n r -- 7
  SFApp'   :: Bool → SF n r → SF n r → SF n r   -- 6
  Tuple    :: [SF n r] → SF n r          -- 5
  (:+)     :: SF n r → SF n r → SF n r   -- 4
  (:∪)     :: SF n r → SF n r → SF n r   -- 3
  (:→)     :: SF n r → SF n r → SF n r   -- 2 right associative
  SFForAll :: n → SF n r → SF n r → SF n r        -- 1
  SFVec    :: Size r → SF n r → SF n r


pattern LConst x = LCon x []

pattern LIota  = LConst Iota
pattern LIotaB = LConst IotaB
pattern LShift = LConst Shift
pattern LCStr  x = LConst (CStr x)
pattern LCompose x y = LCon Compose [x,y]
pattern LConcat = LConst Concat 
pattern LNop = LConst Nop 
pattern LAndThen x y = LCon AndThen [x,y]
pattern LUndefined = LConst (CStr "undefined")
pattern LElided = LConst (CStrI (0,"…"))
pattern SFApp x y = SFApp' False x y
pattern SFAppS x y = SFApp' True x y

instance IsString (LambdaF a) where
  fromString s = LCon (fromString s) []

data Con where
  App :: Con
  LetApp :: Con
  Tup   :: Con
  Inl   :: Con
  Inr   :: Con
  Iota  :: Con
  IotaB :: Con
  Nop  :: Con
  Shift :: Con
  Compose :: Con
  Concat :: Con
  AndThen :: Con
  Undefined :: Con
  CStr      :: String → Con 
  CStrI     :: (Int,  String) → Con
  Break     :: Con

deriving instance Show Con

instance IsString Con where
  fromString = CStr
 
data LambdaF a where
  LVar     :: a → LambdaF a
  LCon     :: Con → [LambdaF a] → LambdaF a
  Lam      :: Name → (Scope () LambdaF a) → LambdaF a
  LamTup2  :: (Name,Name) → (Scope Int LambdaF a) → LambdaF a
  LamCase  :: Name → (Scope () LambdaF a) → Name → (Scope () LambdaF a) → LambdaF a 
  LIf      :: [(LambdaF a, LambdaF a)] → Maybe (LambdaF a) → LambdaF a
  LSize    :: Size Id → LambdaF a
  LProg    :: Seqn2 Id Id → LambdaF a
  LTerm    :: Name → [(Name, LambdaF a)] → LambdaF a

lCmpOp :: CmpOp -> LambdaF a -> LambdaF a -> LambdaF a
lCmpOp op = Infix (5, mathCmpOp op)

deriving instance (Show a) => Show (LambdaF a)
instance Show1 LambdaF

type IdLambdaF = LambdaF Id

(\≤), (\∧), (\<)  :: LambdaF a → LambdaF a → LambdaF a
[(\≤), (\∧), (\<)] =  map Infix [(5,"≤"),(4,"∧"),(5,"<")]

pattern LApp x y = LCon App [x,y]
pattern LLetApp x y = LCon LetApp [x,y]
pattern Infix s x y = LCon (CStrI s) [x,y]
pattern x :∘ y = LCon Compose [x,y]
pattern x :@ y = LApp x y
pattern x :>> y = Infix (6,"≫") x y
pattern LInl = LCon Inl []
pattern LInr = LCon Inr []
pattern LTup x y = LCon Tup [x,y]
pattern LCase x lv l rv r = LLetApp (LamCase lv l rv r) x
pattern LetTup2 xy v f = LLetApp (LamTup2 xy f) v
pattern Let1 x v f = LLetApp (Lam x f) v

pattern LC x = LCon x []

deriving instance Functor LambdaF 
deriving instance Foldable LambdaF 
deriving instance Traversable LambdaF 
instance Applicative LambdaF where pure = return; (<*>) = ap
instance Monad LambdaF where
  return = LVar

  LVar a >>= f = f a 
  LCon c bs >>= f = LCon c (map (>>= f) bs)
  Lam n e >>= f = Lam n (e >>>= f)
  LamTup2 n a >>= f = LamTup2 n (a >>>= f)
  LamCase a f b g >>= h = LamCase a (f >>>= h) b (g >>>= h)
  LIf xs x >>= f = LIf [(x >>= f, y >>=f) | (x,y) <- xs] (fmap (>>= f) x)
  LSize sz >>= _ = LSize sz
  LProg p  >>= _ = LProg p
  LTerm name vars >>= f = LTerm name [ (var, subst >>= f) | (var, subst) <- vars ]
                                                     

lterm :: Name → [Id] → LambdaF Id
lterm name vars = LTerm name [(id_name a,LVar a) | a <- vars]

lam :: Id → LambdaF Id → LambdaF Id
lam v b = Lam (id_name v) (abstract1 v b)

lamCase :: (Id, LambdaF Id) → (Id, LambdaF Id) → LambdaF Id
lamCase (l, lc) (r, rc)  = LamCase (id_name l) (abstract1 l lc) (id_name r) (abstract1 r rc)

lamTup2 :: (Id, Id) → LambdaF Id → LambdaF Id
lamTup2 (x, y) b = LamTup2 (id_name x, id_name y) (abstract (`elemIndex` [x,y]) b)


lcase :: LambdaF Id → (Id, LambdaF Id) → (Id, LambdaF Id) → LambdaF Id
lcase x (l, lc) (r, rc)  = lamCase (l,lc) (r,rc) `LLetApp` x
let1 x v f = (lam x f) `LLetApp` v
letTup2 xy v f = (lamTup2 xy f) `LLetApp` v

pattern LBreak x = LCon Break [x]

-- deriving instance Functor (Lambda')
type IdSF = SF Id Id
type SF' = SF Name Name

pattern SFType x = SFTypeM Nothing x
pattern SFTypePol p x = SFTypeM (Just p) x
pattern SFTypePlus x = SFTypePol (PolConst True) x
pattern SFTypeMinus x = SFTypePol (PolConst False) x

pattern SFNat = SFPrim Nat
pattern SFEff = SFPrim Eff
pattern SFUnit = SFPrim PUnit
pattern SFVoid = SFPrim PVoid
pattern SFEmpty = SFPrim Empty
pattern SFAtom x = SFPrim (Atom x)
pattern a :× b = Tuple [a,b]

pattern SFN  x =  (x :→ SFEff)
pattern SFNN x = SFN (SFN x)

texSF :: Int → IdSF → TeX
texSF p s = case s of
  s :→ t        → prn p 2 $ texSF 3 s <> "→" <> texSF 2 t
  s `SFApp` t   → prn p 6 $ texSF 3 s <> " " <> texSF 2 t
  s `SFAppS` t  → prn p 6 $ texSF 3 s <> (subscript$ texSF 2 t)
  SFPrim a      → prn p 7 $ texPrim a
  SFVec sz a    → prn p 1 $ texSF 5 a <> " × … " <> texSize 0 sz <> " times"
  SFVar r       → prn p 7 $ texVar r
  SFType t      → braces (texType 5 t) <> superscript "•"
  SFTypePol p t → braces (texType 5 t) <> superscript (texPolVar p)
  Tuple  t      → prn p 5 $ mconcat (intersperse "×" (map (texSF 5) t))
  s :+ t        → prn p 4 $ texSF 4 s <> "+" <> texSF 4 t
  s :∪ t        → prn p 3 $ texSF 3 s <> "∪" <> texSF 3 t



texPrim :: SFPrim → TeX
texPrim Nat = "ℕ"
texPrim Eff = "⫫" 
texPrim Empty = "∅"
texPrim PUnit  = "𝟙"
texPrim (Atom s) = fromString s

data SizedP n r = SizedP (Sized' n r) (PolVar' r)

traverseNames :: forall m a. (Applicative m, Monad m) => (Name → m Name) → (LambdaF a → m (LambdaF a))
traverseNames fname x = go x
  where
  go :: forall a. LambdaF a → m (LambdaF a)
  go x@(LVar _)   = pure x
  go (LCon c xs) = LCon c <$> traverse go xs
  go (Lam n f)  = Lam <$> fname n <*> transverseScope go f
  go (LamTup2 (n₁,n₂) f) = LamTup2 <$> ((,) <$> fname n₁ <*> fname n₂) <*> transverseScope go f
  go (LTup x y) = LTup <$> go x <*> go y
  go (LamCase z₁ f₁ z₂ f₂) = LamCase <$> fname z₁ <*> transverseScope go f₁ <*> fname z₂ <*> transverseScope go f₂
  go (LIf bs b) = LIf <$> traverse (\(x,y) → (,) <$> go x <*> go y) bs <*> traverse go b 
  go (LSize sz) = pure$ LSize sz
  go x@(LProg _)   = pure x
  go x@(LTerm a b)   = LTerm a <$> traverse (\(name, l) -> (name,) <$> go l) b


texName :: Layout a → Name → a
texName Layout{..} = text . ensureMath . texVarStr

texLambda :: Double → LambdaF Id → TeX
texLambda w l = do
  d <- texLambdaSimpl layout2D l
  prettyCust w d

texLambdaOneLine :: LambdaF Id → TeX
texLambdaOneLine = texLambdaSimpl layout1D

texLambdaSimpl :: forall a. Layout a → LambdaF Id → a
texLambdaSimpl lo@Layout{..} l = go 0 $ fmap (texName lo . id_name) $ flip evalState def $ traverseNames disambiguateName_ l 
  where
    go :: Int → LambdaF a → a 

    go p (LVar v) = v

    go p (LetTup2 (x,y) v f) = prn p 7 $
      hang (keyword "let" <+> texName lo x <> "," <+> texName lo y <+> "=" <+> go 9 v) <+>
            keyword "in" </> (go 7 $ instantiate ([LVar (texName lo x), LVar (texName lo y)] !!) f)

    go p (Let1 x v f) = prn p 7 $ 
      hang (keyword "let" <+> texName lo x <+> "=" <+> go 9 v) <+> keyword "in" </>
              (go 7 $ instantiate (\() → LVar (texName lo x)) f)

    go p (x :@ y) = prn p 7$ go 7 x <+> go 8 y

    go p (LIf [] (Just b)) = go p b

    go p (LIf [] (Nothing)) = mempty

    go p (LIf ((c,a):as) e)  = 
      F.foldl1 (</>) $
        [hang (keyword "if" <+> go 0 c <+> keyword "then" <+> go 0 a)] ++
        [hang (keyword "else if" <+> go 0 c <> keyword "then" <+> go 0 a) | (c,a) <- as] ++
        [hang (keyword "else" <+> go 0 a) | Just a <- [e]]


    go p (Infix (q,c) x y) = prn p q $ go q x <+> texName lo c </> go q y
    go p (Lam n f) = prn p 2 $ ("λ" <> texName lo n <> "." </> go 2 (instantiate (\() → LVar (texName lo n)) f))
    go p (LamTup2 (x,y) f) = prn p 2 $ "λ(" <> texName lo x <> "," <> texName lo y <> ") ↦" <> go 2 (instantiate ([LVar (texName lo x), LVar (texName lo y)] !!) f)
    go p (LIota) = "ι"
    go p (LamCase x f y g) = keyword "λ" <> "("  <>
                             keyword "inl" <+> texName lo x <> " ↦ "
                               <+> go 0 (instantiate (\() → LVar (texName lo x)) f) </> " | " <>
                             keyword "inr" <+> texName lo y <> " ↦ "
                               <+> go 0 (instantiate (\() → LVar (texName lo y)) g) <+> ")" 
    go p (LCase z x f y g) = hang (keyword "case" <+> go 0 z <+> keyword "of") </> 
                               indent (hang (keyword "inl" <+> texName lo x <> " ↦ "
                                             <+> go 0 (instantiate (\() → LVar (texName lo x)) f))) </> " | " <>
                               indent (hang (keyword "inr" <+> texName lo y <> " ↦ "
                                             <+> go 0 (instantiate (\() → LVar (texName lo y)) g))) 
    go p (LTup x y) = prn p 6$ go 0 x <> "," <+> go 0 y

--    go p (LCStr s) = texName lo s
    go p (LSize sz) = text$ ensureMath$ texSize p sz
    go p (LProg s) = programLo s <> text (ensureMath$ superscript "•")
    go p (LCompose x y) = prn p 4$ go 4 x <+> "∘" <+> go 4 y
    go p (LCon c xs) = F.foldl1 (<+>) (text (texCon c):map (go 9) xs)

    go p (LTerm name vars) = texName lo name <> "[" <> mconcat (intersperse ","
                                                             [ {- case l of
                                                                 LVar v' | normalizeTeX v == normalizeTeX v' -> text v
                                                                 _ -> text v <> "⧸" <> go 0 l
                                                                -} go 0 l
                                                             | (texVarStr -> v,l) <- vars]) <> "]"


normalizeTeX :: TeX -> String
normalizeTeX = (>>= (\case
                        s | isSpace s -> ""
                        c -> [c]
                     )) . renderSimple Plain

    
cmdN :: String -> [TeX] -> TeX
cmdN a [] = cmd0 a
cmdN a [x] = cmd a x
cmdN a (x:xs) = cmd a x <> mconcat (map braces xs)

cmd2 a x y = cmdN a [x,y]

sfrac :: TeX -> TeX -> TeX
sfrac = cmd2 "sfrac"

andThen = (:>>)
x `andThenNL` y = x :>> y

instance Element (IdLambdaF) where
  type Target (IdLambdaF) = TeX
  element = ensureMath . texLambdaOneLine

texCon :: Con → TeX
texCon Shift = TP.keyword "shift"
texCon Iota = "ι"
texCon Inl = TP.keyword "inl"
texCon Inr = TP.keyword "inr"
texCon Nop = TP.keyword "nop"
texCon Concat = TP.keyword "concat"
texCon (CStr s) = ensureMath (mathrm (fromString s))
texCon (CStrI (_,s)) = textual s
texCon d = TP.keyword $ show d
