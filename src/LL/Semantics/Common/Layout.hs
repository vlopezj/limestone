{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}
module LL.Semantics.Common.Layout where

import MarXup
import MarXup.Tex
import qualified MarXup.PrettyPrint as PP
import MarXup.Latex
import MarXup.Latex.Math
import Data.Monoid
import Data.String
import Control.Monad
import LL

import TexPretty (math, autoindent, programS, programOneLineS) 

data Layout a where
  Layout :: (Monoid a, IsString a) => {
    text :: TeX → a
   ,hang :: a → a
   ,indent :: a → a
   ,(<+>) :: a → a → a
   ,(</>) :: a → a → a
   ,(<//>) :: a → a → a
   ,(<|>) :: a → a → a
   ,keyword :: String → a
   ,programLo :: IdSeqn2 → a
   } → Layout a

type Doc = PP.Docu
instance Monoid Doc where mempty = return mempty; mappend = liftM2 mappend
instance IsString Doc where fromString = PP.text . ensureMath . fromString

layout2D :: Layout Doc
layout2D = Layout{..} where
    text = PP.text
    hang = liftM$ PP.hang 4
    indent = liftM$ PP.indent 4
    (<+>) = liftM2 (PP.<+>)
    (</>) = liftM2 (PP.</>)
    (<//>) = liftM2 (PP.<//>)
    (<|>) = (</>)
    keyword = PP.text . math . mathsf . tex
    programLo = autoindent . programS

layout1D :: Layout TeX
layout1D = Layout{..} where
    text = id
    hang = id
    indent = id
    x <+> y = ensureMath$ x <> cmd0 "," <> y
    (</>) = (<+>)
    (<//>) = (</>)
    x <|> y = x <> ";" <> y
    keyword = math . mathsf . tex
    programLo = programOneLineS . erase . erase

prn :: (IsString s, Monoid s, Ord a) => a -> a -> s -> s
prn p q x = if p > q then "(" <> x <> ")" else x
