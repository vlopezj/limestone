{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeOperators #-}
module LL.Semantics.Measure where

import TexPretty
import MarXup
import MarXup.Tex
import MarXup.Latex
import LL
import MarXup.Latex.Math as M
import Data.Monoid ((<>))
import Data.Maybe
import Control.Applicative
import Control.Lens hiding (element)
import Data.List
import LL.Semantics.Common
import LL.Semantics.Common.Layout
import Reductions
import LL.Semantics.Static
import Eval
import TypeCheck
import Polarize
import Ritchie.Polar.Type
import Data.Default
import Ritchie.Polar
import ContM
import Data.Monoid

data Measure = MParen Measure 
             | MTyp IdType 
             | MProg (Seqn2 Id Id) 
             | MProgP (SeqnPol Id Id) 
             | MLam  (IdLambdaF)
             | MPlus Measure Measure 
             | MMax Measure Measure 
             | MMul Measure Measure 
             | MK IdSize
             | MEff Id
             | MInf
             | MCtxLength IdType
             | MPos IdType
             | MCase Id (Id, Measure) (Id, Measure)
             | MIf [((IdSize, CmpOp, IdSize), Measure)]
             | MBranches
             | MSum IdSize Measure

deriving instance Show (Measure)

instance Element Measure where
  type Target Measure = TeX
  element = ensureMath . texMeasure

instance Num Measure where
  MK 0 + a = a
  a + MK 0 = a
  MK a + MK b = MK$ a + b
  x + y    = MPlus x y

  MK 0 * a = 0
  a * MK 0 = 0
  MK 1 * a = a
  a * MK 1 = a
  MK a * MK b = MK$ a * b

  a * b | isLtMul b = a * MParen b
  a * b | isLtMul a = MParen a * b
  x * y         = MMul x y

  fromInteger = MK . fromInteger

isLtMul :: Measure -> Bool
isLtMul (MK (Size (_:_:_))) = True
isLtMul MPlus{} = True
isLtMul _       = False

texMeasure m = do
  texComment (show m)
  texMeasure' m
  where
    Layout{..} = layout1D

    texMeasure' m = case m of
        MK x -> element x
        MInf -> "∞"
        MProg x@(Seqn2 (Seqn SeqCtx{_linear} _)) -> (norm $ programOneLineTypedS x) <> brackets (mconcat$ intersperse ","
                                                                [texVar ref |
                                                                 ref ::: _ <- _linear])
        MPlus x y -> texMeasure x <> "+" <> texMeasure y
        MMul x y -> texMeasure x <> "·" <> texMeasure y
        MMax x y -> keyword "max" <> paren (texMeasure x <> "," <> texMeasure y)
        MTyp x -> norm (element x)
        MParen x -> paren (texMeasure x)
        MEff x -> texVar x
        MLam x -> norm (texLambdaOneLine x)
        MCtxLength x -> keyword "\\#v" <> paren (element x)
        MPos  x -> keyword "χ₊" <> paren (element x)
        MCase e (x,l) (y,r) ->
          keyword "match" <+> texVar e <+> (braces $ keyword "inl" <+> texVar x <> "↦" <> texMeasure l
                                        <+> ";" <+> keyword "inr" <+> texVar y <> "↦" <> texMeasure r)
        MIf br -> keyword "if" <+> (braces$ mconcat $ intersperse " ; "
                                       [ (element sz₁ <> mathCmpOp op <> element sz₂ <> "↦" <> texMeasure l)
                                       | ((sz₁,op,sz₂),l) <- br ])
        MBranches -> "#branches"
        MSum 0 _ -> texMeasure 0
        MSum 1 x -> texMeasure x
        MSum n x -> cmd0"sum" <> subscript (element n) <> texMeasure (MParen x)

    norm = ensureMath . parenthesize "|" "|"
    paren  = ensureMath . parenthesize "(" ")"
    braces  = ensureMath . parenthesize "{" "}"
    brackets  = ensureMath . parenthesize "[" "]"
 

measure m@(T t) = case t of
  a :+: b -> 1 + MMax (measure a) (measure b)
  Array _ n a -> MMul (MK n) (measure a)
  a :*: b -> MPlus (measure a) (measure b)
  Zero -> MK sizeZero
  One -> MK sizeZero
  Var _ (MetaSyntactic True _ _) -> MTyp m
  _ -> measure (neg m)

measureAritySeq m = go m
  where
  go m@(T t) = case t of
    ArraySeqPos _ a -> go a
    ArraySeqNeg _ a -> MMax 1 (go a)
    ArrayPar    _ a -> (go a)
    Array _ (sizeIsInteger → Just i) a ->  (fromInteger i) * (go a)
    ArrayTensor _ a -> MMul (MInf) (go a)
    a :*: b -> MPlus (go a) (go b)
    a :|: b -> MMax (go a) (go b)
    a :+: b -> MMax (go a) (go b)
    a :&: b -> MMax (go a) (go b)
    Unit _ → MK 0 
    Primitive _ _ -> MK 0
    _ -> MTyp m

-- | Measure the time of a parallel execution of the program on an
--   arbitrarily large number of processors.
measurePP (Seqn2 s) = cost s
  where
  cost s'@(Seqn _ s) = case s of
    (Ax v v' ) -> 1 -- FIXME: size of type
    (NCut nfo _ sz cs s t) -> 1 + MMax (cost s) (cost t)
    Halt{} -> 1
    SBot v -> 1
    Cross _β v v' w t -> 1 + cost t
    Par _β w v s v' t -> 1 + MMax (cost s) (cost t)
    Plus w (Branch v s) (Branch v' t) -> 1 + MMax (cost s) (cost t)
    With _β v' c w s -> 1 + cost s
    SZero w vs -> 1
    SOne _β w t -> 1 + cost t
    Split sz x y z t -> 1 + cost t
    BigTensor x z t ->  1 + cost t
    BigPar z ts -> 1 + foldr1 MMax [ cost t | (sz,Branch x t) <- ts]
    BigSeq ts   -> 1 + foldr1 (+) [ MK seqSz * cost seqProg | SeqStep{..} <- ts]
    Sync{..} -> 
      foldr1 (+) $ [1, cost contL] ++
                   maybeToList (cost <$> syncCont) ++
                   maybeToList (cost <$> syncContR) ++
                   [MTyp ty | (1,ty,_,_,_) <- syncIntr ] ++
                   [MK sz * MTyp ty | (sz,ty,_,_,_) <- syncIntr, sz /= 1 ] 

    Foldmap2 {..} -> 1 + cost fmUnit + cost fmCont + MK fmSize * cost fmMap + MParen (MK (fmSize - 1)) * MParen (5 + cost fmMix) 
    SizeCmp  {..} -> 1 + foldr1 MMax [cost t | (_,t) <- bCmp]
    _ -> MProg (Seqn2 s')

-- | Measure time of a sequential execution of the program.
measureP (Seqn2 s) = cost s
  where
    cost s'@(Seqn ann s) = case s of
      (Ax (_ ::: ty :^ 1) (_ ::: _)) -> 1 -- MTyp ty
      (NCut nfo FanLeft 1 cs s t) -> 1 + (cost s) + (cost t)
      (NCut nfo FanLeft sz cs s t) -> foldr1 (+) [MTyp ty | (_,_,ty) <- cs] +  (cost t) + (MK sz * cost s)
      NCut {} -> cost $ LL.seq %~ swapCut $ s' 
      Halt{} -> 1
      SBot v -> 1
      Cross _β v v' w t -> 1 + cost t
      Par _β w v s v' t -> 1 + cost s + cost t
      Plus w (Branch v s) (Branch v' t) -> 1 + MMax (cost s)(cost t)
      With _β v' c w s -> 1 + cost s
      SZero w vs -> 1
      SOne _β w t -> 1 + cost t
      Split sz x y z t -> 1 + cost t
      BigTensor x z t ->  1 + cost t
      BigPar z ts -> 1 + foldr1 (+) [ MK sz * cost t | (sz,Branch x t) <- ts]
      BigSeq ts   -> 1 + foldr1 (+) [ MK seqSz * cost seqProg | SeqStep{..} <- ts]
      Sync{..} -> foldr1 (+) $ [1, cost contL] ++
                               maybeToList (cost <$> syncCont) ++
                               maybeToList (cost <$> syncContR) ++
                   [MTyp ty | (1,ty,_,_,_) <- syncIntr ] ++
                   [MK sz * MTyp ty | (sz,ty,_,_,_) <- syncIntr, sz /= 1 ] 
      Foldmap2 {..} -> 1 + cost fmUnit + cost fmCont + MK fmSize * cost fmMap + MParen (MK (fmSize - 1)) * MParen (5 + cost fmMix) 
      SizeCmp  {..} -> 1 + foldr1 MMax [cost t | (_,t) <- bCmp]
      _ -> MProg ((Seqn2 s'))

χpos :: SAName a a → Bool 
χpos (SAn (sIsPos -> STrue) _)  = True
χpos (SAn (sIsPos -> SFalse) _) = False

measureContext :: IdTypedCtx → Measure
measureContext _ = 1
{-
measureLambda :: IdSeqnPol → Measure
measureLambda (SeqnPol s) = cost s
  where
    cost s'@(Seqn ann s) = case s of
      Ax (SAr _ (_ ::: (TyPol _ (T (Primitive _ _) :^ 1)))) _ -> 1
--      (Ax (_ ::: ty :^ 1) (_ ::: _)) -> MTyp ty
      NCut nfo FanLeft 1 [_] s t -> 1 + (cost s) + (cost t)
      NCut nfo FanLeft sz [(x,y,_)] (s@(Seqn ctx _)) t -> 1 + (cost t) +
                                               (MK sz * (if χpos x then (+ 1) else id) (cost s)) +
                                               measureContext ctx

      NCut {} -> cost $ LL.seq %~ swapCut $ s' 

      Halt{} -> 1
      SBot (SAr _ (v ::: _)) -> 2

      Cross _β v v' w t -> 2 + cost t
      Par _β w v s v' t -> 2 + cost s + cost t

      Plus w (Branch v s) (Branch v' t) -> 1 + MMax (cost s)(cost t)
      With _β v' c w s -> 1 + cost s
      SZero w vs -> 1
      SOne _β w t -> 2 + cost t
      Split sz x y z t -> 1 + cost t
      BigTensor x z t ->  1 + cost t
      BigPar z ts -> 1 + foldr1 (+) [ MK sz * cost t | (sz,Branch x t) <- ts]
      BigSeq ts   -> 1 + fromIntegral (length (nub [r | SeqStep{..} <- ts
                                        , (r,n) <- seqChunk ])) + foldr1 (+) 
                         [ measureContext ctx + MK seqSz * cost seqProg
                         | SeqStep{seqProg = seqProg@(Seqn ctx _),..} <- ts
                         ]
      Sync{..} -> foldr1 (+) $ [1, cost contL] ++
                               maybeToList (cost <$> syncCont) ++
                               maybeToList (cost <$> syncContR) ++
                   [MTyp ty | (1,TyA (TyPol _ ty),_,_,_) <- syncIntr ] ++
                   [MK sz * MTyp ty | (sz,TyA (TyPol _ ty),_,_,_) <- syncIntr, sz /= 1 ] 
      Foldmap2 {..} -> 1 + cost fmUnit + cost fmCont + MK fmSize *
                       cost fmMap + MParen (MK (fmSize - 1)) *
                       MParen (5 + cost fmMix) 
      SizeCmp  {..} -> 1 + foldr1 MMax [cost t | (_,t) <- bCmp]
      TApp{cont} ->  1 + cost cont
      TUnpack{cont} ->  1 + cost cont
      _ -> MProg (erase$ SeqnPol s')
-}
ad = erase . polarizeDerivUnsafe . analyseDeriv

lV s =
  let DPol (Deriv{_derivSequent}) = polarizeDerivUnsafe s in
  translateSeqn def _derivSequent

lTr s =
  let DPol (Deriv{_derivSequent}) = polarizeDerivUnsafe s in
  measureSeqn def _derivSequent

allRedMeasureRules :: [(TeX, ((IdDeriv2, [Measure]), ([Measure], IdDeriv2)))]

assocMeasureRules = [("Associativity-L",let s = cutAssoc1  in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
                    ,("Associativity-R",let s = cutAssoc1R in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
                    ]

structMeasureRules = assocMeasureRules ++
              [ ("Ax", let s = cutAx in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
               -- ,("Split", let s = cutSplit in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
              , ("Split", let s = cutSplitR in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
              -- , ("loop fusion", let s = cutFoldmap in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
              -- , ("loop fusion 2", let s = cutFoldmap2 in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
              ]
syncMeasureRules =
    [
      -- (math par<>"⊗L", let s = cutParCrossL szN id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
      (math par<>"⊗R", let s = cutParCross sizeOne id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    , (amp<>"⊕1", let s = cutWithPlus LL.Inl id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    , (amp<>"⊕2", let s = cutWithPlus LL.Inr id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    , ("⊥1", let s = cutUnit in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    -- , ("∃∀", let s = cutQuant id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    , ("n"<> math par <> "⊗", let s = cutBigL in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    -- , ("?!", let s =  cutSaveOffer in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    , ("n§", let s =  fuseMBigSeq in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
    ]

pushMeasureRules = [("κ⊗", let s = commutCross sizeOne in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            -- ,("κ⅋L", let s = commutParL id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κ⅋", let s = commutPar sizeOne id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κ⊕", let s = commutPlus in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κcompare", let s = commutSizeCmp in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            -- ,("κ∀", let s = commutForall id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            -- ,("κ∃", let s = commutExists in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            -- ,("κS", let s = commutSave in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            -- ,("κL", let s = commutLoad in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κn⊗", let s = commutBigTensor in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κn⅋", let s = commutBigPar in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κn§", let s = commutBigSeq in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            --,("κfoldmap", let s = commutFoldmap in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))

            ,("κsync", let s = commutSync in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κloop", let s = commutLoop in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κsplit", let s = commutSplit in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ,("κ&1", let s = commutWith LL.Inl id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            -- ,("κ&2", let s = commutWith Inr id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
            ]

mergeMeasureRules = [{-("merge-fold", let s = mergeFoldmap in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))-}
              ("merge-n§", let s = mergeBigSeq in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))
             ,("merge-n⅋", let s = mergeBigPar in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTr s']), ([lTr r'], r')))]


allRedMeasureRules = structMeasureRules ++ syncMeasureRules ++ pushMeasureRules ++ mergeMeasureRules

lTrS s = 
  let DPol (Deriv{_derivSequent}) = polarizeDerivUnsafe s in
  translateSeqn def{showIndexing = True} _derivSequent

allRedPreservationRules :: [(TeX, ((IdDeriv2, [IdLambdaF]), ([IdLambdaF], IdDeriv2)))]

assocPreservationRules = [("Associativity-L",let s = cutAssoc1  in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
                    ,("Associativity-R",let s = cutAssoc1R in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
                    ]

structPreservationRules = assocPreservationRules ++
              [ ("Ax", let s = cutAx in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
               -- ,("Split", let s = cutSplit in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
              , ("Split", let s = cutSplitR in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
              -- , ("loop fusion", let s = cutFoldmap in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
              -- , ("loop fusion 2", let s = cutFoldmap2 in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
              ]
syncPreservationRules =
    [
      -- (math par<>"⊗L", let s = cutParCrossL szN id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
      (math par<>"⊗R", let s = cutParCross sizeOne id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    , (amp<>"⊕1", let s = cutWithPlus LL.Inl id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    , (amp<>"⊕2", let s = cutWithPlus LL.Inr id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    , ("⊥1", let s = cutUnit in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    -- , ("∃∀", let s = cutQuant id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    , ("n"<> math par <> "⊗", let s = cutBigL in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    -- , ("?!", let s =  cutSaveOffer in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    , ("n§", let s =  fuseMBigSeq in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
    ]

pushPreservationRules = [("κ⊗", let s = commutCross sizeOne in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            -- ,("κ⅋L", let s = commutParL id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κ⅋", let s = commutPar sizeOne id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κ⊕", let s = commutPlus in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κcompare", let s = commutSizeCmp in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            -- ,("κ∀", let s = commutForall id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            -- ,("κ∃", let s = commutExists in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            -- ,("κS", let s = commutSave in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            -- ,("κL", let s = commutLoad in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κn⊗", let s = commutBigTensor in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κn⅋", let s = commutBigPar in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κn§", let s = commutBigSeq in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            --,("κfoldmap", let s = commutFoldmap in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))

            ,("κsync", let s = commutSync in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κloop", let s = commutLoop in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κsplit", let s = commutSplit in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ,("κ&1", let s = commutWith LL.Inl id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            -- ,("κ&2", let s = commutWith Inr id id in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
            ]

mergePreservationRules = [{-("merge-fold", let s = mergeFoldmap in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))-}
              ("merge-n§", let s = mergeBigSeq in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))
             ,("merge-n⅋", let s = mergeBigPar in let s' = ad s in let r = ideval 1 s in let r' = ad r in ((s', [lTrS s']), ([lTrS r'], r')))]

allRedPreservationRules = structPreservationRules ++ syncPreservationRules ++ pushPreservationRules ++ mergePreservationRules

measureSeqn :: TranslateSeqn → IdSeqnPol → Measure
measureSeqn opts@TranslateSeqn{..} (SeqnPol _derivSequent) = go _derivSequent
  where
  ignore = "???\\_"

  idx1 :: Measure 
  idx1 =
    if showIndexing then
        -- letTup2 (offset, array) x <$> cont (LVar array :@ (LSize (var offset + i)))  
        app
    else
        app

  shifted :: IdType → Measure
  shifted ty = 
    if showShifts then 
         if showIndexing then
           MPos ty
--          return$ lam x'$ let1 x (LTup (LSize 0) (lam u (lam κ (LVar κ :@ LVar x')))) r
        else
           MPos ty
--         return$ lam x'$ let1 x (lam κ (LVar κ :@ LVar x')) r
    else 0
-- return$ lam x r

  uidx :: Measure
  uidx = if not showIndexing then 0
         else 0

  demote :: [Id ::: IdSized] → Measure
  demote _ | not showIndexing = 0
  demote [] = 0
  demote ((x ::: ty :^ sz):xs) = 3 * MCtxLength ty + demote xs

  app = 1
  destruct = 1
  arith = 0
  mbranch l = if length l >= 2 then 1 else 0
  mschedule = 1
  mallocate = 3
  mwrite = 3
  mread = 3
{-                                      do
    MCtx letTup2 (offset, array) (LVar x) .
      let1 x (LTup (LSize$ var offset + i * sz) (LVar array)) <$> -}
       
  go :: SeqnPol' Id Id → Measure
  go s'@(Seqn ann s) = case s of
        Ax {wElim = SAr (sIsPos → STrue) _, zElim = _} → 1
        Ax {wElim, zElim} → go$ Seqn ann$ Ax{wElim = zElim, zElim = wElim}

        Cross {zElim = SAr SPos (zElim ::: _), xIntr = SAn _ (xIntr ::: _), yIntr = SAn _ (yIntr ::: _),..} ->
          idx1 + app + destruct + go cont
    
        Halt _ → 0

        SBot { zElim = SAr SNeg (zElim ::: _) } → 0
        SOne { zElim = SAr SPos (zElim ::: _), cont } → idx1 + app + go cont
        SZero { zElim = SAr SPos (zElim ::: _) } → idx1 + 0 

        NCut _ _ 1 [] a b → 2 + (go a) + (go b)

        NCut _ _ 1 [(SAn (sIsPos → SFalse) (xIntr ::: _),SAn _ (yIntr ::: _),_)] a@(Seqn SeqCtx{_linear} _)  b → do
          uidx + go a + go b
          {-
            y <- uidx xIntr (go a) ((pure.).lam)
            let1 yIntr (LTup (LSize 0) y) <$> go b -}


        NCut i fan 1 [(x,y,ty)] a b → go$ Seqn ann$ NCut i fan 1 [(y,x,dual ty)] b a 
        NCut i FanRight sz [(x,y,ty)] a b → go$ Seqn ann$ NCut i FanLeft sz [(y,x,dual ty)] b a 

        NCut _ FanLeft sz [(SAn _ (xIntr ::: (TyPol _ (tx :^ _))),SAn _ (yIntr ::: _),_)] a@(Seqn SeqCtx{_linear} _)  b → 
            let s = MSum sz (shifted tx + demote [v | v@(vIntr ::: _) <- _linear, vIntr /= xIntr] + go a) in
            s + go b

        Par { xIntr = SAn _ (xIntr ::: TyPol _ (tx :^ 1))
            , yIntr = SAn _ (yIntr ::: TyPol _ (ty :^ 1))
            , zElim = SAr SNeg (zElim ::: _)
            , ..} -> idx1 + 2 + (shifted tx + go contL) + (shifted ty + go contR)
                   
        Plus { zElim = SAr SPos (zElim ::: _)
             , inl = Branch (SAn _ (xIntr ::: _)) contL
             , inr = Branch (SAn _ (yIntr ::: _)) contR
             } ->
             idx1 + app + destruct + MCase zElim (xIntr, go contL) (yIntr, go contR)

        With { zElim = SAr SNeg (zElim ::: _)  
             , xIntr = SAn _ (xIntr ::: TyPol _ (tx :^ _))
             , .. } ->
          idx1 + app + shifted tx + go cont

        BigTensor { zElim = SAr SPos (zElim ::: _)
                  , xIntr = SAn _    (xIntr ::: _)
                  , .. } →
          idx1 + app + go cont

        BigPar { zElim    = SAr SNeg (zElim ::: _)
               , branches 
               , .. } →
                 
            let gob = \case
                            []  → 0
                            (sz,Branch (SAn _ (xIntr ::: TyPol _ (tx :^ _))) cont@(Seqn SeqCtx{_linear} _)):rest →
                              MSum sz (mbranch branches +
                                    demote [v | v@(vIntr ::: _) <- _linear, vIntr /= xIntr] +
                                    shifted tx +
                                    go cont 
                                    ) + gob rest
            in
            idx1 + app + gob branches 


        BigSeq steps ->
          let refsPosAll = nub$ [ref | SeqStep{seqChunk} <- steps
                                    , (SAr SApoP (ref ::: _),_) ← seqChunk] :: [Id]

              refsNegAll = nub$ [ref | SeqStep{seqChunk} <- steps
                                    , (SAr SApoN (ref ::: _),_) ← seqChunk] :: [Id]
          in
          fromIntegral (length refsPosAll) * (idx1 + app) +
          fromIntegral (length refsNegAll) * (idx1) +
          (MK (sum [seqSz | SeqStep{seqSz} <- steps]) * mbranch steps) + 
          sum [  
            let refs_pos = [(zElim, xIntr) | (SAr SApoP (zElim ::: _), SAn _ (xIntr ::: _)) ← seqChunk] :: [(Id,Id)]
                refs_neg = [(wElim, yIntr) | (SAr SApoN (wElim ::: _), yIntr) ← seqChunk] :: [(Id, SAName Id Id)] 
            in
                let pos_loop =  
                            MSum seqSz (idx1 * fromIntegral (length refs_pos) + uidx +
                              demote [v | v@(vIntr ::: _) <- _linear
                                     , not$ vIntr `elem` [n | (_,SAn _ (n ::: _)) <- seqChunk]]
                               + go seqProg)

                in case refs_neg of
                    [(wElim,SAn (sIsPos -> p) (yIntr ::: TyPol _ (ty :^ _)))] → 
                        uidx + shifted ty + pos_loop
                    [] -> mschedule + pos_loop

              | SeqStep {seqProg = seqProg@(Seqn SeqCtx{_linear} _),..} <- steps ]

         -- Business as usual for the positive connectives
          -- Use the state translation for the negative ones
        Sync { syncIntr = [(sz, ty, SAn _ (xIntr ::: _), mu, mr)]
             , contL , syncCont, syncContR } →

            let a = mallocate

                w = mwrite 

                u = case mu of
                        Nothing → 0
                        Just (SyncUpdate{syncWIntr = SAn _ (wIntr ::: _), ..}) → (mread + destruct + mwrite) * MK syncSteps 

                r = case mr of
                        Nothing → 0
                        Just (SyncRead{syncYIntr = SAn _ (yIntr ::: _), ..}) →  mread * MK syncCopies 

            in (maybe id id $ (+) . go <$> syncCont) $ (maybe id id $ (+) . go <$> syncContR) $ go contL + MK sz * (a + w + u + r)
            
        TUnpack{zElim = SAr _ (zElim ::: _)
               ,xIntr = SAn _ (xIntr ::: _)
               ,tyIntr = SAn _ (tyIntr ::: _)
               ,..} → 
              
              idx1 + app + destruct + uidx + go cont 
{-
              idx1 (LVar zElim) 0 $ \v → 
              (v :@) <$> (uidx xIntr (go cont) $ \xIntr' c' → pure$ lamTup2 (tyIntr, xIntr') c')
 -}       
        TApp{zElim = SAr _ (zElim ::: _)
            ,xIntr = SAn _ (xIntr ::: TyPol _ (tx :^ 1))
            ,ty = TyA (TyPol _ ty)
            , ..} → 
         idx1 + app + shifted tx + go cont

        Merge{zElim   = SAr _ (zElim ::: TyPol _ (_ :^ sz₁)) 
             ,wElim   = SAr _ (wElim ::: TyPol _ (_ :^ sz₂))
             ,splitSz = sz
             ,xIntr   = SAn _ (xIntr ::: _)
             ,cont
             } → (MK (sz₁ + sz₂) * mbranch [(),()]) + go cont --let1 xIntr (mergeT (LVar wElim) (LVar zElim)) <$> go cont

        Split { zElim = SAr _ (zElim ::: _ :^+ n)
              , xIntr = SAn _ (xIntr ::: _)
              , yIntr = SAn _ (yIntr ::: _)
              , splitSz
              , cont } →

              0 + go cont

        SizeCmp{sz1, sz2, bCmp} -> 
            arith + MIf [ ((sz1, op, sz2), go cont) | (op, cont) <- bCmp ]

--        s -> MLam$ translateSeqn opts (SeqnPol (Seqn ann s))
        s -> MProg$ erase (SeqnPol (Seqn ann s))
--LTerm (whatString what) [(id_name v, LVar v) | ElemLinear (SAr _ (v ::: _)) <- zVars]
{-
        _ → return (LProg (erase (SeqnPol s'))) -}
