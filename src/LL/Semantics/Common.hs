{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
module LL.Semantics.Common(module LL.Semantics.Common
                          ,module LL.Semantics.Common.Core) where

import LL.Semantics.Common.Core 
import LL
import Id
import TexPretty

import MarXup
import MarXup.Tex
import MarXup.Latex
import MarXup.Latex.Math
import Fresh

import Data.Generics.Genifunctors

import Data.List
import Data.Monoid
import Data.String
import Data.Bifunctor
import Data.Bifoldable
import Data.Bitraversable

instance Bifunctor SF where bimap = bimapDefault
instance Bifoldable SF where bifoldMap = bifoldMapDefault
instance Bitraversable SF where bitraverse = $(genTraverse ''SF)

instance Element (IdSF) where
  type Target (IdSF) = TeX
  element = ensureMath . texSF 0

instance Element (SFPrim) where
  type Target (SFPrim) = TeX
  element = ensureMath . texPrim

instance Element (SF Name Name) where
  type Target (SF Name Name) = TeX
  element = ensureMath . texSF 0  . runFreshM . fromNamesLiberal







