{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
module LL.Semantics.Dynamic where

import LL
import Type
import Type.Traversal
import LL.Semantics.Common
import LL.Semantics.Static
import Fresh
import Control.Applicative
import Control.Monad
import Data.Traversable as T
import Id

-- Property: if T is positive connective at top level,
-- then:  translateTypePlus (T) = SFN (translateTypeMinus (dual T))
--
-- translateSizedPol (SizedPol (t :^ s, +)) = translatePlus  (ArrayTensor sz t)
-- translateSizedPol (SizedPol (t :^ s, -)) = translateMinus (ArrayTensor sz t)
--
translateTypePlus  (T t) = case t of
    t :|: u               → SFN  ((rm (dual t) :× r (dual u)) :+ (r (dual t) :× rm (dual u)))
    t :*: u               → SFNN (r t :× r u)


    ArrayPar    _ t       → SFN (SFNat :× (SFNat :× (SFNat :→ (r (dual t) :∪ rm (dual t)))))
    ArrayTensor _ t       → SFNN (SFNat :× (SFNat :→ r t))

    ArraySeq    _ t       → SFNN (SFNat :× (SFNat :→ r t))

    t                     → maybe (SFTypePlus (T t)) id (translateType' r (T t))
  where
    r   = translateTypePlus
    rm = translateTypeMinus


translateTypeMinus (T t) = case t of
    t :*: u               → SFNN ((rp t :× r u) :+ (r t :× rp u))
    t :|: u               → SFN  (r (dual t) :× r (dual u))

    ArrayTensor _ t       → SFNN (SFNat :× (SFNat :× (SFNat :→ (r t :∪ rp t))))
    ArrayPar    _ t       → SFN  (SFNat :× (SFNat :→ r (dual t)))

    ArraySeq    _ t       → SFN (SFNat :× (SFNat :→ r (dual t)))

    t                     → maybe (SFTypeMinus (T t)) id (translateType' r (T t))
  where
    rp = translateTypePlus
    r  = translateTypeMinus

translateSizedPlusCLLM tysz@(ty :^ sz) = (:^ sz) <$> translateTypePlusCLL ty
translateSizedMinusCLLM tysz@(ty :^ 1)  = (:^ 1)  <$> translateTypeMinusCLL ty

translateSizedPlusCLL = runFreshMFrom <*> translateSizedPlusCLLM
translateSizedMinusCLL = runFreshMFrom <*> translateSizedMinusCLLM

translateTypePlusCLL :: IdType → FreshM IdType
translateTypePlusCLL  (T t) = case t of
    t :|: u               → (&) <$> ((⅋) <$> rm t <*> r u) <*> ((⅋) <$> r t <*> rm u)
    ArrayPar    sz t      → do
                              i  ← freshFrom "i"; let isz = var i
                              p  ← freshFrom "p"
                              tm ← rm t
                              t' ← r  t
                              return$ (bigPar sz tm) & (T$ Forall i (T TSize) ({-T$ Forall p (T$ TProp (isz,Lt,sz))-} (
                                                            (bigPar isz tm ⅋
                                                             t' ⅋
                                                             bigPar (sz - isz - 1) tm))))
                                                            
    ArraySeq    sz t      → bigSeqPlus sz <$> r t

    Var ts (MetaSyntactic b₁ True v) → T.mapM r ts >>= \ts' → pure$ T$ Var ts' (MetaSyntactic b₁ True (v ++ "⁺"))
    Var ts (MetaSyntactic b₁ False v) → T.mapM r ts >>= \ts' → pure$ T$ Var ts' (MetaSyntactic (not b₁) True (v ++ "⁻"))

    t                     → T <$> traverseType'' defTypeTraverse{tty = r} t 

  where
    r   = translateTypePlusCLL
    rm = translateTypeMinusCLL

translateTypeMinusCLL :: IdType → FreshM IdType
translateTypeMinusCLL (T t) = case t of
    t :*: u               → (⊕) <$> ((⊗) <$> rp t <*> r u) <*> ((⊗) <$> r t <*> rp u)
    ArrayTensor    sz t   → do
                              i  ← freshFrom "i"; let isz = var i
                              p  ← freshFrom "p"
                              tp ← rp t
                              t' ← r  t
                              return$ (bigTensor sz tp) ⊕ (T$ Exists i (T TSize) ({-T$ Exists p (T (TProp (isz,Lt,sz)))-} (
                                                            (bigTensor isz tp ⊗
                                                             t' ⊗
                                                             bigTensor (sz - isz - 1) tp))))
                                                            
    ArraySeq    sz t      → bigSeqMinus sz <$> r t

    Var ts (MetaSyntactic b₁ b₂ v) → T.mapM r ts >>= \ts' → pure$ T$ Var ts' (MetaSyntactic b₁ b₂ (v ++ "⁻"))

    t                     → T <$> traverseType'' defTypeTraverse{tty = r} t 

  where
    rp = translateTypePlusCLL
    r  = translateTypeMinusCLL



translateSizedPol :: SizedP n r → SF n r

translateSizedPol (SizedP (t :^ sz) (PolConst True))  = (SFNat :× (SFNat :→ (translateTypePlus t)))
translateSizedPol (SizedP (t :^ sz) (PolConst False)) = (SFNat :× SFNat :× (SFNat :→ (translateTypePlus t :∪ translateTypeMinus t)))
translateSizedPol (SizedP (t :^ sz) p)                = (SFNat :× (SFUnit :+ SFNat) :× (SFNat :→ (SFN (translateTypePlus t) :∪ SFN (translateTypeMinus t))))


translateSeqn :: IdSeqn2 → LambdaF Id
translateSeqn (Seqn2 _derivSequent) = runFreshM$ go _derivSequent

  where

  go :: Seqn2' Id Id → FreshM (LambdaF Id)
  go s'@(Seqn _ s) = case s of
    _ → return$ LProg (Seqn2 s')

translateDeriv0PolName :: (MonadFresh m) => Maybe Name → IdDeriv0 → m IdDeriv0
translateDeriv0PolName name' d@(D0 Deriv{_derivCtx=SeqCtx{_linear}}) =
  let ident' = flip fmap name' (\name' ->
        let [ident] = [ ident | ident@Id{..} ::: _ <- _linear
                                , name' == id_name
                                ]
        in ident)
  in translateDeriv0Pol ident' d



translateDeriv0Pol :: (MonadFresh m) => Maybe Id → IdDeriv0 → m IdDeriv0
translateDeriv0Pol ident (D0 (Deriv{_derivCtx = SeqCtx{..},..})) = do
  _derivSequent <- translateSeqn0Pol ident _derivSequent
  return$
    D0 (Deriv{
         _derivName
        ,_derivCtx = SeqCtx{_intuit
                           ,_linear = [ case ident of
                                          Just ident' | ref == ident' ->
                                            nameAsMinus ref ::: translateSizedMinusCLL t
                                          _ ->
                                            nameAsPlus ref ::: translateSizedPlusCLL t

                                      | ref ::: t <- _linear ]
                           }
        ,_derivSequent
        })

trDerivPol :: Maybe Name → IdDeriv0 → IdDeriv0
trDerivPol v d = runFreshMFrom d (translateDeriv0PolName v d)

translateSeqn0Pol :: forall m. (MonadFresh m) => Maybe Id
               → IdSeqn0
               → m IdSeqn0
translateSeqn0Pol v (S0 s) = (S0 `liftM` go s v) >>= refreshFromNames
  where
    r :: Seqn0' Id Id -> Maybe Id -> m (Seqn0' Id Id) 
    r (S' s) v = S' <$> go s v

    go Par{contR = contR', contL = contL', ..} (Just v) | v == zElim = do
      contL <- r contL' (Just xIntr) 
      contR <- r contR' (Just yIntr) 
      return $
        Par{zElim = nameAsMinus zElim
           ,xIntr = nameAsMinus xIntr
           ,contL 
           ,yIntr = nameAsMinus yIntr
           ,contR
           ,β}

    go Par{contL = contL', contR = contR',..} v' = do
      x <- refreshId xIntr
      case v' of
        Just v | v `usedIn` contL' -> do
          contL <- r contL' (Just v) 
          contR <- r contR' (Just yIntr) 
          return$
            With{zElim  = nameAsPlus zElim
                ,choose = LL.Inr
                ,xIntr = x 
                ,cont = S'$ Par{zElim = nameAsPlus x
                                ,xIntr = nameAsPlus xIntr
                                ,contL
                                ,yIntr = nameAsMinus yIntr
                                ,contR
                                ,β
                                }
                ,β
                }   

        _ -> do
          contL <- r contL' (Just xIntr) 
          contR <- r contR' v' 
          return$
            With{zElim  = nameAsPlus zElim
                ,choose = LL.Inl
                ,xIntr = x 
                ,cont = S'$ Par{zElim = nameAsPlus x
                            ,xIntr = nameAsMinus xIntr
                            ,contL
                            ,yIntr = nameAsPlus yIntr
                            ,contR
                            ,β
                            }
                ,β
                }   

    go s@Cross{β, zElim = zElim', xIntr = xIntr1', yIntr = yIntr1', cont = contL'} (Just v) | v == zElim' = do

        ([_], S' Cross{zElim = z2'
             ,xIntr = xIntr2'
             ,yIntr = yIntr2'
             ,cont = contR'
             }) <- refreshBinders' (S' s) [zElim']

        z1' <- refreshId zElim'

        let zElim = nameAsMinus zElim'

        let xIntr1 = nameAsPlus  xIntr1'
        let yIntr1 = nameAsMinus yIntr1'
        let xIntr2 = nameAsMinus xIntr2'
        let yIntr2 = nameAsPlus  yIntr2'

        contL <- r contL' (Just yIntr1')
        contR <- r contR' (Just xIntr2')

        return$ 
            Plus { zElim
                 , inl = Branch (nameAsMinus z1') (S'$ Cross {β, zElim = z1',
                                        xIntr = xIntr1, yIntr = yIntr1, cont = contL})
                 , inr = Branch (nameAsMinus z2') (S'$ Cross {β, zElim = z2',
                                        xIntr = xIntr2, yIntr = yIntr2, cont = contR}) }


    go s@BigPar{zElim = zElim', branches = branches'} (Just v) | v == zElim' = do
      branches <- T.sequence [((sz,) . Branch (nameAsMinus xIntr)) <$> r cont (Just xIntr) | (sz,Branch xIntr cont) <- branches']
      return$ BigPar{zElim = nameAsMinus zElim'
                    ,branches 
                    }

    go s@BigPar{zElim = zElim', branches = branches'} (Just v) = do
      let (before',(1,Branch z2 cont1'):after') = break (\(_, Branch _ cont) -> v `usedIn`cont) branches'
      [zA, zB, zB', zC, z1, z3] <- T.sequence $ take 6 $ repeat (refreshId zElim')
      cont1 <- r cont1' (Just v)
      before <- T.sequence [((sz,) . Branch (nameAsMinus xIntr)) <$> r cont (Just xIntr) | (sz,Branch xIntr cont) <- before']
      after  <- T.sequence [((sz,) . Branch (nameAsMinus xIntr)) <$> r cont (Just xIntr) | (sz,Branch xIntr cont) <- after']
      let β = False
      return$ With{β
       ,zElim = zElim'
       ,choose = LL.Inr
       ,xIntr = zA
       ,cont = S'$ TApp{β
          ,zElim = nameAsPlus zA
          ,ty     = T (SizeT (sum [sz | (sz,_) <- before]))
          ,xIntr  = zB
          ,cont   = {-S'$ TApp{β
                ,zElim = nameAsPlus zB
                ,ty     = T Prop --(TProp (sum [sz | (sz,_) <- before], Lt, sum [sz | (sz,_) <- branches']))
                ,xIntr  = zB'
                ,cont   =-} S'$ Par {β
                                    ,zElim = zB
                                    ,xIntr = z1 
                                    ,contL = S'$ BigPar z1 before
                                    ,yIntr = zC
                                    ,contR = S'$ Par{β
                                                    ,zElim = zC
                                                    ,xIntr = nameAsPlus z2
                                                    ,contL = cont1
                                                    ,yIntr = z3
                                                    ,contR = S'$ BigPar z3 after
                                                    } } {- } -} } }
        
    go s@BigPar{zElim = zElim', branches = branches'} (Nothing) = do
      [zA] <- T.sequence $ take 1 $ repeat (refreshId zElim')
      branches <- T.sequence [((sz,) . Branch (nameAsMinus xIntr)) <$> r cont (Just xIntr) | (sz,Branch xIntr cont) <- branches']
      let β = False
      return$ With{β
       ,zElim = zElim'
       ,choose = LL.Inl
       ,xIntr = zA
       ,cont = S'$ BigPar zA branches }

    
                   
    go (BigSeq fence) v' =
      BigSeq <$> T.sequence [ do
                              seqProg <- r seqProg' (v' >>= flip lookup seqChunk')
                              return$ SeqStep{
                                seqSz
                               ,seqChunk = [if Just ref == v'
                                              then (ref, nameAsMinus name)
                                              else (ref, nameAsPlus name)
                                           | (ref,name) <- seqChunk']
                               ,seqProg
                              }
                          | SeqStep{seqChunk = seqChunk',seqProg = seqProg',..} <- fence ]

    go What{..} v' = 
      let zVars' =
            [case z of
                ElemLinear r -> ElemLinear$ nameAsPol r v'
                _ -> z
            | z <- zVars ]
      in return$ What{zVars = zVars',..}

nameAsPlus Id{..} = Id { id_name = id_name ++ "⁺", .. }
nameAsMinus Id{..} = Id { id_name = id_name ++ "⁻", .. }

nameAsPol a@Id{..} (Just b) | a == b = nameAsMinus a
nameAsPol a        _                 = nameAsPlus a 

                 
                 
                  
