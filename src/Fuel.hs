{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
-- | The Fuel Transformer Monad (TM)
-- Written by jyp,
-- Refactored to a separate module by danr
-- Keeps an internal state consisting of a number of fuel units. Computations may
-- require fuel units to work, and will stop if there's no more fuel available. 
module Fuel where

import Control.Monad.Identity
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Except.Extra
import Control.Applicative

newtype FuelT m a = FuelT { getFuel :: StateT Int m a }

deriving instance (Functor m) => (Functor (FuelT m))
deriving instance (Functor m, Monad m) => (Applicative (FuelT m))
deriving instance (Monad m) => (Monad (FuelT m))
deriving instance (MonadTrans FuelT)
instance (MonadState r m) => MonadState r (FuelT m) where
  state f = lift $ state f
deriving instance (Functor m, MonadPlus m) => (Alternative (FuelT m))
deriving instance (MonadPlus m) => (MonadPlus (FuelT m))

instance (MonadFuel m) => (MonadFuel (ReaderT r m)) where
  consume a (ReaderT f) = ReaderT (\r -> consume a (f r))
  consumeN k a (ReaderT f) = ReaderT (\r -> consumeN k a (f r))

instance (MonadFuel m) => (MonadFuel (ExceptT e m)) where
  consume a (ExceptT m) = ExceptT $ consume (Right a) m
  consumeN k a (ExceptT m) = ExceptT $ consumeN k (Right a) m

type Fuel = FuelT Identity
type Natω = Maybe Int

-- | consumeN 1 ≡ consume
--   consumeN 0 _ ≡ id 
--   consumeN k ≡ consume0  (∀k. k < 0)
--   consumeN (n + m) a ≡ consumeN n a . consumeN m a | n ≥ 0 
class (Monad m) => MonadFuel m where
  consume :: a -> m a -> m a
  consume = consumeN 1

  consumeN :: Int -> a -> m a -> m a
  consumeN k a m | k > 0 = consume a (consumeN (k-1) a m) 
  consumeN _ _ m = m
  {-# MINIMAL consume | consumeN #-}

consumeNω :: (MonadFuel m) => Natω -> a -> m a -> m a
consumeNω Nothing  = const . return
consumeNω (Just k) = consumeN k
-- | Executes the given computation if there's one unit of fuel left, else stop with
-- the given final value.

instance (Monad m) => MonadFuel (FuelT m) where
    consume :: a -> FuelT m a -> FuelT m a
    consume stop continue = do
      fuel <- FuelT get
      if fuel == 0 then return stop else (FuelT (put (fuel-1))) >> continue

instance (MonadFuel m) => (MonadFuel (StateT r m)) where
  consume a (StateT f) = StateT $ \s -> consume (a,s) (f s)

-- | Run the computation and retrieve the final fuel level, together with the result.
runAndGetFuelT :: Functor m => Int -> FuelT m a -> m (a,Int)
runAndGetFuelT n (FuelT m) = runStateT m n

-- | Run the computation and retrieve the result
runFuelT :: Functor m => Int -> FuelT m a -> m a
runFuelT n m = fst `fmap` runAndGetFuelT n m

-- | Run the computation and retrieve the final fuel level, together with the result.
runAndGetFuel :: Int -> Fuel a -> (a,Int)
runAndGetFuel n = runIdentity . runAndGetFuelT n

-- | Run the computation and retrieve the result
runFuel :: Int -> Fuel a -> a
runFuel n = fst . runAndGetFuel n

mapFuelT :: (forall a. m a -> n a) -> FuelT m a -> FuelT n a
mapFuelT f (FuelT m) = FuelT$ mapStateT f m 
