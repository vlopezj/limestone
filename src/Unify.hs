{-# LANGUAGE ViewPatterns,MultiParamTypeClasses,GeneralizedNewtypeDeriving, TupleSections,
             OverloadedStrings,ScopedTypeVariables,PatternGuards,CPP,TypeFamilies,
             PatternSynonyms, NamedFieldPuns #-}
#define REPORT report (__FILE__ ++ ":" ++ show (__LINE__ :: Integer))
module Unify where

import MetaVar

import PrettyId

import Text.PrettyPrint

import qualified Data.Map as M

import Control.Applicative hiding (empty)
import Control.Monad
import Control.Monad.State

import Text.Show.Pretty (ppDoc)
import Data.List ((\\),nub)
import Data.Traversable

import LL hiding (choice,Choice,second)

import DsErrors

import DsMonad

import Data.Tuple

import Unify.Size

import Type.Traversal

unify :: MetaInfo -> IdType {- origin's type -} -> IdType {- ^ match type -} -> DsM ()
unify blame u0 v0 = do
    su <- gets st_mgu
    u0' <- applySubst su u0
    v0' <- applySubst su v0
    REPORT $ hang "Unify" 4 (pretty u0' $$ pretty v0' $$ ppInfo blame)
    go [] u0' v0'
    su' <- gets st_mgu
    REPORT $ vcat [ pretty mv <+> "->" <+> pretty t | (mv,(_sc,t)) <- M.toList su' ]
  where
    -- The two type's scope list is maintained to check for alpha-equivalence (see the TVar case)
    go :: [(Id,Id)] -> IdType -> IdType -> DsM ()
    go tvs (T t₁) (T t₂) = go' t₁ t₂
                           where
        go' (Var xs (Meta (MetaVar False u scope info))) t2 = go' (Var xs (Meta (MetaVar True u scope info))) (unT$ neg (T t2))
        go' t1@(Var _xs (Meta (MetaVar True  u _scope _info))) t2@(Var _ys (Meta (MetaVar b u' _ _)))
          | u == u' = if b then return ()
                           else throw $ msgUnificationError blame (T t1) (tyRenameList (map swap tvs) (T t2))
        go' (Var _xs (Meta (MetaVar True  u scope info))) t2 = do
            let tv_subst = map swap tvs

            let t2' = tyRenameList tv_subst (T t2)
                t2'_mvs = tyMetaRefs t2'
                t2'_free = nub (tyRefs t2') \\ nub (tyBinders t2')

            REPORT $ hang ("Meta var resolved" <+> ppDoc u) 4
                $  "scope:" <+> ppDoc scope
                $$ "tvs:" <+> ppDoc tvs
                $$ "t2':" <+> pretty t2'
                $$ "t2'_free:" <+> ppDoc t2'_free
                $$ "blame:" <+> ppInfo blame
                $$ "info:" <+> ppInfo info

            when (u `elem` t2'_mvs) $ throw (msgOccursCheck u t2')
            forM_ t2'_free $ \ tv -> when (tv `notElem` scope) $
                throw (msgTypeVariableEscapes tv scope t2' info) -- info or blame?

            su <- gets st_mgu

            case M.lookup u su of
                Nothing        -> modify $ \ st -> st { st_mgu = M.insert u (scope,t2') su }
                Just (ltvs,t3) -> go (zip ltvs (map fst tvs)) t2' t3
                    -- this case happens when when an existential has
                    -- substituted in the same meta variable several times

        go' t1 (Var xs (Meta (MetaVar pos u scope info))) = go (map swap tvs) (T (Var xs (Meta (MetaVar pos u scope info)))) (T t1)

        go' (Var _ (TVar b1 x1)) (Var _ (TVar b2 x2))
          | b1 == b2 && (x1 == x2 || (x1,x2) `elem` tvs)
          = return ()

        go' (a :*: b)    (a' :*: b')  = go tvs a a' >> go tvs b b'
        go' (a :|: b)    (a' :|: b')  = go tvs a a' >> go tvs b b'
        go' (a :+: b)    (a' :+: b')  = go tvs a a' >> go tvs b b'
        go' (a :&: b)    (a' :&: b')  = go tvs a a' >> go tvs b b'
        go' (Bang u)     (Bang v)     = go tvs u v
        go' (Quest u)    (Quest v)    = go tvs u v
        go' (Array d1 s1 u) (Array d2 s2 v) | d1 == d2 = goSize tvs s1 s2 >> go tvs u v
        go' (Forall x a u) (Forall y b v) = go tvs a b >> go ((x,y):tvs) u v
        go' (Exists x a u) (Exists y b v) = go tvs a b >> go ((x,y):tvs) u v
        go' (Rec b1 x u) (Rec b2 y v) | b1 == b2 = go ((x,y):tvs) u v
        go' (Index b1)   (Index b2)   | b1 == b2 = return ()
        go' (Int b1)     (Int b2)   | b1 == b2 = return ()
        go' Zero         Zero         = return ()
        go' One          One          = return ()
        go' Top          Top          = return ()
        go' Bot          Bot          = return ()
        go' TType        TType         = return ()
        go' (SizeT s1)   (SizeT s2)   = goSize tvs s1 s2


        go' t1 t2 = throw $ msgUnificationError blame (T t1) (tyRenameList tvs (T t2))

    goSize :: [(Id,Id)] -> IdSize -> IdSize -> DsM ()
    goSize tvu s1 s2 = do
      pairs <- unifySize blame tvu s1 s2
      forM_ pairs $ \(m, sz) -> go tvu (symb (Meta m)) (T$ SizeT sz)


-- in the monad to throw errors and write debug messages
-- todo: rewrite as a traverse
applySubst :: UnifySubst -> IdType -> DsM IdType
applySubst su t0 = do
    REPORT $ "applySubst from" <+> pretty t0
    r <- go t0
    REPORT $ "applySubst from" <+> pretty t0 <+> "=" <+> pretty r
    return r
  where
    go = foldType defTypeFold{sty,ssz}
    ssz _ sz = applySubstSize su sz
    sty _ t = case t of
           ty0@(Var _ (Meta (MetaVar pos u scope _info))) -> case M.lookup u su of
            Just (delta,ty) -> do
                let tv_subst = zip delta scope
                let ty' = tyRenameList tv_subst ((if pos then id else neg) ty)
                ty'' <- dsRunFresh (refreshType ty')
                REPORT $ hang ("applySubst" <+> text (show u) <+> "->" <+> pretty ty) 4
                    $  "scope:" <+> ppDoc scope
                    $$ "delta:" <+> ppDoc delta
                    $$ "ty':" <+> pretty ty'
                    $$ "ty'':" <+> pretty ty''
                go ty''
            Nothing  -> return (T ty0)

           (Array d sz u)     -> return $ arrayOp' d sz u

           Plupp{}            -> error "Plupp in unify!?"
           UnpushedNeg{}      -> error "UnpushedNeg in unify!?"

           SizeT{}   -> return (T t)
           t@TType  -> return (T t) 
           (:*:){}  -> return (T t)
           (:|:){}  -> return (T t) 
           (:+:){}  -> return (T t) 
           (:&:){}  -> return (T t) 
           Bang{}   -> return (T t)
           Quest{}          -> return (T t)
           Forall{} -> return (T t)
           Exists{} -> return (T t)
           Rec{} -> return (T t)
           Zero{} -> return (T t)
           One{} -> return (T t)
           Top  {} -> return (T t)
           Bot{} -> return (T t)
           Pole{} -> return (T t)
           Var{} -> return (T t)
           Index{} -> return (T t)
           Int{} -> return (T t)
        -- neither of these three should be created by the desugarer

           Lollipop{}              -> return (T t)
           (Var _ MetaSyntactic{}) -> return (T t)
           (MetaNeg{})             -> return (T t)

newMeta :: (EmbedSymbol s, Ref s ~ Id) => MetaInfo -> DsM s
newMeta mi = do
    u <- dsFresh
    tvs <- gets (map (fst . snd) . st_iscope)
    REPORT $ "newMeta:" <+> text (show u) <+> "scope:" <+> ppDoc tvs
    return $ symb (Meta (MetaVar True u tvs mi))

newMetas :: (EmbedSymbol s, Ref s ~ Id) => Int -> MetaInfo -> DsM [s]
newMetas n mi = replicateM n (newMeta mi)

