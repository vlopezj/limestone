{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TransformListComp #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE PatternGuards, OverloadedStrings, RecordWildCards #-}
{-# LANGUAGE UnicodeSyntax #-}
module TypeCheck (TypeCheckEnv(..)
                 ,typeCheck
                 ,typeCheckDeriv
                 ,typeCheckStrict
                 ,analyseDeriv
                 ,TypeError(..)) where

import LL hiding ((&), linear, intuit)
import LL.Traversal
import Type.Traversal (foldTypeWithSelf',defTypeFold)
import qualified Type.Traversal as TT
import qualified Type.Ctx
import Type.Ctx hiding (linear, intuit)
import Data.List
import Control.Applicative
import Control.Monad
import Data.Default
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Writer hiding (Dual)
import Control.Monad.Except.Extra
import Text.PrettyPrint hiding ((<>))
import GHC.Exts
import Data.Traversable as T
import Control.Lens
import Text.Show.Pretty (ppShow)
import Tagged
import Fresh
import Data.Bifunctor
import Data.Function
import Data.Tuple
import Development.Placeholders
import Data.Generics.Geniplate
import Data.Monoid ((<>),mempty)
import Id
import FileLocation
import Polarize
import TypeCheck.Core as TypeCheck
import Op
import ContM
import Type.Data
import Lint (checkCtxDeriv)
import Control.Arrow



typeCheckDeriv :: Deriv1 Id Id -> ([TypeError], Deriv2 Id Id)
typeCheckDeriv d1 = runFreshM $ flip runReaderT def (typeCheck d1)

typeCheck :: Deriv1 Id Id -> TypeCheckM Id Id ([TypeError], Deriv2 Id Id)
typeCheck (D1 d) = do
  fmap swap $ runWriterT $ D2 <$> do
    local (typeCheckCtx %~ (d ^. derivCtx <>)) $
      liftTypeCheckNIA_ (predsFromCtx (d ^. derivCtx)) $
        d & derivSequent   %%~ typeCheckSeq

typeCheckStrict :: Deriv1 Id Id -> ExceptT [TypeError] (TypeCheckM Id Id) (Deriv2 Id Id)
typeCheckStrict d1 = do
  (errs, d2) <- lift $ typeCheck d1
  case errs of
    [] -> return d2
    _  -> throwError errs

typeCheckType :: Type' Id Id -> TypeCheckErrM (Type' (Id :::@ Type' Id Id) (Id :::@ Type' Id Id))
typeCheckType = foldTypeWithSelf' defTypeFold{TT.sty = sty}
                   where
                     sty (T t0') t0 = pure$ fmap T $ case t0 of
                        (Var ts s)     -> Var <$> T.sequence ts <*> typeCheckSymbol s
                        Forall n t s | Forall _ t' _ <- t0' -> Forall (n :::! t') <$> t <*> local (intuit %~ ((n :::. t'):)) (s)
                        Exists n t s | Exists _ t' _ <- t0' -> Exists (n :::! t') <$> t <*> local (intuit %~ ((n :::. t'):)) (s)
                        (Rec r n s)    -> Rec r (n :::! T TType) <$> local (intuit %~ ((n :::. T TType):)) (s)

                        (SizeT s)        -> SizeT <$> typeCheckSize s
                        (Array pol sz t) -> Array pol <$> typeCheckSize sz <*> t

                        TType      -> pure $ TType
                        (a :+: b) -> (:+:) <$> a <*> b
                        (a :*: b) -> (:*:) <$> a <*> b
                        (a :|: b) -> (:|:) <$> a <*> b
                        (a :&: b) -> (:&:) <$> a <*> b
                        (Bang t)  -> Bang <$> t
                        (Quest t) -> Quest <$> t
                        (MetaNeg t) -> MetaNeg <$> t
                        (RevNeg t) -> RevNeg <$> t
                        (Lollipop s t) -> Lollipop <$> s <*> t 
                        (Index b) -> pure $ Index b
                        (Primitive b n) -> pure $ Primitive b n
                        TSize           -> pure $ TSize
                        (Plupp t)       -> Plupp <$> t
                        (UnpushedNeg t) -> UnpushedNeg <$> t    
                        One             -> pure One
                        Bot             -> pure Bot
                        Prop            -> pure Prop
                        TProp (sz₁, op, sz₂) -> TProp <$> ((,op,) <$> typeCheckSize sz₁ <*> typeCheckSize sz₂)
                        Zero            -> pure Zero                                                            
                        Top             -> pure Top                                                            
                        _               -> throwError $ "typeCheckType:Unsupported type " ++ show  (T t0') ++ "."

oops :: String -> TypeCheckErrM a -- (SeqAnn1' Id Id)
oops = throwError

checkSizePred :: (IdSize,CmpOp,IdSize) -> ContM TypeCheckErrM (Maybe Bool)
checkSizePred p = liftTypeCheckNIA $ sizePredProve p

assertProveSizesEq :: IdSize -> [IdSize] -> ContM_ TypeCheckErrM 
assertProveSizesEq sz szs cont = checkSizePred (sz, Eq, sum szs) $ \p ->
    if p == Just True then cont
    else oops $ "Incompatible sizes " ++ show sz ++ " and " ++ show szs ++ "."

addSizePred :: (IdSize,CmpOp,IdSize) -> ContM_ TypeCheckErrM
addSizePred p = liftTypeCheckNIA_ $ sizePredAdd p

typeHasKind :: IdType ::: IdType -> ContM_ TypeCheckErrM
typeHasKind t cont = case t of
  T SizeT{} ::: T TSize   -> cont
  T Prop    ::: k@(T (TProp p)) -> checkSizePred p $ \res -> case res of
    Just True -> cont
    _         -> throwError $ "Type " ++ show k ++ " is not inhabited."
  _         ::: T TType       -> cont
  -- TODO: Refine this
  T (Var [] _) ::: T (Var [] _)      -> cont

typeCheckSymbol :: Symbol Id -> TypeCheckErrM (Symbol (Id :::@ Type' Id Id))
typeCheckSymbol symb = do
  cg <- view codeGen
  case symb of
    TVar pol ref -> fmap (TVar pol) $
      lookupIntuTy1 ref $ \ty'@(T ty) -> case ty of
        TSize -> OkType $ pure $ ref :::! ty'
        TType -> OkType $ pure $ ref :::! ty'
        TProp p -> OkType $ pure $ ref :::! ty'
        Var [] v → OkType $ pure $ ref :::! ty'
        _     -> WrongType [T TSize, T TType, T (TProp ("n",Le,"m"))]
    _ -> if cg then throwError $ "Unexpected non-variable symbol " ++ show symb ++ " in type."
               else pure $ (:::! T TType) <$> symb

typeCheckSize :: Size Id -> TypeCheckErrM (Size (Id :::@ Type' Id Id))
typeCheckSize sz = flip tr sz $ \s -> case s of
  TVar True ref -> fmap (TVar True) $ 
    lookupIntuTy1 ref $ \(T ty) -> case ty of
      TSize -> OkType $ pure $ ref :::! T TSize
      _     -> WrongType [T TSize]
  TVar False _  ->  throwError $ "Unexpected negative symbol " ++ show s ++ " in size " ++ show sz ++ "."
  _ -> throwError $ "Unexpected non-variable symbol " ++ show s ++ " in size " ++ show sz ++ "."

  where
    tr = $(genTransformBiM' [t| (Symbol Id -> TypeCheckErrM (Symbol Id)) -> 
                                 Size   Id -> TypeCheckErrM (Size Id) |])

checkVarTy, checkIntuTy :: (MonadError String m) => (Id :::@ Sized' Id Id) -> (Sized' Id Id -> StatusType (m a)) -> 
                      m a
checkVarTy  v = checkVarTy' Linear v
checkIntuTy v = checkVarTy' Intuit v

checkVarTy1, checkIntuTy1 :: (MonadError String m) => (Id :::@ Sized' Id Id) ->
              (Type' Id Id -> StatusType (m a)) -> 
              m a
checkVarTy1 v = checkVarTy1' Linear v
checkIntuTy1 v = checkVarTy1' Intuit v

checkVarTy1' :: (MonadError String m) => SeqCtxBehaviour ->
              (Id :::@ Sized' Id Id) ->
              (Type' Id Id -> StatusType (m a)) -> 
              m a
checkVarTy1' bhv z k =
  checkVarTy' bhv z $ \t -> case t of
    (ty :^  1) -> k ty
    (ty :^ sz) ->
      case k ty of
        OkType _   -> WrongSize 1
        s          -> s

checkVarTy' :: (MonadError String m) => SeqCtxBehaviour ->
              (Id :::@ Sized' Id Id) ->
              (Sized' Id Id -> StatusType (m a)) -> 
              m a
checkVarTy' bhv' (v ::: (bhv,ty)) κ | bhv' /= bhv = throwError $
  "Variable " ++ show v ++ " expected in " ++ show bhv' ++ ", found in " ++ show bhv ++ "."
checkVarTy' _    (v ::: (_,  ty)) κ = 
  case κ ty of
    OkType m -> m
    WrongType tys -> throwError $ "Wrong type for variable " ++ show v ++ " : " ++ show ty ++ ". Expected one of " ++ show tys ++ "."
    WrongSize sz  -> throwError $ "Wrong size for variable " ++ show v ++ " : " ++ show ty ++ ". Expected " ++ show sz ++ "."

lookupVar :: Id -> TypeCheckErrM (SeqCtxBehaviour, Sized' Id Id)
lookupVar v = do
  ctx <- ask
  ts <- view $ intuit . mapping tupled
  vs <- view $ linear . mapping tupled
  case (lookup v vs, lookup v ts) of
    (Just t, Nothing)  -> return (Linear,t)
    (Nothing, Just t)  -> return (Intuit,t)
    (Nothing, Nothing) ->
      throwError $ "Unknown variable : " ++ show v ++
                   "\n in context "      ++ ppShow ctx
    (Just _, Just _)   -> throwError $ "Variable " ++ show v ++ " found in both linear and intuitionistic contexts."

lookupIntuTy1 :: Id
              -> (Type' Id Id -> StatusType (TypeCheckErrM a))
              -> TypeCheckErrM a
lookupIntuTy1 r κ = lookupVar r >>= flip checkIntuTy1 κ . (r :::)

typeCheckSeq :: Seqn1 Id Id -> TypeCheckLogM Id Id (Seqn2 Id Id)
typeCheckSeq =
  view _4 . runIdentity . foldSeqn defSeqnFold{sseq,ssz,sty,stysz} . seqn1
  where
    -- ^ Assume that type variables are OK
{-
    ssz   _ = Identity . (\x -> x :: SizeAnn name ref)  . fmap  (::: KindTy)
    sty   _ = Identity . (\x -> x :: TypeAnn name ref)  . bimap (::: KindTy) (::: KindTy) 
    stysz _ = Identity . (\x -> x :: SizedAnn name ref) . bimap (::: KindTy) (::: KindTy)
-}
    ssz _ = Identity . (typeCheckSize >=> pure . fmap erase)
    sty _ = Identity . (typeCheckType >=> pure . bimap erase erase)
    stysz _ (ty :^ sz) = Identity $ bimap erase erase <$> ((:^) <$> typeCheckType ty <*> typeCheckSize sz)

    sseq :: [Id]
                        -> Seq''
                             ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckLogM Id Id (Seqn2 Id Id))
                             (TypeCheckErrM (Type' Id Id))
                             (TypeCheckErrM (Size Id))
                             (TypeCheckErrM (Sized' Id Id))
                             Id
                             Id
                        -> Identity
                             ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckLogM Id Id (Seqn2 Id Id))
    sseq refs seq = Identity . (refs,Right,1,) $ do
      () <- (return ()) :: (TypeCheckLogM Id Id ())
      ctx <- ask
    {-
      vs  <- view (vars   . mapping tupled)
      ts  <- view (tyvars . mapping tupled)
      vs_ <- view vars
      ts_ <- view tyvars
-}
      let 
        -- | Captures errors as a "What" sequent, annotates the monad
        annotate :: TypeCheckErrM (Seqn2'' Id Id)
                 -> TypeCheckLogM Id Id (Seqn2  Id Id)
        annotate seqm = Seqn2 <$> do
            _seqCtx <- view typeCheckCtx
            let _ann = _seqCtx
            _seq <- catchExceptT seqm $ \msg -> do
              tell $ msg:[]
              return $ What (Huh msg) [ElemLinear ref | ref <- _seqCtx ^. Type.Ctx.linear] []
            return $ Seqn {..}

        -- | Enqueue a transformation of the context to a sequent. This will
        --   take place after any previous transformations, and before the
        --   new names are added to the context.
        --
        --   Only names which
        modifyCtx :: (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)) ->
                     ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckErrM a) ->
                     ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckErrM a)
        modifyCtx f = _2 %~ (>=> f)

        -- | Demotes linear variables by a size, and keeps track of that in the sequent annotation.
        demoteVarsBy :: Size Id
                     -> ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckErrM a)
                     -> ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckErrM a)

        demoteVarsBy sz tup =
          tup
          & modifyCtx (Type.Ctx.linear %%~ demoteBy' sz)
          & _3 %~ (sz *)

        modifyM :: (TypeCheckErrM a -> TypeCheckErrM a) ->
                   ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckErrM a) ->
                   ([Id], (TypeCheckCtx Id Id -> Either String (TypeCheckCtx Id Id)), Size Id, TypeCheckErrM a)
        modifyM = (_4 %~) 

                  
        
        -- | For each continuation in the sequent node (i.e. each branch), run
        --   it after adding any names introduced by the current sequent node
        --   that are also mentioned in the list of refs for that continuation.
        -- | Adds information about the context to the variables
        withNames  p' = withNames' p
          where
          Identity p  = traverseSeq'' defSeqTraverse{tname} p'

          
          tname :: Id ::: Sized' Id Id -> Identity (Id :::@ Sized' Id Id)
          tname (v ::: tysz@(ty :^ sz)) | isKindType ty = return $ v :::! tysz
          tname (v ::: tysz)                            = return $ v :::~ tysz

        -- | Typesafe, faster variant of withNames for sequents which don't have names
        --   or continuations.
        closedSeq p = do
          traverseSeq'' defSeqTraverse{tref} p
          where
            tref  (v ::: (_,ty)) = pure (v ::: ty)

        continueSeq p = do
          traverseSeq'' defSeqTraverse{tref,tseq} p
          where
            tref  (v ::: (_,ty)) = pure (v ::: ty)
            tseq (refs, f, sz, m) = do
              -- 1) First, restrict ourselves to the references mentioned in this context.
              local (linear %~ (filter ((`elem` refs) . dropTag))) $ do
              -- 2) Then, transform the context (e.g. downsize it)
              ectx <- f <$> view typeCheckCtx
              case ectx of
                Left msg  -> oops msg
                Right ctx -> local (typeCheckCtx .~ ctx) m 

        withNames' p = do
          traverseSeq'' defSeqTraverse{tseq,tref,tname} p
          where
            names0 :: [Id :::@ Sized' Id Id]
            names0 = names0Seq p

            names0IntuVar = [v ::: ty | v :::! ty <- names0 ]
            names0VarAll  = [v ::: ty | v :::~ ty <- names0 ]

            tseq (refs, f, sz, m) = do
              let names0Var = filter ((`elem` refs) . dropTag) names0VarAll
              -- 1) First, restrict ourselves to the references mentioned in this context.
              local (linear %~ (filter ((`elem` refs) . dropTag))) $ do
              -- 2) Then, transform the context (e.g. downsize it)
              ectx <- f <$> view typeCheckCtx
              case ectx of
                Left msg  -> oops msg
                Right ctx ->
                  local (typeCheckCtx .~ ctx) $ 
                  -- 3) Finally, add the new names
                  local (linear %~ (++ names0Var)) $
                  local (intuit %~ (++ names0IntuVar)) $
                  m 
            tref  (v ::: (_,ty)) = pure (v ::: ty)
            tname (v ::: (_,ty)) = pure (v ::: ty)

        assertTypeEq' :: forall a. (Show a) => (a -> a -> Bool) -> a -> a -> ContM TypeCheckErrM a
        assertTypeEq' pred t₁ t₂ cont =
          pureContM (t₁ `pred` t₂) $ \b ->
            if b then cont t₁
              else oops $ "Incompatible types " ++ ppShow t₁ ++ " and " ++ ppShow t₂ ++ "."

        assertTypeDuals' :: forall a. (Show a, Dual a) => (a -> a -> Bool) -> a -> a -> ContM TypeCheckErrM a
        assertTypeDuals' pred = assertTypeEq' (\x y -> x `pred` dual y)

        eqVarSized :: (a :::@ Sized' Id Id) -> (b :::@ Sized' Id Id) -> Bool        
        eqVarSized (_ ::: (bhv₁, tysz₁)) (_ ::: (bhv₂, tysz₂)) = eqSized tysz₁ tysz₂
        
        assertTypeEq        t₁ t₂ = assertTypeEq' eqType t₁ t₂
        assertSizedEq       t₁ t₂ = assertTypeEq' eqSized t₁ t₂
        assertVarSizedEq :: (Show a) => (a :::@ Sized' Id Id) -> (a :::@ Sized' Id Id) -> ContM_ TypeCheckErrM 
        assertVarSizedEq    v₁ v₂ = voidContM$ assertTypeEq' eqVarSized v₁ v₂ 

        assertTypeDuals :: Type' Id Id -> Type' Id Id -> ContM TypeCheckErrM (Type' Id Id)
        assertTypeDuals     t₁ t₂ = assertTypeDuals' eqType t₁ t₂
        assertSizedDuals :: Sized' Id Id -> Sized' Id Id -> ContM TypeCheckErrM (Sized' Id Id)
        assertSizedDuals    t₁ t₂ = assertTypeDuals' eqSized t₁ t₂
        assertVarSizedDuals :: (Show a) => (a :::@ Sized' Id Id) -> (a :::@ Sized' Id Id) -> ContM_ TypeCheckErrM 
        assertVarSizedDuals v₁ v₂ = voidContM$ assertTypeDuals' eqVarSized v₁ v₂ 

        typeCheckCtx'' ctx = [(name ::: tysz, tysz) | (name,tysz) <- ctx]
   
        liftConts seq = 
          fmapSeq'' defSeqFmap{fseq} seq
            where
            fseq :: (a,b,sz,TypeCheckLogM Id Id c) -> (a,b,sz,TypeCheckErrM c)
            fseq = (_4 %~ lift)

        unwrapSeqn2 seq =
          fmapSeq'' defSeqFmap{fseq} seq
            where
            fseq :: (Functor f) => (a,b,sz,f (Seqn2 Id Id)) -> (a,b,sz,f (Seqn2' Id Id))
            fseq = (_4 %~ fmap seqn2)

        unwrapTypeSize =
          traverseSeq'' defSeqTraverse{tty,tsz,ttysz}
            where
              tty   = id
              tsz   = id
              ttysz = id


        -- | Annotate each reference in the current sequent node with it's type
        --   in the current context. Also, it lifts the continuations into the
        --   error monad.
        lookupRefs seq = do
          traverseSeq'' defSeqTraverse{tref} seq

          where
            tref :: Id -> TypeCheckErrM (Id :::@ Sized' Id Id)
            tref ref = (ref :::) <$> lookupVar ref 

      annotate $ pure seq 
                     >>= pure . liftConts 
                     >>= unwrapTypeSize
                     >>= pure . unwrapSeqn2 
                     >>= lookupRefs 
                     >>= \seq' -> case seq' of
          Ax {..} ->
               checkVarTy1 wElim $ \vt  -> OkType $ 
               checkVarTy1 zElim $ \vt' -> OkType $
               vt `assertTypeEq` neg vt' $ pure $
               closedSeq $ Ax { .. }

          NCut{ncut_cs = ncut_cs',
               ncut_left = ncut_left',
               ncut_right = ncut_right',
               ..} -> do
            let ncut_cs = [(v₁ ::: dual vt :^ szL, v₂ ::: vt :^ szR, vt) | (v₁,v₂,vt) <- ncut_cs'] 
            let ncut_left  = demL ncut_left'
            let ncut_right = demR ncut_right'

            withNames $ NCut{..}
            where
              dem = demoteVarsBy ncut_size
              (szL,szR,demL,demR) = case ncut_fanning of FanLeft  -> (1,ncut_size,dem,id);
                                                         FanRight -> (ncut_size,1,id,dem)

-- If there's a single flow of control, it shouldn't be possible to convert
-- (A :*: B)^n -> A^n :*: B^n 
-- However, if this is introduced via a cut, and is eliminated, it will
-- actually work.

          Cross {..} -> 
            checkVarTy1 zElim $ \t ->
            case t of
              T (vt :*: vt') -> OkType $ do
                withNames $ Cross { 
                  xIntr = xIntr :::. vt
                 ,yIntr = yIntr :::. vt'
                 , ..}
              _ -> WrongType [T$ meta "A" :*: meta "B"]

          Par {contL = contL', contR = contR', ..} ->
             checkVarTy zElim $ \t -> case t of
               (T (vt :|: vt')):^sz -> OkType $ do
                 let contL = demoteVarsBy sz contL'
                 let contR = contR'
                 withNames $ Par {
                   xIntr = xIntr ::: vt  :^ 1
                  ,yIntr = yIntr ::: vt' :^ sz
                  , .. }
               _ -> WrongType [T$ meta "A" :|: "B"]

          Halt refs -> closedSeq $ Halt refs
  
          Sync{syncIntr = syncIntr',..} -> do

            syncIntr <- T.forM syncIntr' $ \(syncSz, syncTy, xIntr', upd', rd') -> do
              dt <- mkData syncTy
              let xIntr = xIntr' ::: dataWrite dt :^ syncSz

              p <- freshPolVar

              rd <- T.forM rd' $ \SyncRead{syncYIntr = syncYIntr',..} -> do
                let syncYIntr = syncYIntr' ::: dataRead dt :^ syncSz * syncCopies
                return SyncRead{..}

              upd <- T.forM upd' $ \SyncUpdate{syncWIntr = syncWIntr',..} -> do
                let syncWIntr = syncWIntr' ::: T (ArraySeqPol p syncSteps (dataUpdate dt)) :^ syncSz
                return SyncUpdate{..}

              return (syncSz, syncTy, xIntr, upd, rd)
 
            withNames $ Sync{..}
          
          Plus{..} -> 
            checkVarTy1 zElim $ \t ->
            case t of
              (T (vt :+: vt')) -> OkType $ do
                withNames $ Plus { inl = inl { branchName = branchName inl ::: vt  :^ 1 },
                                   inr = inr { branchName = branchName inr ::: vt' :^ 1 },
                                   .. } 
              _ -> WrongType [T$ meta "A" :+: meta "B"]

          With{xIntr = xIntr',..} -> 
            checkVarTy zElim $ \t ->
            case t of
              (T (vt :&: vt')):^sz -> OkType $ do
                  let xIntr = xIntr' ::: choice choose (vt :^ sz) (vt' :^ sz)
                  withNames $ With{..}
              _ -> WrongType  [T$ meta "A" :&: meta "B"]

          SOne{..} -> 
            checkVarTy zElim $ \t -> case t of
              (T One) :^ sz -> OkType $ withNames SOne{..}
              _         -> WrongType [T One]
          
          SBot{..} -> 
            checkVarTy1 zElim $ \t -> case t of
              (T Bot) -> OkType $ closedSeq $ SBot{..}
              _   -> WrongType [T Bot]

          SZero{..} -> 
            checkVarTy1 zElim $ \t -> case t of 
              (T Zero) -> OkType $ closedSeq $ SZero{..}
              _    -> WrongType [T Zero]

          What{..} -> closedSeq $ What{..}
          
          TApp{xIntr = xIntr',..} ->
            checkVarTy1 zElim $ \t -> case t of
              (T (Forall tv tvk tyA)) -> OkType $ do
               typeHasKind (ty ::: tvk) $ do
                    xIntrTy <- tySubst tv ty tyA
                    withNames $ TApp {
                      xIntr = xIntr' ::: xIntrTy :^ 1
                     ,..}
              _ -> WrongType [T$ Forall "x" (meta "B") (meta "C")]

          TUnpack{tyIntr = tyIntr', xIntr = xIntr', ..} ->
            checkVarTy1 zElim $ \t -> case t of
              (T (Exists tw_old ttw tyA)) -> OkType $ do
                let new_ty = tyRename tw_old tyIntr' tyA
                let mod = case ttw of
                      T (TProp p) -> addSizePred p
                      _           -> id

                mod$ withNames'$ TUnpack{
                      tyIntr = tyIntr' :::!  ttw    :^ 1
                     ,xIntr  = xIntr'  :::~  new_ty :^ 1
                     ,..}
                --cont
              _ -> WrongType [T$ Exists "v" (meta "A") (meta "B")]
          
          Offer{xIntr = xIntr', ..} ->
            checkVarTy1 zElim $ \t -> case t of
              (T (Quest tyA)) -> OkType $ do
                withNames $ Offer{
                  xIntr = xIntr' ::: tyA :^ 1
                 ,..}
              _ -> WrongType [T$ Quest (meta "A")]
             
          Demand{xIntr = xIntr', ..} ->
            checkVarTy zElim $ \t -> case t of
              (T (Bang tyA)) :^ sz -> OkType $ do
                withNames $ Demand{
                  xIntr = xIntr' ::: tyA :^ sz
                 ,..}
              _ -> WrongType [T$ Bang (meta "A")]

          Load{xIntr = xIntr', ..} ->
            checkIntuTy1 zLoad $ \ty -> OkType $ do
              withNames' $ Load{xIntr = xIntr' :::~ ty :^ loadSize, ..}

          Save{xSave = xSave', ..} ->
            checkVarTy1 zElim $ \t -> case t of
              (T (Bang tyA)) -> OkType $ do
                withNames' $ Save{xSave = xSave' :::! tyA :^ 1, ..}
              _ -> WrongType [T$ Bang (meta "A")]
          
          BigPar{branches = branches', ..} ->
            checkVarTy1 zElim $ \t -> case t of
              T (ArrayPar sz tyA) -> OkType $ do
                let branches = [ (sz, Branch{..})
                               | (sz, Branch branchName' branchProg') <- branches',
                               let branchName = branchName' ::: tyA :^ 1,
                               let branchProg = demoteVarsBy sz branchProg',
                               not $ sz == 0 ]
                sz `assertProveSizesEq` [sz | (sz,_) <- branches] $ 
                  withNames BigPar{..} 
              _               -> WrongType [T$ ArrayPar (meta "n") (meta "A")]

          
          BigTensor{xIntr = xIntr', ..} ->
            checkVarTy1 zElim $ \t -> case t of
              T (ArrayTensor sz tyA) -> OkType $ 
                withNames $ BigTensor{xIntr = xIntr' ::: tyA :^ sz, ..}
              _                  -> WrongType [T$ ArrayTensor (meta "n") (meta "A")]
              
          
          Split{xIntr = xIntr', yIntr = yIntr', ..} -> 
            checkVarTy zElim $ \(ty :^ sz) -> OkType $ do
               let sz₁ = splitSz
               let sz₂ = sz - sz₁ 
               withNames $ 
                 Split{ xIntr = xIntr' ::: ty :^ sz₁
                       , yIntr = yIntr' ::: ty :^ sz₂
                       , .. } 

          Merge{xIntr = xIntr', ..} -> 
            checkVarTy zElim $ \(tz :^ sz₁) -> OkType $
            checkVarTy wElim $ \(tw :^ sz₂) -> OkType $ do
            tz `assertTypeEq` tw $ \ty ->
              sz₁ `assertProveSizesEq` [splitSz] $
                withNames $ Merge{ xIntr = xIntr' ::: ty :^ (sz₁ + sz₂), ..} 

          Permute{xIntr = xIntr', ..} -> 
            checkVarTy zElim $ \(ty :^ oldSz) -> OkType $
              -- There is an implicit assertion _oldSz == newSz
              assertProveSizesEq newSz [oldSz] $
                withNames $ Permute{ xIntr = xIntr' ::: ty :^ newSz, ..}

          Foldmap{..} -> $(todo "Foldmap") 

          Foldmap2{fmUnitTrg = fmUnitTrg'
                   ,fmMapTrg  = fmMapTrg'
                   ,fmMixL    = fmMixL'
                   ,fmMixR    = fmMixR'
                   ,fmMixTrg  = fmMixTrg'
                   ,fmMon     = fmMon'
                   ,fmMap     = fmMap'
                   ,..} -> 
            do
            
              withNames $ 
                Foldmap2 {fmUnitTrg = fmUnitTrg' ::: dual fmTyM :^ 1
                         ,fmMapTrg  = fmMapTrg'  ::: dual fmTyM :^ 1
                         ,fmMixL    = fmMixL'    :::      fmTyM :^ 1
                         ,fmMixR    = fmMixR'    :::      fmTyM :^ 1
                         ,fmMixTrg  = fmMixTrg'  ::: dual fmTyM :^ 1
                         ,fmMon     = fmMon'     :::      fmTyM :^ 1
                         ,fmMap     = demoteVarsBy fmSize fmMap'
                         ,..}
              
          BigSeq fence' -> do
            -- 1. Each sequence must be mentioned at mostonce per interval 
            T.sequence [case refs \\ nub refs of
                        []   -> return ()
                        dupl -> oops $ "Sequences " ++ intercalate ", " [show r | r <- dupl] ++ 
                                       " referenced more than once in the same interval."
                     | t <- fence'
                     , let refs = seqStepRefs t]

            -- 2. Check that the sum of all chunks equals the number of sequents
            res <- sequenceContM_ <$> T.sequence [let checkSz x = return (assertProveSizesEq x (seqStepSz0 `map` step)) in
                        checkVarTy (the ref) $ \t -> case t of
                            T (ArraySeq totalSeqSz _) :^ 1 -> OkType$ checkSz totalSeqSz 
                            T (ArraySeq 0 _)          :^ n -> OkType$ checkSz 0
                            T (Pole)                  :^ n -> OkType$ checkSz 0
                            _                              -> WrongType [T$ Array ANeutralU (meta "n") (meta "A"), T (Unit ANeutralU)]
                     | step <- fence'
                     , ref  <- seqStepRefs step
                     , then group by ref using groupWith] 

            res $ do

                -- 3. Construct the final sequent.
                fence <- T.forM fence' $ \step -> case step of
                  SeqStep{..} -> do
                    return   SeqStep{ seqProg = demoteVarsBy seqSz seqProg
                                    , seqChunk = [ (zElim, xIntr ::: tyA :^ 1)
                                                 | (zElim@(_ :::~ T (ArraySeq _sz tyA) :^ 1), xIntr) <- seqChunk] 
                                    , ..}

                withNames $ BigSeq fence

          SizeCmp{bCmp = bCmp',..} -> do
            let globalOp = foldl joinCmp CmpFalse [o | (o,_) <- bCmp']
            checkSizePred (sz1, globalOp, sz2) $ \t -> case t of 
                Just True -> do
                  let bCmp = [(op, modifyM (liftTypeCheckNIA_ (sizePredAdd (sz1,op,sz2))) p) | (op, p) <- bCmp']
                  continueSeq SizeCmp{..}
                _ -> oops$ "Case " ++ show sz1 ++ " " ++ show (negCmp globalOp) ++ " " ++ show sz2 ++ " not covered."

            

          
          
instance (name ~ ref) => Erase (Seqn2 name ref) (Seqn1 name ref) where
  erase (Seqn2 Seqn{_ann,_seq}) =
    Seqn1 (Seqn (map erase $ _ann ^. Type.Ctx.linear)
                (fmapSeq'' defSeqFmap{fseq,fname,fref} _seq))
    where
      fseq  = seqn1 . erase . Seqn2
      fname = erase
      fref  = erase 

instance (name ~ ref) => Erase (Deriv2 name ref) (Deriv1 name ref) where
  erase = D1 . (derivSequent %~ erase) . deriv2



analyseDeriv :: Deriv0 Id Id -> Deriv2 Id Id
analyseDeriv =
  (checkCtxDeriv &&& id)
  >>> (\(res, d) -> either (\msg -> error (msg {- ++ "\n" ++ ppShow d -})) id res)
  >>> (typeCheckDeriv &&& id) >>> (\((log,v), orig) -> case log of
                                      [] -> v
                                      _  -> error $ intercalate "\n" (log)) -- ++ [ppShow orig]))

