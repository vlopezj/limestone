{-# LANGUAGE TransformListComp #-}
-- | Naive propositional solver. This should be replaced by a SAT solver
module Prop where

import Data.Maybe
import Data.List
import Data.Function
import Control.Monad
import GHC.Exts

-- Disjunctive normal form; falsehood is obvious in this case
type Conj a = [(a,Bool)]
type Prop a = [Conj a]

pTrue, pFalse :: Prop a
pTrue  = [[]] -- empty and
pFalse = []   -- empty or

pBool True = pTrue
pBool False = pFalse

lit   a = [[(a,True)]]
neg   a = [[(a,False)]]

solve :: Prop a -> Maybe Bool
solve [[]] = Just True 
solve []   = Just False
solve _    = Nothing

-- | This should simplify the proposition, even if it can't solve it
solve' :: (Prop a -> b) -> Prop a -> Either b Bool
solve' f x = maybe (Left (f x)) Right $ solve x

pOr :: (Ord a) => Prop a -> Prop a -> Prop a
pOr p₁ p₂ = subsumeAll $ p₁ ++ p₂

pAnd :: (Ord a) => Prop a -> Prop a -> Prop a
pAnd cs₁ cs₂ = subsumeAll $ [ c | c₁ <- cs₁, c₂ <- cs₂, c <- maybeToList $ c₁ `cAnd` c₂]

pOrs :: (Ord a) => [Prop a] -> Prop a
pOrs = nub . concat

pAnds :: (Ord a) => [Prop a] -> Prop a
pAnds ps = foldl pAnd pTrue ps

cAnd :: (Ord a) => Conj a -> Conj a -> Maybe (Conj a)
x `cAnd` y = forM [(the p, nub sign) |
                   (p, sign) <- x ++ y 
                  ,then group by p using groupWith] $ \(p, signs) -> 
                     case signs of
                       []  -> error "Impossible" 
                       [s] -> Just (p,s)
                       _   -> Nothing 

-- | Removes redundant propositions (e.g. a -> a ∧ b, so a ∧ b should be removed if a is present)
subsumeAll :: (Ord a) => Prop a -> Prop a 
subsumeAll = go [] . sortBy (compare `on` length)
  where
    go seen [] = seen
    go seen (x:xs) = if any (`implies` x) seen
                     then go seen     xs
                     else go (x:(filter (not . (x `implies`)) seen)) xs 

implies :: (Eq a) => Conj a -> Conj a -> Bool
implies xs ys = all (`elem` ys) xs

pNot :: (Ord a) => Prop a -> Prop a
pNot prop = pAnds [pOrs [[[(a, not s)]] |
                      (a,s) <- conj]
                  | conj <- prop ]

data PropSyntax a = STrue
                  | SFalse
                  | SAnd (PropSyntax a) (PropSyntax a)
                  | SOr  (PropSyntax a) (PropSyntax a)
                  | SLit a
                  | SNeg (PropSyntax a)

syntax :: Prop a -> PropSyntax a
syntax = go
         where
        go p = case p of
          [] -> SFalse
          cs -> foldl1 SOr [
            case c of
              [] -> STrue
              vs -> foldl1 SAnd [
                case s of
                  True -> SLit a
                  False -> SNeg (SLit a)
                | (a,s) <- vs
                ]
            | c <- cs]
