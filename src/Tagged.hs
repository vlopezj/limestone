{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
module Tagged where

import Data.Function
import Control.Lens
import Data.Bifunctor
import Data.Foldable
import Data.Traversable

data a ::: b = a ::: b deriving (Show, Read)

dropTag :: (a ::: b) -> a
dropTag (a ::: _) = a

dropValue :: (a ::: b) -> b
dropValue (_ ::: b) = b
  
infix 3 :::

instance Bifunctor (:::) where bimap f g (a ::: b) = f a ::: g b

instance Eq a => Eq (a ::: b) where (==) = (==) `on` dropTag
instance Ord a => Ord (a ::: b) where compare = compare `on` dropTag

type Tagged = (:::)

pattern Tagged a b = (:::) a b

class Erase a b | a -> b where
  erase :: a -> b

instance Erase (a ::: b) a where
  erase (x ::: y) = x

toTuple :: a ::: b -> (a,b)
toTuple (a ::: b) = (a,b)

fromTuple :: (a,b) -> a ::: b
fromTuple (a,b) = a ::: b

tupled :: Simple Iso (a ::: b) (a,b)
tupled = iso toTuple fromTuple

tag :: Simple Lens (a ::: b) b
tag f (a ::: b) =  (a :::) `fmap` f b 

value :: Simple Lens (a ::: b) a
value f (a ::: b) =  (::: b) `fmap` f a 

-- Erase law: For any two objects in the program, if erase a ~ erase b, then a ~ b

data NoShow a = NoShow { ns :: a } deriving (Eq,Functor,Traversable,Foldable)

noShow = NoShow undefined

instance Show (NoShow a) where
  showsPrec _ (NoShow _) = ("noShow" ++)


