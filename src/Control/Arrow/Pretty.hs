-- | Laying-out Kleisli arrow pipelines in a more readable way
module Control.Arrow.Pretty(module Control.Arrow
                           ,module Control.Arrow.Pretty) where

import Control.Arrow
import Data.Traversable as T

k  f = Kleisli f
a ─╼ b = (a >>> (b &&& arr id)) >>> arr snd
a ── b = a >>> b 
a ╰─ b = a ── b 
a ╰╼ b = a ─╼ b 
a ├╼ b = a ─╼ b 
infixl 1 ─╼ 
infixl 1 ──
infixl 1 ╰─
infixl 1 ╰╼ 

forEach :: (Monad m, Traversable t) => Kleisli m a b -> Kleisli m (t a) (t b) 
forEach (Kleisli f) = Kleisli (T.mapM f)
