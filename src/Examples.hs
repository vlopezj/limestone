{-# OPTIONS_GHC -w #-}
{-# LANGUAGE TupleSections #-}
module Examples where

import ILL
import PrettyId
import Data.List (nub)
import Fresh
import Control.Applicative  
import Eval
import Lint
import Text.Show.Pretty hiding (Name)


-- FFT Implementation

swapType :: Type' Name Name
swapType = Forall "α" Type $ Forall "β" Type ((a :*: b) ⊸ (b :*: a))
  where
    a,b :: Type' Name Name
    a = var "α"
    b = var "β"

type NameDeriv = Deriv' Name Name


-- zipWith :: (a ⊗ b ⊸ c) → ⨂a ⊸ ⨂b ⊸ ⨂c
zipWith' = tyLam index $ \n ->
           tyLam Type $ \a ->
           tyLam Type $ \b ->
           tyLam Type $ \c ->
           tyLam (tref a ⊗ tref b ⊸ tref c) $ \f ->
           lam (Array Positive (tref n) (tref a)) $ \xs ->
           lam (Array Positive (tref n) (tref b)) $ \ys ->
           mapTerm' `tyApp` tref f `app` (zipTerm' `tyApp` tref a `tyApp` tref b `app` ref xs `app` ref ys)

-- map :: (a ⊸ b) → ⨂n a ⊸ ⨂n b
mapTerm' = tyLam index $ \n ->
           tyLam Type $ \a -> 
           tyLam Type $ \b -> 
           tyLam (tref a ⊸ tref b) $ \f -> 
           lam (Array Positive (tref n) (tref a)) $ \xs -> 
           parallel (tref n) [xs]$ \ [x] -> -- [load f `app` ref x | x <- xs]  
           load f `app` ref x

-- halve :: ⨂(2n)a ⊸ ⨂n a ⊗ ⨂n a
halveTerm' = tyLam index $ \n ->
             tyLam Type $ \a -> 
             let n2 = sizeMul (sizeLit 2) (tref n) in
             lam (Array Positive n2 $ tref a) $ \xs ->
             ILL.split $ ref xs

-- zip :: ⨂a ⊸ ⨂b ⊸ ⨂(a ⊗ b)
zipTerm' = tyLam index $ \n ->
           tyLam Type $ \a ->
           tyLam Type $ \b ->
           lam (Array Positive (tref n) (tref a)) $ \xs ->
           lam (Array Positive (tref n) (tref b)) $ \ys ->
           parallel (tref n) [xs,ys] $ \[x,y] ->
           pair (ref x) (ref y)

-- zip :: ⨂a ⊗ ⨂b ⊸ ⨂(a ⊗ b)
zipTerm'' = tyLam index $ \n ->
            tyLam Type $ \a ->
            tyLam Type $ \b ->
            lam (Array Positive (tref n) (tref a) ⊗ Array Positive (tref n) (tref b)) $ \xs_ys ->
            splitPair (ref xs_ys) $ \xs ys ->
            parallel (tref n) [xs,ys] $ \[x,y] ->
            pair (ref x) (ref y)

-- unhalve :: ⅋n (a ⅋ a) ⊸ ⅋(2n) a
unhalveTerm' = tyLam index $ \n ->
               tyLam Type $ \a ->
               lam (Array Negative (tref n) (tref a :|: tref a)) $ \xs ->
               unhalve' (ref xs)

scheduleTerm = tyLam index $ \n ->
               tyLam Type $ \a ->
               autoSchedule (tref a) n
               
-- ∀ c n . ((c ⊗ c) ⊸ (c ⅋ c)) ⊸ ⨂(2n)(c ⊗ c)  ⊸ ⨂(2n)(c ⊗ c)
fftStepNext
        = tyLam Type $ \c ->
          tyLam index $ \n ->
          let szExp :: IdType
              szExp = SizeExp $ tref n  in
          tyLam (((tref c) ⊗ (tref c)) ⊸ ((tref c) ⅋ (tref c))) $ \bff ->
          (freezeTerm `tyApp` (sizeMul (sizeLit 2) szExp) `tyApp` tref c)
            `o`
          (unhalveTerm' `tyApp` szExp `tyApp` tref c)
            `o`
          (scheduleTerm `tyApp` (tref n) `tyApp` ((tref c) ⅋ (tref c)))
            `o`
          (mapTerm' `tyApp` szExp `tyApp` ((tref c) ⊗ (tref c)) `tyApp`  ((tref c) ⅋ (tref c)) `tyApp` tref bff)
            `o`
          (zipTerm'' `tyApp` szExp `tyApp` (tref c) `tyApp` (tref c))
            `o`
          (splitTerm `tyApp` (sizeLit 1) `tyApp` (sizeLit 1) `tyApp` szExp `tyApp` (tref c))

-- ∀ c n . ((c ⊗ c) ⊸ (c ⅋ c)) ⊸ ⨂(2ⁿ)(c ⊗ c)  ⊸ ⨂(2ⁿ)(c ⊗ c)
{-
fft =
  tyLam Type $ \c ->
  tyLam index $ \n ->
  let szExp :: IdType
      szExp = SizeExp $ tref n  in
  tyLam (((tref c) ⊗ (tref c)) ⊸ ((tref c) ⅋ (tref c))) $ \bff ->
  -}

-- ⊗(i+j)nA ⊸ (⊗inA  ⊗  ⊗jnA)
splitTerm = tyLam index $ \i ->
            tyLam index $ \j ->
            tyLam index $ \n ->
            tyLam Type  $ \a ->
            lam (Array Positive (sizeMul (sizeAdd (tref i) (tref j)) (tref n)) (tref a)) $ \arr ->
            ILL.split (ref arr)

tileTerm =
  tyLam index $ \n ->
  tile n

freezeTerm =
  tyLam index $ \n ->
  tyLam Type $ \a ->
  lam (Array Negative (tref n) (tref a)) $ \xs ->
  freeze (ref xs)

-- Bitonic sorting networks

halfCleaner
        = tyLam Type $ \c ->
          tyLam index $ \n ->
          let szExp :: IdType
              szExp = SizeExp $ tref n  in
          tyLam (((tref c) ⊗ (tref c)) ⊸ ((tref c) ⅋ (tref c))) $ \swap ->
          (freezeTerm `tyApp` (sizeMul (sizeLit 2) szExp) `tyApp` tref c)
            `o`
          (unhalveTerm' `tyApp` szExp `tyApp` tref c)
            `o`
          (scheduleTerm `tyApp` (tref n) `tyApp` ((tref c) ⅋ (tref c)))
            `o`
          (mapTerm' `tyApp` szExp `tyApp` ((tref c) ⊗ (tref c)) `tyApp`  ((tref c) ⅋ (tref c)) `tyApp` tref swap)
            `o`
          (zipTerm'' `tyApp` szExp `tyApp` (tref c) `tyApp` (tref c))
            `o`
          (splitTerm `tyApp` (sizeLit 1) `tyApp` (sizeLit 1) `tyApp` szExp `tyApp` (tref c))

-- Misc terms

compose = tyLam Type $ \a ->
          tyLam Type $ \b ->
          tyLam Type $ \c ->
          lam (tref b ⊸ tref c) $ \f ->
          lam (tref a ⊸ tref b) $ \g ->
          lam (tref a) $ \x ->
          app (ref f) (app (ref g) (ref x))

apa =
  tyLam index $ \n ->
  tyLam Type $ \a ->
  lam (Array Negative (tref n) (tref a)) $ \xs ->
  freezeTerm `tyApp` (tref n) `tyApp` (tref a) `app` (idT `tyApp` (Array Negative (tref n) (tref a)) `app` (ref xs))

idT = tyLam Type $ \a -> lam (tref a) $ \aa -> (ref aa)

-- bepa = fromNames $
--        Deriv "" [("n",Index False),("A",Type)] [("a",Array Negative (var "n") (var "A")),("b", Array Negative (var "n") (neg (var "A")))] $
--        cut1 "u" "v" One (SBot "u") $
--        Freeze (var "n") (var "A") "a'" "b'" (SOne False "v" (Ax dum "a" "a'")) (Ax dum "b" "b'")

-- Useful functions for printing and evaluating derivations and term

showTerm = putStrLn . showDeriv . mkDeriv

showEvalTerm evalSteps = putStrLn . showDeriv . ideval evalSteps . mkDeriv

printEvalTerm evalSteps = putStrLn . ppShow . ideval evalSteps . mkDeriv

showLintEvalTerm evalSteps = putStrLn . showDeriv . lint . ideval evalSteps . lint . mkDeriv

lint term = either error (const term) (lintDeriv term)

-- Derivations written without ILL

{-
fftStep :: Deriv' Name Name
fftStep = Deriv "fftStep" [("n",index),("A",Type)] 
            [("in", Array Positive n2 a),
             ("out",Array Positive n2 b),
             ("schedule",Array Negative n One)] $
          TensorMul "in'" "in" $ 
          TensorMul "out'" "out" $ 
          TensorAdd "loIn" "hiIn" "in'" $
          TensorAdd "loOut" "hiOut" "out'" $
          Distribute "dum" "schedule" $ 
          SOne False "dum" $ 
          What "s" ["in'","out'"] []
  where 
    n2 = sizeMul (sizeLit 2) n
    n = sizeVar "n"
    a = var "A"
    b = dual $ var "A"
-}

-- mapTerm :: NameDeriv
-- mapTerm = Deriv "map" [] 
--                       [("map",neg (Forall "n" index $ Forall "A" Type $ Forall "B" Type $ Forall "f" (a ⊸ b) $ Array Positive n a ⊸ Array Positive n b))] $
--           TUnpack "n" "fun1" "map" $
--           TUnpack "A" "fun2" "fun1" $
--           TUnpack "B" "fun3" "fun2" $
--           TUnpack "f" "fun4" "fun3" $
--           Cross False dum "xs" "ys" "fun4" $
--           Distribute "y" "ys" $  
--           Load "f'" "f" $
--           Par False dum "a" "b" "f'"   (ax "a" "xs") (ax "b" "ys")
--   where 
--     n = sizeVar "n"
--     a = var "A"
--     b = var "B"
   
-- halveTerm :: NameDeriv
-- halveTerm = Deriv "halve" [("n",index),("A",Type)]
--             [("xs",Array Positive n2 a),
--              ("ys", neg (Array Positive n a ⊗ Array Positive n a))] $
--             TensorAdd "xs1" "xs2" "xs" $ 
--             Par False dum "ys1" "ys2" "ys" 
--             (ax "xs1" "ys1") (ax "xs2" "ys2")
--   where 
--     n2 = sizeMul (sizeLit 2) n
--     n = sizeVar "n"
--     a = var "A"

-- zipTerm :: NameDeriv
-- zipTerm = Deriv "zip" [("n",index),("A",Type),("B",Type)] 
--           [("as",Array Positive n a),
--            ("bs",Array Positive n b),
--            ("cs",neg $ Array Positive n (a ⊗ b))]  $ 
--           Distribute "c" "cs" $
--           Par False dum "a" "b" "c" 
--           (ax "a" "as")(ax "b" "bs")
--   where 
--     n = sizeVar "n"
--     a = var "A"
--     b = var "B"


-- fftStep :: Deriv' Name Name
-- fftStep = Deriv "fftStep" [("n",index),("A",Type)] 
--             [("in", Array Positive n2 a),
--              ("out",Array Positive n2 b),
--              ("schedule",Array Negative n One)] $
--           TensorMul 0 "in'" "in" $ -- Fix the index
--           TensorMul 0 "out'" "out" $ -- Fix the index
--           TensorAdd "loIn" "hiIn" "in'" $
--           TensorAdd "loOut" "hiOut" "out'" $
--           Distribute "dum" "schedule" $ 
--           SOne False "dum" $ 
--           What "s" ["in'","out'"] []
--   where 
--     n2 = sizeMul (sizeLit 2) n
--     n = sizeVar "n"
--     a = var "A"
--     b = dual $ var "A"
-- putDeriv = putStrLn . showDeriv . fromNames
-- fftStepPr = putDeriv $ fftStep
-- ax = Ax dum

{-
swap :: Deriv' Name Name
swap = Deriv "swap" [] [("swapee",neg $ swapType)]
   $ TUnpack "α" "swapee'" "swapee"
   $ TUnpack "β" "swapee''" "swapee'"
   $ Cross False dum "input" "output" 0
   $ Cross False dum "a_in" "b_in" 0
   $ Par False dum "b_out" "a_out" 1 ax ax
-}


{-
t0 = Forall "α" $ Forall "β" $ (a :*: b) ⊸ (b :*: a)
  where a = var 1
        b = var 0


ctx = [("x",var 0 :*: var 1),("y",var 0 ⊸ TVar False 1)]

s0 = Deriv ["a","b"] ctx $
       Cross False dum "v" "w" 0 $
       Exchange [0,2,1] $
       par "x" "y" 1 ax
             ax
bool = One :+: One

s1 = Deriv [] [("x",bool), ("y",neg (bool :*: bool))] $
       Plus "m" "n" 0 (SOne False 0 $ Par False dum "a" "b" 0 (With False dum "c" True  0 SBot) (With False dum "d" True  0 SBot))
              (SOne False 0 $ Par False dum "a" "b" 0 (With False dum "c" False 0 SBot) (With False dum "d" False 0 SBot))

test = Deriv [] [("x",neg t0)]  $
       TUnpack "y" 0 $
       TUnpack "z" 0 $
       Cross False dum "m" "n" 0 $
       Cross False dum "o" "p" 0 $
       Exchange [1,2,0] $
       Par False dum "a" "b" 1 ax ax

par = Par False dum

-- | Test exponentials
expTest = Deriv ["α"] [("x",neg (Bang (var 0) ⊸ (Bang (var 0) :*: Bang (var 0))))] $
          Cross False dum "y" "z" 0 $
          Alias False 0 "w" $
          Exchange [0,2,1] $
          par "a" "b" 1 ax ax

-- | Test exchange
exchTest = Deriv ["a","b","c"] [("x",var 0),("y",One),("z",neg (var 0))] $
           Exchange [1,2,0] $ SOne False 0 ax

-- | A program for boolean not
notProgram
    = Deriv [] [("x",bool),("r",neg bool)]
    $ Plus "y" "z" 0
        (SOne False 0 (With False dum "u" True 0 SBot))
        (SOne False 0 (With False dum "u" False 0 SBot))

-- | The not program wrapped as a function
notFunction
    = Deriv [] [("not",bool ⊸ bool)]
    $ Cross False dum "x" "r" 0 (derivSequent notProgram)


-- | Testing cuts
cutTest = Deriv [] [("x",Zero)] $
          Cut "a" "b" Bot 1
            (SOne False 0 (SZero 0))
            SBot

mpTest = Deriv ["α"] [("p",var 0 :*: neg (var 0))] $
         Cross False (var 0) "a" "b" 0 $
         Cut "x" "y" (var 0) 1
           (Ax (var 0) 0 1 )
           (Ax (neg (var 0)) 0 1)

-- Small example program

two = One :+: One

bType 0 = One
bType n = two :*: bType (n-1)

semiAdder = Deriv [] [("x",two),("y",two),("z",neg (two :*: two))] $
            Plus "t" "u" 0
              (SOne False 0 $
               Plus "v" "w" 0
               (SOne False 0 $
                Par False (neg two) "k" "l" 0
                  (With False dum "m" True 0 SBot)
                  (With False dum "n" True 0 SBot)
               )
               (SOne False 0 $
                Par False (neg two) "k" "l" 0
                  (With False dum "m" True 0 SBot)
                  (With False dum "n" False 0 SBot)
               ))
              (Par False (neg two) "k" "l" 2
                 (SOne False 0 $
                  Ax two 0 1)
                 (With False dum "n" False 0 SBot)
              )
--p = Deriv [] [("x",bType b),("y",bType b),("z",neg (bType b))] $
--


swap = Deriv [] [("swapee",neg $ swapType)]
   $ TUnpack "swapee'" 0
   $ TUnpack "swapee''" 0
   $ Cross False dum "input" "output" 0
   $ Cross False dum "a_in" "b_in" 0
   $ Exchange [1,2,0]
   $ Par False dum "b_out" "a_out" 1 ax ax

use_swap_once = Deriv ["a","b"] [("in",a⊗b),("out",neg (b⊗a))]
    $ Cut "mkswap" "swap" t0 0 (derivSequent swap)
    $ TApp False dum "swap'" 0 a
    $ TApp False dum "swap''" 0 b
    $ Exchange [1,0,2]
    $ Par False dum "u" "v" 1 ax ax
  where
    a = var 0
    b = var 1

use_swap_twice = Deriv ["a","b"] [("input",a⊗b),("out",neg (a⊗b))]
    $ Cut "mkswap" "swapfun" (Bang t0) 0 (Offer False "swappers" 0 $ derivSequent swap)
    $ Alias False 0 "swapfun'"
    $ Exchange [0,2,1,3]
    $ Cut "flop" "glop" (b⊗a) 2
        ( Demand "swap" dum 1
        $ TApp False dum "swap'" 1 a
        $ TApp False dum "swap''" 1 b
        $ Exchange [2,1,0]
        $ Par False dum "u" "v" 1 ax ax
        )
        ( Demand "swap" dum 1
        $ TApp False dum "swap'" 1 b
        $ TApp False dum "swap''" 1 a
        $ Par False dum "u" "v" 1 ax ax
        )
  where
    a = var 0
    b = var 1

tensors =
    Deriv
      { derivTypeVars = []
      , derivContext =
          [ ( "x" , One ⊗ Zero ) , ( "y" , (Top ⊗ Bot) ⊗ Bot ) ]
      , derivSequent =
          Cross
            False
            (One ⊗ Zero)
            "a"
            "b"
            0
            (Exchange
               [ 2 , 1 , 0 ]
               (Cross
                  False
                  ((Top ⊗ Bot) ⊗ Bot)
                  "s"
                  "t"
                  0
                  (exchange
                     [ 0 , 2 , 1 , 3 ]
                     (Cross
                        False
                        (Top ⊗ Bot)
                        "u"
                        "v"
                        0
                        (exchange [ 2 , 1 , 0 , 3 , 4 ] (SZero 0))))))
      }

tensors2 = Deriv
  { derivTypeVars = []
  , derivContext =
      [ ( "x" , One :*: Zero ) , ( "y" , (Top :*: Bot) :*: Bot ) ]
  , derivSequent =
      Cross
        False
        (One :*: Zero)
        "a"
        "b"
        0
        (exchange
           [ 2 , 1 , 0 ]
           (Cross
              False
              ((Top :*: Bot) :*: Bot)
              "s"
              "t"
              0
              (Cross False (Top :*: Bot) "u" "v" 0 (SZero 3))))
  }



-}

-- -}
