{-# LANGUAGE TemplateHaskell #-}
module AbsMxUtils where

import Data.Generics.Geniplate
import Parser.AbsMx

idsOfExpr :: Expr -> [Id]
idsOfExpr = $(genUniverseBi 'idsOfExpr)

idPos :: Id -> (Int,Int)
idPos (Id (p,_)) = p


