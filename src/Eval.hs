{-# LANGUAGE PatternGuards, OverloadedStrings, TemplateHaskell, RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-} 
{-# LANGUAGE PatternSynonyms #-} 
{-# LANGUAGE PatternGuards #-} 
{-# LANGUAGE RankNTypes #-} 
{-# LANGUAGE ViewPatterns #-} 
{-# LANGUAGE ParallelListComp #-} 
{-# LANGUAGE TupleSections #-} 
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DoAndIfThenElse #-} 
{-# LANGUAGE UnicodeSyntax #-} 
module Eval(module Eval) where

import Development.Placeholders

import Eval.Core as Eval
import Eval.BigSeq
import LL
import LL.Traversal
import Id
import Data.List (find, (\\), findIndex)
import Fresh
import Lint
import Fuel
import Control.Monad.State
import Control.Monad.Except.Extra
import Data.List
import Data.Default
import Data.Generics.Geniplate
import Control.Applicative
import Control.Lens hiding (Fold)
import Control.Monad.Reader
import FileLocation
import qualified Data.Traversable as T
import GHC.Stack
import Data.Bifunctor
import Data.Bitraversable
import Debug.Trace
import Data.Generics.Is

select :: [(Bool, a)] -> a
select = maybe err snd . find fst
  where err = error $ "select: ++ impossible case found"

evaluateFDeriv :: Deriv0 Id Id -> EvalM (Deriv0 Id Id)
evaluateFDeriv (D0 d) = fmap D0 $
                        liftEvalNIA_ (predsFromCtx (d ^. derivCtx)) $
                        d & derivSequent %%~ elimCuts

contextOf :: Seq' Id Id -> [Id]
contextOf s = case checkCtx s of
  Left x  -> error x
  Right (Seqn1 (Seqn {_ann})) -> _ann

-- substVal [x]s i t  ~   t[[x']s/i] (where x' fresh)
--
-- substVal ty x s i t = t[Load c i v] -> t[cut { c -> v; [x -> s](refresh) }]
--    where x : ty
substVal :: CutInfo -> IdType -> Id -> IdSeq -> Id -> IdSeq -> EvalM IdSeq
substVal ci0 ty x s i = transformBiM $ \(S0 v0) -> case v0 of
    Load i' sz c v | i' == i -> do
        x' <- lift (refreshId x)
        s' <- lift (refreshBindersSeq (rename x x' s))
        ty' <- lift (refreshType ty)
        rec ci0 FanLeft sz c x' (neg ty') (S v) s'
            -- FIXME: Not sure about fanning/size here.
            
          -- FIXME: if one substitutes a concrete n for the value of
          -- the size variable in folmap, we must rewrite the foldmap
          -- extensively.
    _ -> return (S0 v0)

-- | As 'cut', but consume a bit of fuel.
-- rec ci0 side sz a b tt l r | trace ("rec[" ++ intercalate ", " [show ci0, show side, show sz, show a, show b, show tt, show (prune0 l), show$ prune0 r] ++ "]") False = undefined
rec ci0 side sz a b tt l r = consumeCut ci0 (mkCut ci0 side sz a b tt l r) (cut ci0 side sz a b tt l r)

-- | Check if the variable is in "ready" state for fusion.
--   If it isn't, the sequent will be commuted with the fuse rule,
--   until a "ready" variable is reached.
-- | As cut rule, but do not produce an actual cut (fuse)
rec,cut :: CutInfo -> Fanning -> IdSize -> Id -> Id -> IdType -> IdSeq -> IdSeq -> EvalM IdSeq
cut ci0 side sz a b tt (S0 l) (S0 r)
{-
  Γ,a:~T⊢l    Δ,b:T⊢r
-----------------------
     Γ,Δ⊢
-}

{-
 Γ,a:~T,x:~U ⊢ s   Ξ,y:U ⊢ t
------------------------------ cut(U)
  Γ,Ξ,a:~T⊢                            xΔ,b:T⊢ u
------------------------------------------------- cut(T)
     Γ,Ξ,Δ⊢


 Γ,a:~T,x:~U ⊢ s   xΔ,b:T⊢ u
------------------------------ cut(T)
  Γ,Δ,x:~U⊢                            Ξ,y:U ⊢ t
-------------------------------------------------- cut(U)
     Γ,Ξ,Δ⊢
-}

  | NCut _ _ _ _ s _ <- l, a `usedIn` S s = cut ci0 side sz a b tt (S0$ swapCut l) (S0 r)

  | NCut ci side' sz' cv s t <- l, u <- r, a `usedIn` S t
  = case (side,side') of
      -- TODO: Traspose context
      (FanLeft , FanLeft ) -> S0 . NCut ci FanLeft  (sz * sz') cv s <$> (rM $ rec ci0 FanLeft sz a b tt (S t) (S0 u))
      (FanLeft , FanRight) ->
        -- Necessarily, sz' == 1
        S0 . NCut ci FanLeft  (sz' * sz) cv s <$> (rM $ rec ci0 FanLeft sz a b tt (S t) (S0 u))

      (FanRight, FanLeft ) -> S0 . NCut ci FanLeft  (sz')      cv s <$> (rM $ rec ci0 FanRight sz                   a b tt (S t) (S0 u))
      (FanRight, FanRight) -> S0 . NCut ci FanRight (sz')      cv s <$> (rM $ rec ci0 FanRight (sizeDivSafe sz sz') a b tt (S t) (S0 u))
     -- sz' == 1
  | NCut _ _ _ _ _ _ <- l = $(err') $ "Unused cut variable " ++ show a ++ ""
{-
    let szDiv | side' == FanRight = sizeDivSafe sz sz'
              | otherwise = sz
        sz'Mul | side == FanLeft = sz' * sz
               | otherwise = sz'
    in S0 . NCut ci side' sz'Mul cv s <$> (rM $ rec ci0 side szDiv a b tt (S t) (S0 u))
-}
{-
 Γ,x:~U ⊢ s   Ξ,a:~T,y:U ⊢ t
------------------------------ cut(U)
  Γ,Ξ,a:~T⊢                            Δ,b:T⊢ u
------------------------------------------------- cut(T)
     Γ,Ξ,Δ⊢

              Ξ,a:~T,y:U ⊢ t    Δ,b:T⊢ u
             ----------------------------
 Γ,x:~U ⊢ s             y:U,Ξ,Δ⊢
-------------------------------------------------- cut(U)
     Γ,Ξ,Δ⊢

-}


  | NCut {} <- r = converse cut

  | (Ax x y) <- l, s <- r
  = select [(a == x, return $ rename b y (S0 s)),
            (a == y, return $ rename b x (S0 s))]

  | Ax{} <- r = converse cut

  | Foldmap{} <- r, (S0 r) `worksOn` b = apply    foldmapEval
  | Foldmap{} <- l, (S0 l) `worksOn` a = converse foldmapEval

  | Foldmap2{} <- r, (S0 r) `worksOn` b = apply    foldmap2Eval
  | Foldmap2{} <- l, (S0 l) `worksOn` a = converse foldmap2Eval

  | Load{} <- l = apply    load
  | Load{} <- r = converse load

  | Split{} <- r = apply    split
  | Split{} <- l = converse split

  | Sync{} <- l, sz /= sizeOne, side == FanLeft  = apply sync
  | Sync{} <- r, sz /= sizeOne, side == FanRight = converse sync

  -- Commuting conversions
  | not ((S0 l) `worksOn` a){-, side == FanRight || sz == sizeOne-} = apply commute
  | not ((S0 r) `worksOn` b){-, side == FanLeft  || sz == sizeOne-} = converse commute
  -- not (l `worksOn` a) = apply commute
  -- not (r `worksOn` b) = converse commute

  | What{} <- l = original
  | What{} <- r = original
  | Fix{} <- l = original
  | Fix{} <- r = original
{-
  | Loop{} <- l, Loop{} <- r {- l `worksOn` a, r `worksOn` b -} = apply looploop
  | SPole{} <- l, SPole{} <- r, a `elem` pole l, b `elem` pole r = apply polepole
  | Chop{} <- l, Chop{} <- r, a `elem` (map gashRef $ gashes l), b `elem` (map gashRef $ gashes r) = apply chopchop
-}
  | BigSeq fl <- l, BigSeq fr <- r, a `elem` (fl >>= seqStepRefs), b `elem` (fr >>= seqStepRefs), FanLeft <- side = apply bigseq
  | BigSeq fl <- l, BigSeq fr <- r, a `elem` (fl >>= seqStepRefs), b `elem` (fr >>= seqStepRefs), FanRight <- side = converse bigseq


  -- Precondition : 
  -- l `worksOn` a && r `worksOn` b
  | positiveType tt = apply    oper
  | otherwise       = converse oper

 where
   original = pure $ apply mkCut
   converse f = f ci0 (dual side) sz b a (neg tt) (S0 r) (S0 l)
   apply    f = f ci0 side sz a b tt (S0 l) (S0 r)

foldmapEval, foldmap2Eval, split, oper,commute,load,bigseq, sync :: 
  CutInfo -> Fanning -> IdSize -> Id -> Id -> IdType -> IdSeq -> IdSeq -> EvalM IdSeq
--bigseq _ _ _ _ _ _ _ _ | trace "bigseq" False = undefined
bigseq ci0 FanLeft fuseSz a b (T (Array (pol -> Neutral) _ ty))  (S0 (BigSeq fl)) (S0 (BigSeq fr)) =
  fuseseqs fl fr [] []
  where
    -- ^ If the sizes of both § constructs are correct, then there should
    --   be no more sequence remainining in these cases, and, therefore,
    --   no need to fuse.
    fuseseqs :: [IdSeqStep] -> [IdSeqStep] -> [IdSeqStep] -> [(Id, (Id, IdSize), (Id,IdSize))] -> EvalM IdSeq
 {-   fuseseqs ls rs acc _ | trace ("fuseseqs[ "
                                ++ show a ++ " ; " ++ show b
                                ++ " ;\n " ++ show (map pruneStep ls)
                                ++ " ;\n " ++ show (map pruneStep rs)
                                ++ " ;\n " ++ show (map pruneStep acc)
                                ++ " ]") False = undefined -}
    fuseseqs [] [] acc splits = postproc (reverse splits) (reverse acc) id
    fuseseqs (l:ls) rs acc splits | not $ l `worksOn` a = fuseseqs ls rs (l:acc) splits 
    fuseseqs ls (r:rs) acc splits | not $ r `worksOn` b = fuseseqs ls rs (r:acc) splits 

    fuseseqs (l:ls) (r:rs) acc splits = do
      case (l `worksOn` a, r `worksOn` b) of
        (False,    _) -> do
                           l' <- fuseSz `resizeByM` l
                           fuseseqs ls (r:rs) (l':acc) splits
        (_    ,False) -> fuseseqs (l:ls) rs (r:acc) splits 
        (True ,True ) -> do
          -- Then both l and r are SeqInterval.
          -- Find the local names for the sequence chunks.
          let leq =
                do
                  ((r', rest), splits') <- splitInterval (seqStepSz0 l) r
                  next <- T.mapM (fusestep l) r'
                  fuseseqs ls (rest ++ rs) (next ++ acc) (splits' ++ splits)
          let geq =
                do
                  ((l', rest), splits') <- splitInterval (seqStepSz0 r) l
                  next <- T.mapM (flip fusestep r) l'
                  fuseseqs (rest ++ ls) rs (next ++ acc) (splits' ++ splits)

          let eq = do
                     next <- fusestep l r
                     fuseseqs ls rs (next:acc) splits 
          
          sizePerhapsBranchLEq (seqStepSz0 l) (seqStepSz0 r)
            (\ord -> case ord of
                BLEq -> leq
                BEq  -> eq
                BGEq -> geq)

      where

        fusestep :: IdSeqStep -> IdSeqStep -> EvalM IdSeqStep
--        fusestep _ _ | trace "fusestep" False = undefined
        fusestep l@SeqStep{} r@SeqStep{} = do
          let ([(_,a')], chunkA) = partition ((== a) . fst) (seqChunk l) 
          let ([(_,b')], chunkB) = partition ((== b) . fst) (seqChunk r) 
          (S seqProgAB) <- rec ci0 FanLeft fuseSz a' b' ty (S (seqProg l)) (S (seqProg r))
          return $ SeqStep {
             seqSz = seqSz l -- (or r)
            ,seqChunk = chunkA ++ chunkB
            ,seqProg = seqProgAB
          }

    -- ^ Split variables
    postproc :: [(Id, (Id, IdSize), (Id, IdSize))] -> [IdSeqStep] -> (IdSeq -> IdSeq) -> EvalM IdSeq 
--    postproc _ _ _ | trace "postproc" False = undefined
    postproc []       steps rest = return $ rest (S0 (BigSeq steps))
    postproc ((name, (name1, sz1), (name2,sz2)):sps) steps rest = 
      let f = \(S cont) -> S0$ Split { splitSz = sz1,
                                  xIntr = name1,
                                  yIntr = name2,
                                  zElim = name,
                                  cont = cont }
      in postproc sps steps (rest . f)

load ci0 side sz a b tt (S0 (Load z sz' x s)) r = S0 . Load z newSz x <$> (rM$ rec ci0 side sz a b tt (S s) r)
  where newSz = case side of
           FanLeft -> sz * sz'
           FanRight -> sz'

commute ci0 side sz a b tt (S0 l) r
  = do let (s',mk) = conv a (S0 l)
       let ys = contextOf r \\ [b]
       let Just sz' = case l of
             BigSeq steps -> case find (usedIn a . fst) [(S seqProg, seqSz) | step@SeqStep{..} <- steps] of
               Just (_,seqSz) -> sz `sizeDiv` seqSz
             BigPar{..} -> case find (\(_,Branch _ p) -> a `usedIn` S p) branches of
               Just (splitSz,_) -> sizeDiv sz splitSz
             -- Par{..} | a `usedIn` contL == (fanning == FanLeft) ->
             --       sizeDiv sz sz -- FIXME: the divisor should be the
             --                     -- size of the ⅋
             --                     -- vector. Unfortunately this
             --                     -- information is not available here.
             _ -> Just sz
       let noDemotion = $(is 'FanRight) side || $(isP [p| 1 |]) sz
       let canCommute = case l of
             TUnpack{}   -> noDemotion
             Cross{}     -> noDemotion
             BigTensor{} -> noDemotion
             BigSeq{}    -> noDemotion
             _           -> True
       
       if canCommute then  
         mk ys $ map (\ u -> lift (refreshBindersSeq r) >>= \ r' -> rec ci0 side sz' a b tt u r') s'
       else
         rec NoCommute side sz a b tt (S0 l) r

-- FIXME: How does this implementation interact with creating more than one copy?
-- TODO: Define ordering of elements in resulting copy
-- E.g.: If the array is linear, either the copies are in the same row, or
-- in the same column, but not both.
-- ... indicate using shape notation?
sync ci0 FanLeft sz@1 a b tt (S0 (Sync{syncIntr = syncIntr'
                                      ,contL  = contL'
                                      ,syncCont = syncCont'
                                      ,syncContR = syncContR'
                                      ,..
                                      })) u 

  | a `usedIn` (S contL') = s0$ do
      let syncIntr = resizeSyncIntrBy sz `map` syncIntr'
      contL      <- rM$ rec ci0 FanLeft sz a b tt (S contL') u
      syncCont   <- T.mapM (sz `resizeByM`) syncCont'
      syncContR  <- T.mapM (sz `resizeByM`) syncContR'

      return $ Sync {..}
           

  | Just syncCont'' <- syncCont',
    a `usedIn` (S syncCont'') = s0$ do
      let syncIntr = resizeSyncIntrBy sz `map` syncIntr'

      contL  <- sz `resizeByM` contL' 
      syncCont <- liftM Just . rM$ rec ci0 FanLeft sz a b tt (S syncCont'') u
      syncContR  <- T.mapM (sz `resizeByM`) syncContR'

      return $ Sync {..}

  | Just syncContR'' <- syncContR',
    a `usedIn` (S syncContR'') = s0$ do
      let syncIntr = resizeSyncIntrBy sz `map` syncIntr'

      contL      <- sz `resizeByM` contL' 
      syncCont   <- T.mapM (sz `resizeByM`) syncCont'
      syncContR  <- liftM Just . rM$ rec ci0 FanLeft sz a b tt (S syncContR'') u

      return $ Sync {..}

  | otherwise = error "sync"

sync ci0 FanLeft sz a b tt t@(S0 (Sync{})) u = rec NoCommute FanLeft sz a b tt t u
                
foldmapEval ci0 side sz a b tt l (S0 (Foldmap{..})) = S0 <$> do
  (S fmMap) <- rec ci0 side sz' a b tt l (S fmMap)
  return Foldmap
      {fmArr = (contextOf l ++ fmArr) \\ [a,b]
      ,..}
  where Just sz' = sizeDiv sz (SizeExp $ sizeVar fmSz)

foldmap2Eval ci0 side sz a b tt l (S0 (Foldmap2{..})) = S0 <$> do
  (S fmMap) <- rec ci0 side sz' a b tt l (S fmMap)
  return Foldmap2
      {fmArr = (contextOf l ++ fmArr) \\ [a,b]
      ,..}
  where Just sz' = sizeDiv sz fmSize

split ci0 side k a b tt l (S0 r@(Split z sz1 x y s))
  | side == FanLeft, z == b = do
      let divCtx = contextOf l \\ [a]
      a' <- lift $ refreshId a
      divCtx' <- mapM (lift . refreshId) divCtx  -- two copies of the context are needed: we split that.
      divCtx'' <- mapM (lift . refreshId) divCtx
      let splitAll :: IdSeq -> IdSeq
          splitAll = S . foldr (.) seqn0 [S' . Split ref sz1 nm1 nm2 | (nm1,nm2,ref) <- zip3 divCtx' divCtx'' divCtx]
          -- FIXME: the size above (sz1), is wrong; it should be multiplied by the size factor of ref remaining in l.
          -- Unfortunately there is no way to compute this factor with the info we have at the moment.
          {-



                  A^m,A^n,Δ ⊢
              ------------------------- split (m)
  Γ^a,~A ⊢         A^(m+n), Δ ⊢
--------------------------------------- cut (m+n)
   Γ^(am+an),Δ ⊢


====>


               Γ^a,~A ⊢                 A^m, A^n, Δ ⊢
              ------------------------------------------ cut n
  Γ^a,~A ⊢         A^m, Γ^an, Δ ⊢
-------------------------------------- cut m
  Γ^am, Γ^an, Δ ⊢
--------------------- split(am)
   Γ^(am+an),Δ ⊢

  How to get 'a' ??? One possibility is to add an explicity distributivity rule.

-}
      l1 <- renameList        (zip divCtx divCtx') <$> pure l
      l2 <- renameList ((a,a'):zip divCtx divCtx'') <$> (lift . refreshBindersSeq) l

      inner <- rec ci0 side sz2 a' y tt l2 (S s)
      splitAll <$> rec ci0 side sz1 a x tt l1 inner
  | side == FanRight = -- invariant b /= z (because the size of b is one)
      S0 <$> Split z (sz1 * k) x y <$> (rM$ rec ci0 side k a b tt l (S s))
  | otherwise = commute ci0 (dual side) k b a (neg tt) (S0 r) l
      where sz2 = k - sz1

oper ci0 side sz _a _b (T One) (S0 (SBot _)) (S0 (SOne _β _ s)) = return (S s)

oper ci0 side sz _a _b (T (ta :*: tb)) (S0 (Par _β _a' x s y t)) (S0 (Cross _β' _b' x' y' u))
    = rec ci0 FanLeft sz x x' ta (S s) =<< rec ci0 side sz y y' tb (S t) (S u)

oper ci0 _side 1 _a _b (T (Array APositive sz ty)) (S0 (BigPar {branches=[(_,Branch y s)]})) (S0 (BigTensor _ x t))
    = rec ci0 FanLeft sz y x ty (S s) (S t)
 
oper ci0 _side 1 _a _b (T (Array APositive sz0 ty)) (S0 (BigPar {..})) (S0 (BigTensor _ x t))
  = do let go :: IdSize -> [(IdSize,Branch Id Id)] -> [(IdSize,Id)] -> EvalM IdSeq
           go _ [] acc = goMerge (reverse acc)
           go sz ((splitSz,Branch y p):bs) acc = do
             let sz' = sz - splitSz
             y' <- lift $ refreshId y
             cont <- go sz' bs ((splitSz,y'):acc)
             rec ci0 FanLeft splitSz y y' ty (S p) cont
           goMerge [(sz1,v1),(_sz2,v2)] = do
             merge' ci0 sz1 x v1 v2 (S t)
           goMerge ((sz1,v1):(sz2,v2):rest) = do
             z <- lift $ freshId
             mergeRest <- goMerge ((sz1 + sz2,z):rest)
             merge' ci0 sz1 z v1 v2 mergeRest
           goMerge xs = error $ "Go-merge: " ++ show xs ++ show (length branches)
       go sz0 branches []

oper ci0 side sz _a _b (T (ta :+: tb)) (S0 (With _ _a' bl z s)) (S0 (Plus {..}))
    = rec ci0 side sz z branchName ty (S s) (S branchProg)
    where (Branch{..},ty) = case bl of Inl -> (inl,ta); Inr -> (inr,tb)


oper ci0 side 1 _a _b (T (Bang ty)) (S0 (Offer _β _a' x s)) (S0 (Save _z i t))
    = substVal ci0 ty x (S s) i (S t)

{-

    cut
        { g -> offer g' for g in s[g',hs]
        ; f -> let f' = alias f in t
        }

    =>

    let hs2 = alias hs in
    cut
        { g2 -> offer g2' for g2 in s[g2',hs2]
        ; f' -> cut
            { g -> offer g' for g in s[g',hs]
            ; f -> t
            }
        }

-}

-- oper side sz _a _b (Rec Mu n ty) (Unfold x _ s) (Fold y _ t)
--     = rec x y (foldTy' n ty) s t

oper ci0 side sz _a _b (T (Exists v _ ty)) (S0 (TApp{ zElim = _a'
                                                    , ty = ty'
                                                    , xIntr = x
                                                    , cont = s}))
                                           (S0 (TUnpack{ zElim = _b'
                                                       , xIntr = y
                                                       , tyIntr = unpack_var
                                                       , cont = t }))
   | _a == _a', _b == _b' = do
         tz <- lift (tySubst v ty' ty)
         rec ci0 side sz x y tz (S s) =<< substEverywhere (S t)
       where substEverywhere = case ty' of
                 T (Var _ (TVar True tv')) -> return . tyVarSubst unpack_var tv'
                 _ -> tySubstSeq unpack_var ty' -- This ensures that, in the
                                         -- case of simple variable
                                         -- substitutions, the
                                         -- substitution also occurs
                                         -- in Load.

-- If cut-elimination is completely implemented this case should never trigger.
oper ci0 side sz a b ty s t = $(err') $ show $ prune0 $ mkCut ci0 side sz a b ty s t

mkCut :: CutInfo -> Fanning -> IdSize -> Id -> Id -> IdType -> IdSeq -> IdSeq -> IdSeq
mkCut ci0 side sz x y ty (S s) (S t) = S0$ NCut ci0 side sz [(x,y,ty)] s t


justOne :: (Functor m) => (t, t3 -> t2) -> ([t], [t1] -> [m t3] -> m t2)
justOne (s,f) = ([s],\_ [t] -> f <$> t)

justN :: (Applicative m) => ([t], [t1] -> [t3] -> t2) -> ([t], [t1] -> [m t3] -> m t2)
justN (ss,f) = (ss,\t1 t3m -> f t1 <$> T.sequenceA t3m)


-- | Commuting conversions
conv :: Id -> IdSeq -> ([IdSeq],[Id] -> [EvalM IdSeq] -> EvalM IdSeq)
conv ident seq0@(S0 seq) | if not (ident `usedIn` seq0) then 
                             errorWithStackTrace (show ident ++ "not used in" ++ show (seq0)) 
                           else if seq0 `worksOn` ident then
                             errorWithStackTrace (show ident ++ " is worked on by" ++ show (prune0 seq0)) 
                           else True
                                =
                                  
  let (seqs, cont) = conv' ident seq
  in (map S seqs, \idents seqs ->
       fmap S0 (cont idents (map (fmap seqn0) seqs)))
  where
-- conv _ (SBot x)              = return ([],\_ _ -> SBot x)
-- Impossible:

{-
    s
--------
  ⊥,~C ⊢      C,Ξ ⊢
-----------------
   ⊥,Ξ ⊢


can't use bot rule in s.

-}
  conv' a (NCut{ncut_left=ncut_left',ncut_right=ncut_right',..}) 
    | a `usedIn` (S ncut_left')  = justOne (ncut_left',\ncut_left -> NCut{ncut_right=ncut_right',..})
    | a `usedIn` (S ncut_right') = justOne (ncut_right',\ncut_right -> NCut{ncut_left=ncut_left',..})

  conv' _ (Tile x sz s)         = justOne (s,Tile x sz)
  conv' _ (SOne β x s)          = justOne (s,SOne β x)
  conv' a (Par β z x s y t)
    | a `usedIn` (S s) = justOne (s,\ s' -> Par β z x s' y t)
    | a `usedIn` (S t) = justOne (t,\ t' -> Par β z x s y t')
  conv' _ (Cross β z x y s)  = justOne (s,Cross β z x y)
  conv' _ (Fold x w s)          = justOne (s,Fold x w)
  conv' _ (Unfold x w s)        = justOne (s,Unfold x w)
  conv' _ (TApp β x tyB v s) = justOne (s,TApp β x tyB v)
  conv' a (Plus {..})           = justN ([branchProg inl,branchProg inr],
                               \_ [lProg,rProg] -> Plus{inl = inl {branchProg = lProg}
                                                       ,inr = inr {branchProg = rProg}
                                                       ,..})
  conv' _ (With b w c x s)   = justOne (s,With b w c x)
  
  --  conv' _ (Offer b w x s)       = justOne (s,Offer b w x)
  conv' a (BigPar {..}) = case findIndex (\(_,Branch _ p) -> a `usedIn` (S p)) branches of
    Just ix -> let (l,(sz,Branch x s):r) = splitAt ix branches
         in justOne (s,\s -> BigPar {branches=l++(sz,Branch x s):r,..})
  
  conv' _ (BigTensor z x s)     = justOne (s,BigTensor z x)
  conv' _ (Permute z sz x s)    = justOne (s,Permute z sz x)
  conv' _ (Demand x w s)      = justOne (s,Demand x w)
  conv' _ (Alias b x w s)       = justOne (s,Alias b x w)
  conv' _ (Ignore b x s)        = justOne (s,Ignore b x)
  conv' _ (TUnpack z k n x s)     = justOne (s,TUnpack z k n x)
  conv' _ (Save y x s)          = justOne (s,Save y x)
  conv' _ (Load y sz x s)       = justOne (s,Load y sz x)
  conv' a (SZero x xs)          = justN ([],\ys _ -> SZero x ((xs \\ [a]) ++ ys))
  -- conv' _ (Foldmap res empt comb z s) = justOne (s,Foldmap res empt comb z)
  conv' a (Foldmap {..})        | a `usedIn` (S fmMap)
                               = justOne (fmMap,\s' -> Foldmap {fmMap = s',..})
                               | a `usedIn` (S fmCont)
                               = justOne (fmCont,\s' -> Foldmap {fmCont = s',..})
  conv' a (Foldmap2 {..})        | a `usedIn` (S fmMap)
                               = justOne (fmMap,\s' -> Foldmap2 {fmMap = s',..})
                               | a `usedIn` (S fmCont)
                               = justOne (fmCont,\s' -> Foldmap2 {fmCont = s',..})
  conv' _ (IntOp  op z1 z2 x s) = justOne (s,IntOp  op z1 z2 x)
  conv' _ (IntCmp op z1 z2 x s) = justOne (s,IntCmp op z1 z2 x)
  conv' _ (IntCopy z x y s)     = justOne (s,IntCopy z x y) 
  conv' _ (IntIgnore z s)       = justOne (s,IntIgnore z)
  conv' _ (IntLit x lit s)      = justOne (s,IntLit x lit)
  conv' _ (Split z sz x y s)   = justOne (s,Split z sz x y) 
  conv' a (Sync{..}) | a `usedIn` (S contL)
                     = justOne (contL,\contL' -> Sync{contL = contL', ..})

                     | Just syncCont' <- syncCont
                     , a `usedIn` (S syncCont')
                     = justOne (syncCont',\(Just -> syncCont) -> Sync{..})

                     | Just syncContR' <- syncContR
                     , a `usedIn` (S syncContR')
                     = justOne (syncContR',\(Just -> syncContR) -> Sync{..})

  conv' a (BigSeq steps) | (r1, (True, r@SeqStep{..}) : r2) <- break fst
        [(usedHere , step)
        | step <- steps
        , let usedHere = case step of
                SeqStep{..} -> a `usedIn` (S seqProg) ]
    =
    justOne (seqProg, \seqProg' -> BigSeq $ map snd r1 ++ r{ seqProg = seqProg' } : map snd r2)
  
  conv' a SizeCmp {bCmp = bCmp',..} = ([p | (_,p) <- bCmp'],
                         (\_ progs -> do
                            let bCmp = [ (op,sM p) | p <- progs
                                                   | (op, _) <- bCmp' ]
                            fmap _seq$ rM$ retractEval SizeCmp {..}))
  
  
  conv' a s = error $ "conv':" ++ show a ++ "p:" ++ show (prune s)

evalSizeAssert :: (IdSize, CmpOp, IdSize) -> EvalM a -> EvalM a
evalSizeAssert p = liftEvalNIA_$ sizePredAdd p

retractEval :: Seq'' (EvalM IdSeq) IdType IdSize IdSized Id Id -> EvalM IdSeq
retractEval s = S0 <$> case s of

        SizeCmp{bCmp = bCmp',..} -> do
            bCmp <- T.sequenceA [ (op,) <$> evalSizeAssert (sz1,op,sz2) (rM cont')
                      | (op, cont') <- bCmp' ]
            return SizeCmp {..}

        TUnpack{tyKind = tyKind@(T (TProp p)),cont = cont',..} -> do
            cont <- evalSizeAssert p (rM cont')
            return TUnpack {..}

        _ -> traverseSeq'' defSeqTraverse{tseq = rM} s

  

-- | Eliminate cuts everywhere.
elimCuts :: IdSeq -> EvalM IdSeq
--transformBiM elimTopCut
elimCuts = runIdentity . foldSeqn defSeqnFold{sseq} . seqn0
  where
    sseq :: () -> Seq'' (EvalM IdSeq) IdType IdSize IdSized Id Id -> Identity (EvalM IdSeq)
    sseq _ t = Identity $ case t of

        Merge y z sz x k -> k >>= merge' Fuse sz x y z

--        NCut ci0 side sz [(a,b,ty)] s@(S' What{}) t@(S' What{}) -> mkCut ci0 (dual side) sz b a (neg ty) <$> t <*> s
        NCut ci0 side sz [(a,b,ty)] s' t' -> do
{-
          s' <- s --elimCuts (S s)
          t' <- t --elimCuts (S t)
-}
          s <- s' 
          t <- t'
          case (s,t) of
            -- hack for paper (cutswap)
            (S0 What{}, S0 What{}) -> pure $ mkCut ci0 (dual side) sz b a (neg ty) t s
            (_,_)                  -> rec ci0 side sz a b ty s t
        _              -> retractEval t

-- | Eliminate cuts everywhere.
evaluateF :: [(IdSize,CmpOp,IdSize)] -> Int -> IdSeq -> FreshM IdSeq
evaluateF preds fuel s = runFuelT fuel $ unsafeExceptT $ flip runReaderT def $ liftEvalNIA_ (sizePredsAdd preds) $ elimCuts s

runEval :: (Bitraversable t) => (t Id Id -> EvalM (t Id Id)) -> Int -> t Id Id -> t Id Id
runEval f n = runEvalEnv f n def

runEvalEnv :: (Bitraversable t) => (t Id Id -> EvalM (t Id Id)) -> Int -> EvalEnv -> t Id Id -> t Id Id
runEvalEnv f n env d = runFreshMFrom d $ runFuelT n $ unsafeExceptT $ flip runReaderT env $ f d

ideval ::  Int -> IdDeriv -> IdDeriv
ideval = runEval evaluateFDeriv

merge', merge :: CutInfo -> IdSize -> Id -> Id -> Id -> IdSeq -> EvalM IdSeq
merge' ci0 szA nr r1 r2 (S s) = consumeCut ci0 (S0 $ Merge r1 r2 szA nr s) (merge ci0 szA nr r1 r2 (S s))
merge ci0 szA nm r1 r2 (S0 s)
  | (S0 s) `worksOn` nm
  = case s of
    -- case hackCompareSizes szK szA of
      Split _nm szK nm1 nm2 t -> do
          -- We recycle the 'nm' identifier instead of making up a new one
          let le = S0 . Split r1 szK         nm1  nm   <$> (rM$ merge' ci0 (szA - szK) nm2 nm   r2   $ (S t))
          let eq = return (rename nm1 r1 (rename nm2 r2 (S t)))
          let ge = S0 . Split r2 (szK - szA) nm   nm2  <$> (rM$ merge' ci0 szA         nm1 r1   nm   $ (S t))
          sizePerhapsBranchLEq szK szA (\ord -> case ord of
                                                  BEq  -> eq
                                                  BLEq -> le
                                                  BGEq -> ge) 
  
      fm@Foldmap2{..} -> do
        let divCtx = fmArr \\ [nm]
        divCtx1 <- lift $ mapM refreshId divCtx  -- two copies of the context are needed: we split that.
        divCtx2 <- lift $ mapM refreshId divCtx
        [z,z_,x1_,x2_,x12_,x12] <- lift $ freshIds 6
        let splitAll :: IdSeq -> IdSeq
            splitAll = S . foldr (.) seqn0 [S' . Split r szA nm1 nm2 | (nm1,nm2,r) <- zip3 divCtx1 divCtx2 divCtx]
        fmMapTrg1 <- lift $ refreshId fmMapTrg
        fmMapTrg2 <- lift $ refreshId fmMapTrg
        let renamer1 = renameList ((fmMapTrg,fmMapTrg1):(nm,r1):zip divCtx divCtx1)
        let renamer2 = renameList ((fmMapTrg,fmMapTrg2):(nm,r2):zip divCtx divCtx2)
        (S fmMap1) <- lift $ refreshBindersSeq $ renamer1 (S fmMap)
        (S fmMap2) <- lift $ refreshBindersSeq $ renamer2 (S fmMap)
        (S0 fmRefr) <- lift $ refreshBindersSeq (S0 fm)
        (S branch1) <- foldmapSimpl (fm {fmSize=szA,               fmArr=r1:divCtx1
                                    ,fmCont=S'$ Ax fmMon x1_
                                    ,fmMapTrg=fmMapTrg1
                                    ,fmMap=fmMap1
                                          })
        (S branch2) <- foldmapSimpl (fmRefr  {fmSize=fmSize - szA,fmArr=r2:divCtx2
                                         ,fmCont=S'$ Ax (LL.fmMon fmRefr) x2_
                                          ,fmMapTrg=fmMapTrg2
                                          ,fmMap=fmMap2
                                          })
        rec ci0 FanLeft 1 z z_ (fmTyM ⊗ fmTyM ⊸ fmTyM)
          (S0$ Cross False z x12 fmMixTrg $
           S'$ Cross False x12 fmMixL fmMixR $
           fmMix) $
           splitAll $
           (S0$ Par False z_ x12_
             (S'$ Par False x12_ x1_ branch1 x2_ branch2)
             fmMon 
             fmCont)
{-      Cross β ty xIntr yIntr _nm cont  -> do
        xIntr1 <- lift $ refreshId xIntr
        xIntr2 <- lift $ refreshId xIntr
        yIntr1 <- lift $ refreshId yIntr
        yIntr2 <- lift $ refreshId yIntr
        inner <- merge ci0 szA xIntr xIntr1 xIntr2 =<< merge ci0 szA yIntr yIntr1 yIntr2 cont
        return $ Cross β ty xIntr1 yIntr1 r1 $ Cross β ty xIntr2 yIntr2 r2 $ inner-}
    {-
      Ax{wElim, zElim} -> do
        let ax = return . S0 . case (wElim == nm, zElim == nm) of
                   (True,False) -> (\wElim -> Ax{..})
                   (False,True) -> (\zElim -> Ax{..})

        sizePerhapsBranchEq szA 1 $ \case
          True  -> LL.mix <$> ax r1 <*> pure (LL.halts [r2])
          False -> LL.mix <$> pure (LL.halts [r1]) <*> ax r2
     -} 
      s@(What{..}) ->  return $ S0$ Merge r1 r2 szA nm (S' s) 

      s ->
        let rule r' = return$ S0$ fmapSeq'' defSeqFmap{fref = (\x → if x == nm then r' else x)} s in

        sizePerhapsBranchEq szA 1 $ \case
          True  -> LL.mix <$> rule r1 <*> pure (LL.halts [r2])
          False -> LL.mix <$> pure (LL.halts [r1]) <*> rule r2

        
  | nm `usedIn` (S0 s)
  = case s of
       BigPar {..} ->
         let go (b@(sz',Branch x p):bs) acc
               | nm `usedIn` (S p)
               = do let divCtx = contextOf (S p) \\ [x,nm]
                    -- all the variables used in the branch except x
                    -- and nm which are handled separately
                    let diff = sz' - szA      -- the size of the 1st chunk
                    x' <- refreshId x
                    -- newly introduced variable in the new branch
                    divCtx' <- mapM refreshId divCtx  -- two copies of the context are needed: we split that.
                    divCtx'' <- mapM refreshId divCtx
                    let splitAll :: IdSeq -> IdSeq
                        splitAll = S . foldr (.) seqn0 [S'. Split r szA nm1 nm2 | (nm1,nm2,r) <- zip3 divCtx' divCtx'' divCtx]
                    S p1 <- renameList        ((nm,r1):zip divCtx divCtx') <$> pure (S p)
                    S p2 <- renameList ((x,x'):(nm,r2):zip divCtx divCtx'') <$> refreshBindersSeq (S p)
                    return $ splitAll $
                             S0 $ BigPar {branches = reverse acc++(szA,Branch x p1):(diff,Branch x' p2):bs,..}
               | otherwise = go bs (b:acc)
             go [] _acc = error "merge bigPar: merged var unused!"
         in lift $ go branches []

       NCut {ncut_cs=[(x,y,ty)],..} | ncut_size /= 1, ncut_fanning == FanLeft,  nm `usedIn` (S ncut_left)  -> do
         -- context where merged variable is not demoted to 1 not implemented
         let divCtx = contextOf (S ncut_left) \\ [x,nm]
         let diff = ncut_size - szA      -- the size of the 1st chunk
         x' <- refreshId x
         y' <- refreshId y
         y'' <- refreshId y
         divCtx' <- mapM refreshId divCtx
         divCtx'' <- mapM refreshId divCtx
         let splitAll :: IdSeq -> IdSeq
             splitAll = S . foldr (.) seqn0 [S'. Split r szA nm1 nm2 | (nm1,nm2,r) <- zip3 divCtx' divCtx'' divCtx]
         S p1 <- renameList        ((nm,r1):zip divCtx divCtx') <$> pure (S ncut_left)
         S p2 <- renameList ((x,x'):(nm,r2):zip divCtx divCtx'') <$> refreshBindersSeq (S ncut_left)
         return$ splitAll $ S0 $
                NCut{ncut_info
                    ,ncut_fanning
                    ,ncut_size = szA
                    ,ncut_cs = [(x,y',ty)]
                    ,ncut_left = p1
                    ,ncut_right = (S' NCut{
                         ncut_info
                        ,ncut_fanning
                        ,ncut_size = diff
                        ,ncut_cs = [(x',y'',ty)]
                        ,ncut_left = p2
                        ,ncut_right = (S' (Merge {splitSz = szA
                                                 ,zElim = y', wElim = y''
                                                 ,xIntr = y, cont = ncut_right
                                                 }))})}

       NCut {..} | ncut_size /= 1, ncut_fanning == FanRight, nm `usedIn` (S ncut_right) -> merge ci0 szA nm r1 r2 (S0$ swapCut s)
       BigSeq{..} -> 
         let go (b@SeqStep{seqSz = seqSz', seqProg=p, seqChunk}:bs) acc
               | nm `usedIn` (S p)
               = do let divCtx = contextOf (S p) \\ ([nm] ++ [name | (ref,name) <- seqChunk])
                    -- all the variables used in the branch except x
                    -- and nm which are handled separately
                    let diff = seqSz' - szA      -- the size of the 1st chunk
                    seqChunk' <- sequence [(ref,) <$> refreshId name | (ref,name) <- seqChunk]
                    -- newly introduced variable in the new branch
                    divCtx' <- mapM refreshId divCtx  -- two copies of the context are needed: we split that.
                    divCtx'' <- mapM refreshId divCtx
                    let splitAll :: IdSeq -> IdSeq
                        splitAll = S . foldr (.) seqn0 [S'. Split r szA nm1 nm2 | (nm1,nm2,r) <- zip3 divCtx' divCtx'' divCtx]
                    S p1 <- renameList        ((nm,r1):zip divCtx divCtx') <$> pure (S p)
                    S p2 <- renameList ([(x,x') | (_,x ) <- seqChunk
                                                | (_,x') <- seqChunk']++(nm,r2):zip divCtx divCtx'') <$> refreshBindersSeq (S p)
                    let step1 = SeqStep{seqSz=szA, seqChunk = seqChunk,  seqProg = p1}
                    let step2 = SeqStep{seqSz=diff, seqChunk = seqChunk', seqProg = p2}
                    return $ splitAll $
                             S0 $ BigSeq {fence = reverse acc++step1:step2:bs,..}
               | otherwise = go bs (b:acc)
             go [] _acc = error "merge bigSeq: merged var unused!"
         in lift $ go fence []

       Par {..} | trace "WARNING: Commuting Par through merge is not complete" False -> $(err "") 
       -- It is necessary to know the size of the Par in order to know whether to
       -- just commute the merge (sz = 1), split the par (sz = n+m), or sth in between
       -- (sz | n + m)
       Par {..}  -- fanning = FanLeft,
         | nm `usedIn` (S contL) -> do
            let divCtx = contextOf (S contL) \\ [xIntr,nm]
            -- all the variables used in the branch except x
            -- and nm which are handled separately
            xIntr' <- lift $ refreshId xIntr
            zElim1 <- lift $ refreshId zElim
            zElim2 <- lift $ refreshId zElim
            yIntr1 <- lift $ refreshId yIntr
            yIntr2 <- lift $ refreshId yIntr
            -- newly introduced variable in the new branch
            divCtx' <- mapM (lift . refreshId) divCtx  -- two copies of the context are needed: we split that.
            divCtx'' <- mapM (lift . refreshId) divCtx
            let splitAll :: IdSeq -> IdSeq
                splitAll = S . foldr (.) id [S'. Split r szA nm1 nm2 | (nm1,nm2,r) <- zip3 divCtx' divCtx'' divCtx] . r0
            p1 <- renameList        ((nm,r1):zip divCtx divCtx') <$> pure (S contL)
            p2 <- renameList ((xIntr,xIntr'):(nm,r2):zip divCtx divCtx'') <$> lift (refreshBindersSeq (S contL))
            inner <- merge' ci0 szA yIntr yIntr1 yIntr2 (S contR)
            return $ S0$ Split zElim szA zElim1 zElim2 $ r0$ 
                       splitAll $ S0$ Par False zElim1 xIntr (r0 p1) yIntr1 $ 
                         S'$ Par False zElim2 xIntr' (r0 p2) yIntr2 (r0 inner)
       _ ->  -- push the merge inside s
            do let (s',mk) = conv nm (S0 s)
               mk (contextOf (S0 s) \\ [nm]) (map (merge' ci0 szA nm r1 r2 <=< (lift . refreshBindersSeq)) s')
   | otherwise = error "merge: merged var unused!"
    

  where foldmapSimpl (Foldmap2 {fmSize = 1,..}) = rec ci0 FanLeft 1 fmMapTrg fmMon fmTyM (S fmMap) (S fmCont)
        foldmapSimpl x = return (S0 x)


