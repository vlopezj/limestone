{-# OPTIONS_GHC -fno-warn-unused-binds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternSynonyms #-}
module PrettyId where

import Text.PrettyPrint as PP hiding ((<>))
import Data.Monoid
import LL
import LL.Para
import Id
import MetaVar
import Pol
import qualified Type.Size 

------------------------------------
-- Pretty printing of sequents
class Pretty a where
  pretty :: a -> Doc

prettyShow :: Pretty a => a -> String
prettyShow = render . pretty

instance Pretty String where
    pretty = text

instance Pretty Unique where
  pretty = text . show

instance Pretty Id where
  pretty i = text (id_name i) <> "_" <> pretty (id_unique i)

instance Pretty IdType where
  pretty = pType 0

instance Pretty IdSized where
  pretty (ty :^ 1)  = pType 0 ty
  pretty (ty :^ sz) = pType 5 ty <> "^" <> pretty sz

instance Pretty IntOp where
  pretty intop = case intop of
    Add -> "+"
    Sub -> "-"
    Mul -> "*"
    Div -> "div"
    Mod -> "mod"

instance Pretty CmpOp where
  pretty cmpop = case cmpop of
    Eq -> "=="
    Ne -> "/="
    Gt -> ">"
    Ge -> ">="
    Le -> "<="
    Lt -> "<"

instance Pretty Integer where
  pretty = text . show

showDeriv :: IdDeriv -> String
showDeriv = render . pDeriv

pDeriv :: IdDeriv -> Doc
pDeriv (D0 (Deriv who (seqctx_ -> (ts, vs)) s)) =
    hang (hang (pCtx ts <+> ";") 0
               (pCtx vs <+> "⊢")) 0 $
    pSeq who True ts vs s

prn :: Ord a => a -> a -> Doc -> Doc
prn p k = if p > k then parens else id

pType :: (Pretty name, Pretty ref, Ord ref) => Int -> Type' name ref -> Doc
pType p (T t) = go p t
                where
    go p (Array (polarity -> pol) sz t) = prn p 0 $ op pol <+> pretty sz <+> pType 5 t
      where op Positive = "⊗⊗"
            op Negative = "⅋⅋"
            op Neutral = "§"
    go _ TType = "*"
    go p (Forall v s t) = prn p 0 $ hang ("∀" <> pretty v <> ":" <> pType 0 s <> ".") 2 (pType 0 t)
    go p (Exists v s t) = prn p 0 $ hang ("∃" <> pretty v <> ":" <> pType 0 s <> ".") 2 (pType 0 t)
    go p (x :|: y) = prn p 0 $ hang (pType 2 x <+> "⅋") 2 (pType 1 y)
    go p (x :+: y) = prn p 0 $ hang (pType 3 x <+> "⊕") 2 (pType 2 y)
    go p (x :*: y) = prn p 0 $ hang (pType 4 x <+> "⊗") 2 (pType 3 y)
    go p (x :&: y) = prn p 0 $ hang (pType 5 x <+> "&") 2 (pType 4 y)
    go _ Zero = "0"
    go _ One = "1"
    go _ Top = "⊤"
    go _ Bot = "⊥"
    go _ Pole = "▯"
    go _ (Var xs s) = pretty s <> args
      where args = if null xs then mempty else brackets (cat $ punctuate "," $ map (pType 0) xs)

    go p (Rec b x t) = prn p 4 $ binder <+> pretty x <+> "." <+> pType 0 t
      where binder = if b == Mu then "μ" else "ν"
    go _ (Index positive) = (if positive then mempty else "~") <> "index"
    go _ (Int positive) = (if positive then mempty else "~") <> "Int"
    go p (Bang t) = prn p 4 $ "!" <> pType 4 t
    go p (Quest t) = prn p 4 $ "?" <> pType 4 t
    go _ Lollipop{} = error "pType: Lollipop"
    go _ MetaNeg{} = error "pType: MetaNeg"
    go _ (SizeT p) = pretty p
    {-
    go p (SizeAdd x y) = prn p 0 $ pType 1 x <+> "+" <+> pType 2 y
    go p (SizeMul x y) = prn p 0 $ pType 2 x <+> "×" <+> pType 3 y
    go _ SizeOne   = integer 1
    go _ SizeZero  = integer 0
    -}
    --go p (SizeExp x) = prn p 3 $ "2^"<> pType 2 x
    go _ (Plupp t) = pType 5 t <> "∙"
    go _ (UnpushedNeg t) = "~" <> parens (pType 0 t)

instance Pretty ref => Pretty (Symbol ref) where
  pretty (TVar positive x) = (if positive then mempty else "~") <> (pretty x)
  pretty (Meta v) = pretty v
  pretty (MetaSyntactic b _bs s) = if_ b ("~" <>) $ text s 

instance Pretty ref => Pretty (MetaVar ref) where
  pretty (MetaVar b u _ _) = if_ b ("~" <+>) $ "?" <> pretty u

--pSeq :: Name -> Bool -> IdCtx -> IdCtx -> IdSeq -> Doc
pSeq who showTypes = foldIdSeqNoFresh sf ("pSeq:" ++ who) where
--   sf :: Deriv' Id Id -> SeqFinal'' Id Id Doc Doc
   sf (D0 (Deriv _ (seqctx_ -> (_ts, vs)) (S0 s0))) = SeqFinal {..} where
        sty _                    = pretty
        ssz _                    = pretty 
        stysz _                  = pretty 
        sax v v' _               = pretty v <+> "<->" <+> pretty v' where [(_,vt),(_,vt')] = vs
        sncut cs s t = pp_block cut (sep $ punctuate "," [varT v vt | (v,vt,_,_) <- cs]) s
                                    (sep $ punctuate "," [varT v vt | (_,_,v,vt) <- cs]) t
          where
            cut | length cs == 0 = "mix"
                | length cs == 1 = "cut"
                | length cs == 2 = "bicut"
                | otherwise      = PP.int (length cs) <> "-" <> "cut"

        sfreeze cs s t =
             pp_block "freeze" (sep $ punctuate "," [varT v vt | (v,vt,_,_) <- cs]) s
                               (sep $ punctuate "," [varT v vt | (_,_,v,vt) <- cs]) t

        shalt = "halt"
        smix s t  = pp_block "mix" mempty s
                                   mempty t

        scross _ w v vt v' vt' t = pp_let (hang (varT v vt <+> ",") 2 (varT v' vt')) (pretty w) t

        spar _ w v vt v' vt' s t = pp_block ("connect" <+> pretty w <+> "to")
                                            (varT v  vt) s
                                            (varT v' vt') t
        splus w v _vt v' _vt' s t  = pp_block ("case" <+> pretty w <+> "of")
                                            ("inl" <+> pretty v) s
                                            ("inr" <+> pretty v') t

        swith _ _ b w v' _ t = pp_let (pretty v') (choice b "fst" "snd" <+> pretty w) t

        sbot v              = pretty v
        szero w vt          = "dump" <+> "in" <+> pretty w
        sone _ w t          = pp_let "◇" (pretty w) t
        sxchg _ t           = t
        stapp _ v _ _ w tyB s = pp_let (pretty w) (pretty v <> "∙" <> tyB) s
        stunpack tw w _ v _ s = pp_let ("⟨" <> whenShowTypes (pretty tw) <> "," <> pretty v <> "⟩") (pretty w) s
        soffer _ v w _ty s   = "offer" <+> pretty w <+> "for" <+> pretty v <+> "in" $$ s

        ssave x _ty _ s        = pp_let ("*" <> pretty n) ("save" <+> pretty x) s
          where Save _ n _ = s0

        sload x ty _ s        = pp_let (varT n ty) ("load" <+> ("*" <> pretty x)) s
          where Load _ _ n _ = s0

        sdemand v w ty s    = pp_let (pretty w) (text "demand" <+> varT v ty) s
        signore w _ty s      = "ignore " <> pretty w $$ s
        salias _ w w' ty s  = pp_let (pretty w') ( "alias" <+> varT w ty) s
        sfold w w' _ty s     = pp_let (pretty w) ("fold" <+> pretty w') s
        sunfold w w' _ty s   = pp_let (pretty w) ("unfold" <+> pretty w') s
        swhat _a _ = braces $ undefined -- pCtx vs
        smem ty t u = "serve " <> ty <> " " <> vcat ["server" <> u
                                                    ,"clients" <> t
                                                    ]
        stile x xty s = pp_let (pretty x) ("tile" <+> xty) s
        sBigPar z _ty ts   = pp_block_n ("distribute" <+> pretty z)
                             (map (\(n,b,_) -> (pretty n, b)) ts)
        sBigTensor x z xTy _ sz s = pp_let (varT x xTy <> "^" <> pretty sz) ("split× " <> pretty z) s
        sSplit x _sx y _sy z _ s = pp_let (pretty x <> " , " <> pretty y) ("split+ " <> pretty z) s
        -- sparadd  x xTy y yTy z s t = pp_block ("connect" <+> pretty z <+> "to")
        --                                     (varT x xTy) s
        --                                     (varT y yTy) t

        sfoldmap arrTy monTy mapper mixer k =
            pp_block_n ("foldmap" <+> hsep (punctuate ";" $ zipWith varT fmArr arrTy))
                [ (hsep $ map pretty [fmMapTrg],mapper)
                , (hsep $ map pretty [fmMixL, fmMixR, fmMixTrg],mixer)
                , (varT fmMon monTy,k)
                ] where Foldmap {..} = s0
        sfoldmap2 arrTy monTy mapper mixer unit k =
            pp_block_n ("foldmap" <+> hsep (punctuate ";" $ zipWith varT fmArr arrTy))
                [ (hsep $ map pretty [fmMapTrg],mapper)
                , (hsep $ map pretty [fmMixL, fmMixR, fmMixTrg],mixer)
                , (hsep $ map pretty [fmUnitTrg],unit)
                , (varT fmMon monTy,k)
                ] where Foldmap2 {..} = s0

{-
        sloop vecTy seqTy stateTy k' step k =
          pp_block_n ("loop" <+> hsep (punctuate ";" $ zipWith varT loopSeq (vecTy ++ seqTy)))
                [ (varT wIntr stateTy,k')
                , (hsep $ map pretty $ [xIntr] ++ loopVec ++ loopSeq ++ [yIntr],step)
                , (varT zIntr stateTy,k)]
                where Loop {..} = s0

        spole stateTy a b =
          pp_block_n "pole" 
                [ (varT wIntr stateTy,a)
                , (varT zIntr stateTy,b)]
                where SPole {..} = s0
-}
        sintop x op z1 z2   = pp_let (pretty x) (pretty z1 <+> pretty op  <+> pretty z2)
        sintcmp x cmp z1 z2 = pp_let (pretty x) (pretty z1 <+> pretty cmp <+> pretty z2)
        sintlit x lit       = pp_let (pretty x) (integer lit)
        sintcopy x y z      = pp_let (pretty x <+> "," <+> pretty y) ("copy" <+> pretty z)
        sintignore x s      = "ignore" <+> pretty x $$ s
        sfix x ty_x z ty_z  = pp_let (varT x ty_x) ("fix" <+> varT z ty_z)
        snatrec = error "snatrec pretty"

   pp_let lhs rhs = hang (hang "let" 4 (hang (lhs <+> "=") 2 (rhs <+> "in"))) 0
   pp_block_n _ [] = error "pp_block empty list"
   pp_block_n top ((lhs1,rhs1):rest) = hang top 2 $
        vcat $  [ hang ("{" <+> lhs1 <+> "->") 2 rhs1 ]
             ++ [ hang (";" <+> lhs <+> "->") 2 rhs | (lhs,rhs) <- rest ]
             ++ [ "}" ]

   pp_block top lhs1 rhs1 lhs2 rhs2 = pp_block_n top [(lhs1,rhs1),(lhs2,rhs2)]

   varT x y | showTypes = hang (pretty x <+> ":") 2 y
            | otherwise = pretty x
   whenShowTypes | showTypes = id
                 | otherwise = const "?"

pCtx' :: Pretty id => [(id,Doc)] -> Doc
pCtx' vs = sep $ punctuate comma $ [pretty v <> " : " <> t | (v,t) <- vs]

pCtx :: (Pretty id, Pretty ty) => [(id,ty)] ->  Doc
pCtx = pCtx' . map (second pretty)

instance (Ord ref, Pretty ref) => Pretty (Size ref) where
  pretty sz = case sz of
    Size _    -> pretty $ syntax $ toPol sz
    SizeExp s -> "2^" <> pretty s

instance (Pretty a, Pretty ref) => Pretty (PolSyntax ref a) where
  pretty (PAdd a b) = pretty a <+> "+" <+> pretty b
  pretty (PProd a b) = pretty a <+> "×" <+> pretty b
  pretty (PSub a b) = pretty a <+> "-" <+> pretty b
  pretty (PPower a (PolNat n)) = pretty a <> superscript n
  pretty (PParens a) = "(" <> pretty a <> ")"
  pretty (PNeg a) = "-" <> pretty a
  pretty (PScalar a) = pretty a
  pretty (PVariable v) = pretty v

superscript :: Integer -> Doc
superscript = text . map (\c -> case c of
  '+' -> '⁺'
  '-' -> '⁻'
  '0' -> '⁰'
  '1' -> '¹'
  '2' -> '²'
  '3' -> '³'
  '4' -> '⁴'
  '5' -> '⁵'
  '6' -> '⁶'
  '7' -> '⁷'
  '8' -> '⁸'
  '9' -> '⁹'
  _   -> error $ "No superscript equivalent known for " ++ (c:[])) . show
  



