{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
module MetaVar where

import Id
import qualified Parser.AbsMx as Abs
import Data.Traversable
import Data.Foldable

type MetaVarRef = Unique

data MetaInfo = NoInfo
    | FromExpr Abs.Expr
    | FromPat Abs.Pat
    | FromType Abs.Type 
  deriving (Show,Ord,Eq,Read)

data MetaVar ref = MetaVar Bool       {- true: positive, false: negative -}
                           MetaVarRef {- a unique identifier -}
                           [ref]      {- scope -}
                           MetaInfo   {- origin info -}
  deriving (Show,Read,Eq,Ord,Functor,Traversable,Foldable)

