{-# LANGUAGE ViewPatterns,MultiParamTypeClasses,GeneralizedNewtypeDeriving, TupleSections,
             OverloadedStrings,ScopedTypeVariables,PatternGuards,CPP #-}
module DsMonad where

import Text.PrettyPrint

import MetaVar

import Data.Map (Map)
import qualified Data.Map as M

import Control.Applicative hiding (empty)
import Control.Monad
import Control.Monad.Error
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Writer hiding ((<>))
import Control.Arrow (second)

import LL hiding (choice,Choice,second)

import Id
import Fresh hiding (name)

import DsErrors

import qualified Parser.AbsMx as Abs
import Parser.AbsMx (IdList(..),TopDecl(..),Decl(..))

name :: Abs.Id -> Name
name (Abs.Id (_,n)) = n

makeId :: Abs.Id -> DsM Id
makeId (Abs.Id (pos,n)) = do
    u <- dsFresh
    return (Id n u (Just pos))

makeAbsId :: DsM Abs.Id
makeAbsId = do
    u <- dsFresh
    return (Abs.Id ((0,0),'`':show u))

--  | These cannot be referred to by the user so we don't need
--    to add them to the scope
makeTmp :: String -> DsM Id
makeTmp s = do
    u <- dsFresh
    return (Id s u Nothing)

makeTmps :: Int -> DsM [Id]
makeTmps = mapM makeTmp . flip replicate "tmp"

dsFresh :: DsM Unique
dsFresh = dsRunFresh fresh

dsRunFresh :: FreshM a -> DsM a
dsRunFresh = DsM . lift . lift . lift . lift

unTopDecl :: TopDecl -> Decl
unTopDecl (TopDecl d) = d

insertAliases :: [Decl] -> DsM a -> DsM a
insertAliases ds = local (M.union $ M.fromList
    [ (name x,(t,as))
    | AliasDecl il t <- ds
    , let (x:as) = idList il
    ])

assertEmptyScope :: DsM ()
assertEmptyScope = do
    scope <- getScope
    unless (M.null scope) (throw (msgIdentifiersEscape (map snd $ M.elems scope)))

cleanly :: DsM a -> DsM a
cleanly m = do
    s <- get
    r <- m
    put s
    return r

newtype DsM a = DsM (ReaderT Aliases (ErrorT Err (WriterT [(String,Doc)] (StateT St FreshM))) a)
  deriving
    ( Functor, Applicative, Monad
    , MonadState St
    , MonadReader Aliases -- type aliases
    , MonadWriter [(String,Doc)]   -- debug messages
    , MonadError Err      -- errors
    , MonadFix
    )

type Aliases = Map Name (Abs.Type,[Abs.Id])

type Scope = Map Name (IdType,Id)

type UnifySubst = Map MetaVarRef ([Id],IdType)
           -- invariant a metavariable never goes to a type containing itself
           -- type variables that fall out of scope could be catched here
           -- for better error messages, or we just catch them in the
           -- resolver later

data St = St
    { st_iscope    :: [(Maybe Name,(Id,IdType))]
    -- ^ Type variables in scope
    , st_scope  :: Scope
    -- ^ Mapping between syntax identifiers to real identifiers their types
--    , st_derivs :: Map Name IdDeriv
--    -- ^ Derivations you can refer to
    , st_mgu    :: UnifySubst
    -- ^ Unification constraints
    }

-- | Report a debug message
report :: String -> Doc -> DsM ()
report s d = tell ([(s,d)])

throw :: Err -> DsM a
throw = throwError

runDsM :: forall a . Map Name (Abs.Type,[Abs.Id]) -> DsM a -> (Either Err a,String)
runDsM as (DsM m)
    = second (\ xs -> unlines [ floc ++ ": " ++ render d | (floc,d) <- xs ])
    . runFreshM
    . flip evalStateT init_st
    . runWriterT
    . runErrorT
    $ runReaderT m as
  where
    init_st :: St
    init_st = St
        { st_iscope = []
        , st_scope = M.empty
        , st_mgu = M.empty
        }

reallyInsertIVar :: Maybe Name -> Id -> IdType -> DsM a -> DsM a
reallyInsertIVar mn i t m = do
    tvs <- gets st_iscope
    modify $ \ st -> st { st_iscope = tvs ++ [(mn,(i,t))] }
    r <- m
    modify $ \ st -> st { st_iscope = tvs }
    return r

reallyInsertTyVar :: Maybe Name -> Id -> DsM a -> DsM a
reallyInsertTyVar mn i m = reallyInsertIVar mn i (T TType) m

-- | Insert a type variable that has already been given a unique id
--  (ok ok this is quite ugly)
reinsertIVar :: Abs.Id -> Id -> IdType -> DsM a -> DsM a
reinsertIVar a i t = reallyInsertIVar (Just (name a)) i t

reinsertTyVar :: Abs.Id -> Id -> DsM a -> DsM a
reinsertTyVar = reallyInsertTyVar . Just . name

-- | Insert a type variable without origin from the source code
addTyVar :: Id -> DsM a -> DsM a
addTyVar = reallyInsertTyVar Nothing

insertTyVar :: Abs.Id -> (Id -> DsM a) -> DsM a
insertTyVar x m = insertIVar x (T TType) m

insertIVar :: Abs.Id -> IdType -> (Id -> DsM a) -> DsM a
insertIVar x t m = snd <$> insertIVar' x t m

insertTyVar' :: Abs.Id -> (Id -> DsM a) -> DsM (Id,a)
insertTyVar' x m = insertIVar' x (T TType) m

insertIVar' :: Abs.Id -> IdType -> (Id -> DsM a) -> DsM (Id,a)
insertIVar' x t m = do
    i <- makeId x
    (,) i <$> reallyInsertIVar (Just (name x)) i t (m i)

insertTyVars :: [Abs.Id] -> ([Id] -> DsM a) -> DsM a
insertTyVars []     m = m []
insertTyVars (x:xs) m = insertTyVar x $ \ x' -> insertTyVars xs $ \ xs' -> m (x':xs')

-- | The 'false' Boson (‽)
false :: Boson
false = False

-- | Looks up an identifier and removes it (it's now used!)
del :: Abs.Id -> DsM (IdType,Id)
del x = do -- TODO: Type
    u <- lkup x
    modScope (M.delete (name x))
    return u

-- | Looks up an identifier but does not remove it (alias)
lkup :: Abs.Id -> DsM (IdType,Id)
lkup x = do
    scope <- getScope
    case M.lookup (name x) scope of
        Just i  -> return i
        Nothing -> throw (msgUnboundIdentifier x)

-- | Inserts an identifier at a type
--   (will throw error instead of shadowing)
insert :: Abs.Id -> IdType -> DsM Id
insert x t = do
    scope <- getScope
    case M.lookup (name x) scope of
        Just (_,i) -> throw (msgAlreadyBound x i)
        Nothing    -> do
            i <- makeId x
            modScope (M.insert (name x) (t,i))
            return i

getScope :: DsM Scope
getScope = gets st_scope

putScope :: Scope -> DsM ()
putScope = modScope . const

modScope :: (Scope -> Scope) -> DsM ()
modScope f = modify $ \ st -> st { st_scope = f (st_scope st) }

idList :: IdList -> [Abs.Id]
idList (OneId x)     = [x]
idList (ConsId x il) = x : idList il

toIdList :: Abs.Id -> [Abs.Id] -> IdList
toIdList x []     = OneId x
toIdList x (y:ys) = ConsId x (toIdList y ys)

