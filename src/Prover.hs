module Main where

import Folly.DSL hiding (neg,(&))
import qualified Folly.DSL as Folly
import Folly.TPTP

import Data.List hiding (all)

import Prelude hiding ((+),(*),all)

[zero,one,top,bot]  = map constant ["zero","one","top","bot"]
[(&),(|||),(+),(*)] = map binary ["with","par","plus","tensor"]
neg                 = unary "neg"

var                 = unary "var"

[bang,quest]        = map unary ["bang","quest"]

[all,ex]            = map binary ["all","ex"]
subst               = trinary "subst"

inhabited = predicate "inhabited"
x <-> y = inhabited x <=> inhabited y

x `lol` y  = neg x ||| y

infixr 9 |||
infixr 8 *
infixr 7 &
infixr 6 +
infix  4 <->

ops =
        [("tensor",(*),one)
        ,("par",   (|||),bot)
        ,("plus",  (+),zero)
        ,("with",  (&),top)
        ]


decls :: [Decl]
decls =
    [ axiom "par_l" $ forall' $ \ g d x y ->
        (inhabited (x * g) /\ inhabited (y * d)) ==>
        inhabited ((x ||| y) * g * d)

    , axiom "par_r" $ forall' $ \ g x y ->
        inhabited ((x ||| y) * g) ==>
        (exists' $ \ d1 d2 ->
            (d1 * d2 === g /\ inhabited (x * d1) /\ inhabited (y * d2))
        )

    , axiom "plus" $ forall' $ \ g x y ->
        (inhabited (x * g) /\ inhabited (y * g)) <=>
        inhabited ((x + y) * g)

    , axiom "with" $ forall' $ \ g x y ->
        (inhabited (x * g) \/ inhabited (y * g)) <=>
        inhabited ((x & y) * g)

    , axiom "bot" $ inhabited bot

    , axiom "zero" $ forall' $ \ g -> inhabited (zero * g)

    , axiom "offer" $ forall' $ \ x us -> inhabited (x * bang us) ==> inhabited (quest x * bang us)
    , axiom "derelict" $ forall' $ \ x g -> inhabited (x * g) ==> inhabited (bang x * g)
    , axiom "wk" $ forall' $ \ x g -> inhabited g ==> inhabited (g * bang x)
    , axiom "alias" $ forall' $ \ x g -> inhabited g ==> inhabited (g * bang x * bang x)

--    , axiom "ax" $ forall' $ \ a -> inhabited (a * neg a)
    , axiom "ax_var" $ forall' $ \ a -> inhabited (var a * neg (var a))

    , axiom "bang_distrib"  $ forall' $ \ a b -> bang a  *   bang b  === bang  (a & b)
    , axiom "quest_distrib" $ forall' $ \ a b -> quest a ||| quest b === quest (a + b)
    ]
    ++ concat
    [ [ axiom (name ++ "_comm")  $ forall' $ commutative op
      , axiom (name ++ "_assoc") $ forall' $ associative op
      , axiom (name ++ "_id")    $ forall' $ identity op i
      ]
    | (name,op,i) <- ops
    ]
    ++ (map (axiom "neg") . concat)
    [ unit_neg zero top
    , unit_neg one bot
    , bin_neg (&) (+)
    , bin_neg (|||) (*)
    , [ forall' $ \ x -> neg (neg x) === x ]
    , [ forall' $ \ x -> neg (neg (var x)) === var x ]
    ]
    {-
    ++ concat
    [ [ axiom (name ++ "_subst")    $ forall' $ \ x y a u -> subst (x `op` y) u a === subst x u a `op` subst y u a
      , axiom (name ++ "_id_subst") $ forall' $ \ u a     -> subst i u a === i
      ]
    | (name,op,i) <- ops
    ]
    ++
    [ axiom "subst"      $ forall' $ \ a u   -> subst (var a) u a === u
    , axiom "subst_miss" $ forall' $ \ b u a -> b != a ==> subst (var b) u a === var b
    , axiom "neg_subst"      $ forall' $ \ a u   -> subst (neg (var a)) u a === neg u
    , axiom "neg_subst_miss" $ forall' $ \ b u a -> b != a ==> subst (neg (var b)) u a === neg (var b)
    ]
    ++ concat
    [ [ axiom ("sub_"     ++ name) $ forall' $ \ a b t u -> a != b ==> subst (q b t) u a === q b (subst t u a)
      , axiom ("subshdw_" ++ name) $ forall' $ \ a t u   -> subst (q a t) u a === q a t
      ]
    | (name,q) <- [("all",all),("ex",ex)]
    ]
    ++
    [ axiom "all" $ forall' $ \ a t u g -> inhabited (subst t u a * g) ==> inhabited (all a t * g)
    , axiom "ex"  $ forall' $ \ a t g -> inhabited (t * g) ==> inhabited (ex a t * g)
    , axiom "neg_all" $ forall' $ \ a t -> neg (all a t) === ex  a (neg t)
    , axiom "neg_ex"  $ forall' $ \ a t -> neg (ex a t)  === all a (neg t)
    ]
    -}
  where
    unit_neg x y = [ neg x === y , neg y === x ]
    bin_neg (&&) (++) =
        [ forall' (\ x y -> neg (x && y) === neg x ++ neg y)
        , forall' (\ x y -> neg (x ++ y) === neg x && neg y)
        ]

main = outputTPTP $ (:decls) $
    -- conjecture' $ inh [one * (bot ||| zero) * one]
    -- conjecture' $ inh [neg (all a (all b ((var a `lol` var b) `lol` (var a `lol` var b))))]
    -- conjecture' $ inh [all a (all b (var a ||| var b))]
    ---conjecture' $ inh [neg (bang bot * quest one)]
    conjecture' $ inh [bang bot * quest one]
    -- conjecture' $ forall' $ \ a -> inh [var a * neg (var a)]
    -- conjecture' $ inh [neg ((var a `lol` var a)))]
    -- conjecture' $ forall' $ \ a b -> inh [((var a `lol` var b) `lol` (var a `lol` var b))]
    -- axiom' $ forall' $ \ a -> a === a
  where
    inh = inhabited . foldr1 (*)
    [a,b,c] = map (constant . return) "abc"





