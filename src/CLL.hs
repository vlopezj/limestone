{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ViewPatterns #-}

module CLL where

import Prelude hiding (mapM)
import LL hiding (halt)
import Type.Ctx
import Fresh
import Control.Applicative
import Control.Monad
import Control.Lens hiding (Choice)
import Control.Monad.Writer
import qualified Data.Traversable as T
import Data.Traversable (traverse)
import Eval (evaluateF)
import Data.List as List
import Development.Placeholders
import Data.Default
import Control.Monad.Except.Extra
import Control.Arrow ((&&&))
-- import PrettyId

--d0M name ctx = runFreshM . unsafeExceptT . (>>= makeBindersSafe) . lift . fmap (d0 name ctx)
type Program name ref = FreshM (Seq' name ref)

instance Monoid (Program') where
  mempty = return mempty
  mappend = liftM2 mappend
  mconcat = fmap mconcat . T.sequence

type Program' = Program Id Id

tInt = T PInt
tDouble = T PDouble
tComplex = T PComplex
tChar = T PChar
tExists :: (MonadFresh m) => String -> IdType -> (Id -> m IdType) -> m IdType
tExists name ty κ = do
  v <- freshFrom name
  T . Exists v ty <$> κ v

ffiCall :: Type' Id Id
        -> String -> [Id]
        -> (Id -> Program Id Id)
        -> Program Id Id
ffiCall ty str xs = fuse ty (ffiCall' str xs)

ffiCall' str xs z' = pure $ S0 $ What (FFI (Call str)) (List.map (ElemLinear) (z':xs)) [0 .. length xs]


-- ^ Shorthand to call a procedure returning "void"
ffiProc :: String -> [Id]
        -> Program Id Id
ffiProc str xs = ffiCall (T One) str xs (flip ignore halt)

ffiProc1 :: String -> Id -> Program Id Id
ffiProc1 str = ffiProc str . (:[])

ffiLit  :: String ::: Type' Id Id -> (Id -> Program Id Id) -> Program Id Id
ffiLit (str ::: ty) = fuse ty (ffiLit' (str ::: ty))

ffiLit'  :: String ::: Type' Id Id -> Id -> Program Id Id
ffiLit' (str ::: ty) z' = pure $ S0 $ What (FFI (PrimLit str)) [ElemLinear z'] [0]

ffiInt :: Integer -> (Id -> Program Id Id) -> Program Id Id
ffiInt i = ffiLit (show i ::: T PInt)

ffiDouble :: Double -> (Id -> Program Id Id)  -> Program Id Id
ffiDouble i = ffiLit (show i ::: T PDouble)

fPlus  ty a b = ffiCall ty "+" [a,b]
fMinus ty a b = ffiCall ty "-" [a,b]
fTimes ty a b = ffiCall ty "*" [a,b]
fDiv   ty a b = ffiCall ty "/" [a,b]
fNeg   ty a   = ffiCall ty "neg" [a]

fPlusD  a b = ffiCall (T PDouble) "+" [a,b]
fPlusD'  a b = ffiCall' "+" [a,b]
fMinusD a b = ffiCall (T PDouble) "-" [a,b]
fTimesD a b = ffiCall (T PDouble) "*" [a,b]
fTimesD' a b = ffiCall' "*" [a,b]
fDivD   a b = ffiCall (T PDouble) "/" [a,b]
fNegD   a   = ffiCall (T PDouble) "neg" [a]

fCmpGeD   a b = ffiCall bool ">=" [a,b]
fCmpLeD   a b = ffiCall bool "<=" [a,b]
fCmpGtD   a b = ffiCall bool ">"  [a,b]
fCmpLtD   a b = ffiCall bool "<"  [a,b]
fCmpEq    a b = ffiCall bool "=="  [a,b]

ffiChar :: Char -> (Id -> Program Id Id) -> Program Id Id
ffiChar c = ffiLit (show c ::: T PChar)          

sizeAsDouble i = fuse tDouble (\z' -> pure $ S0 $ What (FFI (Call "(double)")) [ElemLinear z',ElemIntuit (T (SizeT i)) (T TSize)] [0,1])

derivCall :: (Monad m) => Name   -- ^ Name of the derivation
          -> [ref ::: Type' name ref]  -- ^ For each Id, put the argument into the context with the id, and run the program
          -> [ref]  -- ^ For each Id, Instantiations for regular variables 
          -> m (Seq' name ref)
derivCall name i p = return (S0 (What (LLDeriv name)
                                 ([ElemIntuit (var v) ty | v ::: ty <- i] ++
                                  [ElemLinear v | v <- p]) [0..(length i + length p - 1)]))

-- ^ Load type or size
                     {-
saveDep :: String -> (IdType ::: IdType) -> (Id -> Program') -> Program' 
saveDep α_str (ty ::: k) κ = do
  [α ,α₁] <- mapM freshFrom [α_str,α_str]
  [o₁,o₂] <- mapM freshFrom ["o","o"]
  res <- cut (T (Forall α k (T Bot))) (\x -> s0$ TUnpack x α₁ o₁  . S' . SOne False o₁ <$> rM (κ α₁))
                       (\y -> pure $ S0 (TApp False y ty o₂ (S' (SBot o₂))))
  return $ res & _Wrapping Seqn0 . LL.seq . cutInfo .~ Polymorphic
-}

infoCut :: CutInfo -> Program' -> Program'
infoCut ci0 = fmap$ _Wrapping Seqn0 . LL.seq . cutInfo .~ ci0
  

-- ^ Kinds can depend on other types.
saveContext :: [(Name, IdType ::: ([Id] -> IdType))]
            -> ([IdType] -> [Id ::: IdSized])
            -> ([Id ::: IdType] -> [Id] -> Program')
            -> Program'
saveContext qs xs prog = do
  ctxty <- contextToType [(v,k) | (v, _ ::: k) <- qs]
                         (\vs -> [tysz | _ ::: tysz <- xs [var v | v <- vs]])

  infoCut Polymorphic $ cut ctxty
    (\c' -> let tys = [ty | (_, ty ::: _) <- qs] in
            packContext c' tys [(v,sz) | v ::: _ :^ sz <- xs tys])
    (\c -> unpackContext c
                         [k | (_,_ ::: k) <- qs]
                         (\vs -> [sz | _ ::: _ :^ sz <- xs [var v | v <- vs]])
                         prog)
  
  where
    contextToType :: (MonadFresh m) => [(Name, [Id] -> IdType)] -> ([Id] -> [IdSized]) -> m (IdType)
    contextToType = go []
      where
        go scope [] cont = return$ linearContextToType $ cont scope
        go scope ((name, f):qs) cont = do
          v <- freshFrom name
          rest <- go (scope ++ [v]) qs cont
          return$ T$ Exists v (f scope) rest 

    -- translate a linear context [A_i^n_i] into a type of the form '⨂_(n_i) A_i ⊗ (… ⊗ 1)'
    -- if n_i = 1, then the tensor is omitted
    linearContextToType :: [IdSized] -> IdType
    linearContextToType ctx = case ctx of
      [] -> T$ One
      (ty :^ 1):ctx  -> T$ ty              :*: linearContextToType ctx
      (ty :^ sz):ctx -> T$ (T$ ArrayTensor sz ty) :*: linearContextToType ctx

    unpackContext :: Id -> [[Id] -> IdType] -> ([Id] -> [IdSize]) -> ([Id ::: IdType] -> [Id] -> Program') -> Program' 
    unpackContext = go []
      where
        go scope a [] ctx' cont = unpackLinearContext a (ctx' (fmap erase scope)) (cont scope)
        go scope a (k:rest) ctx' cont =
          let kind = k (fmap erase scope) in
          unpack a kind $ \ty v -> go (scope ++ [ty ::: kind]) v rest ctx' cont

    unpackLinearContext :: Id -> [IdSize] -> ([Id] -> Program') -> Program' 
    unpackLinearContext = go []
      where
        go scope a [] f    = ignore a $ f scope
        go scope a (sz:rest) f = tensor a $ \v a' ->
          case sz of
            1 -> go (scope ++ [v]) a' rest f  
            _ -> slice v $ \v' -> go (scope ++ [v']) a' rest f  

    packContext :: Id -> [IdType] -> [(Id, IdSize)] -> Program'
    packContext = go 
      where
        go a [] lin = packLinearContext a lin
        go a (ty:rest) lin = tApp a ty $ \a' -> go a' rest lin

    packLinearContext :: Id -> [(Id,IdSize)] -> Program' 
    packLinearContext a [] = yield a 
    packLinearContext a ((v,1):vs) = par a (axiom v)  
                                           (\a' -> packLinearContext a' vs)
    packLinearContext a ((v,sz):vs) = par a (\x  -> coslice x sz (axiom v))
                                            (\a' -> packLinearContext a' vs)

unpack :: Id -> IdType -> (Id -> Id -> Program') -> Program'
unpack zElim tyKind cont' = do
  [tyIntr,xIntr] <- mapM freshFrom ["β","x"]
  S cont <- cont' tyIntr xIntr 
  return$ S0$ TUnpack{..}

tApp :: Id -> IdType -> (Id -> Program') -> Program'
tApp zElim ty cont' = do
  let β = False
  xIntr <- freshFrom "x"
  S cont <- cont' xIntr
  return$ S0$ TApp{..}
  

saveDep :: String -> (IdType ::: IdType) -> (Id -> Program') -> Program' 
saveDep n (ty ::: k) cont = saveContext [(n, ty ::: (\[] -> k))] (\_ -> []) (\[ident] [] -> cont (erase ident))

saveType ty = saveDep "α" (ty ::: T TType)
saveSize sz = saveDep "σ" (sz ::: T TSize)
                                                     
sizeCompare :: (Applicative m) => Size Id -> Size Id -> (Ordering -> m (Seq' Id Id)) -> m (Seq' Id Id)
sizeCompare sl sr f = s0 $ SizeCmp sl sr <$> T.sequenceA [(Lt,) <$> rM (f LT)
                                                         ,(Eq,) <$> rM (f EQ)
                                                         ,(Gt,) <$> rM (f GT)
                                                         ]

axiom :: (Applicative m) => Id -> Id -> m (Seq' Id Id)
axiom x y = s0 . pure $ LL.Ax x y

axiomAll :: (MonadFresh m) => IdSize -> Id -> Id -> m (IdSeq)
axiomAll sz r1 r2 = axiom r1 r2 >>= resizeByM sz

whatever :: String -> Id -> Program'
whatever s x = whatevers s [x]

whatevers' s xs x = whatevers s (x:xs)

whatevers :: String -> [Id] -> Program'
whatevers s vars = s0 . pure $ What (Huh s) (fmap ElemLinear vars) []

whataxiom :: Id -> Id -> Program'
whataxiom x y = whatevers "ax" [x,y]

load :: Id -> (Id -> Program') -> Program'
load ref cont1 = s0 $ do
  name <- freshId
  Load ref sizeOne name <$> (rM $ cont1 name)

ignore :: Id -> Program' -> Program'
ignore x k = s0 $ SOne False x <$> rM k

resume :: Id -> Program'
resume x = s0 $ pure $ SBot x

halt :: Program'
halt = CLL.halts []

halts1 :: Id -> Program'
halts1 = CLL.halts . (:[])

halts :: [Id] -> Program'
halts refs = s0 $ pure $ Halt refs

yield :: Id -> Program'
yield x = s0 $ pure $ SBot x
 
par :: Id
       -> (Id -> Program Id Id)
       -> (Id -> Program Id Id)
       -> Program Id Id 
par ref cont1 cont2 = s0 $ do
  --[name1,name2] <- freshIds 2
  name1 <- freshFrom "τ"
  name2 <- freshFrom "σ"
  Par False ref <$> pure name1 <*> (rM $ cont1 name1)
                <*> pure name2 <*> (rM $ cont2 name2)

tensor :: Id -> (Id -> Id -> Program Id Id) -> Program Id Id
tensor ref cont12 = do
  [name1,name2] <- freshIds 2
  tensorNames name1 name2 ref cont12

tensor' :: String -> String
           -> Id
           -> (Id -> Id -> Program Id Id)
           -> Program Id Id
tensor' s t ref cont12 = do
  [name1,name2] <- mapM freshFrom [s,t]
  tensorNames name1 name2 ref cont12

tensorNames :: Id -> Id -> Id -> (Id -> Id -> Program Id Id) -> Program Id Id
tensorNames name1 name2 ref cont12 = 
  s0 $ Cross False ref name1 name2 <$> (rM$ cont12 name1 name2)

coslice :: Id -> Size Id -> (Id -> Program Id Id) -> Program Id Id
--coslice z 1 b = b z
coslice z sz b = elimBigPar z [(sz,b)]


coslice' :: Id -> Size Id -> (Id -> Program Id Id) -> Program Id Id
coslice' z 1 b = b z
coslice' z sz b = elimBigPar z [(sz,b)]

elimBigPar :: Id -> [(Size Id, Id -> Program Id Id)] -> Program Id Id
elimBigPar z bs = s0$ BigPar z <$> T.traverse go bs
  where go (sz,b) = do
          name <- refreshId z
          p <- rM$ b name
          return (sz,Branch name p)

permute :: Size Id -> Id -> (Id -> Program Id Id) -> Program Id Id
permute newSz zElim k = s0$ do
  xIntr <- refreshId zElim
  cont <- rM$ k xIntr
  return $ Permute{..}

-- Apply the big tensor rule; ⊗n(A) -> A^n
slice :: Id -> (Id -> Program Id Id) -> Program Id Id
slice ref cont1 = s0$ do
  name <- refreshId ref
  BigTensor ref name <$> (rM$ cont1 name)

cut :: Type' Id Id -> (Id -> Program Id Id) -> (Id -> Program Id Id) -> Program Id Id
cut ty cont1 cont2 = do
  name1 <- freshFrom "v"
  name2 <- freshFrom "w"
  cut1 name1 name2 ty <$> (cont1 name1) <*> (cont2 name2)

cut' :: String -> IdType -> (Id -> Program') -> (Id -> Program') -> Program'
cut' s ty cont1 cont2 = do
  [name1,name2] <- mapM freshFrom [s,"_" <> s]
  cut1 name1 name2 ty <$> (cont1 name1) <*> (cont2 name2)


fuse' :: String -> IdType -> (Id -> Program') -> (Id -> Program') -> Program'
fuse' s ty cont1 cont2 = do
  [name1,name2] <- mapM freshFrom [s,"_" <> s]
  fuse1 name1 name2 ty <$> (cont1 name1) <*> (cont2 name2)

fuse :: IdType -> (Id -> Program') -> (Id -> Program') -> Program'
fuse ty cont1 cont2 = do
  [name1,name2] <- freshIds 2
  fuse1 name1 name2 ty <$> (cont1 name1) <*> (cont2 name2)

compose :: IdType -> (Id -> Id -> Program') -> (Id -> Id -> Program') -> Id -> Id -> Program'
compose ty cont1 cont2 input output = do
  cut ty (cont1 input) (\mid -> cont2 mid output)

cutMany :: IdSize -> IdType -> (Id -> Program') -> (Id -> Program') -> Program'
cutMany n ty cont1 cont2 = do
  [name1,name2] <- freshIds 2
  cutNl n name1 name2 ty <$> (cont1 name1) <*> (cont2 name2)

fuseMany :: IdSize -> IdType -> (Id -> Program') -> (Id -> Program') -> Program'
fuseMany n ty cont1 cont2 = do
  [name1,name2] <- freshIds 2
  fuseNl n name1 name2 ty <$> (cont1 name1) <*> (cont2 name2)

with :: Choice -> Id -> (Id -> Program') -> Program'
with choose zElim κ = do
  xIntr <- refreshId zElim
  (\(…) -> S0 With{β = defaultBoson, cont = (…), ..}) <$> rM (κ xIntr)

withL = with Inl
withR = with Inr

-- similar to axiom on an array, but the tensor part is already eliminated.
axiomArr sz elements parArr = coslice parArr sz $ axiom elements


zip :: IdSize -> Id -> Id -> Program'
zip sz inputs result =
  tensor inputs $ \inputL inputR ->
  slice inputL $ \a ->
  slice inputR $ \b ->
  coslice result sz $ \c ->
  par c (axiom a) (axiom b)

zipWith :: Size Id -> String -> Id -> Id -> Id -> Id -> Program Id Id
zipWith sz fctName fct inputL inputR result =
  slice inputL $ \a ->
  slice inputR $ \b ->
  coslice result sz $ \c ->
  whut fctName [a,b,c]
  -- load fct $ \f ->
  -- par f (axiom a)
  --    (\f' -> par f' (axiom b)(axiom c))

zipWithDeriv :: Deriv' Id Id
zipWithDeriv = runFreshM $ D0 <$> do
  [xs,ys,zs,f] <- mapM freshFrom ["xs","ys","zs","f"]
  [a,b,c] <- mapM freshFrom ["A","B","C"]
  let [tyA,tyB,tyC] = fmap var [a,b,c]
  n <- freshFrom "n"
  let szN = var n
  Deriv "zipWith(f)"
    def{ _intuit = [v :::. T TType | v <- [a,b,c]]
                ++ [n :::. T TSize]
       , _linear =  -- [(fct,(a ⊸ b ⊸ c) :^ sizeOne)]
                 [xs :::. bigTensor szN tyA,
                  ys :::. bigTensor szN tyB,
                  zs :::. neg (bigTensor szN tyC)] } <$>
    CLL.zipWith szN "f" f xs ys zs

-- map :: IdType -> Id -> Id -> Id -> Program'
map :: Size Id -> String -> Id -> Id -> Id -> Program Id Id
map sz fname _fun input result =
  slice input $ \a ->
  coslice result sz $ \r ->
  whut fname [a,r]
  -- load fun $ \f ->
  -- par f (axiom a)
  --       (axiom b)

mapSeqSimple :: Size Id -> String -> Id -> Id -> Id -> Program Id Id
mapSeqSimple sz fname _fun input result =
  CLL.bigseq [ CLL.seqstep sz [input,result] $ \[a,b] -> whut fname [a,b] ]

mapInline :: IdSize -> (Id -> Id -> Program')-> (Id -> Id -> Program')
mapInline sz fct input result =
  slice input $ \a ->
  coslice result sz $ \b ->
  fct a b

mapN :: Size Id -> Id -> Id -> Id -> Program Id Id
mapN sz f input result =
  slice input $ \a ->
  coslice result sz $ \b ->
  par f (axiom a)
        (axiom b)

mapDeriv = runFreshM $ do
  [xs,ys,f] <- mapM freshFrom ["x","y","f"]
  [a,b] <- mapM freshFrom ["A","B"]
  let [tyA,tyB] = fmap var [a,b]
  n <- freshFrom "n"
  let szN = var n
  D0 . Deriv "map(f)" def{ _intuit = [v :::. T TType | v <- [a,b]] <>
                             [n :::. T TSize]
                 , _linear = [xs :::. bigTensor szN tyA
                             ,ys :::. neg (bigTensor szN tyB)] }
    <$> CLL.map szN "f" f xs ys

mapSeqSimpleDeriv = runFreshM $ do
  [xs,ys,f] <- mapM freshFrom ["x","y","f"]
  [a,b] <- mapM freshFrom ["A","B"]
  let [tyA,tyB] = fmap var [a,b]
  n <- freshFrom "n"
  let szN = var n
  D0 . Deriv "map-seq-simple" def{ _intuit = [v :::. T TType | v <- [a,b]] <>
                             [n :::. T TSize]
                 , _linear = [xs :::. bigTensor szN tyA
                             ,ys :::. neg (bigTensor szN tyB)] }
    <$> CLL.mapSeqSimple szN "f" f xs ys


mapNDeriv :: IdDeriv
mapNDeriv = D0 $ runFreshM $ do
  input  <- freshFrom "xs"
  result <- freshFrom "ys"
  fun <- freshFrom "f"
  n <- freshFrom "n"
  let sz = var n
  a   <- freshFrom "A"
  b   <- freshFrom "B"
  let tyA = var a
  let tyB = var b
  Deriv "mapF" def{  _intuit = [v :::. T TType | v <- [a,b]] <>
                              [n :::. T TSize]
                  , _linear =
                     [fun   :::. bigTensor sz (tyA ⊸ tyB)
                     ,input :::. bigTensor sz tyA
                     ,result :::. neg (bigTensor sz tyB)]
                 }
    <$> (
    slice fun $ \fun ->
    CLL.mapN sz fun input result)

foldMap :: Id -- ^ size
        -> Id -- ^ type
        -> Type' Id Id -- ^ monoid type
        -> (Id -> Id -> Id -> FreshM (Seq' Id Id))
        -> (Id -> Id -> FreshM (Seq' Id Id))
        -> (Id -> FreshM (Seq' Id Id))
        -> FreshM (Seq' Id Id)
foldMap fmSz x monoid combiner mapper cont = do
--  [fmMapTrg,fmMixL,fmMixR,fmMixTrg, fmMon] <- freshIds 5
  fmMapTrg <- freshFrom "α"
  fmMixL   <- freshFrom "β"
  fmMixR   <- freshFrom "γ"
  fmMixTrg <- freshFrom "ε"
  fmMon    <- freshFrom "ζ"

  fmMix <- rM$ combiner fmMixL fmMixR fmMixTrg
  fmCont <- rM$ cont fmMon
  slice x $ \x' -> do
    fmMap <- rM$ mapper x' fmMapTrg
    return $ S0 $ Foldmap {
      fmTyM = monoid,
      fmArr = [x'],
      ..
      }

foldMap2 :: Size Id
            -> Id
            -> Type' Id Id
            -> (Id -> Id -> Id -> Program Id Id)
            -> (Id -> Id -> Program Id Id)
            -> (Id -> Program Id Id)
            -> (Id -> Program Id Id)
            -> Program Id Id
foldMap2 fmSize x fmTyM combiner mapper uniter cont = do
  fmMapTrg <- freshFrom "α"
  fmMixL   <- freshFrom "β"
  fmMixR   <- freshFrom "γ"
  fmMixTrg <- freshFrom "ε"
  fmMon    <- freshFrom "ρ"
  fmUnitTrg <- freshFrom "z"
  foldMap2Names fmMapTrg fmMixL fmMixR fmMixTrg fmMon fmUnitTrg
    fmSize x fmTyM combiner mapper uniter cont

foldMap2Names fmMapTrg fmMixL fmMixR fmMixTrg fmMon fmUnitTrg
    fmSize x fmTyM combiner mapper uniter cont = do
  fmMix <- rM$ combiner fmMixL fmMixR fmMixTrg
  fmCont <- rM$ cont fmMon
  fmUnit <- rM$ uniter fmUnitTrg
  slice x $ \x' -> do
    fmMap <- rM$ mapper x' fmMapTrg
    return $ S0 Foldmap2 {
      fmArr = [x'],
      ..
      }


foldMap2' :: Size Id
            -> [Id]
            -> Type' Id Id
            -> (Id -> Id -> Id -> Program Id Id)
            -> ([Id] -> Id -> Program Id Id)
            -> (Id -> Program Id Id)
            -> (Id -> Program Id Id)
            -> Program Id Id
foldMap2' fmSize xs fmTyM combiner mapper uniter cont = do
  [fmMapTrg,fmMixL,fmMixR,fmMixTrg, fmMon, fmUnitTrg] <- mapM freshFrom ["t","u","v","x","y","z"]
  fmMix <- rM$ combiner fmMixL fmMixR fmMixTrg
  fmCont <- rM$ cont fmMon
  fmUnit <- rM$ uniter fmUnitTrg
  fmMap <- rM$ mapper xs fmMapTrg
  return $ S0 Foldmap2 {
    fmArr = xs,
    ..
    }

merge :: IdSize -> Id -> Id -> (Id -> Program') -> Program'
merge sz1 r1 r2 cont = do
  m <- freshFrom "m"
  (\(S cont) ->
    S0 Merge { zElim = r1
          , wElim = r2
          , xIntr = m
          , splitSz = sz1
          , cont
          }) <$> cont m 

-- ⨂2n(C) -> ⨂nC ⊗ ⨂nC
halve :: IdSize -> Id -> Id -> Program'
halve sz input result =
  slice input $ \z ->
  CLL.splitAt sz z $ \x y ->
  par result
    (\ w -> coslice w sz $ axiom x)
    (\ w -> coslice w sz $ axiom y)

--  ⨂nC ⊗ ⨂nC -> ⨂ 2n(C)
unhalve :: IdSize -> Id -> Id -> Program'
unhalve sz input output =
  tensor input $ \input₁ input₂ ->
  slice input₁ $ \input₁ -> 
  slice input₂ $ \input₂ -> 
  merge sz input₁ input₂ $ \input ->
  coslice output (2 * sz) $ axiom input


mix :: Program' -> Program' -> Program'
mix k1 k2 = LL.mix <$> k1 <*> k2

schedule :: Id -> Id -> Id -> Program'
schedule logsz input result =
  slice input $ \i ->
  slice result $ \r ->
  do [fmMapTrg,fmMixL,fmMixR,fmMixTrg, fmMon] <- freshIds 5
     fmMix <- rM$ ignore fmMixL $
              ignore fmMixR $
              resume fmMixTrg
     fmCont <- rM$ ignore fmMon $ halt
     fmMap <- rM$ CLL.mix (axiom i r) (resume fmMapTrg)
     return $ S0 Foldmap {
      fmSz = logsz,
      fmTyM = T One,
      fmArr = [i,r],
      ..
      }

bareSchedule :: IdSize -> Id -> Program'
bareSchedule fmSize result =
  slice result $ \r ->
  CLL.bigseq [ CLL.seqstep fmSize [] $ \[] -> yield r ] 
{-
  do fmMapTrg  <- freshFrom "a"
     fmMixL    <- freshFrom "l"
     fmMixR    <- freshFrom "r"
     fmMixTrg  <- freshFrom "b"
     fmMon     <- freshFrom "y"
     fmUnitTrg <- freshFrom "z"
     fmMix <-  rM$ CLL.mix (yield fmMixL) (axiom fmMixR fmMixTrg)
     fmCont <- rM$ yield fmMon
     fmMap <- rM$ axiom fmMapTrg r
     fmUnit <- rM$ ignore fmUnitTrg $ halt
     return $ S0 Foldmap2 {
      fmTyM = T Bot,
      fmArr = [r],
      ..
      }
-}
bareScheduleDeriv :: Deriv' Id Id
bareScheduleDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"
  let sz = var n
  input <- freshFrom "xs"
  Deriv "schedule" def{ _intuit = [n :::. T TSize]
                      , _linear = [input :::. bigTensor sz (T Bot)] } <$> bareSchedule sz input

schedule2 :: IdSize -> Id -> Id -> Program'
schedule2 fmSize input result =
  slice input $ \i ->
  slice result $ \r ->
  do [fmMapTrg,fmMixL,fmMixR,fmMixTrg, fmMon, fmUnitTrg] <- freshIds 6
     fmMix <- rM$ ignore fmMixL $
              ignore fmMixR $
              resume fmMixTrg
     fmCont <- rM$ ignore fmMon $ halt
     fmMap <- rM$ CLL.mix (axiom i r) (resume fmMapTrg)
     fmUnit <- rM$ yield fmUnitTrg
     return $ S0 Foldmap2 {
      fmTyM = T One,
      fmArr = [i,r],
      ..
      }

schedule2Deriv :: Deriv' Id Id
schedule2Deriv = D0 $ runFreshM $ do
  n <- freshFrom "n"
  let sz = var n
  a <- freshFrom "A"
  let tyA = var a
  input <- freshFrom "x"
  result <- freshFrom "y"
  Deriv "schedule2" def{ _intuit = [ a :::. T TType, n :::. T TSize ]
                      , _linear =
                         [input ::: bigTensor sz tyA :^ sizeOne
                         ,result ::: neg (bigPar sz tyA) :^ sizeOne]
                      } <$>
    schedule2 sz input result

schedule3 :: Bool -> Int -> IdSize -> Id -> Id -> Program'
schedule3 inlineSched steps fmSize input result = evaluateF [] steps =<<
  (slice input $ \i ->
    slice result $ \r ->
    fuse' "xs" (neg $ bigTensor fmSize (T Bot)) (if inlineSched then bareSchedule fmSize else \x -> s0 $ pure$ What (Builtin BIFSchedSeq) [ElemLinear x] []) (\s -> coslice s fmSize $ \ss -> ignore ss $ axiom i r ))

seqSchedule :: IdSize -> Program' -> Program' 
seqSchedule n p = CLL.bigseq [ seqstep n [] $ \[] -> p ]

bifScheduleSeq ::IdSize -> Program' -> Program'
bifScheduleSeq sz cont = fuse' "sched" (neg $ bigTensor sz (T Bot))
                    (\x -> s0 $ pure$ What (Builtin BIFSchedSeq) [ElemLinear x] [])
                    (\s -> coslice s sz $ \ss -> ignore ss $ cont )

schedule3Ev :: IdSize -> Id -> Id -> Program'
schedule3Ev = schedule3 True 4

schedule3Deriv :: Bool -> Int -> Deriv' Id Id
schedule3Deriv inlineSched steps = D0 $ runFreshM $ do
  n <- freshFrom "n"
  let sz = var n
  a <- freshFrom "A"
  let tyA = var a
  input <- freshFrom "x"
  result <- freshFrom "y"
  Deriv "schedule3" def{ _intuit = [ a :::. T TType, n :::. T TSize ]
                      , _linear =
                         [input ::: bigTensor sz tyA :^ sizeOne
                         ,result ::: neg (bigPar sz tyA) :^ sizeOne]
                      } <$>
    schedule3 inlineSched steps sz input result

freezeK :: Size Id -> Size Id -> Type' Id Id -> Id -> Id -> FreshM (Seq' Id Id) 
freezeK cnt sz typ input output = do
  x <- freshFrom "vf"
  y <- freshFrom "wf"
  freezeSeq1 cnt sz typ x y <$> (coslice input sz $ axiom x) <*> (coslice output (sz * cnt) $ axiom y)

freeze :: Size Id -> Type' Id Id -> Id -> Id -> FreshM (Seq' Id Id)
freeze = freezeK 1

freeze' :: String -> Size Id -> Type' Id Id -> Id -> Id -> FreshM (Seq' Id Id)
freeze' s sz typ input output = do
  [x,y] <- mapM freshFrom [s,"_" <> s]
  freezeSeq1 1 sz typ x y <$> (coslice input sz $ axiom x) <*> (coslice output sz $ axiom y)


freezeDerivK :: Deriv' Id Id
freezeDerivK = D0 $ runFreshM $ do
  k <- freshFrom "k"
  let cnt = var k
  n <- freshFrom "n"
  let sz = var n
  a <- freshFrom "A"
  let tyA = var a
  input <- freshFrom "x"
  result <- freshFrom "y"
  Deriv "freeze" def{ _intuit = [ a :::. T TType, n :::. T TSize, k :::. T TSize ]
                    , _linear = [input ::: bigPar sz tyA :^ sizeOne
                                ,result ::: neg (bigTensor (sz * cnt) tyA) :^ sizeOne
                                ] } <$> freezeK cnt sz tyA input result

freezeDeriv2 :: Deriv' Id Id
freezeDeriv2 = D0 $ runFreshM $ do
  let cnt = 2 
  let sz = 2 
  -- sz <- var <$> freshFrom "n"
  a <- freshFrom "A"
  let tyA = var a
  input <- freshFrom "x"
  result <- freshFrom "y"
  Deriv "freeze" def{ _intuit = [ a :::. T TType ]
                    , _linear = [input ::: bigPar sz tyA :^ sizeOne
                                ,result ::: neg (bigTensor (sz * cnt) tyA) :^ sizeOne
                                ] } <$> freezeK cnt sz tyA input result


freezeDeriv :: Deriv' Id Id
freezeDeriv = D0 $ runFreshM $ do
  (n,sz) <- (id &&& var) <$> freshFrom "n"
  (a,tyA) <-  (id &&& var) <$> freshFrom "A"
  input <- freshFrom "xs"
  result <- freshFrom "ys"
  Deriv "freeze" def { _intuit = [ a :::. T TType, n :::. T TSize ]
                     , _linear =
                          [input ::: bigPar sz tyA :^ sizeOne
                          ,result ::: neg (bigTensor sz tyA) :^ sizeOne
                          ] } <$> freeze' "z" sz tyA input result

fftStepDeriv evalSteps = D0 $ runFreshM $ do
  [input,result] <- freshIds 2
  bff <- freshFrom "f"
  n <- freshFrom "n"
  let sz = SizeExp $ var n
      sz2 = sizeMul (sizeLit 2) sz
  c_ <- freshFrom "C"
  let c = var c_
  let fftStep = evaluateF [] evalSteps =<<
          (compose (bigTensor sz c ⊗ bigTensor sz c) (CLL.halve sz) $
               compose (bigTensor sz (c ⊗ c)) (CLL.zip sz) $
               compose (bigTensor sz (c ⅋ c)) (CLL.map sz "bff" bff) $
               compose (bigPar sz (c ⅋ c)) (CLL.schedule n) $
               compose (bigPar sz c ⅋ bigPar sz c) (flip (CLL.zip sz)) $
               compose (bigPar sz2 c) (flip (CLL.halve sz)) $
               freeze sz2 c
              ) input result
  Deriv "fftStep" def{ _intuit = [ n :::. T TSize
                                 , c_ :::. T TType
                                 , bff ::: (c ⊗ c) ⊸ (c ⅋ c) :^ sizeOne]
                     , _linear = [input ::: bigTensor sz2 c :^ sizeOne
                                  ,result ::: neg (bigTensor sz2 c) :^ sizeOne
                                  ] } <$> fftStep 


bff i o = 
  tensor i $ \x y ->
  tensor o $ \z w ->
  whut "bff" [x,y,z,w]
    
  

fftStep2Deriv evalSteps = D0 $ runFreshM $ do
  [input,result] <- mapM freshFrom ["i","o"]
  -- bff <- freshFrom "f"
  (n,sz) <- (id &&& var) <$> freshFrom "n"
  let sz2 = sizeMul (sizeLit 2) sz
  (c_,c) <- (id &&& var) <$> freshFrom "C"
  let fftStep = evaluateF [] evalSteps =<<
          (compose (bigTensor sz c ⊗ bigTensor sz c) (CLL.halve sz) $
               compose (bigTensor sz (c ⊗ c)) (CLL.zip sz) $
               compose (bigTensor sz (c ⅋ c)) (CLL.mapInline sz bff) $
               compose (bigPar sz (c ⅋ c)) (CLL.schedule3Ev sz) $
               compose (bigPar sz c ⅋ bigPar sz c) (flip (CLL.zip sz)) $
               compose (bigPar sz2 c) (flip (CLL.halve sz)) $
               freeze sz2 c
              ) input result
  Deriv "fftStep" def{
                  _intuit = [ n  :::. T TSize
                            , c_ :::. T TType ]
                , _linear = 
                  [ input ::: bigTensor sz2 c :^ sizeOne
                  , result ::: neg (bigTensor sz2 c) :^ sizeOne
                  ] } <$> fftStep 

-- Normalized version of fftStep2Deriv
fftStep2DerivEval = D0 $ runFreshM $ do
  [input,result] <- mapM freshFrom ["i","o"]
  n <- freshFrom "n"
  c_ <- freshFrom "C"
  let sz = var n 
  let sz2 = 2 * sz
  let c = var c_
  Deriv "fftStep" (seqctx [(n, T TSize :^ 1)
                          ,(c_, T TType :^ 1)]
    [(input,bigTensor sz2 c :^ sizeOne)
    ,(result, neg (bigTensor sz2 c) :^ sizeOne)
    ]) <$> (slice input $ \i ->
           CLL.splitAtS "j" "k" sz i $ \j k ->
           sync (c :^ sz2)
           (\s -> CLL.splitAtS "v" "w" sz s $ \v w -> 
             foldMap2' sz [j,k,v,w] c
                    (\foo bar baz -> CLL.mix (yield foo) (axiom bar baz))
                    (\args u -> ignore u $ (whut "bff" args))
                    (\b -> ignore b $ halt)
                    (\a -> yield a)
           )
           (\t -> coslice result sz2 (\o -> axiom t o)))

type CPLL = (Id -> Program') -> Program'
infixl `app`
infix 0 =: 

app :: CPLL -> Id -> CPLL
app fun arg r =
  fun $ \f ->
    par f (axiom arg)
          r

app' :: CPLL -> CPLL -> CPLL
app' fun arg r =
  fun $ \f ->
    par f (arg . axiom)
          r

(=:) :: Id -> CPLL -> Program'
x =: expr = expr (axiom x)

sum sz ty zero plus input result =
  do x <- freshFrom "x"
     a <- freshFrom "a"
     b <- freshFrom "b"
     c <- freshFrom "c"
     s <- freshFrom "s"
     z <- freshFrom "z"
     foldMap2Names x a b c s z 
                  sz input ty
                  -- (\x' y' r -> r =: load plus `app` x' `app` y')
                  (\x' y' r -> whut "(+)" [x',y',r])
                  axiom
                  -- (\res -> load zero $ \z -> axiom res z)
                  (\res -> whut "0" [res])
                  (axiom result)

sumDeriv :: IdDeriv
sumDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"
  let sz = var n
  a_ <- freshFrom "a"
  let a = var a_
  plus <- freshFrom "(+)"
  zero <- freshFrom "0"
  xs <- freshFrom "xs"
  result <- freshFrom "r"
  Deriv "sum" def { _intuit =
                       [a_ :::. T TType
                       ,n  :::. T TSize
                       ,plus ::: (a ⊸ a ⊸ a) :^ sizeOne
                       ,zero ::: a :^ sizeOne
                       ]
                  , _linear = [xs ::: bigTensor sz a :^ sizeOne
                              ,result ::: neg a :^ sizeOne]
    } <$> CLL.sum sz a zero plus xs result


{-
dotProduct n sz a plus mul x y result steps = evaluateF [] steps =<< 
   cut (bigTensor sz a) (CLL.zipWith sz "(*)" mul x y)
   (\intermediate ->
     foldMap n intermediate a (\x' y' r -> r =: load plus `app` x' `app` y')
                  axiom
                  (axiom result))

dotProductDeriv steps = D0 $ runFreshM $ do
  n <- freshFrom "n"
  let sz = 2 * var n
  a_ <- freshFrom "a"
  let a = var a_
  plus <- freshFrom "plus"
  mul <- freshFrom "mul"
  x <- freshFrom "x"
  y <- freshFrom "y"
  result <- freshFrom "r"
  Deriv "dotProduct" def {
    _intuit = [a_ ::: T TType :^ 1
              ,plus ::: (a ⊸ a ⊸ a) :^ sizeOne
              ,mul  ::: (a ⊸ a ⊸ a) :^ sizeOne
              ,n  ::: T TSize :^ 1
              ]
   ,_linear = [x ::: bigTensor sz a :^ sizeOne
              ,y ::: bigTensor sz a :^ sizeOne
              ,result ::: neg a :^ sizeOne]
  } <$> CLL.dotProduct n sz a plus mul x y result steps

-}
dotProduct2 inlineSum sz a zero plus mul x y result steps = evaluateF [] steps =<< 
   cut (bigTensor sz a) (CLL.zipWith sz "(*)" mul x y)
   (\intermediate -> if inlineSum then CLL.sum sz a zero plus intermediate result else whut "sum" [intermediate,result])

dotProduct2Deriv inlineSum steps = D0 $ runFreshM $ do
  n <- freshFrom "n"
  va <- freshFrom "A"
  let sz = var n
      a = var va
  plus <- freshFrom "(+)"
  zero <- freshFrom "0"
  mul <- freshFrom "(*)"
  x <- freshFrom "xs"
  y <- freshFrom "ys"
  result <- freshFrom "r"
  Deriv "dotProduct2" def {
    _intuit = [va :::. T TType
              ,n  :::. T TSize
              -- ,zero  ::: a :^ sizeOne
              -- ,plus ::: (a ⊸ a ⊸ a) :^ sizeOne
              -- ,mul  ::: (a ⊸ a ⊸ a) :^ sizeOne
              ]
   ,_linear = [x ::: bigTensor sz a :^ sizeOne
              ,y ::: bigTensor sz a :^ sizeOne
              ,result ::: neg a :^ sizeOne]
   } <$> CLL.dotProduct2 inlineSum sz a zero plus mul x y result steps


matrixProduct n m p x y z =
  slice x $ \x' ->
  slice y $ \y' ->
  coslice z (sizeMul n p) $ \z' ->
  whut "dot product" [x',y',z']
  
matrixProductDeriv :: IdDeriv
matrixProductDeriv = D0 $ runFreshM $ do
  [n_,m_,p_,a_] <- freshFromList ["n","m","p","A"]
  let n = var n_
  let m = var m_
  let p = var p_
  let a = var a_
  x <- freshFrom "xs"
  y <- freshFrom "ys"
  result <- freshFrom "rs"
  Deriv "matrixProductDeriv" def{
      _intuit =        [a_ :::. T TType, m_ :::. T TSize, n_ :::. T TSize, p_ :::. T TSize]
    , _linear =        [x ::: bigTensor (n*m*p) a :^ sizeOne
                       ,y ::: bigTensor (m*p*n) a :^ sizeOne
                       ,result ::: (neg $ bigTensor (n*p) a) :^ sizeOne]
    } <$> CLL.matrixProduct n m p x y result

cutBang :: IdType -> (Id -> Program') -> (Id -> Program') -> Program'
cutBang ty cont1 cont2 = do
  off <- freshId
  sav <- freshId
  cut (T (Bang ty)) (\x -> S0 . Offer False x off <$> (rM$ cont1 off))
                (\x -> S0 . Save x sav <$> (rM$ cont2 sav))

whut x xs = pure $ S0 $ What (Huh x) (fmap ElemLinear xs) [0..length xs - 1]

-- saxpy ty sz a mul plus x y result = evaluateF 5 =<<
--   cutBang (ty ⊸ ty)
--     (\io -> tensor io $ \i o ->
-- load mul $ \m ->
-- par m (axiom i)
--       (\rest -> par rest (load a . axiom) (axiom o))
--       )
--     (\f -> 
--       cut (bigTensor sz ty) (CLL.map sz "f" f x)
--           (\mapped -> CLL.zipWith sz plus mapped y result)
--                      )

saxpyN steps ty sz a mul plus x y result = evaluateF [] steps =<< do
  cut' "z" (bigTensor sz ty) 
     (CLL.zipWith sz "(*)" mul a x)
     (\ax -> CLL.zipWith sz "(+)" plus ax y result)

saxpyNDeriv :: Int-> IdDeriv
saxpyNDeriv steps = D0 $ runFreshM $ do
  [a,tyA,n,mul,plus,x,y,result] <- T.mapM freshFrom ["a","A","n","mul","plus","xs","ys","rs"]
  let ty = var tyA
  let sz = var n
  Deriv "saxpy" def{ _intuit = [ tyA :::. T TType
                               , n   :::. T TSize
                               ]
                   , _linear =
                   [x ::: bigTensor sz ty :^ sizeOne
                   ,y ::: bigTensor sz ty :^ sizeOne
                   ,a ::: bigTensor sz ty:^ sizeOne
                   ,result ::: neg (bigTensor sz ty) :^ sizeOne]
  } <$> saxpyN steps ty sz a mul plus x y result

saxpyNFFI ty sz a mul plus x y result = 
--  cut' "z" (bigTensor sz ty) 
     ffiCall (bigTensor sz tDouble) "zipWith(*)" [a,x] $ \ax ->
     pure$ S0$ What (FFI (Call "zipWith(+)")) [ElemLinear result, ElemLinear ax, ElemLinear y] [0,1,2] 


saxpyNFFIDeriv :: IdDeriv
saxpyNFFIDeriv = D0 $ runFreshM $ do
  [a,tyA,n,mul,plus,x,y,result] <- T.mapM freshFrom ["a","A","n","mul","plus","xs","ys","rs"]
  let ty = var tyA
  let sz = var n
  Deriv "saxpy" def{ _intuit = [ tyA :::. T TType
                               , n   :::. T TSize
                               ]
                   , _linear =
                   [x ::: bigTensor sz ty :^ sizeOne
                   ,y ::: bigTensor sz ty :^ sizeOne
                   ,a ::: bigTensor sz ty:^ sizeOne
                   ,result ::: neg (bigTensor sz ty) :^ sizeOne]
  } <$> saxpyNFFI ty sz a mul plus x y result


----------------------------

stencil sz a x0 xn dupFct stFct inputArr outPutArr = evaluateF [(sz,Ge,1)] 24 =<<
    (cut (aN ⊗ aN) (dupl sz a dupFct inputArr) $ \a1a2 ->
     tensor a1a2 $ \a1 a2 ->
     cut aN1 (extendL sz x0 a1) $ \a1' ->
     cut aN1 (extendR sz xn a2) $ \a2' ->
     cut aN1 (CLL.zipWith szN1 "(-)" stFct a1' a2') $
     axiom outPutArr
    )
  where aN = bigTensor sz a
        aN1 = bigTensor szN1 a
        szN1 = (sz `sizeAdd` sizeOne)

stonestamp steps n k a x0 xn stFct inputArrsK outPutArrK = evaluateF [(n,Ge,1)] steps =<<
    (slice inputArrsK $ \inputArrs ->
     coslice' outPutArrK k $ \outPutArr ->
     CLL.splitAt sizeOne inputArrs $ \a1 a2 ->
      cut aN1 (extendL n x0 a1) $ \a1' ->
      cut aN1 (extendR n xn a2) $ \a2' ->
      -- whatevers "ss" [a1',a2',x0,xn,outPutArr]
      cut aN1 (CLL.zipWith n1 "(-)" stFct a1' a2') $
      axiom outPutArr
      )

  where aN1 = bigTensor n1 a
        n1 = (n `sizeAdd` sizeOne)

rotate steps n a inp out = evaluateF [] steps =<<(
  cut (a ⊗ bigTensor n a) (matchHead n inp) $ \a1 ->
  tensor' "x" "y" a1 $ \x a2 ->
  (extendL n x a2) out)

rotateDeriv steps = D0 $ runFreshM $ do
  output <- freshFrom "o"
  [a_,n] <- freshFromList ["A","n"]
  let a = var a_
  let sz = var n
  let sz1 = sizeAdd sz sizeOne
  input <- freshFrom "i"
  let aN1 = bigTensor sz1 a
  Deriv "rotate" def{
        _intuit = [n  :::. T TSize
                  ,a_ :::. T TType
                  ]
      , _linear = [input  :::    aN1 :^ sizeOne
                  ,output ::: neg aN1 :^ sizeOne
                  ]
      } <$> rotate steps sz a input output

-- error here
circStencil steps n k a stFct inputArrsK outPutArrK = evaluateF [] steps =<<
   (slice inputArrsK $ \inputArrs ->
    coslice' outPutArrK k $ \outPutArr ->
    CLL.splitAt sizeOne inputArrs $ \a0 b0 ->
    cut aN1 (rotate 20 n a a0) $ \a3 ->
    CLL.zipWith n1 "(-)" stFct a3 b0 outPutArr
   )

  where aN = bigTensor n a
        aN1 = bigTensor n1 a
        n1 = (n `sizeAdd` sizeOne)

circStencilMem steps n a stFct inputArrsK outPutArrK = evaluateF [] steps =<<
   cut (bigTensor (n + 1) a)
     -- (\o -> whut "w" [i,inputArrsK])
        (\o -> circStencilEvalled n 1 a stFct inputArrsK o)
   (\i -> schedule3Ev (n+1) i outPutArrK)

circStencilMemDeriv :: Int -> IdDeriv
circStencilMemDeriv steps = D0 $ runFreshM $ do
  stFct <- freshFrom "s"
  output <- freshFrom "o"
  [a_,n] <- freshFromList ["A","n"]
  let a = var a_
  let sz = var n
  let sz1 = sizeAdd sz sizeOne
  input <- freshFrom "i"
  let aN = bigTensor (sz+1) a
  Deriv "circularStencilMem" def{ 
    _intuit = [a_ :::. T TType
              ,n  :::. T TSize
              ,stFct ::: (a ⊸ a ⊸ a) :^ sizeOne]
   ,_linear = [input  :::     bigTensor (sizeLit 2) aN  :^ sizeOne
              ,output :::     (bigTensor sz1 (neg a))   :^ sizeOne
              ]
  } <$> circStencilMem steps sz a stFct input output
   

stonestampEvalled = stonestamp 14
stonestamp2 steps n k a x0 xn x0' xn' stFct inputArrsK outPutArrK = evaluateF [(n,Ge,2)] steps =<<
    cut (bigTensor (sizeLit 2 `sizeMul` k) (bigTensor szN1 a))
       (stonestampEvalled n (sizeLit 2 `sizeMul` k) a x0 xn stFct inputArrsK)
       -- (\o -> whatevers "arst" [inputArrsK,x0,xn,o])
       -- (\i -> whatevers "arst" [i,x0',xn',i,outPutArrK])
       (\i -> stonestampEvalled szN1 k a x0' xn' stFct i outPutArrK)
  where szN1 = (n `sizeAdd` sizeOne)

circStencilEvalled = circStencil 15
circStencil2 steps n k a stFct inputArrsK outPutArrK = evaluateF [(n,Ge,1)] steps =<<
    cut (bigTensor (sizeLit 2 `sizeMul` k) (bigTensor szN1 a))
       (circStencilEvalled n (sizeLit 2 `sizeMul` k) a stFct inputArrsK)
       -- (\o -> whatevers "arst" [inputArrsK,x0,xn,o])
       -- (\i -> whatevers "arst" [i,x0',xn',i,outPutArrK])
       (\i -> circStencilEvalled n  k a stFct i outPutArrK)
  where szN1 = (n `sizeAdd` sizeOne)

stonestampDeriv2 steps = D0 $ runFreshM $ do
  stFct <- freshFrom "s"
  output <- freshFrom "o"
  [a_,n,k_] <- freshFromList ["A","n","k"]
  [p₁] <- freshFromList ["p"]
  let a = var a_
  let sz = var n
  let k = var k_
  input <- freshFrom "i"
  x0 <- freshFrom "x"
  xn <- freshFrom "y"
  x0' <- freshFrom "x"
  xn' <- freshFrom "y"
  let arr = bigTensor sz a
      aN1 = bigTensor (sz `sizeAdd` sizeOne) a
      aN2 = bigTensor ((sz `sizeAdd` sizeOne) `sizeAdd` sizeOne) a
      kk = sizeAdd k k
  Deriv "stonestamp2" (seqctx
        [(a_ , T TType :^ 1)
        ,(n  , T TSize :^ 1)
        ,(k_ , T TSize :^ 1)
        ,(p₁ , T (TProp (sz, Ge, 2)) :^ 1)
        ,(stFct, (a ⊸ a ⊸ a) :^ sizeOne)]
        [(input ,     bigTensor (sizeLit 2 `sizeMul` (sizeLit 2  `sizeMul` k)) arr  :^ sizeOne),
         (x0, a :^ kk),(xn,a :^ kk),
         (x0', a :^ k),(xn',a :^ k),
         (output,neg (bigTensor k                        aN2) :^ sizeOne)
        ]
      ) <$> (stonestamp2 steps sz k a x0 xn x0' xn' stFct input output)

circStencilDeriv2 steps = D0 $ runFreshM $ do
  [input,stFct,output,va,vsz,vk,p₁] <- mapM freshFrom ["i","s","o","a","n","k","p"]
  let [sz,k] = List.map var [vsz,vk]
      a = var va
  let aN1 = bigTensor (sz `sizeAdd` sizeOne) a
  Deriv "stencil" (seqctx
        [(va , T TType :^ 1), (vsz , T TSize :^ 1) , (vk , T TSize :^ 1),(p₁ , T (TProp (sz, Ge, 1)) :^ 1)]
        [(input ,     bigTensor (sizeLit 2 `sizeMul` (sizeLit 2  `sizeMul` k)) aN1  :^ sizeOne),
         (output,neg (bigTensor k                        aN1) :^ sizeOne)
        ]
      ) <$> (circStencil2 steps sz k a stFct input output)

-- Hand-normalized version of (circStencilMemDeriv 31)
circStencilDerivEval = D0 $ runFreshM $ do
  stFct <- freshFrom "s"
  output <- freshFrom "ys"
  [a_,n] <- freshFromList ["A","n"]
  [p₁] <- freshFromList ["p"]
  let a = var a_ 
  let sz = var n
  let sz1 = sizeAdd sz sizeOne
  input <- freshFrom "xs"
  let aN = bigTensor (sz+1) a
  Deriv "circStencilEval" (seqctx
    [(a_, T TType :^ 1)
    ,(n,  T TSize :^ 1)
    ,(p₁ , T (TProp (sz, Ge, 1)) :^ 1)
    ,(stFct, (a ⊸ a ⊸ a) :^ sizeOne)]
    [(input ,     bigTensor (sizeLit 2) aN  :^ sizeOne),
     (output,     (bigTensor sz1 (neg a))   :^ sizeOne)
    ] )
    <$> (slice input $ \i ->
        CLL.splitAtS "x" "y" sizeOne i $ \x y ->
        slice x $ \xi ->
        CLL.splitAtS "a" "b" sizeOne xi $ \d j ->
        slice y $ \yi ->
        CLL.splitAtS "c" "d" sizeOne yi $ \zi zk ->
        slice output $ \oi ->
        CLL.splitAtS "v" "w" sizeOne oi $ \a b ->
        CLL.mix (whut "(-)" [d,zi,a])
                (CLL.bigseq [ seqstep sz [] $ \[] -> (whut "(-)" [j,zk,b]) ]))
  {- 
        (foldMap2' sz [j,zk,b] (T One)
         (\p r q -> ignore p (ignore r (yield q)))
         (\[j,yi,oi] m -> 
         (\u -> yield u)
         (\t -> ignore t halt)))
  -}

stonestampDeriv kGen steps = D0 $ runFreshM $ do
  stFct <- freshFrom "s"
  output <- freshFrom "o"
  (a_,a) <- (id &&& var) <$> freshFrom "a"
  (n,sz) <- (id &&& var) <$> freshFrom "n"
  k_ <- freshFrom "k"
  [p₁] <- freshFromList ["p"]
  let k = if kGen then var k_ else 1
  input <- freshFrom "i"
  x0 <- freshFrom "x"
  xn <- freshFrom "y"
  let arr = bigTensor sz a
      aN1 = bigTensor (sz `sizeAdd` sizeOne) a
  Deriv ("stonestamp" ++ show k) (seqctx
        [(a_, T TType :^ 1)
        ,(n , T TSize :^ 1)
        ,(k_, T TSize :^ 1)
        ,(p₁ , T (TProp (sz, Ge, 1)) :^ 1)
        ,(stFct, (a ⊸ a ⊸ a) :^ sizeOne)]
        [(input ,     bigTensor (sizeLit 2  `sizeMul` k) arr  :^ sizeOne),
         (x0, a :^ k),(xn,a :^ k),
         (output,neg (bigTensor' k                        aN1) :^ sizeOne)
        ]
      ) <$> stonestamp steps sz k a x0 xn stFct input output

circStencilDeriv kGen steps = D0 $ runFreshM $ do
  stFct <- freshFrom "s"
  output <- freshFrom "o"
  (a_,a) <- (id &&& var) <$> freshFrom "a"
  (n ,sz) <- (id &&& var) <$> freshFrom "n"
  let sz1 = sizeAdd sz sizeOne
  k_ <- freshFrom "k"
  let k = if kGen then  var k_ else 1
  input <- freshFrom "i"
  let aN1 = bigTensor sz1 a
  Deriv ("circularStencil" ++ show k) (seqctx
        [(a_, T TType :^ 1)
        ,(n, T TSize :^ 1)
        ,(k_, T TSize :^ 1)
        ,(stFct, (a ⊸ a ⊸ a) :^ sizeOne)]
        [(input ,     bigTensor (sizeLit 2  `sizeMul` k) aN1  :^ sizeOne),
         (output,neg (bigTensor' k                       aN1) :^ sizeOne)
        ])
        <$> circStencil steps sz k a stFct input output

-- Hand normalized version of circStencilDeriv 1000
circStencilDeriv' kGen = D0 $ runFreshM $ do
  stFct <- freshFrom "s"
  output <- freshFrom "o"
  (a_,a) <- (id &&& var) <$> freshFrom "A"
  (n,sz) <- (id &&& var) <$> freshFrom "n"
  let sz1 = sizeAdd sz sizeOne
  k_ <- freshFrom "k"
  let k = if kGen then  var k_ else 1 
  input <- freshFrom "z"
  let aN1 = bigTensor sz1 a
  Deriv "diff" (seqctx
        --[a] -- [(stFct, (a ⊸ a ⊸ a) :^ sizeOne)]
        [(a_, T TType :^ 1)
        ,(n, T TSize :^ 1)
        ,(k_, T TSize :^ 1)
        ]
        [(input ,     bigTensor (sizeLit 2  `sizeMul` k) aN1  :^ sizeOne),
         (output,neg (bigTensor' k                       aN1) :^ sizeOne)
        ])
        <$> slice input (\i ->
            CLL.splitAtS "x" "y" (sizeLit 1) i $ \x y ->
            slice x $ \xi ->
            CLL.splitAtS "a" "b" (sizeLit 1) xi $ \c d ->
            slice y $ \yi ->
            CLL.splitAtS "c" "d" (sizeLit 1) yi $ \a b ->
            elimBigPar output [(sz,      \o1 -> whut "(-)" [d,b,o1])
                              ,(sizeOne, \o2 -> whut "(-)" [c,a,o2])])

bigTensor' :: (Ord ref) => Size ref -> Type' name ref -> Type' name ref
bigTensor' 1 = id
bigTensor' sz = bigTensor sz

stencil2Fuse steps sz a x0 xn x0' xn' dupFct stFct inputArr outPutArr = evaluateF [(sz,Ge,1)] steps =<<
   cut aN1 (stencil sz a x0 xn dupFct stFct inputArr)
           (\inputArr' -> stencil szN1 a x0' xn' dupFct stFct inputArr' outPutArr)
  where aN1 = bigTensor szN1 a
        szN1 = sz + 1

stencil2 = stencil2Fuse 45

stencilDeriv = D0 $ runFreshM $ do
  dupFct <- freshFrom "d"
  stFct <- freshFrom "s"
  output <- freshFrom "o"
  [p₁] <- freshFromList ["p"]
  (a_,a) <- (id &&& var) <$> freshFrom "a"
  (n,sz) <- (id &&& ((+1) . var)) <$> freshFrom "n"
  input <- freshFrom "i"
  x0 <- freshFrom "x"
  xn <- freshFrom "y"
  let arr = bigTensor sz a
      aN1 = bigTensor (sz `sizeAdd` sizeOne) a
  Deriv "stencil" (seqctx 
        [(a_, T TType :^ 1)
        ,(n, T TSize :^ 1)
        ,(p₁ , T (TProp (sz, Ge, 1)) :^ 1)
        ,(dupFct, (a ⊸ a ⊗ a) :^ sizeOne),(stFct, (a ⊸ a ⊸ a) :^ sizeOne)]
        [(input,arr :^ sizeOne),(output,neg aN1 :^ sizeOne),
         (x0,unit a),(xn,unit a)
        ])
        <$> (stencil sz a x0 xn dupFct stFct input output)


stencilDeriv2 = D0 $ runFreshM $ do
  dupFct <- freshFrom "d"
  stFct <- freshFrom "s"
  output <- freshFrom "o"
  (a_,a) <- (id &&& var) <$> freshFrom "a"
  (n,sz) <- (id &&& var) <$> freshFrom "n"
  [p₁] <- freshFromList ["p"]
  input <- freshFrom "i"
  x0 <- freshFrom "x"
  xn <- freshFrom "y"
  x0' <- freshFrom "x'"
  xn' <- freshFrom "y'"
  let arr = bigTensor sz a
      aN2 = bigTensor (sz + 2) a
  Deriv "stencil2" (seqctx
        [(a_, T TType :^ 1)
        ,(n,  T TSize :^ 1)
        ,(p₁ , T (TProp (sz, Ge, 1)) :^ 1)
        ,(dupFct, (a ⊸ a ⊗ a) :^ sizeOne),(stFct, (a ⊸ a ⊸ a) :^ sizeOne)]
        [(input,arr :^ sizeOne),(output,neg aN2 :^ sizeOne),(x0,unit a),(xn,unit a),
         (x0',unit a),(xn',unit a)])
        <$> (stencil2 sz a x0 xn x0' xn' dupFct stFct input output)

  
-------------------------------------------------

splitAt :: IdSize -> Id -> (Id -> Id -> Program') -> Program'
splitAt pos z cont = do
  x <- freshFrom "x"
  y <- freshFrom "y"
  s0$ Split z pos x y <$> (rM$ cont x y)

splitAtS :: String -> String -> IdSize -> Id -> (Id -> Id -> Program') -> Program'
splitAtS x y pos z cont = do
  x <- freshFrom x
  y <- freshFrom y
  s0$ Split z pos x y <$> (rM$ cont x y)

matchHead :: IdSize -> Id -> Id -> Program'
matchHead sz xs0 ys0  = 
  slice xs0 $ \xs1 ->
  CLL.splitAt sizeOne xs1 $ \ x xs ->
  par ys0 (axiom x)
          (\ys -> coslice ys sz $ axiom xs)

extendL sz el1 inputArr outputArr =
  slice inputArr $ \el ->
  elimBigPar outputArr [(sizeOne,axiom el1),(sz,axiom el)]

extendR sz el1 inputArr outputArr =
  slice inputArr $ \el ->
  elimBigPar outputArr [(sz,axiom el),(sizeOne,axiom el1)]

extendLDeriv :: IdDeriv
extendLDeriv = D0 $ runFreshM $ do
  [el,inputArr,outputArr] <- freshIds 3
  (n,sz) <- (id &&& var) <$> freshFrom "n"
  (a_,a) <- (id &&& var) <$> freshFrom "a"
  Deriv "extendL" (seqctx
                   [(a_, T TType :^ 1)
                   ,(n,  T TSize :^ 1)]
                   [(el,a :^ sizeOne)
                   ,(inputArr,bigTensor sz a :^ sizeOne)
                   ,(outputArr,neg (bigTensor (sz `sizeAdd` sizeOne) a) :^ sizeOne)])
    <$> extendL sz el inputArr outputArr

extendRDeriv :: IdDeriv
extendRDeriv = D0 $ runFreshM $ do
  [el,inputArr,outputArr] <- freshIds 3
  (n,sz) <- (id &&& var) <$> freshFrom "n"
  (a_,a) <- (id &&& var) <$> freshFrom "a"
  Deriv "extendR" (seqctx [(a_, T TType :^ 1)
                          ,(n, T TSize :^ 1)]
                   [(el,a :^ sizeOne),(inputArr,bigTensor sz a :^ sizeOne),(outputArr,neg (bigTensor (sz `sizeAdd` sizeOne) a) :^ sizeOne)])
    <$> extendR sz el inputArr outputArr

----------------------------------
dupl sz a dupFct inputArr outPutArrs = evaluateF [] 5 =<<
  cut (bigTensor sz (a ⊗ a))
     (\parArr -> slice inputArr $ \element ->
        coslice parArr sz    $ \copies  ->
        load dupFct             $ \dup ->
        par dup
        (axiom element) (axiom copies)
     )
     (\tensorArr -> slice tensorArr $ \elements ->
       tensor elements $ \el1 el2 ->
       par outPutArrs (axiomArr sz el1) (axiomArr sz el2))


duplDeriv :: IdDeriv
duplDeriv = D0 $ runFreshM $ do
  dupFct <- freshFrom "d"
  output <- freshFrom "o"
  ty <- freshFrom "a"
  sz <- freshFrom "n"
  input <- freshFrom "i"
  let szN = var sz
      a = var ty
      arr = bigTensor szN a
  Deriv "duplicate" (seqctx
        [(ty, T TType :^ 1)
        ,(sz, T TSize :^ 1)
        ,(dupFct, (a ⊸ a ⊗ a) :^ sizeOne)]
        [(input,arr :^ sizeOne),(output,neg (arr ⊗ arr) :^ sizeOne)] )
        <$> (dupl szN a dupFct input output)

-- duplicate = do


----------------------------------

-- | Like cut, but with intermediate storage of multiple copies.
syncK :: Size Id -- ^ k: Number of copies
      -> IdSized -- ^ Type of the elements of the array, -- ^ n: Size of the array
      -> (Id -> Program') -- ^ Producer
      -> (Id -> Program') -- ^ Consumer
      -> Program'
syncK cnt (typ :^ sz) cont1 cont2 = do
  [x,y] <- mapM freshFrom ["s","t"]
  S contL <- cont1 x
  S syncContR <- cont2 y
  return$ S0 $ Sync { syncIntr = [(sz, typ, x, Nothing, Just $ SyncRead { syncCopies = cnt, syncYIntr = y })]
            , contL
            , syncCont= Nothing
            , syncContR = Just syncContR 
            }

  --freezeSeq1 cnt sz typ x y <$> cont1 x <*> cont2 y

putIndex :: IdSize -> (Id -> Program') -> Program'
putIndex sz cont = do
  ty <- bigTensor sz <$> tExists "i" tSize (\i -> tExists "p" (tProp (var i,Lt,sz)) (\_ -> pure one))
  fuse ty (\y -> pure$ S0$ What (Builtin BIFIndex) [ElemLinear y] [0]) (\vec -> slice vec cont)

getIndex :: Id -> IdSize -> (IdSize -> Program') -> Program'
getIndex elem sz cont =
  unpack elem tSize $ \i prop ->
  let idx = var i in
  unpack prop (tProp (idx,Lt,sz)) $ \_ one ->
  ignore one $ cont $ idx

splitAt' :: [IdSize] -> Id -> ([Id] -> Program') -> Program'
splitAt' [] z cont = cont [z]
splitAt' (pos:ps) z cont = CLL.splitAt pos z (\x y -> splitAt' (fmap (subtract pos) ps) y (\xs -> cont (x:xs)))

splitAtK' :: Int -> IdSize -> Id -> ([Id] -> Program') -> Program'
splitAtK' 0   sz z cont = CLL.mix' [halts1 z, cont []]
splitAtK' cnt sz z cont = splitAt' (take (cnt - 1) (iterate (+ sz) sz)) z cont

sync :: IdSized      -- ^ type to make the copy of
     -> (Id -> Program') -- ^ Producer
     -> (Id -> Program') -- ^ Consumer
     -> Program'
sync = syncK 1

mix' :: [Program'] -> Program'
mix' [] = halt
mix' [p] = p
mix' (p:ps) = CLL.mix p (mix' ps)

type SeqProgram' = FreshM (IdSeqStep)

seqstep :: IdSize  -> [Id] -> ([Id] -> Program') -> SeqProgram'
seqstep sz seqs k = do
  seqs' <- mapM refreshId seqs
  S prog <- k seqs'
  return $ SeqStep {
           seqSz = sz
          ,seqChunk = List.zip seqs seqs'
          ,seqProg = prog
        }

bigseq :: [SeqProgram'] -> Program' 
bigseq m = s0 $ do
  ss <- sequence m
  return $ BigSeq ss

{-
bigseq :: Fence ([Id] -> [Id] -> Program') Interval -> Program'
bigseq fence = do
  -- Generate boundaries with fresh ids
  fence' <- flip bimapM fence return $ \Interval {..} ->
              sbs <- forM ivTypes $ \ty -> 
                SyncBoundary ty <$>
                ((,) <$> freshId <*> freshId)
                ((,) <$> freshId <*> freshId)

  
                SeqInterval {
                  seqSz = ivSz,
                  seqChunk = zip ivSeqs ss,
                  seqBoundary = sbs,
                  seqProg = ivk vs ss
                  })

  -- Now, use this ids to connect everything in sequence
  fence'' <- flip (flip bimapM (extendBigSeqBoundary fence')) id $ 
               \(begin, body, end) -> body begin end

  return $ BigSeq fence''
-}
syncK' :: Int -- ^ k: Number of copies
       -> IdSized  -- ^ Type of the elements of the array, -- ^ n: Size of the array
       -> (Id -> Program') -- ^ Producer
       -> ([Id] -> Program') -- ^ Consumer
       -> Program'
syncK' cnt (typ :^ sz) prod k = do
  syncK (fromIntegral cnt) (typ :^ sz) prod
    (\copy -> splitAtK' cnt sz copy k)

dump :: IdSize -> IdType -> Id -> Program' -> Program'
{-dump sz ty ref k = do
  xIntr <- freshFrom "d"
  CLL.mix' [(\(S contL) -> S0$ Sync { syncSz = sz 
                                    , syncTy = ty
                                    , xIntr
                                    , contL
                                    , syncUpdate = Nothing, syncRead = Nothing
                                    }) <$> axiomN sz ref xIntr
           ,k
           ]
-}

--dump sz ty ref k = syncK 0 sz ty (axiomN sz ref) $ \ref' -> CLL.mix' [CLL.halts [ref'], k]
dump sz typ ref k = do
  [x] <- mapM freshFrom ["s"]
  S contL <- axiomN sz ref x
  let a = S0 $ Sync { syncIntr = [(sz, typ, x, Nothing, Nothing)]
            , contL
            , syncCont = Nothing
            , syncContR = Nothing
            }
  CLL.mix' [return a, k]

dump1 :: IdType -> Id -> Program' -> Program'
--dump1 ty ref k = syncK 0 1 ty (axiom ref) $ \ref' -> CLL.mix' [CLL.halts [ref'], k]
dump1 ty ref k = dump 1 ty ref k

axiomN :: Size Id -> Id -> Id -> Program'
axiomN 1   a a' = axiom a a'
axiomN szN a a' = foldMap2' szN [a,a'] (T One)
                    (\a b c -> ignore a $ ignore b $ yield c)
                    (\[a,a'] b -> CLL.mix (a `axiom` a') (yield b))
                    (\b -> yield b)
                    (\b -> ignore b $ halt)

tensorize :: IdSize -> IdType -> Id -> (Id -> Program') -> Program' 
tensorize szN ty a = cut (bigTensor szN ty) (\par -> coslice par szN (axiom a)) 

-- | Makes a big tensor into a small tensor
tensorizeK :: Int -> IdType -> Id -> (Id -> Program') -> Program'
tensorizeK cnt ty a | cnt >= 1 =
    cut tyT     $ \t  ->
    splitAtK' cnt 1 a $ \as ->
    unfoldParK t as
  where
    tyT = foldr ((T.) . (:*:)) ty $ take (cnt-1) $ repeat ty

    unfoldParK a [b] =  axiom a b
    unfoldParK a (b:bs) =
      par a (\a0 -> axiom a0 b)
            (\a1 -> unfoldParK a1 bs)
   
seqsync :: IdSized -- ^ number and type of elements to allocate
        -> IdSize  -- ^ number of copies to make available at the end
        -> IdSize  -- ^ number of updates to allow for
        -> (Id -> Program') -> (Id -> Program') -> (Id -> Program') -> Program'
seqsync tysz@(syncTy :^ syncSz) syncCopies syncSteps init update end = do
  [xIntr,syncWIntr,syncYIntr] <- mapM freshFrom ["mw","mu","mr"]
  (\(S contL) (S (Just -> syncCont)) (S (Just -> syncContR)) -> do
    let syncUpdate = Just$ SyncUpdate{..}
    let syncRead   = Just$ SyncRead{..}
    let syncIntr   = [(syncSz,syncTy,xIntr,syncUpdate,syncRead)]
    S0$ Sync{..}) <$> init xIntr <*> update syncWIntr <*> end syncYIntr

seqsync0 :: IdSized -- ^ number and type of elements to allocate
         -> IdSize  -- ^ number of updates to allow for
         -> (Id -> Program') -> (Id -> Program') -> Program'
seqsync0 tysz@(syncTy :^ syncSz) syncSteps init update = do
  [xIntr,syncWIntr] <- mapM freshFrom ["mw","mu"]
  (\(S (contL)) (S (Just -> syncCont)) -> do
    let syncUpdate = Just$ SyncUpdate{..}
    let syncRead   = Nothing
    let syncContR  = Nothing
    let syncIntr   = [(syncSz,syncTy,xIntr,syncUpdate,syncRead)]
    S0$ Sync{..}) <$> init xIntr <*> update syncWIntr

discardData :: IdType -> Id -> Program' 
discardData tyA ref = copyData 0 tyA ref (\[] -> halt)

copyData :: Int -> IdType -> Id -> ([Id] -> Program') -> Program'
copyData 0   tyA ref prog = dump1 tyA ref $ prog []
copyData 1   _   ref prog = prog [ref]
copyData cnt tyA ref prog = syncK' cnt (tyA :^ 1) (axiom ref) prog

copyDataN :: Int -> IdSized -> Id -> ([Id] -> Program') -> Program'
copyDataN 0   (tyA :^ sz) ref prog = dump sz tyA ref $ prog []
copyDataN 1   _   ref prog = prog [ref]
copyDataN cnt (tyA :^ sz) ref prog = syncK' cnt (tyA :^ sz) (axiomN sz ref) prog


stencilTBigSeq :: IdSize -- ^ size
                -> IdType -- ^ type of the array 
                -> IdType -- ^ type of the result 
                -> Id -- ^ tau
                -> Id -- ^ step (t-1)
                -> Id -- ^ step (t)
                -> Id -- ^ step (t+1)
                -> Program'
stencilTBigSeq szN ty tyR tau prev curr res = 
  CLL.seqsync0 (ty :^ 1) (szN + 1) (ffiLit ("0" ::: ty) . axiom) $ \buf₁ ->
  CLL.seqsync0 (ty :^ 1) (szN + 1) (ffiLit ("0" ::: ty) . axiom) $ \buf₂ ->
  CLL.syncK    szN (ty :^ 1) (axiom tau) $ \tau ->  
  CLL.splitAt 1         tau $ \tau₁ tau ->
  CLL.splitAt (szN - 2) tau $ \tau₂ tau₃ ->
  CLL.bigseq [
    -- Load stencil (has to be a fixed window size)
    seqstep 1 [curr,buf₁] $ \[curr,buf₁] -> foldConst buf₁ ty (axiom curr)
   ,seqstep 1 [curr,buf₂] $ \[curr,buf₂] -> foldConst buf₂ ty (axiom curr)

   ,seqstep 1 [prev, buf₁, buf₂, res] $ \ [prevL, buf₁, buf₂, resL] ->
      foldIdentity buf₁ ty $ \currL0a ->
      foldIdentity buf₂ ty $ \currL0b ->
      ffiCall tyR "waveL" [tau₁, currL0a, currL0b, prevL] (axiom resL)

   ,seqstep (szN - 2) [prev, curr, res, buf₁, buf₂] $ \[prev, curr, res, buf₁, buf₂] ->
      foldYield    buf₁ ty $ \buf₁r buf₁w ->
      foldYield' 2 buf₂ ty $ \[buf₂r, buf₂r'] buf₂w ->
      copyData  2 ty curr $ \[curr,curr'] ->

      CLL.mix' [axiom buf₂r' buf₁w
               ,axiom curr'  buf₂w
               ,ffiCall tyR "waveC" [tau₂, buf₁r, buf₂r, curr, prev] (axiom res)]

   ,seqstep 1 [prev, res, buf₁, buf₂] $ \[prevR, resR, buf₁, buf₂] ->
      foldIdentity buf₁ ty $ \buf₁ ->
      foldIdentity buf₂ ty $ \buf₂ ->
      ffiCall tyR "waveR" [tau₃, prevR, buf₁, buf₂] (axiom resR)
   ]

appendSeq :: IdSize -> IdSize -> IdType -> Id -> Id -> (Id -> Program') -> Program'
appendSeq szN₁ szN₂ ty a b cont = 
  cut' "append" (dual$ bigSeq (szN₁ + szN₂) ty) cont $ \append ->
    CLL.bigseq [
      seqstep szN₁ [a, append] $ \[a,append] -> axiom a append
     ,seqstep szN₂ [b, append] $ \[b,append] -> axiom b append
     ]

windowSeq :: IdSize -> IdSize -> IdType -> Id -> (Id -> Program') -> Program'
windowSeq szK szN ty start cont = 
  cut' "window" (dual$ bigSeq szN (ty -⊸ bigTensor szK ty)) cont $ \seq ->
    CLL.seqsync0 (bigTensor (szK - 1) ty :^ 1) szN (axiom start) $ \buf ->
    CLL.bigseq [
      seqstep szN [seq, buf] $ \[f, buf] ->
       tensor f $ \input output ->
       foldYield' 2 buf (bigTensor (szK - 1) ty) $ \[prev₁,prev₂] next ->
       slice prev₁ $ \prev₁ ->
       slice prev₂ $ \prev₂ ->
       copyData 2 ty input $ \[input₁, input₂] -> 
       merge (szK-1) prev₁ input₁ $ \output' ->
       CLL.splitAt 1 prev₂ $ \discarded prev₂ ->
       dump1 ty discarded $
       merge (szK-2) prev₂ input₂ $ \next' -> 
       CLL.mix' [coslice output szK       (axiom output')
                ,coslice next   (szK - 1) (axiom next')
                ] 
       ]
  
windowSeq1 :: IdSize -> IdSize -> IdType -> Id -> (Id -> Program') -> Program'
windowSeq1 szK szN ty start cont = 
  cut' "window1" (dual (bigSeq szN ((ty -⊸ bigTensor szK ty) &: one))) cont $ \seq ->
    CLL.seqsync0 (bigTensor (szK - 1) ty :^ 1) szN (axiom start) $ \buf ->
    CLL.bigseq [
      seqstep szN [seq, buf] $ \[f, buf] ->
       plus f $ \case
          Right o -> foldSkip buf (bigTensor (szK - 1) ty) $ yield o
          Left  f ->
            tensor f $ \input output ->
            foldYield' 2 buf (bigTensor (szK - 1) ty) $ \[prev₁,prev₂] next ->
            slice prev₁ $ \prev₁ ->
            slice prev₂ $ \prev₂ ->
            copyData 2 ty input $ \[input₁, input₂] -> 
            merge (szK-1) prev₁ input₁ $ \output' ->
            CLL.splitAt 1 prev₂ $ \discarded prev₂ ->
            dump1 ty discarded $
            merge (szK-2) prev₂ input₂ $ \next' -> 
            CLL.mix' [coslice output szK       (axiom output')
                        ,coslice next   (szK - 1) (axiom next')
                        ] 
            ]

delaySeq :: IdSize -> IdSize -> IdType -> Id -> (Id -> Program') -> Program'
delaySeq szK szN ty start cont =
  windowSeq (szK+1) szN ty start $ \window ->
  arrowSeq szN (bigTensor (szK + 1) ty) ty
    (\buffer result ->
        slice buffer $ \buffer ->
        CLL.splitAt 1 buffer $ \result' rest ->
        dump szK ty rest $
        axiom result' result) $ \first ->
  composeSeq szN ty (bigTensor (szK + 1) ty) ty window first cont
                   
repeatSeq :: IdSize -> IdType -> Id -> (Id -> Program') -> Program'
repeatSeq szN ty f cont =
  cut' "repeat" (dual$ bigSeq szN ty) cont $ \seq ->
    CLL.bigseq [
      seqstep szN [seq] $ \[seq] -> axiom f seq
      ]

composeSeq :: IdSize -> IdType -> IdType -> IdType -> Id -> Id -> (Id -> Program') -> Program'
composeSeq szN tyA tyB tyC f g cont = 
  cut' "h" (dual$ bigSeq szN (tyA ⊸ tyC)) cont $ \seq ->
    CLL.bigseq [
    CLL.seqstep szN [f,g,seq] $ \[f,g,h] -> composeF f g h
    ]
    
composeF :: Id -> Id -> Id -> Program'
composeF f g h' = tensor h' $ \h₁ h₂ ->
  par g (\g₁ -> apply f [h₁] (axiom g₁))
        (axiom h₂)

applySeq :: IdSize -> Id -> Id -> Id -> Program'
applySeq szN arrow input output =
  CLL.bigseq [
    seqstep szN [arrow, input, output] $ \[f, input, output] -> apply f [input] (axiom output)
    ]

coapplySeq :: IdSize -> IdType -> IdType -> Id -> Id -> (Id -> Program') -> Program'
coapplySeq szN tyA tyB arrow output cont =
  cut' "coapply" (dual$ bigSeq szN (dual tyA)) cont $ \input ->
  applySeq szN arrow input output

stackSeq :: IdSize -> IdType -> IdType -> IdType -> IdType -> Id -> Id -> (Id -> Program') -> Program'
stackSeq szN tyA tyB tyC tyD f g cont =
  cut' "h" (dual$ bigSeq szN (tyA ⊗ tyC ⊸ tyB ⊗ tyD)) cont $ \seq ->
    CLL.bigseq [
      CLL.seqstep szN [f, g, seq] $ \[f, g, seq] ->
       tensor seq $ \input output ->
       tensor input $ \a c ->
       par output (apply f [a] . axiom) 
                  (apply g [c] . axiom)
       ]

duplSeq :: IdSize -> IdType -> (Id -> Program') -> Program'
duplSeq szN tyA cont =
  cut' "dupl" (dual$ bigSeq szN (tyA ⊸ tyA ⊗ tyA)) cont $ \seq ->
    CLL.bigseq [
      CLL.seqstep szN [seq] $ \[seq] ->
       tensor seq $ \input output ->
         copyData 2 tyA input $ \[output₁,output₂] ->
           par output (axiom output₁) (axiom output₂)
      ]

forkSeq szN ty tyA tyB seqA seqB cont =
  duplSeq szN ty $ \dupl ->
    stackSeq szN ty  tyA  ty tyB seqA seqB $ \stacked ->
      composeSeq szN ty (ty ⊗ ty) (tyA ⊗ tyB) dupl stacked cont

arrowSeq szN tyA tyB f cont =
  cut' "arrow" (dual$ bigSeq szN (tyA ⊸ tyB)) cont $ \fseq ->
    CLL.bigseq [
      CLL.seqstep szN [fseq] $ \[fseq] -> tensor fseq $ \a b -> f a b
      ]
      
       
stencilTBigSeqSymmArrow :: IdSize -- ^ size
                -> IdType -- ^ type of the array 
                -> Id -- ^ tau
                -> Id -- ^ step (t-1,t)
                -> Id -- ^ step (t,t+1)
                -> Program'
stencilTBigSeqSymmArrow szN ty tau prev next =
  -- Duplicate make copies of the constant
  CLL.syncK szN (ty :^ 1) (axiom tau) $ \tau ->  
  CLL.splitAt 1         tau $ \tau₁ tau ->
  CLL.splitAt (szN - 2) tau $ \tau₂ tau₃ ->

  cut (ty ⊗ ty) (\p -> par p (ffiLit ("0" ::: ty) . axiom) (ffiLit ("0" ::: ty) . axiom)) $ \inputPadding ->
    repeatSeq 1 (ty ⊗ ty) inputPadding $ \inputPadding ->
      cut (dual$ (ty ⊗ bigTensor 3 ty)) (discardData (ty ⊗ bigTensor 3 ty)) $ \outputPadding ->
        repeatSeq 1 (dual$  (ty ⊗ bigTensor 3 ty)) outputPadding $ \outputPadding ->

      cut (bigTensor 2 ty) (\p -> coslice p 2 (ffiLit ("0" ::: ty) . axiom)) $ \windowPadding ->
        cut (bigTensor 1 ty) (\p -> coslice p 1 (ffiLit ("NAN" ::: ty) . axiom)) $ \delayPadding ->
            
            arrowSeq 1         (ty ⊗ bigTensor 3 ty) ty (\waveL' result ->
                               tensor waveL' $ \low high ->
                               slice high $ \high ->
                               CLL.splitAt' [1,2] high $ \[a,b,c] ->
                               dump1 ty a $
                               ffiCall ty "waveL" [tau₁,low,b,c] (axiom result)) $ \waveL ->
                               
            arrowSeq (szN - 2) (ty ⊗ bigTensor 3 ty) ty (\waveC' result ->
                               tensor waveC' $ \low high ->
                               slice high $ \high ->
                               CLL.splitAt' [1,2] high $ \[a,b,c] ->
                               ffiCall ty "waveC" [tau₂,a,b,c,low] (axiom result)) $ \waveC ->

            arrowSeq 1         (ty ⊗ bigTensor 3 ty) ty (\waveR' result ->
                               tensor waveR' $ \low high ->
                               slice high $ \high ->
                               CLL.splitAt' [1,2] high $ \[a,b,c] ->
                               dump1 ty c $
                               ffiCall ty "waveR" [tau₃,low,a,b] (axiom result)) $ \waveR ->

            arrowSeq szN       (ty ⊗ bigTensor 3 ty) ty (\wave0' result ->
                               tensor wave0' $ \low high ->
                               slice high $ \high ->
                               CLL.splitAt' [1,2] high $ \[a,b,c] ->
                               dump1 ty low $
                               dump1 ty a $
                               dump1 ty c $
                               axiom result b) $ \wave0 ->
                               
            -- Build the wave equation, apply it before the output, then add padding
            appendSeq 1 (szN - 2) ((ty ⊗ bigTensor 3 ty) ⊸ ty) waveL waveC $ \wave₁ -> 
            appendSeq (szN - 1) 1 ((ty ⊗ bigTensor 3 ty) ⊸ ty) wave₁ waveR $ \waveF  -> 

            forkSeq szN (ty ⊗ bigTensor 3 ty) ty ty wave0 waveF $ \waveStep ->
            coapplySeq szN (ty ⊗ bigTensor 3 ty) (ty ⊗ ty) waveStep next $ \next ->

            -- Pipeline to shift the data to expose the appropiate window.
            delaySeq 1 (1 + szN) ty delayPadding $ \delay ->
              windowSeq 3 (1 + szN) ty windowPadding $ \window ->
                stackSeq (1 + szN) ty ty ty (bigTensor 3 ty) delay window $ \stencil ->

                   -- Pad the input and the output, apply pipeline 
                   appendSeq szN 1 (ty ⊗ ty)                     prev       inputPadding $ \prev ->
                   appendSeq 1 szN (dual$ (ty ⊗ bigTensor 3 ty)) outputPadding      next $ \next ->

                   applySeq (szN + 1) stencil prev next


stencilTBigSeqSymm = stencilTBigSeqSymmArrow

stencilTBigSeqSymmLowLevel :: IdSize -- ^ size
                -> IdType -- ^ type of the array 
                -> Id -- ^ tau
                -> Id -- ^ step (t-1,t)
                -> Id -- ^ step (t,t+1)
                -> Program'
stencilTBigSeqSymmLowLevel szN ty tau prev next = 
          --          -- p1
          -- a b    -- z
          --   e    -- m1
  CLL.seqsync0 (ty :^ 1) (szN + 1) (ffiLit ("0" ::: ty) . axiom) $ \buf_a ->
  CLL.seqsync0 (ty :^ 1) (szN)     (ffiLit ("0" ::: ty) . axiom) $ \buf_b ->
  CLL.seqsync0 (ty :^ 1) (szN + 1) (ffiLit ("0" ::: ty) . axiom) $ \buf_e ->
  CLL.syncK szN (ty :^ 1) (axiom tau) $ \tau ->  
  CLL.splitAt 1         tau $ \tau₁ tau ->
  CLL.splitAt (szN - 2) tau $ \tau₂ tau₃ ->
  CLL.bigseq [
          seqstep 1 [prev, buf_a, buf_e] $ \[prev, buf_a, buf_e] ->
            tensor prev $ \m1 z ->
            CLL.mix' [foldConst buf_a ty (axiom z)
                     ,foldConst buf_e ty (axiom m1)
                     ]

          -- a -- eq
          -- e -- m1

         ,seqstep 1 [prev, buf_a, buf_e, buf_b, next] $ \[prev, buf_a, buf_e, buf_b, next] ->
            tensor prev $ \m1 z ->
            copyData 2 ty z $ \[z,z'] ->
            foldIdentity' 2 buf_a ty $ \[buf_a, buf_a'] ->
            foldYield      buf_e ty $ \buf_e buf_ew ->
            par next (axiom buf_a') (\p1 ->
            CLL.mix' [foldConst buf_b ty (axiom z')
                     ,axiom buf_ew m1
                     ,ffiCall ty "waveL" [tau₁,buf_a, z, buf_e] (axiom p1)])
          -- a b    -- z
          --   e    -- m1

         ,seqstep (szN - 2) [prev, buf_a, buf_e, buf_b, next] $ \[prev, buf_a, buf_e, buf_b, next] ->
            tensor prev $ \m1 z ->

            foldYield       buf_a ty $ \buf_a buf_aw ->
            foldYield'    3 buf_b ty $ \[buf_b, buf_b',buf_b''] buf_bw ->
            foldYield       buf_e ty $ \buf_e buf_ew ->
            copyData     2 ty z     $ \[z,z'] ->
            par next (axiom buf_b'')
                     (\p1 ->
                       CLL.mix' [ffiCall ty "waveC" [tau₂,buf_a, buf_b, z, buf_e] (axiom p1)
                                ,axiom buf_aw buf_b'
                                ,axiom buf_bw z'
                                ,axiom buf_ew m1])
                       
          -- a b    
          --   e   

         ,seqstep 1 [buf_a, buf_b, buf_e, next] $ \[buf_a, buf_b, buf_e,next] ->
              foldIdentity    buf_a   ty $ \buf_a ->
              foldIdentity' 2 buf_b ty   $ \[buf_b, buf_b'] ->
              foldIdentity    buf_e   ty $ \buf_e ->

              par next (axiom buf_b')
                       (\p1 -> ffiCall ty "waveR" [tau₃,buf_a, buf_b, buf_e] (axiom p1))
         ]

foldYieldMaybe :: Int -> Id -> IdType -> ([Id] -> Id -> Program') -> Program'
foldYieldMaybe copies ref tyA f = tensor ref $ \r w ->
  copyData copies tyA r $ \refs ->
  f refs w 

foldYieldMaybe1 :: Id -> (Id -> Id -> Program') -> Program'
foldYieldMaybe1 ref f = tensor ref $ \r w -> f r w 

foldYield' :: Int -> Id -> IdType -> ([Id] -> Id -> Program') -> Program'
foldYield' copies ref tyA κ = foldYieldMaybe copies ref tyA (\refs v -> withL v (\v' -> κ refs v'))

foldIdentity' :: Int -> Id -> IdType -> ([Id] -> Program') -> Program'
foldIdentity' copies ref tyA κ = foldYieldMaybe copies ref tyA (\refs v -> withR v (\v' -> ignore v' (κ refs)))

foldConst :: Id -> IdType -> (Id -> Program') -> Program'
foldConst ref tyA κ = foldYield' 0 ref tyA (\[] id -> κ id)

foldIdentity :: Id -> IdType -> (Id -> Program') -> Program'
foldIdentity ref tyA κ = foldIdentity' 1 ref tyA (\[x] -> κ x)

foldSkip :: Id -> IdType -> Program' -> Program'
foldSkip ref tyA κ = foldIdentity' 0  ref tyA (\[] -> κ)

foldYield :: Id -> IdType -> (Id -> Id -> Program') -> Program'
foldYield ref tyA κ = foldYield' 1 ref tyA (\[x] y -> κ x y)

append :: IdType -> [(Id,IdSize)] -> (Id -> Program') -> Program'
append a chunks cont =
  cut (bigTensor (List.sum (fmap snd chunks)) a)
                        (\l' -> elimBigPar l' (fmap (\(a, sz) -> (sz, axiom a)) chunks))
                        (\l  -> slice l cont)    
  
waveBigSeqDeriv = D0 $ runFreshM $ do
  prev <- freshFrom "sp"
  curr <- freshFrom "sc"
  next <- freshFrom "sn"
  tau  <- freshFrom "tau"

  -- Type variables
  [n,a,b] <- mapM freshFrom ["n","A","B"]
  let szN = var n
  let [tyA,tyB] = fmap var [a,b]
  
  Deriv "waveLoop" (seqctx 
                           [(a, T TType  :^ 1)
                           ,(b, T TType  :^ 1)
                           ,(n, T TSize  :^ 1)]
    [(tau,  tyA :^ 1),
     (prev, bigSeq szN tyA :^ 1),
     (curr, bigSeq szN tyA :^ 1),
     (next, bigSeq szN (dual (tyB)) :^ 1)]) <$>
    stencilTBigSeq szN tyA tyB tau prev curr next 

waveBigSeqSymmDeriv = D0$ runFreshM $ do
  -- Edge cases and general case
  tau  <- freshFrom "tau"
  prev <- freshFrom "sp"
  next <- freshFrom "sn"

  -- Type variables
  [n,a,p] <- mapM freshFrom ["n","A","p"]
  let szN = var n
  let tyA = var a
  
  Deriv "waveBigSeqSymm" (seqctx
    [(a, T TType :^ 1)
    ,(n, T TSize :^ 1)
    ,(p, T (TProp (szN, Gt, 4)) :^ 1)]
    [(tau,  tyA :^ 1),
     (prev, bigSeq szN (tyA ⊗ tyA) :^ 1),
     (next, dual $ bigSeq szN (tyA ⊗ tyA) :^ 1)]) <$>
    stencilTBigSeqSymm szN tyA tau prev next 

waveBigSeqSymm2Cut szN ty a tau prev next =
  syncK' 2 (a :^ 1) (axiom tau) $ \[tau₁,tau₂] ->
  cut ty (\next -> stencilTBigSeqSymm szN a tau₁ prev next) 
         (\prev -> stencilTBigSeqSymm szN a tau₂ prev next)

waveBigSeqSymm2Fused szN ty a tau prev next steps =
  syncK' 2 (a :^ 1) (axiom tau) $ \[tau₁,tau₂] ->
  evaluateF [] steps =<<
  cut ty (\next -> stencilTBigSeqSymm szN a tau₁ prev next) 
         (\prev -> stencilTBigSeqSymm szN a tau₂ prev next)

waveBigSeqSymmCompose count szN a tau prev next = case count of
  1 -> stencilTBigSeqSymm szN a tau prev next
  n | n >= 1 ->
    syncK' 2 (a :^ 1) (axiom tau) $ \[tau₁,tau₂] ->
        cut (T (cArraySeq szN (T$ a :*: a)))
          (\next  -> stencilTBigSeqSymm szN a tau₁ prev next)
          (\prev ->  waveBigSeqSymmCompose (n-1) szN a tau₂ prev next)

waveBigSeqSymmComposeDeriv count = D0 $ runFreshM $ do
  -- Edge cases and general case
  prev <- freshFrom "sp"
  next <- freshFrom "sn"

  -- Type variables
  n <- freshFrom "n"; let szN = var n
  a <- freshFrom "A"; let tyA = var a
  p <- freshFrom "p"
  tau <- freshFrom "tau"

  let ty = bigSeq szN (tyA ⊗ tyA) 
  Deriv ("waveBigSeqSymmCompose_" ++ (show count)) (seqctx 
    [(a,T TType :^ 1)
    ,(n,T TSize :^ 1)
    ,(p,T (TProp (szN, Gt, count * 4)) :^ 1)]
    [(tau , unit tyA),
     (prev, unit ty),
     (next, unit $ dual ty)]) <$>
     waveBigSeqSymmCompose count szN tyA tau prev next

waveBigSeqSymm2Deriv = D0 $ runFreshM $ do
  -- Edge cases and general case
  prev <- freshFrom "sp"
  next <- freshFrom "sn"

  -- Type variables
  n <- freshFrom "n"; let szN = var n
  a <- freshFrom "A"; let tyA = var a
  p <- freshFrom "p"
  tau <- freshFrom "tau"

  let ty = bigSeq szN (tyA ⊗ tyA) 
  Deriv "waveBigSeqSymm2" (seqctx 
    [(n, T TSize :^ 1)
    ,(a, T TType :^ 1)
    ,(p, T (TProp (szN, Gt, 4)) :^ 1)]
    [(tau , unit tyA),
     (prev, unit ty),
     (next, unit $ dual ty)]) <$>
     waveBigSeqSymm2Cut szN ty tyA tau prev next

waveBigSeqSymm2FusedDeriv = D0 $ runFreshM $ do
  -- Edge cases and general case
  prev <- freshFrom "sp"
  next <- freshFrom "sn"
  tau <- freshFrom "tau"

  -- Type variables
  (n,szN) <- (id &&& var) <$> freshFrom "n"
  (a_,a) <- (id &&& var) <$> freshFrom "A"

  let ty = bigSeq (szN + 4) (a ⊗ a) 
  Deriv "waveBigSeqSymm2Fused" (seqctx 
    [(n, T TSize :^ 1)
    ,(a_, T TType :^ 1)]
    [(tau , unit a),
     (prev, unit ty),
     (next, unit $ dual ty)]) <$>
     waveBigSeqSymm2Fused (szN + 4) ty a tau prev next 100

-- (jyp-set-file-local-variable 'flycheck-haskell-ghc-executable "nix-ghc")

-- Local Variables:
-- flycheck-haskell-ghc-executable: "nix-ghc"
-- End:

plus :: Id -> (Either Id Id -> Program') -> Program'
plus a cont = do
  left <- refreshId a
  right <- refreshId a
  S contL <- cont (Left left)
  S contR <- cont (Right right)
  return$ S0$ Plus a (Branch left  contL)
                     (Branch right contR)

plusOption :: Id -> (Maybe Id -> Program') -> Program'
plusOption a cont = plus a $ \case
  Left a  -> cont $ Just a 
  Right a -> ignore a $ cont Nothing

mkLazy :: (Maybe Id -> Program') -> Id -> Program'
mkLazy prog a = plus a $ \case
  Left a  -> prog $ Just a
  Right b -> CLL.mix' [prog Nothing, yield b]

forceLazy :: Id -> (Id -> Program') -> Program' 
forceLazy x prog = withL x prog 

ignoreLazy :: Id -> Program' -> Program' 
ignoreLazy x prog = withR x $ \o -> ignore o $ prog

mapSeq1Deriv = D0 $ runFreshM $ do
  
  n <- freshFrom "n"; let szN = var n
  a <- freshFrom "A"; let tyA = var a
  b <- freshFrom "B"; let tyB = var b
  input <- freshFrom "input"
  output <- freshFrom "output"
  f <- freshFrom "f"

  Deriv "mapSeq1" (SeqCtx
      [n :::. T TSize 
      ,a :::. T TType
      ,b :::. T TType]
      [f      :::  (tyA ⊸ tyB) &: one :^ szN
      ,input  :::. bigSeq szN (option tyA)
      ,output :::. dual (bigSeq szN (option tyB))
      ]) <$>
      CLL.bigseq [ seqstep szN [input,output] $ \[input,output] -> plusOption input $ \case
        Just  input  -> withL f $ \f -> withL output $ \output -> apply f [input] (axiom output)
        Nothing      -> withR f $ \o -> ignore o $ withR output yield
        ]

mapSeq1 szN tyA tyB f input output =
  saveContext [("n", T (SizeT szN) ::: \[] -> T TSize)
              ,("a", tyA           ::: \[_] -> T TType)
              ,("b", tyB           ::: \[_,_] -> T TType)
              ] (\[sizeFromType -> Just sz, tyA, tyB] ->
                  [f      :::  (tyA ⊸ tyB) &: one :^ szN
                  ,input  :::. bigSeq szN (option tyA)
                  ,output :::. dual (bigSeq szN (option tyB))
                  ])
                  (derivCallD mapSeq1Deriv)

mapSeqDeriv = D0 $ runFreshM $ do
  
  n <- freshFrom "n"; let szN = var n
  a <- freshFrom "A"; let tyA = var a
  b <- freshFrom "B"; let tyB = var b
  input <- freshFrom "input"
  output <- freshFrom "output"
  f <- freshFrom "f"

  Deriv "mapSeq" (SeqCtx
      [n :::. T TSize 
      ,a :::. T TType
      ,b :::. T TType]
      [f      :::  (tyA ⊸ tyB) :^ szN
      ,input  :::. bigSeq szN tyA
      ,output :::. dual (bigSeq szN tyB)
      ]) <$>
      CLL.bigseq [ seqstep szN [input,output] $ \[input,output] -> apply f [input] (axiom output) ]

mapSeq szN tyA tyB f input output =
  saveContext [("n", T (SizeT szN) ::: \[] -> T TSize)
              ,("a", tyA           ::: \[_] -> T TType)
              ,("b", tyB           ::: \[_,_] -> T TType)
              ] (\[sizeFromType -> Just sz, tyA, tyB] ->
                  [f      :::  (tyA ⊸ tyB) :^ szN
                  ,input  :::. bigSeq szN tyA
                  ,output :::. dual (bigSeq szN tyB)
                  ])
                  (derivCallD mapSeqDeriv)


copySeq :: IdType -> IdSize -> Id -> [Id -> Program'] -> Program'
copySeq tyA szN seq conts = go conts $ \copies seqs' ->
        bigseq [seqstep szN (seq:seqs') $ \(seq:seqs') ->
                    copyData copies tyA seq $ \seqs ->
                    mix' [ axiom x y
                         | x <- seqs'
                         | y <- seqs
                         ]
                   ]
  where
    go :: [Id -> Program'] -> (Int -> [Id] -> Program') -> Program' 
    go [] cont = cont 0 []
    go (x:xs) cont = cut (bigSeq szN tyA)
                         (\a -> go xs (\n as -> cont (n+1) (a:as)))
                         (\a -> x a) 

copySeq1 :: IdType -> IdSize -> Id -> [Id -> Program'] -> Program'
copySeq1 tyA szN seq conts = go conts $ \copies seqs' ->
        bigseq [seqstep szN (seq:seqs') $ \(seq:seqs') ->
                plusOption seq $ \case
                  Nothing  -> mix' [ withR x $ yield | x <- seqs' ]
                  Just seq ->
                    copyData copies tyA seq $ \seqs ->
                    mix' [ withL x (axiom y)
                         | x <- seqs'
                         | y <- seqs
                         ]
                   ]
  where
    go :: [Id -> Program'] -> (Int -> [Id] -> Program') -> Program' 
    go [] cont = cont 0 []
    go (x:xs) cont = cut (bigSeq szN (option tyA))
                         (\a -> go xs (\n as -> cont (n+1) (a:as)))
                         (\a -> x a) 

copySeqLazy :: IdType -> IdSize -> Id -> [Id -> Program'] -> Program'
copySeqLazy tyA szN seq conts = go conts $ \copies seqs' ->
        bigseq [seqstep szN (seq:seqs') $ \(seq:seqs') ->
                    copyData copies tyA seq $ \seqs ->
                    mix' [ axiom x y
                         | x <- seqs'
                         | y <- seqs
                         ]
                   ]
  where
    go :: [Id -> Program'] -> (Int -> [Id] -> Program') -> Program' 
    go [] cont = cont 0 []
    go (x:xs) cont = cut (bigSeq szN tyA &: one)
                         (\a' -> plus a' $ \case
                                        Left a''   -> go xs (\n as -> cont (n+1) (a'':as))
                                        Right bot -> CLL.mix' [yield bot
                                                              ,go xs cont])
                         (\a -> x a) 

hoistOptionSeq szN tyA input output =
  {- Deriv "hoistOption" (SeqCtx
      [n   :::. T TSize
      ,a   :::. T TType]
      [input :::. bigSeq szN tyA
      ,output :::. dual (bigSeq szN (option tyA))
      ]) <$> -}
      CLL.bigseq [ seqstep szN [input,output] $ \[input,output] -> withL output (axiom input) ]


whether :: Id -> (Bool -> Program') -> Program'
whether b prog = plus b $ \case
  Left  o -> ignore o $ prog True
  Right o -> ignore o $ prog False
  

-- A is data
splitDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  a <- freshFrom "A"; let tyA = var a
  b <- freshFrom "B"; let tyB = var b

  pred <- freshFrom "pred"
  input <- freshFrom "input"
  [output₁,output₂] <- mapM freshFrom ["output","output"]

  Deriv "split" (SeqCtx
    [n  :::.  T TSize
    ,a  :::.  T TType
    ,b  :::.  T TType
    ]
    [pred   :::  (tyB ⊸ bool) &: one :^ szN
    ,input  :::. bigSeq szN (option (tyA ⊗ tyB))
    ,output₁ :::. dual (bigSeq szN (option tyA))
    ,output₂ :::. dual (bigSeq szN (option tyA))
    ]) <$>
    CLL.bigseq [ seqstep szN [input,output₁,output₂] $ \[input,output₁,output₂] ->
      plusOption input $ \case
        Nothing -> ignoreLazy pred $ CLL.mix' [withR output₁ $ yield
                                              ,withR output₂ $ yield
                                              ]
        Just input -> 
          tensor input $ \a k ->
            forceLazy pred $ \pred ->
                apply pred [k] $ \b ->
                  whether b $ \case 
                    True  -> CLL.mix' [withL output₁ (axiom a)
                                      ,withR output₂ $ yield
                                      ]
                    False  -> CLL.mix' [withL output₂ (axiom a)
                                       ,withR output₁ $ yield
                                       ]
      ]

splitSeq szN tyA tyB f input output₁ output₂ = 
  saveContext [("n", T (SizeT szN) ::: \[] -> T TSize)
              ,("a", tyA           ::: \[_] -> T TType)
              ,("b", tyB           ::: \[_,_] -> T TType)
              ] (\[sizeFromType -> Just sz, tyA, tyB] ->
                  [f       :::  (tyB ⊸ bool) &: one :^ szN
                  ,input   :::. bigSeq szN (option (tyA ⊗ tyB))
                  ,output₁ :::. dual (bigSeq szN (option tyA))
                  ,output₂ :::. dual (bigSeq szN (option tyA))
                  ])
                  (derivCallD splitDeriv)

diffSeq szN tyA a b =
  seqsync0 (tyA :^ 1) szN (ffiLit ("0" ::: tyA) . axiom) $ \previous ->
  bigseq [
    seqstep szN [a, previous, b] $ \[a, previous, b] ->
     foldYield previous tyA $ \previous next ->
     copyData 2 tyA a $ \[a₁, a₂] ->
     mix' [axiom a₁ next
          ,fMinus tyA previous a₂ (axiom b)
          ]
   ]

diff2Seq szN tyA a c =
  cut (bigSeq szN tyA)
    (\b' -> diffSeq szN tyA a b')
    (\b  -> diffSeq szN tyA b c)
    
apply :: Id  -- function
      -> [Id]  -- Value
      -> (Id -> Program')
      -> Program'
apply f [] cont     = cont f
apply f (x:xs) cont = par f (axiom x) (\g -> apply g xs cont)

unapply :: Int
        -> Id  -- function dual
        -> ([Id] -> Id -> Program') -- arguments and result dual (first the arguments, then the result)
        -> Program'
unapply 0 f cont         = cont [] f
unapply n f cont | n > 0 = tensor f (\a g -> unapply (n-1) g (\as ret  -> cont (a:as) ret))


foldDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  a <- freshFrom "A"; let tyA = var a
  b <- freshFrom "B"; let tyB = var b

  initial <- freshFrom "initial"
  f <- freshFrom "f"
  input <- freshFrom "input"
  output <- freshFrom "output"

  Deriv "fold" (SeqCtx
      [n   :::. T TSize 
      ,a   :::. T TType
      ,b   :::. T TType
      ]
      [initial :::. tyB
      ,f       :::  (tyB ⊸ tyA ⊸ option tyB) &: one :^ szN
      ,input   :::. bigSeq szN (option tyA)
      ,output  :::. dual tyB
      ]) <$> (
        flip (seqsync (tyB :^ 1) 1 szN (axiom initial)) (axiom output) $ \folder ->  
        CLL.bigseq [ 
          seqstep szN [input,folder] $ \[input,folder] ->
              plusOption input $ \case
                Nothing    -> foldSkip         folder tyB $ withR f $ \o -> ignore o $ halt 
                Just input -> foldYieldMaybe 1 folder tyB $ \[prev] next -> withL f $ \f -> apply f [prev,input] $
                  flip plusOption $ \case
                     Just next' -> withL next $ (axiom next')
                     Nothing    -> withR next $ (flip ignore halt)
          ]
      )

foldSeq sz tyA tyB initial fold input output = 
    saveContext [("n", T (SizeT sz) ::: \[]  -> T TSize)
                ,("a", tyA          ::: \[_] -> T TType)
                ,("b", tyB          ::: \[_,_] -> T TType)
                ]
                (\[sizeFromType -> Just sz, tyA, tyB] ->
                  [initial :::. tyB
                  ,fold    :::  (tyB ⊸ tyA ⊸ option tyB) &: one :^ sz
                  ,input   :::. bigSeq sz (option tyA)
                  ,output  :::. dual tyB
                  ]
                  )
    $ derivCallD foldDeriv

bestDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  p <- freshFrom "p" 
  a <- freshFrom "A"; let tyA = var a
  b <- freshFrom "B"; let tyB = var b

  input   <- freshFrom "input"
  output  <- freshFrom "output"
  better  <- freshFrom "better"
  empty   <- freshFrom "empty"

  Deriv "best" (SeqCtx
      [n   :::. tSize 
      ,p   :::. tProp (szN, Ge, 1)
      ,a   :::. T TType
      ,b   :::. T TType
      ]
      [better   :::  (tyB ⊸ tyB ⊸ bool) :^ (szN-1)
      ,empty    :::. tyA ⊗ tyB
      ,input    :::. bigSeq szN (tyA ⊗ tyB)
      ,output   :::. dual tyA
      ]) <$> (
      tensor empty $ \emptyA emptyB ->
      seqsync0 (tyB :^ 1)   szN (axiom emptyB) $ \bestKey ->
      flip (seqsync  (tyA :^ 1) 1 szN (axiom emptyA)) (axiom output) $ \best ->
       CLL.bigseq [
        seqstep 1         [best, bestKey, input] $ \[best, bestKey, input] ->
          tensor input $ \a b ->
          CLL.mix' [foldConst best    tyA (axiom a)
                   ,foldConst bestKey tyB (axiom b)
                   ]
       ,seqstep (szN - 1) [best, bestKey, input] $ \[best, bestKey, input] ->
           tensor input $ \a new ->
                copyData 2 tyB new $ \[new₁, new₂] ->
                foldYieldMaybe1 bestKey $ \old bestKey ->
                apply better [new₁, old] $ \b ->
                whether b $ \case
                  True  -> withL bestKey $ \bestKey -> 
                           foldConst best tyA $ \best ->
                           CLL.mix' [axiom a best, axiom bestKey new₂]

                  False -> withR bestKey $ \o -> ignore o $ 
                           foldSkip best tyA $ 
                           dump1 tyA a $ dump1 tyB new₂ $ halt
       ])

best sz tyA tyB better empty input output = 
  saveContext 
      [("n", T (SizeT sz) ::: \[] -> T TSize)
      ,("p", T Prop       ::: \[sz] -> tProp (var sz,Gt,0))
      ,("a", tyA          ::: \[_,_] -> T TType)
      ,("b", tyB          ::: \[_,_,_] -> T TType)
      ] (\[(sizeFromType -> Just szN), _, tyA, tyB] ->
            [better  :::  (tyB ⊸ tyB ⊸ bool) :^ (szN - 1)
            ,empty   :::. tyA ⊗ tyB
            ,input   :::. bigSeq szN (tyA ⊗ tyB)
            ,output  :::. dual tyA
            ])
      $ derivCallD bestDeriv

best1Deriv = D0$ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  p <- freshFrom "p" 
  a <- freshFrom "A"; let tyA = var a
  b <- freshFrom "B"; let tyB = var b

  input   <- freshFrom "input"
  output  <- freshFrom "output"
  better  <- freshFrom "better"
  empty   <- freshFrom "empty"

  Deriv "best1" (SeqCtx
      [n   :::. tSize 
      ,p   :::. tProp (szN, Ge, 1)
      ,a   :::. T TType
      ,b   :::. T TType
      ]
      [better   :::  ((tyB ⊸ tyB ⊸ bool) &: one) :^ (szN-1)
      ,input    :::. bigSeq szN (option$ tyA ⊗ tyB)
      ,output   :::. dual (option tyA)
      ]) <$> (
        cut ((tyA ⊕ one) ⊗ (tyB ⊕ one)) (\x -> par x (\a -> withR a $ yield)
                                                     (\a -> withR a $ yield)) $ \empty ->
        cutMany (szN-1) ((tyB ⊕ one)  ⊸  (tyB ⊕ one)  ⊸ bool) (\f -> unapply 2 f $ \[a,b] c ->
                     plusOption a $ \case
                       Nothing -> dump1 (tyB ⊕ one) b $ ignoreLazy better $ withR c $ yield
                       Just a -> 
                         plusOption b $ \case
                          Nothing -> ignoreLazy better $ dump1 tyB a $ withL c $ yield
                          Just b  -> forceLazy better $ \f' -> apply f' [a, b] (axiom c)) $ \better ->

        cutMany szN (((tyA ⊗ tyB) ⊕ one) ⊸ ((tyA ⊕ one) ⊗ (tyB ⊕ one)))
                   (\f -> unapply 1 f $ \[a] r -> plusOption a $
                       \case 
                          Nothing -> par r (\a -> withR a $ yield)
                                           (\a -> withR a $ yield)
                          Just p  -> tensor p $ \x y ->
                                       par r (\a -> withL a $ axiom x) (\a -> withL a $ axiom y)) $ \commutePlus ->
        cut (bigSeq szN ((tyA ⊕ one) ⊗ (tyB ⊕ one)))
          (mapSeq szN ((tyA ⊗ tyB) ⊕ one) ((tyA ⊕ one) ⊗ (tyB ⊕ one)) commutePlus input)
          (\points -> best szN (tyA ⊕ one) (tyB ⊕ one) better empty points output)

      )


best1 szN tyA tyB better input output = 
  saveContext
      [("n",T (SizeT szN)   ::: \[] -> tSize)
      ,("p",T Prop          ::: \[n] -> tProp (var n, Ge, 1))
      ,("a",tyA             ::: \[_,_] -> T TType)
      ,("b",tyB             ::: \[_,_,_] -> T TType)
      ] (\[sizeFromType -> Just szN, _, tyA, tyB] ->
        [better   :::  ((tyB ⊸ tyB ⊸ bool) &: one) :^ (szN-1)
        ,input    :::. bigSeq szN (option$ tyA ⊗ tyB)
        ,output   :::. dual (option tyA)
        ]) (derivCallD best1Deriv)

                 
tPoint = bigTensor 2 tDouble

with0th szK tyA p r =
  slice p $ \p ->
  CLL.splitAt 1 p $ \x xs ->
  copyData 2 tyA x $ \[x₁,x₂] ->
  par r (\p' -> elimBigPar p'  [(1, axiom x₁)
                               ,(szK - 1, axiom xs)
                               ])
        (axiom x₂)

quickhullStep1Deriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  p <- freshFrom "p"

  all <- freshFrom "all"
  left <- freshFrom "left"
  right <- freshFrom "right"

  Deriv "quickhullStep1" (SeqCtx
      [n        :::. T TSize
      ,p        :::. tProp (szN,Ge,1)
      ]
      [all      :::. bigSeq szN tPoint 
      ,left     :::. dual tPoint
      ,right    :::. dual tPoint
      -- outputs
      ]) <$> (
         syncK' 2 ((tPoint ⊗ tDouble) :^ 1) (\p -> par p (\r₁ -> coslice r₁ 2 (ffiLit ("NAN" ::: tDouble) . axiom))
                                                         (ffiLit ("NAN" ::: tDouble) . axiom))
                                                         
         $ \[empty₁, empty₂] ->
         cutMany szN (tPoint ⊸ (tPoint ⊗ tDouble))
            (\f -> unapply 1 f $ \[p] r -> with0th 2 tDouble p r)
         $ \with0th ->
             cut (bigSeq szN (tPoint ⊗ tDouble))
             (mapSeq szN tPoint (tPoint ⊗ tDouble) with0th all) $
             \points ->
                cutMany (szN-1) (tDouble ⊸ tDouble ⊸ bool)
                            (\f -> unapply 2 f $ \[a,b] r -> fCmpGtD a b (axiom r)) $ \isMax ->
                cutMany (szN-1) (tDouble ⊸ tDouble ⊸ bool)
                            (\f -> unapply 2 f $ \[a,b] r -> fCmpLtD a b (axiom r)) $ \isMin ->
                copySeq (tPoint ⊗ tDouble) szN points
                  [
                   \points -> best szN tPoint tDouble isMin empty₁ points left
                  ,\points -> best szN tPoint tDouble isMax empty₂ points right
                  ]
      )

quickhullStep2Deriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  p <- freshFrom "p"

  all <- freshFrom "all"
  left <- freshFrom "left"
  right <- freshFrom "right"
  top   <- freshFrom "top"
  above <- freshFrom "above"
  bottom <- freshFrom "bottom"
  below  <- freshFrom "below"

  Deriv "quickhullStep2" (SeqCtx
      [n        :::. T TSize
      ,p        :::. tProp (szN,Ge,1)
      ]
      [all      :::. bigSeq szN tPoint 
      ,left     :::. tPoint
      ,right    :::. tPoint
      -- outputs
      ,top      :::. dual (option tPoint)
      ,above    :::. dual (bigSeq szN (option tPoint))
      ,bottom   :::. dual (option tPoint)
      ,below    :::. dual (bigSeq szN (option tPoint))
      ]) <$> (
      cut (bigSeq szN (option tPoint)) (hoistOptionSeq szN tPoint all) $ \all ->
      cut (bigSeq szN (option (tPoint ⊗ tDouble))) (mapWithDistanceSeq szN left right all) $ \all' ->
      cut (bigSeq szN (option (tPoint ⊗ tDouble))) (splitTopAbove szN all' above top) $ \all'' ->
      cutMany szN (((tPoint ⊗ tDouble) ⊸ (tPoint ⊗ tDouble)) &: one)
        (mkLazy (\case Nothing -> halt
                       Just f  -> unapply 1 f $ \[p] r ->
                                  tensor p $ \p d ->
                                  par r (axiom p)
                                        (fNegD d . axiom)))
         $ \negSnd ->
           cut (bigSeq szN (option (tPoint ⊗ tDouble))) (mapSeq1 szN (tPoint ⊗ tDouble) (tPoint ⊗ tDouble) negSnd all'') $ \all''' ->
           cut (bigSeq szN (option (tPoint ⊗ tDouble))) (splitTopAbove szN all''' below bottom) $ \rest ->
           dump1 (bigSeq szN (option (tPoint ⊗ tDouble))) rest $ halt
           )

greaterThanZero d r = ffiLit ("0" ::: tDouble) $ \zero -> greaterThan d zero r 
greaterThan a b r = fCmpGtD a b (axiom r)

fstF tyA tyB a r = tensor a $ \a b -> dump1 tyB b $ axiom a r
sndF tyA tyB a r = tensor a $ \a b -> dump1 tyA a $ axiom b r

withDupl tyB x r  = tensor x $ \a b ->
                    copyData 2 tyB b $ \[b₁,b₂] ->
                    par r (\r -> par r (axiom a) (axiom b₁)) (axiom b₂)

splitTopAboveDeriv = runFreshM $ D0 <$> do
  n       <- freshFrom "n"; let szN = var n
  p       <- freshFrom "p"
  all     <- freshFrom "all"
  above_r <- freshFrom "above_r"
  top     <- freshFrom "top"
  rest    <- freshFrom "rest"

  Deriv "splitTopAbove" (SeqCtx
    [ n         :::. T TSize
    , p         :::. tProp (szN,Ge,1)
    ]
    [ all        :::. bigSeq szN (option (tPoint ⊗ tDouble)) 
    , above_r    :::. dual (bigSeq szN (option tPoint))
    , top        :::. dual (option tPoint)
    , rest       :::. dual (bigSeq szN (option (tPoint  ⊗ tDouble)))
    ]) <$> (
    cutMany szN (((tPoint ⊗ tDouble) ⊸ ((tPoint ⊗ tDouble) ⊗ tDouble)) &: one)
      (mkLazy (\case Nothing -> halt
                     Just f  -> unapply 1 f $ \[d] r -> withDupl tDouble d r)) $ \withDupl ->
     cut (bigSeq szN (option $ (tPoint ⊗ tDouble) ⊗ tDouble))
         (mapSeq1 szN (tPoint ⊗ tDouble) ((tPoint ⊗ tDouble) ⊗ tDouble) withDupl all)
     $ \all ->
     cutMany szN ((tDouble ⊸ bool) &: one) (mkLazy (\case
                                                           Nothing -> halt
                                                           Just f  -> unapply 1 f $ \[d] r -> greaterThanZero d r))
     $ \gt0 ->
       cut (bigSeq szN (option (tPoint ⊗ tDouble)))
           (\above' -> splitSeq szN (tPoint ⊗ tDouble) tDouble gt0 all above' rest)
           $ \above -> copySeq1 (tPoint ⊗ tDouble) szN above
                      [\above ->  cutMany szN (((tPoint ⊗ tDouble) ⊸ tPoint) &: one)
                                    (mkLazy (\case Nothing -> halt
                                                   Just f  -> unapply 1 f $ \[d] r -> fstF tPoint tDouble d r)) $
                                    \fst -> mapSeq1 szN (tPoint ⊗ tDouble) tPoint fst above above_r
                      ,\above -> cutMany (szN-1) ((tDouble ⊸ tDouble ⊸ bool) &: one)
                                    (mkLazy (\case Nothing -> halt
                                                   Just f  -> unapply 2 f $ \[a,b] r -> greaterThan a b r)) $ \isMax ->
                                      best1 szN tPoint tDouble isMax above top
                      ]
     )

splitTopAbove szN all above_r top rest =
  saveContext [("n", T (SizeT szN) ::: \[] -> T TSize)
              ,("p", T Prop        ::: \[n] -> tProp (var n, Ge, 1))
              ] (\[sizeFromType -> Just szN, _] -> 
                [ all        :::. bigSeq szN (option (tPoint ⊗ tDouble)) 
                , above_r    :::. dual (bigSeq szN (option tPoint))
                , top        :::. dual (option tPoint)
                , rest       :::. dual (bigSeq szN (option (tPoint  ⊗ tDouble)))
                ]) (derivCallD splitTopAboveDeriv)

mapWithDistanceSeq :: IdSize -> Id -> Id -> Id -> Id -> Program'
mapWithDistanceSeq szN left right all result =
  syncK szN (tPoint :^ 1) (axiom left)  $ \left  -> 
  syncK szN (tPoint :^ 1) (axiom right) $ \right -> 
  cutMany szN ((tPoint  ⊸ ((tPoint ⊗ tDouble))) &: one)
              (mkLazy (\case Nothing -> dump1 tPoint left $ dump1 tPoint right$ halt
                             Just f  -> unapply 1 f $ \[p] r ->
                                          copyData 2 tPoint p $ \[p₁, p₂] ->
                                          par r (axiom p₁)
                                                (lineDistance left right p₂)))
    (\wd -> mapSeq1 szN tPoint (tPoint ⊗ tDouble) wd all result)
   
                   
quickhullStep3Deriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  p <- freshFrom "p"

  all <- freshFrom "all"
  left <- freshFrom "left"
  top   <- freshFrom "top"
  right <- freshFrom "right"
  above_l <- freshFrom "above_left"
  top_l   <- freshFrom "top_left"
  above_r <- freshFrom "above_right"
  top_r   <- freshFrom "top_right"

  Deriv "quickhullStep3" (SeqCtx
      [n        :::. T TSize
      ,p        :::. tProp (szN,Ge,1)
      ]
      [all      :::. bigSeq szN tPoint 
      ,left     :::. tPoint
      ,top      :::. tPoint
      ,right    :::. tPoint
      -- outputs
      ,above_l  :::. dual (bigSeq szN (option tPoint))
      ,top_l    :::. dual (option tPoint)
      ,above_r  :::. dual (bigSeq szN (option tPoint))
      ,top_r    :::. dual (option tPoint)
      ]) <$>  (
      copyData 2 tPoint top $ \[top₁,top₂] ->
        cut (bigSeq szN (option tPoint)) (hoistOptionSeq szN tPoint all) $ \all ->
        cut (bigSeq szN (option (tPoint ⊗ tDouble))) (mapWithDistanceSeq szN left top₁ all) $ \all'  -> 
        cut (bigSeq szN (option (tPoint ⊗ tDouble))) (splitTopAbove szN all'  above_l top_l) $ \rest₁  -> 
        cut (bigSeq szN (option tPoint)) (\rest₂' ->
            cutMany szN (((tPoint ⊗ tDouble) ⊸ tPoint) &: one)
                        (mkLazy (\case Nothing -> halt
                                       Just f  -> unapply 1 f $ \[d] r -> fstF tPoint tDouble d r)) $
                        \fst -> mapSeq1 szN (tPoint ⊗ tDouble) tPoint fst rest₁ rest₂')
           $ \rest₂ ->
                cut (bigSeq szN (option (tPoint ⊗ tDouble))) (mapWithDistanceSeq szN top₂ right rest₂) $ \rest₃  -> 
                cut (bigSeq szN (option (tPoint ⊗ tDouble)))
                      (\rest₄' -> splitTopAbove szN rest₃ above_r top_r rest₄')
                      (\rest₄  -> dump1 (bigSeq szN (option (tPoint ⊗ tDouble))) rest₄ $ halt)
       )

dotDeriv :: Deriv0 Id Id 
dotDeriv = D0 $ runFreshM $ do
  [n,a,b,r] <- mapM freshFrom ["n","a","b","r"]
  let szN = var n
  Deriv "dot"
              SeqCtx {
                  _intuit = [n :::. T TSize]
                 ,_linear = [a :::  T PDouble :^ szN
                            ,b :::  T PDouble :^ szN
                            ,r :::. dual (T PDouble)]
                 }
            <$>
                 (foldMap2' szN
                          [a,b]
                          (T PDouble)
                          (\x y z -> x `fPlusD` y $ axiom z)
                          (\[a,b] z -> a `fTimesD` b $ axiom z)
                          (\u -> ffiDouble 0 $ axiom u)
                          (axiom r)
                 )

dot szN a b r = 
    saveContext [("n", T (SizeT szN) ::: \[]  -> T TSize)
                ]
                (\[sizeFromType -> Just szN] ->
                  [a :::  T PDouble :^ szN
                  ,b :::  T PDouble :^ szN
                  ,r :::. dual (T PDouble)]
                  )
    $ derivCallD dotDeriv

dotSeq szN a b r = 
    saveContext [("n", T (SizeT szN) ::: \[]  -> T TSize)
                ]
                (\[sizeFromType -> Just szN] ->
                  [a :::. bigTensor szN tDouble
                  ,b :::. bigTensor szN tDouble
                  ,r :::. dual (T PDouble)]
                  )
    $ derivCallD dotSeqInlinedDeriv

dotSeqDeriv :: Deriv0 Id Id 
dotSeqDeriv = D0 $ runFreshM $ do
  [n,a,b,r] <- mapM freshFrom ["n","a","b","r"]
  let szN = var n
  Deriv "dotSeq"
        SeqCtx {
            _intuit = [n :::. T TSize]
           ,_linear = [a :::. bigTensor szN tDouble
                      ,b :::. bigTensor szN tDouble
                      ,r :::. dual (T PDouble)]
           }
      <$>
           (ffiCall (bigTensor szN tDouble) "zipWith(∗)" [a,b] $ \c -> slice c $ \c -> 
                seqsync0 (tDouble :^ 1) (1 + szN)
                 (\z -> ffiLit' ("0.0" ::: tDouble) z)
                 (\s ->
                      CLL.bigseq [
                         seqstep szN [s] $ \[s] -> foldYield s tDouble $
                                             \prev next -> (prev `fPlusD'` c $ next)

                        ,seqstep 1   [s] $ \[s] -> foldIdentity s tDouble (axiom r) 
                         ])              )

dotSeqInlinedDeriv :: Deriv0 Id Id 
dotSeqInlinedDeriv = D0 $ runFreshM $ do
  [n,a,b,r] <- mapM freshFrom ["n","a","b","r"]
  let szN = var n
  Deriv "dotSeq"
        SeqCtx {
            _intuit = [n :::. T TSize]
           ,_linear = [a :::. bigTensor szN tDouble
                      ,b :::. bigTensor szN tDouble
                      ,r :::. dual (T PDouble)]
           }
      <$> (
           slice a $ \a ->
           slice b $ \b ->
           cut (bigTensor szN tDouble) (\c -> coslice c szN $ (\c -> (a `fTimesD` b) (axiom c))) $ \c -> slice c $ \c -> 
                seqsync0 (tDouble :^ 1) (1 + szN)
                 (\z -> ffiLit' ("0.0" ::: tDouble) z)
                 (\s ->
                      CLL.bigseq [
                         seqstep szN [s] $ \[s] -> foldYield s tDouble $
                                             \prev next -> (prev `fPlusD'` c $ next)
                        ,seqstep 1   [s] $ \[s] -> foldIdentity s tDouble (axiom r) 
                         ])              )


lineDistanceDeriv = D0 $ runFreshM $ do
  start <- freshFrom "start"
  end   <- freshFrom "end"
  point <- freshFrom "point"
  distance <- freshFrom "distance"

  Deriv "lineDistance" (SeqCtx []
     [start     :::. tPoint
     ,end       :::. tPoint
     ,point     :::. tPoint 
     ,distance  :::. dual tDouble
     ]) <$> (
    copyData 2 tPoint start $ \[start,start₁] ->
    slice start $ \start ->
    slice end   $ \end   ->
    cutMany 2 tDouble (fMinusD end start . axiom) $ \v ->
    cut tPoint (\v' -> coslice v' 2 (axiom v)) $ \v -> 

    -- rotate 90° counterclockwise
    slice v $ \v ->
    CLL.splitAt 1 v $ \x₁ y₁ ->                                
    fNegD y₁ $ \y₁' ->
    merge 1 y₁' x₁ $ \v' ->
    
    -- projection
    slice point  $ \point  ->
    slice start₁ $ \start₁ ->
    cutMany 2 tDouble (fMinusD point start₁ . axiom) $ \w ->
    cut tPoint (\v₁' -> coslice v₁' 2 (axiom v')) $ \v₁ ->
    cut tPoint (\w₁' -> coslice w₁' 2 (axiom w)) $ \w₁ ->
    cut tDouble (dotSeq 2 v₁ w₁) $ \base ->
    axiom distance base
    )
         
lineDistance start end point distance =
    saveContext [] (\[] ->
        [start     :::. tPoint
        ,end       :::. tPoint
        ,point     :::. tPoint
        ,distance  :::. dual tDouble
        ]) $ derivCallD lineDistanceDeriv

derivCallD = derivCall . _derivName . deriv0


tensorToParDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"
  let szN = var n
  a <- freshFrom "A"
  let tyA = var a
  xs <- freshFrom "x"
  ys <- freshFrom "y"
  Deriv "tensorToPar" (SeqCtx [
      n :::. tSize
     ,a :::. T TType]
     [xs :::. bigTensor szN tyA
     ,ys :::. dual (bigPar szN tyA)
     ])
     <$> (
     cut (bigSeq szN tyA)
       (\zs -> slice xs $ \x -> CLL.bigseq [ CLL.seqstep szN [zs] $ \[z] -> axiom z x ])
       (\zs -> slice ys $ \y -> CLL.bigseq [ CLL.seqstep szN [zs] $ \[z] -> axiom y z ]))
       

countTrueDeriv = D0$ runFreshM$ do
  n <- freshFrom "n"; let szN = var n
  xs <- freshFrom "xs" 
  c  <- freshFrom "c"
  Deriv "countTrueDeriv" (SeqCtx [
      n :::. tSize]
     [xs :::. bigSeq szN (one ⊕ one)
     ,c  :::. neg tInt
     ]) <$> (
     flip (seqsync (tInt :^ 1) 1 szN (ffiInt 0 . axiom)) (axiom c) (\acc ->
       CLL.bigseq [
         seqstep szN [xs, acc]$ \[xs, acc] ->
           whether xs $ \case
             True  -> foldYield acc tInt (\v r -> ffiInt 1 (\one -> 
                                                  fPlus tInt one v (axiom r)))
             False -> foldSkip acc tInt $ halt
             ]))
     
countTrue szN xs c =
  saveContext [("n", T (SizeT szN) ::: \[] -> T TSize)
              ] (\[sizeFromType -> Just sz] ->
                  [xs :::. bigSeq szN (one ⊕ one)
                  ,c  :::. neg tInt]) (derivCallD countTrueDeriv)


countNewLineDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n
  xs <- freshFrom "xs" 
  c  <- freshFrom "c"
  Deriv "countNewLine" (SeqCtx [
     n :::. tSize]
    [xs :::. bigSeq szN tChar
    ,c  :::. neg tInt
    ]) <$> (
    cut (bigSeq szN bool) (\seqIsNl' ->
      cutMany szN (tChar ⊸ bool) (\isnl' ->
        tensor isnl' $ \a res ->
          ffiChar '\n' $ \nl ->
          fCmpEq nl a (axiom res))                               
        (\isnl -> mapSeq szN tChar bool isnl xs seqIsNl'))
      (\seqIsNl -> countTrue szN seqIsNl c))

countNewLine szN xs c = 
  saveContext [("n", T (SizeT szN) ::: \[] -> T TSize)
              ] (\[sizeFromType -> Just sz] ->
                  [xs :::. bigSeq szN tChar 
                  ,c  :::. neg tInt]) (derivCallD countNewLineDeriv)

litTrue :: (Id -> Program') -> Program'
litTrue = cut bool (\b -> withL b yield)

litFalse :: (Id -> Program') -> Program'
litFalse = cut bool (\b -> withR b yield)

countWordDeriv = D0$ runFreshM$ do
  n <- freshFrom "n"; let szN = var n
  xs <- freshFrom "xs" 
  c  <- freshFrom "c"
  Deriv "countWord" (SeqCtx [
    n :::. tSize]
    [xs :::. bigSeq szN tChar
    ,c  :::. neg tInt
    ]) <$> (
    cut (bigSeq szN bool) (\wordBoundary' ->
        cut (bigTensor 1 bool)
            (\blanks' -> coslice blanks' 1 (litFalse . axiom))
            (\blanks  -> 
            windowSeq1 2 szN bool blanks $ \window2 ->
              CLL.bigseq [
               seqstep szN [xs, window2, wordBoundary'] $ \[x, w, wb'] ->
                 cut (bool ⊕ one) (\res' -> 
                   copyData 2 (T PChar) x $ \[x₀,x₁] ->
                   ffiCall bool "isspace" [x₀]$ \sp ->
                     whether sp $ \case
                       True -> mix' [discardData tChar x₁
                                    ,withL res' (litTrue . axiom)]
                       False -> ffiCall bool "isprint" [x₁]$ \pr ->
                         whether pr $ \case
                           True -> withL res' (litFalse . axiom)
                           False -> withR res' yield )
                    $ \res ->
                         plus res $ \case
                           Left b -> withL w $ \w' ->
                             par w' (axiom b)
                                    (\x' -> slice x' $ \x'' ->
                                            CLL.splitAt 1 x'' $ \prev curr ->
                                              whether prev $ \case
                                                False -> whether curr $ \case
                                                  True  -> litTrue (axiom wb')
                                                  False -> litFalse (axiom wb')
                                                True  ->
                                                  CLL.mix' [
                                                    whether curr $ \_ -> halt
                                                   ,litFalse (axiom wb')]
                                                   )
                           Right o₁ -> CLL.mix' [withR w $ \o₂ -> ignore o₁ $ ignore o₂ $ halt
                                                ,litFalse (axiom wb')
                                                ]
                 ]))
         (\wordBoundary -> countTrue szN wordBoundary c))

countWord szN xs c = 
  saveContext [("n", T (SizeT szN) ::: \[] -> T TSize)
              ] (\[sizeFromType -> Just sz] ->
                  [xs :::. bigSeq szN tChar 
                  ,c  :::. neg tInt]) (derivCallD countWordDeriv)


wordCountDeriv = D0$ runFreshM$ do
  n <- freshFrom "n"; let szN = var n
  xs <- freshFrom "xs" 
  lines  <- freshFrom "lines"
  words  <- freshFrom "words"
  Deriv "wordCount" (SeqCtx [
    n :::. tSize
    ]
    [xs    :::. bigSeq szN tChar
    ,lines :::. neg tInt ⊕ one
    ,words :::. neg tInt ⊕ one
    ]) <$> (copySeqLazy tChar szN xs [\xs₁ -> plus lines $ \case
             Left lines' -> withL xs₁ $ \xs₁' -> countNewLine szN xs₁' lines'
             Right o₁    -> withR xs₁ (\o₂ -> ignore o₁$ ignore o₂$ halt),
           \xs₂ -> plus words $ \case
             Left words' -> withL xs₂ $ \xs₂' -> countWord szN xs₂' words'
             Right o₁    -> withR xs₂ (\o₂ -> ignore o₁$ ignore o₂$ halt)
             ])
             
    
                  
