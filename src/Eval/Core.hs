{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
-- | Core module for cut-elimination
module Eval.Core where

import Fuel
import Fresh
import LL
import Type.Size
import Control.Applicative
import Control.Monad
import Control.Monad.Reader
import Control.Lens hiding (Fold)
import Control.Monad.Trans
import Data.List (find, (\\), findIndex)
import Data.Default
import Control.Monad.Except.Extra
import ContM

data EvalEnv = EvalEnv {
  -- Returns the cost of evaluating the predicate
  _evalCost         :: CutInfo -> Natω
  --
  -- Stores knowledge about sizes, normalized
  -- The operator says how the size relates to 0.
  -- e.g. (IdSize, ≥) ~ (IdSize > 0)
 ,_sizeAssertions   :: IdSizePredicates

  -- Allow insertion of branches when size comparisons
  -- can't be decided
 ,_branchingAllowed :: Bool
  }
$(makeLenses ''EvalEnv)

instance Default EvalEnv where
  def = EvalEnv {
    _evalCost         = const$ Just 1
   ,_sizeAssertions   = def
   ,_branchingAllowed = False 
   }

type EvalM = ReaderT EvalEnv (ExceptT String (FuelT FreshM))

class HasRef a => WorksOn a where
  worksOn :: a -> (Ref a) -> Bool

instance (Eq ref) => WorksOn (Seq' name ref) where
  worksOn (S0 seq) = go seq
    where
        go What{..} x = x `elem` [zElim | ElemLinear zElim <- zVars]
        go Permute{..} _ = False
        go Foldmap{..} x = x `elem` fmArr
        go (BigSeq fence) x = any (`worksOn` x) fence
        go Foldmap2{..} x = x `elem` fmArr
        go (IntOp _ x1 x2 _ _) x = x == x1 || x == x2
        go (IntCmp _ x1 x2 _ _) x = x == x1 || x == x2
        go (Sync{}) _x = False
        go (Cross _ x1 _ _ _) x = x == x1
        go (Par{..})     x = x == zElim
        go (Plus {..})      x1 = zElim == x1
        go (With _ x _ _ _)    x1 = x == x1
        go (SOne _ x _)          x1 = x == x1
        go (SBot x)              x1 = x == x1
        go (SZero x _)           x1 = x == x1
        go TApp{..}              x1 = zElim == x1
        go TUnpack{..}           x1 = zElim == x1
        go (Offer _ x _ _)       x1 = x == x1
        go (Save x _ _)          x1 = x == x1
        go (Load {}) _ = False
        go (Demand x _ _)       x1 = x == x1
        go (Fold x _ _)           x1 = x == x1
        go (Unfold x _ _)         x1 = x == x1
        go (Ignore _ x _)         x1 = x == x1
        go (Alias _ x _ _)        x1 = x == x1
        go (Mem x _ _ _ _ _ _)    x1 = x == x1
        go (NCut{})                _ = False
        go (Ax{..})                x = x `elem` [wElim,zElim]
        go (BigPar{..})     x = x == zElim
        go (BigTensor{..})     x = x == zElim
        go (Tile _ _ _)           _  = False
        go (NatRec _ _ _ _ z _ _) x = x == z
        go Merge {..} x = x == zElim || x == wElim
        go Split{..}             x = x == zElim
        go (IntLit{})             _ = False
        go (IntCopy z _ _ _)      x = x == z
        go (IntIgnore z _)        x = x == z
        go (Fix{})                _ = False
        go (Halt{})               _ = False
        go (SizeCmp{})            _ = False

instance (Eq ref) => WorksOn (SeqStep' seq sz name ref) where
  worksOn step ref = ref `elem` seqStepRefs step

consumeCut :: CutInfo -> a -> EvalM a -> EvalM a
consumeCut NoCommute a _ = return a
consumeCut ci0 a m = do
  cost <- view evalCost <*> pure ci0
  consumeNω cost a m

withSizeKnowledge :: IdSizePredicates -> EvalM a -> EvalM a
withSizeKnowledge szp = local (sizeAssertions .~ szp)

withSizeAssertion :: (IdSize, CmpOp, IdSize) -> EvalM a -> EvalM a
withSizeAssertion t@(sz₁, b, sz₂) = liftEvalNIA_$ sizePredAdd t

-- | There are two sorts of decisions that must be made when simplifying a program:
--   + Equality test (for zipping things together)
--   + ≤ test (for splitting)
--
--   Handling equality can in most cases be done as a particular case of the
--   handling of ≤ or ≥, so it's not that important to tell them apart.
--
--   Besides, a < b can be expressed as a + 1 ≤ b, so nothing is lost.
--
sizePerhapsBranchLEq :: IdSize -> IdSize -> ContMR EvalM IdSeq BOrdering
sizePerhapsBranchLEq szl szr f = do
  sizeNoBranchLEq szl szr `liftEvalNIA` \res -> 
    case res of
      Just op -> f op
      Nothing-> do
        allowed <- view branchingAllowed
        if allowed
          then S0 <$> (SizeCmp szl szr <$> sequence [(Le,) <$> withSizeAssertion (szl, Le, szr) (rM (f BLEq))
                                                    ,(Ge,) <$> withSizeAssertion (szl, Ge, szr) (rM (f BGEq))])
          else do
            knows <- view sizeAssertions
            throwError $ "Can't decide size ordering " ++ show szl ++ " ⋚ " ++ show szr ++ ".\n" ++
                         "Knowledge: " ++ show knows ++ "." 

sizePerhapsBranchEq :: IdSize -> IdSize -> ContMR EvalM IdSeq Bool
sizePerhapsBranchEq szl szr f = do
  sizePredProve (szl,Eq,szr) `liftEvalNIA` \res -> 
    case res of
      Just True  -> f True
      Just False -> f False
      Nothing -> do
        allowed <- view branchingAllowed
        if allowed
          then S0 <$> (SizeCmp szl szr <$> sequence [(Eq,) <$> withSizeAssertion (szl, Eq, szr) (rM (f True))
                                                    ,(Ne,) <$> withSizeAssertion (szl, Ne, szr) (rM (f False))])
          else do
            knows <- view sizeAssertions
            throwError $ "Can't decide size equality " ++ show szl ++ " ?= " ++ show szr ++ ".\n" ++
                         "Knowledge: " ++ show knows ++ "." 

newtype WrapEvalNIA a = WrapEvalNIA { unwrapEvalNIA :: EvalM a } deriving (Monad,Functor,Applicative)

instance MonadReader (SizePredicates Id) WrapEvalNIA where
  ask = WrapEvalNIA$ view sizeAssertions
  local f = WrapEvalNIA . local (sizeAssertions %~ f) . unwrapEvalNIA

liftEvalNIA :: (forall m . MonadNIAIdSize m => ContMR m a c) -> ContMR EvalM a c 
liftEvalNIA k = unwrapEvalNIA . k . (WrapEvalNIA .)
  
liftEvalNIA_ :: (forall m . MonadNIAIdSize m => ContM_' m c) -> ContM_' EvalM c 
liftEvalNIA_ k = unwrapEvalNIA . k . WrapEvalNIA 

