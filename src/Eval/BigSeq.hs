{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
module Eval.BigSeq where

import Eval.Core
import LL
import Type
import Control.Monad
import Control.Monad.Writer
import Data.List
import Fresh
import Control.Applicative
import Development.Placeholders
import Debug.Trace

-- ^ Splits an interval in a BigSeq.
--   Returns:
--     + Prefix of interval with the given size.
--     + Sync point, and/or remainder of the interval.
--     + List of references from outside that must be split, together
--       with their names.
splitInterval :: IdSize -> IdSeqStep -> 
                 EvalM (([IdSeqStep],
                         [IdSeqStep]),
                        [(Id, (Id, IdSize), 
                              (Id, IdSize))])
--splitInterval _ _ | trace "splitInterval" False = undefined
splitInterval splitSz si@SeqStep{} | splitSz == seqStepSz0 si = return (([si], []), [])
splitInterval 0       si                                      = return (([], [si]), [])
splitInterval splitSz si@SeqStep{..} = 
  do
    splitVec <- do
      let fromChunk = [name | (ref,name) <- seqChunk]
      let noSplit = fromChunk 
      forM (seqIdRefsUsed (S seqProg) \\ noSplit) $ \vec -> do
        name1 <- refreshId vec
        name2 <- refreshId vec
        return (vec, (name1, splitSz), (name2, seqSz - splitSz))

    (seqChunkA, substsASeq) <- runWriterT $
        forM seqChunk $ \(ref, name) -> do
          name' <- refreshId name
          tell $ [(name, name')]
          return (ref, name')

    let seqChunkB = seqChunk

    let substsVecA = [(vec, a) | (vec, (a, _), _) <- splitVec]
    let substsVecB = [(vec, b) | (vec, _, (b, _)) <- splitVec]
    seqProgA <- renameList (substsVecA ++ substsASeq)  <$> refreshBindersSeq (S seqProg)
    seqProgB <- renameList (substsVecB) <$> pure (S seqProg)

    let i1 = SeqStep {
           seqSz = splitSz,
           seqChunk = seqChunkA,
           seqProg = r0$ seqProgA
         }

    let i2 = SeqStep {
                 seqSz = seqSz - splitSz,
                 seqChunk = seqChunkB,
                 seqProg = r0$ seqProgB
               }

    return (([i1], [i2]), splitVec)
