{-# LANGUAGE RankNTypes, ViewPatterns, DeriveFunctor,  GeneralizedNewtypeDeriving, OverloadedStrings #-}
-- | Typecheck a sequent, checking for mismatched types, and inferring the types
-- of metavariables.
module Resolve (resolve,unsafeResolve) where

import Text.PrettyPrint

import Data.Map (Map)
import qualified Data.Map as M

import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Error
import Control.Monad.Writer hiding ((<>))
import Control.Arrow (first,second)

import Pretty(pType)

import Data.List (elemIndex,delete)
import Data.Maybe (fromMaybe)

import LL hiding (second,matchType)
import qualified LL

import Id
import Perm

import Text.Show.Pretty (ppShow)

__ :: Type
__ = wild

unsafeResolve :: IdDeriv -> Deriv
unsafeResolve d = case resolve d of
  (Right d',_)    -> d'
  (Left err,msg) -> error $ "unsafeResolve: " ++ show err ++ "\n:" ++ render msg ++ "\n:" ++ ppShow d

resolve :: IdDeriv -> (Either Err Deriv,Doc)
resolve = runT . dsDeriv

insertIntCtx :: [(Id,IdType)] -> T a -> T a
insertIntCtx []          m = m
insertIntCtx ((x,t0):xs) m = do
    t <- trType t0
    insertIVar x t $ insertIntCtx xs m

dsDeriv :: IdDeriv -> T Deriv
dsDeriv (Deriv who int_ctx bs s) = do
    insertIntCtx (reverse int_ctx) $ do

        int_ctx' <- gets st_iscope

        bs' <- forM bs $ \ (x,t) -> (,) x <$> bind x t
        (s',sb) <- trSeq s

        pm <- either (throw . uncurry (PermutationError Nothing)) return
                     (getPermutation sb (map fst bs'))

        return $ Deriv who (map (first id_name) int_ctx')
                           (map (first id_name) bs')
                           (Exchange pm s')

newtype T a = T ((StateT St (ErrorT Err (Writer [Doc]))) a)
  deriving
    ( Functor, Applicative, Monad
    , MonadState St
    , MonadWriter [Doc]                        -- debug messages
    , MonadError Err                           -- errors
    )

type RCtx = [(Id,Type)]
data St = St
    { st_iscope    :: RCtx -- order is important here; types can refer to previous types.
    , st_types  :: Map Id Type
    , st_ty_mod :: Id -> Type -> T Type -- strips off tensors from distribute
    }

-- | Used to strip off tensors in Distribute
withTypeMod :: (Id -> Type -> T Type) -> T a -> T a
withTypeMod new_mod m = do
    old_mod <- gets st_ty_mod
    modify $ \ st -> st { st_ty_mod = \ i t -> new_mod i =<< old_mod i t }
    r <- m
    modify $ \ st -> st { st_ty_mod = old_mod }
    return r

-- | Report a debug message
report :: Doc -> T ()
report = tell . (:[])

-- | Make the context a doc for debug messages
_ctxDoc :: T Doc
_ctxDoc = do
    tvs <- gets st_iscope
    ctx <- M.toList <$> gets st_types
    return (prettyCtx tvs ctx)

throw :: Err' -> T a
throw e = do
    tvs <- gets st_iscope
    throwError (Err tvs e)

runT :: forall a. T a -> (Either Err a,Doc)
runT  (T m)
    = second vcat . runWriter . runErrorT
    $ evalStateT m (St [] M.empty (\ _ t -> return t))

data Err = Err
    RCtx -- ^ intuitionistic context (for instance type variables in scope)
    Err' -- ^ actual error

instance Show Err where
    show (Err tvs e) = render (prettyErr tvs e)

data Err'
    = MismatchTypes Seq_ Id Type Id Type
    -- ^ Ax and Cut with types not matching up
    | InsufficientTypeInfo Id Id
    -- ^ Cut with no type information
    | UnboundIdentifier Id
    -- ^ Unbound
    | BoundIdentifier Id Type Id Type
    -- ^ Identifier shadowed
    --   @BoundIdentifier x t1 y t2@ means x : t1 tried to be bound,
    --   but is already bound at y : t2 (hopefully at another position in the source code!)
    | IdentifierEscapes Id
    -- ^ An identifier that had to be used wasn't
    | TypeError Id Type Type
    -- ^ Type error
    --   @TypeError z t1 t2@ means z should have a type of form t1, but had t2
    | NotBang Id Type Id
    -- ^ Offering (?-destruction) with non-bang typed identifiers in context
    --   @NotBang x t z@ means x had type t which is not bang when offering for z
    | PermutationError (Maybe Id) [Id] [Id]
    -- ^ Case with different contexts in the two branches
    | Hole [(Id,Type)]
    -- ^ The identifiers and their types in a hole
    | NoSuchDeriv Id
    -- ^ Refer to a deriv that does not exist
    | DerivMismatchContext Id RCtx [Type] [Type] [Type]
    -- ^ Derivation does not work
    | IncorrectlyAppliedAlias Id [Id] [Type]
    -- ^ Alias arity mismatch
    | Fail String
    -- ^ Fail in the monad

prettyErr :: RCtx -> Err' -> Doc
prettyErr tvs e = case e of
    MismatchTypes s x xt y yt ->
        hang ("Types are not dual of each other in a usage of" <+> prettySeq s <> ":") 4
             (p_ctx [(x,xt),(y,yt)])
    InsufficientTypeInfo x y ->
        "At least one of" <+> prettyId x <+> "and" <+> prettyId y <+>
        "needs to have a type specified in a usage of connect"
    UnboundIdentifier x -> prettyId x <+> "is not bound"
    BoundIdentifier x xt y yt ->
        hang (prettyId x <+> "is already bound:") 4
             (p_ctx [(x,xt),(y,yt)])
    IdentifierEscapes x -> prettyId x <+> "is never used"
    TypeError x t t' ->
        prettyId x <+> "has type" <+> p_ty t' <+> "which does not match the shape" <+> p_ty t
    NotBang x t z ->
        prettyId x <+> "has type" <+> p_ty t <+> "which is not of bang type when offering" <+> prettyId z
    PermutationError Nothing xs [] ->
        hang "Assumptions not used in the sequent:" 4 (ids xs)
    PermutationError Nothing xs ys ->
        -- This case cannot happen, the variables would be reported as unbound instead
        hang "Derivation variables and assumption are not a permutation of each other" 4
            (ids xs $$ ids ys)
    PermutationError (Just z) xs ys ->
        hang (prettyId z <+> "deconstructed, but the case arms do not use the same variables") 4
            $  "Left:" <+> ids xs
            $$ "Right:" <+> ids ys
    Hole ctx -> hang "Hole context:" 4 (p_ctx ctx)
    NoSuchDeriv d -> prettyId d <> colon <+> "no derivation with that name"
    DerivMismatchContext d tvs' ctx tys ctx' ->
        hang (prettyId d <> colon <+> "splicing derivation does not match in types:") 4
--            $  "spliced type variables:" <+> sep (pc (map text tvs'))
            $ "invoked types:" <+> sep (pc (map p_ty tys))
            $$ "spliced types:" <+> sep (pc (map (prettyType $ tvs') ctx))
            $$ "invoked context:" <+> sep (pc (map p_ty ctx'))
    IncorrectlyAppliedAlias x is ts ->
        hang (prettyId x <> colon <+> "incorrectly applied alias:") 4
            $  "defined with these variables:" <+> ids is
            $$ "invoked with these arguments:" <+> sep (pc (map p_ty ts))
    Fail s -> "Unknown error:" <+> text s
  where
    ids :: [Id] -> Doc
    ids [] = parens "none"
    ids xs = sep (pc (map prettyId xs))

    pc = punctuate comma

    p_ty  = prettyType tvs
    p_ctx = prettyCtx tvs

prettyCtx :: RCtx -> RCtx -> Doc
prettyCtx tvs ctx = vcat [ prettyId x <+> colon <+> prettyType tvs t | (x,t) <- ctx ]

prettyId :: Id -> Doc
prettyId (Id n _ Nothing) =                                 colon <+> text n
prettyId (Id n _ (Just (y,x))) = int y <> colon <> int x <> colon <+> text n
-- prettyId = text . show

prettyType :: RCtx -> Type -> Doc
prettyType tvs = pType 0 (map (id_name . fst) tvs ++ ["<gbl:" ++ show x ++ ">" | x <- [0 :: Int ..]])

prettySeq :: Seq_ -> Doc
prettySeq s = case s of
    Ax_ -> "<->"
    Cut_ -> "connect"

instance Error Err where
    strMsg = Err [] . Fail


data Seq_ = Ax_ | Cut_
  deriving Show

typeOf :: Id -> T Type
typeOf x = do
    m <- gets st_types
    ty_mod <- gets st_ty_mod     -- important to run st_ty_mod here!
    case M.lookup x m of
        Just t  -> ty_mod x t
        Nothing -> throw (UnboundIdentifier x)

saveCtx :: T (Id -> T Type)
saveCtx = do
    m <- gets st_types
    ty_mod <- gets st_ty_mod     -- important to save st_ty_mod here!
    return $ \ x -> ty_mod x $ fromMaybe
        (error $ "saveCtx: internal error, lost identifier " ++ show x)
        (M.lookup x m)


eat :: Id -> T Type
eat x = do
    t <- typeOf x
    modify $ \ st -> st { st_types = M.delete x (st_types st) }
    return t


bind :: Id -> IdType -> T Type
bind x t0 = do
    t <- trType t0
    bind' x t >> return t

bind' :: Id -> Type -> T ()
bind' x t = do
    m <- gets st_types
    case (lookupWithKey x m,M.insert x t m) of
        (Just (x',t'),_) -> throw (BoundIdentifier x t x' t')
                             -- Shadowing is not allowed, so we throw an error here
        (Nothing,m') -> modify (\ st -> st { st_types = m'})

lookupWithKey :: Ord k => k -> Map k v -> Maybe (k,v)
lookupWithKey k m = (`M.elemAt` m) <$> M.lookupIndex k m

insertTyVar :: Id -> T a -> T a
insertTyVar x = insertIVar x Type

lookupIVar :: Id -> T Type
lookupIVar i = do
    ts <- gets st_iscope
    {-
    let fs :: [Type -> Type]
        fs = iterate (apply wk .) id
        -}
    case lookup i ts of -- (zipWith (\ f (n,t) -> (n,f t)) fs ts) of
        Nothing -> throw (UnboundIdentifier i)
        Just ty -> return ty

insertIVar :: Id -> Type -> T a -> T a
insertIVar x t m = do
    s <- get
    modify $ \ st -> st
        { st_iscope = (x,apply wk t):map (\(n,t') -> (n,apply wk t')) (st_iscope st)
        , st_types  = M.map (apply wk) (st_types st)
        } -- all identifiers gets their variables shifted
    r <- m
    put s
    return r

-- | Break a context at given position.
munch :: Id -> [Id] -> T ([Id],[Id])
munch x b = case break (== x) b of
    (l,_:r) -> return (l,r)
    _       -> throw (IdentifierEscapes x)

reorder :: (Int -> Int) -> Id -> (Seq,[Id]) -> T (Seq,[Id])
reorder k z (s,zb) = case elemIndex z zb of
    Nothing -> throw (IdentifierEscapes z)
    Just n  -> let (zb',pm) = perm zb (swap x n) in return (exchange pm s,zb' `without` x)
      where x = k (length zb)

putFirst,putLast :: Id -> (Seq,[Id]) -> T (Seq,[Id])
putFirst = reorder (const 0)
putLast = reorder pred


isBang :: Type -> Bool
isBang Bang{} = True
isBang _      = False

matchType :: Id -> Type -> Type -> T [Type]
matchType z t0 t1
    = maybe (throw (TypeError z t0 t1)) return (LL.matchType t0 t1)

-- | The 'false' Boson (‽)
false :: Boson
false = False

name :: Id -> Doc
name = text . id_name


trType :: IdType -> T Type
trType t = do
    tvs <- gets st_iscope
    case idxType t (map fst tvs) of
        Right t' -> return $ bimapType id_name id t'
        Left tv  -> throw (UnboundIdentifier tv)

-- Returns the translated sequent and its context
trSeq :: IdSeq -> T (Seq,[Id])
trSeq sq = case sq of

    Exchange pm s -> do
        (s',ids) <- trSeq s
        return (s',applyPerm pm ids)

    Ax _ x y -> do
        tx <- eat x
        ty <- eat y
        do  tvs <- gets st_iscope
            report $ hang "Ax" 4
                $  name x <+> ":" <+> prettyType tvs tx
                $$ name y <+> ":" <+> prettyType tvs ty
                $$ "tvs:" <+> sep (punctuate comma (map (text . id_name . fst) tvs))
        unless (tx `eqType` neg ty) (throw (MismatchTypes Ax_ x tx y ty))
        return (Ax ty 0 1,[x,y])

    Cut x y ty _IGNORE sx sy -> do
        void $ bind x (neg ty)
        (sx',xb) <- putFirst x =<< trSeq sx

        ty' <- bind y ty
        (sy',yb) <- putFirst y =<< trSeq sy

        return (Cut (id_name x) (id_name y) ty' (length xb) sx' sy',xb ++ yb)

    Cross _β _ty x y z s -> do

        tz <- eat z
        [tx,ty] <- matchType z (__ :*: __) tz

        bind' x tx
        bind' y ty
        (s',bs) <- trSeq s
        let bs' = delete x (delete y bs)

        pm <- case getPermutation bs (x:y:bs') of
                Left (xs,ys) -> throw (PermutationError (Just z) xs ys)
                Right pm -> return pm

        report $ hang (name x <> comma <> name y <> equals <> name z <+> colon) 4
             $ hsep (map name bs)
            $$ hsep (map name (x:y:bs'))
            $$ text (show pm)

        return (Cross false tx (id_name x) (id_name y) 0 (exchange pm s'),z:bs')

    Par _β _ x y z sx sy -> do
        tz <- eat z

        [tx,ty] <- matchType z (__ :|: __) tz

        bind' x tx
        (sx',xb) <- putLast x =<< trSeq sx

        bind' y ty
        (sy',yb) <- putFirst y =<< trSeq sy

        return (Par false tz (id_name x) (id_name y) (length xb) sx' sy',xb ++ [z] ++ yb)

    Distribute x z s -> do

        tz <- eat z
        [tx] <- matchType z (bigPar __) tz

        -- only the types of the identifier of the _current_ scope should
        -- have their big tensors stripped off!
        in_scope <- gets (M.keys . st_types)

        let demote u tu
                | u `elem` in_scope = do
                    [tu'] <- matchType u (bigTensor __) tu
                    return tu'
                | otherwise = return tu

        (s',l,r) <- withTypeMod demote $ bindTrSeqMunch x tx s

        return (Distribute (id_name x) (length l) s',l ++ [z] ++ r)

    Foldmap u v w _ z tym r s t -> do

        tz <- eat z
        tm <- trType tym
        [ta] <- matchType z (bigPar __) tz

        bind' u (bigPar (ta :*: neg tm))
        (r',rb) <- putFirst u =<< trSeq r
        bind' v (bigPar (tm :*: (tm :*: neg tm)))
        (s',sb) <- putFirst v =<< trSeq s
        bind' w tm
        (t',tb) <- putFirst w =<< trSeq t

        return (Foldmap (id_name u) (id_name v) (id_name w)
                (length sb) (length rb) tm r' s' t', rb ++ [z] ++ sb ++ tb)

    Tile x s -> do

        bind' x (bigPar One)

        (s',ctx) <- putFirst x =<< trSeq s
        return (Tile (id_name x) s',ctx)

    Plus x y z sx sy -> do

        tz <- eat z
        [tx,ty] <- matchType z (__ :+: __) tz

        ctx <- get

        put ctx
        bind' x tx
        (sx',xb) <- putFirst x =<< trSeq sx

        put ctx
        bind' y ty
        (sy',yb) <- putFirst y =<< trSeq sy

        p <- either (throw . uncurry (PermutationError (Just z)))
                    (return . (0:) . map succ)
                    (getPermutation yb xb)

        report $ hang ("case" <+> name z <+> "{" <+> name x <> comma <> name y <+> "}" <+> colon) 4
             $ hsep (map name xb)
            $$ hsep (map name yb)
            $$ text (show p)

        return (Plus (id_name x) (id_name y) 0 sx' (exchange p sy'),z:xb)

    With _β _ x ch z s -> do

        tz <- eat z
        [tx,ty] <- matchType z (__ :&: __) tz

        (s',l,r) <- bindTrSeqMunch x (choice ch tx ty) s
        return (With false tz (id_name x) ch (length l) s',l ++ [z] ++ r)

    SBot x -> do
        tx <- eat x
        [] <- matchType x Bot tx
        return (SBot 0,[x])

    SOne _β x s -> do

        tx <- eat x
        [] <- matchType x One tx
        (s',b) <- trSeq s
        return (SOne false 0 s',x:b)

    SZero x xs -> do
        tx <- eat x
        [] <- matchType x Zero tx
        _rest <- mapM eat xs
        return (SZero 0 [1..length xs-2],x:xs)

    -- let x = z @ t in s
    TApp _β _ x z t0' s -> do
        tz <- eat z
        t' <- trType t0'
        [apply (subst0 t') -> tx] <- matchType z (Forall "" __) tz

        (s',l,r) <- bindTrSeqMunch x tx s
        return (TApp false tz (id_name x) (length l) t' s',l ++ [z] ++ r)

    -- let a @ x = z in s
    TUnpack a x z s -> do

        tz <- eat z
        [tx] <- matchType z (Exists "" __) tz

        insertTyVar a $ do
            (s',l,r) <- bindTrSeqMunch x tx s
            return (TUnpack (id_name a) (id_name x) (length l) s',l ++ [z] ++ r)

    -- offer x for z in s
    Offer _β x z s -> do

        tz <- eat z
        [tx] <- matchType z (Quest __) tz

        type_of <- saveCtx

        (s',l,r) <- bindTrSeqMunch x tx s

        -- need to check that the types of l and r are all bang
        -- (hence saveCtx to k)
        forM_ (l ++ r) $ \ u -> do
            t <- type_of u
            unless (isBang t) (throw (NotBang u t z))

        return (Offer false (id_name x) (length l) s',l ++ [z] ++ r)

    -- let x = demand z in s
    Demand x _ty z s -> do

        tz <- eat z
        [tx] <- matchType z (Bang __) tz

        (s',l,r) <- bindTrSeqMunch x tx s
        return (Demand (id_name x) tz (length l) s',l ++ [z] ++ r)

    -- ignore z in s
    Ignore _β z s -> do

        tz <- eat z
        [_] <- matchType z (Bang __) tz

        (s',b) <- trSeq s
        return (Ignore false 0 s', z : b)

    -- let z' = alias z in s
    Alias _β z z' s -> do

        tz <- typeOf z
        [_] <- matchType z (Bang __) tz

        bind' z' tz
        --          z:!t,l,z':!t,r |- s
        -- --------------------------------------
        --    l,z:!t,r |- let z' = alias z in s

        (s1,b1) <- trSeq s
        -- b1 is now some mess and we should put it on a form z,l,z',r
        (s2,b2) <- putFirst z (s1,b1)
        -- b2 is now on the form l,z',r
        (l,r) <- munch z' b2
        return (Alias false (length l) (id_name z') s2,l ++ [z] ++ r)

    Fold x z s -> do

        tz <- eat z
        [MetaSyntactic _ nm _,tr] <- matchType z (Rec Mu "" __) tz
        (s',l,r) <- bindTrSeqMunch x (foldTy nm tr) s
        return (Fold (id_name x) (length l) s',l ++ [z] ++ r)

    Unfold x z s -> do

        tz <- eat z
        [MetaSyntactic _ nm _,tr] <- matchType z (Rec Nu "" __) tz
        (s',l,r) <- bindTrSeqMunch x (unfoldTy nm tr) s
        return (Unfold (id_name x) (length l) s',l ++ [z] ++ r)

    What m xs ys
        | null xs   -> throw . Hole . M.toList =<< gets st_types
        | otherwise -> do
            _ <- mapM eat xs
            return (What m [0..length xs - 1] ys,xs)

    Save x z s -> do
        tz <- eat z
        [tzi] <- matchType z (Bang __) tz
        tvs <- gets st_iscope
        report $ hang "Save" 4
            $  name x <+> ":" <+> prettyType tvs tzi
            $$ name z <+> ":" <+> prettyType tvs tz
--            $$ "tvs:" <+> sep (punctuate comma (map (text . id_name . fst) tvs))
        insertIVar x tzi $ do
            tvs' <- gets st_iscope
            tzi' <- lookupIVar x
            report $ hang "Save, After ivar insertion" 4
                   $ name x <+> ":" <+> prettyType tvs' tzi'
            (s',b) <- trSeq s
            return (Save (id_name x) 0 s',z:b)

    Load x z s -> do
        t <- lookupIVar z
        bind' x t
        (s',b) <- putFirst x =<< trSeq s
        return (Load (id_name x) 0 s',b)

    Mem{} -> error "Resolve.trSeq: Mem not supported"

bindTrSeqMunch :: Id -> Type -> IdSeq -> T (Seq,[Id],[Id])
bindTrSeqMunch x tx s = do
    bind' x tx
    (s',b) <- trSeq s
    (l,r) <- munch x b
    return (s',l,r)

