{-# LANGUAGE GADTs #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE RecordWildCards #-}
module ILL (module ILL) where

import LL as ILL
import Fresh
import Control.Monad (when)
import Eval

-- | Given produce a term (IdSeq) eliminating a value of the dual type (IdType), whose id is given in parameter (Id)
type TypedTerm = Id -> FreshM (IdSeq,IdType)

type TypedId = (Id,IdType)


tyAppl :: TypedTerm -> IdType -> TypedTerm
tyAppl t tyArg r = do
  x <- freshId
  x' <- freshId
  r' <- freshId
  (t',tTy@(Forall n _ tBody)) <- t x
  tBody' <- tySubst n tyArg tBody
  return (cut1 x x' tTy t' (TApp False dum r' x' tyArg $ Ax dum r r'), 
          tBody')

tyApp = tyAppl

tyLam :: IdType -> (TypedId -> TypedTerm) -> TypedTerm
tyLam ty t x = do
  newX <- freshId 
  tv <- freshId 
  (t',t'ty) <- t (tv,ty) newX
  α <- freshId
  t'tyA <- tySubst tv (var α) t'ty
  return $ (TUnpack tv newX x $ t',Forall α ty t'tyA)

lam :: IdType -> (TypedId -> TypedTerm) -> TypedTerm
lam ty t x = do
  newX <- freshId
  v <- freshId
  (t',t'ty) <- t (v,ty) newX
  return $ (Cross False dum v newX x $ t', ty ⊸ t'ty)

sbotBranch = do
  newX <- freshId
  return $ Branch newX (SBot newX)
  
parallel :: IdType -> [TypedId] -> 
            ([TypedId] -> TypedTerm) -> TypedTerm
parallel size ids t x = do
  newX <- freshId
  let ids' = [(v,ty) | (v,Array Positive _ ty) <- ids ]
  (t',t'ty) <- t ids' newX
  sbot <- sbotBranch
  return (BigPar size x (Branch newX t') sbot, Array Positive size t'ty)

pair :: TypedTerm -> TypedTerm -> TypedTerm
pair x y r = do
  a <- freshId
  b <- freshId            
  (x',xTy) <- x a
  (y',yTy) <- y b
  return (Par False dum a b r x' y',xTy :*: yTy)

load :: TypedId -> TypedTerm
load (v, vTy) = \ x -> do
  x' <- freshId
  return $ (Load x' v $ Ax dum x x',vTy)

ref :: TypedId -> TypedTerm
ref (x', x'Ty) = \ x -> return $ (Ax dum x x',x'Ty)

appHelp :: IdType -> TypedTerm -> Id -> Id -> FreshM (IdSeq,IdType)
appHelp xTy fTerm x y = do
  f <- freshId
  f' <- freshId
  x' <- freshId
  y' <- freshId
  (fTerm',fTy@(aTy' :|: bTy)) <- fTerm f
  when (xTy /= neg aTy') $ error $ "Incompatible types : \n" ++ show xTy ++ "\n" ++ show (neg aTy') ++ "\n"
  return $ (cut1 f f' fTy fTerm' $ Par False dum x' y' f' (Ax dum x x') (Ax dum y y'),bTy)

o :: TypedTerm -> TypedTerm -> TypedTerm
o f g r = do
  x_ <- freshId
  (_,xTy_neg :|: _) <- g x_
  (lam (neg xTy_neg) $ \ x -> f `app` (g `app` ref x)) r
  
app :: TypedTerm -> TypedTerm -> TypedTerm
app f a y = do
  x <- freshId
  x' <- freshId
  (a',aTy) <- a x
  (b',bTy) <- appHelp aTy f x' y
  return (cut1 x x' aTy a' b',bTy)

tref :: TypedId -> IdType
tref (v,_) = var v 

what' :: TypedTerm
what' x = return (What "???" [] [],meta "????" )

mkDeriv' :: TypedTerm -> FreshM IdDeriv           
mkDeriv' t = do 
  v <- freshId
  (t',t'ty) <- t v
  return $ Deriv "mkDeriv" [] [(v,dual t'ty :^ SizeOne)] t'

mkDeriv t = runFreshM (mkDeriv' t)


-- ⊗(i+j)nA  ⊸  (⊗inA  ⊗  ⊗jnA)
split :: TypedTerm -> TypedTerm
split t x = do
  a <- freshId
  a' <- freshId
  a1 <- freshId
  a2 <- freshId
  a1' <- freshId
  a2' <- freshId
  (t',t'ty@(Array Positive (sizeFactors -> (SizeAdd i j,n)) tyA)) <- t a
  return
    (cut1 a a' t'ty t'
     $ Split i a1' a2' a'
     $ Par False dum a1 a2 x (Ax dum a1 a1') (Ax dum a2 a2'), 
     arrayOp Positive (SizeMul i n) tyA ⊗ arrayOp Positive (SizeMul j n) tyA)

    {-
   ----------------------- ax  ------------------- ax
     a1:⅋inA, a1':⊗namA ⊢      a2:⅋jnA, a2':⊗nbmA ⊢
   -------------------------------------------------- ⅋
    x:⅋inA  ⅋  ⅋jnA, a1':⊗namA, a2':⊗nbmA ⊢
   --------------------------------------- ⊗+
       x:⅋inA  ⅋  ⅋jnA,a':⊗n(a+b)mA ⊢                       Γ, a:⅋n(a+b)m~A ⊢ t
   ------------------------------------------------------------------------------ cut
                          Γ, x: ⅋inA  ⅋  ⅋jnA
-}

splitPair :: TypedTerm -> (TypedId -> TypedId -> TypedTerm) -> TypedTerm
splitPair pair f r = do
  x <- freshId
  y <- freshId
  z' <- freshId
  z  <- freshId
  (pair',p'Ty@(xTy :*: yTy)) <- pair z'
  (f',fTy) <- f (x,xTy) (y,yTy) r
  return (cut1 z' z p'Ty pair' $
          Cross False dum x y z $
          f'
         ,fTy)

{-
-- append :: ⊗nA -> ⊗mA -> ⊗(n+m)A; this is the moral type of append.
append :: TypedTerm -> TypedTerm -> TypedTerm
append t u x = do {-
   -- x is the name of the variable containing the dual of the result, and we need to eliminate that.
   -- to do this we can use terms that eliminate the input types, given some temp. variable that we can choose.
   here is the shape of the derivation tree we wish to construct:

    Γ,x1:⅋n~A ⊢    Δ,x2:⅋m~A ⊢
   --------------------------------
     Γ, Δ, x:⅋(n+m)~A ⊢
-}
   x1 <- freshId
   x2 <- freshId
   (t',(Array Positive n tyA)) <- t x1
   (u',u'ty@(Array Positive m tyB)) <- u x2
   when (tyA /= tyB) $ error "append: non-matching types"
   return (ParAdd x1 x2 x t' u',
           arrayOp Positive (SizeAdd n m) tyA)
-}

-- appendPush :: ⅋nA -> ⅋mA -> ⅋(n+m)A
appendPush t u x = do
   x1 <- freshId
   x2 <- freshId
   (t',(Array Negative n tyA)) <- t x1
   (u',u'ty@(Array Negative m tyB)) <- u x2
   when (tyA /= tyB) $ error "appendPush: non-matching types"
   return (Split n x1 x2 x $ mix t' u',
           arrayOp Positive (SizeAdd n m) tyA)

{-


  
     Γ, x1:⊗n~A ⊢  t x1     Δ, x2:⊗m~A ⊢ u x2
  ----------------------------------------------- mix
     Γ, Δ, x1:⊗n~A, x2:⊗m~A ⊢
  ---------------------------------⊗+n
      Γ, Δ, x:⊗(n+m)~A ⊢

 -}

unhalve' t = \r -> do
  x  <- freshId
  x' <- freshId
  x2 <- freshId
  y  <- freshId
  y' <- freshId
  z  <- freshId
  z' <- freshId
  (t', ty@(Array Negative n tensor@(tyA :|: tyA'))) <- t x'
  when (tyA /= tyA') $ error "unhalve: type mismatch"
  sbot <- sbotBranch
  return (cut1 x' x ty t' $
          Split n y z r $
          BigPar n x
            (Branch x2 $ Par False dum y' z' x2 (Ax dum y y') (Ax dum z z'))
            sbot
         ,Array Negative (SizeMul (sizeLit 2) n) tyA)

tile :: TypedId -> TypedTerm
tile (n,_) = \r -> do
  fmMapTrg <- freshId
  fmMixL   <- freshId
  fmMixR   <- freshId
  fmMixTrg <- freshId
  fmMon    <- freshId
  return (Foldmap { fmSz = n
                  , fmTyM = Bot
                  , fmArr = [r]
                  , fmMap = Ax dum r fmMapTrg
                  , fmMix = mix (SBot fmMixL) (Ax dum fmMixR fmMixTrg)
                  , fmCont = SBot fmMon
                  , ..
                  }
          ,Array Negative (SizeExp (sizeVar n)) One)

autoSchedule :: IdType -> TypedId -> TypedTerm
autoSchedule tA sz@(n,_) r = do
  [x,x',t,t',t1] <- freshIds 5
  (tile',tTile) <- tile sz t'
  sb <- sbotBranch
  return (cut1 t' t tTile tile' $
          Cross False dum x x' r $
          BigPar (sizeVar n) t
          (Branch t1 $ SOne False t1 $
           Ax dum x x') sb,
          bigTensor expSz tA ⊸ bigPar expSz tA
         )
   where expSz = SizeExp $ sizeVar n
-------------------------------------------
{-
test :: TypedTerm
test = tyLam index $ \n ->
       tyLam Type $ \a ->
       lam (Array Negative (tref n) (tref a :*: tref a)) $ \xs ->
       unhalve (ref xs)
-}

-- ⅋nA -> ⊗nA
freeze :: TypedTerm -> TypedTerm
freeze t r = do
  x  <- freshId
  x' <- freshId
  r' <- freshId
  (t',Array Negative n tyA) <- t x
  sb <- sbotBranch
  return (Freeze n tyA x x' t' $
          BigPar n r
          (Branch r' $ Ax dum r' x') sb
         ,bigTensor n tyA)


-- ⊗nA ⊸ ~M ⊸ (A ⊸ ~M) ⊸ (~M  ⊸ ~M ⊸ ~M) ⊸ ~M
foldMap :: TypedTerm -> TypedTerm -> (TypedId -> TypedTerm) ->
           (TypedId -> TypedId -> TypedTerm) -> TypedTerm
foldMap arr m f mixer r = do
  fmArr' <- freshId
  fmMapTrg <- freshId
  fmMixL <- freshId
  fmMixR <- freshId
  fmMixTrg <- freshId
  fmMon <- freshId
  theA <- freshId
  theSz <- freshId -- Not sure what to do with this one
  (arr',Array Positive n tyA) <- arr fmArr'
  (fmM,fmTyM) <- m fmMon
  (fmMap, fmTyM')  <- f (theA,tyA) fmMapTrg
  (fmMix, tyM') <- mixer (fmMixL,fmTyM) (fmMixR,fmTyM) fmMapTrg
  when (fmTyM /= tyM') $ error $ "Incompatible types : \n" ++ show fmTyM ++ "\n" ++ show tyM' ++ "\n"

  return (
    Foldmap {
       fmSz = theSz,
       fmArr = [fmArr'],
       fmCont = Ax dum r fmMon,
       ..
       }
     ,fmTyM)
