module Util where

includedIn :: Eq a => [a] -> [a] -> Bool
xs `includedIn` ys = all (`elem` ys) xs

