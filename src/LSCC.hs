{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
-- | Limestone C compiler from LL to several backends
module LSCC where

import Development.Placeholders

import Binders
import TypeCheck
import Link
import Ritchie
import Lint
import LL
import Fresh
import Control.Monad.Trans
import Control.Monad.Reader
import Control.Monad.Except.Extra
import System.Console.CmdLib
import Text.PrettyPrint
import Text.Show.Pretty
import Control.Monad
import Control.Applicative
import Control.Lens
import Fuel
import Data.Default
import Data.List (find, intercalate)
import Eval
import Control.Arrow.Pretty
import Data.Traversable as T
import Polarize
import Ritchie.GCC(gcc,GCCOpts(..))
import Control.Monad.Writer
--type DerivProc = Deriv'' (FreshM (Seqn0 Quick.Id Quick.Id)) Quick.Id Quick.Id

compilerMain :: [Deriv0 Id Id] -> IO ()
compilerMain [] = fail "No derivations available for compiling."
compilerMain ss = getArgs >>=
                  executeR (def :: Params) >>= \params ->
                    runCompilerM params $ compilerPipeline ss

findDeriv :: (Wrapped d, Unwrapped d ~ Deriv'' seq name ref) => [d] -> CompilerM [d]
findDeriv ss = do
  names <- asks progDerivNames
  T.forM names $ \name ->
    case find ((== name) . view (_Wrapped' . derivName)) ss of
      Just s  -> return s
      Nothing -> fail $ "Derivation " ++ name ++ " not found. Try one of " ++ derivList ++ "."
  where
    derivList = intercalate ", " [d ^. _Wrapped' . derivName  | d <- ss ]

analyseDeriv :: Deriv0 Id Id -> Deriv2 Id Id
analyseDeriv =
  (checkCtxDeriv &&& id)
  >>> (\(res, d) -> either (\msg -> error (msg {- ++ "\n" ++ ppShow d -})) id res)
  >>> (typeCheckDeriv &&& id) >>> (\((log,v), orig) -> case log of
                                      [] -> v
                                      _  -> error $ intercalate "\n" (log)) -- ++ [ppShow orig]))


fileName :: String -> CompilerM String 
fileName ext = (++ ext) <$> asks outputBaseName

cFile = fileName ".c"
hFile = fileName ".h"
execFile = fileName ""

c :: LiftCompile m a b => (c -> m a) -> Kleisli CompilerM c b
c  f = Kleisli (\x -> liftCompile $ f x)

c' f a = c $ f a 
c_ m = c $ \_ -> m

compilerPipeline :: [Deriv0 Id Id] -> CompilerM ()
compilerPipeline = runKleisli$
  arr id
    ├╼ (c compilerPipelineDocs
        ├╼ (arr mkCFile ── c' writeDoc cFile) 
        ╰─ (arr mkHFile ── c' writeDoc hFile))
    ╰─ c_ externalCompiler

compilerPipelineDocs :: [Deriv0 Id Id] -> CompilerM [(Doc,[TopLevel])]
compilerPipelineDocs = runKleisli pipeline
  where
    pipeline =
      c findDeriv &&& arr id
      ╰─ c (uncurry topoSortDeriv) ─╼  c' writeIR ".s0"

      -- Typecheck all the original programs
      ╰─ forEach 
            (c makeBindersSafe ─╼ c' writeDerivIR ".s1"
              ╰─ c checkCtxDeriv ─╼ c' writeDerivIR ".s2"
                 ╰─ c typeCheckStrict ─╼ c' writeDerivIR ".s3") 
      
      -- Inline derivations, select desired program
      ╰─ c linkDeriv ── c findDeriv ─╼ c' writeIR ".s4"
         ╰─ forEach (
            -- Erase types before cut elimination
            -- Ideally, we would make sure the cut-elimination process
            -- preserves the type info at every step).
               arr ((erase :: IdDeriv1 -> IdDeriv0) . (erase :: IdDeriv2 -> IdDeriv1))
               ╰─ c evaluateFDeriv   ─╼  c' writeDerivIR ".s5"
                  ╰─ c checkCtxDeriv ─╼  c' writeDerivIR ".s6"
                     -- Also, add polarities and propagate them through type-checking
                     ╰─ k paramPolD1 ─╼  c' writeDerivIR ".s7"
                        ╰─ c typeCheckStrict  ─╼  c' writeDerivIR ".s8"
                           -- Solve polarities and generate code
                           ╰─ k polarizeDeriv ─╼  c' writeDerivIR ".s9"
                              ╰─  c genCodeC ) 

externalCompiler :: CompilerM ()
externalCompiler = do
  Params{..} <- ask
  if noGCC then return ()
           else liftCompile gcc

instance RecordCommand Params where
  mode_summary _ = "Limestone CLL compiler"
  run' _ = fail "No preferred implementation is available."


instance Attributes Params where
  attributes _ =
    group "Options" 
    [ outputBaseName %> [ Short ['o']
                       , Long ["output"]
                       , Help "Base name for the output file(s)."
                       , ArgHelp "FILE"
                       , Default (outputBaseName def)
                       ]
    , evalFuel %> [ Short ['f']
                 , Long ["fuel"]
                 , Help "Maximum evaluation steps before compiling."
                 , ArgHelp "NUMBER"
                 , Default (evalFuel def)
                 ]

    , progDerivNames %> [ Help "Built-in program(s) to compile."
                        , ArgHelp "PROG"
                        , Extra True
                        , Required True
                        ]

    , debugBindings %> [ 
                   Long ["debug-bindings"]
                 , Help "Output variables as comments"
                 , ArgHelp "BOOL"
                 , Default (debugBindings def)
                 ]

    , debugAST %> [ 
                    Long ["debug-ast"]
                  , Help "Output AST as comments"
                  , ArgHelp "FLAG"
                  , Default (debugAST def)
                  ]

    , debugAll %> [   Short ['d']
                 , Long ["debug-all"]
                 , Help "Enable all debug flags"
                 , ArgHelp "FLAG"
                 , Default (debugBindings def)
                 ]
    , noIR %> [ 
                   Long ["no-ir"]
                 , Help "Disable IR file creation"
                 , ArgHelp "FLAG"
                 , Default (noIR def)
                 ]
    , noGCC %> [
                   Long ["no-gcc"]
                 , Help "Do not launch GCC compiler" 
                 , ArgHelp "FLAG"
                 , Default (noGCC def)
                 ]
    ]

data Params = Params {
  outputBaseName :: FilePath
 ,evalFuel       :: Int
 ,progDerivNames :: [String]
 ,debugAST       :: Bool
 ,debugBindings  :: Bool
 ,debugAll       :: Bool
 ,noIR           :: Bool
 ,noGCC          :: Bool
} deriving (Data, Typeable, Show, Eq)               

instance Default Params where
  def = Params {
    outputBaseName = "a.out"
   ,evalFuel = 0
   ,progDerivNames = []
   ,debugAST = False
   ,debugBindings = False
   ,debugAll = False
   ,noIR = False
   ,noGCC = False
  }

type CompilerM = FreshT (ReaderT Params (ExceptT String IO))
runCompilerM :: Params -> CompilerM a -> IO a
runCompilerM opts = 
  runExceptT . flip runReaderT opts . runFreshT >=>
  either die return

-- | Writes an intermediate representation
writeIR :: (Show a) => String -> a -> CompilerM ()
writeIR ext stage = do
  s <- asks noIR 
  if s then return () else writeDoc (fileName ext) (ppDoc stage)

writeDerivIR :: (Wrapped d, Unwrapped d ~ Deriv'' seq name ref, Show d) => String -> d -> CompilerM ()
writeDerivIR ext d = do
  s <- asks noIR 
  if s then return () else do
    let dname = d ^. _Wrapped' . derivName 
    writeDoc (fileName$ "." ++ dname ++ ext) (ppDoc d)

-- | Writes a prettyprinted document to a file
writeDoc :: CompilerM String -> Doc -> CompilerM ()
writeDoc fnameM stage = do
  let text = render stage
  fname <- fnameM
  liftIO $ writeFile fname text

class (Applicative m, Monad m) => LiftCompile m a b | a -> b where
  liftCompile :: m a -> CompilerM b

instance LiftCompile CompilerM a a where
  liftCompile = id

instance LiftCompile Identity a a where
  liftCompile = return . runIdentity

instance LiftCompile (Either String) a a where
  liftCompile (Left e)  = fail e
  liftCompile (Right a) = return a
  
instance LiftCompile m a b => LiftCompile (FuelT m) a b where
  liftCompile m =
    do
      fuel <- asks evalFuel
      liftCompile $ runFuelT fuel m

instance LiftCompile m (Either String a) (Either String b) => LiftCompile (ExceptT String m) a b where
  liftCompile m = do
    let m' = runExceptT m
    res <- liftCompile m'
    case res of
      Left err -> liftIO $ die err
      Right a  -> return a 

instance LiftCompile m (Either String a) (Either String b) => LiftCompile (ExceptT [TypeError] m) a b where
  liftCompile = liftCompile . withExceptT unlines

instance LiftCompile FreshM a a where
  liftCompile :: FreshM a -> CompilerM a
  liftCompile = mapFreshT (return . runIdentity)
  
  
instance LiftCompile m a b => LiftCompile (ReaderT (TypeCheckEnv name ref) m) a b where
  liftCompile m = do
    let config = def { _codeGen = True }
    liftCompile $ runReaderT m config

instance LiftCompile m a b => LiftCompile (ReaderT EvalEnv m) a b where
  liftCompile m = do
    let env = def {
          _evalCost = \ci0 -> case ci0 of
             Desugar     -> Just 1
             Fuse        -> Just 1
             NoCommute   -> Nothing
             Polymorphic -> Just 0
             Manual      -> Just 1
             }
    liftCompile $ runReaderT m env

instance LiftCompile m a b => LiftCompile (ReaderT CompileEnv m) a b where
  liftCompile m = do
    Params{..} <- ask
    let env = def {
          _envFlags = (if (debugAll || debugAST)      then [DebugAST]      else [])
                   ++ (if (debugAll || debugBindings) then [DebugBindings] else [])
         }
    liftCompile $ runReaderT m env

instance LiftCompile m a b => LiftCompile (ReaderT GCCOpts m) a b where
  liftCompile m = do
    Params{..} <- ask
    execName <- execFile
    cFile <- cFile
    let outputAssembly = not noIR
    liftCompile $ runReaderT m def{execName,inputFiles=[cFile],outputAssembly} 

instance LiftCompile IO a a where
  liftCompile m = liftIO m

instance LiftCompile m (a,[TopLevel]) b => LiftCompile (WriterT [TopLevel] m) a b where
  liftCompile m = liftCompile$ runWriterT m
     
    
