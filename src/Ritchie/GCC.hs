{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}
module Ritchie.GCC where

import System.Process
import System.Exit
import Control.Monad.Except.Extra
import Control.Monad.Trans
import Control.Monad.Reader
import Data.Default

data GCCOpts = GCCOpts {
  execName   :: String
 ,optLevel   :: String
 ,inputFiles :: [String]
 ,outputAssembly :: Bool
 }


instance Default GCCOpts where
  def = GCCOpts{
    optLevel = "2"
   ,execName = "a.out"
   ,inputFiles = []
   ,outputAssembly = False
   }

gcc :: ReaderT GCCOpts (ExceptT String IO) ()
gcc
  = do
      GCCOpts{..} <- ask
      let cmd = "gcc"
      let cmdargs = ["-c"
                    ,"-O" ++ optLevel
                    ,"-Wall"
                    ,"-Wno-unused-but-set-variable"
                    ,"-std=gnu11"
                    ] ++ inputFiles
                      ++ if outputAssembly then ["-S"] else []
      err <- liftIO$ rawSystem cmd cmdargs
      case err of
        ExitSuccess   -> return ()
        ExitFailure i -> throwError$ "Call " ++ show cmd ++ " " ++ show cmdargs ++ " exited with error code " ++ show i 
  

