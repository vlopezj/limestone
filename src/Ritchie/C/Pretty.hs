{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE ViewPatterns #-}
module Ritchie.C.Pretty
       (Code, CExpr, CType, Tag, Fun, CIdent, LValue
       ,Expr, CLValue, CIndex, Length, LengthLValue
       ,ELValue, Mem(Mem), EA(..)
       ,MonadLength(..)
       ,divLength
       ,cident,litIdent
       ,castExpr
       ,(+.), (-.), (*.), (/.), (%.), neg
       ,(≤.), (<.), (<=.)
       ,(≥.), (>.), (>=.)
       ,(==.), (!=.)
       ,(&&.), (||.)
       ,preIncr,ePreIncr
       ,Cond, CondLValue
       ,(≤:), (<:), (<=:)
       ,(≥:), (>:), (>=:)
       ,(==:), (!=:)
       ,(&&:), (||:), (!:)
       ,biop
       ,tInt, tDouble, tBool, tVoid, tPrim, tChar, tSizeT
       ,tStruct, tUnion
       ,tPointer, tFunPtr
       ,tConst
       ,tArray
       ,dFunction, dFunDecl, sReturn, sReturnA
       ,sNop, sIfElse, sComment, sCommentStr, sWhile, sWhileA, sBreak, sBreakA
       ,assertLengthEqA
       ,statements, stm, stmExpr
       ,eBool, eTrue, eFalse
       ,sUnreachable
       ,(=.),(=:),(+=:)
       ,sIfElseA, sIfElseM, cElseM, solveCond, condIsChoice
       ,statementsA, stmA, stmExprA
       ,sNopA
       ,cElse
       ,sForA,sForADecl
       ,rangeA
       ,declare
       ,call, cCall
       ,cLit
       ,lit, slit, ilit, eLen, cLen
       ,IsExpr, fromExpr, toExpr
       ,BiOp,UnOp
       ,cexpr
       ,amp, star
       ,eLit
       ,eNULL
       ,eSubs
       ,dot
       ,freshCIdent
       ,cAssert
       ,cWithLocal,cWithLocals,declareLocal,cWithLocalMaybe
       ,lenCmpOp
       ,eStr
       ,cFree,cFreeA,cMalloc,cSizeOf,cCast
       ,asScalar,isScalar
       ,anonTy
       ,asDoc
       ,LengthPredicates, lengthPredAdd
       ,scafoldH,scafoldC,vcatBlank
       ,pragmaOmp,pragmaWarning
       ,rangeAPragma
       ,typedef
       ) where
                 

import Text.PrettyPrint as PP hiding ((<>), parens, brackets)
import Data.String
import Data.Maybe
import Data.Monoid
import Fresh
import Id
import Control.Applicative
import Data.Functor.Identity
import Data.Traversable as T
import Control.Lens hiding (assign, (<.))
import Data.Function
import qualified Pol
import Pol(Pol, PolSyntax(..),PolNat(..))
import qualified Prop
import Prop(Prop, PropSyntax(..))
import Op
import Data.Generics.Is
import Development.Placeholders
import Ritchie.Euclidean
import Control.Monad.Reader
import Control.Monad.Writer
import qualified Pol.NIA
import ContM

data EA f where EA :: f a -> EA f

-- | Type with Eq and Ord instances adapted for C code.
data Code = Code Doc 
          | CodeEmpty
          deriving (Show)

instance IsString Code where
  fromString = Code . fromString

instance Monoid Code where
  mempty  = CodeEmpty
  CodeEmpty `mappend` x = x
  x `mappend` CodeEmpty = x
  Code x `mappend` Code y = Code$ x `mappend` y

asDoc :: Code -> Doc
asDoc CodeEmpty = mempty
asDoc (Code d) = d

-- ^ Renders a fragment of Code for comparison
normalizedRender :: Code -> String
normalizedRender = filter (not . flip elem ['\n',' ','\t']) . render . asDoc

instance Eq (Code) where
  (==) = (==) `on` normalizedRender

instance Ord (Code) where
  compare = compare `on` normalizedRender

type Fun = Code  
type CIdent = Code
type CExpr a = Code
type LName = Code
type CLValue a = CExpr a
type Tag = CIdent

data Assoc = LTR | RTL | NoAssoc deriving (Show, Ord, Eq)

-- Unary and binary operators have different precedences in the spec,
-- there's no need to tell them appart.
data Expr a = Expr {_prec :: Int,
                    _assoc :: Assoc,
                    _expr :: Code}
              deriving (Show, Ord, Eq)


$(makeLenses ''Expr)

instance Show (Code -> Expr ()) where
  show f = show $ f "/* CODE */"

-- | CType needs a hole for the variable
data CType = CType {
  _qualifyType :: [Code]
 ,_baseType :: Code
 ,_modType  :: Expr () -> Expr ()
 ,_modTypeAfter :: Expr () -> Expr ()
} 


$(makeLenses ''CType)

instance Show CType where
  showsPrec p cty = showsPrec p $ anonTy cty


-- On top of the expressions, we define sublanguages with simplification
-- rules for certain operators.
-- This makes for prettier code, at the cost of some marshalling.
data Mem a = Star (Mem a)
             | Amp  (Mem a)
             | Mem (Expr a)
            deriving (Show, Ord, Eq)

newtype Length = Length { getLengthPol :: Pol Integer (Expr Integer) } deriving (Num, Show, Eq)
newtype Cond   = Cond   { getCond      :: Prop (Expr Bool)       } deriving (Show, Eq)
type CIndex  = Length

type LengthLValue = ELValue Integer
type LengthPredicates = Pol.PolPredicates (Expr Integer)

type CondLValue = ELValue Bool

class (Applicative m, Monad m, MonadReader LengthPredicates (MLP m)) => MonadLength m where
  type MLP m :: * -> *
  monadLengthWrap :: forall a. Simple Iso (MLP m a) (m a)

instance (MonadLength m) => Euclidean m Length where
  divM :: Length -> Length -> m (Length, Length)
  a `divM` b =  
    case getLengthPol a `Pol.quotRemIntegral` getLengthPol b of
      (q, 0) -> return (Length q, 0)
      (q, r) -> return (Length $ q + (Pol.var $ toExpr (Length r) /. toExpr b),
                        Length $     (Pol.var $ toExpr (Length r) %. toExpr b))

lengthPredProve :: (MonadLength m) => (Length,CmpOp,Length) -> ContM m (Maybe Bool)
lengthPredProve (getLengthPol -> m, op, getLengthPol -> n) κ =
  view monadLengthWrap$ Pol.NIA.polPredProve (m,op,n) $ \res -> view (from monadLengthWrap)$ κ res

a `divLength` b = a // b

isScalar = Pol.isScalar . getLengthPol 
asScalar = Pol.asScalar . getLengthPol 

-- Composing methods from this class is discouraged as it makes type-checking
-- difficult. Use of non-polymorphic synonyms is advised.
class IsExpr e a | e -> a where
  toExpr   :: e -> Expr a
  fromExpr :: Expr a -> e 

instance IsExpr (Expr a) a where
  toExpr = id
  fromExpr = id

instance IsExpr (Mem a) a where
  fromExpr = Mem
  toExpr :: Mem a -> Expr a
  toExpr t = case t of
    Mem e -> e
    Star e -> cStar (toExpr e)
    Amp e -> cAmp (toExpr e)

instance IsExpr Length Integer where
  fromExpr = Length . Pol.var
  toExpr :: Length -> Expr Integer
  toExpr = go . Pol.syntax . getLengthPol
         where
           go (PAdd a b) = go a +. go b
           go (PSub a b) = go a -. go b
           go (PNeg a)  = neg$ go a
           go (PProd a b) = go a *. go b
           go (PPower a (PolNat 0)) = "1"
           go (PPower a (PolNat k)) | k > 0 = foldl1 (*.) $ take (fromIntegral k) $ repeat (go a)
           go (PParens a) = go a
           go (PScalar i) = ilit i
           go (PVariable cexpr) = cexpr

instance IsExpr Cond Bool where
  fromExpr = Cond . Prop.lit
  toExpr = go . Prop.syntax . getCond
           where
             go s = case s of
                STrue    -> "true"
                SFalse   -> "false"
                SAnd a b -> go a &&. go b
                SOr  a b -> go a ||. go b
                SLit a   -> a
                SNeg a   -> (!.) (go a)
              
castExpr :: (IsExpr e a, IsExpr e' b) => e -> e'
castExpr (toExpr -> Expr{..}) = fromExpr$ Expr{..}

cexpr :: forall e a. IsExpr e a => e -> CExpr a
cexpr = view expr . toExpr

cCast :: CType -> Expr a -> Expr b
cCast cty arg = let e = Expr {
  _prec = 3
 ,_assoc = RTL
 ,_expr = parens (anonTy cty) <> maybeParens e arg
 } in e

cond :: Expr Bool -> Cond
cond = fromExpr

-- | Will interact badly with comma operator.
call :: Expr a -> [EA Expr] -> Expr b
call f args = let e = Expr {
  _prec = 2
 ,_assoc = LTR
 ,_expr = maybeParens e f `cCall` [cexpr arg | EA arg <- args]
 } in e
  
-- | LValues hold an lvalue referencing the memory itself. In particular, this
--   means that, for indexing into an array, `amp` should be used to obtain
--   the address, before applying `subs`.
type LValue = Expr
type ELValue = Mem

fromLValue :: IsExpr e a => LValue a -> e 
fromLValue = fromExpr

toLValue :: IsExpr e a => e -> LValue a 
toLValue = toExpr

star :: Mem a -> Mem a
star (Amp e) = e
star e = Star e

amp :: Mem a -> Mem a
amp (Star e) = e
amp e = Amp e

eLift :: (LValue a -> LValue b) -> (Mem a -> Mem b)
eLift f = Mem . f . toLValue

dot (Star (toExpr -> e)) field = Mem $ e `cArrow` field
dot (toExpr -> e)    field = Mem $ e `cDot` field

eSubs (toExpr -> e) i = Mem $ e `subs` i

eLit = Mem . lit

eLen :: (IsExpr e Integer) => e -> Length
eLen = fromExpr . toExpr

eNULL :: Mem a
eNULL = Mem$ lit "NULL"


assocLeft :: Expr a -> Bool 
assocLeft Expr{_assoc = LTR} = True
assocLeft Expr{_assoc = _}   = False
            
assocRight :: Expr a -> Bool 
assocRight Expr{_assoc = RTL} = True
assocRight Expr{_assoc = _}   = False

clvalue :: LValue a -> CLValue a
clvalue = cexpr

parens :: (Monoid s, IsString s) => s -> s
parens x = "(" <> x <> ")"

brackets :: (Monoid s, IsString s) => s -> s
brackets x = "[" <> x <> "]"


maybeParens :: Expr a -- ^ Outer expression
            -> Expr b -- ^ Inner expression
            -> Code
maybeParens eOut eIn =
  ($ eIn ^. expr) $ case (eIn ^. prec) `compare` (eOut ^. prec) of
    LT -- The inner operator binds tighter
      -> id
    GT -- The outer operator binds tighter
      -> parens
    EQ ->
      case (eOut ^. assoc, eIn ^. assoc) of
        (LTR, LTR) -> id
        (RTL, RTL) -> id
        _          -> parens

lValueToCode :: LValue a -> CExpr a
lValueToCode = cexpr

lit :: Code -> Expr a
lit = Expr (-1) NoAssoc 

cLen = eLen . lit

slit :: String -> Expr a
slit = Expr (-1) NoAssoc . fromString

instance IsString (Expr a) where
  fromString = lit . fromString

ilit :: Integer -> Expr a
ilit = lit . fromString . show

neg :: UnOp a b
neg = unop 6 RTL "-"

(!.) :: UnOp a b
(!.) = unop 3 RTL "!" 

type UnOp a b = Expr a -> Expr b
type BiOp a b c = Expr a -> Expr b -> Expr c

unop :: Int -- ^ Precedence: 1 is highest
     -> Assoc -> String -> UnOp a b
unop prec assoc op a =
  let e = Expr prec assoc (fromString op <> maybeParens e a) in e

biop :: Int -> Assoc -> String -> BiOp a b c
biop prec assoc op a b =
  let e = Expr prec assoc (Code (asDoc (maybeParens e a) <+>
                                  fromString op <+>
                                  asDoc (maybeParens e b))) in e

[ (*.), (/.), (%.)] = biop 5 LTR <$> 
 ["*" , "/" , "%" ]

[ (+.) , (-.)] = biop 6 LTR <$> 
 ["+"  , "-" ]

[ (<=.), (<.), (>.), (>=.), (==.), (!=.)] = cCmpOp <$> [Le, Lt, Gt, Ge, Eq, Ne]
(≤.) = (<=.)
(≥.) = (>=.)

(&&.), (||.) :: BiOp a b c
(&&.) = biop 13 LTR  "&&"
(||.) = biop 14 RTL  "||"

(=.) :: BiOp a b a
(=.) = assign

(+=.) :: BiOp a b a
(+=.) = assignPlus

infixr 0 `assign`, =., =:, +=.
infixl 2 ||., ||:
infixl 3 &&., &&: 
infixl 4 <=., ≤., <., <=:, ≤:, <:
infixl 6 +., -.
infixl 7 *., /., %.
infixl 8 `subs`, `eSubs`

cident :: Id -> CIdent
cident ident = litIdent $ idToString ident

litIdent :: String -> CIdent
litIdent = fromString

statements :: [Code] -> Code
statements (filter $(isNot 'CodeEmpty) -> cs) = case cs of
                     [] -> CodeEmpty
                     _  -> Code . vcat . map asDoc $ cs

statementsA :: (Applicative m) => [m Code] -> m Code  
statementsA = fmap statements . sequenceA

hstatements :: [Code] -> Code
hstatements = Code . hsep . map asDoc

hstatementsM :: (Applicative m) => [m Code] -> m Code  
hstatementsM = fmap statements . sequenceA

blockIndent = 4

indent :: Code -> Code
indent = Code . nest blockIndent . asDoc

stm :: Code -> Code
stm c = c <> Code semi

stmExpr :: (IsExpr e a) => e -> Code
stmExpr = stm . cexpr . toExpr

stmExprA :: (Applicative m, IsExpr e a) => e -> m Code
stmExprA = pure . stmExpr

stmA :: Applicative m => Code -> m Code
stmA = pure . stm

block :: Code -> Code
block c = Code$ "{" $+$ asDoc (indent c) $+$ "}"

blockA :: (Functor m) => m Code -> m Code
blockA code = block <$> code 

blockP :: Code -> Code -> Code
blockP pg c = Code$ asDoc pg <+> "{" $+$ asDoc (indent c) $+$ "}"

blockPA :: (Functor m) => Code -> m Code -> m Code
blockPA pg c = blockP pg <$> c 

  
arglist :: [Code] -> Code
arglist = Code . parens . hsep . punctuate "," . map asDoc

prim :: Code -> CType
prim a = CType { _qualifyType = []
               , _baseType = a
               , _modType  = id
               , _modTypeAfter = id
               }

tInt, tDouble, tVoid, tChar :: CType
tBool, tSizeT :: CType

tInt = prim "int"  
tDouble = prim "double"
tVoid = prim "void"
tChar = prim "char"

tSizeT = prim "size_t"
tBool  = prim "bool"

includesBoth = [
  "#include <stddef.h>"
 ,"#include <stdbool.h>"
 ,"#include <complex.h>"
 ]

includesHeader = includesBoth ++ []
includesCode = includesBoth ++ [
  "#include <stdlib.h>",
  "#include <stdio.h>",
  "#include <math.h>",
  "#include <ctype.h>"
  ]
  
scafoldH doc = vcatBlank [vcat includesHeader, doc]
scafoldC doc = vcatBlank [vcat includesCode,   doc]

vcatBlank :: [Doc] -> Doc
vcatBlank [] = mempty
vcatBlank [a] = a
vcatBlank (a:as) = a $+$ text "" $+$ vcatBlank as

type SUType = Code

tQualify :: Code -> CType -> CType
tQualify q = qualifyType %~ (q:)

tConst = tQualify "const"
tRestrict = tQualify "restrict"

tStructUnion :: SUType -> [(Tag, CType)] -> CType
tStructUnion sut fields = prim $ blockP sut (statements [declareStm ty [tag] | (tag,ty) <- fields])

tStruct = tStructUnion "struct"
tUnion  = tStructUnion "union"

anonTy :: CType -> Code
anonTy CType{..} = _baseType <> cexpr (_modTypeAfter $ _modType (lit mempty))

declare :: CType -> [CIdent] -> Code
declare CType{..} idents = Code$ asDoc _baseType <+> (hsep $ punctuate comma $ map (asDoc . cexpr . _modTypeAfter . _modType . lit) idents)

declareStm :: CType -> [CIdent] -> Code
declareStm ty idents = stm $ declare ty idents

declareInitStm :: CType -> CIdent -> Expr a -> Code
declareInitStm ty ident initial = stm $ Code$ asDoc (declare ty [ident]) <+> "=" <+> asDoc (cexpr initial)

typedef :: CType -> CIdent -> Code
typedef ty ident = stm$ Code$ "typedef" <+> asDoc (declare ty [ident])

tPointer :: CType -> CType
tPointer = modType %~ \modType' ident -> cStar (modType' ident)

tPointerR :: CType -> CType
tPointerR = modType %~ \modType' ident -> cStarR (modType' ident)

tArray :: Length -> CType -> CType
tArray len = modType %~ \modType' ident -> (modType' ident) `subs` len

cStar :: Expr a -> Expr a
cStar = unop 3 RTL "*"

cStarR :: Expr a -> Expr a
cStarR = unop 3 RTL "*restrict"

cAmp :: Expr a -> Expr a
cAmp = unop 3 RTL "&"

preIncr :: Expr a -> Expr a
preIncr = unop 3 RTL "++" 

ePreIncr :: ELValue a -> Expr a
ePreIncr = preIncr . toExpr 

cFun :: Expr a -> [Expr b] -> Expr c
cFun f args = let e = Expr 2 LTR (maybeParens e f <> Code (parens (hsep $ punctuate comma $ map (asDoc . cexpr) args))) in e

tFun :: CType -> [CType] -> CType
tFun ty argsty = CType {
  _qualifyType = []
 ,_baseType = anonTy ty 
 ,_modType  = id
 ,_modTypeAfter  = (\ident -> cFun ident (map (lit . anonTy) argsty))
 }

tFunPtr :: CType -> [CType] -> CType
tFunPtr ty argsty = tPointer (tFun ty argsty)

dFunProto :: CType -> CIdent -> [(CIdent, CType)] -> Code
dFunProto ret name args = Code$ asDoc (anonTy ret) <+> asDoc name <+> asDoc (arglist [declare ty [tag] | (tag, ty) <- args]) 

dFunDecl :: CType -> CIdent -> [(CIdent, CType)] -> Code
dFunDecl ret name args = dFunProto ret name args <> Code semi

dFunction :: CType -> CIdent -> [(CIdent, CType)] -> Code -> Code
dFunction ret name args body = Code$ asDoc (dFunProto ret name args) <+> "{" $+$ asDoc (indent body) $+$ "}"
{-
dFunctionA :: (Applicative m) => CType -> CIdent -> [(CIdent, CType)] -> m Code -> m Code
dFunctionA ret name args body = dFunction ret name args <$> body
-}

sNop :: Code
sNop = mempty

sBreak = stm "break"

sBreakA :: (Applicative f) => f Code
sBreakA = pure sBreak

sNopA :: (Applicative m) => m Code
sNopA = pure sNop

sIfElse :: [(Cond, Code)] -> Code
sIfElse cs₀ =
  let cs₁ = [(solveCond' cond, code) | (cond, code) <- cs₀, $(isNot 'CodeEmpty) code]  in
  let (cs₂, listToMaybe -> e') = span $(isNotP [p| (Right True,_) |]) cs₁    in
  let cs =  [(cond, code) | (Left cond, code) <- cs₂]                     in
  case cs of
    [] -> case e' of 
            Nothing    -> sNop
            Just (_,e) -> e
    (cond₁,b₁):as -> Code$ "if" <+> asDoc (parens (cexpr cond₁)) <+> "{" $+$
                          asDoc (indent b₁) $+$
                          vcat [ "}" <+> "else" <+> "if" <+> asDoc (parens (cexpr cond)) <+> "{" $+$
                          asDoc (indent b_i)
                          | (cond,b_i) <- as ] $+$
                          case e' of
                            Nothing     -> "}"
                            Just (_,e)  -> "}" <+> "else" <+> "{" $+$
                                           asDoc (indent e) $+$
                                           "}"

sWhile :: Cond -> [Code] -> Code
sWhile cond body = case statements body of
  CodeEmpty -> CodeEmpty
  body ->
    case solveCond' cond of
         Right False -> CodeEmpty
         Right True  -> Code$ "for(;;)" <+> "{" $+$ asDoc (indent body) $+$ "}"
         Left  cond₁ -> Code$ "while" <+> asDoc (parens (cexpr cond₁)) <+> "{" $+$ asDoc (indent body) $+$ "}"

sWhileA :: (Applicative m) => Cond -> [m Code] -> m Code                                                         
sWhileA cond body = sWhile cond <$> sequenceA body

sIfElseA :: (Applicative m) => [(Cond, m Code)] -> m Code
sIfElseA cond = sIfElse <$> sequenceA [(e,) <$> c | (e,c) <- cond]

sIfElseM :: (Applicative m) => [(m Cond, m Code)] -> m Code
sIfElseM cond = sIfElse <$> sequenceA [(,) <$> e <*> c | (e,c) <- cond]

cElse :: Cond
cElse = Cond $ Prop.pTrue

cElseM :: (Applicative m) => m Cond
cElseM = pure cElse 


eTrue, eFalse :: Cond
eTrue = eBool True
eFalse = eBool False

eBool = Cond . Prop.pBool

sComment :: Code -> Code
sComment str = Code$ "/*" $$ nest 1 (asDoc str) $$ "*/"

sCommentStr :: String -> Code
sCommentStr str = Code$ "/*" $$ nest 1 (fromString str) $$ "*/"
{-
eTrue, eFalse :: Expr Bool
(eTrue, eFalse) = ("true", "false")
-}
sUnreachable :: Code
sUnreachable = sComment "Unreachable"

sReturn :: IsExpr e a => e -> Code
sReturn e = Code$ "return" <+> asDoc (stmExpr e)

sReturnA :: (Applicative m, IsExpr e a) => e -> m Code
sReturnA = pure . sReturn



sForA :: (MonadFresh m, Applicative m) => Name -> Length -> (CIndex -> m Code) -> m Code
sForA name len κ = do
  (i, κ') <- sForADecl name len
  κ' <$> κ i
  
sForADecl :: (MonadFresh m, Applicative m) => Name -> Length -> m (CIndex, Code -> Code)
sForADecl name 0   = return (0, \_ -> sNop)
sForADecl name 1   = return (0, id)
sForADecl name len = do
  ident <- freshCIdent name
  let decl = declareInitStm tSizeT ident (toExpr (0 :: Length))
  let index = lit ident
  let l_index = fromExpr index
  return (l_index,
          \case
             CodeEmpty ->  CodeEmpty
             body ->  blockP (Code$ "for" <+> asDoc (parens (hstatements [
                                                             decl,
                                                             stm$ cexpr $ index <. toExpr len,
                                                             cexpr $ index `assign` toExpr (eLen index + 1) ]))) $
                      body)

assign :: LValue a -> Expr b -> Expr a
assign = biop 16 RTL "="

assignPlus :: LValue a -> Expr b -> Expr a
assignPlus = biop 16 RTL "+="

cDot :: LValue a -> CIdent -> LValue b
cDot a field =
  let e = Expr { _prec = 2
               , _assoc = LTR
               , _expr = maybeParens e a <> "." <> field
               }
  in e

cArrow :: LValue a -> CIdent -> LValue b
cArrow a field =
  let e = Expr { _prec = 2
               , _assoc = LTR
               , _expr = maybeParens e a <> "->" <> field
               }
  in e


subs :: Expr a -> CIndex -> Expr b
subs a position =
  let e = Expr { _prec = 2
               , _assoc = LTR
               , _expr = maybeParens e a <> brackets (_expr $ toExpr position)
               }
  in e

freshCIdent :: (MonadFresh m) => Name -> m CIdent
freshCIdent name = do
    ident <- freshFrom name
    return $ cident $ (ident :: Id)

tPrim :: Name -> CType
tPrim = prim . fromString

cCall :: Code -> [Code] -> Code
cCall n as = n <> arglist as

cLit :: String -> CExpr a
cLit = fromString

cWithLocal :: (MonadFresh m) => CType -> Name -> (ELValue a -> m Code) -> m Code
cWithLocal t name κ = cWithLocals t [name] (\[l] -> κ l)

cWithLocalMaybe :: (MonadFresh m) => Maybe CType -> Name -> (Maybe (ELValue a) -> m Code) -> m Code
cWithLocalMaybe t name κ = case t of
  Nothing -> κ Nothing
  Just t  -> cWithLocal t name (κ . Just)


cWithLocals :: (MonadFresh m) => CType -> [Name] -> ([ELValue a] -> m Code) -> m Code
cWithLocals t names κ = do
  (ls,κ') <- declareLocals t names
  κ' <$> κ ls

declareLocal :: (MonadFresh m) => CType -> String -> m (ELValue a, Code -> Code)
declareLocal cty name = declareLocals cty [name] >>= \([l],κ) -> return (l, κ)

declareLocals :: (MonadFresh m) => CType -> [String] -> m ([ELValue a], Code -> Code)
declareLocals t names = do
  idents <- T.mapM freshCIdent names
  return (map eLit idents, \κ ->
    statements [
      declareStm t idents
     ,κ
     ])

declareLocalInit :: (MonadFresh m) => CType -> String -> Expr a -> m (ELValue a, Code -> Code)
declareLocalInit t name initial = do
  ident <- freshCIdent name
  return (eLit ident, \κ ->
    statements [
      declareInitStm t ident initial
     ,κ
     ])

declareInit :: (MonadFresh m) => CType -> String -> Expr a -> m (ELValue a, Code) 
declareInit t name initial = do
  (l, f) <- declareLocalInit t name initial
  return (l, f mempty)

cComma = biop 20 LTR "," 

cCmpOp :: CmpOp -> Expr a -> Expr b -> Expr c
cCmpOp Eq = biop 9 LTR "=="
cCmpOp Ne = biop 9 LTR "!="
cCmpOp Lt = biop 8 LTR "<"
cCmpOp Le = biop 8 LTR "<="
cCmpOp Gt = biop 8 LTR ">"
cCmpOp Ge = biop 8 LTR ">="
cCmpOp CmpFalse = \a b -> a `cComma` b `cComma` toExpr eFalse
cCmpOp CmpTrue  = \a b -> a `cComma` b `cComma` toExpr eTrue

(<=:), (≤:), (<:), (>=:), (≥:), (>:), (==:), (!=:) :: MonadLength m => Length -> Length -> m Cond
(<=:) = lenCmpOp Le
(≤:)  = (<=:)

(<:)  = lenCmpOp Lt

(>=:) = lenCmpOp Ge
(≥:)  = (>=:)

(>:)  = lenCmpOp Gt

(==:) = lenCmpOp Eq
(!=:) = lenCmpOp Ne

(&&:) :: Cond -> Cond -> Cond
x &&: y = Cond $ Prop.pAnd (getCond x) (getCond y)

-- | Expensive
(||:) :: Cond -> Cond -> Cond
x ||: y = Cond $ Prop.pOr (getCond x) (getCond y)

(!:) :: Cond -> Cond
(!:) = Cond . Prop.pNot . getCond

(=:) :: (IsExpr e b) => Mem a -> e -> Expr a
l =: r = toExpr l =. toExpr r

(+=:) :: (IsExpr e b) => Mem a -> e -> Expr a
l +=: r = toExpr l +=. toExpr r


lenCmpOp :: (MonadLength m) => CmpOp -> Length -> Length -> m Cond
lenCmpOp Eq m n | m == n  = return$ Cond Prop.pTrue
lenCmpOp op (asScalar -> Just i)
            (asScalar -> Just j) =
  return$ Cond (Prop.pBool $ (haskellCmpOp op) i j)
lenCmpOp op m n = lengthPredProve (m,op,n) $ \case
  Just res -> return (eBool res)
  Nothing  -> return$ Cond $ Prop.lit $ (cCmpOp op) (toExpr m) (toExpr n)
{-
assertLengthEq :: Length -> Length -> (Length -> Code) -> Code
assertLengthEq a b κ = runIdentity $ assertLengthEqA a b (Identity . κ)
-}
assertLengthEqA :: (Applicative m, MonadLength m) => Length -> Length ->
                   (Length -> m Code) -> m Code
assertLengthEqA a b κ = 
  statementsA [
    cAssert `liftM` (a ==: b) 
   ,case (a,b) of
       (a,_) | isScalar a -> κ a
       (_,b) | isScalar b -> κ b
       (a,_)              -> κ a 
   ]
                  
cAssert :: Cond -> Code
cAssert (Cond p) | Just True <- Prop.solve p = sNop
cAssert e = "assert" <> parens (cexpr e)

rangeA l x = rangeAPragma mempty l x

rangeAPragma _ 0 κ = pure $ sNop
rangeAPragma _ 1 κ = κ 0
rangeAPragma p len κ =
  statementsA [ return p
              , sForA "i" len κ ]

solveCond = Prop.solve . getCond
solveCond' = Prop.solve' Cond . getCond

-- | Uses Haskell escaping to make C string literals
eStr :: String -> Expr String
eStr a = slit $ show a

cMalloc :: Length -> Expr a
cMalloc len = call "malloc" [EA$ toExpr len] 

cFree :: Expr a -> Code
cFree e = stmExpr$ call "free" [EA e]

cFreeA :: (Applicative m) => Expr a -> m Code
cFreeA = pure . cFree

cSizeOf :: CType -> Length 
cSizeOf ty = fromExpr$ call "sizeof" [EA$ lit (anonTy ty)]

eUnionLit :: Expr a -> Expr b
eUnionLit = lit . Code . braces . asDoc . cexpr

lengthPredAdd :: (MonadReader LengthPredicates m) => (Length,CmpOp,Length) -> m α -> m α 
lengthPredAdd (Length p, op, Length q) = Pol.polPredAdd (p, op, q)

data GlobalDecl = DeclareHeader Code
                | IncludeGlobal String 

pragmaOmp :: String -> Code
pragmaOmp (fromString -> s) = Code$ "#pragma omp" <+> s

pragmaWarning :: String -> Code
pragmaWarning s = "#warning " <> cexpr (eStr s)

condIsChoice :: Op.Choice -> Cond -> Cond
condIsChoice (choiceToBool -> True)  = id
condIsChoice (choiceToBool -> False) = (!:) 
