{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PatternGuards #-}
module Ritchie.C.Mem where

import Ritchie.C.Pretty
import Ritchie.C.Core
import Ritchie.C.Type
import Data.Generics.Is
import Development.Placeholders
import Control.Monad.Writer
import Ritchie.Stash
import Control.Applicative
import Data.Singletons
import Fresh
import FileLocation
import Data.Void
import GHC.Exts
import Data.Monoid
import Op
import Data.Function
import Ritchie.Term (capture', captureM, capture, NbE)

newtype Align = Mask [CType] deriving (Show, Monoid)

data InitF f = Noop | Eager f deriving (Show, Functor)

lengthSelf    :: RichAtomTy b -> Length
lengthSelf = maybe 0 cSizeOf . cTy

alignMaskSelf :: RichAtomTy b -> Align
alignMaskSelf = Mask . maybe [] (:[]) . cTy

alignMaskChildren :: RichAtomTy b -> Align 
alignMaskChildren = go
  where
    go (A RATy{..}) = case tyATy of
      ABox _ ta       -> alignMaskChildren ta
      ATuple ta tb    -> alignMaskChildren ta `maskJoin` alignMaskChildren tb 
      ASum   ta tb    -> alignMaskChildren ta `maskJoin` alignMaskChildren tb 
      ABool           -> mempty
      APrim  _ _      -> mempty
      AUnit  _        -> mempty
      AVec0  _ _      -> mempty
      AVec1  _ ta       -> alignMaskChildren ta
      AVec   _ _ ta     -> alignMaskSelf ta `maskJoin` alignMaskChildren ta
      AVecPacked _ _ _ ta tb -> alignMaskSelf ta `maskJoin` alignMaskChildren ta `maskJoin`
                              alignMaskSelf tb `maskJoin` alignMaskChildren tb `maskJoin`
                              Mask [tSizeT]
      ASigmaSize _ _ ta -> alignMaskChildren ta 
      _ -> mempty

unwrapAtom_ :: (SingI b) => Atom b
               -> (Atom b -> TVoid) -- ^ after memory allocation
               -> TVoid
unwrapAtom_ a κ₂ = unwrapAtom a (flip ($)) κ₂

unwrapAtomM_ :: (NbE Atom TVoid EnvM, SingI b) => Atom b
               -> (Atom b -> EnvM TVoid) -- ^ after memory allocation
               -> EnvM TVoid
unwrapAtomM_ = capture' .  unwrapAtom_  
 

nodeAsWrapped :: Atom b -> (forall a. Maybe (ELValue a, Atom b) -> α) -> α
nodeAsWrapped a@Atom{atomV,atomTy=atomTy@(A RATy{tyNode,..})} κ =
  case nodeIsWrapped tyNode of
    False -> κ Nothing
    True  -> atomAsMemW a$ \(Just l) ->
      let atomTy_c = (A RATy{tyNode = AllocDynamic,..})  
          atomV_c  = if isVoidTy atomTy then AVoid
                                        else AMem (l `dot` "fill")
      in
      κ$ Just (l `dot` "mb", Atom{atomV=atomV_c,atomTy=atomTy_c})

unsafeUnwrapAtom :: Atom b -> Maybe (Atom b)
unsafeUnwrapAtom a@Atom{atomTy=A RATy{tyATy,tyNode,tyComm}} =
  nodeAsWrapped a $ \case 
    Nothing -> Just a
    Just(_,a) -> case (tyComm,tyNode) of
      (SimplexWrite, AllocDeferred) -> Nothing
      (SimplexWrite, Stored)        -> Nothing
      _                             -> Just a

unwrapAtomM :: (NbE Atom TVoid EnvM, SingI b) => Atom b
               -> (Atom b  -> (Atom b' -> EnvM TVoid) -> EnvM TVoid)    -- ^ before memory allocation
               -> (Atom b' -> EnvM TVoid) -- ^ after memory allocation
               -> EnvM TVoid
unwrapAtomM a κ₁ κ₂ = do
  κ₁' <- capture (\a -> captureM $ κ₁ a)
  let f = capture' $ unwrapAtom a κ₁'
  f κ₂

unwrapAtom :: (SingI b) => Atom b
               -> (Atom b  -> (Atom b' -> TVoid) -> TVoid)    -- ^ before memory allocation
               -> (Atom b' -> TVoid) -- ^ after memory allocation
               -> TVoid
unwrapAtom = go
  where
    go :: (SingI b) => Atom b
               -> (Atom b  -> (Atom b' -> TVoid) -> TVoid)
               -> (Atom b' -> TVoid) 
               -> TVoid
    go a@Atom{atomV,atomTy=atomTy@(A RATy{tyComm,tyATy,tyNode,..})} κ₁ κ₂ =
      nodeAsWrapped a $ \case 
        Nothing -> κ₁ a κ₂ 
        Just (mb,a_c) -> 
            case tyComm of
              SimplexWrite ->
                κ₁ a_c $ \a' -> do
                  (f₂,Sum len) <- runWriterT $ initialize a'
                  statementsA [case tyNode of
                                  AllocDeferred -> stmExprA$ mb =: cMalloc len
                                  _ -> sNopA
                              ,f₂ (Block$ toExpr$ mb) 
                              ,κ₂ a']
              _ -> κ₁ a_c κ₂

    initialize :: Atom b -> WriterT (Sum Length) TVoidM (MemBlock -> TVoid)
    initialize = go
     where
       go :: Atom b -> WriterT (Sum Length) TVoidM (MemBlock -> TVoid)
       go a@Atom{atomV,atomTy=atomTy@(A RATy{tyATy,tyNode,..})} = 
          case tyNode of
            Stack -> return (\_ -> sNopA)
            AllocDeferred -> return (\_ -> sNopA)
            Stored -> do
              nodeAsWrapped a$ \(Just (mb',a')) -> do
                _ <- go' a'
                return (\mb -> stmExprA$ mb' =: mb)
            StoredEager   -> $(todo "Eager initialization of sums")
            AllocDynamic -> go' a       

       go' :: Atom b -> WriterT (Sum Length) TVoidM (MemBlock -> TVoid)
       go' a@(Atom atomV (A RATy{tyATy})) =
         atomAsMemW a $ \case
           Nothing -> noInit
           (accessors -> Accessors{..}) -> 
             case tyATy of
               -- This is consistent with Ritchie.C.Type.richTyNode
               ATuple ta tb      -> (align [alignMaskChildren tb] $ go $ π₁ ta) `andThen`
                                                 (go $ π₂ tb)
               ASum   ta tb      -> do
                  censorWriterT (\(LengthMax a) -> (Sum a))$ do
                    censorWriterT (\(Sum a) -> (LengthMax a))$ go (inl ta)
                    censorWriterT (\(Sum a) -> (LengthMax a))$ go (inr tb)
                  noInit

               ASigmaSize _ _ ta -> go (val ta) 
               AVec   _ len'@(lengthVec -> Just len) ta -> 
                 align [alignMaskChildren ta] $ (case len' of
                   Dependent _ -> do
                       tell (Sum$ len * lengthSelf ta)
                       self_l $ \l ->
                         return (\mb -> stmExprA$ l =: tPointer tVoid `cCast` toExpr mb)
                   _ -> noInit
                 )
                 `andThen` 
                 (do
                    (i, κ) <- sForADecl "i" len
                    (f, Sum len_a) <- censor (\(Sum a) -> (Sum (a * len))) $ 
                      listen $ align [alignMaskChildren ta] 
                             $ go (idx len' ta i)
                    return (\mb -> 
                       fmap κ$ do
                         f (mb +> i * len_a)))

               AVecPacked _ _ len'@(lengthVec . Dependent -> Just len) ta tb -> 
                 align [alignMaskChildren ta, alignMaskChildren tb] $ (do
                       ((),Sum skips_len) <- listen$ align [alignMaskSelf ta]$ tell (Sum$ (len + 4) * cSizeOf tSizeT )
                       ((),Sum ta_len)    <- listen$ align [alignMaskSelf tb]$ tell (Sum$ len * lengthSelf ta)
                       ((),Sum tb_len)    <- listen$ tell (Sum$ len * lengthSelf tb)
                       return (\mb ->
                             statementsA [
                               skip $ \l -> stmExprA$ l =: tPointer tVoid `cCast` toExpr mb
                              ,stmExprA$ idx_skip 0 =: (0 :: Length)
                              ,skip $ \l -> stmExprA$ l =: toExpr l +. (toExpr (1 :: Length))
                              ,if not$ isVoidTy ta then packed_inl $ \l -> stmExprA$ l =: tPointer tVoid `cCast` toExpr (mb +> skips_len) else sNopA
                              ,if not$ isVoidTy tb then packed_inr $ \l -> stmExprA$ l =: tPointer tVoid `cCast` toExpr (mb +> skips_len +> ta_len) else sNopA
                              ]))
                 `andThen` 
                 (do
                    (i, κ) <- sForADecl "i" len
                    (f₁, Sum len_a) <- censor (\(Sum x) -> (Sum (x * len))) $ do
                      listen $ align [alignMaskChildren ta] 
                             $ go (idx_inl ta i)
                    (f₂, Sum len_b) <- censor (\(Sum x) -> (Sum (x * len))) $ do
                      listen $ align [alignMaskChildren tb] 
                             $ go (idx_inr tb i)
                    return (\mb -> 
                       statementsA [fmap κ$ f₁ (mb +> i * len_a)
                                   ,fmap κ$ f₂ (mb +> len * len_a +> i * len_b)]))

               AVec   _ _ ta  -> $(err') "Free variable in length when allocating"
               AVec1  _ ta    -> go (self ta)
               ABox _  ta     -> go (self ta)
               _               -> noInit

       align :: (MonadWriter (Sum Length) m) => [Align] -> m α -> m α
       align as m = censor (\(Sum len) -> Sum (alignLength len (mconcat as))) m

       andThen :: (Monad m) => WriterT (Sum Length) m (MemBlock -> TVoid) 
               -> WriterT (Sum Length) m (MemBlock -> TVoid) 
               -> WriterT (Sum Length) m (MemBlock -> TVoid)
       m₁ `andThen` m₂ = do
         (f₁,Sum len₁) <- listen m₁ 
         f₂ <- m₂
         return (\mb -> statementsA [f₁  mb
                                    ,f₂ (mb +> len₁)
                                    ])

       noInit :: WriterT (Sum Length) TVoidM (MemBlock -> TVoid) 
       noInit = return $ const sNopA
         
censorWriterT :: (Monad m) => (w -> w') -> WriterT w m a -> WriterT w' m a     
censorWriterT f = mapWriterT (liftM (\(a,w) -> (a,f w))) 



newtype LengthMax = LengthMax { getLengthMax :: Length }

instance Monoid (LengthMax) where
  mempty = LengthMax 0
  mappend = (LengthMax.) . (lengthMax `on` getLengthMax)

destroyAtom    :: Atom b -> TVoid
destroyAtom = go
  where
    go :: Atom b -> TVoid
    go a@Atom{atomV,atomTy=atomTy@(A RATy{..})} = do
      nodeAsWrapped a $ \case 
          Nothing -> atomAsMem a $ \(accessors -> Accessors{..}) ->
            case tyATy of
              ATuple ta tb      -> statementsA [go (π₁ ta), go (π₁ tb)]
              ASum   ta tb      -> 
                   sIfElseA [(condIsChoice Inl btag, go (inl ta))
                            ,(cElse                , go (inr tb))]
              AVec   _ len'@(lengthVec -> Just len) ta     ->
                rangeA len $ \i -> go (idx len' ta i)
              AVec   _ len ta     -> $(err') "Can't destroy vector with free variables in length"
              ASigmaSize _ v ta' ->
                   let ta = tyBindVarSize (v, σlen) ta' in
                   go (val ta)
              ABox _  ta        -> go (self ta)
              _                 -> sNopA
          Just (mb,fill) ->
            statementsA [
              go fill
             ,case tyNode of
                AllocDeferred -> cFreeA (toExpr mb)
                _ -> sNopA
             ]
 
accessors :: (Maybe (ELValue a)) -> Accessors 
accessors = \case
  Nothing ->
    Accessors {
      π₁   = Atom AVoid
     ,π₂   = Atom AVoid
     ,inl  = Atom AVoid
     ,inr  = Atom AVoid
     ,idx_inl  = flip$ const$ Atom AVoid
     ,idx_inr  = flip$ const$ Atom AVoid
     ,idx_skip = (\_ -> $(err') "Void packed vector")
     ,packed_inl  = (\_ -> $(err') "Void packed vector") :: forall α. (forall a. ELValue a -> α) -> α
     ,packed_inr  = (\_ -> $(err') "Void packed vector") :: forall α. (forall a. ELValue a -> α) -> α
     ,skip = (\_ -> $(err') "Void packed vector") :: forall α. (forall a. ELValue a -> α) -> α
     ,n_skip = $(err') "Void packed vector"
     ,inlArgs = structArgs (AVoid)
     ,inrArgs = structArgs (AVoid)
     ,inlRet  = structRet  (AVoid)
     ,inrRet  = structRet  (AVoid)
     ,btag = $(err') "Can't read tag from void"
     ,btag_w = Nothing 
     ,val  = Atom AVoid
     ,σlen = $(err') "Can't read length from void"
     ,σlen_w = Nothing
     ,idx  = \case
         VecZero     -> flip$ const$ Atom AVoid
         VecOne      -> flip$ const$ Atom AVoid
         Static{}    -> flip$ const$ Atom AVoid
         Dependent{} -> flip$ const$ Atom AVoid
     ,self = Atom AVoid
     ,self_l = (\_ -> $(err') "Void vector") :: forall α. (forall a. ELValue a -> α) -> α
     ,self_bool = $(err') "Void bool"
                   
     }
  Just l ->
    Accessors {
      π₁   = Atom (AMem (l `dot` "pi1"))
     ,π₂   = Atom (AMem (l `dot` "pi2"))
     ,inl  = Atom (AMem (l `dot` "val" `dot` "inl"))
     ,inr  = Atom (AMem (l `dot` "val" `dot` "inr"))
     ,n_skip = (l `dot` "n")
     ,skip = (\f -> f$ l `dot` "skip")
     ,packed_inl = (\f -> f$ l `dot` "inl")
     ,packed_inr = (\f -> f$ l `dot` "inr")
     ,inlArgs = structArgs (AMem (l `dot` "inl"))
     ,inrArgs = structArgs (AMem (l `dot` "inr"))
     ,inlRet  = structRet  (AMem (l `dot` "inl"))
     ,inrRet  = structRet  (AMem (l `dot` "inr"))
     ,val  = Atom (AMem (l `dot` "val"))
     ,btag = fromExpr$ castExpr (l `dot` "tag")
     ,btag_w = Just (l `dot` "tag")
     ,σlen = fromExpr$ castExpr (l `dot` "len")
     ,σlen_w = Just (l `dot` "len")
     ,idx_inl = flip $ \i -> Atom (AMem (l `dot` "inl" `eSubs` i))
     ,idx_inr = flip $ \i -> Atom (AMem (l `dot` "inr" `eSubs` i))
     ,idx_skip = \i -> l `dot` "skip" `eSubs` i
     ,idx  = \case
         VecZero     -> flip$ const$ Atom AVoid 
         VecOne      -> flip$ const$ Atom (AMem l)
         Static{}    -> flip $ \i -> Atom (AMem (l `dot` "vec" `eSubs` i))
         Dependent{} -> flip $ \i -> Atom (AMem (l `eSubs` i))
     ,self = Atom (AMem l)                          
     ,self_l = \f -> f l
     ,self_bool = fromExpr$ castExpr l
     }


data Accessors = Accessors {
  π₁   :: forall b. RichAtomTy b -> Atom b
 ,π₂   :: forall b. RichAtomTy b -> Atom b
 ,inl  :: forall b. RichAtomTy b -> Atom b
 ,inr  :: forall b. RichAtomTy b -> Atom b
 ,idx_inl  :: forall b. RichAtomTy b -> CIndex -> Atom b
 ,idx_inr  :: forall b. RichAtomTy b -> CIndex -> Atom b
 ,idx_skip :: CIndex -> LengthLValue
 ,skip     :: forall α. (forall a. ELValue a -> α) -> α
 ,n_skip :: LengthLValue
 ,packed_inl :: forall α. (forall a. ELValue a -> α) -> α
 ,packed_inr :: forall α. (forall a. ELValue a -> α) -> α
 ,inlArgs :: forall b. RichAtomTy b -> [AtomV]
 ,inrArgs :: forall b. RichAtomTy b -> [AtomV]
 ,inlRet  :: forall b. RichAtomTy b -> AtomV
 ,inrRet  :: forall b. RichAtomTy b -> AtomV
 ,val  :: forall b.  RichAtomTy b -> Atom b
 ,btag   :: Cond
 ,btag_w :: Maybe CondLValue
 ,σlen   :: Length
 ,σlen_w :: Maybe LengthLValue
 ,idx  :: forall b. VecLength -> RichAtomTy b -> CIndex -> Atom b
 ,self :: forall b. RichAtomTy b -> Atom b 
 ,self_l :: forall α. (forall a. ELValue a -> α) -> α
 ,self_bool :: Cond
 }

structArgs a ty = [ case arg of
                      Nothing -> AVoid   
                      Just _  -> case a of AMem l -> AMem (l `dot` argName)
                  | arg     <- fst$ cFunTy ty 
                  | argName <- cFunTyArgNames
                  ]

structRet a ty = case snd$ cFunTy ty of
                      Nothing -> AVoid
                      Just _  -> case a of AMem l -> AMem l
                
   


{-           
data Init = Init {
  lengthSelf        :: Length
 ,alignMaskSelf     :: Align  

 ,lengthChildren    :: Length
 ,alignMaskChildren :: Align  
  -- | Initializes the value to prepare it for writing.
  --   This includes allocating memory for the children in the memory
  --   block, writing pointers in the parent, and initializing the
  --   children values.
 ,initF             :: InitF (MemBlock -> Length       -> EA ELValue -> TVoid)
 ,initFChildren     :: InitF (MemBlock -> LengthLValue -> EA ELValue -> TVoid)
 } deriving (Show)
-}
-- ^ Contains an expression which points to an untyped block of memory
data MemBlock where Block :: Expr a -> MemBlock
deriving instance Show (MemBlock)

instance IsExpr MemBlock Void where
  toExpr (Block e) = castExpr e
  fromExpr = Block

(+>) :: MemBlock -> Length -> MemBlock
Block a +> len = Block$ a +. toExpr len
infixl 5 +>

{-
-- Note: if the alignment is bigger than the length, then it will be
-- built into the length.
-- Contract: MemBlock is well-aligned for any kind of data.
-- Returns amount of memory to heap-allocate for the children. 
initializeType :: (SingI b) => RichAtomTy b -> (Length, MemBlock -> ELValue a -> TVoid)
initializeType ty =
    let Init{..} = initializeType' ty in
    (lengthChildren, \mb l ->
      cWithLocal tSizeT "offset" $ \offset ->
        statementsA $ [ stmExprA$ offset =: (0 :: Length)]
                   ++ [ f mb (fromExpr$ toExpr$ offset) (EA l) | Eager f <- [initF] ]
                   ++ [ f mb offset (EA l) | Eager f <- [initFChildren] ]
                    )

initializeType' :: (SingI b) => RichAtomTy b -> Init
initializeType' (A ty) = case  richTypeInit ty of
  Just i  -> i
  Nothing -> $(err') $ "Type " ++ show (A ty) ++ " is not data."
    -- Contract:
    --   + MemBlock + offset is aligned according to the returned alignment for the
    --     children.
    --   + Memory after one copy of the children, or one copy of self, is well
    --     aligned for more copies.

initializeType :: RichAtomTy b -> EnvM Init

richTypeInit :: forall b. SingI b => RichAtomTy b -> TyEnvM Init
richTypeInit (A RATy{..}) =
    let lengthSelf_ = maybe 0 cSizeOf tyCTy
        alignMaskSelf_ = Mask [t | Just t <- [tyCTy]]
        r x = return$ initializeType' x 
    in
    case tyATy of
    ABox _ ta -> r ta
    ATuple ta tb -> do
      a <- r ta; b <- r tb
      let a_len = alignLength (lengthChildren a) (alignMaskChildren b)  
          b_len = lengthChildren b
          ab_mask = alignMaskChildren a `maskJoin` alignMaskChildren b 
      return$ Init{lengthSelf = lengthSelf_, alignMaskSelf = alignMaskSelf_ 
          ,lengthChildren = alignLength (a_len + b_len) ab_mask
          ,alignMaskChildren  = ab_mask
          ,initF         = case (initF a, initF b) of
            (Noop   , Noop) -> Noop 
            (a      , b   ) ->
             Eager$ \mb offset (EA l) ->
              statementsA $ [ f mb  offset           (EA$ l `dot` "pi1") | Eager f <- [a] ]
                         ++ [ g mb  (offset + a_len) (EA$ l `dot` "pi2") | Eager g <- [b] ]
          ,initFChildren = case (initFChildren a, initFChildren b) of
              (Noop   , Noop   ) -> Noop
              (a      , b      ) -> Eager$ \mb offset (EA l) -> statementsA$ 
                [f mb offset (EA$ l `dot` "pi1") | Eager f <- [a]] ++
                [g mb offset (EA$ l `dot` "pi2") | Eager g <- [b]]
          }

    ASum ta tb -> do
      a <- r ta; b <- r tb
      lengthChildren_ <- lengthChildren a `lengthMax` lengthChildren b

      let initF_ = case (initF a, initF b) of
            (Eager f, Noop) -> Eager $ \mb offset (EA l) -> f mb offset (EA$ l `dot` "val" `dot` "inl")
            (Noop, Eager g) -> Eager $ \mb offset (EA l) -> g mb offset (EA$ l `dot` "val" `dot` "inr")
            (Noop  , Noop ) -> Noop
            _               -> onWrite

          initFChildren_ = case (initFChildren a, initFChildren b) of
            (Eager f, Noop) -> Eager $ \mb offset (EA l) -> f mb offset (EA$ l `dot` "val" `dot` "inl")
            (Noop, Eager g) -> Eager $ \mb offset (EA l) -> g mb offset (EA$ l `dot` "val" `dot` "inr")
            (Noop  , Noop ) -> Noop
            _               -> onWrite 

      let alignMaskChildren_ = alignMaskChildren a `maskJoin` alignMaskChildren b

      return$ Init{lengthSelf = lengthSelf_
                  ,lengthChildren = alignLength lengthChildren_ alignMaskChildren_
                  ,alignMaskSelf = alignMaskSelf_
                  ,alignMaskChildren = alignMaskChildren_
                  ,initF = initF_
                  ,initFChildren = initFChildren_
                  }

    APrim _ s -> return$ Init{ lengthSelf = lengthSelf_
                             , alignMaskSelf = alignMaskSelf_
                             , lengthChildren = 0, alignMaskChildren = Mask []
                             , initF = onWrite
                             , initFChildren = Noop
                             }

    AUnit _           -> return initUnit
    AVec0 p _  -> richTypeInit Nothing (AUnit p)
    AVec1 _ ta -> r ta

    AVec p (Static len) ta -> do
      a <- r ta
      let sz = fromInteger len
          initF_ = flip fmap (initF a) $ \g mb offset (EA l) -> rangeA sz (\i -> g mb (offset + i * lengthChildren a) (EA$ l `eSubs` i))
          initFChildren_ = flip fmap (initFChildren a) $ \g ->
            \mb offset (EA l) -> rangeA sz $ \i -> g mb offset (EA$ l `eSubs` i)

      return$ Init{lengthSelf = lengthSelf_, alignMaskSelf = alignMaskSelf_
          ,alignMaskChildren = alignMaskChildren a
          ,lengthChildren = sz * lengthChildren a
          ,initF = initF_
          ,initFChildren = initFChildren_
          }

    AVec p (Dynamic len) ta -> do
      a <- r ta
      let initF_ = Eager$ \mb offset (EA l) -> stmExprA$ l =: tPointer tVoid `cCast` (toExpr$ mb +> offset)

          initFChildren_ = case (initF a, initFChildren a) of
                 (Noop   , Noop   ) -> Noop
                 (Noop   , Eager g) -> Eager$ \mb offset (EA l) -> statementsA [
                   stmExprA$ offset +=: lenC
                  ,rangeA len $ \i -> g mb offset (EA$ l `eSubs` i)
                  ]
                 (Eager f, Eager g) -> Eager$ \mb offset (EA l) -> 
                                         statementsA [
                                           stmExprA$ offset +=: lenC
                                          ,rangeA len $ \i -> 
                                             statementsA [ f mb (fromExpr$ toExpr offset) (EA$ l `eSubs` i)
                                                         , g mb offset          (EA$ l `eSubs` i) ]
                                          ]
                 (Eager f, Noop   ) -> Eager$ \mb offset (EA l) ->
                   statementsA [stmExprA$ offset +=: lenC
                               ,rangeA len $ \i -> f mb (fromExpr$ toExpr offset) (EA$ l `eSubs` i)]

          lenC  = alignLength (len * lengthSelf a) (alignMaskChildren a)  
          lenCC = len * lengthChildren a 
          maskC = alignMaskSelf a `maskJoin` alignMaskChildren a

      return$ Init{lengthSelf = lengthSelf_
                  ,alignMaskSelf = alignMaskSelf_
                  ,alignMaskChildren = maskC
                  ,lengthChildren = alignLength (lenC + lenCC) maskC
                  ,initF = initF_ 
                  ,initFChildren = initFChildren_
                  }


onWrite :: (IsExpr e Integer) => InitF (MemBlock -> e -> EA ELValue -> TVoid)
onWrite = Eager (\_ _ _ -> sNopA)
-}

lengthMax :: Length -> Length -> Length 
lengthMax 0 b = b
lengthMax a 0 = a
lengthMax a b | a == b = a -- == b
lengthMax a b = fromExpr$ call (lit "size_t_max") [EA$ toExpr a, EA$ toExpr b]


(Mask a) `maskJoin` (Mask b) = Mask$ a <> b

-- Retrieves alignment for a type
alignTyOf :: Align -> CType
alignTyOf (Mask []) = tChar
alignTyOf (Mask [tt]) = tt 
alignTyOf (Mask tts) = tUnion [(fromString$ "y" ++ show i, tt)
                       | i <- [1..] 
                       | tt <- tts] 

alignOf :: Align -> Length 
alignOf (Mask []) = 1
alignOf tts = fromExpr$ call (lit "alignof") [EA$ lit$ anonTy$ alignTyOf tts]

alignLength :: Length -> Align -> Length
alignLength len (Mask [])  = len
alignLength 0   _          = 0
alignLength len tts = fromExpr$ call (lit "size_t_alignof") [EA$ toExpr len, EA$ lit $ anonTy $ alignTyOf tts]


atomVAsMemW :: AtomV -> (forall a. Maybe (ELValue a) -> α) -> α
atomVAsMemW atomV κ = case atomV of
  AMem  l -> κ (Just l)
  ALen  e -> $(err') "Cannot write to length"
  AExpr e -> $(err') "Cannot write to RValue"
  AVoid -> κ Nothing


atomAsMemW :: (SingI b) => Atom b -> (forall a. Maybe (ELValue a) -> α) -> α
atomAsMemW Atom{atomV} κ = atomVAsMemW atomV κ 

atomAsMem :: (MonadFresh m, SingI b) => Atom b -> (forall a. Maybe (ELValue a) -> m Code) -> m Code
atomAsMem a@Atom{..} κ = case atomV of
  AMem  l -> κ (Just l)
  ALen  e -> atomAsMem a{atomV = AExpr$ toExpr e} κ
  AExpr e ->
    case cTy atomTy of
      Just ty -> cWithLocal (tConst ty) "l" $ \l ->
        statementsA [stmExprA$ l =: e
                    ,κ (Just l)]
      Nothing -> atomAsVoid a $ κ Nothing
  AVoid -> κ Nothing

-- ^ Reads memory as an expression
atomAsExpr :: Atom b -> (forall a. Expr a -> r) -> r
atomAsExpr a@Atom{atomV} κ = atomVAsExpr atomV κ

atomVAsExpr :: AtomV -> (forall a. Expr a -> r) -> r
atomVAsExpr atomV κ = case atomV of
  AExpr e    -> κ e
  ALen  e    -> κ$ toExpr e
  AMem  l    -> κ$ toExpr l 
  AVoid      -> κ$ toExpr (star eNULL)

atomAsLength :: Atom True -> Length
atomAsLength a@Atom{..} | ARATy ASize <- atomTy = atomVAsLength atomV

atomVAsLength :: AtomV -> Length
atomVAsLength atomV = case atomV of
    ALen  e       -> e
    _             -> atomVAsExpr atomV $ \e -> fromExpr (castExpr e :: Expr Integer)

atomAsVoid :: (Applicative m) => Atom b -> m Code -> m Code
atomAsVoid a@Atom{..} = atomVAsVoid atomV

atomVAsVoid :: (Applicative m) => AtomV -> m Code -> m Code
atomVAsVoid atomV κ = case atomV of
  ALen _ -> κ
  AExpr e -> statementsA [stmExprA e, κ]
  AMem  l -> κ
  AVoid   -> κ
