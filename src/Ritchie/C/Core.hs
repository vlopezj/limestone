{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE IncoherentInstances #-}
module Ritchie.C.Core(module Ritchie.C.Core
                     ,module Ritchie.C.Declare
                     ) where

import Ritchie.C.Pretty
import Ritchie.C.Declare
import Ritchie.Polar
import Control.Monad.Reader
import Control.Monad.Except.Extra
import Fresh
import Ritchie.Term
import LL hiding (Pos, Neg, Mem, CtxElem)
import Pol.NIA
import Data.Map
import Data.Default
import Data.Functor.Identity
import Control.Applicative
import Control.Lens hiding (As,(<.))
import Control.Monad.Reader
import Control.Monad.Writer hiding (Dual)
import Data.Map as Map
import Control.Monad.Except.Extra
import Ritchie.Stash
import Development.Placeholders
import Data.Monoid (Monoid(..), (<>))
import Tagged
import Ritchie.Euclidean
import qualified Data.Traversable as T
import Data.Generics.Geniplate


type EnvM = Reader CompileEnv
type TyEnvM = Reader TyEnv

type TVoid = TVoidM Code
type TVoidM = DeclareT (ExceptT String FreshM)

type BTerm = BTerm' Atom TVoid
type BaseTerm = BaseTerm' Atom TVoid
type Term = Term' Atom TVoid
type TermP = TermP' Atom TVoid
type CoTermP = CoTermP' Atom TVoid

type LangM = ReaderT CompileEnv TVoidM
type Lang = LangM Code

type CtxElem = CtxElem' Atom TVoid


type AtomTyP = AtomTy

type AtomTyA = AS AtomTy 

-- | Eventually extensible with more dimensions and sequentiality
data EnvFlags = DebugAST | DebugBindings deriving (Show, Eq, Ord)

-- | Communication modes form a lattice
--   Importing a lattice seems overkill for this case; so we'll content
--   ourselves with a Monoid instance.
data AtomComm  = Nilplex  -- ^ No comunication (e.g. a unit type) 
               | SimplexRead -- ^ The consumer reads from memory
               | SimplexWrite -- ^ The consumer writes to memory 
               | Duplex -- ^ Control flow exchange
                 deriving (Show, Eq, Ord)

instance Monoid AtomComm where
  mempty = Nilplex
  Nilplex `mappend` a           = a
  a       `mappend` Nilplex     = a
  a       `mappend` b | a == b  = a
  _       `mappend` _           = Duplex

instance Dual AtomComm where
  dual Nilplex = Nilplex
  dual SimplexRead = SimplexWrite
  dual SimplexWrite = SimplexRead
  dual Duplex = Duplex

data TyEnv = TyEnv {
  tyEnvSizes :: [(Id, Length)]
}

instance Default TyEnv where
  def = TyEnv def

data NodeType = Stack | AllocDynamic | Stored | StoredEager | AllocDeferred deriving (Show,Eq)

data RichAtomTy' r (b :: Bool) where
  RATy :: (SingI p, SingI b, SingI (Not b), IsPos p ~ b, PolProp p) =>
                { tyAPol    :: SPol p
                , tyATy     :: AtomTy' r b
                , tyComm    :: AtomComm
                , tyByRef   :: Bool
                , tyIsRet   :: Bool
                , tyCTy     :: Maybe CType
                , tyCFunTy  :: Either String ([Maybe CType],Maybe CType)
                , tyCTyNode :: Either String CType
                , tyRev :: NoShow (Maybe (RichAtomTy' r (Not b)))
                , tyTag     :: String
                , tyNode    :: NodeType
                } -> RichAtomTy' r b

pattern ARATy x <- A RATy{tyATy=x}

instance DPos RichAtomTy where dposP (A x) κ = dposP x κ
instance DPos (RichAtomTy' r) where dposP (RATy{tyAPol}) κ = revIsConvolution tyAPol$ κ tyAPol

type RichAtomTy = ATy RichAtomTy'
type RichAtomTyA = AS RichAtomTy

deriving instance (SingI b, ShowDPos r) => (Show (RichAtomTy' r b))
instance (ShowDPos r) => (ShowDPos (RichAtomTy' r)) where

deriving instance (SingI b) => Show (RichAtomTy b)
instance (ShowDPos RichAtomTy) where

type EnvLength = ([Id], [(Id, Length)] -> Length)

data VecLength = VecZero
               | VecOne
               | Static    Integer    -- ^ stack allocated
               | Dependent  EnvLength     -- ^ heap allocated, by consumer

pattern Closed x <- ([], ($ []) -> x)
pClosed x = ([], \_ -> x)

pattern Dynamic len <- Dependent (Closed len)

deriving instance Show VecLength

data AS (f :: k -> *) where
  AS :: forall (s :: k) (f :: k -> *). (SingI s) => f s -> AS f 

-- | AtomTy' is an intermediate representation for types.
--   For a given linear type and c representation, there's a single atom type.
data AtomTy' :: ((Bool -> *) -> Bool -> *) where
  -- Memory tensor/par (both read or write)
  ATuple :: (SingI b, SingI b₁, SingI b₂) => r b₁ -> r b₂ -> AtomTy' r b
  -- Memory sum (read)/with  (write)
  ASum   :: (SingI b, SingI b₁, SingI b₂) => r b₁ -> r b₂ -> AtomTy' r b 
  -- Boolean or its dual
  ABool  :: (SingI b) => AtomTy' r b 
  -- Void
  AUnit  :: (SingI b, IsPos p ~ b) => SPol p -> AtomTy' r b
  -- Vector
  AVec  :: (SingI b, IsPos p ~ b, SingI b₁) => SPol p -> VecLength  -> r b₁ -> AtomTy' r b
  AVecPacked :: (SingI i, SingI b, IsPos p ~ b, SingI b₁, SingI b₂) => 
    SBool i -> SPol p -> EnvLength -> r b₁ -> r b₂ -> AtomTy' r b

  AIdx  :: (IsPos p ~ True, SingI b₁) => SPol p -> EnvLength -> r b₁ -> AtomTy' r True
 
  -- Primitive values, read (pos), write (neg)
  APrim  :: SingI b => SBool b -> String -> AtomTy' r b

  -- Does nothing to the type
  ARet  :: r b -> AtomTy' r b
  ANeg  :: (SingI b) => (Bool,r b) -> AtomTy' r (Not b)
  ALol  :: (SingI b₁, SingI b₂) => (Bool,r b₁) -> r b₂ -> AtomTy' r False

  AWith :: (SingI b₁, SingI b₂) => r b₁ -> r b₂ -> AtomTy' r False

  -- Size polymorphism
  APiSize     :: (SingI b₁) =>                     Id -> r b₁ -> AtomTy' r False
  ASigmaSize  :: (SingI b, SingI b₁) => SBool b -> Id -> r b₁ -> AtomTy' r b
  
  -- Intuitionistic
  ASize  :: AtomTy' r True
  AProp  :: AtomTy' r True

  -- Polarity casting
  ABox   :: (SingI b, SingI b₁, b ~ IsPos p) => SPol p -> r b₁ -> AtomTy' r b

pattern AVec0 p x = AVec  p VecZero x
pattern AVec1 p x = AVec  p VecOne  x

-- Recursors for lengths
instance (Applicative t) => TransformBiM t EnvLength VecLength where
  transformBiM :: (EnvLength -> t EnvLength) -> VecLength -> t (VecLength)
  transformBiM f = \case
    VecZero -> pure VecZero
    VecOne -> pure VecOne
    Static i -> pure$ Static i
    Dependent x -> Dependent <$> f x

instance (Applicative t) => TransformBiM t EnvLength (AtomTy' r b) where
  transformBiM :: (EnvLength -> t EnvLength) -> AtomTy' r b -> t (AtomTy' r b)
  transformBiM f = \case
    AVecPacked b p len r s -> (\len -> AVecPacked b p len r s) <$> f len
    AVec p len r -> (\len -> AVec p len r) <$> transformBiM f len
    AIdx p len r -> (\len -> AIdx p len r) <$> f len
    x            -> pure x

instance (Applicative t,Monad t,SingI b) => TransformBiM t EnvLength (RichAtomTy b) where
  transformBiM :: (EnvLength -> t EnvLength) -> RichAtomTy b -> t (RichAtomTy b)
  transformBiM f (A RATy{tyATy=tyATy',tyRev=tyRev',..}) = (\tyATy tyRev -> A RATy{..}) <$>
                                ((traverseAtomTy' (transformBiM f) tyATy') >>= transformBiM f) <*>
                                traverse (traverse (fmap (\(A r) -> r) . transformBiM f . A)) tyRev' 
                      

instance (Applicative t,Monad t, SingI b) => TransformBiM t EnvLength (Atom b) where
  transformBiM f (Atom{atomV,atomTy}) = Atom atomV <$> transformBiM f atomTy

instance DPos r => DPos (AtomTy' r) where 
  dposP :: forall b α. AtomTy' r b -> (forall p. (PolProp p, IsPos p ~ b) => SPol p -> α) -> α
  dposP t = case t of
    -- These may represent both sequences and +/- types
    AUnit p          -> \κ -> revIsConvolution p$ κ p
    AVec p _ _       -> \κ -> revIsConvolution p$ κ p
    AVecPacked b p _ _ _ -> \κ -> revIsConvolution p$ κ p

    ATuple{}         -> dposP (sing :: SBool b)
    ASum{}           -> dposP (sing :: SBool b)
    ABool{}          -> dposP (sing :: SBool b)
    APrim{}          -> dposP (sing :: SBool b)
    ALol{}           -> dposP SFalse
    ASigmaSize{}     -> dposP (sing :: SBool b)
    ARet x           -> dposP x
    ANeg (_,y)       -> \κ -> dposP y (\p -> revIsConvolution p$ κ$ ddual p)
    APiSize{}        -> dposP SFalse
    AWith{}          -> dposP SFalse
    ASize            -> dposP STrue
    AProp            -> dposP STrue
    AIdx{}           -> dposP STrue
    ABox p _         -> \κ -> revIsConvolution p$ κ p

instance DPos AtomTy where dposP (A t) = dposP t

instance DPos (Atom) where
  dposP Atom{..} = dposP atomTy

deriving instance (SingI b, ShowDPos r) => (Show (AtomTy' r b))
instance (ShowDPos r) => (ShowDPos (AtomTy' r)) where

traverseAtomTy' :: forall r₁ r₂ b t. (Applicative t, SingI b) =>
                                                     (forall b. SingI b => r₁ b -> t (r₂ b)) ->
                                                AtomTy' r₁ b -> t (AtomTy' r₂ b)
traverseAtomTy' f t = go t
  where
    go :: forall b. (SingI b) => AtomTy' r₁ b -> t (AtomTy' r₂ b)
    go t = case t of
      ATuple x y -> ATuple <$> f x <*> f y
      ABool      -> pure$ ABool
      ASum x y -> ASum <$> f x <*> f y
      AUnit p    -> pure$ AUnit p
      AVec p len x -> AVec p len <$> f x
      AVecPacked b p len x y -> AVecPacked b p len <$> f x <*> f y
      AIdx p len x -> AIdx p len <$> f x
      APrim b s -> pure$ APrim b s
      ARet x -> ARet <$> f x
      ANeg (b,x) -> (\x -> ANeg (b,x)) <$> f x
      ALol (b,x) y -> (\x y -> ALol (b,x) y) <$> f x <*> f y
      AWith x y -> AWith <$> f x <*> f y

      -- Sigma types
      APiSize v x -> APiSize v <$> f x
      ASigmaSize b v x -> ASigmaSize b v <$> f x

      ASize -> pure ASize
      AProp -> pure AProp

      ABox b x -> ABox b <$> f x

fmapAtomTy' :: forall r₁ r₂ b. (SingI b) =>
               (forall b. SingI b => r₁ b -> r₂ b) ->
               AtomTy' r₁ b -> AtomTy' r₂ b
fmapAtomTy' f = runIdentity . traverseAtomTy' (Identity . f)


deriving instance SingI b => Show (AtomTy' AtomTy b)

data AtomV where
  --  Memory can be read any number of times
  --  Reading memory has no side effects
  AMem :: ELValue a -> AtomV 

  -- Term with no valid C representation. Unit values.
  AVoid :: AtomV

  -- Expressions:
  --     - Can only be read
  --     - Must be evaluated once and only once, as it may have side effects.
  AExpr :: Expr a    -> AtomV 

  -- Length expressions:
  --     - Can be evaluated any number of times, because they are cheap
  --       (optimizing them is left as a job for the C compiler)
  ALen  :: Length    -> AtomV

-- ^ AtomTy is the internal compiler type 
--
--   Internal compiler types
-- 
--   For each linear type, there are 0,1, or many ways to represent it.
--   Each AtomTy translates to a single linear type
data ATy (f :: (Bool -> *) -> Bool -> *) b where A :: (SingI b) => f (ATy f) b -> ATy f b

type AtomTy = ATy AtomTy'

deriving instance (SingI b) => Show (AtomTy b)
--instance (ShowDPos AtomTy) where

deriving instance Show AtomV
data Atom :: (Bool -> *) where
  Atom :: { atomV   :: AtomV
          , atomTy  :: RichAtomTy b
          } -> Atom b

instance Atoms Atom where
  type AIndex Atom = Length
  type AKind  Atom = Type'' () Length Id Id
  type AType  Atom = RichAtomTy
  type EName  Atom = Id
  type ERef   Atom = Id

deriving instance Show (Atom True)
deriving instance Show (Atom False)
instance (ShowDPos Atom) where

deriving instance (SingI b) => Show (Atom b)

type Binding = Binding' Atom TVoid 
data BindingAny where BA :: SingP p => Binding p -> BindingAny
deriving instance Show (BindingAny)

data CompileEnv =
  CompileEnv {
    -- Ids are unique — no need for scoping
    -- Also, only linear values are supported ATM
     _envValues     :: Map Id (BindingAny)
   --,envSyncBoundary :: Map IdSyncBoundary LValue
   -- TODO: use MetaVar for primitive types
   , _envTypeVars   :: Map Id (IdTyPol Pos)
--    ,_envPrimTypes  :: Map MetaVar CType
   , _envSizeValues :: Map Id   Length
   , _envFlags      :: [EnvFlags]
   , _envPredicates :: LengthPredicates
   , _envLocalContext :: Bool  -- ^ True if we are in the context of a sequent, and not a derivation
   } deriving (Show)

newtype MonadLengthCompileEnvWrapper m α = MonadLengthCompileEnvWrapper (m α) deriving (Functor, Applicative, Monad)

instance Default CompileEnv where
  def = CompileEnv def def def def def False

lengthVec :: VecLength -> Maybe Length
lengthVec (VecZero) = Just 0
lengthVec (VecOne) = Just 1
lengthVec (Static i) = Just$ fromInteger i
lengthVec (Dynamic len) = Just len
lengthVec (Dependent len) = lengthEnv len

lengthEnv ([],len) = Just$ len []
lengthEnv _        = Nothing

$(makeLenses ''CompileEnv)

instance Stash EnvM TVoid Lang where
  stash :: EnvM TVoid -> Lang
  stash m = do
    env <- ask
    let r = flip runReader env m
    lift r

  unstash :: Lang -> EnvM TVoid
  unstash m = do
    env <- ask
    let m₁ = flip runReaderT env m
    return m₁

instance Internalize TVoid TVoid where
  internalize = id

instance Defer TVoid where
  defer = internalize . throwError

instance NbEEnv Atom TVoid EnvM where

  bLookupCtx :: ERef Atom {- ~ Id -} -> EnvM (Maybe CtxElem)
  bLookupCtx ident = do
    b  <- view (envValues     . at ident) 
    sz <- view (envSizeValues . at ident) 
    ty <- view (envTypeVars   . at ident) 
    return$ case (b,sz,ty) of
      (Just (BA b),_,_) -> Just$ CtxLinear b
      (_,Just len,_)    -> Just$ CtxSize   len
      (_,_,Just ty) ->
        revIsConvolution ty$
        Just$ CtxType   ty
      _                 -> Nothing
 
  bBindCtx :: EName Atom {- ~ Id -} -> CtxElem -> EnvM α -> EnvM α
  bBindCtx ident e =
    case e of
      CtxLinear b -> local (envValues     %~ Map.insert ident (BA b))
      CtxSize  sz -> local (envSizeValues %~ Map.insert ident sz)
      CtxType  ty -> local (envTypeVars   %~ Map.insert ident (ty))

  indexN :: ALength Atom -> AIndex Atom -> EnvM α -> EnvM α
  indexN pieces ix m = do
    bs <- view envValues >>= T.mapM (\case BA a -> BA <$> sliceB pieces ix a)
    local (envValues .~ bs) m
    where
      sliceB :: (SingI p) => Length -> CIndex -> Binding p -> EnvM (Binding p)
      sliceB 1      0 = return
      sliceB pieces i = \(asBindingN -> BindingN{bSz,bPermute,bTerm}) -> do
        len' <- bSz // pieces
        return$ BindingN len' (\j -> bPermute (len' * i + j)) bTerm
  
  capture :: (α -> EnvM β) -> EnvM (α -> β)
  capture f = do
    env <- ask
    return $ \α -> 
      let res = flip runReader env $ f α in
      res

  assertSize p = local (envPredicates %~ lengthPredAdd p id)

  ctxLocal = local (envLocalContext .~ True)

  cmpSize (a,op,b) = solveCond <$> lenCmpOp op a b

data Builtin where
  NumUnOp :: (forall a b.   UnOp a b) -> Builtin
  NumBiOp :: (forall a b c. BiOp a b c) -> Builtin
  CmpBiOp :: (forall a b c. BiOp a b c) -> Builtin

builtin :: [(String, Builtin)]
builtin = [("+" ,NumBiOp (+.)),
           ("-" ,NumBiOp (-.)),
           ("*" ,NumBiOp (*.)),
           ("/" ,NumBiOp (/.)),
           ("%" ,NumBiOp (%.)),
           ("<=",CmpBiOp (<=.)),
           ("<" ,CmpBiOp (<. )),
           (">=",CmpBiOp (>=.)),
           (">" ,CmpBiOp (>. )),
           ("==",CmpBiOp (==.)),
           ("neg", NumUnOp Ritchie.C.Pretty.neg)]

$(makeWrapped ''MonadLengthCompileEnvWrapper)

instance (Applicative m, Monad m) => MonadLength (ReaderT CompileEnv m) where
  type MLP (ReaderT CompileEnv m) = MonadLengthCompileEnvWrapper (ReaderT CompileEnv m)
  monadLengthWrap = _Wrapped' 

instance (Monad m) => MonadReader LengthPredicates (MonadLengthCompileEnvWrapper (ReaderT CompileEnv m)) where
  ask = MonadLengthCompileEnvWrapper$ view envPredicates
  local f (MonadLengthCompileEnvWrapper m) = MonadLengthCompileEnvWrapper$ local (envPredicates %~ f) m
  

