{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ImpredicativeTypes #-}
module Ritchie.C.Type(comm,toRichAtomTy,arrayToRichAtomTy{-,richSetRev-}
                     ,richAtomTyRev,richAtomTyPosToNeg 
                     ,cTy
                     ,cFunTy,cFunTyArgs,cFunTyRet
                     ,aUnit
                     ,aSize
                     ,aArray
                     ,isTyRead
                     ,isFunctionType
                     ,translateDataType
                     ,isByRef
                     ,isVoidTy,isTyWrite
                     ,tyCoercibleTo
                     ,tyBindVarSize
                     ,nodeIsWrapped
                     ,nodeHasImmediates
                     ,cFunTyArgNames) where

import Ritchie.C.Pretty
import Ritchie.C.Core
import Type
import FileLocation
import Data.Singletons
import Ritchie.Polar
import Ritchie.Term
import Control.Applicative
import Data.Maybe
import qualified Data.List.NonEmpty as NE
import Data.String
import Data.Monoid
import Tagged
import Data.Generics.Is
import Op
import Control.Lens.Zoom
import Control.Monad.Writer
import Data.List
import Data.Functor.Identity
import ContM
import Development.Placeholders
import Id
import Data.Generics.Geniplate

-- | Function types whose associated function is only called once.
isTailType :: AtomTy' (ATy RichAtomTy') b -> Bool
isTailType ra = case ra of
    ANeg _ -> True
    ALol{} -> True
    APiSize{} -> True
    ARet{} -> True
    ABox _ (ARATy x)  -> isTailType x
    AVec1 _ (ARATy x) -> isTailType x
    AIdx{} -> False --  It's not a valid tail, since it may cause the arguments
                    --  before it to be called more than once.
    AWith{} -> True
    _ -> False

-- | Types which translate into functions 
isFunctionType ra = case ra of
  _ | isTailType ra -> True
  AIdx{} -> True
  _ -> False

asFunctionTail :: (SingI b) => RichAtomTy b -> RichAtomTy (Not b) -> RichAtomTy b
asFunctionTail ra@(A RATy{tyATy}) ra' | isTailType tyATy = ra
asFunctionTail ra@(A RATy{tyAPol,tyIsRet}) ra' | tyIsRet  = aRet ra
asFunctionTail ra@(A RATy{tyAPol}) ra'@(A RATy{tyByRef}) = notIsConvolution ra$ aNeg ra'

comm :: RichAtomTy b -> AtomComm
comm (A RATy{tyComm}) = tyComm

isCommWrite = \case
  Nilplex      -> True
  SimplexRead  -> False
  SimplexWrite -> True
  Duplex       -> False
 

isTyRead (A a) = case tyComm a of
  Nilplex      -> True
  SimplexRead  -> True
  SimplexWrite -> False
  Duplex       -> False

isTyWrite (A a) = isCommWrite $ tyComm a

isTySimplex (A a) = isSimplex$ tyComm a

isSimplex x = case x of
  Nilplex      -> True
  SimplexRead  -> True
  SimplexWrite -> True
  Duplex       -> False

richSetRev :: RichAtomTy (Not b) -> RichAtomTy b -> RichAtomTy b
richSetRev (A tyr) (A RATy{..}) = A RATy{tyRev = NoShow (Just tyr),..}


isByRef (A x) = tyByRef x

{-
ri :: (SingI b) => AtomTy' RichAtomTy b -> DeclareT TyEnvM (Maybe CType -> Maybe Init)
ri a = lift$ capture (\cty -> fmap Just $ richTypeInit cty a)

-- ^ Data
initUnit = Init{  lengthSelf = 0, alignMaskSelf = Mask []
                , lengthChildren = 0, alignMaskChildren = Mask []
                , initF = Noop
                , initFChildren = Noop
                }
-}
-- | Limit beyond which arrays are heap- instead of stack- allocated
staticLimit :: (Num a) => a 
staticLimit = 4 

wrapNode :: (MonadDeclare m) => String -> Maybe CType -> m CType
wrapNode tyTag cty = do
  let tname = "node" `mkTag` [tyTag] 
  let rTy = tStruct$ catMaybes [Just ("mb", tPointer tChar)
                                    ,("fill",) <$> cty
                                    ]
  declareTypeDef tname rTy

stackRich :: forall p b b₁ f. (b ~ IsPos p) =>
        SPol p -> AtomTy' RichAtomTy b -> AtomComm -> Bool -> Bool -> RichAtomTy b
stackRich p a c b₁ b₂ = unsafeIgnoreDeclare$ rich p a c b₁ b₂

rich :: forall p b b₁ f m. (MonadDeclare m, b ~ IsPos p) =>
        SPol p -> AtomTy' RichAtomTy b -> AtomComm -> Bool -> Bool -> m (RichAtomTy b)
rich                                                       tyAPol tyATy tyComm  tyIsRet      tyByRef                     =
--                                                         ====== ===== ======  =======      =======                     
   revIsConvolution tyAPol$ do
     let tyTag = richTyTag tyATy
     let tyNode = richTyNode tyATy 
     tyCTy <- case runWriterT (richCTy tyATy) of
       Nothing -> return Nothing
       Just (c,w) -> do tell w; return$ Just c
     tyCFunTy <- case isFunctionType tyATy of
       False -> return$ Left$ "This is not a function type: " ++ show tyATy
       True -> Right `liftM` richCFunTy tyATy 
     tyCTyNode <- case tyNode of
       Stack -> return $ Left$ "Stack-based nodes cannot be memory managed: " ++ show tyATy
       _     -> Right `liftM` wrapNode tyTag tyCTy
     return$ A (RATy{tyAPol,tyATy,tyComm,tyByRef, tyIsRet,tyCTy,tyTag,tyNode,tyCFunTy,tyCTyNode,tyRev = NoShow Nothing})

vecSpec :: VecLength -> RichAtomTy b -> (Bool, Bool)
vecSpec len x = case len of
  VecZero -> (False, False)
  VecOne  -> (isRetTy x, isByRef x)
  Static{} -> (False, isByRef x)
  Dependent{} -> (False, False) 

vecLength :: EnvLength -> VecLength
vecLength len = case len of
  ([], asScalar . ($ []) -> Just 0) -> VecZero
  ([], asScalar . ($ []) -> Just 1) -> VecOne
  ([], asScalar . ($ []) -> Just k) | k <= staticLimit -> Static k
  _ -> Dependent len

aTuplePos comm x y   = let a = ATuple x y in           rich SPos a comm         False       (isByRef x || isByRef y) 
aTupleNeg comm x y   = let a = ATuple x y in           rich SNeg a comm         False       (isByRef x || isByRef y) 
aArray' :: (SingI b, MonadDeclare m) => AtomComm -> SPol p -> VecLength -> RichAtomTy b -> m (RichAtomTy (IsPos p))
aArray' comm p len x = revIsConvolution p$ case vecSpec len x of
  (isRetTy, isByRef) -> rich p (AVec p len x) comm isRetTy isByRef

aArrayPacked' :: (SingI i, SingI b₁, SingI b₂, MonadDeclare m) => SBool i -> AtomComm -> SPol p -> EnvLength ->
                 RichAtomTy b₁ -> RichAtomTy b₂ ->
                 m (RichAtomTy (IsPos p))
aArrayPacked' b comm p len x y = revIsConvolution p$ rich p (AVecPacked b p len x y) comm False ($(isP [p| SFalse |]) b)

aPrimPos s           = let a = APrim STrue  s in       rich SPos a SimplexRead  True        False                    
aPrimNeg s           = let a = APrim SFalse s in       rich SNeg a SimplexWrite False       True                     
aSumPos x y          = let a = ASum x y in        rich SPos a (comm x <> comm y)  False       (isByRef x || isByRef y) 
aSumNeg x y          = let a = ASum x y in        rich SNeg a (comm x <> comm y) False       True                     
aBoolPos              = let a = ABool in          stackRich SPos a SimplexRead  True       False 
aBoolNeg              = let a = ABool in          stackRich SNeg a SimplexWrite False       True                     
aSigmaSizePos v x      = let a = ASigmaSize STrue v x in   rich SPos a SimplexRead  False       (isByRef $ x)  
aSigmaSizeNeg v x      = let a = ASigmaSize SFalse v x in  rich SNeg a SimplexWrite False       True                     

aLol x y             = let a = ALol (isByRef x,x) y in rich SNeg a Duplex       False       False                       
aNeg x@(A RATy{tyAPol=p}) = let  p' = ddual p in
  revIsConvolution p$  let a = ANeg (isByRef x, x) in  stackRich p'   a Duplex       False       False                      
aRet ra@(A RATy{tyAPol=p}) =
                       let a = ARet ra in              stackRich p    a Duplex       False       False                     

-- ^ Negative
aIdx p len x =         let a = AIdx p len x in         stackRich p    a Duplex       False       False                    

aWith x y =            let a = AWith x y in            rich SNeg a Duplex       False       False                   
aPiSize v x =         let a = APiSize v x in           rich SNeg a Duplex       False       False                  

-- ^ Intuitionistic
aSize, aProp :: RichAtomTy True
aSize =                let a = ASize in                stackRich SPos a SimplexRead  False False 
aProp =                let a = AProp in                stackRich SPos a Nilplex      False False 

-- ^ Data: NilPlex, Simplex
aUnit :: SPol p -> RichAtomTy (IsPos p)
aUnit p             = revIsConvolution p$ 
  let p' = ddual p
      u  =             let a = AUnit p in              stackRich p    a     Nilplex  True False 
      u' =             let a = AUnit p' in             stackRich p'   a     Nilplex  True False 
  in
  richSetRev u' u

aBox :: forall b p. (SingI b) => SPol p -> RichAtomTy b -> DeclareT EnvM (RichAtomTy (IsPos p))
aBox p x = revIsConvolution p $ notIsConvolution (sing :: SBool b)$ do
    u  <- richBox p x
    case richAtomTyRev x of
      Nothing  -> return u
      Just rev -> richSetRev' (richBox (ddual p) rev) u 
  where
    richBox :: forall b p. (SingI b) => SPol p -> RichAtomTy b -> DeclareT EnvM (RichAtomTy (IsPos p))
    richBox p x = revIsConvolution p $ 
      let a = ABox p x in
      rich p a (commTy x) (isRetTy x) (isByRef x) 

unsafeIgnoreDeclareT :: (Monad m) => DeclareT m a -> m a
unsafeIgnoreDeclareT m = do
  res <- runWriterT m
  case res of
    (a,[]) -> return a

unsafeIgnoreDeclare = runIdentity . unsafeIgnoreDeclareT

-- ^ Par, negation
aNeg :: RichAtomTy b -> RichAtomTy (Not b)
aRet :: RichAtomTy b -> RichAtomTy b 

aArrayPacked b comm p len x y = aArrayPacked' b comm p len x y >>= 
                      richSetRev' (aArrayPacked' (sNot b) (dual comm) (ddual p) len
                                   ($fromJst $ richAtomTyRev x)
                                   ($fromJst $ richAtomTyRev y))

aArray'' comm p len x = aArray' comm p len x  >>= 
                        richSetRev' (aArray' (dual comm) (ddual p) len
                          ($fromJst $ richAtomTyRev x))

aArray comm p len x = aArray'' comm p (vecLength len) x

richSetRev' :: (Monad m) => m (RichAtomTy (Not b)) -> RichAtomTy b -> m (RichAtomTy b)
richSetRev' x y = do
  x' <- x
  return $ richSetRev x' y

newtype UnpackSimplexSum = UnpackSimplexSum {
  getUnpackSimpleSum :: (forall α. (forall b₁ b₂.
                                   (SingI b₁, SingI b₂) => RichAtomTy b₁ -> RichAtomTy b₂ -> α) -> α)
  }

-- Unpack's a sum type (e.g. a boolean) into it's elements, as long as it
-- transfers information in one direction overall.
unpackSimplexSum :: forall b. (SingI b) => RichAtomTy b ->
                    Maybe UnpackSimplexSum
                    --forall α. (forall b₁ b₂. (SingI b₁, SingI b₂) => RichAtomTy b₁ -> RichAtomTy b₂ -> α) -> α)
unpackSimplexSum t = case (t, sing :: SBool b) of
    (ARATy (ABool), STrue) -> let x = aUnit SPos in  Just (UnpackSimplexSum (mk x x))
    (ARATy (ABool), SFalse) -> let x = aUnit SNeg in Just (UnpackSimplexSum (mk x x))
    (ARATy (ASum  x y), _) -> Just (UnpackSimplexSum (mk x y))
    _ -> Nothing
  where
   mk :: (SingI b₁, SingI b₂) => RichAtomTy b₁ -> RichAtomTy b₂ ->
          (forall α. (forall b₁ b₂. (SingI b₁, SingI b₂) => RichAtomTy b₁ -> RichAtomTy b₂ -> α) -> α)
   mk x y = (\f -> f x y)


arrayToRichAtomTy :: SPol p -> EnvLength -> IdTyPol p₁ -> DeclareT EnvM (RichAtomTy (IsPos p))
arrayToRichAtomTy p len a = revIsConvolution p$ revIsConvolution a$ do
  ra <- toRichAtomTy a
  if | isTySimplex ra ->
        case (vecLength len, sIsApo p, unpackSimplexSum ra) of
          (Dependent len, STrue, Just κ) -> getUnpackSimpleSum κ $ \rx ry -> 
             notIsConvolution rx $ notIsConvolution ry $ 
             aArrayPacked (dpos ra) (comm ra) p len rx ry
          (len, _, _) -> aArray'' (comm ra) p len ra
     | otherwise -> do
        ra' <- toRichAtomTy$ ddual a
        case sIsPos p of
          STrue  -> return$ aIdx p len (asFunctionTail ra ra')
          SFalse -> return$ aNeg (aIdx (ddual p) len (asFunctionTail ra' ra))

translateSizeTy :: IdSize -> EnvM EnvLength
translateSizeTy sz = do
  let vars = sizeSymbs sz
  unknownVars <- catMaybes <$> forM vars (\(TVar True ref) ->
    bLookupSize ref >>= \case
      Nothing -> return$ Just ref
      Just _  -> return$ Nothing)
  (unknownVars,) <$> capture (\vs ->
    recurse_ (\(v,vsz) -> bBindCtx v (CtxSize vsz)) vs $
      translateSize sz)

toRichAtomTy :: IdTyPol p -> DeclareT EnvM (RichAtomTy (IsPos p))
toRichAtomTy t = revIsConvolution t $ case t of
    -- Positive types are not
    TyPos (T (a :*: b)) -> tyPol a $ \a -> tyPol b $ \b -> do
      ra <- r a
      rb <- r b
      case comm ra <> comm rb of
        comm | isSimplex comm -> aTuplePos comm ra rb >>= richSetRev' (aTupleNeg (dual comm) (rev ra) (rev rb)) 
        _                     -> do
          rb' <- r$ ddual b
          aNeg <$> aLol ra (asFunctionTail rb' rb)

    TyNeg (T (a :|: b)) -> tyPol a $ \a -> tyPol b $ \b -> do
      ra <- r a
      rb <- r b
      case comm ra <> comm rb of
        comm | isSimplex comm -> aTupleNeg comm  ra rb >>= richSetRev' (aTuplePos (dual comm) (rev ra) (rev rb)) 
        _ -> do
          ra' <- r$ ddual a
          rb' <- r$ ddual b
          aLol ra' (asFunctionTail rb rb')

    TyNeg (T (a `Lollipop` b)) -> tyPol a $ \a -> tyPol b $ \b -> do
      ra  <- r a
      rb  <- r b
      rb' <- r$ ddual b
      aLol ra (asFunctionTail rb rb')

    TyPol p (T (Array _ sz a)) -> tyPol a $ \a -> do
       len <- lift$ translateSizeTy sz
       arrayToRichAtomTy p len a

    TyPol p (T (Unit _)) -> return$ aUnit p
    TyPos (T (Primitive True  s)) -> aPrimPos s >>= richSetRev' (aPrimNeg s) 
    TyNeg (T (Primitive False s)) -> aPrimNeg s >>= richSetRev' (aPrimPos s)

    TyPol p (T (Var _ (TVar b v))) ->  do
      a <- lift$ bLookupType v
      case sIsPos p of
        STrue  | True  <- b -> r a
        SFalse | False <- b -> r (ddual a)

    TyPos (T (T One :+: T One)) -> return aBoolPos >>= richSetRev' (return aBoolNeg) 
    TyPos (T (a :+: b))   -> tyPol a $ \a -> tyPol b $ \b -> do
      ra <- r a
      rb <- r b
      if | isTySimplex ra && isTySimplex rb {- isTyRead ra && isTyRead rb -} -> aSumPos  ra rb >>= richSetRev' (aSumNeg (rev ra) (rev rb)) 
         | otherwise                  -> do
             ra' <- r$ ddual a
             rb' <- r$ ddual b
             aNeg <$> aWith (asFunctionTail ra' ra) (asFunctionTail rb' rb)

    TyNeg (T (T Bot :&: T Bot)) -> return aBoolNeg  >>= richSetRev' (return aBoolPos)
    TyNeg (T (a :&: b))   -> tyPol a $ \a -> tyPol b $ \b -> do
      ra <- r a
      rb <- r b
      if | isTySimplex ra && isTySimplex rb {- isTyWrite ra && isTyWrite rb -} -> aSumNeg ra rb >>= richSetRev' (aSumPos (rev ra) (rev rb))
         | otherwise -> do
           ra' <- r$ ddual a
           rb' <- r$ ddual b
           aWith (asFunctionTail ra ra') (asFunctionTail rb rb')

    TyPos (T (Exists tyVar (T TSize) a)) -> tyPol a $ \a ->  do
      ra <- r a
      if | isTyRead  ra -> aSigmaSizePos tyVar ra >>= richSetRev' (aSigmaSizeNeg tyVar (rev ra)) 
         | otherwise    ->  do
           ra' <- r$ ddual a
           aNeg <$> aPiSize tyVar (asFunctionTail ra' ra)

    TyNeg (T (Forall tyVar (T TSize) a)) -> tyPol a $ \a -> do
      ra <- r a
      if | isTyWrite ra -> aSigmaSizeNeg tyVar ra >>= richSetRev' (aSigmaSizePos tyVar (rev ra)) 
         | otherwise    -> do
           ra' <- r$ ddual a
           aPiSize tyVar (asFunctionTail ra ra')

    TyPos (T TSize) -> return aSize 
    TyPos (T (TProp _)) -> return aProp 

    TyNeg (T (Forall _ (T (TProp (sz₁,op,sz₂))) a)) -> tyPol a $ \a -> do
      len₁ <- lift$ translateSizeTy sz₁ 
      len₂ <- lift$ translateSizeTy sz₂
      r a >>= aBox SNeg

    TyPos (T (Exists _ (T (TProp _)) a)) -> tyPol a $ \a -> r a >>= aBox SPos

    t -> $(err')$ "Unsupported type" ++ show t
  where
    r :: forall p. IdTyPol p -> DeclareT EnvM (RichAtomTy (IsPos p))
    r = toRichAtomTy

    rev x = $fromJst $ richAtomTyRev x 

{-
captureUC :: (UC -> DeclareT EnvM a) -> DeclareT EnvM (UC -> a)
captureUC f = do
  f' <- lift$ capture (runWriterT . f)
  let (_,decls) = f'
  tell decls
  return (\v -> case f' v of
             (a, decls') | decls == decls' -> a)
-}

instance (SingI b) => Erase (AtomTy' RichAtomTy b) (AtomTy' AtomTy b) where
  erase = fmapAtomTy' erase

instance Erase (RichAtomTy b) (AtomTy b) where
  erase (A RATy{tyATy}) = (A$ erase tyATy)

cTy :: RichAtomTy b-> Maybe CType
cTy (A RATy{tyNode,tyCTy,tyCTyNode}) =
  if nodeIsWrapped tyNode then
    case tyCTyNode of
      Right tyCTyNode -> Just tyCTyNode
      Left  msg       -> $(err') msg
    else tyCTy

cFunTy :: RichAtomTy b-> ([Maybe CType],Maybe CType)
cFunTy (A RATy{tyNode,tyCFunTy}) =
  if nodeIsWrapped tyNode then
    $(err') "Function nodes can't be wrapped"
  else
    case tyCFunTy of
      Right tyCFunTy -> tyCFunTy
      Left  msg      -> $(err') msg

cFunTyRet = snd . cFunTy
cFunTyArgs = fst . cFunTy

tagTy :: RichAtomTy b-> String
tagTy (A RATy{tyTag}) = tyTag

escapeTag :: String -> String
escapeTag = (>>= \case
                '_' -> "_U"
                ' ' -> "_S"
                c   -> return c)
tagOpen  = "_b"
tagClose = "_d"
tagSep   = "_s"

mkTag :: String -> [String] -> String
mkTag s l = s <> tagOpen <> intercalate tagSep l <> tagClose

-- | Types with the same tag have the same C representation
richTyTag :: AtomTy' RichAtomTy b -> String
richTyTag (ATuple    a b)                = "tuple" `mkTag` [tagTy a, tagTy b]
richTyTag (ABool)                        = "bool"
richTyTag (ASum a b)                     = "sum"   `mkTag` [tagTy  a, tagTy b]
richTyTag (APrim     _ s)                = "prim"  `mkTag` [escapeTag s]
richTyTag (AUnit _)                      = "void"
richTyTag (AVec0 _ _)                    = "void"
richTyTag (AVec1 _ a)                    = tagTy a
richTyTag (AVec _ (Static j)  a)         = "vec" `mkTag` [ show j , tagTy a ]
richTyTag (AVec _ (Dependent _)  a)      = "vecN" `mkTag` [ tagTy a ]
richTyTag (AVecPacked i _ _ a b)      = "vecSumN" `mkTag` [ tagTy a, tagTy b ]
richTyTag (ASigmaSize _ _ b) = "sigmaSize" `mkTag` [ tagTy b ]
richTyTag (AProp)                        = "void"
richTyTag (ABox _ a)                     = tagTy a
richTyTag (ANeg (ptr,a))                 = "neg" `mkTag` [show ptr, tagTy a]
richTyTag (ALol (ptr,a) b)               = "fun" `mkTag` [show ptr, tagTy a, tagTy b]
richTyTag (ARet a)                       =  "ret" `mkTag` [tagTy a]
richTyTag (AIdx _ _ a)                   = "idx" `mkTag` [tagTy a]
richTyTag (APiSize _ a) = "piSize" `mkTag` [tagTy a]
richTyTag (AWith a b)                    = "with" `mkTag` [tagTy a, tagTy b]
richTyTag (ASize)                        = "size"

-- ^ Note how the CType doesn't depend on the environment
richCTy :: (SingI b) => AtomTy' RichAtomTy b -> DeclareT Maybe CType
richCTy ty = go ty
         where
           r :: (SingI b) => RichAtomTy b -> DeclareT Maybe CType
           r x = lift$ cTy x

           go :: (SingI b) => AtomTy' RichAtomTy b -> DeclareT Maybe CType
           go ty =
             case ty of
                _ | isFunctionType ty -> do
                  (args,ret) <- richCFunTy ty
                  return$ tFunPtr (maybe tVoid id ret) (catMaybes args)

                ATuple a b -> do
                    rTy <- tStruct <$> noEmpty [("pi1",) <$> r a
                                               ,("pi2",) <$> r b]
                    let tname = richTyTag ty 
                    declareTypeDef tname rTy

                ABool -> return tBool
                ASum a b -> do
                    rTy <- tStruct <$> noEmpty [return ("tag", tBool)
                                               ,("val",) <$> tUnion <$> noEmpty [("inl",) <$> r a
                                                                                ,("inr",) <$> r b]]
                    let tname = richTyTag ty 
                    declareTypeDef tname rTy

                ASigmaSize _ _ b -> do
                    rTy <- tStruct <$> noEmpty [return ("len",tSizeT)
                                               ,("val",) <$> r b]

                    let tname = richTyTag ty
                    declareTypeDef tname rTy

                APrim _ s  -> return$ tPrim (fromString s)

                AVec0 _ ty -> lift$ Nothing 
                AVec1 _ ty -> r ty
                AVec _ (Static j)  a   -> do
                  rTy <- tStruct <$> noEmpty [("vec",) . tArray (fromInteger j) <$> r a]
                  let tname = richTyTag ty
                  declareTypeDef tname rTy
                  
                AVec _ (Dependent _) a -> tPointer <$> r a

                AVecPacked _ _ _ a b -> do
                  rTy <- tStruct <$> noEmpty [return ("n", tSizeT)
                                             ,return ("skip", tPointer tSizeT)
                                             ,("inl",) . tPointer <$> r a
                                             ,("inr",) . tPointer <$> r b
                                             ]
                  let tname = richTyTag ty
                  declareTypeDef tname rTy

                AUnit _        -> lift Nothing

                ASize -> return tSizeT
                AProp -> lift Nothing

                ABox _ a -> r a

                _ -> $(err')$ "Can't translate type " ++ show ty ++ " to C"

richCFunTy :: (SingI b, MonadDeclare m) => AtomTy' RichAtomTy b -> m ([Maybe CType],Maybe CType)
richCFunTy t = case t of
  (ANeg (ptr,ty)) -> return ([fmap (if ptr then tPointer else id)$ cTy ty], Nothing)
  (ALol (ptr,tya) tyb) -> do
    let (args,ret) = cFunTy tyb 
    return (fmap (if ptr then tPointer else id) (cTy tya):args  , ret)

  (ARet tya) ->  return ([], cTy tya)
  (AIdx _ _ tya) -> do
    let (args,ret) = cFunTy tya
    return (Just tSizeT:args, ret)

  (APiSize _ tya)  -> do 
     let (args, ret) = cFunTy tya
     return (Just tSizeT:args, ret)

  (AWith tya tyb) -> do
      let (args₁,ret₁) = cFunTy tya
      let (args₂,ret₂) = cFunTy tyb
      let tnameArgs = "argUnion" `mkTag` [tagTy tya, tagTy tyb]
      let tnameRet  = "retUnion" `mkTag` [tagTy tya, tagTy tyb]
      uTy <- catchDeclareMaybe$ tUnion <$> noEmpty 
                                  [("inl",) <$> 
                                    tStruct <$> noEmpty [(argName,) <$> lift arg
                                                        | arg <- args₁
                                                        | argName <- cFunTyArgNames
                                                        ]
                                  ,("inr",) <$> 
                                    tStruct <$> noEmpty [(argName,) <$> lift arg
                                                        | arg <- args₂
                                                        | argName <- cFunTyArgNames
                                                        ]
                                  ]
      rTy <- catchDeclareMaybe$ tUnion <$> noEmpty [("inl",) <$> lift ret₁
                                                   ,("inr",) <$> lift ret₂
                                                   ]
                                 
      args <- case uTy of
        Nothing  -> return [Just tBool, Nothing]
        Just uTy -> do
                      t <- declareTypeDef tnameArgs uTy
                      return [Just tBool, Just t]

      ret  <- case rTy of
        Nothing  -> return Nothing 
        Just rTy -> do
                      t <- declareTypeDef tnameRet rTy
                      return (Just t)

      return (args,ret)

  (ABox  _  tya) -> return$ cFunTy tya
  (AVec1 _  tya) -> return$ cFunTy tya
  ty ->  $(err')$ "Non-tail argument at tail position: " ++ show ty

cFunTyArgNames :: (IsString s) => [s]
cFunTyArgNames = [fromString$ "arg" ++ show (i :: Int) | i <- [1..]]

noEmpty :: [DeclareT Maybe a] -> DeclareT Maybe [a]
noEmpty as' = do
  as <- sequence $ map catchDeclareMaybe as'
  fmap NE.toList $ lift $ NE.nonEmpty $ catMaybes$  as

nodeHasImmediates :: NodeType -> Bool
nodeHasImmediates  = flip elem [AllocDynamic, Stored, StoredEager]

nodeIsWrapped :: NodeType -> Bool
nodeIsWrapped = flip elem [Stored, StoredEager, AllocDeferred]
 
nodeTy :: RichAtomTy b -> NodeType
nodeTy (A RATy{tyNode}) = tyNode 

-- Every free variable must have a "AllocDeferred" between the existential
-- and the place where it appears
richTyNode :: AtomTy' RichAtomTy b -> NodeType
richTyNode = go
  where
    go t | isFunctionType t = Stack
    go (ATuple a b) = if rim a || rim b then
                                AllocDynamic
                              else
                                Stack
    go (ASum   a b) = if rim a || rim b then
                                Stored
                              else
                                Stack
    go (AVec0  _ _) = Stack
    go (AVec1  _ a) = if rim a then AllocDynamic else Stack
    go (AVec _ (Static _) a) = if rim a then AllocDynamic else Stack
    go (AVec _ (Dependent _) a) = AllocDynamic
    go (AVecPacked _ _ _ a b) = AllocDynamic

    go (ABool    ) = Stack
    go (APrim _ s) = Stack
    go (AUnit _) = Stack
    go (ASigmaSize _ tyVar a) = if rim a then
                                  if tyVar `elem` freeTyNodeVars a
                                  then AllocDeferred
                                  else AllocDynamic
                                else Stack
    go (ASize) = Stack
    go (AProp) = Stack
    go (ABox _ a) = if rim a then AllocDynamic else Stack

    rim = nodeHasImmediates . nodeTy

-- ^ Free vars up to deferred terms
freeTyNodeVars :: RichAtomTy b -> [Id]
freeTyNodeVars (A RATy{..}) = case tyNode of
    Stack -> []
    AllocDeferred -> []
    _ -> case tyATy of
        ATuple a b -> r a <> r b
        ASum   a b -> r a <> r b
        AVec1  _ a -> r a
        AVecPacked _ _ (vs,_) a b -> vs ++ r a ++ r b
        AVec _ (Dependent (vs,_)) a -> vs ++ r a
        AVec _ _ a -> r a 
        ASigmaSize _ tyVar a -> [v | v <- r a, v /= tyVar]
        ABox _ a -> r a

  where
    r :: forall b. RichAtomTy b -> [Id]
    r = freeTyNodeVars 

catchDeclareMaybe :: (MonadDeclare m) => DeclareT Maybe a -> m (Maybe a)
catchDeclareMaybe m = case runWriterT m of
  Nothing    -> return Nothing
  Just (a,w) -> tell w >> return (Just a) 

isVoidTy :: RichAtomTy b -> Bool
isVoidTy = isNothing . cTy

isRetTy :: RichAtomTy b -> Bool
isRetTy (A RATy{tyIsRet}) = tyIsRet

commTy :: RichAtomTy b -> AtomComm
commTy (A RATy{tyComm}) = tyComm

toAType_ :: forall p. SingI p => IdTyPol p -> DeclareT EnvM (RichAtomTyA)
toAType_ t = revIsConvolution t$ fmap AS$ toRichAtomTy t


-- | Translate the type in such way that the producer (i.e. the consumer of
--   a dual type can be scheduled strictly before the consumer, and the
--   consumer can use the values any number of times.
translateDataType' :: forall p q. (SingI p, SingI q) => IdDataPol q p -> DeclareT EnvM (RichAtomTy (IsPos p), RichAtomTy (IsPos q))
translateDataType' ty = revIsConvolution (sing :: SPol p) $ revIsConvolution (sing :: SPol q) $ do
  dty   <- toRichAtomTy (dataPolRead  ty)
  dtyw  <- toRichAtomTy (dataPolWrite ty)
  return (dty, dtyw)
 
translateDataType :: (SingI p, SingI q) => IdDataPol q p -> ContMR EnvM TVoid (RichAtomTy (IsPos p), RichAtomTy (IsPos q))
translateDataType ty = declareContM$ translateDataType' ty

{-
translateSized' :: SingI p => IdSizedPol p -> DeclareT TyEnvM (Length, RichAtomTy True)
translateSized' tysz@(ty :^+ sz) = revIsConvolution tysz $ do
  len <- lift$ translateSize sz
  -- OPT: Redundant translation of size
  ty  <- toRichAtomTy (TyPol SPos (T$ ArrayTensor sz (erase ty)))
  return (len, ty)

translateSized :: SingI p => IdSizedPol p -> ContMR TyEnvM TVoid (Length, RichAtomTy True)
translateSized ty = declareContM$ translateSized' ty
-}

-- | All atom types which represent data are reversible
--   Furthermore, it holds that (richAtomTyRev . richAtomTy == richAtomTy . ddual, when both sides
--   are defined.
--   However, this reversal doesn't always make sense, and computing it from scratch
--   would be expensive.
richAtomTyRev :: RichAtomTy b -> Maybe (RichAtomTy (Not b))
richAtomTyRev self@(A RATy{..}) = 
  notIsConvolution self$ fmap (richSetRev self)$ fmap A$ ns $ tyRev 

-- | Changes the first level from positive to negative without altering the
--   representation
richAtomTyPosToNeg :: RichAtomTy b -> Maybe (RichAtomTy (Not b))
richAtomTyPosToNeg (A RATy{..}) = revIsConvolution tyAPol$ do
  tyATy <- atomTyPosToNeg tyATy
  return$ A$ RATy{
    tyAPol = ddual tyAPol
   ,tyATy  
   ,tyComm
   ,tyByRef
   ,tyIsRet
   ,tyCTy
   ,tyCTyNode
   ,tyCFunTy
   ,tyNode
   ,tyRev = fmap (fmap (\x -> case richAtomTyPosToNeg (A x) of Just (A y) -> y)) tyRev
   ,tyTag
   }

atomTyPosToNeg :: forall b α. (SingI b) => AtomTy' α b -> Maybe (AtomTy' α (Not b))
atomTyPosToNeg (ATuple x y)   = notIsConvolution (sing :: SBool b)$ Just$ ATuple x y
atomTyPosToNeg (AVecPacked b p len x y) = revIsConvolution p$ Just$ AVecPacked b (ddual p) len x y
atomTyPosToNeg (AVec p len y) = revIsConvolution p$ Just$ AVec (ddual p) len y
atomTyPosToNeg (AUnit p)      = revIsConvolution p$ Just$ AUnit (ddual p)
atomTyPosToNeg _              = Nothing

tyCoercibleTo :: (SingI a, SingI b) => RichAtomTy a -> RichAtomTy b -> EnvM (Maybe (AtomV -> Atom b))
tyCoercibleTo x y = do
  let ctypesmatch = fmap anonTy (cTy x) == fmap anonTy (cTy y)
  return$ if ctypesmatch
    then Just (\atomV -> Atom{atomV,atomTy=y})
    else Nothing 

tyBindVarSize :: (TransformBiM Identity EnvLength r) => (Id, Length) -> r -> r
tyBindVarSize (x,bind) = runIdentity . transformBiM (\len@(free,f)  ->  pure$
                                                                       if x `elem` free then
                                                                           ([v | v <- free, v /= x ], 
                                                                             f . ((x,bind):))
                                                                       else len :: EnvLength)


