{-# LANGUAGE RankNTypes #-} 
{-# LANGUAGE ConstraintKinds #-} 
{-# LANGUAGE FlexibleContexts #-} 
module Ritchie.C.Declare where

import Ritchie.C.Pretty
import Control.Monad.Writer
import Data.Functor.Identity 

data TopLevel = TopLevelDecl Code
                deriving (Eq, Show)

declareTypeDef :: (MonadDeclare m) => String -> CType -> m CType
declareTypeDef tname ty = do
  tell [TopLevelDecl $ typedef ty (litIdent tname)]
  return$ tPrim tname


type MonadDeclare = MonadWriter [TopLevel]
type DeclareT = WriterT [TopLevel]
type DeclareM = DeclareT Identity

mapDeclareT :: (forall w. m (a,w) -> n (b,w)) -> DeclareT m a -> DeclareT n b 
mapDeclareT f m = mapWriterT f m

declareContM :: (MonadDeclare mr, Monad m) => DeclareT m a -> (a -> m (mr β)) -> m (mr β)
declareContM m κ = do
  (a,w) <- runWriterT m
  (tell w >>) `liftM` κ a
 
