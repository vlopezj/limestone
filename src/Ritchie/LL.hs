{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ConstraintKinds #-}
module Ritchie.LL where

import Development.Placeholders

import Fuel
import Ritchie.Term
import Ritchie.Stash
import qualified Ritchie.Term as Term
import LL hiding ((&), Pos, Neg, eqSize)
import LL.Traversal
import Type.Ctx
import qualified Type.Traversal as TT
import Data.Proxy
import Control.Monad.Reader
import Control.Monad.Except.Extra
import Control.Monad.State
import qualified Data.Map as M
import Data.Map(Map)
import Unsafe.Coerce(unsafeCoerce)
import Data.Either
import Data.Maybe
import Data.List
import Control.Applicative
import Control.Monad
import Control.Arrow((***),(&&&))
import Data.Singletons
import Fresh
import Data.String
import Control.Lens hiding ((<.))
import FileLocation
import Text.Show.Pretty(ppShow)
import Id
import Data.Generics.Is
import qualified Data.Traversable as T
import Ritchie.Polar
import ContM
import Debug.Trace

type ENameA a = SAName (EName a) (ERef a)
type ERefA a = SARef (EName a) (ERef a)

lookupId :: (Eq a) => (a ::: τ) -> [(a,b)] -> b
lookupId (a ::: _) es = let Just x = lookup a es in x

{-
range :: (NbE a r m) => AIndex a -> (AIndex a -> m r) -> m r
range len κ =
  schedule len $ \i ->
    indexN len i $
      κ i
-}

shiftBindM :: forall a r m p. (NbE a r m, SingI p) => EName a ::: SizedP (EName a) (ERef a) p -> m r -> m (Term' a r (Rev p))
shiftBindM name κ = revIsConvolution (sing :: SPol p) $ shiftM $ \t₁ -> bind1 name t₁ $ κ

shiftBindM' :: forall a r m p. (NbE a r m, SingI p) => EName a ::: SizedP (EName a) (ERef a) (Rev p) -> m r -> m (Term' a r p)
shiftBindM' name κ = revIsConvolution (sing :: SPol p)$ shiftBindM name κ

unreachable :: forall a r m β. (Defer r, Internalize r β) => β
unreachable = defer "UNREACHABLE"

splitB :: (SingI p, NbE a r m) => ALength a -> AIndex a -> Binding' a r p -> m (Binding' a r p)
splitB len offset b@(asBindingN -> BindingN{..}) = do
  p <- len    `eqSize` bSz
  q <- offset `eqSize` 0
  if $(isP [p| (Just True, Just True) |]) (p,q) then return b
            else return$ BindingN len (\j -> bPermute (offset + j)) bTerm
 
translate :: forall a r m. (NbE a r m) => SeqnPol (EName a) (ERef a) -> m r
translate (SeqnPol seqn2) = go seqn2
  where
  repol x = forcePolUnsafeTag x
  repolP p x = forcePolUnsafeTagP p x 

  go :: SeqnPol' (EName a) (ERef a) -> m r
  go seq'@(Seqn _ seq) = case seq of
    --_ | trace ("Compiling rule " ++ show (prune seq)) False -> undefined
    Ax {wElim = SAr _ wElim, zElim = SAr _ zElim,..} ->
      lookup1 wElim $ \w ->
      lookup1 zElim $ \z ->
      case sRev (dpol w) =:= dpol z of
        Proved Refl -> unbox w z
        -- Special case for sequences
        Disproved _   -> case (dpol w, dpol z) of
          (SApoP, SApoP) -> posToNeg z $ unbox w
          (p    , q    ) -> deferM $ "Incompatible polarities " ++ show p ++ " and " ++ show q ++ "."

    Cross {zElim = SAr SPos zElim, xIntr = SAn _ xIntr, yIntr = SAn _ yIntr,..} -> 
        lookup1Pos zElim $ \(Tensor v_l v_r) ->
          bind1U xIntr v_l $
            bind1U yIntr v_r $ go cont

    Halt _ -> Term.halt

    Par { xIntr = SAn _ (xIntr@(_ ::: _ :^+ k₁))
        , yIntr = SAn _ (yIntr@(_ ::: _ :^+ k₂))
        , zElim = SAr SNeg zElim
        , ..} ->
      case (k₁,k₂) of
        (1, k) -> 
         lookupN zElim $ \len f ->
           bindN yIntr len (\i -> 
             indexN len i $
               shiftM $ \t₂ ->
                 shiftBindM xIntr (go contL) >>= \t₁ -> revIsConvolution t₁ $
                   mkNeg (f i) >>= \(Up μ) -> 
                     return$ μ (Tensor t₁ t₂))
             $ go contR

        (k, 1) -> $notImplemented
        _ -> deferM "Invalid combination of sizes in parr"

    Plus { zElim = SAr SPos zElim
         , .. } -> do
        lookup1Pos zElim $ \(Sum c v) -> 
          case c of
                Inl -> inl
                Inr -> inr 
          & \Branch{branchName = SAn _ branchName, branchProg = branchProg} ->
              bind1U branchName v $ go branchProg
     
    With { zElim = SAr SNeg zElim
         , xIntr = SAn _    xIntr
         , .. } -> 
      lookupN zElim $ \len f ->
      bindN xIntr len (\i -> shiftM $ \t ->
        mkNeg (f i) >>= \(Up μ) -> 
          return$ μ (Sum choose t)) $ go cont

    SOne{ zElim = SAr SPos zElim, ..} -> lookup1Pos zElim $ \case
      TUnit  -> go cont
      other  -> $(err') $ show zElim ++ ":" ++ show other

    SBot{ zElim = SAr SNeg zElim, ..} -> lookup1Neg zElim $ \(Up μ) -> return$ μ TUnit

    BigTensor{ zElim = SAr SPos zElim@(_ ::: TyPol _ (T (ArrayTensor sz ty)) :^+ 1), xIntr = SAn _ xIntr, ..} ->
      translateSize sz >>= \len ->
        lookup1Pos zElim $ \(Vec _ μ) -> 
          repol xIntr >>= \x -> bindN x len (\i -> pure$ μ i) $ go cont

    BigPar{ zElim = SAr SNeg zElim@(_ ::: TyPol _ (T (ArrayPar sz ty)) :^+ _), ..} -> 
      -- Recover a common polarity for the introduced value in all branches
      tyPol ty $ \typ -> revIsConvolution typ $
        translateSize sz >>= \len ->

        -- Generate code for each branch
          flip recurse branches (\(szBranch, Branch { branchName = SAn _ xIntr, branchProg }) κ -> 
            repolP typ xIntr >>= \xIntr ->
                translateSize szBranch >>= \lenBranch ->
                  κ (lenBranch, \i -> indexN lenBranch i $ shiftBindM xIntr $ go branchProg)) $ \alts ->

                    -- Aggregate all branches into a single term
                    capture (cosliceTermM alts) >>= \f -> do
                      lookup1Neg zElim $ \(Up μ) -> pure$ μ (Vec len f)

    Split{ zElim = SAr p zElim@(_ ::: _ :^+ sz)
         , xIntr = SAn _ xIntr
         , yIntr = SAn _ yIntr
         , ..} -> 
        translateSize sz >>= \len ->
          translateSize splitSz >>= \offset ->
            bLookup zElim >>= \b ->
              repolP p xIntr >>= \x -> splitB offset 0 b >>= \b₁ -> bBind x b₁ $
                repolP p yIntr >>= \y -> splitB (len - offset) offset b >>= \b₂ -> bBind y b₂ $
                  go cont

    Merge{ zElim = SAr p zElim@(_ ::: _ :^+ sz₁)
         , wElim = SAr p₁ wElim@(_ ::: _ :^+ sz₂) 
         , xIntr = SAn p₂ xIntr
         , ..} | Proved Refl <- p₁ =:= p 
               , Proved Refl <- p₂ =:= p ->
           bLookup zElim >>= \b₁ ->
             bLookup wElim >>= \b₂ ->
               indexBN b₁ $ \len₁ μ₁ -> 
                 indexBN b₂ $ \len₂ μ₂ -> 
                   bindN xIntr (len₁ + len₂) (\i -> upM $ \yield -> 
                     cosliceSzM [(len₁, yield . μ₁)
                                ,(len₂, yield . μ₂)] i) $ go cont
    NCut{..} -> 
      translateSize ncut_size >>= \ncut_len ->
        case ncut_cs of
          [] -> 
            case ncut_fanning of
              FanLeft  -> Term.mix [schedule ncut_len $ \i -> go ncut_left
                                   ,go ncut_right]
              FanRight -> Term.mix [go ncut_left
                                   ,schedule ncut_len $ \i -> go ncut_right]

          [(SAn _ xIntr
           ,SAn _ yIntr
           ,TyA tty)] -> 
             revIsConvolution tty $
             repol xIntr >>= \xIntr ->
             repol yIntr >>= \yIntr ->
             case ncut_fanning of
               FanLeft  -> cutL ncut_info ncut_len tty  
                             (\i t -> bind1 xIntr t $ go ncut_left)
                             (\b   -> bBind yIntr b $ go ncut_right)
               FanRight -> cutR ncut_info ncut_len tty  
                             (\b   -> bBind xIntr b $ go ncut_left)
                             (\i t -> bind1 yIntr t $ go ncut_right)
          _  -> $(todo "No support for more than one variable in cut.")

    BigSeq steps -> 
      let refs = nub (steps >>= seqStepRefs)
          ref_pos = [ x | SAr SApoP x <- refs ] :: [ERefP a ApoP]
          ref_neg = [ x | SAr SApoN x <- refs ] :: [ERefP a ApoN]
      in
      (flip recurse ref_pos $ \x κ -> lookup1Pos x $ \p -> κ (x, p)) $ \terms_pos ->
        (flip recurse ref_neg $ \x κ -> lookup1Neg x $ \(Up μ) -> κ (x, μ)) $ \terms_neg -> do
          (\x y f -> recurseFold f x y) (refs `zip` repeat 0) steps (\offsets step -> case step of
            SeqStep{..}  -> \κ -> 
              translateSize seqSz >>= \stepLen -> do
                let new_offsets = [ (ref, (if ref `elem` map fst seqChunk then (+ stepLen) else id) offset)
                                  | (ref, offset) <- offsets] 
                let refs_pos = [ (r, name) | (ref@(SAr SApoP r), name) <- seqChunk ] :: [(ERefP a ApoP, ENameA a)] 
                let refs_neg = [ (r, name) | (ref@(SAr SApoN r), name) <- seqChunk ] :: [(ERefP a ApoN, ENameA a)]
                let bind_pos i = (flip recurse_ refs_pos $ \(r, SAn _ name) κ -> 
                                     lookup (SAr SApoP r) offsets & \(Just offset) ->
                                       lookup r terms_pos & \(Just (Vec _ μ)) ->
                                         repol name >>= \name -> bind1 name (μ (offset + i)) κ)
                κ new_offsets [(stepLen, offsets, bind_pos, refs_neg, go seqProg)]) $ \offsets (concat -> steps) -> 
                case terms_neg of
--                    [] -> Term.mix $ map (\(len, _, bind_pos, [], κ) -> schedule len $ \i -> bind_pos i κ) steps
                    [] -> scheduleCosliceSzM [(len, flip bind_pos κ) | (len, _, bind_pos, [], κ) <- steps]
                    [(ref_neg@(_ ::: TyPol _ (T (Array _ _ tt)) :^+ _), μ)] -> tyPol tt $ \ty_neg -> revIsConvolution ty_neg $ do
                        let ref_negA = SAr SApoN ref_neg 
                        lookup ref_negA offsets & \(Just len_neg) -> do
                            f <- (capture $ \i -> shiftM' $ \term_neg_i ->
                              flip cosliceM i =<< (forM steps (\(len, offsets, bind_pos, refs_neg, κ) -> do
                                let offset_neg = fromJust$ lookup ref_negA offsets
                                case refs_neg of
                                  []  -> return (offset_neg, 1, \_ -> schedule len $ \i -> bind_pos i κ)
                                  [(ref_neg',SAn _ name_neg)] | ref_neg == ref_neg' -> 
                                    repolP ty_neg name_neg >>= \nn ->
                                        return (offset_neg, offset_neg + len, 
                                                  \i -> indexN len i $ bind_pos i $ bind1 nn term_neg_i $ κ))))
                            return$ μ (Vec len_neg f)
                    _ -> deferM "No more than one negative sequence in context"

--    Sync{syncTy = TyA syncTy, xIntr = SAn _ xIntr, ..} ->
    Sync{..} -> do
        flip recurse syncIntr (\(syncSz, TyA syncTy, xIntr, syncUpdate, syncRead) κ -> revIsConvolution syncTy $
              case runExcept$ mkDataSyncPol syncTy of
                Left err -> deferM $ "Can't allocate non-data type " ++ show syncTy ++ ". " ++ err
                Right (DA tyd) -> revIsConvolution2 tyd $ do
                  alloc_sz <- translateSize syncSz
                  case (syncUpdate, syncRead) of
                    (Nothing, Nothing) -> allocate0 alloc_sz tyd $ \dump ->
                       κ (alloc_sz, xIntr, Nothing, Nothing, (PolA dump, Nothing, Nothing))
                    _ -> allocate alloc_sz tyd $ \read update write -> 
                       κ (alloc_sz, xIntr, syncUpdate, syncRead, (PolA write, Just update, Just$ PolA (stashA read))))
        $ \(vs :: [(AIndex a,
              SAName (EName a) (ERef a),
              Maybe
                (SyncUpdate' (Size (ERef a)) (SAName (EName a) (ERef a)) (SARef (EName a) (ERef a))),
              Maybe (SyncRead' (Size (ERef a)) (SAName (EName a) (ERef a)) (SARef (EName a) (ERef a))),
              (PolA (Binding' a r),
               Maybe ((Binding' a r 'Pos -> m r) -> m r),
               Maybe (PolA (StashAWrapped ((->) (AIndex a)) (Binding' a r)))))]) -> do
                let p_init   = 
                      flip recurse_ vs (\(alloc_sz, SAn _ xIntr, _, _, (PolA write,_,_)) κ ->
                        repol xIntr >>= \x -> bBind x write κ) $ go contL

                let p_update = flip fmap syncCont $ \syncCont ->
                      flip recurse_ [(alloc_sz, upd, update) | (alloc_sz, _, Just upd, _, (_,Just update,_)) <- vs]
                        (\(alloc_sz, SyncUpdate{syncWIntr = SAn _ wIntr, ..}, update) κ ->
                            translateSize syncSteps >>= \num_steps ->
                              update $ \(b_upd :: Binding' a r Pos) ->
                                indexBN b_upd $ \_ upd_f -> do
                                  let f i = pure$ TermAP $ TPos $ Vec num_steps (\_ -> upd_f i)
                                  repol wIntr >>= \w -> bindN w alloc_sz f $ κ) $ go syncCont

                let p_read  = flip fmap syncContR $ \syncContR ->
                      flip recurse_ [(alloc_sz, rd, read) | (alloc_sz, _, _, Just rd, (_,_,Just read)) <- vs] (
                        \(alloc_sz, SyncRead{syncYIntr = SAn _ yIntr, ..}, PolA (unstashA -> read)) κ -> 
                           translateSize syncCopies >>= \num_copies ->
                              repol yIntr >>= \y -> 
                                bBind y (read num_copies) κ) $ go syncContR

                Term.mix$  [p_init]
                           ++ maybeToList p_update
                           ++ maybeToList p_read

    SizeCmp{sz1, sz2, bCmp} -> do
       len1 <- translateSize sz1
       len2 <- translateSize sz2
       sizeCmp (len1,len2) [(op, go m) | (op,m) <- bCmp]
   
    What{what, zVars} -> nWhat what =<< (forM zVars $ \case
        ElemLinear (SAr _ (r ::: tysz)) -> (,TyA tysz) <$> bLookupCtxUnsafe r
        ElemIntuit (TyA (TyPol SPos (sizeFromType -> Just sz))) ty@(TyA (TyPol SPos (T TSize))) -> 
          translateSize sz >>= \len -> return (CtxSize len, TyA (TyPol SPos (T TSize :^ 1))))

    Foldmap2 {
      fmMapTrg  = SAn pMapTrg  fmMapTrg
     ,fmUnitTrg = SAn pUnitTrg fmUnitTrg
     ,fmMixL    = SAn pMixL    fmMixL
     ,fmMixR    = SAn pMixR    fmMixR
     ,fmMixTrg  = SAn pMixTrg  fmMixTrg
     ,fmMon     = SAn pMon     fmMon
     ,fmTyM     = TyA (id &&& dpol -> (fmTyM,p))
     ,..
      } | Proved Refl <- pMapTrg  =:= ddual p
        , Proved Refl <- pUnitTrg =:= ddual p
        , Proved Refl <- pMapTrg  =:= ddual p
        , Proved Refl <- pMixL    =:= p
        , Proved Refl <- pMixR    =:= p
        , Proved Refl <- pMixTrg  =:= ddual p
        , Proved Refl <- pMon     =:= p
      -> revIsConvolution p$ do
      len <- translateSize fmSize
      case runExcept$ mkDataCutPol fmTyM of
        Left err -> deferM $ "Can't fold over non-data type " ++ show fmTyM ++ ". " ++ err
        Right tyd -> do 
            κ_map <- capture$ \i' -> shiftM $ \t ->
                         indexN len i' $
                         bind1 fmMapTrg t $ 
                         go fmMap

            allocateRW1 tyd $ \res_read res_write -> do
                let κ_unit = bind1 fmUnitTrg res_write $ go fmUnit

                let κ_mon  = schedule len $ \index ->
                              allocateRW1 tyd $ \buf_read buf_write ->
                                mixSeq [
                                    unbox buf_write res_read -- Read the result into a buffer
                                   ,bind1 fmMixL   buf_read      $
                                    bind1 fmMixR   (κ_map index) $
                                    bind1 fmMixTrg res_write     $ go fmMix
                                   ]

                let κ_cont = bind1 fmMon res_read $ go fmCont

                mixSeq [κ_unit 
                       ,κ_mon 
                       ,κ_cont]
    TUnpack { zElim  = SAr SPos zElim 
            , tyIntr = SAn SPos tyIntr
            , xIntr  = SAn pX   xIntr
            , tyKind = TyA (TyPol SPos tyKind)
            , ..
            } ->
      case tyKind of
        T TSize ->  lookup1Pos zElim $ \(SigmaSize sz a) ->
          bBindCtx (erase tyIntr) (CtxSize sz) $
          bind1 xIntr (forcePolUnsafe pX a) $ 
          go cont
        T (TProp (sz₁,op,sz₂)) -> 
          lookup1Pos zElim $ \(RBox a) ->
          translateSize sz₁ >>= \len₁ ->
          translateSize sz₂ >>= \len₂ ->
          assertSize (len₁, op, len₂) $
          bind1 xIntr (forcePolUnsafe pX a) $ 
          go cont
        k -> $(err') $ "Unsupported kind " ++ show k

    _ -> $(err') $ "Unsupported rule or wrong polarities: " ++ show (prune seq) 
        
translateDeriv :: forall a r m. (NbE a r m) => DerivPol (EName a) (ERef a) -> (a False -> m r) -> m r
translateDeriv (DPol Deriv{..}) κ = do
  let qs = _derivCtx ^. intuit
  let vs = _derivCtx ^. linear
  let fname = _derivName
  translateType 
        (TyPol SNeg
         (ty₀ qs$
          ty₁ vs$
          T Bot)) $ \ty ->

    cutA1 SNeg fname ty
              (\v -> ctxLocal$ go v qs vs $ translate _derivSequent)
              κ
  where
    ty₀ [] t = t
    ty₀ ((n ::: k  :^ 1):rest) t = (T$ Forall n k (ty₀ rest t))

    ty₁ [] t = t
    ty₁ ((n₁ ::: ty :^ sz):[])                     t = (T$ T One `Lollipop` (dual$ T (ArrayTensor sz ty)))
    ty₁ ((n₁ ::: ty₁ :^ sz₁):(n₂ ::: ty₂ :^ sz₂):[]) t = (T$ T (ArrayTensor sz₁ ty₁) `Lollipop` (dual$ T (ArrayTensor sz₂ ty₂)))
    ty₁ ((n₁ ::: ty :^ sz):rest)                   t = (T$ T (ArrayTensor sz ty) `Lollipop` (ty₁ rest t))

    go :: Term' a r Pos -> [EName a ::: Sized' (EName a) (ERef a)] -> [EName a ::: Sized' (EName a) (ERef a)] -> m r -> m r
    go t a b κ =
      mkPos t $ \t ->
      case (a, b, t) of
        ((n ::: (T k) :^ _):as, bs, SigmaSize sz v) ->
          bBindCtx n (CtxSize sz)  $ go (forcePolUnsafeS v) as bs κ
        ((n ::: (T k) :^ _):as, bs, SigmaType typ v) -> revIsConvolution typ$ do
          bBindCtx n (CtxType typ) $ go (forcePolUnsafeS v) as bs κ

        ((n ::: (T k) :^ _):as, bs, RBox v) -> revIsConvolution v$ do
          go (forcePolUnsafeS v) as bs κ

        ([]          , (n₁ ::: tysz₁@(_ :^ sz₁)):[], Tensor u v)  ->
          tyPol tysz₁ $ \tysz₁ -> do
            sz₁ <- translateSize sz₁
            mkPos (forcePolUnsafe SPos u) $ \TUnit ->
              mkBM (forcePolUnsafe SPos v) $ \v ->
                bBind (n₁ ::: tysz₁) (bVecB sz₁ v) $ κ

        ([]          , (n₁ ::: tysz₁@(_ :^ sz₁)):(n₂ ::: tysz₂@(_ :^ sz₂)):[], Tensor u v)  ->
          tyPol tysz₁ $ \tysz₁ ->
            tyPol tysz₂ $ \tysz₂ -> do
                sz₁ <- translateSize sz₁
                sz₂ <- translateSize sz₂
                mkBM (forcePolUnsafe SPos u) $ \u ->
                  mkBM (forcePolUnsafe SPos v) $ \v ->
                    bBind (n₁ ::: tysz₁) (bVecB sz₁ u) $
                      bBind (n₂ ::: tysz₂) (bVecB sz₂ v) $ κ

        ([]          , (n ::: tysz@(_ :^ sz)):bs, Tensor u v)  ->
          tyPol tysz $ \tysz -> do
            sz' <- translateSize sz
            mkBM (forcePolUnsafe SPos u) $ \u ->
              bBind (n ::: tysz) (bVecB sz' u) $
              go (forcePolUnsafeS v) [] bs κ

        ([]          , []                 , TUnit)       -> κ


