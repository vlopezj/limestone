{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
module Ritchie.Polar(module Core
                    ,module Type
                    ) where

import Ritchie.Polar.Core as Core
import Ritchie.Polar.Type as Type
import Data.Singletons as Core
import Data.Singletons.Prelude as Core(Sing(STrue, SFalse),SBool,Not,If,sIf,sNot)
import Data.Singletons.Decide as Core hiding ((%~))

