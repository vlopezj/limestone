{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators #-}
module Ritchie.Polar.Core where

import Data.Singletons
import Data.Singletons.Prelude
import Data.Singletons.TH
import Tagged
import Data.Constraint
import Data.Maybe
import qualified Data.Singletons.Decide
import Data.Singletons.Decide as Core hiding ((%~))

(=:=) :: forall (a :: k) (b :: k) kparam.
         (kparam ~ 'KProxy, SDecide (kparam :: KProxy k)) =>
         Sing a -> Sing b -> Decision (a :~: b)
a =:= b  = a Data.Singletons.Decide.%~ b

(=::=) :: forall (a :: k) (b :: k) kparam f g.
         (kparam ~ 'KProxy, SDecide (kparam :: KProxy k), SingI a, SingI b) =>
         f a -> g b -> Decision (a :~: b)
_ =::= _ = (sing :: Sing a) =:= (sing :: Sing b)



deriving instance (Show (SBool b))

$(singletons [d|
   data Pol  = Pos | Neg | ApoP | ApoN deriving (Show,Eq)

   rev :: Pol -> Pol
   rev Pos  = Neg
   rev Neg  = Pos
   rev ApoP  = ApoN
   rev ApoN  = ApoP

   isPos :: Pol -> Bool
   isPos Pos = True
   isPos Neg = False
   isPos ApoP = True
   isPos ApoN = False

   isNeg :: Pol -> Bool
   isNeg Pos = False
   isNeg Neg = True
   isNeg ApoP = False
   isNeg ApoN = True

   data PPol = PPos | PNeg | PApo deriving (Show,Eq)

   revP :: PPol -> PPol
   revP PPos = PNeg
   revP PNeg = PPos
   revP PApo = PApo

   pi :: Pol -> PPol 
   pi Pos = PPos
   pi Neg = PNeg
   pi ApoP = PApo
   pi ApoN = PApo

   isApo :: Pol -> Bool
   isApo ApoP = True
   isApo ApoN = True
   isApo Pos  = False
   isApo Neg  = False
  |])

deriving instance (Show (SPol p))
deriving instance (Show (SPPol p))

type PolProp p = (p ~ Rev (Rev p), 
                  Rev p ~ Rev (Rev (Rev p)),
                  Not (IsPos p) ~ IsNeg p,
                  Not (IsNeg p) ~ IsPos p,
                  SingI p,
                  SingI (IsPos p),
                  SingI (IsNeg p),
                  SingI (Rev p),
                  IsPos (Rev p) ~ Not (IsPos p),
                  IsNeg (Rev p) ~ Not (IsNeg p),
                  IsPos (Rev p) ~ IsNeg p,
                  IsNeg (Rev p) ~ IsPos p,
                  Not (Not (IsPos p)) ~ IsPos p,
                  Not (Not (IsNeg p)) ~ IsNeg p)

revIsConvolution :: forall (p :: Pol) r f. (DPol f) => f p -> (PolProp p => r) -> r

revIsConvolution s t =
  case dpol s of
    SPos  -> t
    SNeg  -> t  
    SApoP -> t  
    SApoN -> t  

revIsConvolution2 :: forall (p :: Pol) (q :: Pol) r f. (SingI p, SingI q) => f p q -> ((PolProp p, PolProp q) => r) -> r
revIsConvolution2 _ t = revIsConvolution (sing :: SPol p) $ revIsConvolution (sing :: SPol q) $ t

notIsConvolution :: forall (b :: Bool) f r x . (DPos f) => f b ->
                    (((b ~ Not (Not b),
                       SingI b,
                       SingI (Not b)
                       )) => r) -> r
notIsConvolution b t = case dpos b of
  STrue -> t
  SFalse -> t

revPiCommute :: forall (p :: Pol) f s. (DPol f) => f p -> (RevP (Pi p) ~ Pi (Rev p) => s) -> s
revPiCommute p s = case dpol p of
  SPos  -> s 
  SNeg  -> s 
  SApoP -> s 
  SApoN -> s 

polEq :: forall f g p q r. (DPol f, DPol g) => f p -> g q -> ((p ~ q) => r) -> Maybe r
polEq p q κ = go (dpol p) (dpol q)
  where
    go :: SPol p -> SPol q -> Maybe r
    go SPos  SPos  = Just κ
    go SPos  _     = Nothing
    go SNeg  SNeg  = Just κ
    go SNeg  _     = Nothing
    go SApoP SApoP = Just κ
    go SApoP _     = Nothing
    go SApoN SApoN = Just κ
    go SApoN _     = Nothing

polEqRev :: forall f g p q r. (DPol f, DPol g) => f p -> g q -> ((p ~ Rev q, Rev p ~ q) => r) -> Maybe r
polEqRev p q κ = polEq (dpol p) (ddual (dpol q)) $ revIsConvolution q $ κ

revPIsConvolution :: forall r f (p :: PPol). 
                     SPPol p -> ((p ~ RevP (RevP (p)), 
                               SingI (RevP p),
                               SingI p) => r) -> r
revPIsConvolution s t =
  case s of
    SPPos -> t
    SPNeg -> t  
    SPApo -> t  

polPEq :: forall p q s. SPPol p -> SPPol q -> ((p ~ q) => s) -> Maybe s
polPEq p q s = go p q
  where
    go :: SPPol p -> SPPol q -> Maybe s
    go SPPos  SPPos  = Just s
    go SPPos  _     = Nothing
    go SPNeg  SPNeg  = Just s
    go SPNeg  _     = Nothing
    go SPApo  SPApo = Just s
    go SPApo  _     = Nothing

polEqP :: forall p q s f g. (DPol f, DPol g) => f p -> g q -> ((Pi p ~ Pi q) => s) -> Maybe s
polEqP p q = polPEq (dpolP p) (dpolP q)

polSingP :: forall (p :: Pol) s. (SingI p) => SPol p -> (SingI (Pi p) => s) -> s
polSingP p = withSingI (sPi p)

polPEqRev :: forall p q s. SPPol p -> SPPol q -> ((p ~ RevP q, RevP p ~ q) => s) -> Maybe s
polPEqRev p q s = polPEq p (sRevP q) $ revPIsConvolution q $ s

polEqPRev :: forall p q s f g. (DPol f, DPol g) => f p -> g q -> ((Pi p ~ RevP (Pi q), RevP (Pi p) ~ Pi q) => s) -> Maybe s
polEqPRev p q = polPEqRev (dpolP p) (dpolP q) 

dpolP :: (DPol f) => f p -> SPPol (Pi p)
dpolP p = sPi (dpol p)

class DPol (f :: Pol -> *) where
  dpol  :: f p -> SPol p

class DPos (f :: Bool -> *) where
  {-# MINIMAL (dpos,dposP|dposP) #-}
  dpos :: f b -> SBool b 
  dpos x = dposP x (\p -> case p of
                     SPos -> STrue 
                     SNeg -> SFalse 
                     SApoP -> STrue 
                     SApoN -> SFalse)

  dneg :: f b -> SBool (Not b)
  dneg x = sNot $ dpos x

  dposP :: f b -> (forall p. (PolProp p, IsPos p ~ b) => SPol p -> r) -> r
  dposP = defaultDPosP

defaultDPosP :: DPos f => f b -> (forall p. (PolProp p, IsPos p ~ b) => (SPol p -> r)) -> r
defaultDPosP x κ = case dpos x of
  STrue -> κ SPos 
  SFalse -> κ SNeg 

instance DPos Sing where
  dpos  = id
  dposP = defaultDPosP

class (DPol f) => DDual (f :: Pol -> *) where
  ddual :: f p -> f (Rev p)

dpol' :: (SingI p) => f p -> SPol p
dpol' _ = sing

instance DPol (Sing) where
  dpol  = id

instance DDual (Sing) where
  ddual = sRev 

forcePol :: (DPol f, DPol g) => f p -> g q -> Maybe (g p)
forcePol p t = case (dpol p,dpol t) of
  (SPos,SPos) -> Just t 
  (SNeg,SNeg) -> Just t
  (SApoP,SApoP) -> Just t 
  (SApoN,SApoN) -> Just t 
  (SPos,_   ) -> Nothing
  (SNeg,_   ) -> Nothing
  (SApoP,_   ) -> Nothing
  (SApoN,_   ) -> Nothing

forcePolP :: (DPol g) => SPPol p -> g q -> (forall p'. (Pi p' ~ p, SingI p') => g p' -> s) -> Maybe s
forcePolP p t s = case (p,dpol t) of
  (SPPos,SPos)  -> Just (s t)
  (SPPos,_   )  -> Nothing

  (SPNeg,SNeg)  -> Just (s t)
  (SPNeg,_   )  -> Nothing

  (SPApo,SApoP) -> Just (s t)
  (SPApo,SApoN) -> Just (s t)
  (SPApo,_   )  -> Nothing

forcePolUnsafe :: (DPol f, DPol g) => f p -> g q -> g p
forcePolUnsafe x y = fromJust $ forcePol x y

forcePolUnsafeS :: (DPol g, SingI p) => g q -> g p
forcePolUnsafeS = forcePolUnsafe sing

forcePolS :: (DPol g, SingI p) => g q -> Maybe (g p)
forcePolS = forcePol sing

forcePolPUnsafe :: (DPol g) => SPPol p -> g q -> (forall p'. (Pi p' ~ p, SingI p') => g p' -> s) -> s
forcePolPUnsafe x y s = fromJust $ forcePolP x y s 

forcePolModApo :: (DPol f, DPol g, IsPos a ~ IsPos b) => f b -> g a -> Maybe (g b)
forcePolModApo p t = case dpol p =:= dpol t of
  Proved Refl -> Just t
  Disproved _ -> Nothing

forcePolModApoS :: (SingI b, DPol g, IsPos a ~ IsPos b) => g a -> Maybe (g b)
forcePolModApoS = forcePolModApo sing 

asPol :: (DPol f) => f p -> (PolProp p => g p) -> g p
asPol p t = revIsConvolution (dpol p) t

asPolM :: (DPol f) => f p -> (PolProp p => m (g p)) -> m (g p)
asPolM p t = revIsConvolution (dpol p) t


asPolTag :: (DPol f, DPol g) => f p -> (SingI p => a ::: g p) -> a ::: g p
asPolTag p t = withSingI (dpol p) t

asPolDualOf :: (DPol f, DPol g) => f p -> (forall q. SingI q => g q) -> g (Rev p)
asPolDualOf p t = revIsConvolution p t

type SingP p = (SingI p, SingI (IsPos p))

asPos :: (DPol f) => f p -> Maybe (f Pos)
asPos = forcePol SPos

asNeg :: (DPol f) => f p -> Maybe (f Neg)
asNeg = forcePol SNeg 

forcePos :: (DPos f, DPos g) => f b -> g b' -> Maybe (g b)
forcePos p t = case (dpos p, dpos t) of
  (STrue, STrue)     -> Just t
  (SFalse, SFalse)   -> Just t
  (_, _) -> Nothing

forcePosS :: (DPos g, SingI b') => g b -> Maybe (g b')
forcePosS = forcePos sing

forcePosUnsafe :: (DPos f, DPos g) => f b -> g b' -> g b
forcePosUnsafe  x y = fromJust$ forcePos x y

forcePosUnsafeS :: (DPos g, SingI b') => g b -> g b'
forcePosUnsafeS = forcePosUnsafe sing

data PolA f where
  PolA :: (PolProp p) => f p -> PolA f

polA :: forall f p. (SingI p) => f p -> PolA f
polA x =  revIsConvolution (sing :: SPol p) $ PolA x

