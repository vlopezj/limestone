{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UnicodeSyntax #-}
module Ritchie.Polar.Type where

import Tagged
import Ritchie.Polar.Core
import Id
import Data.Singletons
import Data.Singletons.Decide hiding ((%~))
import LL hiding ((&))
import LL.Traversal
import Control.Lens
import Control.Monad.Except.Extra
import Control.Applicative
import Type.Data
import Data.Bifunctor
import Data.Bifoldable
import Data.Bitraversable

arrayPolarityP :: (DPol f) => f p -> ArrayPol' a
arrayPolarityP t = case dpol t of
  SPos -> APositive
  SNeg -> ANegative
  SApoP -> ANeutral' (Just$ PolConst True)
  SApoN -> ANeutral' (Just$ PolConst False)

-- | Polarities for types
data BiFPol f name ref (p :: Pol) where
  TyPol :: SPol p -> f name ref -> BiFPol f name ref p

type TyPol = BiFPol Type'
type SizedPol = BiFPol Sized'
type SizedP = SizedPol

--type DataPol = BiFPol DataType

data BiFPolA f name ref where TyA :: SingI p => BiFPol f name ref p -> BiFPolA f name ref

instance Bifunctor f => Bifunctor (BiFPolA f) where bimap f g (TyA (TyPol p x')) = (\x -> TyA (TyPol p x)) $ bimap f g x' 
instance Bifoldable f => Bifoldable (BiFPolA f) where bifoldMap f g (TyA (TyPol p x')) = bifoldMap f g x'
instance Bitraversable f => Bitraversable (BiFPolA f) where bitraverse f g (TyA (TyPol p x')) = (\x -> TyA (TyPol p x)) <$> bitraverse f g x'

instance Polar (BiFPolA f name ref) where
  arrayPolarity (TyA p) = arrayPolarityP p

instance Dual (f name ref) => Dual (BiFPolA f name ref) where
  dual (TyA p) = revIsConvolution p$ TyA (ddual p)

type TyPolA = BiFPolA Type'
type SizedPolA = BiFPolA Sized'

deriving instance (SingI p, Show (f name ref)) => Show (BiFPol f name ref p)
deriving instance (Show (f name ref)) => Show (BiFPolA f name ref)

type IdTyPol = TyPol Id Id
type IdTyPolA = TyPolA Id Id
type IdDataPol = DataPol Id Id
type IdDataPolA = DataPolA Id Id
type IdSizedPol = SizedPol Id Id
type IdSizedPolA = SizedPolA Id Id

data DataPol name ref q p = DataPol {
  dataPolRead  :: TyPol name ref p
 ,dataPolWrite :: TyPol name ref q
 }

data DataPolA name ref where
  DA :: (SingI p, SingI q) => DataPol name ref q p -> DataPolA name ref


mkDataSyncPol :: forall α name ref p. (Show name, Show ref) => TyPol name ref p -> Except String (DataPolA name ref)
mkDataSyncPol (TyPol p ty) = do
  dt <- mkData ty
  tyPol (dataRead  dt) $ \dataPolRead  ->
    tyPol (dataWrite dt) $ \dataPolWrite ->
     return $ DA DataPol{..}

mkDataCutPol :: forall α name ref p. (Show name, Show ref) => TyPol name ref p -> Except String (DataPol name ref (Rev p) p)
mkDataCutPol tp@(TyPol p ty) = do
  _ <- mkData ty
  let dataPolRead  = tp
  let dataPolWrite = ddual dataPolRead 
  return DataPol{..}

{-
dataPolAsType :: DataPol name ref p -> TyPol name ref p 
dataPolAsType (TyPol p dty) = (TyPol p (dataAsType dty))
-}

pattern TyPos ty  = TyPol SPos  ty
pattern TyNeg ty  = TyPol SNeg  ty
pattern TyApoP ty = TyPol SApoP ty
pattern TyApoN ty = TyPol SApoN ty

instance DPol (BiFPol f name ref) where
  dpol  (TyPol s _) = s

instance Dual (f name ref) => DDual (BiFPol f name ref) where
  ddual (TyPol s t) = TyPol (ddual s) (dual t)

instance Erase (BiFPol f name ref p) (f name ref) where
  erase (TyPol _ ty) = ty

tyPol :: (Polar (f name ref)) => f name ref -> ((forall p. PolProp p => BiFPol f name ref p -> r) -> r)
tyPol ty κ = tyP ty $ \p -> κ $ TyPol p ty

tyPolA :: (Polar (f name ref)) => f name ref -> BiFPolA f name ref
tyPolA ty = tyPol ty TyA

tyP :: (Polar (f name ref)) => f name ref -> ((forall p. PolProp p => SPol p -> r) -> r)
tyP ty κ = case arrayPolarity ty of
  APositive -> κ SPos
  ANegative -> κ SNeg
  ANeutral PolConstPos -> κ SApoP
  ANeutral PolConstNeg -> κ SApoN
  ANeutral' v           -> error$ "Unsolved variable " ++ show v ++ " in polarized type."

data SAName name ref where SAn :: SingI p => SPol p -> name ::: SizedPol name ref p -> SAName name ref
data SARef  name ref where SAr :: SingI p => SPol p -> ref  ::: SizedPol name ref p -> SARef name ref

instance Bifunctor SAName where bimap = bimapDefault
instance Bifoldable SAName where bifoldMap = bifoldMapDefault
instance Bitraversable SAName where
  bitraverse f g (SAn p₁ (x' ::: (TyPol p₂ tysz'))) =
    (\x tysz -> (SAn p₁ (x ::: (TyPol p₂ tysz)))) <$> f x' <*> bitraverse f g tysz'

instance Bifunctor SARef where bimap = bimapDefault
instance Bifoldable SARef where bifoldMap = bifoldMapDefault
instance Bitraversable SARef where
  bitraverse f g (SAr p₁ (x' ::: (TyPol p₂ tysz'))) =
    (\x tysz -> (SAr p₁ (x ::: (TyPol p₂ tysz)))) <$> g x' <*> bitraverse f g tysz'

pattern x :^+ y <- ((\case TyPol p (a :^ b) -> (TyPol p a, b)) -> (x,y))

(.^+) :: TyPol name ref p -> Size ref -> SizedPol name ref p
(TyPol p x) .^+ y = TyPol p (x :^ y)

deriving instance (Show name, Show ref) => Show (SAName name ref)
deriving instance (Show name, Show ref) => Show (SARef  name ref)
instance (Eq name, Eq ref) => Eq (SAName name ref) where
  (SAn p1 t1) == (SAn p2 t2) =
    case p1 =:= p2 of
      Proved Refl -> t1 == t2
      Disproved _ -> False

instance (Eq name, Eq ref) => Eq (SARef name ref) where
  (SAr p1 t1) == (SAr p2 t2) =
    case p1 =:= p2 of
      Proved Refl -> t1 == t2
      Disproved _ -> False



newtype SeqnPol name ref = SeqnPol { seqnPol :: SeqnPol' name ref } deriving (Show)
type IdSeqnPol = SeqnPol Id Id


type SeqnPol' name ref = Seqn (TypedCtx  name ref) 
                                                     (TyPolA name ref)
                                                     (Size ref)
                                                     (SizedPolA name ref)
                                                     (SAName name ref)
                                                     (SARef  name ref) 

pattern SP x ← SeqnPol (Seqn _ x)

newtype DerivPol name ref = DPol { derivPol :: Deriv'' (SeqnPol name ref) name ref } deriving (Show)

type IdDerivPol = DerivPol Id Id

saName :: name ::: Sized' name ref -> SAName name ref
saName (name ::: tysz) = tyPol tysz $ \typsz -> SAn sing $ name ::: typsz

saRef :: ref ::: Sized' name ref -> SARef name ref
saRef (ref ::: tysz) = tyPol tysz $ \typsz -> SAr sing $ ref ::: typsz

saSeqn :: Seqn2 name ref -> SeqnPol name ref
saSeqn (Seqn2 seqn) = SeqnPol $ go seqn
  where
    go (Seqn ann seq) = Seqn ann $ fmapSeq'' defSeqFmap{fseq = go
                                                       ,fty = \ty -> tyPolA ty 
                                                       ,ftysz = \ty -> tyPolA ty
                                                       ,fname = saName
                                                       ,fref = saRef} seq


saDeriv :: Deriv2 name ref -> DerivPol name ref
saDeriv (D2 d2) = DPol$ d2 & derivSequent %~ saSeqn
 
$(makeWrapped ''DerivPol)

instance Bifunctor (SeqnPol) where
  bimap = bimapDefault

instance Bifoldable (SeqnPol) where
  bifoldMap = bifoldMapDefault

instance Bitraversable (SeqnPol) where
  bitraverse f g (SeqnPol (Seqn ann' seq')) =
                    (\ann seq -> SeqnPol (Seqn ann seq)) <$>
                      traverse (\(name ::: tysz) -> (:::) <$> f name <*> bitraverse f g tysz) ann'  <*>
                      traverseSeq'' SeqTraverse{
                                    tseq  = fmap seqnPol . bitraverse f g . SeqnPol
                                   ,tty   = bitraverse f g
                                   ,tsz   = traverse g
                                   ,ttysz = bitraverse f g
                                   ,tname = bitraverse f g
                                   ,tref  = bitraverse f g
                                   } seq'

  
