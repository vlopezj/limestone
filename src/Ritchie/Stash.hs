{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PolyKinds #-}
module Ritchie.Stash where

import Control.Applicative
import Data.Functor.Identity

-- | If r is also a monad, it might be possible to combine them.
--   Laws:
--
--   stash . unstash == id
--   ¬ (∀ x. unstash . stash == id)
class Stash m r mr | m r -> mr, 
                     mr -> m r where
  stash    :: m r -> mr
  unstash  :: mr  -> m r

stashA   :: a (r x) -> StashAWrapped a r x
unstashA :: StashAWrapped a r x -> a (r x)

newtype StashAWrapped a f x  = StashA { getStashA :: a (f x) }

stashA = StashA
unstashA = getStashA 

instance (Functor a, Functor f) => Functor (StashAWrapped a f) where
  fmap f = StashA . fmap (fmap f) . getStashA
instance (Applicative a, Applicative f) => Applicative (StashAWrapped a f) where
  pure = StashA . pure . pure
  (StashA x) <*> (StashA y) = StashA$ liftA2 (<*>) x y

