{-# LANGUAGE CPP #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE IncoherentInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE IncoherentInstances #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
module Ritchie.Term where

import Data.Singletons
import Data.Singletons.Prelude hiding (Id)
import Text.Show.Functions
import Data.Functor.Contravariant
import Control.Applicative
import GHC.Exts
import Data.Constraint
import Data.Default
import Type
import Type.Ctx
import LL(What(Huh))
import Id
import Data.Maybe
import Ritchie.Polar
import Development.Placeholders
import ContM
import Control.Monad
import Data.Functor.Identity
import Ritchie.Euclidean
import Data.Singletons.Decide
import Op as Ritchie.Term
import Control.Monad.Except.Extra
import qualified FileLocation
import Debug.Trace
import FileLocation

#define CRASH $(FileLocation.err')
crash = FileLocation.err'

type IxLen i = i 

data CtxElem' a r where
  CtxLinear :: (SingP p) => Binding' a r p -> CtxElem' a r
  CtxSize   :: AIndex a -> CtxElem' a r
  CtxType   :: (SingP p, p ~ Pos) => TyPol (EName a) (ERef a) p -> CtxElem' a r

deriving instance (ShowAtoms a) => Show (CtxElem' a r)

-- | An environment
class (Applicative m, Monad m, Euclidean m (AIndex a),
       Show (EName a),
       Show (ERef a), Ord (EName a), Ord (ERef a),
       Atoms a,
       ShowAtoms a
       ) => NbEEnv a r m | m -> a r where

  bLookupCtx :: ERef a -> m (Maybe (CtxElem' a r))
  bBindCtx   :: EName a -> CtxElem' a r -> m α -> m α

  indexN  :: ALength a
          -> AIndex a
          -> m α -> m α

  capture  :: (α -> m β) -> m (α -> β)
  release  :: m (α -> β) -> (α -> m β)
  release f a = fmap ($ a) f

  assertSize :: (AIndex a, CmpOp, AIndex a) -> m α -> m α 

  ctxLocal :: m α -> m α

  cmpSize ::  (AIndex a, CmpOp, AIndex a) -> m (Maybe Bool)

eqSize x y = cmpSize (x,Eq,y)

bLookupCtxUnsafe :: forall a r m. (NbEEnv a r m) => ERef a -> m (CtxElem' a r)
bLookupCtxUnsafe r = bLookupCtx r >>= mkUnsafeLookup r

mkUnsafeLookup r = \case 
  Just x  -> return x
  Nothing -> CRASH $ show r ++ " not found in context."  


bLookup :: forall a r m p. (NbEEnv a r m, SingI p) => ERef a ::: SizedPol (EName a) (ERef a) p -> m (Binding' a r p)
bLookup (r ::: ty) = bLookupCtxUnsafe r >>= \case
  CtxLinear b -> case forcePol ty b of
     Just b  -> return b
     Nothing -> CRASH $ show r ++ " has polarity " ++ show (dpol b) ++ ", expected " ++ show (dpol ty)
  x -> CRASH $ show r ++ " is " ++ show x ++ ", expected an linear value."

bLookupSize :: (NbEEnv a r m) => ERef a -> m (Maybe (AIndex a))
bLookupSize r = bLookupCtx r >>= \case
  Just (CtxSize sz) -> return (Just sz)
  Nothing -> return (Nothing)
  ty -> CRASH $ "Expected a size, got " ++ show ty ++ "."

bLookupSizeUnsafe r = bLookupSize r >>= mkUnsafeLookup r

bLookupType :: (NbEEnv a r m) => ERef a -> m (TyPol (EName a) (ERef a) Pos)
bLookupType r = bLookupCtxUnsafe r >>= \case
  CtxType ty -> return (ty)
  ty -> CRASH $ "Expected a type, got " ++ show ty ++ "."

  
bBind :: (NbEEnv a r m, SingI p) => EName a ::: SizedP (EName a) (ERef a) p -> Binding' a r p -> m α -> m α 
bBind (n ::: _) b = revIsConvolution b$ bBindCtx n (CtxLinear b)

-- | Giving a meaning to atoms
class (Applicative m, Monad m, DPos a
      ,NbEEnv a r m
      ,Defer r
      ,Internalize r r
      ) => NbE a r m | m -> a r , r -> m where

  {-# MINIMAL axL, axR, axP,
              (cutL | cutR), cutA1,
              ((schedule, mix) | posToNegB),
              cosliceM,
              allocate,
              sizeCmp,
              translateType,
              translateArrayType
    #-}
  -- | Direct use of these functions should be avoided, as it might evaluate
  --   atoms unnecessarily. Use axB, axP or axT instead.
  axL  :: a False -> m (CoTermP' a r)
  axR  :: a True  -> (TermP' a r -> m r) -> m r

  axP :: a True    -> a False -> m r
  axP = defaultAxP

  cutL :: CutInfo ->
          ALength a -> TyPol (EName a) (ERef a) p -> (AIndex a -> Term' a r (Rev p) -> m r)
          -> (Binding' a r p -> m r)
                         -> m r
  cutL ci0 len typ = revIsConvolution typ$ defaultCutL ci0 len typ

  cutR :: CutInfo ->
          ALength a -> TyPol (EName a) (ERef a) p -> (Binding' a r (Rev p) -> m r)
                           -> (AIndex a -> Term' a r p -> m r)
                           -> m r
  cutR ci0 len typ l r = revIsConvolution typ$ cutL ci0 len (ddual typ) r l

  -- ^ Makes a term into an atom
  cutA1 :: SPol p
        -> Name -- ^ Suggested name for the atom
        -> AType a (IsPos p) 
        -> (Term' a r (Rev p) -> m r)
        -> (a (IsPos p) -> m r)
        -> m r       

  -- | Reimplementing for atoms is recomended.
  --   It needs only work with the multiplicative fragment.
  posToNegB :: BTerm' a r True -> ((BTerm' a r False -> m r) -> m r)
  posToNegB = defaultPosToNegB

  halt :: m r
  halt = schedule 0 (\_ -> huh "HALT")


  mix :: [m r] -> m r
  mix [] = halt
  mix [ma] = ma 
  mix (ma:mas) = 
    ma >>= \a -> mix mas >>= \b ->
    posToNegB (TPos$ Tensor (TUp$ \TUnit -> a) (TUp$ \TUnit -> b)) $
          axB (TPos$ Tensor (T0    TUnit)      (T0    TUnit))

  mixSeq :: [m r] -> m r
  mixSeq = mix

  -- | The environment need not be demoted
  schedule :: AIndex a -> (AIndex a -> m r) -> m r
  schedule len f = do
    posToNegB    (TPos$ Vec len (\_ -> T0    TUnit)) $ \b ->
       axB b =<< (TPos. Vec len<$> capture (\i -> TUp<$> capture (\TUnit -> indexN len i$ f i)))

  scheduleSeq :: AIndex a -> (AIndex a -> m r) -> m r
  scheduleSeq = schedule

  -- | Vectors
  cosliceM :: [(AIndex a, AIndex a, AIndex a -> m r)] -> AIndex a -> m r


  -- | The first atom is Read, the second is Write
  --   The atoms returned can be both used (axL …, axR …) any number of times, as long as:
  --   - At least one read evaluated *before* a write.
  allocate :: (SingI p, SingI q) => AIndex a
           -> DataPol (EName a) (ERef a) q p -> ((AIndex a -> Binding' a r p) -- copies -> read
                                               -> ((Binding' a r Pos -> m r) -> m r) -- update term B ⊗ (1 & B⊥)
                                               -> Binding' a r q      -- write
                                               -> m r) -> m r

  allocate0 :: (SingI p, SingI q) => AIndex a
            -> DataPol (EName a) (ERef a) q p -> (Binding' a r q      -- dump
                                              -> m r) -> m r
  allocate0 len tyd κ = allocate len tyd (\_read _ write -> κ write)
    
  sizeCmp :: (AIndex a, AIndex a) -> [(CmpOp, m r)] -> m r
  

  nWhat :: What -> [(CtxElem' a r, SizedPolA (EName a) (ERef a))] -> m r
  nWhat w _ = huh $ show w

  -- Silent failure
  huh :: String -> m r
  huh msg = nWhat (Huh msg) []

  translateType :: TyPol (EName a) (ERef a) p -> ContMR m r (AType a (IsPos p))
  translateArrayType :: SPol p -> AIndex a -> TyPol (EName a) (ERef a) pa -> ContMR m r (AType a (IsPos p))

axB :: forall b a r m. (SingI b, NbE a r m) => BTerm' a r b -> BTerm' a r (Not b) -> m r
axB a b = case sing :: SBool b of
  SFalse -> axB b a
  STrue  -> case (a,b) of
    (TAtom a, TAtom b) -> axP a b
    (TPos  a, TAtom b) -> axL b >>= axT a
    (TAtom a, TNeg  b) -> axR a $ flip axT b
    (TPos  a, TNeg  b) -> axT a b
    (_    , _    ) -> bugInGHC
 
axT :: (NbE a r m) => TermP' a r -> CoTermP' a r -> m r
axT (RPrim a) (LPrim b) = axP a b
axT (RBox  a) (LBox  b) = unboxUnsafeS a b
axT t         (Up f)    = return$ f t

data Binding' a r p where
  Binding1 :: (SingI p) => Term' a r p -> Binding' a r p
  BindingN :: (SingI p) =>
             { bSz      :: ALength a
               -- This function will be applied to the indices before being passed to the vector 
             , bPermute :: AIndex a -> AIndex a
               -- Notice that, if the BTerm is an atom, it might be evaluated
               -- up to bSz times.
             , bTerm  :: BTerm' a r True
             } -> Binding' a r p

instance DPol (Binding' a r) where
  dpol Binding1{} = sing
  dpol BindingN{} = sing

deriving instance (SingP p, ShowAtoms a) => Show (Binding' a r p)

class (Num (AIndex a)) => Atoms (a :: Bool -> k) where
  type AType  a :: Bool -> *
  type AKind  a :: *
  type AIndex a :: *
  type EName  a :: *
  type ERef   a :: *

type ShowAtoms a = (ShowDPos a, Show (AIndex a), Show (AKind a), Show (AType a True), Show (AType a False), Show (EName a), Show (ERef a))

asBindingN :: (Num (AIndex a)) => Binding' a r p -> Binding' a r p
asBindingN (Binding1 t)                    = revIsConvolution t$ BindingN 1 id (TPos$ Vec 1 (\_ -> t))
asBindingN b@(BindingN bSz bPermute bTerm) = b
 
indexB1 :: (NbE a r m) => SingI p => Binding' a r p -> ContMR m r (Term' a r p)
indexB1 b κ = indexBN b (\_ f ->  κ (f 0)) 

indexBN :: forall a r m p. (NbE a r m, SingI p) => Binding' a r p -> (AIndex a -> (AIndex a -> Term' a r p) -> m r) -> m r
indexBN b κ = case b of
    Binding1 bTerm -> κ 1 $ \_ -> bTerm
    BindingN{..}   -> mkPos (TermP bTerm) $ \(Vec _ f) -> κ bSz (forcePolUnsafeS . f . bPermute)

lookup1 :: (NbE a r m) => SingI p => ERef a ::: SizedPol (EName a) (ERef a) p -> ContMR m r (Term' a r p)
lookup1 n κ = bLookup n >>= flip indexB1 κ

lookupN :: forall a r m p. (NbE a r m, SingI p) => ERef a ::: SizedPol (EName a) (ERef a) p -> (AIndex a -> (AIndex a -> Term' a r p) -> m r) -> m r
lookupN n κ = bLookup n >>= flip indexBN κ

bSingle :: (SingI p) => Term' a r p -> Binding' a r p
bSingle t = Binding1 t

bVecB bSz bt = BindingN {bSz, bPermute = id, bTerm = bt }

bVec :: forall i a r m p. (i ~ AIndex a, NbE a r m, SingI p) => i -> (i -> Term' a r p) -> (Binding' a r p)
bVec bSz f = revIsConvolution (sing :: SPol p)$ bVecB bSz (TPos (Vec bSz f))

-- | Allocates 1 copy of size 1
allocateRW1 :: forall a r m p q. (NbE a r m, SingI p, SingI q) =>
               DataPol (EName a) (ERef a) q p ->
               (Term' a r p -> Term' a r q -> m r) -> m r
allocateRW1 tyd κ =
  allocate 1 tyd $ \r _ w ->
  revIsConvolution (sing :: SPol p) $
  revIsConvolution (sing :: SPol q) $
  indexB1 (r 1) $ \tr ->
  indexB1 w $ \tw ->
  κ tr tw
  
{-
aInd1 :: (NbE a r m, SingI p) => (AIndex a -> Term' a r p) -> m (AIndex a -> Term' a r p)
aInd1 f = capture$ \a -> upM $ \yield -> aInd a $ \i -> yield (f i)

indA1 :: (NbE a r m, SingI p) => (AIndex a -> Term' a r p) -> m (AIndex a -> Term' a r p)
indA1 f = capture$ (\i -> f <$> indA i)
-}
up :: forall a r m p. (NbE a r m, SingI p) => ((Term' a r p -> r) -> r) -> m (Term' a r p)
up f = case sing :: SPol p of
  SPos  -> fmap TermNN$ capture$ \b₁ ->
    fmap f$ capture$ \t₂ ->
      fmap (mkB t₂)$ capture$ \b₂ -> axB b₁ b₂

  SNeg  -> 
    fmap TUp$ capture$ \t₁ -> -- :: TermP' a r ->
      fmap f$ capture$ \t₂ -> -- :: Term' a r Neg ->
        (mkBM t₂)$ \b₂ -> axB (TPos t₁) b₂

  SApoP -> fmap TermAP'$ up $ f . (. isoApo)
  SApoN -> fmap TermAN'$ up $ f . (. isoApo)

isoApo :: forall a r p p'. (SingI p, SingI p', IsPos p ~ IsPos p') => Term' a r p -> Term' a r p'
isoApo = case (sing :: SPol p, sing :: SPol p') of
  (SPos, SPos)   -> id
  (SPos, SApoP)  -> TermAP'

  (SNeg, SNeg)   -> id
  (SNeg, SApoN)  -> TermAN'

  (SApoP, SApoP) -> id
  (SApoP, SPos)  -> \(TermAP' t) -> t

  (SApoN, SApoN) -> id
  (SApoN, SNeg)  -> \(TermAN' t) -> t

  (_    , _    ) -> bugInGHC

upM :: (NbE a r m, SingI p) => ((Term' a r p -> m r) -> m r) -> m (Term' a r p)
upM f = captureM f >>= up

-- | Default implementation for joining atoms:
--   * It is expected to loop if applied to atoms which cannot be
--     evaluated (e.g. primitive terms)
--   * It might not offer the best performance (in C it would be better
--     to do straightforward memcpy instead.
defaultAxP :: (NbE a r m) => a True -> a False -> m r 
defaultAxP aR aL =
  axR aR $ \r -> do
    l <- axL aL
    axT r l

defaultCutL :: forall p a r m. (NbE a r m) 
            => CutInfo -> ALength a -> TyPol (EName a) (ERef a) p
            -> (AIndex a -> Term' a r (Rev p) -> m r)
            -> (Binding' a r p -> m r)
            -> m r
defaultCutL ci0 len ty bl br = revIsConvolution ty$ case ci0 of
  Manual -> cutAL len ty bl br
  _      -> fuseCutL len bl br

fuseCutL :: forall p i a r m. (NbE a r m, i ~ AIndex a, SingI p)
            => i
            -> (i -> Term' a r (Rev p) -> m r)
            -> (Binding' a r p -> m r)
            -> m r
fuseCutL len contL contR = bVec len <$> capture (\i -> indexN len i$ shiftM$ contL i) >>= contR

cutAL :: (NbE a r m) => ALength a
      -> TyPol (EName a) (ERef a) p 
      -> (AIndex a -> Term' a r (Rev p) -> m r)
      -> (Binding' a r p -> m r)
      -> m r
cutAL len typ contL contR = revIsConvolution typ$ 
  translateArrayType SPos len typ $ \aty ->
    cutA1 SPos "cutA" aty
      (\term -> do
          v <- capture (\i -> indexN len i$ shiftM' $ contL i)            
          unbox' term (TermP$ TPos (Vec len v)))
      (\atom -> contR (bVecB len (TAtom atom)))
    
cutAR :: (NbE a r m) => ALength a
      -> TyPol (EName a) (ERef a) p 
      -> (Binding' a r (Rev p) -> m r)
      -> (AIndex a -> Term' a r p -> m r)
      -> m r
cutAR len typ contL contR = revIsConvolution typ$ cutAL len (ddual typ) contR contL

defaultPosToNegB :: (NbE a r m) => BTerm' a r True -> ((BTerm' a r False -> m r) -> m r)
defaultPosToNegB b κ = mkPos (TermP b) $ \case
    TUnit      -> (κ. TNeg. Up =<<)$ capture$ \TUnit -> halt
    Tensor a b -> (κ. TNeg. Up =<<)$ capture$ \(Tensor a' b') ->
      mix [unboxUnsafeS a a', unboxUnsafeS b b']
    Vec len f  -> (κ. TNeg. Up =<<)$ capture$ \(Vec _ g) -> schedule len (\i -> unboxUnsafeS (f i) (g i))
    _ -> CRASH "Can't schedule non-multiplicative term" 

posToNeg :: (NbE a r m) => Term' a r ApoP -> (Term' a r ApoN -> m r) -> m r
posToNeg (TermAP p) κ = posToNegB p $ \n -> κ$ TermAN n

-- | Internalized failure
deferM :: forall a r m β. (Internalize r β, Defer r, Applicative m) => String -> m β
deferM x = pure $ defer x

type ALength a = AIndex a

data TermP' a r where
  Tensor    :: (SingP p₁, SingP p₂) =>
            Term' a r p₁ -> Term' a r p₂ -> TermP' a r
  Sum       :: (SingP p₁) => Choice -> Term' a r p₁ -> TermP' a r
  -- TODO: The size could very possibly be removed
  Vec       :: (SingP p)  => ALength a -> (AIndex a -> Term' a r p) -> TermP' a r
  SigmaType :: (q ~ Pos, SingP p)  => TyPol (EName a) (ERef a) q -> Term' a r p -> TermP' a r
  SigmaSize :: (SingP p)  => ALength a -> Term' a r p -> TermP' a r
  TUnit     :: TermP' a r
  RBox      :: (SingP p) => Term' a r p -> TermP' a r
  RPrim     :: a True -> TermP' a r

data CoTermP' a r where
  -- Not sure if this one will ever get instantiated
  LPrim  :: a False -> CoTermP' a r 
  LBox   :: (SingP p) => Term' a r p -> CoTermP' a r
  -- In general, the function encapsulated inside Up will accept one and only
  -- one of the constructors in TermP', depending on the type that the term
  -- represents.
  Up      :: (TermP' a r -> r) -> CoTermP' a r

-- | A 'focused' term (it has had all the layers of indirection removed)
data BTerm' a r b where
  TPos    :: TermP' a r        -> BTerm' a r True
  TNeg    :: CoTermP' a r      -> BTerm' a r False
  TAtom   :: SingI b => a b    -> BTerm' a r b


data BaseTerm' a r where
  BPos    :: TermP' a r               -> BaseTerm' a r
  BNeg    :: CoTermP' a r             -> BaseTerm' a r

data Term' :: (Bool -> *) -> * -> Pol -> * where
  TermP   :: BTerm' a r True         -> Term' a r Pos
  TermN   :: BTerm' a r False        -> Term' a r Neg
  TermNN  :: (BTerm' a r False -> r) -> Term' a r Pos
  TermAP' :: Term' a r Pos -> Term' a r ApoP
  TermAN' :: Term' a r Neg -> Term' a r ApoN

tAtom :: forall a r p. SingI p => a (IsPos p) -> Term' a r p
tAtom a = case sing :: SPol p of
  SPos  -> TermP (TAtom a)
  SNeg  -> TermN (TAtom a)
  SApoP -> TermAP (TAtom a)
  SApoN -> TermAN (TAtom a)

pattern TermAP p = TermAP' (TermP p)
pattern TermAN n = TermAN' (TermN n)
pattern TermANN f = TermAP' (TermNN f)
pattern TUp f = TermN (TNeg (Up f))
pattern T0 a  = TermP (TPos a)

instance DPol (Term' a r) where
  dpol TermP{}  = SPos
  dpol TermN{}  = SNeg
  dpol TermNN{} = SPos
  dpol TermAP'{} = SApoP
  dpol TermAN'{} = SApoN

instance DPos (BTerm' a r) where
  dpos TPos{} = sing
  dpos TNeg{} = sing
  dpos TAtom{} = sing

  dposP = defaultDPosP


unbox :: forall a r m p. (NbE a r m, SingI p) => Term' a r p -> Term' a r (Rev p) -> m r
-- the function is symmetric, we only need to implement one half
--unbox u v | trace (revIsConvolution u$ "Unbox " ++ show u ++ show v ++ ".") False = undefined
unbox u v = revIsConvolution u $ mkBM u $ \a -> mkBM v $ \b -> axB a b

unbox' :: forall a r m p. (NbE a r m, SingI p) => Term' a r (Rev p) -> Term' a r p -> m r
unbox' u v = revIsConvolution v $ unbox v u

unboxUnsafeS :: forall a r m p q. (NbE a r m, SingI p, SingI q) => Term' a r p -> Term' a r q -> m r
unboxUnsafeS p q = case (sRev (sing :: SPol p)) %~ (sing :: SPol q) of
  Proved Refl -> unbox p q
  Disproved _ -> CRASH $ "unboxUnsafeS: Terms have the same polarity" 
  

mkBase :: forall a r m p. (SingI p, NbE a r m) => Term' a r p -> ((BaseTerm' a r) -> m r) -> m r
mkBase t μ = case (sing :: SPol p, t) of
  (SPos, _) -> mkPos t (μ . BPos)
  (SNeg, _) -> mkNeg t >>= μ . BNeg
  (SApoP, TermAP' tp) -> mkBase tp μ
  (SApoN, TermAN' tn) -> mkBase tn μ
  (_    , _    ) -> bugInGHC

-- No element evaluation
mkB :: Term' a r p -> (BTerm' a r (IsPos p) -> r) -> r
mkB t κ = case t of
  TermP p   -> κ p
  TermN n   -> κ n
  TermNN f  -> f (TNeg$ Up (κ . TPos)) 
  TermAP' p -> mkB p κ
  TermAN' n -> mkB n κ


mkBM :: (NbE a r m) => Term' a r p -> ((BTerm' a r (IsPos p) -> m r) -> m r)
mkBM t = capture'$ mkB t

capture' :: (NbE a r m) => ∀a. ((a -> r) -> r) -> (a -> m r) -> m r
capture' κ f = capture f >>= return . κ

captureM :: (NbE a r m) => ∀a. ((a -> m r) -> m r) -> m ((a -> r) -> r) 
captureM f = capture f >>= \f' -> return $ \κ -> f' (return . κ)
 

mkPos :: (NbE a r m, True ~ IsPos p, SingI p) => Term' a r p -> (TermP' a r -> m r) -> m r
mkPos (TermP (TAtom a)) κ = axR a κ
mkPos (TermP (TPos  p)) κ = κ p
mkPos (TermNN f) κ = fmap (f . TNeg . Up) $ capture κ  
mkPos (TermAP' t) κ = mkPos t κ 

mkNeg :: (NbE a r m, True ~ IsNeg p, SingI p) => Term' a r p -> m (CoTermP' a r)
mkNeg (TermN (TAtom a)) = axL a 
mkNeg (TermN (TNeg  t)) = return t
mkNeg (TermAN' t) = mkNeg t 

shift :: forall p r a. (SingI p) => ((PolProp p, SingI (Rev p)) => Term' a r (Rev p) -> r) -> (Term' a r p)
shift κ = case sing :: Sing p of
  SNeg  -> TUp (\p -> κ (TermP (TPos p)))
  SPos  -> TermNN (\n -> κ (TermN n))
  SApoN -> TermAN (TNeg (Up (\p -> κ (TermAP (TPos p)))))
  SApoP -> TermANN (\n -> κ (TermAN n))

-- | This function is isomorphic to shift, but Haskell cannot see it
{-
shift' :: forall p r a. (SingI p) => (Term' a r p -> r) -> (Term' a r (Rev p))
shift' = revIsConvolution (sing :: SPol p) shift
-}

shiftM :: forall p r a m. (NbE a r m, SingI p) => (SingP (Rev p) => Term' a r (Rev p) -> m r) -> m (Term' a r p)
shiftM κ = do
  κ₂ <- capture κ₁ 
  return$ shift κ₂
  where
    κ₁ :: Term' a r (Rev p) -> m r
    κ₁ x = revIsConvolution (sing :: SPol p) $ κ x

shiftM' :: forall p r a m. (NbE a r m, SingI p) => (Term' a r p -> m r) -> m (Term' a r (Rev p))
shiftM' κ = revIsConvolution (sing :: SPol p) $ shiftM κ

cosliceTermM :: (NbE a r m, SingI p) => [(AIndex a, AIndex a -> m (Term' a r p))] -> AIndex a -> m (Term' a r p)
cosliceTermM alts j = upM $ \yield -> cosliceSzM [(sz, \i -> m i >>= yield) | (sz, m) <- alts] j


cosliceSzM :: (NbE a r m) => [(AIndex a, AIndex a -> m r)] -> AIndex a -> m r
cosliceSzM ops i = cosliceM (go 0 ops) i
    where
      go _ [] = []
      go offset ((len, μ):bs) = (offset, offset + len, μ):go (offset + len) bs

scheduleCosliceSzM :: (NbE a r m) => [(AIndex a, AIndex a -> m r)] -> m r
scheduleCosliceSzM m = mixSeq [ schedule sz f | (sz, f) <- m ]

-- | Pack a fixed action as a term. Useful for error reporting
class Internalize r β | β -> r where
  internalize :: r -> β

internalize1 :: r -> b -> r
internalize1 = const 

instance Internalize r (CoTermP' a r) where
  internalize :: r -> CoTermP' a r
  internalize = Up . internalize1

instance Internalize r (BTerm' a r False) where
  internalize :: r -> BTerm' a r False
  internalize = TNeg . internalize

instance (SingI p) => Internalize r (Binding' a r p) where
  internalize :: r -> Binding' a r p
  internalize = Binding1 . internalize

instance (SingI p) => Internalize r (Term' a r p) where
  internalize :: r -> Term' a r p
  internalize = case sing :: SPol p of
    SPos  -> TermNN  . internalize1
    SNeg  -> TermN   . internalize
    SApoP -> TermAP' . internalize
    SApoN -> TermAN' . internalize
                                      
instance (ShowDPos a, SingI b) => Show (a b) where
  showsPrec = showsPrecDPos

-- Errors without knowing the inner value
class Defer r where
  defer :: (Internalize r β) => String -> β
  defer = error

class (Show (a True), Show (a False)) => ShowDPos a where
  showsPrecDPos :: forall b. (SingI b) => Int -> a b -> ShowS
  showsPrecDPos i a = case sing :: SBool b of
    STrue  -> showsPrec i a
    SFalse -> showsPrec i a

instance (ShowDPos Sing) where

type ENameP a p = (EName a) ::: SizedP (EName a) (ERef a) p
type ERefP a p = (ERef a) ::: SizedP (EName a) (ERef a) p

type IdENameP p = Id ::: SizedP Id Id p
type IdERefP  p = Id ::: SizedP Id Id p
  
deriving instance ShowAtoms a => Show (TermP' a r)
deriving instance ShowAtoms a => Show (CoTermP' a r)
deriving instance ShowAtoms a => Show (BaseTerm' a r)
deriving instance (ShowAtoms a, SingI b) => Show (BTerm' a r b)
deriving instance (ShowAtoms a, SingP p) => Show (Term' a r p)

translateSize :: (NbEEnv a r m) => Size (ERef a) -> m (AIndex a)
translateSize Size{..} = do
    len <- flip catchExceptT CRASH $
      sum `liftM`
      sequence [
        ((fromInteger coef *) . product) `liftM`
          mapM lkup symbs
        | (coef, symbs) <- getSize]
    return$ len
    where
      lkup (TVar True ref) = lift$ bLookupSizeUnsafe ref
      lkup symb            = throwError$ "Unexpected non-variable symbol " ++ show symb ++ " in size."

lookup1Pos :: forall a r m p τ name ref. (NbE a r m, EName a ~ name, ERef a ~ ref, SingI p, True ~ IsPos p)
         => ERef a ::: SizedP (EName a) (ERef a) p -> (TermP' a r -> m r) -> m r
lookup1Pos r κ = lookup1 r $ \t -> mkPos t κ

lookup1Neg :: (NbE a r m, EName a ~ name, ERef a ~ ref, SingI p, True ~ IsNeg p)
         => ERef a ::: SizedP (EName a) (ERef a) p -> (CoTermP' a r -> m r) -> m r
lookup1Neg r κ = lookup1 r $ \t -> mkNeg t >>= κ

bind1 :: (NbE a r m, SingI p) => EName a ::: SizedP (EName a) (ERef a) p -> Term' a r p -> m r -> m r
bind1 ident t κ = bBind ident (Binding1 t) κ

bind1U :: (NbE a r m, SingI p, SingI q) => EName a ::: SizedP (EName a) (ERef a) p -> Term' a r q -> m r -> m r
bind1U ident t κ = forcePolUnsafeTag ident >>= \i -> bind1 i t κ

bind1P :: (NbE a r m, SingI p, SingI q) => f q -> EName a ::: SizedP (EName a) (ERef a) p -> Term' a r q -> m r -> m r
bind1P _ ident t κ = forcePolUnsafeTag ident >>= \i -> bind1 i t κ

bindN :: forall a r m p name ref. (NbE a r m, SingI p) => 
         EName a ::: SizedP (EName a) (ERef a) p -> AIndex a -> (AIndex a -> m (Term' a r p)) -> m r -> m r
bindN ident@(_ ::: ty :^+ _) len f κ = do
  f₁  <- capture f
  withSingI (sIsPos (sing :: SPol p)) $
    bBind ident (asPol ty$ (bVec len f₁)) κ

bindNU :: forall a r m p q name ref. (NbE a r m, SingI p, SingI q) => 
         EName a ::: SizedP (EName a) (ERef a) p -> AIndex a -> (AIndex a -> m (Term' a r q)) -> m r -> m r
bindNU ident len t κ = forcePolUnsafeTag ident >>= \i -> bindN i len t κ

forcePolUnsafeTag :: forall α a f (p :: Pol) (q :: Pol) r m.
                     (NbE a r m, SingI p, SingI q, Show α) => α ::: f p -> m (α ::: f q)
forcePolUnsafeTag p@(α ::: _) =
  let a = (sing :: SPol p)
      b = (sing :: SPol q) 
  in
  case a =:= b of
    Proved Refl -> return p
    Disproved _ -> CRASH $ "Expected polarity " ++ show b ++ " for " ++ show α ++ ", got " ++ show a ++ "."

forcePolUnsafeTagP :: forall α a f g (p :: Pol) (q :: Pol) r m.
                     (NbE a r m, SingI p, SingI q, Show α) => g q -> α ::: f p -> m (α ::: f q)
forcePolUnsafeTagP _ x = forcePolUnsafeTag x


forcePolUnsafeTerm :: forall (p :: Pol) (q :: Pol) r f. (SingI p, SingI q, Defer r, Internalize r (f p)) => f q -> f p
forcePolUnsafeTerm t = 
  let a = (sing :: SPol q)
      b = (sing :: SPol p) 
  in
  case a =:= b of
    Proved Refl -> t
    Disproved _ -> defer $ "Expected polarity " ++ show b ++ ", got " ++ show a ++ "."

bTermIsAtom :: BTerm' a r b -> Maybe (a b)
bTermIsAtom (TAtom a) = Just a
bTermIsAtom (TPos (RPrim a)) = Just a
bTermIsAtom (TNeg (LPrim a)) = Just a
bTermIsAtom _ = Nothing

termIsAtom :: Term' a r p -> ((Either (BTerm' a r (IsPos p)) (a (IsPos p))) -> r) -> r
termIsAtom t κ = mkB t  $ \bt -> κ (maybe (Left$ bt) Right (bTermIsAtom bt))

termB :: forall a r b p. (b ~ IsPos p, SingI p) => BTerm' a r b -> Term' a r p
termB bt = case (sing :: SPol p) of
  SPos  -> TermP bt
  SNeg  -> TermN bt
  SApoP -> TermAP bt
  SApoN -> TermAN bt

-- TODO: Can be optimized for atoms
mkBox :: forall a r q p. (SingI q, SingI p) => Term' a r p -> Term' a r q
mkBox t = revIsConvolution (sing :: SPol p) $ case (sing :: SPol q) of
  SPos  -> TermP (TPos (RBox t))
  SApoP -> TermAP'$ mkBox t
  SApoN -> TermAN'$ mkBox t
  SNeg  -> TermN (TNeg (LBox t))


mkVec1 :: forall a r m q p. (SingI q, SingI p, NbE a r m) => Term' a r p -> m (Term' a r q)
mkVec1 t = revIsConvolution (sing :: SPol p) $ case (sing :: SPol q) of
  SPos  -> pure$ TermP (TPos (Vec 1 (const t)))
  SApoP -> TermAP' <$> mkVec1 t
  SApoN -> TermAN' <$> mkVec1 t
  SNeg  -> TermN . TNeg . Up <$> capture (\(Vec _ f) -> unbox' ($fromJst $ forcePolS (f 0)) t)
