{-# LANGUAGE MultiParamTypeClasses #-}
module Ritchie.Euclidean where

import Control.Applicative

class (Num a, Applicative m) => Euclidean m a where
  {-# MINIMAL divM | (%), (//) #-}
  divM :: a -> a -> m (a,a) 
  a `divM` b = (,) <$> (a // b) <*> (a % b)

  (//) :: a -> a -> m a
  a // b = fst <$> a `divM` b

  (%)  :: a -> a -> m a   
  a %  b = snd <$> a `divM` b
