{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE PatternGuards #-}
-- | This module glues together the C pretty printer with the term language
--   by providing an instance of NbE
module Ritchie.C(module Ritchie.C) where

import Ritchie.C.Type as Ritchie.C
import Ritchie.C.Core as Ritchie.C hiding (crash)
import Ritchie.C.Mem  as Ritchie.C
import Ritchie.C.Pretty 

import Ritchie.Euclidean as Ritchie.Core hiding (crash)
import Ritchie.Term as Ritchie.Core hiding (crash)
import Ritchie.Polar as Ritchie.Core hiding (crash)

import Control.Monad.Writer hiding (Sum)
import Control.Monad.Reader
import Control.Monad.Except.Extra
import Control.Applicative
import Data.String
import Data.Maybe
import Data.List
import Fresh
import qualified Data.Map as Map
import Data.Map(Map)
import Id
import LL hiding (Pos, Neg, Mem, CtxElem, mix, halt)
import Text.PrettyPrint (Doc)
import Control.Lens
import Fuel
import Data.Default
-- import Development.Placeholders
import Data.Singletons
import ContM
import Data.Generics.Genifunctors
import Ritchie.Stash
import Pol.NIA
import Data.Generics.Is
import FileLocation
import Debug.Trace

-- ^ Integer expressions make this an instance of Num
instance Num (Atom True) where
  fromInteger i = Atom (ALen (fromInteger i)) (aSize)

rangeAEnv :: Length -> (CIndex -> EnvM TVoid) -> EnvM TVoid
rangeAEnv len κ = unstash$ rangeA len $ \i -> stash$ assertSize (0, Le, i)$ assertSize (i, Lt, len)$ indexN len i $ κ i

rangeAPragmaEnv :: Code -> Length -> (CIndex -> EnvM TVoid) -> EnvM TVoid
rangeAPragmaEnv pragma len κ = unstash$ rangeAPragma pragma len $
                               \i -> stash$
                                     assertSize (0, Le, i)$
                                     assertSize (i, Lt, len)$
                                     indexN len i$ κ i

mkTAtom :: forall b r. SingI b => RichAtomTy b -> (forall p. (PolProp p, b ~ IsPos p) => (AtomV -> Term p) -> r) -> r
mkTAtom atomTy κ = dposP atomTy go
  where
    go :: forall p. (SingI p, b ~ IsPos p) => SPol p -> r
    go p = revIsConvolution p $ κ $ \atomV -> (tAtom Atom{atomV,atomTy} :: Term p)

unreachable :: forall (p :: Pol). (SingI p) => Term p
unreachable | p <- (sing :: SPol p) =
  revIsConvolution p $
  shift (\(λ :: Term (Rev p)) -> return $ sUnreachable)

-- | This translation may not be injective
--   This function returns Nothing if the type has 0-bits of information
--   (which means it has no C representation).
--   Errors are signaled in another way.

-- Assumptions
length1ArrayIsElementType = id

-- | This translation is well defined, p = Pos or Neg
lTy :: SPol p -> AtomTyP (IsPos p) -> IdTyPol p
lTy = error "Implement lTy"

-- ^ Writes an expression to memory.
--   This is a shallow copy, it will only allocate a (small) amount of memory
--   on the stack.

lengthAsAtom :: Length -> Atom True
lengthAsAtom e = Atom{ atomV = ALen e, atomTy = aSize } 

atomToTerm :: (SingI p) => Atom (IsPos p) -> Term p 
atomToTerm a@Atom{..} = tAtom a

fromJust' (Just x) = x
todo = error
crash = error
notImplemented = error "not implemented"

callFunction :: forall b p a. (SingI p, b ~ IsPos p) => Expr a  -> RichAtomTy b -> EnvM (Term p)
callFunction g ty =
  revIsConvolution (sing :: SPol p)$ go ty $ \_ go' -> go' (\args ret -> ret$ AExpr$ g `call` args) >>= return . (fromJust' . forcePolModApoS)
  where
  go :: forall b α. (SingI b)
     => RichAtomTy b
     -> (forall p. (PolProp p, b ~ IsPos p) => SPol p -> (([EA Expr] -> ContMR EnvM TVoid AtomV) -> EnvM (Term p))
         -> EnvM α) -> EnvM α

  go ty@(ARATy t) κ = notIsConvolution (sing :: SBool b)$ case t  of
   (AIdx p (Closed len) ret@(A RATy{tyAPol})) ->
     revIsConvolution p$ revIsConvolution tyAPol $
     go ret $ \_ go' ->
     κ sing $ \g -> do
        bt <- TPos . Vec len <$> capture (\i -> go' (\es -> g (EA (toExpr i):es)))
        return$ asPol p (termB bt)

   (APiSize tyVar ret@(A RATy{tyAPol})) ->
     revIsConvolution tyAPol$
     κ sing $ \g ->
     TermN . TNeg . Up <$> capture (
       \(SigmaSize sz v) ->
       go (tyBindVarSize (tyVar, sz) ret) $ \_ go' ->
       go' (\es -> g (EA (toExpr sz):es)) >>= \t' ->
       unbox' (fromJust' $ forcePolS v)
              (fromJust' $ forcePolModApo tyAPol t')
       )

   (ALol (_,arg@(A RATy{tyAPol = tyAPol₁})) ret@(A RATy{tyAPol = tyAPol₂})) -> 
     revIsConvolution tyAPol₁ $ revIsConvolution tyAPol₂ $
     κ sing $ \g -> 
     TermN . TNeg . Up <$> capture (\(Tensor u v) ->
       termAsArgs (fromJust' $ forcePol tyAPol₁ u) arg $ \e ->
       go  ret $ \_ go' -> go' (\es -> g (e ++ es)) >>= \t ->
       unbox' (fromJust' $ forcePol       (ddual tyAPol₂) v) 
              (fromJust' $ forcePolModApo tyAPol₂         t))
       

   (AWith arg₁@(A RATy{tyAPol = tyAPol₁}) arg₂@(A RATy{tyAPol = tyAPol₂})) -> 
    revIsConvolution tyAPol₁ $ revIsConvolution tyAPol₂ $
    κ sing $ \g ->
    TermN . TNeg . Up <$> capture (
       \(Sum choice w) ->
       let ([_, argTy], retTy) = cFunTy ty in unstash$
       cWithLocalMaybe argTy "warg" $ \l@(accessors -> Accessors{..}) -> stash$
       let call_g = g ([EA$ toExpr$ eBool$ choiceToBool choice] ++ [EA$ toExpr l | Just l <- [l]]) 
           (fields, hasRet, sel) = case choice of 
              Inl -> (inlArgs arg₁, isJust$ cFunTyRet arg₁, "inl")
              Inr -> (inrArgs arg₂, isJust$ cFunTyRet arg₂, "inr")
           g' = (\es ret -> unstashA$ 
                        statementsA [ 
                             statementsA [ dst `assignAtomV` (AExpr src)
                                         | (dst, EA src) <- fields `zip` es
                                         ] 
                            , 
                             stashA$ call_g $ \v -> unstashA$
                               if hasRet then atomVAsExpr v $ \e -> stashA$ ret (AExpr (toExpr$ fromExpr e `dot` sel)) 
                                         else atomVAsVoid v $ stashA$ ret AVoid
                           ]) :: ([EA Expr] -> ContMR EnvM TVoid AtomV)
       in 
                case choice of
                  Inl -> 
                    go arg₁ $ \_ go' -> go' g' >>= \t -> 
                    unbox (fromJust' $ forcePol (ddual tyAPol₁) w)
                          (fromJust' $ forcePolModApo  tyAPol₁  t)
                  Inr -> 
                    go arg₂ $ \_ go' -> go' g' >>= \t -> 
                    unbox (fromJust' $ forcePol (ddual tyAPol₂) w)
                          (fromJust' $ forcePolModApo  tyAPol₂  t)

       )

   (ANeg (_,arg@(A RATy{tyAPol})))     -> 
     let 
         f g = shiftM (\v -> termAsArgs (asPol tyAPol v) arg $ \e ->
                            g e $ \w -> unstashA$ atomVAsVoid w $ sNopA)
     in
     κ (sRev tyAPol) f
   

   (ARet ret) -> tAtom'' ret $ \tAtom'₁ -> κ sing $ \g -> upM $ \yield -> g [] $ yield . tAtom'₁ 

   (ABox p ret) -> revIsConvolution p $ go ret $ \_ go' -> κ sing $ \g -> go' g >>= \t -> return$ asPol p$ mkBox t

axPrim :: Atom True -> Atom False -> EnvM TVoid
axPrim src@(Atom _ (ARATy (APrim STrue  s₁)))
       dst@(Atom _ (ARATy (APrim SFalse s₂))) = pure$
             unwrapAtom_ src $ \src ->
             unwrapAtom_ dst $ \dst ->
             atomAsMemW  dst $ \case 
               Just l -> atomAsExpr src $ \e ->
                         stmExprA$ l =: e
               Nothing -> atomAsVoid src $ sNopA

instance NbE Atom TVoid EnvM where
  axL :: Atom False -> EnvM (CoTermP)
  axL a@Atom{atomV,atomTy=atomTy@(A RATy{tyATy})} = go 
    where
      fuc :: (TermP -> EnvM TVoid) -> EnvM (CoTermP)
      fuc x = fmap Up$ capture x

      fneg = dposP atomTy $ \p -> atomAsExpr (fromJust' $ unsafeUnwrapAtom a) $ \f -> callFunction f atomTy >>= \t -> mkNeg (asPol p t)

      go :: EnvM CoTermP
      go = case tyATy of
        APrim _ s -> return$ LPrim a


        ATuple ca cb -> fuc$ \(Tensor u v) -> 
          unwrapAtomM_ a $ \a -> unstash$ 
          atomAsMem a $ \(accessors -> Accessors{..}) -> 
          statementsA [
            stash$ tAtom₁ (π₁ ca) $ unbox' (forcePolUnsafeS u) 
           ,stash$ tAtom₁ (π₂ cb) $ unbox' (forcePolUnsafeS v) 
           ] 

        ABool -> fuc$ \(Sum choice t) ->
          unwrapAtomM_ a (\a -> unstash$ atomAsMem a $ \l -> 
                    statementsA [
                      case l of
                        Nothing -> sNopA
                        Just l  -> stmExprA$ l =: eBool (choiceToBool choice)
                     ,stash$ mkPos (forcePolUnsafe SPos t) $ \TUnit -> unstash sNopA
                     ]
                     )

        ASum ca cb -> fuc$ \(Sum choice t) ->
          let  f :: forall b'.
                       (((RichAtomTy b') -> Atom b')
                     -> ((RichAtomTy b') -> Atom b')
                     -> Atom b') -> EnvM TVoid
               f x = unwrapAtomM a
                (\a' κ -> unstash$ atomAsMem a' $ \(accessors -> Accessors{..}) -> 
                    statementsA [
                      case btag_w of
                        Nothing -> sNopA
                        Just l  -> stmExprA$ l =: eBool (choiceToBool choice)
                     ,stash$ κ$ x inl inr
                     ])
                (\a' -> tAtom₁ a' $ unbox' (forcePolUnsafeS t))
          in case choice of
               Inl -> f (\l r -> l ca)
               Inr -> f (\l r -> r cb)

        AUnit _   -> fuc$ \case
          TUnit -> 
            unwrapAtomM_ a $ \a ->
            unstashA$ atomAsVoid a $ sNopA
          no_unit -> error $ show no_unit
{-
        AVec0 _  _     -> fuc$ \(Vec _ f)  -> unwrapAtomM_ a $ \a -> unstashA$ atomAsVoid a$ sNopA
        AVec1 _ ca   -> fuc$ \(Vec _ f)    ->
          unwrapAtomM_ a $ \Atom{atomV} -> unstash$
          stash$ tAtom₁ (Atom atomV ca) $ unbox' (fromJust' $ forcePolS$ f 0)
-}
        AVec  _ len ca -> fuc $ \(Vec _ f) ->
          unwrapAtomM_ a $ \a -> unstash$ atomAsMem a $ \(accessors -> Accessors{..}) -> stash$
          rangeAEnv (fromJust' $ lengthVec len) $ \i ->
          tAtom₁ (idx len ca i) $ unbox' (forcePolUnsafeS$ f i)

        AVecPacked STrue  SApoN (fromJust' . lengthEnv -> len) ca cb -> fuc $ \(Vec _ f) ->
          unwrapAtomM_ a $ \a -> unstash$ atomAsMem a $ \case
            (accessors -> Accessors{..}) -> 
                cWithLocal tSizeT "j" $ \j ->
                  cWithLocal tSizeT "i" $ \i -> 
                      let i_c = eLen i - eLen (idx_skip (eLen j - 1)) in
                      statementsA [stmExprA$ i  =: toExpr (0 :: Length)
                                  ,stmExprA$ j  =: toExpr (0 :: Length)
                                  ,sWhileA (eTrue)  [
                                     statementsA [
                                        statementsA [
                                           sIfElseM [(eLen j ≥: eLen n_skip, sBreakA)]
                                         , (castExpr i_c <: castExpr (idx_skip (castExpr j))) >>=
                                              flip sWhileA [stash s,stmExprA$ ePreIncr i]
                                         , stmExprA$ ePreIncr j
                                         ]
                                         | s <- [tAtom₁ (idx_inl ca i_c) $ \t -> unbox' (forcePolUnsafeS$ f (eLen i)) (TermP$ TPos$ Sum Inl t)
                                                ,tAtom₁ (idx_inr cb i_c) $ \t -> unbox' (forcePolUnsafeS$ f (eLen i)) (TermP$ TPos$ Sum Inr t)]
                                         ]
                                  ]]

        AVecPacked SFalse SApoN (fromJust' . lengthEnv -> len) _ _ -> fuc $ \(Vec _ f) ->
          axR (Atom atomV $ (fromJust'$ richAtomTyPosToNeg atomTy)) $ \(Vec _ g) ->
            rangeAEnv len $ \i ->
            revIsConvolution (f i) $
            revIsConvolution (g i) $
            unbox (f i) (fromJust'$ forcePolS $ g i)
  
        ABox _ ca -> return$ tAtom₁ a LBox

        ASigmaSize _ tyVar ca -> fuc$ \(SigmaSize sz t) ->
          unwrapAtomM a
            (\a' κ -> unstash$ atomAsMem a' $ \(accessors -> Accessors{..}) -> 
              stash$ κ$ val (tyBindVarSize (tyVar,sz) ca))
            (\a' -> tAtom₁ a' $ unbox' (forcePolUnsafeS t))
                                  
        ALol{}    -> fneg 
        ANeg{}    -> fneg
        ARet{}    -> fneg
        APiSize{} -> fneg
        AWith{}   -> fneg
         
  axR :: Atom True -> (TermP -> EnvM TVoid) -> EnvM TVoid
  axR a κ = 
    unwrapAtomM_ a $ \a@Atom{atomV,atomTy = atomTy@(A RATy{tyATy})} -> unstash$
    --atomAsMem a $ \(accessors -> Accessors{..}) ->
    let res = go
            where
              lup f = stash$ capture κ >>= \κ' -> f κ'

              fpos = stash$ dposP atomTy $ \p -> atomAsExpr a$ \f -> callFunction f atomTy >>= \t -> mkPos (asPol p t) κ

              go = case tyATy of
                APrim _ _ -> stash$ κ (RPrim a)
                ATuple ca cb ->
                   atomAsMem a $ \(accessors -> Accessors{..}) ->
                   lup$ \κ -> return $ 
                   tAtom₁ (π₁ ca) $ \ta -> tAtom₁ (π₂ cb) $ \tb -> κ (Tensor ta tb)

  {-
                AVec0 _ ca   -> lup$ \κ -> return $ atomAsVoid a$ dposP ca $ \p -> κ $ Vec 0 (\_ -> asPol p unreachable)
                AVec1 _ ca   -> lup$ \κ -> return $ tAtom' atomV ca $ \ta -> κ $ Vec 1 (\_ -> ta)
  -}
                AVec  _ len ca ->
                  atomAsMem a $ \(accessors -> Accessors{..}) ->
                  lup$ \κ -> return $
                  tAtom'' ca $ \f ->
                  κ$ (Vec (fromJust' $ lengthVec len)) (\i -> let Atom{atomV} = idx len ca i in f atomV)

                AVecPacked STrue _ (fromJust' . lengthEnv -> len) ca cb ->
                  atomAsMem a $ \(accessors -> Accessors{..}) ->
                  cWithLocal tSizeT "j" $ \j ->
                      statementsA [
                        stmExprA$ j  =: (0 :: Length) 
                      , eLen (idx_skip (eLen j)) ==: 0 >>= \step ->
                        stmExprA$ j  =: eLen j + castExpr step
                      , stash$ κ =<< Vec len <$> capture (\i -> 
                         (upM $ \yield -> unstash$
                             let i_c =  i - eLen (idx_skip (eLen j - 1)) in
                             statementsA [
                                (i_c ≥: eLen (idx_skip (eLen j))) >>= \step -> stmExprA$ j =: eLen j + castExpr step
                               ,sIfElseM [((eLen j % 2) >>= (==: 0),
                                           statementsA [ stash$ tAtom₁ (idx_inl ca i_c) $ yield . TermP . TPos . Sum Inl ])
                                         ,(cElseM      , 
                                           statementsA [ stash$ tAtom₁ (idx_inr cb i_c) $ yield . TermP . TPos . Sum Inr ])]])
                             :: EnvM (Term Pos))]

                -- The skip sequence will have some padding for performance reasons.
                AVecPacked SFalse _ (fromJust' . lengthEnv -> len) ca cb ->
                  atomAsMem a $ \case
                    Nothing -> stash$ κ =<< Vec len <$> capture (\_ -> (shiftM $ \p -> mkPos p $ \(Sum choice t) ->
                                                case choice of
                                                  Inl -> tAtom₁ (Atom AVoid ca) $ unbox' (forcePolUnsafeS t) 
                                                  Inr -> tAtom₁ (Atom AVoid cb) $ unbox' (forcePolUnsafeS t)) :: EnvM (Term Neg))
                    (accessors -> Accessors{..}) ->
                        cWithLocal tSizeT "j" $ \j -> statementsA [
                              -- Adding some padding is allowed
                              stmExprA$ j =: (0 :: Length)
                            , stmExprA$ idx_skip (eLen j) =: (0 :: Length)
                            , stash$ κ =<< Vec len <$> capture ((\i -> shiftM (\p -> mkPos p $ \(Sum choice t) ->
                                   let 
                                       i_c = i - eLen (idx_skip (eLen j - 1))
                                       f :: forall b₁. (SingI b₁) => Length -> (Length -> Atom b₁) -> EnvM TVoid
                                       f pos self = unstash$ 
                                         statementsA [ (eLen j % 2) >>= \e -> (e ==: pos) >>= \b -> 
                                                         stmExprA$ j =: eLen j + castExpr b
                                                     , stmExprA$ idx_skip (eLen j) =: i_c + 1
                                                     , stash$ tAtom₁ (self i_c) $ unbox' (forcePolUnsafeS t)
                                                     ]
                                   in
                                   case choice of
                                     Inl -> f 1  (idx_inl ca) 
                                     Inr -> f 0  (idx_inr cb) 
                                   )) :: CIndex -> EnvM (Term Neg))

                            , stmExprA$ n_skip =: (eLen j + 1) 
                            ] 

                AUnit _ -> lup$ \κ ->
                  return$ atomAsVoid a $ κ TUnit

                ASum ca cb ->
                  atomAsMem a $ \(accessors -> Accessors{..}) ->
                  lup$ \κ -> return$ 
                  sIfElseA [(condIsChoice Inl btag, tAtom₁ (inl ca) $ κ . Sum Inl)
                            ,(cElse               , tAtom₁ (inr cb) $ κ . Sum Inr)]

                ABool      ->
                  atomAsMem a $ \(accessors -> Accessors{..}) ->
                  lup$ \κ -> return$
                  sIfElseA [(condIsChoice Inl self_bool, κ (Sum Inl (TermP$ TPos$ TUnit)))
                            ,(cElse                    , κ (Sum Inr (TermP$ TPos$ TUnit)))
                            ]

                ASigmaSize _ v ca' ->
                   atomAsMem a $ \(accessors -> Accessors{..}) -> 
                   lup$ \κ -> return$
                   let ca = tyBindVarSize (v, σlen) ca' in
                   tAtom₁ (val ca) $ κ . SigmaSize σlen

                ABox _ ca -> lup$ \κ -> return$ κ$ tAtom' atomV ca RBox

                AIdx{} -> fpos
                ANeg{} -> fpos
                ARet{} -> fpos
    in res

  axP a₁ a₂ = go a₁ a₂
     where
        go :: Atom True -> Atom False -> EnvM TVoid
        go src@(Atom _ (ARATy (APrim STrue  s₁)))
           dst@(Atom _ (ARATy (APrim SFalse s₂))) = axPrim src dst

        go a b = defaultAxP a b

  {- cutA1 :: (b ~ IsPos p)
        => SPol p
        -> Name -- ^ Suggested name for the atom
        -> RichAtomTy (IsPos p) -- TyPol (EName a) (ERef a) p
        -> (Term (Rev p) -> EnvM TVoid) -- ^ Function body
        -> (Atom b -> EnvM TVoid)  -- ^ Function consumer, atom will be the dual
        -> m r        -- ^ Program -}
  cutA1 p fname ty@(A RATy{..}) bl br = revIsConvolution p$ do
    case tyComm of 
      Nilplex -> case p of
        SNeg -> mix [bl$ tAtom (Atom AVoid (fromJust' $ richAtomTyRev ty))
                    ,br$ Atom AVoid ty]
        SPos -> mix [br$ Atom AVoid ty
                    ,bl$ tAtom (Atom AVoid (fromJust' $ richAtomTyRev ty))]   
      SimplexRead -> 
          allocate1' ty $ \v -> 
                mix [bl$ tAtom (Atom v (fromJust' $ richAtomTyRev ty))
                    ,br$ Atom v ty]
      SimplexWrite -> 
          allocate1' ty $ \v -> 
                mix [br$ Atom v ty
                    ,bl$ tAtom (Atom v (fromJust' $ richAtomTyRev ty))]
      Duplex -> unstash$ do
        name <- view envLocalContext >>= \case
          True  -> cident <$> freshFrom fname
          False -> pure$ litIdent fname 
        statementsA [stash$ mkFun name [] ty (\t -> revIsConvolution t$ bl$ fromJust' $ forcePolModApo (ddual p) t)
                    ,stash$ br$ Atom (AMem (eLit name)) ty
                    ]

  cutL = defaultCutL
  
  posToNegB t κ = case t of
    TAtom Atom{atomV,atomTy = (richAtomTyPosToNeg -> Just atomTy)} -> κ$ TAtom Atom{atomV,atomTy}
    _ -> defaultPosToNegB t κ

  mix = unstashA . statementsA . fmap stashA
  halt = unstashA $ sNopA
  schedule len κ = rangeAEnv len κ

  -- allocate :: Length -> IdDataPol q p -> ((CIndex -> Binding p) -> Binding q -> EnvM TVoid) -> EnvM TVoid
  allocate len tyd κ = 
    revIsConvolution2 tyd$ 
    -- 1. Decide target types
    translateDataType tyd $ \(atomTyD@(A RATy{..}), atomTyDW) -> 
    declareContM (aArray SimplexRead  SPos (pClosed len) (atomTyD)) $ \atomTyMem -> 
    allocate1 "a" atomTyMem $ \atomV₁ -> 
    -- 2. Handle copies
    declareContM (aArray SimplexWrite SPos (pClosed len) (atomTyDW)) $ \atomTyMemW -> do
        let atom_r₁ = Atom atomV₁ atomTyMem
        let atom_w₁ = Atom atomV₁ atomTyMemW
        binding_r <- capture$ \copies -> bVecB (copies * len) <$> 
          case (copies, len) of
            (1,_) -> return$ TAtom atom_r₁
            (_,_) -> do
                       μ <- capture (\i -> upM $ \yield -> 
                                indexAVec atom_r₁ $ \(_,v) -> do
                                  i' <- i // copies
                                  tAtom₁ (v i') $ \ta ->
                                    yield (forcePolUnsafe (dataPolRead tyd) ta)) 
                       return$ TPos$ Vec (copies * len) (\i -> asPol (dataPolRead tyd) $ μ i)

        let binding_w = bVecB len (TAtom atom_w₁)

        -- Double buffering
        let binding_upd κ = 
              allocate1 "swp" atomTyMem $ \atomV₂ ->
                let atom_r₂ = Atom atomV₂ atomTyMem
                    atom_w₂ = Atom atomV₂ atomTyMemW
                in
                indexAVec atom_r₁ $ \(_,fr₁) -> 
                indexAVec atom_w₁ $ \(_,fw₁) -> 
                indexAVec atom_w₂ $ \(_,fw₂) -> do
                    b <- bVec len <$> capture (\i -> upM $ \yield -> unstash$
                          localAtomVs atomTyD ["buf1","buf2"] $ \[b₁,b₂] -> stash$ do
                            buf_maybe_w <- capture $ \case 
                              (Sum Inr t) -> mixSeq [unstash$ b₁ `assignAtomV` atomV (fw₁ i)
                                                    ,unstash$ b₂ `assignAtomV` atomV (fw₂ i)
                                                    ,unbox' (forcePolUnsafeTerm t) (TermP (TPos TUnit))
                                                    ]
                              (Sum Inl t) -> mixSeq [unstash$ b₂ `assignAtomV` atomV (fw₁ i)
                                                    ,tAtom₁ (fw₂ i) $ unbox' (forcePolUnsafeTerm t)
                                                    ,unstash$ b₁ `assignAtomV` atomV (fw₂ i)
                                                    ]
  
                            mixSeq [tAtom₁ (fr₁ i) $ \ta -> (yield (TermP$ TPos$ Tensor ta (TermN$ TNeg$ Up buf_maybe_w)))
                                   ,unstash$ atomV (fw₁ i) `assignAtomV` b₁
                                   ,unstash$ atomV (fw₂ i) `assignAtomV` b₂
                                   ])
                    κ b 
  
        κ binding_r binding_upd binding_w


  allocate0 len tyd κ = 
    revIsConvolution2 tyd $ 
    translateDataType tyd $ \(_, atomTyDW) -> 
        declareContM (aArray SimplexWrite SPos (pClosed len) (atomTyDW)) $ \atomTyMemW -> do
            let binding_w = bVecB len (TAtom (Atom{atomTy = atomTyMemW,atomV=AVoid}))
            κ binding_w

  translateType :: TyPol (EName Atom) (ERef Atom) p {- ~ IdTyPol p -} -> ContMR EnvM TVoid (AType Atom (IsPos p))
  translateType ty = declareContM$ toRichAtomTy ty 

  translateArrayType :: SPol p -> ALength Atom -> TyPol (EName Atom) (ERef Atom) pa -> ContMR EnvM TVoid (AType Atom (IsPos p))
  translateArrayType p len tya = declareContM$ arrayToRichAtomTy p (pClosed len) tya

  cosliceM ops i =
    statementsA <$> sequence [ 
       sIfElseA <$> sequence [(,) <$> ((&&:) <$> (lb ≤: i) <*> (i <: ub)) <*> cont (i - lb)]
                              | (lb,ub,cont) <- ops]

  sizeCmp (a, b) ops = sIfElseA <$> sequence [(,) <$> lenCmpOp op a b <*> cont | (op,cont) <- ops]


  --nWhat wh vars | trace ("What " ++ show wh ++ show vars) False = undefined
  nWhat wh vars = case wh of
    Huh msg -> unstashA$ statementsA[
      pure$ pragmaWarning msg
     ,stmExprA $ (lit "failure") `call` [EA$ eStr msg]
     ]
    -- Interactions with the C runtime (numeric literals, operators,
    -- function calls.
    FFI (PrimLit str) -> do
      case vars of 
        [(CtxLinear b, TyA (ty' :^+ 1))] -> revIsConvolution ty'$ 
          translateType (ddual ty') $ \ty ->
            indexB1 b $ \ret' -> do
              let ret = fromJust' $ forcePol ty' ret'
              unbox ret (tAtom₀ (AExpr (lit (fromString str))) ty)
        _   -> errRet

    FFI (Call name) -> do
      pure (todo "Support FFI with size arguments")
      case vars of
        (CtxLinear zb, TyA (tyz' :^+ 1)):vars_arg ->
            revIsConvolution tyz'$ 
            indexB1 zb $ \zt' ->
            translateType (ddual tyz') $ \tyz -> 
            let zt = (fromJust' $ forcePol tyz' zt') in

            recurse (\case
                       (CtxLinear b, TyA (tyb :^+ 1)) -> \κ -> revIsConvolution tyb$
                          indexB1 b $ \t' ->
                          translateType tyb $ \ty -> 
                          let t = (fromJust' $ forcePol tyb t') in
                          termAsArgs t ty κ
                       (CtxSize  sz, _) -> \κ -> κ [EA $ toExpr sz]
                     ) vars_arg $ 
              \(concat -> args) ->
                case lookup name builtin of
                  -- The compiler has a built-in list of operators.
                  Just op ->
                    case (op, args) of
                      (NumUnOp fun, [EA arg])          -> yield$ fun arg
                      (NumBiOp fun, [EA arg₁,EA arg₂]) -> yield$ fun arg₁ arg₂
                      (CmpBiOp fun, [EA arg₁,EA arg₂]) -> yield$ fun arg₁ arg₂
                      (_          , (length -> arity)) -> huh $ "Unsupported operator " ++ name ++ "/" ++ show arity ++ "."
                    where
                      yield :: Expr a -> EnvM TVoid
                      yield e = unbox zt (tAtom₀ (AExpr e) tyz)

                  -- By default, a function is called with the given arguments.
                  Nothing -> unbox zt (tAtom₀ (AExpr (lit (fromString name) `call` args)) tyz)

        _ -> errRet
    FFI (Macro name)  -> (todo "Macro-based FFI")
    FFI a             -> crash $ "Unsupported FFI call " ++ show a ++ " " ++ show vars ++ "."
    (LLDeriv name)    -> deferM $ "Call to derivation " ++ name ++ " must be inlined"
    Builtin b         -> bif b
    where
      errRet = crash $ "FFI calls must consume a context of the form [A⊥, …], where A is primitive or 1." 

      bif b = case (b,vars) of
       (BIFSchedSeq, [(CtxLinear b, TyA (TyPol SPos ((T (ArrayTensor _ (T Bot))) :^ 1)))]) ->
        indexB1 b $ \t -> mkPos (fromJust' $ forcePol SPos t) $ \(Vec len f) ->
         rangeAEnv len $ \i ->
           mkNeg (fromJust' $ forcePol SNeg$ f i) >>= axT TUnit

       (BIFSchedPar, [(CtxLinear b, TyA (TyPol SPos ((T (ArrayTensor _ (T Bot))) :^ 1)))]) ->
        indexB1 b $ \t -> mkPos (fromJust' $ forcePol SPos t) $ \(Vec len f) ->
         rangeAPragmaEnv (pragmaOmp "parallel for") len $ \i -> 
           mkNeg (fromJust' $ forcePol SNeg$ f i) >>= axT TUnit

       (BIFIndex   , [(CtxLinear b, TyA (TyPol SNeg ((T (ArrayPar sz (T (Forall _ (T TSize) 
                     (T (Forall _ (T (TProp (_,_,_))) (T Bot))))))) :^ 1)))]) ->
         translateSize sz >>= \len ->
           indexB1 b $ \t -> mkNeg (fromJust' $ forcePol SNeg t) >>= \(Up μ) -> 
             pure$ μ (Vec len (\i -> TermP (TPos (SigmaSize i (TermP (TPos (RBox (TermP (TPos TUnit)))))))))

       (BIFCoalesce, [(CtxLinear b_seq', TyA (TyPol p₁ _))
                     ,(CtxLinear b_res', TyA (TyPol p₂  _))
                     ]) -> notImplemented {- do 
         let Just b_seq = forcePol p₁ b_seq'
         let Just b_res = forcePol p₂ b_res'
         indexB1 b_seq $ \t_seq ->
         indexB1 b_res $ \t_res ->
         case (p₁, p₂) of
           (SApoP, SApoN) ->
             cWithLocal tSizeT "ic" $ \ic ->
             mkPos t_seq $ \(Vec len f_seq) ->
             mkNeg t_res >>= \(Up μ) -> μ . Vec len <$> capture (\i -> 
               cWithLocal tBool "flag" $ \flag ->
               statementsA [stmExprA$ flag =: cTrue
                           ,cDoWhile (flag)  [
                              mkPos (f_seq ic) $ \Sum choice t ->
                                case choice of
                                  Inl -> yield (Sum Inl t) 
                                  Inr -> sNopA 
                             ,stmExprA$ ePreIncr ic
                             ]
                            ] 
              
              
         indexB1 b_seq $ \t_seq -> mkPos (fromJust' $ forcePol SPos t_seq) $ \(Vec len μ) ->
           indexB1 b_res $ \t_res ->
             notImplemented-}

       (_,           _) -> crash $ "Unsupported BIF " ++ show b ++ " " ++ show vars ++ "."
{-
mAlloc :: Length -> (MemBlock -> EnvM TVoid) -> EnvM TVoid
mAlloc 0 κ = κ (Block$ toExpr eNULL)
mAlloc len κ = unstash$ cWithLocal (tPointer tChar) "ptr" $ \ptr ->
  statementsA [stmExprA$ ptr =: cMalloc len
              ,stash$ κ (Block $ toExpr ptr)
              -- 3. Free the structure
              ,cFreeA (toExpr ptr)
              ]
-}
{-
termIsAtomWithTy :: Term p -> RichAtomTy (IsPos p) -> (Maybe (Atom (IsPos p)) -> EnvM TVoid) -> EnvM TVoid
termIsAtomWithTy t ty κ = revIsConvolution t$
  capture' (termIsAtom t) $ \case
    Just (a@(Atom{atomV,atomTy})) ->
      atomTy `tyCoercibleTo` ty >>= \case
        Just f  -> κ$ Just$ f atomV
        Nothing -> κ Nothing 
    Nothing -> κ Nothing
-}

termAsAtomWithTy :: Term p -> RichAtomTy (IsPos p) -> (Atom (IsPos p) -> EnvM TVoid) -> EnvM TVoid
termAsAtomWithTy t ty κ = revIsConvolution t$ 
  capture' (termIsAtom t) $ \case
    Right (a@(Atom{atomV,atomTy})) ->
      atomTy `tyCoercibleTo` ty >>= \case
        Just f  -> κ$ f atomV
        Nothing -> tAtom₁ a $ \t -> cutA1 (dpol t) "tae" ty (unbox t) κ
    Left b -> cutA1 (dpol t) "tae" ty (unbox' (termB b)) κ

termAsArgs :: (SingP p) => Term p -> RichAtomTy (IsPos p) -> ([EA Expr] -> EnvM TVoid) -> EnvM TVoid
termAsArgs t ty κ =
  termAsAtomWithTy t ty $ \a -> 
  case isVoidTy ty of
    True -> unstashA$ atomAsVoid a $ stashA$ κ []
    False -> case isByRef ty of
      True  -> unstash$ atomAsMem a (\(Just l) -> stash$ κ [EA$ toExpr$ amp l])
      False -> atomAsExpr a $ \e -> κ [EA e]

allocate1' = allocate1 "a"

allocate1 :: Name -> RichAtomTy b -> (AtomV -> EnvM TVoid) -> EnvM TVoid
allocate1 name atomTy@(A RATy{tyNode=tyNode',..}) κ | isTyWrite atomTy =
    case cTy atomTy of
      Just cty -> unstash$
          case tyNode' of
            Stack         -> cWithLocal cty name $ \ v -> stash$ κ (AMem v)
            _ -> do
              let atomTy = A RATy{tyNode=AllocDeferred,..} 
              let Just cty = cTy atomTy
              cWithLocal cty name $ \v -> do
              let a₀ = (Atom (AMem v) atomTy)
              statementsA[ 
                stash$ case tyNode' of
                  AllocDynamic  -> unwrapAtomM_ a₀ (\a -> κ$ atomV a )
                  Stored        -> unwrapAtomM_ a₀ (\_ -> κ$ atomV a₀)
                  StoredEager   -> (todo "Eager initialization not implemented")
                  AllocDeferred -> κ (AMem v)
               ,stash$ return$ destroyAtom a₀ ]
      Nothing -> κ AVoid
allocate1 name atomTy κ | isTyRead atomTy = allocate1 name (fromJust' $ richAtomTyRev atomTy) κ


dFunctionEnv ::  CType -> CIdent -> [(CIdent, CType)] -> EnvM TVoid -> EnvM TVoid
dFunctionEnv cty cident args body' = do
  local <- view envLocalContext
  body  <- ctxLocal body' 
  return $ do
    if not local
      then tell [TopLevelDecl (dFunDecl cty cident args)]
      else return ()
    dFunction cty cident args <$> body


mkFun :: (SingI b) => CIdent -> [CIdent] -> RichAtomTy b -> (forall p. (b ~ IsNeg p, SingI p) => Term p -> EnvM TVoid) -> EnvM TVoid
mkFun name argnames ty@(cFunTy -> (cTyArgs',cTyRet)) bl = go cTyArgs' argnames [] []
  where
    --κ args | flip trace False $ "mkFun:κ:" ++ show (erase ty) ++ ":" ++ show cTyArgs' ++ ":" ++ show args = notImplemented
    κ lvs = unpackArgs ty lvs bl

    go :: [Maybe CType] -> [CIdent] -> [(CIdent,CType)] -> [AtomV] -> EnvM TVoid
    go cTyArgs [] args lvalues = unstash$ do
      c <- freshFrom "arg"
      stash$ go cTyArgs [cident c] args lvalues

    go (Nothing:cTyArgs) cids args lvs = go cTyArgs cids args (AVoid : lvs)
    go (Just cTyArg:cTyArgs) (cid:cids) args lvs = go cTyArgs cids ((cid,cTyArg):args) (AMem (eLit cid) : lvs)

    go [] _ args lvs =
      case cTyRet of
        Nothing -> dFunctionEnv tVoid name (reverse args) (κ (reverse (AVoid : lvs)))
        Just cTyRet ->
          dFunctionEnv cTyRet name (reverse args) $ unstash$
            cWithLocal cTyRet "ret" $ \l ->
              statementsA [stash$ κ$ reverse$ (AMem l):lvs
                          ,sReturnA l
                          ] 
          
    unpackArgs :: (SingI b) => RichAtomTy b -> [AtomV] -> (forall p. (IsNeg p ~ b, PolProp p) => Term p -> EnvM TVoid) -> EnvM TVoid
    --unpackArgs ty vs _             | flip trace False $ "--> mkFun:unpackArgs:" ++ show (erase ty) ++ ":" ++ show vs = notImplemented
    unpackArgs ty@(ARATy tya) vs κ = notIsConvolution ty$ case tya of
      AIdx p _ ty₁    |  v:vs <- vs             -> revIsConvolution p$ do
        bt <- TNeg . Up <$> capture (\(Vec _ f) -> 
          unpackArgs ty₁ vs $ unbox' (fromJust' $ forcePolS$ (f (atomVAsLength v))))
        κ $ asPol (ddual p) $ termB bt
      APiSize tyVar ty₁ | (AMem (fromExpr . castExpr -> v):vs) <- vs             -> unpackArgs (tyBindVarSize (tyVar, v) ty₁) vs $ 
        κ . TermP . TPos . SigmaSize v
      ALol (ptr,ty₁) ty₂ | v:vs <- vs             -> unpackArgs ty₂ vs $ tAtom' (derefAtomV ptr v) ty₁ $ ((κ . TermP . TPos) .) . Tensor
      ANeg (ptr,ty₁)     | [v,AVoid]  <- vs       -> notIsConvolution ty₁$ tAtom' (derefAtomV ptr v) ty₁ κ
      ARet ty₁           | [v]  <- vs             -> tAtom' v (fromJust' $ richAtomTyRev ty₁) κ
      ABox p ty₁                                -> revIsConvolution p$ unpackArgs ty₁ vs $ \t -> κ $ asPol (ddual p) $ mkBox t
      AVec1 p ty₁                               -> revIsConvolution p$ unpackArgs ty₁ vs $ \t -> (asPolM (ddual p) $ mkVec1 t) >>= κ
      AWith ty₁ ty₂    | [AMem tag,args,ret] <- vs   -> unstashA$ 
         sIfElseA [(condIsChoice Inl (fromExpr$ castExpr$ tag), stashA$
                                          atomVAsMemW args $ \(accessors -> Accessors{..}) ->
                                          let args = inlArgs ty₁ in
                                          atomVAsMemW ret $ \(accessors -> Accessors{..}) ->
                                          let ret  = inlRet  ty₁  in
                                          unpackArgs ty₁ (args ++ [ret]) $ κ . TermP . TPos . Sum Inl)
                  ,(cElse               , stashA$
                                          atomVAsMemW args $ \(accessors -> Accessors{..}) ->
                                          let args = inrArgs ty₂ in
                                          atomVAsMemW ret $ \(accessors -> Accessors{..}) ->
                                          let ret  = inrRet  ty₂  in
                                          unpackArgs ty₂ (args ++ [ret]) $ κ . TermP . TPos . Sum Inr)
                  ] 

      t               | not (isFunctionType t) -> error$ "Can't unpack non-function type" ++ show t
      t                                        -> error$ "Internal error: " ++ show (erase t) ++ ", " ++ show vs



tAtom₀ :: forall p b. (IsPos p ~ b, SingI p) => AtomV -> RichAtomTy b -> Term p
tAtom₀ x y = tAtom' x y (fromJust' . forcePolModApoS)

tAtom' :: AtomV -> RichAtomTy b -> (forall p. (IsPos p ~ b, PolProp p, b ~ Not (Not b)) => Term p -> α) -> α 
tAtom' atomV atomTy κ = tAtom'' atomTy $ \tAtom'₁ -> κ$ tAtom'₁ atomV

tAtom'' :: RichAtomTy b -> (forall p. (IsPos p ~ b, PolProp p, b ~ Not (Not b)) => (AtomV -> Term p) -> α) -> α 
tAtom'' atomTy@(A RATy{tyAPol}) κ =
  let p = tyAPol in
  revIsConvolution p$ notIsConvolution (sIsPos p)$ κ$ \atomV -> asPol p$ tAtom (Atom{atomV,atomTy})

tAtom₁ :: Atom b -> (forall p. (IsPos p ~ b, PolProp p, b ~ Not (Not b)) => Term p -> α) -> α 
tAtom₁ Atom{atomV,atomTy} = tAtom' atomV atomTy
 
indexAVec :: (SingI b) => Atom b -> (forall b₁. SingI b₁ => (Length, CIndex -> Atom b₁) -> EnvM TVoid) -> EnvM TVoid
indexAVec a κ =
  unwrapAtomM_ a $ \a@Atom{atomV,atomTy=atomTy@(A RATy{..})} -> unstash$ 
  case tyATy of
    AVec0 _ ca     -> atomAsVoid a$ stash$ κ (0, \_ -> Atom AVoid ca)
    AVec1 _ ca     -> stash$ κ (1, \_ -> Atom atomV ca)
    AVec  _ len ca -> atomAsMem a $ \(accessors -> Accessors{..}) -> stash$ κ (fromJust' $ lengthVec len, \i -> idx len ca i)

assignAtomV :: (Applicative m) => AtomV -> AtomV -> m Code 
assignAtomV dst src =
  atomVAsMemW dst $ \case
    Nothing  -> atomVAsVoid src $ sNopA
    Just dst ->
      atomVAsExpr src $ \src ->
      stmExprA $ dst =: src

localAtomVs :: (MonadFresh m) => RichAtomTy b -> [Name] -> ([AtomV] -> m Code) -> m Code
localAtomVs ty names κ =
  case cTy ty of
    Nothing  -> κ [AVoid | _ <- names] 
    Just cty -> cWithLocals cty names $ κ . fmap AMem
              
derefAtomV :: Bool -> AtomV -> AtomV
derefAtomV False = id
derefAtomV True = \case
  AExpr e -> AExpr (toExpr$ star$ fromExpr e)
  AMem  l -> AMem  (star l)
  AVoid   -> AVoid
  ALen  _ -> error "Can't dereference length"
