{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
-- | Renderings of transformation rules for the linear logic sequent calculus
module Reductions where
import Data.Monoid
import LL
import Rules
import TexPretty
import MarXup.Tex
import Control.Lens hiding (Choice)
import Fresh
import Data.Default

fillTypes = id

fillDeriv :: Name -> Ctx' name ref -> Ctx' name ref -> Seq' name ref -> Deriv' name ref
fillDeriv a b c seq = fillTypes $ D0 $ Deriv a
                      (SeqCtx (b ^. mapping (from tupled))
                              (c ^. mapping (from tupled))) seq

cutSwap_ = mkDeriv "cutSwap_" (theta ++ sizeCtx[szN]) [gamma,deltaN] $ fuseNr szN "x" "y" (meta "A") (whatA`using` gamma `using'` "x") (whatB`using` delta `using'` "y")

cutAssoc1 = mkDeriv "cutAssoc1" (theta ++ sizeCtx[szN,szM]) [gamma,deltaM,xiMN] $ fuseNr (sizeMul szM szN) "y" "_y" (meta "B")
               (cutNr szM "x" "_x" (meta "A") (whatA `using` gamma `using'` "x") (whatB`using` delta `using'` "_x"`using'` "y"))
                (whatC`using` xi `using'` "_y")

cutAssoc1R = mkDeriv "cutAssoc1R" (theta ++ sizeCtx[szN,szM]) [gammaM,deltaMN,xi] $ fuseNl szM "y" "_y" (meta "B")
               (cutNl szN "x" "_x" (meta "A") (whatA `using` delta `using'` "x") (whatB`using` gamma `using'` "_x"`using'` "y"))
                (whatC`using` xi `using'` "_y")

cutSplit = mkDeriv "cutSplit" (theta ++ sizeCtx[szN,szM]) [gamma,deltaMpN] $ fuseNr (sizeAdd szM szN) "z" "_z" (neg $ meta "A")
           (S0$ Split "z" szM "x" "y" (r0$ whatA `using'` "x" `using'` "y" `using` gamma)) (whatB `using` delta `using'` "_z")

cutSplitR = mkDeriv "cutSplit" (theta ++ sizeCtx[szN,szM]) [gamma,deltaMpN] $ fuseNl (sizeAdd szM szN) "_z" "z" (meta "A")
            (whatB `using` delta `using'` "_z")(S0$ Split "z" szM "x" "y" (r0$ whatA `using'` "x" `using'` "y" `using` gamma))

cutAx = mkDeriv "cutAx" theta [gamma,("w",unit $ neg $ meta "A")] $
              fuse1 "x" "y" (meta "A") ((S0$ What "a" [ElemLinear "x"] [0]) `using` gamma) (S0$ Ax "y" "w")

cutParCross' β sz f g = mkDeriv "cutParCrossL'" (theta++sizeCtx[sz]) [gammaSz sz,delta,xiSz sz]
              (fuseNr sz "_z" "z" (T (f (meta "A") :*: g (meta "B")))
               (S0$ Par β "_z" "_x" (r0$ whatA`using` gamma `using'` "_x")
                               "_y" (r0$ whatB`using` delta `using'` "_y"))
               (S0$ Cross β "z" "x" "y" (r0$ whatC`using`xi `using'` "x"`using'` "y")))


cutParCross = cutParCross' False
cutWithPlus' :: Boson -> Choice
                -> (forall name ref. Type' name ref -> Type' name ref)
                -> (forall name ref. Type' name ref -> Type' name ref)
                -> IdDeriv
cutWithPlus' β b f g = runFreshM $ fromNamesLiberal $
                fillDeriv "cutWithPlus'" theta [gamma,delta]
                (fuse1 "z" "_z" (T (f (meta "A") :+: g (meta "B")))
                  (S0$ With β "z" b "x" (r0$ whatA`using` gamma `using'` "x"))
                  (S0$ Plus {zElim = "_z"
                       ,inl = Branch "_x" (r0$ whatB`using'` "_x" `using` delta)
                       ,inr = Branch "_y" (r0$ whatC `using'` "_y" `using` delta) })
                    )

cutWithPlus :: Choice
                -> (forall name ref. Type' name ref -> Type' name ref)
                -> (forall name ref. Type' name ref -> Type' name ref)
                -> IdDeriv
cutWithPlus = cutWithPlus' False

cutUnit' β = mkDeriv "cutUnit" theta [gamma] $ fuse1 "z" "_z" (T One) (S0$ SBot "z") (S0$ SOne β "_z" (r0$ whatA`using` gamma))
cutUnit = cutUnit' False

cutQuant' β f g = mkDeriv "cutQuant" (theta) [gamma,xi] $
           fuse1 "z" "_z" (T (Exists "α" (meta "k") (f $ metaSyntactic True True "A" [var "α"])))
                         (S0$ TApp β "z" (g $ meta "B") "x" (r0$ whatA`using` gamma `using'` "x"))
                         (S0$ TUnpack "_z" (meta "k") "β" "_x" (r0$ whatB`using`xi  `using'` "_x"))
cutQuant f g = cutQuant' False f g

cutBigL = mkDeriv "cutBigL" (theta ++ sizeCtx [szN,szM]) [gammaN,deltaM,xi] $
          fuse1 "z" "_z" (bigTensor (sizeAdd szN szM) $ meta "A")
                    (S0$ BigPar    {zElim = "z", branches = [(szN, Branch "x" (r0$ whatA `using` gamma `using'`  "x")),
                                                             (szM, Branch "y" (r0$ whatC `using` delta `using'` "y"))]})
                    (S0$ BigTensor "_z" "_x" $ (r0$ whatB `using` xi `using'` "_x"))

cutBigSimpl = mkDeriv "cutBigSimpl" (theta ++ sizeCtx [szN]) [gammaN,xi] $
          fuse1 "z" "_z" (bigTensor szN $ meta "A")
                    (S0$ BigPar    {zElim = "z", branches = [(szN, Branch "x" (r0$ whatA `using` gamma `using'` "x"))]})
                    (S0$ BigTensor "_z" "_x" (r0$ whatB `using` xi `using'` "_x"))
{-
cutFoldmap = mkDeriv "cutFoldmap" theta [gamma2N,delta2N,xi] $ fuseNl (SizeExp szN) "z" "_z" (meta "A")
          (whatA `using` gamma `using'` "z")
          Foldmap {fmSz = "n"
                  ,fmTyM = (metaSyntactic True True "B" [var "n"])
                  ,fmArr = ["_z","?δ"]
                  ,fmMapTrg = "w"
                  ,fmMap =(whatB `using'` "_z" `using` delta) --  mapper
                  ,fmMixL = "x", fmMixR = "y", fmMixTrg = "v"
                  ,fmMix = whatC -- combiner
                  ,fmMon = "w"  -- a better name for this is "fmResult"
                  ,fmCont = whatD -- result
            }
-}
cutFoldmap2 = mkDeriv "cutFoldmap" (theta ++ sizeCtx[szN])
              [gammaN,deltaN,xi] $ fuseNl szN "z" "_z" (meta "A")
          (whatA `using` gamma `using'` "z")
          (S0$ Foldmap2 {fmSize = szN
                  ,fmTyM = tB 
                  ,fmArr = ["_z","?δ"]
                  ,fmUnitTrg = "y"
                  ,fmUnit = r0$ whatE `using'` "y" 
                  ,fmMapTrg = "w"
                  ,fmMap =(r0$ whatB `using'` "_z" `using` delta `using'` "w") -- mapper
                  ,fmMixL = "x", fmMixR = "y", fmMixTrg = "v"
                  ,fmMix = r0$ whatC `usings` ["x","y","v"] -- combiner
                  ,fmMon = "w"  -- a better name for this is "fmResult"
                  ,fmCont = r0$ whatD `using` xi `usings` ["w"] -- result
            })
{-
cutBang' β = mkDeriv "cutBang" theta [(mempty, Bang (meta "Γ")), delta] $
          fuse1 "z" "_z" (Bang $ meta "A") (Offer β "x" "z" (whatA `using` (mempty, Bang (meta "Γ")) `using'` "x")) (Demand "_x" "_z" (whatB`using` delta `using'` "_x"))
cutBang = cutBang' False
-}

cutSaveOffer'' β f = mkDeriv "cutOffer" theta [gamma] $
          fuse1 "z" "_z" (T$ Bang $ f $ meta "A") (S0$ Offer β "z" "x" (r0$ whatA `using'` "x"))
                        (S0$ Save "_z" "_x"
                            (S'$ Load "_x" sizeOne "y" (r0$ whatB `using` gamma `using'` "y")))

cutSaveOffer' = cutSaveOffer'' False
cutSaveOffer = cutSaveOffer' id

commutPar sz f g = mkDeriv "commutPar" (theta++sizeCtx[sz]) [("?γ",tgamma :^ sz) ,("w",(T (f (meta "A") :|: g (meta "B"))):^sz),delta, ("?ξ",txi :^ sz)] $
    fuseNl sz "x" "y" (meta "C")
        (whatA `using` xi `using'` "x")
        (S0$ Par False "w" "u" (r0$ whatB `using` gamma `using'` "u")
                           "v" (r0$ whatC `using` delta `using'` "v" `using'` "y"))

commutCross sz = mkDeriv "commutCross" (theta ++ sizeCtx[sz]) [gamma,("w",(T (meta "B" :*: meta "C")):^sz),delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ Cross False "w" "u" "v" (r0$ whatB `using` delta `using'` "u" `using'` "v" `using'` "y"))

commutPlus = mkDeriv "commutPlus" theta [gamma,("w",(T (meta "B" :+: meta "C")):^sizeOne),delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ Plus "w" (Branch "u"  (r0$ whatB `using` delta `using'` "u" `using'` "y"))
                  (Branch "v"  (r0$ whatC `using` delta `using'` "v" `using'` "y")))

commutSizeCmp = mkDeriv "commutSizeCmp" (theta ++ sizeCtx[szN,szM]) [gamma,delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ SizeCmp szN szM [(Ge, r0$ whatB `using` delta `using'` "y")
                             ,(Le, r0$ whatC `using` delta `using'` "y")
                             ])

commutWith b f g = mkDeriv "commutWith" theta [gamma,(("w",(T (f (meta "B") :&: g (meta "C"))) :^ sizeOne)),delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ With False "w" b "u" (r0$ whatB `using` delta `using'` "u" `using'` "y"))

commutForall f g = mkDeriv "commutForall" theta [gamma,("w",T (Forall "α" (T TType) (f $ metaSyntactic True True "B" [var "α"])) :^ sizeOne),delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ TApp False "w" (g $ meta "B") "u" (r0$ whatB `using` delta `using'` "u" `using'` "y"))

commutExists = mkDeriv "commutExists" theta [gamma,("w",T (Exists "α" (T TType) (metaSyntactic True True "B" [var "α"])) :^ sizeOne),delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ TUnpack "w" (meta "k") "β" "u" (r0$ whatB `using` delta `using'` "u" `using'` "y"))

commutSave = mkDeriv "commutSave" theta [gamma,("w",T (Bang (meta "B")) :^ sizeOne),delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ Save "w" "u" (r0$ whatB `using` delta `using'` "y"))


commutLoad = mkDeriv "commutLoad" (theta++[("w",meta "B" :^ sizeOne)]) [gamma,delta] $
    fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ Load "w" sizeOne "u" (r0$ whatB `using` delta `using'` "u" `using'` "y"))

commutBigPar = mkDeriv "commutBig" (theta ++ sizeCtx[szN,szM]) [gammaN,xiM,("w", bigPar (sizeAdd szN szM) (meta "B") :^ sizeOne),deltaN] $
    fuseNl szN "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (S0$ BigPar {zElim = "w", branches = [(szN, Branch "u" (r0$ whatB `using` delta `using'` "u" `using'` "y")),
                                          (szM, Branch "v" (r0$ whatC `using'` "v" `using` xi)) ]})

commutBigSeq = runFreshM$ do
    seqB <- bigSeqPol (sizeAdd szN szM) (meta "B")
    mkDerivM "commutBigSeq" (theta ++ sizeCtx[szN,szM]) [gammaN,xiM,("w", seqB :^ sizeOne),deltaN] $
        fuseNl szN "x" "y" (meta "A")
            (whatA `using` gamma `using'` "x")
            (S0$ BigSeq {fence = [SeqStep szN [("w", "u")] (r0$ whatB `using` delta `using'` "u" `using'` "y")
                                ,SeqStep szM [("w", "v")] (r0$ whatC `using'` "v" `using` xi)
                                ]
                        })
                            

commutBigTensor = mkDeriv "commutBig" (theta ++ sizeCtx[szN]) [gammaN,("w", bigTensor szN (meta "B") :^ sizeOne),delta] $
-- commutBigTensor = mkDeriv "commutBig" theta [gamma,("w", bigTensor (sizeAdd szN szM) (meta "B") :^ sizeOne),delta] $
    fuseNl szN "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        -- (BigTensor szN "z" "w" "y" $ whatB `using` delta `using'` "w"`using'` "z")
        (S0$ BigTensor "w" "z" (r0$ whatB `using` delta `using'` "z" `using'` "y"))

commutGen = mkDeriv "commut" theta [gamma,("w",tC :^ sizeOne),delta] $
   fuse1 "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (whatB `using` delta `using'` "w" `using'` "y")

commutSync = mkDeriv "commutSync" (theta++sizeCtx[szN,szM,szK]) [gammaN,delta,xi] $
    fuseNl szN "x" "y" (meta "A")
    (whatA `using` gamma `using'` "x")
    (freezeSeq1 szK szM (meta "B") "v" "w" (whatB `using` delta `using'` "v" ) (whatC `using` xi `using'` "w"  `using'` "y" ))
        
commutCut1 = mkDeriv "commutCut1" (theta) [gamma,delta,xi] $
    fuse1 "x" "y" (meta "A")
    (whatA `using` gamma `using'` "x")
    (cut1  "v" "w" (meta "B") (whatB `using` delta `using'` "v" ) (whatC `using` xi `using'` "w"  `using'` "y"))

commutLoop = mkDeriv "commutSync" (theta++sizeCtx[szN,szM]) [gammaN,delta,xi] $
    fuseNl szN "x" "y" (meta "A")
    (whatA `using` gamma `using'` "x")
    (S0$ Sync
           [(1, tA, "w", Just (SyncUpdate szM "u"), Nothing)]
           (r0$ whatB `using` delta `using'` "w")
           (Just (r0$ whatC `using` xi `using'` "u" `using'` "y"))
           Nothing)

commutMix = mkDeriv "commutMix" (theta) [gamma,delta,xi] $
    fuseNl 1 "x" "y" (meta "A")
        (mix (whatA `using` gamma `using'` "x") (whatB `using` delta))
        (whatC `using` xi `using'` "y")

commutMix1 = mkDeriv "commutMix" (theta++sizeCtx[szN]) [gammaN,deltaN,xi] $
    fuseNl szN "x" "y" (meta "A")
        (mix (whatA `using` gamma `using'` "x") (whatB `using` delta))
        (whatC `using` xi `using'` "y")
 
commutMix2 = mkDeriv "commutMix2" (theta++sizeCtx[szN]) [gammaN,deltaN,xi] $
    fuseNl szN "x" "y" (meta "A")
        (mix (whatA `using` gamma) (whatB `using` delta  `using'` "x"))
        (whatC `using` xi `using'` "y")
 
commutMix3 = mkDeriv "commutMix3" (theta++sizeCtx[szN]) [gammaN,delta,xi] $
    fuseNl szN "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (mix (whatB `using` delta `using'` "y") (whatC `using` xi))
 
commutMix4 = mkDeriv "commutMix4" (theta++sizeCtx[szN]) [gammaN,delta,xi] $
    fuseNl szN "x" "y" (meta "A")
        (whatA `using` gamma `using'` "x")
        (mix (whatB `using` delta `using'` "y") (whatC `using` xi))
 
commutSplit = mkDeriv "cutSplit" (theta ++ sizeCtx[szN,szM]) [gamma,deltaMpN] $ fuseNr (sizeAdd szM szN) "z" "_z" (neg $ meta "A")
           (S0$ Split "z" szM "x" "y" (r0$ whatA `using'` "x" `using'` "y" `using` gamma)) (whatB `using` delta `using'` "_z")

commutFoldmap = mkDeriv "commutFoldmap" (theta++sizeCtx[szN]) [gamma,deltaN,xi] $
    fuse1 "x" "y" (meta "A")
        (S0$ Foldmap2 ["?δ"] szN tB 
         "y_" (r0$ whatE `using'` "y_")
         "y_" (r0$ whatA `using` delta `using'` "y_")
         "u" "v" "w" (r0$ whatB `usings` ["u","v","w"])
         "z" (r0$ whatD `using` gamma `using'` "x"`using'` "z"))
    (whatC `using` xi `using'` "y")
        
fuseMBigSeq = runFreshM$ do
  seqA <- (bigSeqPol (szN + szM) tA)
  mkDerivM "fuseMBigSeq" (theta++sizeCtx[szN,szM]) [gammaMpN,deltaN,xiM] $
    fuseNl 1 "x" "y" seqA 
    (S0$ BigSeq$ 
       [SeqStep {
           seqSz = szM + szN,
           seqChunk = [("x","x₁")],
           seqProg = Seqn def $ What "a" [ElemLinear (fst gamma), ElemLinear "x₁"] []
        }
       ])

    (S0$ BigSeq $ 
      [SeqStep {
          seqSz = szN,
          seqChunk = [("y","y₁")],
          seqProg = r0$ whatB `using'` "y₁" `using` delta
       },
       SeqStep {
          seqSz = szM,
          seqChunk = [("y","y₂")],
          seqProg = r0$ whatC `using'` "y₂" `using` xi
       }])


fuseMBigSeqCmp op = runFreshM$ do
  seqA <- bigSeqPol (szN + szM) tA
  mkDerivM "fuseMBigSeq" (theta++sizeCtx[szN,szM]++[("p",unit$ T$ TProp (szN,op,szM))]) [gammaM1,gammaN2,deltaN1,deltaM2] $
    fuseNl 1 "x" "y" seqA 
    (S0$ BigSeq$ 
       [SeqStep {
           seqSz = szM, 
           seqChunk = [("x","x₁")],
           seqProg = r0$ whatA `using` gamma1 `using'` "x₁"
        },
        SeqStep {
           seqSz = szN,
           seqChunk = [("x","x₂")],
           seqProg = r0$ whatB `using` gamma2 `using'` "x₂"
        }
        
       ])

    (S0$ BigSeq $ 
      [SeqStep {
          seqSz = szN,
          seqChunk = [("y","y₁")],
          seqProg = r0$ whatC `using'` "y₁" `using` delta1
       },
       SeqStep {
          seqSz = szM,
          seqChunk = [("y","y₂")],
          seqProg = r0$ whatD `using'` "y₂" `using` delta2
       }])


assocRules :: [(TeX,IdDeriv)]
assocRules = [("Associativity-L",cutAssoc1)
             ,("Associativity-R",cutAssoc1R)]

structRules = assocRules ++
              [ ("Ax",cutAx)
               -- ,("Split",cutSplit)
              , ("Split",cutSplitR)
              -- , ("loop fusion",cutFoldmap)
              -- , ("loop fusion 2",cutFoldmap2)
              ]
syncRules =
    [
      -- (math par<>"⊗L",cutParCrossL szN id id)
      (math par<>"⊗R",cutParCross sizeOne id id)
    , (amp<>"⊕1",cutWithPlus Inl id id)
    , (amp<>"⊕2",cutWithPlus Inr id id)
    , ("⊥1",cutUnit)
    -- , ("∃∀",cutQuant id id)
    , ("n"<> math par <> "⊗",cutBigL)
    -- , ("?!", cutSaveOffer)
    , ("n§", fuseMBigSeq)
    ]

pushRules = [
             ("κ⊗",commutCross sizeOne)
            -- ,("κ⅋L",commutParL id id)
            ,("κ⅋",commutPar sizeOne id id)
            ,("κ⊕",commutPlus)
            ,("κcompare",commutSizeCmp)
            -- ,("κ∀",commutForall id id)
            -- ,("κ∃",commutExists)
            -- ,("κS",commutSave)
            -- ,("κL",commutLoad)
            ,("κn⊗",commutBigTensor)
            ,("κn⅋",commutBigPar)
            ,("κn§",commutBigSeq)
            --,("κfoldmap",commutFoldmap)

            ,("κsync",commutSync)
            ,("κloop",commutLoop)
            ,("κsplit",commutSplit)
            ,("κ&1",commutWith Inl id id)
            -- ,("κ&2",commutWith Inr id id)
            ]

allRedRules,syncRules,structRules,pushRules,mergeRules :: [(TeX,IdDeriv)]
allRedRules = structRules ++ syncRules ++ pushRules ++ mergeRules

{-
altParPush = fillDeriv "altParPush" ["Θ"] (derivContext parRule ++ [xi]) (cut1 "z" "_z" (meta "C") 3 (Par False "_x" "_y" 2 whatA whatB) whatB)

questRuleProper β  = Deriv "questRule" ["Θ"] [("?γ",Bang $ meta "Δ"),("z",Quest (meta "A"))] $ Offer β "_x" 1 whatA
                     -- This version does not generate a nice output sometimes, but is more proper.

questPush = fillDeriv "questPush" ["Θ"] [("?δ",Bang $ meta "Δ"),
                                         ("z", Quest (meta "A")),
                                         ("?x", Bang $ meta "Ξ")] $
            cut1 "z" "_z" (Quest $ meta "C") 2
              (derivSequent $ questRuleProper False)
              whatC

pushRules = (textual "κ⅋0", altParPush):(textual "κ?", questPush) :
   [(textual "κ"<>seqName s,
     fillDeriv ("pushRule:" ++ derivName d) ["Θ"] (derivContext d ++ [xi])
      (cut1 "z" "_z" (meta "C")
           l whatC))
   | d <- map fillTypes [
       parRule,
       crossRule,
       oneRule,
       plusRule,
       withRule Inl,
       zeroRule,
       forallRule,
       existsRule
       -- bangRule,
       -- weakenRule False,
       -- contractRule False
       ],
     let l = length $ derivContext d
         s = derivSequent d
   ]

-}
--    | Merge {splitSz :: Size' name ref, xIntr :: name, zElim, wElim :: ref, cont :: Seq' name ref} -- should 'go through' all rules. here for meta reasoning.

mergeFoldmap = mkDeriv "mergeFoldmap" (theta ++ sizeCtx[szM,szN]) [gamma,("x",tA :^ szM),("y",tA :^ szN),deltaMpN]
             $ S0$ Merge "x" "y" szM "z"
             $ S'$ Foldmap2 {fmSize = sizeAdd szM szN
                       ,fmTyM = tB
                       ,fmArr = ["z","?δ"]
                       ,fmUnitTrg = "y_"
                       ,fmUnit = r0$ whatE `using'` "y_"
                       ,fmMapTrg = "w"
                       ,fmMap = r0$ whatB `using'` "z" `using` delta `using'` "w" -- mapper
                       ,fmMixL = "x", fmMixR = "y", fmMixTrg = "v"
                       ,fmMix = r0$ whatC `usings` ["x","y","v"] -- combiner
                       ,fmMon = "w"  -- a better name for this is "fmResult"
                       ,fmCont = r0$ whatD `using` gamma `using'` "w" -- result
            }

mergeBigPar =  mkDeriv "mergeBigPar" (theta++sizeCtx[szM,szN])
                 [("w",bigPar (sizeAdd szM szN) tB :^ sizeOne)
                 ,("x",tA :^ szM),("y",tA :^ szN),deltaMpN]
             $ S0$ Merge "x" "y" szM "z"
             $ S'$ BigPar "w" [(sizeAdd szM szN,Branch "v" $ r0$ whatA `using'` "z" `using'` "v" `using` delta)
                          ]
mergeCutN =  mkDeriv "mergeCutN" (theta++sizeCtx[szM,szN])
                 [gammaMpN,delta,("x",tA :^ szM),("y",tA :^ szN)]
             $ S0$ Merge "x" "y" szM "z"
             $ r0$ cutNl (szN + szM) "v" "w" tB
                            (whatA `using'` "z" `using'` "v" `using` gamma)
                            (whatB `using'` "w" `using` delta)

mergeBigSeq =  runFreshM$ do
  seqB <- bigSeqPol (sizeAdd szM szN) tB
  mkDerivM "mergeBigPar" (theta++sizeCtx[szM,szN])
                 [("w",seqB :^ sizeOne)
                 ,("x",tA :^ szM),("y",tA :^ szN),deltaMpN]
             $ S0$ Merge "x" "y" szM "z"
             $ S'$ BigSeq [SeqStep {
                               seqSz = sizeAdd szM szN
                              ,seqChunk = [("w","v")]
                              ,seqProg = r0$ whatA `using'` "z" `using'` "v" `using` delta
                              }]

mergeRules = [{-("merge-fold",mergeFoldmap)-}
              ("merge-n§",mergeBigSeq)
             ,("merge-n⅋",mergeBigPar)
             ,("merge-ncut",mergeCutN)
             ]

