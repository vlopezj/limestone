{-# OPTIONS_GHC -w #-}
{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}
module Parser.PrintMx where

-- pretty-printer generated by the BNF converter

import Parser.AbsMx
import Data.Char


-- the top-level printing method
printTree :: Print a => a -> String
printTree = render . prt 0

type Doc = [ShowS] -> [ShowS]

doc :: ShowS -> Doc
doc = (:)

render :: Doc -> String
render d = rend 0 (map ($ "") $ d []) "" where
  rend i ss = case ss of
    "["      :ts -> showChar '[' . rend i ts
    "("      :ts -> showChar '(' . rend i ts
    "{"      :ts -> showChar '{' . new (i+1) . rend (i+1) ts
    "}" : ";":ts -> new (i-1) . space "}" . showChar ';' . new (i-1) . rend (i-1) ts
    "}"      :ts -> new (i-1) . showChar '}' . new (i-1) . rend (i-1) ts
    ";"      :ts -> showChar ';' . new i . rend i ts
    t  : "," :ts -> showString t . space "," . rend i ts
    t  : ")" :ts -> showString t . showChar ')' . rend i ts
    t  : "]" :ts -> showString t . showChar ']' . rend i ts
    t        :ts -> space t . rend i ts
    _            -> id
  new i   = showChar '\n' . replicateS (2*i) (showChar ' ') . dropWhile isSpace
  space t = showString t . (\s -> if null s then "" else (' ':s))

parenth :: Doc -> Doc
parenth ss = doc (showChar '(') . ss . doc (showChar ')')

concatS :: [ShowS] -> ShowS
concatS = foldr (.) id

concatD :: [Doc] -> Doc
concatD = foldr (.) id

replicateS :: Int -> ShowS -> ShowS
replicateS n f = concatS (replicate n f)

-- the printer class does the job
class Print a where
  prt :: Int -> a -> Doc
  prtList :: [a] -> Doc
  prtList = concatD . map (prt 0)

instance Print a => Print [a] where
  prt _ = prtList

instance Print Char where
  prt _ s = doc (showChar '\'' . mkEsc '\'' s . showChar '\'')
  prtList s = doc (showChar '"' . concatS (map (mkEsc '"') s) . showChar '"')

mkEsc :: Char -> Char -> ShowS
mkEsc q s = case s of
  _ | s == q -> showChar '\\' . showChar s
  '\\'-> showString "\\\\"
  '\n' -> showString "\\n"
  '\t' -> showString "\\t"
  _ -> showChar s

prPrec :: Int -> Int -> Doc -> Doc
prPrec i j = if j<i then parenth else id


instance Print Integer where
  prt _ x = doc (shows x)


instance Print Double where
  prt _ x = doc (shows x)



instance Print Hole where
  prt _ (Hole (_,i)) = doc (showString ( i))


instance Print Id where
  prt _ (Id (_,i)) = doc (showString ( i))
  prtList es = case es of
   [] -> (concatD [])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])



instance Print Prog where
  prt i e = case e of
   Prog topdecls -> prPrec i 0 (concatD [prt 0 topdecls])


instance Print TopDecl where
  prt i e = case e of
   TopDecl decl -> prPrec i 0 (concatD [prt 0 decl])

  prtList es = case es of
   [] -> (concatD [])
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ";") , prt 0 xs])

instance Print Decl where
  prt i e = case e of
   AliasDecl idlist type' -> prPrec i 0 (concatD [doc (showString "type") , prt 0 idlist , doc (showString "=") , prt 0 type'])
   DerivDecl ctxs expr -> prPrec i 0 (concatD [prt 0 ctxs , doc (showString "|-") , prt 0 expr])

  prtList es = case es of
   [] -> (concatD [])
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ";") , prt 0 xs])

instance Print Ctxs where
  prt i e = case e of
   BothCtxs binders0 binders -> prPrec i 0 (concatD [prt 0 binders0 , doc (showString ";") , prt 0 binders])
   OnlyLinear binders -> prPrec i 0 (concatD [prt 0 binders])


instance Print IdList where
  prt i e = case e of
   OneId id -> prPrec i 0 (concatD [prt 0 id])
   ConsId id idlist -> prPrec i 0 (concatD [prt 0 id , prt 0 idlist])


instance Print Binder where
  prt i e = case e of
   TypedBinder id type' -> prPrec i 0 (concatD [prt 0 id , doc (showString ":") , prt 0 type'])
   Binder id -> prPrec i 0 (concatD [prt 0 id])

  prtList es = case es of
   [] -> (concatD [])
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])

instance Print Lit where
  prt i e = case e of
   LitOne  -> prPrec i 0 (concatD [doc (showString "1")])
   LitZero  -> prPrec i 0 (concatD [doc (showString "0")])
   Lit n -> prPrec i 0 (concatD [prt 0 n])


instance Print Size where
  prt i e = case e of
   SizeLit lit -> prPrec i 3 (concatD [prt 0 lit])
   SizeVar id -> prPrec i 3 (concatD [prt 0 id])
   SizeSum size0 size -> prPrec i 1 (concatD [prt 2 size0 , doc (showString "+") , prt 1 size])
   SizeProd size0 size -> prPrec i 2 (concatD [prt 3 size0 , doc (showString "*") , prt 2 size])
   SizeHole hole -> prPrec i 3 (concatD [prt 0 hole])


instance Print QBList where
  prt i e = case e of
   OneTyped id type' -> prPrec i 0 (concatD [doc (showString "(") , prt 0 id , doc (showString ":") , prt 0 type' , doc (showString ")")])
   OneUntyped id -> prPrec i 0 (concatD [prt 0 id])
   ConsTyped id type' qblist -> prPrec i 0 (concatD [doc (showString "(") , prt 0 id , doc (showString ":") , prt 0 type' , doc (showString ")") , prt 0 qblist])
   ConsUntyped id qblist -> prPrec i 0 (concatD [prt 0 id , prt 0 qblist])


instance Print Type where
  prt i e = case e of
   Tensor type'0 type' -> prPrec i 4 (concatD [prt 5 type'0 , doc (showString "*") , prt 4 type'])
   Par type'0 type' -> prPrec i 5 (concatD [prt 5 type'0 , doc (showString "|") , prt 6 type'])
   One  -> prPrec i 8 (concatD [doc (showString "1")])
   Bot  -> prPrec i 8 (concatD [doc (showString "_|_")])
   Plus type'0 type' -> prPrec i 2 (concatD [prt 3 type'0 , doc (showString "+") , prt 2 type'])
   With type'0 type' -> prPrec i 3 (concatD [prt 3 type'0 , doc (showString "&") , prt 4 type'])
   Top  -> prPrec i 8 (concatD [doc (showString "T")])
   Zero  -> prPrec i 8 (concatD [doc (showString "0")])
   Type  -> prPrec i 8 (concatD [doc (showString "Type")])
   Int  -> prPrec i 8 (concatD [doc (showString "int")])
   Lollipop type'0 type' -> prPrec i 1 (concatD [prt 2 type'0 , doc (showString "-o") , prt 1 type'])
   TyVar id -> prPrec i 8 (concatD [prt 0 id])
   TyApp type'0 type' -> prPrec i 7 (concatD [prt 7 type'0 , prt 8 type'])
   TySize size -> prPrec i 1 (concatD [doc (showString "size") , prt 3 size])
   TyHole hole -> prPrec i 8 (concatD [prt 0 hole])
   Bang type' -> prPrec i 6 (concatD [doc (showString "!") , prt 6 type'])
   Quest type' -> prPrec i 6 (concatD [doc (showString "?") , prt 6 type'])
   Neg type' -> prPrec i 6 (concatD [doc (showString "~") , prt 6 type'])
   BigTensor size type' -> prPrec i 6 (concatD [doc (showString "Tensor") , prt 3 size , prt 6 type'])
   BigPar size type' -> prPrec i 6 (concatD [doc (showString "Par") , prt 3 size , prt 6 type'])
   Index  -> prPrec i 8 (concatD [doc (showString "Index")])
   Forall qblist type' -> prPrec i 1 (concatD [doc (showString "forall") , prt 0 qblist , doc (showString ".") , prt 0 type'])
   Exists qblist type' -> prPrec i 1 (concatD [doc (showString "exists") , prt 0 qblist , doc (showString ".") , prt 0 type'])
   Mu id type' -> prPrec i 1 (concatD [doc (showString "mu") , prt 0 id , doc (showString ".") , prt 0 type'])
   Nu id type' -> prPrec i 1 (concatD [doc (showString "nu") , prt 0 id , doc (showString ".") , prt 0 type'])


instance Print InlInr where
  prt i e = case e of
   Inl  -> prPrec i 0 (concatD [doc (showString "inl")])
   Inr  -> prPrec i 0 (concatD [doc (showString "inr")])


instance Print FstSnd where
  prt i e = case e of
   Fst  -> prPrec i 0 (concatD [doc (showString "fst")])
   Snd  -> prPrec i 0 (concatD [doc (showString "snd")])


instance Print LamPat where
  prt i e = case e of
   ValPat pat -> prPrec i 0 (concatD [prt 6 pat])
   TyPat id -> prPrec i 0 (concatD [doc (showString "@") , prt 0 id])
   TyTyPat id type' -> prPrec i 0 (concatD [doc (showString "@") , doc (showString "(") , prt 0 id , doc (showString ":") , prt 0 type' , doc (showString ")")])


instance Print Pat where
  prt i e = case e of
   PVar id -> prPrec i 6 (concatD [prt 0 id])
   PUnit  -> prPrec i 6 (concatD [doc (showString "()")])
   PUnpack id pat -> prPrec i 6 (concatD [doc (showString "<<") , prt 0 id , doc (showString ",") , prt 2 pat , doc (showString ">>")])
   PLeft pat -> prPrec i 1 (concatD [prt 2 pat , doc (showString ",,") , doc (showString "-")])
   PRight pat -> prPrec i 1 (concatD [doc (showString "-") , doc (showString ",,") , prt 2 pat])
   PBuild pat -> prPrec i 4 (concatD [doc (showString "build") , prt 5 pat])
   PUnfold pat -> prPrec i 4 (concatD [doc (showString "unfold") , prt 5 pat])
   PMany id -> prPrec i 4 (concatD [doc (showString "many") , prt 0 id])
   PSig pat type' -> prPrec i 2 (concatD [prt 3 pat , doc (showString ":") , prt 0 type'])
   PPair pat0 pat -> prPrec i 1 (concatD [prt 2 pat0 , doc (showString ",") , prt 1 pat])

  prtList es = case es of
   [] -> (concatD [])
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString "/") , prt 0 xs])

instance Print MultiPat where
  prt i e = case e of
   OnePat lampat -> prPrec i 0 (concatD [prt 0 lampat])
   ConsPat lampat multipat -> prPrec i 0 (concatD [prt 0 lampat , prt 0 multipat])
   MultiPat manypat type' -> prPrec i 0 (concatD [doc (showString "(") , prt 0 manypat , doc (showString ":") , prt 0 type' , doc (showString ")")])


instance Print ManyPat where
  prt i e = case e of
   TwoManyPat pat0 pat -> prPrec i 0 (concatD [prt 6 pat0 , prt 6 pat])
   ConsManyPat pat manypat -> prPrec i 0 (concatD [prt 6 pat , prt 0 manypat])


instance Print LetDecl where
  prt i e = case e of
   LetDecl pat expr -> prPrec i 0 (concatD [prt 0 pat , doc (showString "=") , prt 0 expr])

  prtList es = case es of
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ";") , prt 0 xs])

instance Print LitBranch where
  prt i e = case e of
   LitBranch lit expr -> prPrec i 0 (concatD [prt 0 lit , doc (showString "->") , prt 0 expr])

  prtList es = case es of
   [x] -> (concatD [prt 0 x , doc (showString ";")])
   x:xs -> (concatD [prt 0 x , doc (showString ";") , prt 0 xs])

instance Print BinOp where
  prt i e = case e of
   Eq  -> prPrec i 0 (concatD [doc (showString "==")])
   Ne  -> prPrec i 0 (concatD [doc (showString "/=")])
   Gt  -> prPrec i 0 (concatD [doc (showString ">")])
   Ge  -> prPrec i 0 (concatD [doc (showString ">=")])
   Le  -> prPrec i 0 (concatD [doc (showString "<=")])
   Lt  -> prPrec i 0 (concatD [doc (showString "<")])


instance Print Expr where
  prt i e = case e of
   Ax expr0 expr -> prPrec i 6 (concatD [prt 7 expr0 , doc (showString "<->") , prt 6 expr])
   Cut pats0 expr1 pats expr -> prPrec i 16 (concatD [doc (showString "cut") , doc (showString "{") , prt 0 pats0 , doc (showString "->") , prt 0 expr1 , doc (showString ";") , prt 0 pats , doc (showString "->") , prt 0 expr , doc (showString "}")])
   Connect expr0 pat1 expr2 pat expr -> prPrec i 16 (concatD [doc (showString "connect") , prt 0 expr0 , doc (showString "to") , doc (showString "{") , prt 0 pat1 , doc (showString "->") , prt 0 expr2 , doc (showString ";") , prt 0 pat , doc (showString "->") , prt 0 expr , doc (showString "}")])
   Pack type' expr -> prPrec i 16 (concatD [doc (showString "<<") , prt 0 type' , doc (showString ",") , prt 16 expr , doc (showString ">>")])
   Case expr0 pat1 expr2 pat expr -> prPrec i 16 (concatD [doc (showString "case") , prt 0 expr0 , doc (showString "of") , doc (showString "{") , doc (showString "inl") , prt 0 pat1 , doc (showString "->") , prt 0 expr2 , doc (showString ";") , doc (showString "inr") , prt 0 pat , doc (showString "->") , prt 0 expr , doc (showString "}")])
   CaseLit expr0 litbranchs id expr -> prPrec i 16 (concatD [doc (showString "case") , prt 0 expr0 , doc (showString "of") , doc (showString "{") , prt 0 litbranchs , prt 0 id , doc (showString "->") , prt 0 expr , doc (showString "}")])
   Return pat expr -> prPrec i 16 (concatD [doc (showString "return") , doc (showString "{") , prt 0 pat , doc (showString "->") , prt 0 expr , doc (showString "}")])
   Halt  -> prPrec i 16 (concatD [doc (showString "halt")])
   Unit  -> prPrec i 16 (concatD [doc (showString "()")])
   Var id -> prPrec i 16 (concatD [prt 0 id])
   ExprHole hole -> prPrec i 16 (concatD [prt 0 hole])
   Pair expr0 expr -> prPrec i 6 (concatD [prt 7 expr0 , doc (showString ",") , prt 6 expr])
   Choices expr0 expr -> prPrec i 6 (concatD [prt 7 expr0 , doc (showString ",,") , prt 6 expr])
   ExprSig expr type' -> prPrec i 6 (concatD [prt 7 expr , doc (showString ":") , prt 5 type'])
   ELit lit -> prPrec i 16 (concatD [prt 0 lit])
   EPlus expr0 expr -> prPrec i 7 (concatD [prt 8 expr0 , doc (showString "+") , prt 7 expr])
   EMinus expr0 expr -> prPrec i 7 (concatD [prt 8 expr0 , doc (showString "-") , prt 7 expr])
   EMul expr0 expr -> prPrec i 8 (concatD [prt 9 expr0 , doc (showString "*") , prt 8 expr])
   EDiv expr0 expr -> prPrec i 8 (concatD [prt 9 expr0 , doc (showString "/") , prt 8 expr])
   EMod expr0 expr -> prPrec i 8 (concatD [prt 9 expr0 , doc (showString "%") , prt 8 expr])
   ENegate expr -> prPrec i 9 (concatD [doc (showString "-") , prt 9 expr])
   EBinOp expr0 binop expr -> prPrec i 10 (concatD [prt 11 expr0 , prt 0 binop , prt 11 expr])
   App expr0 expr -> prPrec i 14 (concatD [prt 14 expr0 , prt 15 expr])
   MkPlus inlinr expr -> prPrec i 14 (concatD [prt 0 inlinr , prt 15 expr])
   Choose fstsnd expr -> prPrec i 14 (concatD [prt 0 fstsnd , prt 15 expr])
   Build expr -> prPrec i 14 (concatD [doc (showString "build") , prt 15 expr])
   Unfold expr -> prPrec i 14 (concatD [doc (showString "unfold") , prt 15 expr])
   Load id -> prPrec i 15 (concatD [doc (showString "!") , prt 0 id])
   Promote expr -> prPrec i 14 (concatD [doc (showString "many") , prt 15 expr])
   Crash expr -> prPrec i 14 (concatD [doc (showString "crash") , prt 15 expr])
   Fix expr -> prPrec i 14 (concatD [doc (showString "fix") , prt 15 expr])
   Ignore id -> prPrec i 14 (concatD [doc (showString "ignore") , prt 0 id])
   Copy expr -> prPrec i 14 (concatD [doc (showString "copy") , prt 15 expr])
   Lam multipat expr -> prPrec i 6 (concatD [doc (showString "\\") , prt 0 multipat , doc (showString "->") , prt 6 expr])
   TApp expr type' -> prPrec i 14 (concatD [prt 14 expr , doc (showString "@") , prt 8 type'])
   Let letdecls expr -> prPrec i 6 (concatD [doc (showString "let") , prt 0 letdecls , doc (showString "in") , prt 6 expr])



