{-# LANGUAGE ViewPatterns,MultiParamTypeClasses,GeneralizedNewtypeDeriving, TupleSections,
             OverloadedStrings,ScopedTypeVariables,PatternGuards,CPP #-}
#define REPORT report (__FILE__ ++ ":" ++ show (__LINE__ :: Integer))
-- | Unification and substitution on sizes
module Unify.Size where

import MetaVar

import Control.Applicative hiding (empty)
import Control.Monad
import Control.Monad.State

import DsErrors
import DsMonad

import Type

import Data.List
import Data.Maybe
import Data.Either
import Data.Tuple
import Data.Bifunctor
import Data.Traversable

import qualified Data.Map as M
import Id
import Pol

import Text.PrettyPrint
import Text.Show.Pretty (ppDoc)
import PrettyId 


-- | Unify two size polynomials.
--
--   Currently supports unifying two polynomials in which at most one of them
--   has one metavariable, on which the polynomial depends linearly.
unifySize :: MetaInfo
          -> [(Id,Id)]
          -> IdSize -- ^ origin's size
          -> IdSize -- ^ match size
          -> DsM [(MetaVar Id, IdSize)]
unifySize blame sub s1 s2 = val
  where
    val = do 
      let p1 = toPol s1
      let p2 = toPol s2

      let meta1 = nub $ catMaybes $ map getMeta $ polRefs p1 
      let meta2 = nub $ catMaybes $ map getMeta $ polRefs p2 

      let res = case (meta1, meta2) of
            ([], []) -> if szRenameList sub s1 == s2  
                        then return [] 
                        else Left msgSizeUnificationError 
      -- Linear case
            ([m],[]) -> solveOne sub m s1 s2
            ([],[m]) -> solveOne (map swap sub) m s2 s1
            (_  ,_ ) -> Left msgSizeMultipleVariablesError

      case res of
        Right x -> return x
        Left e  -> throw $ e blame (fromPol p1) (fromPol p2)

    solveOne sub m s1 s2 = do 
      let p1 = toPol $ szRenameList sub s1 
      let p2 = toPol s2 
      let res = solvePolLinIntegral (Meta m) (p2 - p1)
  
      bimap (msgSizeSolvingError m) ((:[]) . (m,) . fromPol) res

applySubstSize :: UnifySubst -> IdSize -> DsM IdSize
applySubstSize su p = p `sizeBindM` substSymbolSize 
  where
  substSymbolSize :: IdSymbol -> DsM IdSize
  substSymbolSize s0 =
    case s0 of
      s0@(TVar{}) -> return (sizeSymb s0)
      Meta m@(MetaVar pos u scope _info) -> case M.lookup u su of
          Just (delta, ty) -> do
              sz <- maybe (throw $ "Type metavariable in size context: " <+> pretty m <+> "=" <+> pretty ty)  
                          return (sizeFromType ty)
              let tv_subst = zip delta scope
              let sz' = szRenameList tv_subst sz
              -- This recursion should terminate, as eventually all metavariables
              -- will be substituted.
              sz'' <- applySubstSize su sz'
              REPORT $ hang ("applySubst" <+> text (show u) <+> "->" <+> pretty sz) 4
                  $  "scope:" <+> ppDoc scope
                  $$ "delta:" <+> ppDoc delta
                  $$ "sz':" <+> pretty sz'
                  $$ "sz'':" <+> pretty sz''
              return sz''
          Nothing  -> return (sizeSymb s0)  
    

