{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# OPTIONS_GHC -w #-}

module TexPretty where

import Id
import LL hiding (index, (&))
import MarXup
import MarXup.Tex
import MarXup.Latex
import MarXup.Latex.Math
import MarXup.DerivationTrees
import MarXup.PrettyPrint.Core as PP (pretty,Doc(Union),flatten,group)
import MarXup.PrettyPrint as PP
import MarXup.Diagram.Tikz (outline, noOutline)
import Data.List
import Data.Monoid
import Framework hiding (todo)
import Data.Foldable as T
import Data.Traversable as T
import Control.Applicative
import Data.Char (toUpper)
import GHC.Exts hiding (seq)
import Text.Show.Pretty (ppShow)
import Data.Functor.Identity
import Type.Ctx
import Control.Lens hiding (Choice, index, element)
--import TypeCheck
import Lint
import Development.Placeholders (todo)
import Fresh
import Tagged
import Control.Monad.Except.Extra
import TypeCheck (analyseDeriv)
import LL.Traversal
import Data.Maybe
import Type.Data
import Polarize (polarizeDerivUnsafe)
import Data.Default
--import Lint (lintDeriv)

functionName = cmd "mathit"

instance Element (Type' a b) => Element (Type'' (Type' a b) (Size b) a b) where
  type Target (Type'' (Type' a b) (Size b) a b) = Target (Type' a b)
  element = element . T

instance Element IdType where
  type Target IdType = TeX
  element = math . texType 0

instance Element IdSized where
  type Target IdSized = TeX
  element = math . texSized 0

instance Element (Type' String String) where
  type Target (Type' String String) = TeX
  element = math . texType 0 . runFreshM . fromNamesLiberal

instance Element (Seq' Id Id) where
  type (Target (Seq' Id Id)) = TeX
  element = math . linearize . texProgSimpl "element?" whatDefault False

instance Element (IdSymbol) where
  type Target IdSymbol = TeX
  element = math . texSymbol 0 mempty
  
instance Element (IdSize) where
  type Target IdSize = TeX
  element = math . texSize 0 

instance Element (Sized' String String) where
  type Target (Sized' String String) = TeX
  element = math . texSized 0 . runFreshM . fromNamesLiberal

instance Element (Size String) where
  type Target (Size String) = TeX
  element = math . texSize 0 . sizeFromNamesLiberal


math = ensureMath
index = subscript
index' = superscript

plupp :: TeX -> TeX
plupp t = -- oxford t --  <> "∙"
          braces t <> tex "^" <> braces "∙"

-- infix 1 `for`
-- x `for` lens = over lens x

bot :: TeX
bot = math $ cmd0 "simbot"

par, amp :: TeX
par = math $ cmd0 "parr"
amp = math $ cmd "hspace" "1pt" <> cmd0 "&"  <> cmd "hspace" "1pt"

ruleName ::  Tex a -> Tex a
ruleName = smallcaps

indicator ::  Choice -> TeX
indicator b = case b of Inl -> "1"
                        Inr -> "2"

isBoson' :: Bool -> TeX -> TeX
isBoson' False x = x
isBoson' True  x = ruleName "B" <> x

-- seqName :: Seq' a b  -> Tex ()
seqName = seqName' 

capitalize [] = []
capitalize (c:cs) = toUpper c:cs

indexSz sz = if sz == sizeOne then mempty else math (index (texSize 0 sz))
indexSzCnt sz cnt = let sub = if sz  == sizeOne then mempty else (index (texSize 0 sz)) in
                    let sup = if cnt == sizeOne then mempty else (index' (texSize 0 cnt)) in
                    math $ sub <> sup

-- seqName' ::  t -> Seq' t1 t2 -> Tex ()
seqName' :: Seq'' seq ty (IdSize) tysz name ref -> TeX
seqName' s = case s of
   -- Exchange _ _       -> ruleName "Ex."
   Ax{}           -> ruleName "Ax"
   NCut{..} -> ruleName (textual $ capitalize (cutnames ncut_info !! length ncut_cs) ) <> indexSz ncut_size
   Halt{}               -> ruleName "Halt"
   Cross{β} -> isBoson' β "⊗"
   Par  {..} -> isBoson' β "⅋"
   Plus {}            -> "⊕"
   With{β,choose = b} -> isBoson' β $ math $ (amp <> index (indicator b))
   SOne{β}        -> isBoson' β $ "1"
   SZero{}          -> "0"
   SBot{}             -> bot
   TApp{β}  -> isBoson' β "∀"
   TUnpack{}    -> "∃"
   Offer{β}     -> isBoson' β $ ruleName "Offer"
   Demand{}     -> "!"
   Ignore{β}       -> isBoson' β $ ruleName "Wk"
   Alias{β}      -> isBoson' β $ ruleName "Con"
   Mem{..} -> ruleName "BM" <> math ( index (textual (show count))
                                                    -- <> texType 0 ctx ty
                                                  )
   Save{}             -> ruleName "Save"
   Load{..}             -> ruleName "Load" <> indexSz loadSize
   Split{..}        -> ruleName "Split" <> indexSz splitSz
   Merge{..}        -> ruleName "Merge" <> indexSz splitSz
   BigTensor{}        -> math $ cmd0 "bigotimes"
   BigPar{}        -> math $ cmd0 "bigparr"
   Permute{}        -> ruleName "shuffle"
   Foldmap{}        -> ruleName "foldmap"
   Foldmap2{}        -> ruleName "foldmap"
   Sync{}           -> ruleName (textual$ syncnames s) <> syncindex s
   BigSeq{}        -> math $ ruleName "§"
   SizeCmp{}      -> ruleName "compare"
   _                  -> error "TexPretty: seqName'"

derivRootName :: Deriv' Id Id -> TeX
derivRootName (D0 (Deriv _ _ (Seqn0 (Seqn _ seq)))) = seqName seq

syncnames :: Seq'' seq ty (IdSize) tysz name ref -> String
syncnames s = case s of
   Sync{syncIntr} | (():_) <- [() | (_,_,_,Just _, _) <- syncIntr] ->  "loop"
   Sync{syncIntr} | [] <- [() | (_,_,_,_, Just _) <- syncIntr] ->  "discard"
{-   Sync{syncIntr} | [()] <- [() | (1,_,_,_, Just (SyncRead { syncCopies })) <- syncIntr
                                  , syncCopies /= 1] ->  "copy"-}
   Sync{} ->  "sync" 


syncindex :: Seq'' seq ty (IdSize) tysz name ref -> TeX
syncindex Sync{syncIntr = [(szN,_,_,Nothing, Just SyncRead{syncCopies = szK})]} = indexSzCnt szN szK
syncindex _ = mempty



opName :: IsString s => CmpOp -> s
opName Le = "≤"
opName Eq = "="
opName Ge = "≥"
opName Gt = ">"
opName Lt = "<"
opName Ne = "≠"

unknownTypeEnv ::  [String]
unknownTypeEnv = repeat "VAR"

seqLab :: Seq' n r -> String
seqLab (S0 s) = case s of
   -- Exchange{} -> "X"
   Ax{}       -> "↔"
   NCut{ncut_cs = []} -> "Mix"
   NCut{ncut_cs = [_]} -> "Cut"
   NCut{ncut_cs = [_,_]} -> "BiCut"
   NCut{ncut_cs = l} -> (show (length l) ++ "-Cut")
   Cross{}    -> "⊗"
   Par{}      -> "⅋"
   Plus {}    -> "⊕"
   With{}     -> "\\&"
   SOne{}     -> "1"
   SZero{}    -> "0"
   SBot{}     -> "⊥" -- note: not squiggly
   TApp{}     -> "∀"
   TUnpack{}  -> "∃"
   Offer{}    -> "?"
   Demand{}   -> "!"
   Ignore{}   -> "Wk"
   Alias{}    -> "Con"
   Mem{}      -> "M"
   Save{}     -> "Save"
   Load{}     -> "Load"
   _          -> error "seqLab"


data TexSeqOpt = TexSeqOpt {
  showProg :: Bool
 ,showVars :: Bool
 }

instance Default TexSeqOpt where
  def = TexSeqOpt {
    showProg = False
   ,showVars = False
   }

texSeq :: Name -> Bool -> Seqn2 Id Id -> Derivation
texSeq name showProgAndVars seq = texSeqOpt name def{showProg = showProgAndVars
                                                    ,showVars = showProgAndVars
                                                    } seq

texSeqOpt :: Name -> TexSeqOpt -> Seqn2 Id Id -> Derivation
texSeqOpt who TexSeqOpt{..} = runIdentity . foldSeqnWithSelf defSeqnFold{sseq,sty,ssz,stysz}
                                  . seqn2
  where
  sty _   = pure . texType 0 
  ssz _   = pure . texSize 0
  stysz _ = pure . texSized 0

  sseq s2@(Seqn ctx seq2) sD = pure $
    case sD of
      Permute{..} ->
        let oldSz = texSize 0 (dropType . dropValue $ zElim) in
        rul $ seqs ++ [Node (Rule noOutline mempty mempty (math $ oldSz <> " = " <> newSz)) []]

    {-  SizeCmp{..} -> rul $ seqs ++ 
                          [Node (Rule noOutline mempty mempty (
                                   math $ sz1 <> " " <> mconcat [opName op | (op,_) <- bCmp] <> " " <> sz2)) []]-}
      
      What{..} -> Node (Rule noOutline mempty mempty
                          (math (printedCtx <> "⊢" <> sureProg)))
                         -- always show the program so we know how to refer to this proof continuation.
                  []
      _ -> rul seqs
    where
      s0 = erase $ erase $ Seqn2 s2

      rul :: [Derivation] -> Derivation
      rul subs = texRule (seqName' seq2) (math (printedCtx <> "⊢" <> maybeProg)) subs

      maybeProg = if showProg then sureProg else mempty
      sureProg = linearize (texProg who ctx s0)

      usefulCtx = ctx & intuit %~ filter (\(v ::: T t :^ _sz) -> case t of
                                         TType -> False
                                         TSize -> False
                                         _     ->
                                           v `T.elem` s0)

      printedCtx = texDoubleSeqCtx showVars usefulCtx
      
      seqs = seqs0Seq sD 

texRuleOpenLeaf :: TeX -> Derivation
texRuleOpenLeaf base = texRule mempty base []

texRuleLeaf :: TeX -> Derivation
texRuleLeaf base = Node (Rule noOutline mempty mempty base) []

texRule :: TeX -> TeX -> [Derivation] -> Derivation
texRule name base subs = Node (Rule (outline "black") mempty (textSize FootnoteSize name)
                       (base)) (map (defaultLink ::>) subs)
 

keyword :: String -> TeX
keyword = math . mathsf . tex
let_ ::  TeX
let_ = keyword "let "
in_ ::  TeX
in_ = keyword "in "
_of ::  TeX
_of = keyword " of"
_of_ ::  TeX
_of_ = keyword " of "
diamond :: TeX
diamond = cmd0 "diamond"
case_ ::  TeX
case_ = keyword "case "
connect_ ::  TeX
connect_ = keyword "connect "
ignore_ ::  TeX
ignore_ = keyword "ignore "
dump_ ::  TeX
dump_ = keyword "dump "
alias_ ::  TeX
alias_ = keyword "alias "
inl_ ::  TeX
inl_ = keyword "inl "
inr_ ::  TeX
inr_ = keyword "inr "
fst_ ::  TeX
snd_ ::  TeX
[fst_,snd_] = map keyword ["fst ","snd "]
choice_ ::  Choice -> TeX
choice_ c = case c of
  Inl -> fst_
  Inr -> snd_
choice'_ ::  Choice -> TeX
choice'_ c = case c of
  Inl -> inl_
  Inr -> inr_
separator :: TeX
separator = cmd "hline" mempty
save_,load_ :: TeX
[save_,load_] = map (keyword . (++ nbsp)) ["save","load"]
-- cut_,mix_,bicut_ :: TeX
 {-@[mix_,cut_,bicut_] -}
cutnames nfo = ["mix",glue,"bicut"]
  where glue = case nfo of
          Fuse -> "fuse"
          NoCommute -> "COMMUTE BLOCKED"
          _ -> "cut"

cutkeyw nfo = ["mix",glue,"bicut"]
  where glue = case nfo of
          Fuse -> "fuse"
          _ -> "cut"

nbsp :: String
nbsp = " "

block' ::  [TeX] -> TeX
block' = xblock "l" . map (:[])
xblock ::  TeX -> [[TeX]] -> TeX
xblock format bod@(firstRow:_) = do
  env' "array" ["t"] $ do
    braces format
    mkrows (map mkcols bod)
  return ()

-- left_ = cmd "Leftarrow" mempty
-- right_ = cmd "Rightarrow" mempty

indent :: TeX
indent = cmd "hspace" $ tex "1em"


data Block = Splitt TeX [([TeX],TeX,Block)] | Final TeX | Instr TeX Block

t <:> Final u = Final (t <> u)
t <:> Instr u b = Instr (t <> u) b
t <:> Splitt u b = Splitt (t <> u) b


texProg :: Name -> SeqCtx (Id ::: IdSized) -> IdSeq -> Block
texProg who = texProg' who True

{-
texUntypedProg :: Name -> [Id] -> Seq -> TeX
texUntypedProg who vs s = math $ linearize $ texProg' who False
                                    (zip vs (repeat $ error "accessing type when texing untyped term")) s
                                    -}

linearize :: Block -> TeX
linearize (Final t) = t
linearize (Instr h t) = h <> ";" -- keyword " in "
                          <> linearize t
linearize (Splitt h xs) = h <> brac (TexPretty.punctuate "; "
                                      [ TexPretty.punctuate ", " x <> (if null x then mempty else cmd0 "mapsto") <> y <> linearize ts
                                      | (x,y,ts) <- xs
                                      ])

punctuate ::  Monoid c => c -> [c] -> c
punctuate p = mconcat . intersperse p
commas ::  [TeX] -> TeX
commas = TexPretty.punctuate ", "

autoindent :: Block -> Tex Doc
autoindent x = do
  xs <- autoindent' x
  return $ PP.align (fillSep xs)

autoLayout xs = do                                                              
  l <- text "{"                                                                 
  r <- text "}"                                                                 
  semi <- text $ math "; "                                                      
  return $ Union (flatten $ l <> mconcat (PP.punctuate semi xs) <> r) (l <> PP.align (vcat xs) <> r)
                                                                                
x <#> y = PP.align (nest 10 (x <//> y))
  
autoindent' :: Block -> Tex [Doc]
autoindent' (Final t) = (:[]) <$> (text $ math t)
autoindent' (Instr h t) = do
  h' <- text $ math h
  t' <- autoindent' t
  semi <- text $ math ";"
  return (h' <> semi : t')
autoindent' (Splitt h xs) = do
  h' <- text $ math h
  xs' <- T.forM xs $ \(vars,modif,block) -> do
    vs' <- case vars of
                [] -> return mempty
                _ -> text $ math $ TexPretty.punctuate "," vars <> cmd0 "mapsto" <> modif
    -- vs' <- forM vars $ \v -> do
    --   v' <- text $ math v
    --   return v'
    b' <- autoindent block
    return $ vs' <#> b'
  xs'' <- autoLayout xs'
  return $ [h' <#> xs'']
  
indentation :: Block -> Tex ()
indentation t = math $ block' $ indentation' t

indentation' :: Block -> [TeX]
indentation' (Final t) = [t]
indentation' (Splitt h xs) =
    [ h
    , TexPretty.indent <> xblock (tex "l@{}l")
        [ [ TexPretty.punctuate "; " bs
          , (if null bs then mconcat else mapsto modif) (indentation' a)
          ]
        | (bs,modif,a) <- xs
        ]
    ]
indentation' (Instr h t) = h : indentation' t

mapsto :: TeX -> [TeX] -> TeX
mapsto modif xs = cmd "mapsto" (modif <> xblock "l" (map (:[]) xs))


connect nm x a y b = Splitt nm [(x,mempty,a),(y,mempty,b)]

texProg' :: Name -> Bool -> SeqCtx (Id ::: IdSized) -> IdSeq -> Block
texProg' who showTypes _ctx s = texProgSimpl who whatDefault showTypes s

whatDefault = what 
  where what :: What -> [IdSeqCtxElem] -> TeX
        what (FFI (PrimLit str)) [w] = (texSeqCtxElem w) <> "↔" <> functionName (texVarStr str)
        what (FFI (Call str)) (w:ws) = (texSeqCtxElem w) <> "↔" <> functionName (texVarStr str) <> brack (commas $ map texSeqCtxElem ws)
        what a [] = functionName (texVarStr$ whatToString a)
        what a ws = functionName (texVarStr$ whatToString a) <> brack (commas $ map texSeqCtxElem ws)

        whatToString :: What -> String
        whatToString (Huh msg) = msg
        whatToString (FFI (Call str)) = str
        whatToString (FFI (PrimLit str)) = str
        whatToString (LLDeriv str) = str
        whatToString (Builtin BIFSchedSeq) = "sched"
        whatToString (Builtin BIFSchedPar) = "sched par"
        whatToString (Builtin BIFCoalesce) = "coalesce"
        whatToString (Builtin BIFIndex) = "index"

texSeqCtxElem (ElemLinear v) = texVar v
texSeqCtxElem (ElemIntuit ty _) = texType 0 ty

texOp op = case op of
   Eq -> "="
   Ne -> "≠"
   Gt -> ">"
   Ge -> "≥"
   Le -> "≤"
   Lt -> "<"

texProgSimplTyped' who what showTypes s2 = case s2 of
  BigSeq steps   -> Splitt (keyword "traverse") $
                            steps >>= \step ->
                              case step of
                                SeqStep{..} ->
                                  let vars = map (\(ref ::: (ty :^ _),name ::: _) ->  texVarT ref (texType 0 ty) <>
                                                                                      hspace"2pt" <> keyword "as" <> hspace"2pt" <> texVar name) seqChunk  in
                                  [(case vars of
                                       [] -> [mempty]
                                       _  -> vars ,
                                     (tex "\\!" <> subscript (texSize 0 seqSz)), seqProg)]


  s2 →
    let a = runIdentity$ traverseSeq'' defSeqTraverse{tname=pure.erase,tref=pure.erase} s2 in
    texProgSimpl' who what showTypes a 

texProgSimplTyped who what showTypes (Seqn2 (Seqn _ s2)) =
   texProgSimplTyped' who what showTypes $ runIdentity$ traverseSeq'' defSeqTraverse{tseq} s2
   where
   tseq = pure . texProgSimplTyped who what showTypes . Seqn2


texProgSimpl who what showTypes (Seqn0 (Seqn _ s0)) = texProgSimpl' who what showTypes $ runIdentity$ traverseSeq'' defSeqTraverse{tseq} s0
   where
   tseq = pure . texProgSimpl who what showTypes . Seqn0

texProgSimpl' who what showTypes s0 = case s0 of
  (Ax v v' ) -> Final $ texVar v <> "↔" {-<> subscript (texType 0 ty)-} <> texVar v'
  (NCut nfo fan sz cs s t) -> connect ((keyword (cutkeyw nfo !! length cs)))
                               [texVarT'n v (neg vt) (if fan == FanLeft then sizeOne else sz) | (v,_,vt) <- cs] (tps s)
                               [texVarT'n v (vt)     (if fan == FanRight then sizeOne else sz)| (_,v,vt) <- cs] (tps t)
  Halt{} -> Final $ keyword "halt"
  SBot v -> Final $ keyword "yield to " <> texVar v
  Cross _β w v v' t -> Instr (let_ <> texVar v <> "," <> texVar v' <> " = " <> texVar w) (tps t)
  Par _β w v s v' t -> connect (connect_ <> texVar w <> keyword " to")
                        [texVar v ] (tps s)
                        [texVar v'] (tps t)
  Plus w (Branch v s) (Branch v' t) -> Splitt (case_ <> texVar w <> _of)
                      [([keyword "inl " <> texVar v],mempty,tps s),
                       ([keyword "inr " <> texVar v'],mempty,tps t)]
  SizeCmp {..} -> Splitt (keyword "compare " <> texSize 0 sz1 <> "," <> texSize 0 sz2)
                      [([texOp op <> " "],mempty, tps p) |  (op,p) <- bCmp]
  With _β w c v' s -> let'' (choice'_ c <> texVarNoT' v' ty) (texVar w) $ tps s
  SZero w vs -> Final $ dump_ <> whenShowTypes (commas $ map texVar vs) <> keyword " in " <> texVar w
  SOne _β w t -> let'' diamond (texVar w) $ tps t
  What a ws ys -> Final $ what a [ws !! y | y <- ys] 
  Split z sz x y t -> let'' (texVar x <> "," <> texVar y) (keyword "split"<> index (texSize 0 sz) <> hspace"2pt"  <> texVar z) $ tps t
  BigTensor z x t -> let'' (texVar x) (keyword "slice "  <> texVar z) $ tps t
  BigPar z ts -> Splitt (keyword "coslice " <> texVar z)
                             [([texVar x] , tex "\\!" <> subscript (texSize 0 sz) , tps t) | (sz,Branch x t) <- ts]
  Sync{syncIntr = syncIntr',..} ->
    let syncIntr = unsafeExcept $ T.mapM (_2 %%~ mkData) syncIntr' in
    Splitt (keyword (syncnames s0)) $ [([texVarT'n xIntr (dataWrite syncTy) syncSz | (syncSz, syncTy, xIntr, _, _) <- syncIntr],mempty, tps contL)] ++

                              (flip map (maybeToList syncCont) $ \syncCont ->
                                ([texVarT'n syncWIntr (arrayOp Neutral syncSteps (dataUpdate syncTy)) syncSz
                                 | (syncSz, syncTy, _, Just SyncUpdate{..}, _) <- syncIntr], 
                                 mempty,tps syncCont))
                              ++
                              (flip map (maybeToList syncContR) $ \syncContR ->
                                 ([texVarT'n syncYIntr (dataRead syncTy) (syncCopies * syncSz)
                                  | (syncSz, syncTy, _, _, Just SyncRead{..}) <- syncIntr],
                                  mempty,tps syncContR))

  Foldmap2 {..}  -> Splitt  (keyword "foldmap" <> indexSz fmSize <> " " <> commas (map texVar fmArr))
                       [(map texVar [fmMapTrg] {-fmArr-},mempty,tps fmMap)
                       ,(map texVar [fmUnitTrg],mempty,tps fmUnit)
                       ,(map texVar [fmMixL,fmMixR,fmMixTrg],mempty,tps fmMix)
                       ,(map texVar [fmMon],mempty,tps fmCont)]

  BigSeq steps   -> Splitt (keyword "traverse") $
                            steps >>= \step ->
                              case step of
                                SeqStep{..} ->
                                  let vars = map (\(ref,name) ->  texVar ref <> hspace"2pt" <> keyword "as" <> hspace"2pt" <> texVar name) seqChunk  in
                                  [(case vars of
                                       [] -> [mempty]
                                       _  -> vars ,
                                     (tex "\\!" <> subscript (texSize 0 seqSz)), tps seqProg)]

  Merge{..} -> Instr (let_ <> texVar xIntr <> " = " <> keyword "merge " <> texVar zElim <> "," <> texVar wElim) (tps cont)

  TApp{..}    ->  Instr (let_ <> texVar xIntr <> " = " <> texVar zElim <> "@" <> texType 0 ty) (tps cont)
  TUnpack{..} ->  Instr (let_ <> texVar xIntr <> "〈" <> texVar tyIntr <> "〉" <> " = " <> texVar zElim) (tps cont)

  x -> error $ "texProgSimpl: " ++ show (prune x)
 where 
   tps = id
   texVarT'n x y 1 = texVarT x (texType 0 y)
   texVarT'n x y sz = texVarT x (braces (texType 0 y) <> superscript (texSize 0 sz))
   texVarT' x y | showTypes = texVarT x (texType 0 y)
                | otherwise = texVar x
   texVarNoT' x y = texVar x
   whenShowTypes | showTypes = id
                 | otherwise = const "?"
   let'' w    v t = Instr (let_ <> w <> "=" <> v) t

{-
    | Foldmap2 { -- version with constant monoid type
      fmSize :: Type' name ref,
      fmTyM :: Type' name ref,
      fmArr :: [ref], {-^ a list of arrays folded in parallel -}
      fmUnitTrg :: name,
      fmUnit :: Seq' name ref,
      fmMapTrg :: name,
      fmMap :: Seq' name ref, -- ^ mapper
      fmMixL, fmMixR, fmMixTrg :: name,
      fmMix :: Seq' name ref, -- ^ combiner
      fmMon :: name,  -- a better name for this is "fmResult"
      fmCont ::  (Seq' name ref) -- ^ result
      }


    | Split {splitSz :: Size' name ref, xIntr, yIntr ::  name, zElim :: ref, cont :: Seq' name ref}
-}

mathrm :: Tex a -> Tex a
mathrm = cmd "mathrm"

texVarTy :: Id -> IdSized -> TeX
-- texVarTy v (Type:^_) = texVar v <> space <> mathrm "fresh"
texVarTy v t = texVarT v (texSized 0 t)

texVarT ::  Id -> TeX -> TeX
texVarT v t = texVarTStr (id_name v) (texVar v) t

texVarTStr [] v t = t
texVarTStr ('?':_) v t = t
texVarTStr _ v t = v <> ":" <> t

texVar :: Id -> TeX
texVar v = texVarStr (id_name v) -- <> "#" <> (textual $ show $ id_unique v) -- DEBUG

texVarStr :: String -> TeX
texVarStr ('?':'?':'?':tx) = tex tx -- ex tx
texVarStr ['?','γ'] = "Γ"
texVarStr ['?','δ'] = "Δ"
texVarStr "??"      = cmd "mathbf" "?"
texVarStr ('?':rest) = textual rest
texVarStr ('_':nm) = (cmd "bar" $ texVarStr (reverse inside)) <> texVarStr (reverse end)
  where
    -- don't overbar trailing primes
    (end,inside) = span (== '\'') (reverse nm)
texVarStr nm | length pre > 0 && length post > 1 = textual pre <> tex "_{" <>
                                                textual (tail post) <> tex "}"
          | otherwise = textual nm
  where (pre,post) = break (=='_') nm

-- | Prints both the intuitionistic and the linear context
texDoubleCtx :: Bool -> IdCtx -> IdCtx -> TeX
texDoubleCtx show_vars ivs vs = case ivs of
    [] -> texCtx show_vars vs
    _  -> texCtx True ivs <> ";" <> texCtx show_vars vs

texCtx :: Bool -> IdCtx -> TeX
texCtx showVars vs = do
  -- uncomment to show the types context
  -- commas (map texVar $ reverse ts) >>  textual ";"
  texCtx' showVars vs -- (map (second (texType 0)) vs)

texDoubleSeqCtx :: Bool -> SeqCtx (Id ::: IdSized) -> TeX
texDoubleSeqCtx show_vars SeqCtx{ _intuit = [], _linear } = texSeqCtx show_vars _linear
texDoubleSeqCtx show_vars SeqCtx{ _intuit, _linear }      = texSeqCtx show_vars _intuit <> ";" <> texSeqCtx show_vars _linear

texSeqCtx :: Bool -> [Id ::: IdSized] -> TeX
texSeqCtx True vs  = commas [ texVarTy v t  | v ::: t <- vs] -- DEBUG var
texSeqCtx False vs = commas [ texSized 0 t  | _v ::: t <- vs ]

texCtx' ::  Bool -> IdCtx -> TeX
texCtx' True vs = commas [ texVarTy v t | (v,t) <- vs] -- DEBUG var
texCtx' False vs = commas [ texSized 0 t | (_v,t) <- vs ]

texId :: Id -> TeX
texId = tex . show

texInteger :: Integer -> TeX
texInteger = tex . show

texNat :: PolNat -> TeX
texNat = tex . show . getNat

texSized :: Int -> IdSized -> TeX
texSized p (ty :^ 1) = texType p ty
texSized p (ty :^ sz) = braces (texType 5 ty) <> superscript (texSize 0 sz)

texSymbol :: Int -> TeX -> IdSymbol -> TeX
texSymbol _ as' (TVar True x) = texVar x <> as'
texSymbol p as' (TVar False x) = prn p 5 $ texSymbol 6 as' (TVar True x) <> texNeg False
texSymbol p as' (MetaSyntactic _b bs x) = prn p 5 $ textual x <> as' <> texNeg bs

texPolVar :: PolVar -> TeX
texPolVar t = case t of
  PolConst True  -> "+"
  PolConst False -> "-"
  PolVar True p  -> texId p
  PolVar False p -> cmd "overline" $ texId p

texKindedVar :: Id → IdType → TeX
texKindedVar x (T TType) = texVar x
texKindedVar x k = texVar x <> ":" <> texType 0 k

texType :: Int -> IdType -> TeX
texType p (T t0) = go p t0
                   where
    go p (MetaNeg t) = prn p 5 $ texType 6 t <> texNeg False
    go p (RevNeg t) = texType p (neg t)
    go p (Forall v k t) = prn p 0 $ "∀" <> texKindedVar v k <> ". "  <> texType 0 t
    go p (Exists v k t) = prn p 0 $ "∃" <> texKindedVar v k <> ". "  <> texType 0 t
    go p (x@(T (Var _ (TVar False _))) :|: y) = prn p 0 $ texType 1 (neg x) <> "⊸" <> texType 0 y
    go p (x :|: y) = prn p 0 $ texType 1 x <> "⅋" <> texType 0 y
    go p (x :+: y) = prn p 1 $ texType 1 x <> "⊕" <> texType 1 y
    go p (x :&: y) = prn p 1 $ texType 1 x <> amp <> texType 1 y
    go p (x :*: y) = prn p 2 $ texType 2 x <> "⊗" <> texType 2 y
    go p (x :>: y) = prn p 2 $ texType 2 x <> "▹" <> texType 2 y
    go _ TType = "*"
    go _ Zero = "0"
    go _ One = "1"
    go _ Top = "⊤"
    go _ Pole = "▯"
    go _ Bot = bot
    go p (Var as s) = texSymbol p as' s
      where as' = if null as then mempty else  brack (commas $ map (texType 0) as)
    go p (Bang t) = prn p 4 $ "!" <> texType 4 t
    go p (Quest t) = prn p 4 $ "?" <> texType 4 t
    go p (UnpushedNeg t) = texType 5 t <> texNeg False
    go p (Lollipop x y) = prn p 0 $ texType 1 x <> "⊸" <> texType 0 y
    go p (Plupp x)      = plupp (texType 5 x)
    go _ (TInt b)  = keyword "ℤ" <> texNeg b
    go _ (TDouble b)  = keyword "ℝ" <> texNeg b
    go _ (TComplex b)  = keyword "ℂ" <> texNeg b
    go _ (TError b)  = keyword "Error" <> texNeg b
    go p (Array pol sz t) = prn p 4 $ op 
                                         <> index (kern <> texSize 0 sz) <> texType 4 t
      where op = cmd "mathop" $ case pol of
                      APositive -> cmd0 "bigotimes"
                      ANegative -> cmd0 "bigparr"
                      ANeutral p@(PolConst _) -> cmd0 "S" <> (index' $ kern <> texPolVar p)
                      ANeutral' _ -> cmd0 "S"

            kern = case pol of
              ANeutral (PolConst _)  -> tex "\\kern 0pt"
              _ -> mempty

    go p (Index True) = keyword "Ix"
    go p (Index False) = texType p $ T$ MetaNeg $ T$ Index True
    go p (SizeT size) = texSize p size
    go p (TSize) = "ℕ"
    go p (TProp (a,op,b)) = texSize 0 a <> texOperator op <> texSize 0 b
    go p (Primitive b s) = keyword s <> texNeg b
    go p (Prop) = "⊤"
    go _ t = error $ "texType:" ++ show t

    
texOperator op = case op of
   CmpFalse -> "⊥"
   Gt       -> ">"
   Eq       -> "="
   Lt       -> "<"
   Ge       -> "≥"
   Le       -> "≤"
   Ne       -> "≠"
   CmpTrue  -> "⊤"

prn p k = if p > k then paren else id

texSize :: Int -> IdSize -> TeX
texSize p = go p . sizeSyntax 
  where
    go n s@(PSize p) = goPol n p
    go p (PExp x) = prn p 4 $ "2" <> (superscript $ go 2 x)

    goPol p (PSub x y) = prn p 1 $ goPol 1 x <> "-" <> goPol 2 y
    {-
    goPol p sz@(PAdd _ _)
      | Just k <- getLit sz = tex $ show k
      | Just (k,x) <- getFactors sz = goPol p (SizeMul (sizeLit k) x)
    -}
    goPol p (PAdd x y) = prn p 1 $ goPol 1 x <> "+" <> goPol 1 y
    goPol p (PProd x y) = prn p 2 $ goPol 2 x <> goPol 2 y -- <> "×"
    goPol p (PPower x y) = prn p 4 $ goPol 5 x <> texNat y -- <> "×"
    goPol p (PNeg x) = prn p 5 $ "-" <> goPol 6 x 
    goPol p (PParens x) = goPol p x
    goPol p (PScalar a) = texInteger a 
    goPol p (PVariable ref) = texSymbol p mempty ref
{-    
getFactors (SizeAdd x y) =
  do (k,x') <- getFactors y
     if x == x'
       then return (k+1,x)
       else Nothing
getFactors x = pure (1,x)
-}
{-
unicodeTextual :: String -> TeX
unicodeTextual = foldMap unicodeToTex

unicodeToTex :: Char -> TeX
unicodeToTex 'α' = cmd0 "alpha"
unicodeToTex '\945' = cmd0 "oops"
unicodeToTex 'β' = cmd0 "beta"
unicodeToTex 'Γ' = cmd0 "Gamma"
unicodeToTex 'Δ' = cmd0 "Delta"
unicodeToTex 'Ξ' = cmd0 "Xi"
unicodeToTex c = tex [c]
-}

texNeg :: Bool -> TeX
texNeg True = mempty
texNeg False = "⟂" -- I am using just one character so that graphviz
                   -- doesn't get crazy; the proper rendering is done
                   -- at the latex level.

derivOpt :: TexSeqOpt {- ^ Show terms? -} -> IdDeriv -> TeX
derivOpt showProg d0 = do
  let d2@(D2 (Deriv{..})) = analyseDeriv d0
  derivationTree' $ texSeqOpt _derivName showProg _derivSequent

-- | Render a derivation tree.
deriv :: Bool {- ^ Show terms? -} -> IdDeriv -> TeX
deriv showProg d0 = do
  let d2@(D2 (Deriv{..})) = analyseDeriv d0
  derivationTree' $ texSeq _derivName showProg _derivSequent

derivPol :: Bool {- ^ Show terms? -} -> IdDeriv -> TeX
derivPol showProg d0 = do
  let d2@(D2 (Deriv{..})) = erase . polarizeDerivUnsafe . analyseDeriv $ d0
  derivationTree' $ texSeq _derivName showProg _derivSequent

derivationTree' ::  Derivation -> Tex ()
derivationTree' x = do
   centerVertically $
     element $ derivationTreeD x
   return ()

derivation, sequent,programOneLine :: IdDeriv -> TeX
derivation = deriv True
sequent = deriv False

ribbonFraction = 0.85
prettyCust w = pretty ribbonFraction w

texProgD d = texProg

-- | Render a derivation as a program (term)
program w (D0 (Deriv who ctx s)) = do
  d <- autoindent (texProg who ctx s)
  prettyCust w d

programOneLine = math . linearize . programD 

programD (D0 (Deriv who ctx s)) = texProg who ctx s
programS s@(Seqn2 (Seqn ctx _)) = texProg "who?" ctx (erase$ erase$ s)

programOneLineS  s = math $ linearize $ texProgSimpl "who?" whatDefault True s
programOneLineTypedS  s = math $ linearize $ texProgSimplTyped "who?" whatDefault True s
  
programWithCtx w (D0 (Deriv who ctx s)) = do
  d <- autoindent (texProg who ctx s)
  t <- text (let c = texDoubleSeqCtx True ctx <> "⊢" in
             case who of
                  "who?" -> math c 
                  _      -> keyword who <> " ≡ " <> math c)
  prettyCust w (t </> d)

programWithLinearCtx w (D0 (Deriv who (SeqCtx _ ctx') s)) = do
  let ctx = SeqCtx [] ctx'
  d <- autoindent (texProg who ctx s)
  t <- text (let c = texDoubleSeqCtx True ctx <> "⊢" in
             case who of
                  "who?" -> math c 
                  _      -> keyword who <> " ≡ " <> math c)
  prettyCust w (t </> d)

programWithCtxLong (D0 (Deriv who ctx s)) = do
  indentation (texProg who ctx s)

{-
programOneLineWithCtx (Deriv who ctx s) =
--  array[] "l" [[texCtx True ctx <> "⊢"], [programOneLine (Deriv ctx s)]]
  texCtx True ctx <> "⊢" <> programOneLine (Deriv who ctx s)
  -}
