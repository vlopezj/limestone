{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
module Pipeline where

import Parser.ParMx
import Parser.LayoutMx (resolveLayout)
import Parser.ErrM
import Parser.PrintMx (printTree)

import PrettyId
import Desugar (desugar)
import qualified Eval as LL
import qualified LL
import qualified Lint

import Control.Monad
import Control.Monad.Writer
import Control.Monad.Error
import qualified Haskell
import Haskell (Style(..))
import qualified HaskellLoop
import PrettyHaskell (ppProgram)

{-
import Erll.Prelude
import qualified Erll.Compile as E
-}

import qualified LLVM

import Text.PrettyPrint (render)
import Text.Show.Pretty

import System.FilePath

type P a = WriterT [String] (Either String) a

runP :: P a -> (Either String,[String])
runP

throw :: String -> P a
throw = lift . throwError

strToAbs ::  [Char] -> P Parser.AbsMx.Prog
strToAbs s = do
    let tks = resolveLayout True (myLexer s)
    case pProg tks of
        Bad e -> throw $ "Parse failed:" ++ e
        Ok p -> return p

desugarAbs ::  Parser.AbsMx.Prog -> P [LL.IdDeriv]
desugarAbs p = do
    let (r,msg) = desugar p
    case r of
        Left e   -> throw $ "Desugar failed:" ++ show e
        Right ds -> return ds

lintDeriv ::  LL.IdDeriv -> WriterT [Msg] (Either String) LL.IdDeriv
lintDeriv d = case Lint.lintDeriv d of
    Right () -> return d
    Left x   -> throw $ "Lint failed:" ++ show x

