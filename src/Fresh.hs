{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
-- | Monad for generating unique identifiers
module Fresh where

import Control.Applicative hiding (empty)
import Control.Monad.State
import Control.Monad.Writer
import Control.Monad.Reader
import Control.Monad.Except.Extra
import Control.Monad.Trans.Maybe
import Control.Arrow ((&&&))
import Id
import Control.Lens
import Fuel 
import Data.String

type FST = Int

-- | New fresh Id
newtype FreshT m a = FreshT { dirty :: StateT FST m a }
  deriving (Monad, Applicative, Functor, MonadFix)

type FreshM = FreshT Identity

class (Monad m, Applicative m) => MonadFresh m where
  fresh :: m Unique

instance (MonadFresh m) => MonadFresh (StateT r m) where fresh = lift fresh
instance (MonadFresh m) => MonadFresh (ReaderT r m) where fresh = lift fresh
instance (MonadFresh m, Monoid a) => MonadFresh (WriterT a m) where fresh = lift fresh
instance (MonadFresh m) => MonadFresh (MaybeT m) where fresh = lift fresh
instance (MonadFresh m) => MonadFresh (ExceptT e m) where fresh = lift fresh

instance (MonadTrans FreshT) where lift = FreshT . lift

deriving instance (MonadIO m) => MonadIO (FreshT m)
deriving instance (MonadReader r m) => MonadReader r (FreshT m)
deriving instance (MonadError e m) => MonadError e (FreshT m)
instance (MonadState s m) => MonadState s (FreshT m) where
  state f = FreshT $ lift $ state f

instance (Functor m, Monad m) => MonadFresh (FreshT m) where
  fresh :: FreshT m Unique
  fresh = FreshT $ state (Unique &&& succ)

deriving instance (MonadFresh m) => MonadFresh (FuelT m)
deriving instance (MonadFuel m) => MonadFuel (FreshT m)

freshFrom :: (MonadFresh m) => Name -> m (Id)
freshFrom = refreshFrom

freshFromList :: (MonadFresh m) => [Name] -> m [Id]
freshFromList = mapM freshFrom

refreshFrom :: (MonadFresh m) => Name -> m (Id)
refreshFrom x = do
  u <- fresh
  return $ mkId x u

name   f x = (\n -> x { id_name = n }) `fmap` f (id_name x) 
unique f x = (\u -> x { id_unique = u }) `fmap` f (id_unique x) 

freshId :: (MonadFresh m) => m (Id)
freshId = freshFrom "τ"

freshIds :: (MonadFresh m) => Int -> m [Id]
freshIds n = replicateM n freshId

refreshId :: (MonadFresh m) => Id -> m (Id)
refreshId ident = refreshFrom (ident ^. name)

freshElemId :: (MonadFresh m) => Id -> m Id
freshElemId x = do
    u <- fresh
    return $ x & unique .~ u & name %~ (++ "_i")

runFreshM :: FreshM a -> a
runFreshM = runIdentity . runFreshT 

runFreshT :: (Monad m) => FreshT m a -> m a
runFreshT (FreshT m) = evalStateT m 0

runFreshMFromUnique :: Unique -> FreshM a -> a
runFreshMFromUnique (Unique n) (FreshT m) = evalState m n

mapFreshT :: (forall a. m a -> n a) -> FreshT m a -> FreshT n a
mapFreshT f (FreshT m) = FreshT $ mapStateT f m


  
   
