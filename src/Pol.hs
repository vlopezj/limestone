{-# LANGUAGE GeneralizedNewtypeDeriving, GADTs, DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
module Pol(module Pol) where

import Prelude hiding (map, mapM, foldl, foldl1)
import Data.Function
import Data.Tuple

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import Control.Monad hiding (mapM)
import Data.Traversable as T
import Data.Foldable hiding (toList)
import Control.Applicative
import Data.Bifunctor

import Data.Maybe
import Data.List (sortBy)
import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty(NonEmpty(..))

import Data.Ratio

import Pol.Term hiding (toList, fromList)
import qualified Pol.Term as Term
import Pol.Core as Pol
import Pol.NIA as Pol

import Control.Monad.Except.Extra
import Data.Either

import Control.Lens
import Control.Monad.Reader

bindListM :: (Ord s, Num a, Eq a, Monad m) => [(a,[r])] -> (r -> m (Pol a s)) -> m (Pol a s)
s `bindListM` f =
  liftM (foldl (+) zero) $
  T.mapM (\(a, rs) -> do
               rs' <- T.mapM f rs
               let rs'' = foldl times one rs'
               return $ a `timesNum` rs'') s

polJoin :: (Ord r, Num a, Eq a) => Pol a (Pol a r) -> Pol a r
polJoin = flip bind $ id

fromMonomial :: (Num a, Eq a, Ord ref) => Monomial a ref -> Pol a ref
fromMonomial = fromMonomials . (:[])

timesTerm :: (Ord ref) => Term ref -> Pol a ref -> Pol a ref
timesTerm = liftPolRaw . Map.mapKeysMonotonic . termProd

timesMonomial :: (Ord ref, Num a, Eq a) => Monomial a ref -> Pol a ref -> Pol a ref
timesMonomial (a,t) = timesTerm t . timesNum a

var :: (Num a) => ref -> Pol a ref
var = Pol . flip Map.singleton 1 . termVar

isZero :: Pol a ref -> Bool
isZero = Map.null . getPol
           
isScalar :: (Num a) => Pol a ref -> Bool
isScalar p = case asScalar p of
  Just _ -> True
  Nothing -> False
 
asScalar :: (Num a) => Pol a ref -> Maybe a
asScalar p = case asScalar' p of
  Right a -> Just a
  Left  _ -> Nothing

isNum :: (Eq a, Num a) => a -> Pol a ref -> Bool
isNum a p = case asScalar p of
  Just b  -> b == a
  Nothing -> False

isOne :: (Num a, Eq a) => Pol a ref -> Bool
isOne = isNum 1
 
addMonomial :: (Ord ref, Num a, Eq a) => Monomial a ref -> Pol a ref -> Pol a ref
addMonomial (a,t) = liftPolRaw $ flip Map.alter t
                                 (\v -> case (fmap (+ a) v) of
                                     Nothing    -> Just a
                                     Just 0     -> Nothing
                                     b@(Just _) -> b)

-- | Division by a single polynomial is locally confluent. (Proposition 5.33)
quotRem :: (Fractional a, Eq a, Ord ref) => Pol a ref -> Pol a ref -> (Pol a ref, Pol a ref)
quotRem f p  = maybe (error "Division by zero") id $ quotRemSafe f p

quotRemSafe :: (Fractional a, Eq a, Ord ref) => Pol a ref -> Pol a ref -> Maybe (Pol a ref, Pol a ref)
quotRemSafe _ p | isZero p = Nothing
quotRemSafe f p  = Just $ go f 0
  where
    hm = headMonomial p
    go f q =
      case reduce (monomials f) of
        Just m -> go (f - (m `timesMonomial` p)) (m `addMonomial` q)
        Nothing -> (q,f)

    reduce [] = Nothing
    reduce (x:xs) = maybe (reduce xs) Just (x `monomialDivide` hm)

toFractionalPol :: (Integral a) => Pol a ref -> Pol (Ratio a) ref
toFractionalPol = liftPolRaw $ fmap (%1)

toIntegralPol :: (Integral a) => Pol (Ratio a) ref -> Maybe (Pol a ref)
toIntegralPol q = Pol <$> T.mapM fractionalToIntegral (getPol q)
  where
    fractionalToIntegral :: (Integral a) => Ratio a -> Maybe a
    fractionalToIntegral x =
      case denominator x of
        1 -> Just $ numerator x
        _ -> Nothing
{-
quotRemIntegral :: (Integral a, Ord ref) => Pol a ref -> Pol a ref -> Maybe (Pol a ref, Pol a ref)
quotRemIntegral a b = do
  let (q,r) = (Pol.quotRem `on` toFractionalPol) a b
  q' <- toIntegralPol q
  r' <- toIntegralPol r
  return (q',r')
-}
-- Quotient of integral polynomials without going through the fractional representation
--
-- If dividing the polynomials over a Fractional would give a result, then
-- the result is the same as using `quotRemIntegralDirect`
quotRemIntegral :: (Integral a, Ord ref) => Pol a ref -> Pol a ref -> (Pol a ref, Pol a ref)
quotRemIntegral f p | isZero p = (0, f)
quotRemIntegral f p  = go f 0
  where
    hm = headMonomial p
    go f q =
      case reduce (monomials f) of
        Just m -> go (f - (m `timesMonomial` p)) (m `addMonomial` q)
        Nothing -> (q,f)

    reduce [] = Nothing
    reduce (x:xs) =
      case  x `monomialIntegralDivide` hm of
        Nothing     -> reduce xs
        Just (m, _) -> return m

divSafe :: (Fractional a, Eq a, Ord ref) => Pol a ref -> Pol a ref -> Maybe (Pol a ref)
divSafe a b = do
  (q,0) <- a `quotRemSafe` b
  return q

-- | Exact integral division of pols.
--   The coefficients must be arbitrary precision for this to work correctly.
divSafeIntegral :: (Integral a, Ord ref) => Pol a ref -> Pol a ref -> Maybe (Pol a ref)
divSafeIntegral a b = do
  case a `quotRemIntegral` b of
    (q, 0) -> return q
    _      -> Nothing
    
-- Notation as per Becker book on Gröbner Bases
(<~) :: (Ord ref) => Pol a ref -> Pol a ref -> Bool
(<~) = (<=) `on` (fmap fst . Map.toDescList . getPol)

-- ^ Partial function
headTerm :: Pol a ref -> Term ref
headTerm =  head . terms 

reductum :: (Ord ref) => Pol a ref -> Pol a ref
reductum = headTerm >>= liftPolRaw . Map.delete

monic :: (Num a, Eq a, Ord ref) => Pol a ref -> Bool
monic = (== 1) . headCoef 

terms :: Pol a ref -> [Term ref]
terms = fmap snd . toMonomials

natFromIntegral :: (Integral a) => a -> PolNat
natFromIntegral a =
  let b = fromIntegral a in
  if b < 0 then error $ show b ++ " is not a natural number."
           else PolNat b

newtype PolNat = PolNat { getNat :: Integer }

data PolSyntax a ref  = PAdd (PolSyntax a ref) (PolSyntax a ref)
                      | PSub (PolSyntax a ref) (PolSyntax a ref)
                      | PProd (PolSyntax a ref) (PolSyntax a ref)
                      | PPower (PolSyntax a ref) PolNat
                      | PParens (PolSyntax a ref)
                      | PNeg (PolSyntax a ref)
                      | PScalar a
                      | PVariable ref


syntax :: (Num a, Eq a, Ord ref) => Pol a ref -> PolSyntax a ref
syntax s =
  let m = sortBy (revdeglex `on` snd) $ monomials s in
  case m of
    [] -> PScalar 0
    (a,t):xs ->
      foldl (\acc (a,t) ->
        case numSyntax a of
          PNeg x -> PSub acc (x `termSyntax` t)
          x      -> PAdd acc (x `termSyntax` t))
        (termSyntax (numSyntax a) t) 
        xs
      where
        termSyntax :: (Num a, Eq a) => PolSyntax a ref -> Term ref -> PolSyntax a ref
        termSyntax coef t =
          let tc = Term.toList' t in
          case tc of
            [] -> coef
            _ ->
              let vs = fmap variableSyntax tc in
              case coef of
                PScalar 1        -> foldl1 PProd vs
                PNeg (PScalar 1) -> PNeg (foldl1 PProd vs)
                _                -> foldl PProd coef vs

        variableSyntax :: (Num a) => (ref,Int) -> PolSyntax a ref
        variableSyntax (_,n) | n <= 0 = error "Impossible, counts are positive"
        variableSyntax (v,1) = PVariable v
        variableSyntax (v,n) = PPower (PVariable v) (natFromIntegral n)

        numSyntax :: (Num a, Eq a) => a -> PolSyntax a ref
        numSyntax a =
          case signum a of
            (-1) -> PNeg (PScalar (negate a))
            0    -> PScalar 0
            _    -> PScalar a

polRefs :: Pol a ref -> [ref]
polRefs p = terms p >>= termRefs

rebaseMap :: (Ord s0, Ord s1, Num a, Eq a) => (r -> Either s0 s1) -> Pol a r -> Pol (Pol a s0) s1
rebaseMap f = fromMonomials . fmap (first fromMonomial . rebaseMapMonomial f) . toMonomials

polLinear :: (Ord ref, Num a, Eq a) => ref -> Pol a ref -> Maybe (Pol a ref, Pol a ref)
polLinear ref p = do
  let ms = toMonomials p
  (qs, rs) <- partitionEithers <$> T.mapM (monomialLinear ref) ms
  return (fromMonomials qs, fromMonomials rs)

solvePolLin :: (Ord ref, Num a, Eq a, Fractional a) => ref -> Pol a ref -> Either String (Pol a ref)
solvePolLin m p = do
  (a,b) <- maybe (throwError "Non-linear polynomial") return $ polLinear m p
  maybe (throwError "Non-exact division") return $ (-b) `divSafe` a

solvePolLinIntegral :: (Ord ref, Num a, Eq a, Integral a) => ref -> Pol a ref -> Either String (Pol a ref)
solvePolLinIntegral m p =
  do
    a <- solvePolLin m (toFractionalPol p)
    maybe (throwError "Non-integral solution") return $ toIntegralPol a
