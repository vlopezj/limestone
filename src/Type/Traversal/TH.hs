{-# LANGUAGE TemplateHaskell #-}
module Type.Traversal.TH where

import Type.Core
import Data.Generics.Genifunctors
import Control.Applicative

genTraverseType'' :: (Applicative f)
                 => (a₁ -> f b₁)
                 -> (a₂ -> f b₂)
                 -> (a₃ -> f b₃)
                 -> (a₄ -> f b₄)
                 -> Type'' a₁ a₂ a₃ a₄ -> f (Type'' b₁ b₂ b₃ b₄)

genTraverseType'' = $(genTraverse ''Type'')




