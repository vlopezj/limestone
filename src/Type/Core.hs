{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveFunctor,TypeSynonymInstances,FlexibleInstances,MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell,ScopedTypeVariables,PatternGuards,QuasiQuotes #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}

module Type.Core(module Type.Core
                ,module Type.Size.Core
                ,module Type.Symbol) where
import Id
import Data.Foldable
import Data.Traversable
import Control.Applicative
import Tagged as Type.Core
import Type.Size.Core
import Type.Symbol
import Data.Monoid(Monoid, mappend, mempty)
import Data.Generics.Geniplate ()
import Control.Lens
import Op

type Type = Type' Name Int
type IdType = Type' Id Id
type IdSized = Sized' Id Id
type IdSymbol = Symbol Id
type IdSize = Size Id

data Sized' name ref = Type' name ref :^ Size ref 
  deriving (Show)

dropSize :: Sized' name ref -> Type' name ref
dropSize (a :^ _b) = a
dropType :: Sized' name ref -> Size ref
dropType (_a :^ b) = b

infix 4 :^
           
data Rec = Mu | Nu
  deriving (Eq,Show,Read,Ord)

isKindType :: Type' name ref -> Bool
isKindType (T (TType)) = True
isKindType _         = False

isValueType :: Type' name ref -> Bool
isValueType = not . isKindType

data BosonVar = BosonVar Id deriving (Show,Eq,Ord)

data ArrayPol' p where
  APositive :: ArrayPol' p
  ANegative :: ArrayPol' p
  ANeutral'  :: Maybe (PolVar' p) -> ArrayPol' p 

pattern ANeutralU = ANeutral' Nothing
pattern ANeutral x = ANeutral' (Just x)


deriving instance (Show p) => Show (ArrayPol' p)
deriving instance (Read p) => Read (ArrayPol' p)
deriving instance (Ord  p) => Ord (ArrayPol' p)
deriving instance (Eq  p) => Eq (ArrayPol' p)
deriving instance Functor (ArrayPol')
deriving instance Foldable (ArrayPol')
deriving instance Traversable (ArrayPol')

data PolVar' p = PolConst Bool
              | PolVar   { isPositiveVar :: Bool
                         , idVar         :: p
                         }
                deriving (Show,Read,Eq,Ord,Functor,Traversable,Foldable)

instance Polar (PolVar) where
  arrayPolarity t = ANeutral' (Just t)

instance Dual (PolVar) where
  dual t = case t of
    PolConst b -> PolConst (dual b)
    PolVar b v -> PolVar (dual b) v

type PolVar = PolVar' Id

instance Polar (ArrayPol) where
  arrayPolarity = id

arrayPol :: Polarity -> ArrayPol' p
arrayPol Positive = APositive
arrayPol Negative = ANegative
arrayPol Neutral  = ANeutralU

instance Dual (ArrayPol) where
  dual t = case t of
    APositive     -> ANegative
    ANegative     -> APositive
    ANeutralU     -> ANeutralU 
    ANeutral v    -> ANeutral (dual v)

deparamPol :: ArrayPol' p -> ArrayPol' q
deparamPol t  = case t of
    ANeutral (PolVar _b _v) -> ANeutralU
    -- Rest are invariant
    APositive  -> APositive
    ANegative  -> ANegative
    ANeutralU  -> ANeutralU
    ANeutral (PolConst b) -> ANeutral (PolConst b)

paramPol :: (Applicative m) => m a -> ArrayPol' p -> m (ArrayPol' a)
paramPol m t = case t of
       ANeutralU   -> polVar <$> m
       ANeutral (PolVar _ _) -> polVar <$> m

       APositive   -> pure $ APositive
       ANegative   -> pure $ ANegative
       ANeutral (PolConst b) -> pure $ ANeutral (PolConst b)

polVar :: a -> ArrayPol' a
polVar x = ANeutral (PolVar True x)

type ArrayPol = ArrayPol' Id

-- | Abstract syntax for types
data Type'' ty sz name ref
    =
    -- Kinds
      TType -- ^ Type of types
    | TSize -- ^ The type of sizes, TSize ⊂ Type
    | TProp (sz,CmpOp,sz) -- ^ A type for predicates on sizes

    -- Type-level values
    | SizeT { getSizeT :: sz } -- ^ A size embedded as a type
    | Prop                    -- ^ Proof of a proposition

    -- Types
    | ty :+: ty
    | ty :&: ty
    | Product ArrayPol ty ty
    | Zero | Top | Unit ArrayPol
    | Var [ty] (Symbol ref) 
    | Forall name (ty) (ty)
    | Exists name (ty) (ty)
    | Bang (ty)
    | Quest (ty)
    | Rec Rec name (ty) -- False if the mu is negated; the subtype has an extra free var.
    | MetaNeg (ty) -- (irreducible negation, not found anywhere but in typesetting code)
    | RevNeg (ty) -- ^ invisible negation; used only in typesetting coupling diagrams  (graphiz)
    | Lollipop (ty) (ty) -- Used for printing, and function declarations

    | Array' ArrayPol [(sz,ty)] -- + for ⊗;  - for ⅋ ; 0 for §
    -- Polarized sequence. Used when compiling
    | Index Bool
        {- ^ False : negated Index
             True  : positive Index
        -}
    | Primitive Bool Name
        {- ^ False : negated primitive
             True  : positive primitive
        -}
    -- For the CPS article
   | Plupp (ty)
   | UnpushedNeg (ty)
  deriving (Show,Eq,Ord)

pattern a :*: b = Product APositive a b
pattern a :|: b = Product ANegative a b
pattern a :>: b <- Product (ANeutral' _) a b 

pattern One  = Unit APositive
pattern Bot  = Unit ANegative
pattern Pole <- Unit (ANeutral' _)
pattern Array p sz ty = Array' p [(sz,ty)]

cPole :: forall ty sz name ref. Type'' ty sz name ref
cPole = Unit ANeutralU

tM :: (Functor f) => f (Type'' (Type' name ref) (Size ref) name ref) -> f (Type' name ref)
tM = fmap T

t'M :: (Functor f) => f (Type' name ref) -> f (Type'' (Type' name ref) (Size ref) name ref)
t'M = fmap unT

newtype Type' name ref = T { unT :: Type'' (Type' name ref) (Size ref) name ref } deriving (Show)

-- We use C names to easen translation, but this is not necessary.
pattern TInt b = Primitive b "int"
pattern TDouble b = Primitive b "double"
pattern TChar b = Primitive b "char"
pattern TComplex b = Primitive b "double complex"
pattern TError b = Primitive b "error_t"

pattern Int b = TInt b

pattern PInt = TInt True
pattern PDouble = TDouble True
pattern PChar = TChar True
pattern PComplex = TComplex True

data Polarity = Negative | Positive | Neutral
  deriving (Eq,Show,Read,Ord)

instance Monoid Polarity where
  mempty = Positive
  Positive `mappend` a = a
  Negative `mappend` a = dual a
  Neutral  `mappend` _a = Neutral

instance Dual Polarity where
  dual Negative = Positive
  dual Positive = Negative
  dual Neutral  = Neutral

class Polar a where
  {-# MINIMAL polarity | arrayPolarity #-}
  polarity :: a -> Polarity
  polarity = defPolarity

  arrayPolarity :: a -> ArrayPol
  arrayPolarity = defArrayPolarity

defPolarity :: (Polar a) => a -> Polarity 
defPolarity a = case arrayPolarity a of
  APositive   -> Positive
  ANeutral'{} -> Neutral
  ANegative   -> Negative

defArrayPolarity :: (Polar a) => a -> ArrayPol
defArrayPolarity a = case polarity a of 
  Positive -> APositive
  Negative -> ANegative
  Neutral  -> ANeutralU

instance Polar Polarity where
  polarity = id

instance Polar Bool where
  polarity True = Positive
  polarity False = Negative

pol :: forall a. Polar a => a -> Polarity
pol t = polarity t

class Dual x where
  dual :: x -> x

instance Dual a => Dual (Maybe a) where
  dual = fmap dual

instance Dual Bool where
  dual = not

-- | dual . dual == id
--   Furthermore, if x is an instance of polar, it must
--   satisfy:
--
--   dual . polarity = polarity . dual 

instance Polar Rec where
  polarity Mu = Positive
  polarity Nu = Negative

instance Dual Rec where
  dual Mu = Nu
  dual Nu = Mu

class Ord r => EqType n r where
    eqType :: Type' n r -> Type' n r -> Bool

eqSized :: (EqType name ref, EqSize ref) => Sized' name ref -> Sized' name ref -> Bool
(a :^ sz₁) `eqSized` (b :^ sz₂) = a `eqType` b && sz₁ `eqSize` sz₂

pattern PolConstPos = PolConst True
pattern PolConstNeg = PolConst False

pattern ArraySeqPol p sz ty = Array (ANeutral p) sz ty

pattern ArraySeqPos sz ty = ArraySeqPol (PolConst True) sz ty
pattern ArraySeqNeg sz ty = ArraySeqPol (PolConst False) sz ty

pattern ArrayPar    sz tt = Array ANegative sz tt
pattern ArrayTensor sz tt = Array APositive sz tt
pattern ArraySeq    sz tt <- Array (ANeutral' _) sz tt

cArraySeq :: forall ty sz name ref. sz -> ty -> Type'' ty sz name ref
cArraySeq sz tt = Array (ANeutral' Nothing) sz tt

sizeAsType :: Simple Prism (Type' name ref) (Size ref)
sizeAsType = prism (T . SizeT) (\t -> case t of
                                   T (SizeT sz) -> Right sz
                                   _            -> Left t)
                                   
sizeAsTypeUnsafe :: Simple Iso (Size ref) (Type' name ref)
sizeAsTypeUnsafe = iso  (T . SizeT) (getSizeT . unT)

-- (jyp-set-file-local-variable 'flycheck-haskell-ghc-executable "nix-ghc")
-- (jyp-set-file-local-variable 'flycheck-ghc-search-path '(".."))
-- (flycheck-compile 'haskell-ghc)

-- Local Variables:
-- flycheck-haskell-ghc-executable: "nix-ghc"
-- flycheck-ghc-search-path: ("..")
-- End:

option :: Type' name ref -> Type' name ref
option a = T$ a :+: (T One)

lazy :: Type' name ref -> Type' name ref
lazy a = T$ a :&: (T One)


