{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
module Type.Size.Core where

import Data.Foldable
import Data.Traversable
import Type.Symbol

-- | Type for size expressions
type SizeSymb ref = Symbol ref

data Size ref = Size { getSize :: [(Integer,[SizeSymb ref])] }
              | SizeExp  (Size ref)
               deriving (Show, Read, Functor, Traversable, Foldable)

class EqSize r where
  eqSize :: Ord r => Size r -> Size r -> Bool


