{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
module Type.Data(module Type.Data) where
 
import Type
import Type.Data.Core as Type.Data

import MetaVar
import Control.Monad.Except.Extra
import Control.Applicative
import Type.Traversal
import Data.Functor.Identity

dataAsType :: DataType name ref -> Type' name ref
dataAsType (DT ty) = T $ runIdentity $ traverseType'' defTypeTraverse{tty = pure . dataAsType} ty

-- | More general dual type
--   In particular, introduced sequences are not dualized,
--   so they will be able to all take a positive value when they are polarized. 
dataAsDualType :: DataType name ref -> Type' name ref
dataAsDualType = dataDual . dataAsType 
  where
    dataDual = dataNeg

    dataNeg  = foldTypeWithSelf' defTypeFold{sty = \t' t -> pure . T $ dataNeg0 t' t}

    dataNeg0 _ (ArraySeqPol p sz ty) = ArraySeqPol p sz ty
    dataNeg0 t' t = neg0' t' t

mkData :: (Applicative m, MonadError String m, Show name, Show ref) => Type' name ref -> m (DataType name ref)
mkData = foldType defTypeFold{sty}
  where
    sty _ t = fmap DT$ case t of
      Index True       -> ok
      Primitive True _ -> ok
      Bang{}           -> ok
      (:+:){}          -> ok
      (:*:){}          -> ok
      (:|:){}          -> ok
      Array{}          -> ok
      Unit{}           -> ok
      Exists _ _ _     -> ok
      TSize            -> ok
      Var _ (TVar True _) -> ok
      Var _ (Meta _)      -> ok
      Var _ (MetaSyntactic{})      -> ok
      _                -> throwError$ "Not data: " ++ show (pruneTy t)
      where
        ok = return t


dataWrite :: DataType name ref -> Type' name ref
dataWrite t = dataAsDualType t
            {-
  foldType defTypeFold{sty}
  where
    sty _ t = tM$ case t of
        -- These are dualized
        (Index True) -> pure$ Index False
        (Primitive True name)   -> pure$ Primitive False name
        (Bang (T t))  -> pure$ t -- ^ Only needs to be produced one
        (x :+: y) -> pure$ x :&: y
        Zero -> pure Top

        -- This preserve the control
        (Array APositive sz t) -> pure$ Array APositive sz t
        a@(_ :*: _) -> pure a
        a@(One)     -> pure a

        -- Not so sure about how these work:
        (Exists v s t) -> pure$ Forall v s t
        (Var xs (TVar b x))     -> pure$ Var xs (TVar (not b) x)
        (Rec b n t)    -> pure$ Rec (dual b) n t
        (Var xs (MetaSyntactic b bs x))      -> pure$ Var xs $ MetaSyntactic (not b) (not bs) x 
        (Var xs (Meta (MetaVar pos u sc i))) -> pure$ Var xs (Meta (MetaVar (not pos) u sc i))
        (MetaNeg (T x)) -> pure x
        (RevNeg _) -> throwError "dataWrite: RevNeg"
        Plupp{}    -> throwError "dataWrite: Plupp"
        (UnpushedNeg t) -> pure$ UnpushedNeg t -- debatable
        TType      -> throwError "dataWrite: TType"
        (SizeT _)     -> throwError "dataWrite: SizeT"
        _ -> throwError "Not data"
-}

-- maybe add exponential, or?
dataRead :: DataType name ref -> Type' name ref
dataRead t = dataAsType t

dataUpdate :: DataType name ref -> Type' name ref
dataUpdate t = T$ dataRead t :*: lazy (dataWrite t)

instance Polar (DataType name ref) where
  polarity = polarity . dataAsType


