{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}
module Type.Traversal where

import Type.Core
import Type.Data.Core
import Type.Traversal.TH as Type.Traversal
import Data.Bifunctor
import Data.Bitraversable
import Data.Bifoldable
import Data.Traversable
import Data.Foldable
import Control.Applicative
import Data.Generics.Genifunctors
import Data.Monoid
import Control.Monad
import Data.Functor.Identity
import Control.Monad.Writer
import Control.Monad.State
import Type.Data.Core
import Op

data TypeTraverse m ty  sz  name  ref
                    ty' sz' name' ref' =
  TypeTraverse {
    tty   :: ty   -> m ty',
    tsz   :: sz   -> m sz',
    tname :: name -> m name',
    tref  :: ref  -> m ref' }

traverseType'' TypeTraverse{..} = genTraverseType'' tty tsz tname tref

defTypeTraverse = TypeTraverse pure pure pure pure

defTypeTraverseVoid :: (Applicative m) => TypeTraverse m ty sz name ref
                                                         () () ()   ()
defTypeTraverseVoid = TypeTraverse v v v v where v _ = pure () 

data TypeFold m ann ty sz  name  ref
                           name' ref' =
  TypeFold {
      sty   :: ann -> Type'' ty sz name ref -> m ty
     ,ssz   :: ann -> (Size ref) -> m sz
     ,sname :: ann -> name' -> m name
     ,sref  :: ann -> ref'  -> m ref
    } 


defTypeZipWithM = TypeTraverse (pure . fst) (pure . fst) (pure . fst) (pure . fst)  

defTypeFold :: Applicative m => TypeFold m ann (Type' name ref) (Size ref)
                                               name ref 
                                               name ref

zipWithTypeM :: (Monad m, Applicative m) => (Type'' ty (Size ref) name ref ->
            Type'' ty (Size ref) name ref ->
            m ty) ->
           Type' name ref ->
           Type' name ref ->
           m ty
zipWithTypeM f (T ty₁) (T ty₂) = zipWithTypeM'' (zipWithTypeM f) ty₁ ty₂ >>= uncurry f

zipWithTypeM'' :: (Monad m, Applicative m)
                  => (ty -> ty -> m ty')
                  -> Type'' ty sz name ref
                  -> Type'' ty sz name ref
                  -> m (Type'' ty' sz name ref, Type'' ty' sz name ref)

zipWithTypeM'' f t₁ t₂ = do
  let elems₁ = execWriter $ flip traverseType'' t₁ defTypeTraverseVoid{
      tty = \ty₁ -> append_ ty₁
    }
  (t₁',res) <- runWriterT $ flip evalStateT elems₁ $ flip traverseType'' t₂ defTypeTraverse{
      tty = \ty₂ -> pop >>= \ty₁ -> lift (lift (f ty₁ ty₂)) >>= append
    }
  let t₂' = flip evalState res $ flip traverseType''  t₁ defTypeTraverse{
      tty = \_   -> pop
    } 
  return (t₁',t₂')
   
  where 
    pop = state $ \(e:es) -> (e, es)
    append a = tell [a] >> return a
    append_ a = tell [a] 

defTypeFold = TypeFold 
                    (const $ pure . T)
                    (const pure)
                    (const pure)
                    (const pure)

foldType :: (Applicative m, Monad m)
         => TypeFold m () ty sz name' ref'
                                name  ref
         -> Type' name ref
         -> m ty
foldType fold@TypeFold{..} (T ty) =
  do
    ty' <- traverseType'' TypeTraverse{..} ty
    sty () ty' 
  where
    tname = sname ()
    tref  = sref ()
    tsz   = traverse tref >=> ssz ()
    tty   = foldType fold

foldDataType :: (Applicative m, Monad m)
         => TypeFold m () ty sz name' ref'
                                name  ref
         -> DataType name ref
         -> m ty
foldDataType fold@TypeFold{..} (DT ty) =
  do
    ty' <- traverseType'' TypeTraverse{..} ty
    sty () ty' 
  where
    tname = sname ()
    tref  = sref ()
    tsz   = traverse tref >=> ssz ()
    tty   = foldDataType fold

foldType' :: 
            TypeFold Identity () ty sz name' ref'
                                       name  ref
         -> Type' name ref
         -> ty
foldType' = (runIdentity .) . foldType

foldTypeWithSelf' :: 
            TypeFold Identity (Type' name ref)
                       ty sz name' ref'
                             name  ref
         -> Type' name ref
         -> ty
foldTypeWithSelf' = (runIdentity .) . foldTypeWithSelf

foldTypeWithSelf :: (Applicative m, Monad m)
         => TypeFold m (Type' name ref)
                       ty sz name' ref'
                             name  ref
         -> Type' name ref
         -> m ty
foldTypeWithSelf fold@TypeFold{..} sn@(T ty) =
  do
    ty' <- traverseType'' TypeTraverse{..} ty
    sty sn ty'
  where
    tname = sname sn
    tref  = sref sn
    tsz   = traverse tref >=> ssz sn
    tty   = foldTypeWithSelf fold


instance Bifunctor Type' where bimap = bimapDefault                                           
instance Bifoldable Type' where bifoldMap = bifoldMapDefault
instance Bitraversable Type' where bitraverse = $(genTraverseT [(''Type'', 'genTraverseType'')] ''Type')

instance Functor (Type' name) where fmap = bimap id
instance Foldable (Type' name) where foldMap = bifoldMap (const mempty)
instance Traversable (Type' name) where traverse = bitraverse pure

deriving instance Functor (Sized' name)
deriving instance Foldable (Sized' name)
deriving instance Traversable (Sized' name)

pruneTy :: Type'' ty sz name ref -> Type'' (:…) sz name ref
pruneTy = runIdentity . traverseType'' defTypeTraverse{tty = const $ return (:…)}

