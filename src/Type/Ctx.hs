{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveFunctor, DeriveTraversable, DeriveFoldable #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Type.Ctx(module Type.Ctx,
                mempty) where

import Development.Placeholders
import Id
import Type.Core
import Type.Size
import Control.Arrow
import Control.Lens
import Data.Monoid hiding (Dual)
import Data.Tagged
import Data.Proxy
import Tagged
import Data.Either
import Data.List
import Data.Foldable (Foldable)
import Data.Traversable (Traversable)
import Data.Default
import Control.Applicative
import Control.Lens
import Control.Monad.Except.Extra
import Data.Generics.Geniplate

type IdCtx   = Ctx' Id Id

type CtxElem name tysz = (name, tysz)
type Ctx'' name tysz = [CtxElem name tysz]
type Ctx' name ref = Ctx'' name (Sized' name ref) 

type EmptyCtx name ref = ()
type LinearCtx name ref = [ref]
type TypedCtx name ref = SeqCtx (name ::: Sized' name ref)

type IdTypedCtx = TypedCtx Id Id

pattern EmptyCtx = ()

data SeqCtx ref = SeqCtx {
  _intuit :: [ref],
  _linear :: [ref] 
} deriving (Show, Read, Functor, Traversable, Foldable)

$(makeLenses ''SeqCtx)

seqctx :: [(a,b)] -> [(a,b)] -> SeqCtx (a ::: b)
seqctx intuit linear = SeqCtx { _intuit = fmap fromTuple intuit
                              , _linear = fmap fromTuple linear }

seqctx_ :: SeqCtx (a ::: b) -> ([(a,b)],[(a,b)])
seqctx_ = (,) <$> view (intuit . mapping tupled) <*> view (linear . mapping tupled)

data SeqCtxBehaviour = Linear | Intuit deriving (Show, Eq, Ord, Read)

pattern (:::~) x y = x ::: (Linear, y)
pattern (:::!) x y = x ::: (Intuit, y)
pattern (:::.) x y = x ::: y :^ 1 

infix 3 :::~
infix 3 :::!
infix 3 :::.

type    (:::@) x y = x ::: (SeqCtxBehaviour,y)

instance (Dual ty) => Dual (v ::: ty) where
  dual (v ::: ty) = v ::: dual ty

instance (Dual ty) => Dual (SeqCtxBehaviour, ty) where
  dual (bhv, ty) = (bhv, dual ty)

instance Default (SeqCtx ref) where
  def = mempty 

instance Monoid (SeqCtx ref) where
  mempty = SeqCtx mempty mempty
  a `mappend` b = SeqCtx { _intuit = _intuit a <> _intuit b
                  , _linear = _linear a <> _linear b
                  }

matchTypedCtx :: (MonadError String m, EqType name ref, EqSize ref, Show name, Show ref)
                 => TypedCtx name ref -> TypedCtx name ref -> m ()
matchTypedCtx src dst = do
    go' (src ^. intuit) (dst ^. intuit)
    go' (src ^. linear) (dst ^. linear)
    return ()
    where
        go' the_as the_bs = go the_as the_bs
          where
            go []     [] = return ()
            go (a@(_ ::: ty₁):as) (b@(_ ::: ty₂):bs) = do
--              when (not (ty₁ `eqSized` ty₂)) $ throwError $ "Expected " ++ show a ++ ", got " ++ show ty₂ ++ "."
              go as bs
            go _ _  =  throwError $ "Count mismatch. Expected " ++ show (length the_as) ++ " ≠ " ++ show (length the_bs) ++ "."

{-
data CtxI name ref  = CtxTy { getTy :: Sized' name ref }
                    | CtxSz { getSz :: Size ref } deriving (Show)

type Ctx = Ctx' Name Int

type CtxTy name ref = [(name, Sized' name ref)]
type CtxSz name ref = [(name, Size ref)]

ctxPartition :: Ctx' name ref -> (CtxTy name ref, CtxSz name ref)
ctxPartition [] = ([],[])
ctxPartition ((k, CtxTy v):xs) = (first ((k,v):))  $ ctxPartition xs
ctxPartition ((k, CtxSz v):xs) = (second ((k,v):)) $ ctxPartition xs

ctxMerge :: CtxTy name ref -> CtxSz name ref -> Ctx' name ref
ctxMerge a b = map (second CtxTy) a ++ map (second CtxSz) b

ctxTy :: Lens' (Ctx' name ref) (CtxTy name ref)
ctxTy = lens (fst . ctxPartition) $ \s a -> a `ctxMerge` (snd $ ctxPartition s)

ctxSz :: Lens' (Ctx' name ref) (CtxSz name ref)
ctxSz = lens (snd . ctxPartition) $ \s b -> (fst $ ctxPartition s) `ctxMerge` b 
-}

demoteBy :: (Ord ref, Show name, Show ref) => Size ref -> Ctx' name ref -> Ctx' name ref
demoteBy sz xs = [ (y,a:^q)
                 | (y,a:^d) <- xs,
                   let q = sizeDivSafe d sz
                 ]

demoteBySized :: (Ord ref, Show name, Show ref) => Size ref -> Sized' name ref -> Maybe (Sized' name ref)
demoteBySized sz (a :^ d) = case sizeDiv d sz of
  Just q  -> Just $ a :^ q
  Nothing -> Nothing

demoteBy' :: (Ord ref, Show name, Show ref)
             => Size ref
             -> [ref ::: Sized' name ref]
             -> Either String [ref ::: Sized' name ref]
demoteBy' sz xs =
  -- Tag the errors with a special value to prevent mixups
  let pError = Proxy :: Proxy "Error" in
  case partitionEithers $
                  [ case sizeDiv d sz of
                      Just q  -> Right $ y ::: a :^ q
                      Nothing -> Left  $ tagWith pError $ y ::: a :^ d
                  | y ::: a :^ d <- xs ] of
    ([], result) -> Right result
    (errors, _)  -> Left $ "Variables " ++ intercalate ", " [show $ proxy e pError | e <- errors] ++ " cannot be demoted by " ++ show sz
    

demoteSeqBy :: (Ord ref, Show name, Show ref) => Size ref -> Ctx' name ref -> Ctx' name ref
demoteSeqBy sz zs =
  [(y, e :^ m) | (y, t@(T (ArraySeq d ty)) :^ m) <- zs,
                 let e = demoteSeqBy' sz t ]

demoteSeqBy' :: (Ord ref, Show name, Show ref) => Size ref -> Type' name ref -> Type' name ref
demoteSeqBy' sz (T (ArraySeq d ty)) = 
   let q = sizeDivSafe d sz in
   case q of
     1 -> ty
     _ -> $(todo "Folding over sequences in steps bigger than 1 not yet implemented")

demoteSeqByAny :: (Ord ref, Show name, Show ref) => Ctx' name ref -> Ctx' name ref
demoteSeqByAny zs =
  [(y, e :^ m) | (y, t@(T (ArraySeq d ty)) :^ m) <- zs,
                 let e = demoteSeqByAny' t ]

demoteSeqByAny' :: (Ord ref, Show name, Show ref) => Type' name ref -> Type' name ref
demoteSeqByAny' (T (ArraySeq d ty)) = ty

typeOfSized :: Sized' name ref -> Type' name ref
typeOfSized (ty :^ sz) = T (Array APositive sz ty)

typeOfCtx :: (Ord ref) => Ctx' name ref -> Type' name ref
typeOfCtx []  = T One
typeOfCtx ctx = foldl1 ((T .) . (:|:)) [typeOfSized tysz | (name, tysz) <- ctx]

sizeCtx :: (Eq ref) => [Size ref] -> [(ref, Sized' name ref)]
sizeCtx sz = (,T TSize :^ sizeOne) <$> nub [ref | TVar _ ref <- sz >>= sizeSymbs]
