{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE TemplateHaskell,QuasiQuotes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE PatternSynonyms #-}

module Type.Size (module Type.Size,
                  BOrdering(..),
                  PolSyntax(..),
                  PolNat(..))
       where

import Prelude hiding (foldl)
import Pol 
import Type.Symbol
import Id
import Data.Traversable as T
import Data.Foldable as T
import Control.Applicative
import Data.Function
import Data.Maybe
import Control.Arrow
import Control.Monad.Reader
import Fresh
import Data.List as List
import Data.Generics.Geniplate
import Data.Generics.Genifunctors
import MetaVar
import Control.Monad.State
import Type.Core
import Data.Void
import Data.Default
import Binders
import GHC.Exts
import Pol.NIA
import Control.Lens
import ContM

-- ^ Operations are handled with a general polynomial module
type SizePol ref = Pol Integer (SizeSymb ref)
                
instance (Eq ref, Ord ref) => Eq (Size ref) where
  (Size s)  == (Size t)   = ((==) `on` Pol.fromList) s t
  SizeExp s == SizeExp t  = s == t
  _         == _          = False

instance (Eq ref, Ord ref) => Ord (Size ref) where
  (SizeExp s) `compare` (SizeExp t)  = s `compare` t
  _           `compare` SizeExp _    = LT
  SizeExp _   `compare` _            = GT
  Size s      `compare` Size t       = (compare `on` Pol.fromList) s t

fromPol :: SizePol ref -> Size ref
fromPol = Size . Pol.toList

toPol :: (Ord ref) => Size ref -> SizePol ref
toPol Size {..} = Pol.fromList $ getSize

withPol :: (Ord r) => (SizePol r -> SizePol s) -> Size r -> Size s
withPol = (fromPol .) . (. toPol)

normalize :: (Ord r) => Size r -> Size r
normalize = fromPol . toPol 

withPol2 :: (Ord r, Ord s) =>
            (SizePol r -> SizePol s -> SizePol t) ->
            Size r -> Size s -> Size t
withPol2 f a b = fromPol (f (toPol a) (toPol b))

withPol2F :: (Ord r, Ord s, Functor f) =>
            (SizePol r -> SizePol s -> f (SizePol t)) ->
            Size r -> Size s -> f (Size t)
withPol2F f a b = fmap fromPol (f (toPol a) (toPol b))


sizeBind :: (Ord s) => Size r -> ((Symbol r) -> Size s) -> Size s
sizeBind Size{..} f = fromPol $ getSize `bindList` (toPol . f)
sizeBind (SizeExp s) f = SizeExp (s `sizeBind` f)

sizeIsInteger :: (Ord r) => Size r -> Maybe Integer
sizeIsInteger = Pol.asScalar . toPol

sizeEval :: Size r -> ((Symbol r) -> Integer) -> Integer
sizeEval s f = fromJust $ sizeIsInteger $ ((s `sizeBind` (fromIntegral . f)) :: Size Void)

sizeBindM :: (Monad m, Ord s) => Size r -> ((Symbol r) -> m (Size s)) -> m (Size s)
sizeBindM Size{..} f = liftM fromPol $ getSize `bindListM` (liftM toPol . f)
sizeBindM (SizeExp s) f = liftM SizeExp $ s `sizeBindM` f

sizeLit :: Integer -> Size r
sizeLit 0 = Size []
sizeLit n = Size [(n,[])]

sizeZero = sizeLit 0
sizeOne = sizeLit 1

instance UniverseBi (Size ref) (Symbol ref) where
  universeBi = $(genUniverseBi' [t| Size ref -> [Symbol ref] |])

sizeSymbs :: forall ref. Size ref -> [Symbol ref]
sizeSymbs = universeBi 

instance (Ord ref, Eq ref) => Num (Size ref) where
  (+) = withPol2 (+)
  (-) = withPol2 (-)
  (*) = withPol2 (*)
  abs = withPol abs
  signum = withPol signum
  negate = withPol negate
  fromInteger = sizeLit

sizeToList :: (Ord ref) => Size ref -> [(Integer,[Symbol ref])]
sizeToList = Pol.toList . toPol

sizePow :: Size ref -> Size ref
sizePow x = SizeExp x

sizeLog :: Size ref -> Maybe (Size ref)
sizeLog (SizeExp sz) = Just sz
sizeLog _ = Nothing

sizeDiv :: (Ord ref) => Size ref -> Size ref -> Maybe (Size ref)
sizeDiv = withPol2F divSafeIntegral

sizeAdd, sizeSub, sizeMul :: (Ord ref, Eq ref) => Size ref -> Size ref -> Size ref
sizeAdd = (+)
sizeSub = (-)
sizeMul = (*)
 
sizeDivSafe :: (Show ref, Ord ref) => Size ref -> Size ref -> Size ref
sizeDivSafe s1 s2 = fromMaybe (error $ "Division: " ++ show s2 ++ " does not divide " ++ show s1)
                          $ s1 `sizeDiv` s2

isZero :: (Ord ref) => Size ref -> Bool
isZero (Size []) = True
isZero s0        = Pol.isZero $ toPol s0

isOne :: (Ord ref) => Size ref -> Bool
isOne = Pol.isOne . toPol

sizeVar :: ref -> Size ref
sizeVar = sizeSymb . symbolVar

sizeSymb ::  Symbol ref -> Size ref
sizeSymb symb = Size [(1, [symb])]

idxSize :: (Eq name) => Size name -> ReaderT [name] (Either name) (Size Int)
idxSize s0 = go s0
  where
    go s0 = case s0 of
      Size l    -> Size <$> T.mapM (\(a, t) -> (a,) <$> T.mapM idxSymbol t) l
      SizeExp s -> SizeExp <$> go s

szSubst :: (MonadFresh m, Ord a) => a -> Size a -> Size a -> m (Size a)
szSubst i s = szSubsts [(i,s)]
    
szSubsts :: (MonadFresh m, Ord a) => [(a,Size a)] -> Size a -> m (Size a)
szSubsts su  s = return $
  s `sizeBind` (\symb -> case symb of
               TVar pos i | Just t <- lookup i su -> t
               _                                  -> sizeSymb symb)

instance EqSize Int where
  eqSize = (==)

instance EqSize Id where
  eqSize t1 t2 = case (k t1,k t2) of
        (Right u1,Right u2) -> u1 `eqSize` u2
        _                   -> False
      where
        refs' = nub (szRefs t1 ++ szRefs t2)
        k :: Size Id -> Either Id (Size Int)
        k t = runReaderT (idxSize t) refs'

eqSizeM :: (MonadNIASize a m) => Size a -> Size a -> ContM m Bool
eqSizeM sz₁ sz₂ = fmapContM (maybe False id) $ sizePredProve (sz₁,Eq,sz₂) 

eqTypeM :: (MonadNIASize a m) => Size a -> Size a -> ContM m Bool
eqTypeM sz₁ sz₂ = fmapContM (maybe False id) $ sizePredProve (sz₁,Eq,sz₂) 

szRefs :: (Ord ref) => Size ref -> [ref]
szRefs = (polRefs . toPol) >=> symbRefs

szRenameList :: forall a. Eq a => [(a,a)] -> Size a -> Size a
szRenameList su = tr $ \ x -> fromMaybe x (lookup x su)
  where
    tr :: (a -> a) -> Size a -> Size a
    tr = $(genTransformBiT' [ [t|MetaVar a|] ]
                            [t| (a -> a) -> Size a -> Size a |])

instance IsString ref => IsString (Size ref) where fromString = fromStringEmbed      

type SizePolSyntax ref = PolSyntax Integer (SizeSymb ref)

data SizeSyntax ref = PSize (SizePolSyntax ref)
                    | PExp (SizeSyntax ref)

type IdSizeSyntax = SizeSyntax Id
type IdSizePolSyntax = SizePolSyntax Id


sizeSyntax :: (Ord ref) => Size ref -> SizeSyntax ref
sizeSyntax (SizeExp s)  = PExp (sizeSyntax s)
sizeSyntax s            = PSize $ syntax $ toPol s

sizeFromNames ::  Traversable p => p Name -> p Id
sizeFromNames = sizeFromAnythingFlag True idFromName

sizeFromNamesLiberal ::  Traversable p => p Name -> p Id
sizeFromNamesLiberal = sizeFromAnythingFlag False idFromName

  
sizeFromAnythingFlag :: (Traversable p,Show a,Ord a)
                    => Bool -- ^ if False, free variables will also be renamed.
                    -> (Unique -> a -> Id) -> p a -> p Id
sizeFromAnythingFlag safe mk u = runFreshM (evalStateT (traverse look u) [])
  where
    fresh_name s = do
       x <- lift fresh
       let r = mk x s
       modify ((s,r):)
       return r
    look s = do
      m <- get
      case lookup s m of
        Nothing ->
          if safe
              then error $ "Unknown identifier : " ++ show s
              else fresh_name s
        Just x -> return x

  
class (HasRef a, Num (Size (Ref a))) => SizeAction a where
  resizeBy :: Size (Ref a) -> a -> a

class (HasRef a, Num (Size (Ref a))) => SizeActionM m a where
  resizeByM :: Size (Ref a) -> a -> m a

(.^) :: SizeAction a => a -> Size (Ref a) -> a
(.^) = flip resizeBy 

instance HasRef (Size ref) where
  type Ref (Size ref) = ref
  
instance EmbedSymbol (Size ref) where
  symb s = Size [(1, [s])]

instance HasRef (Sized' name ref) where
  type Ref (Sized' name ref) = ref

instance (Ord ref, Eq ref) => SizeAction (Sized' name ref) where
  resizeBy sz = (:^) <$> dropSize <*> ((sz *) <$> dropType)

instance SizeAction a => SizeAction [a] where
  resizeBy sz = map (resizeBy sz)

instance SizeAction a => SizeAction (b, a) where
  resizeBy = second . resizeBy

type SizePredicates ref = PolPredicates (Symbol ref)
type IdSizePredicates = SizePredicates Id
type MonadNIASize ref m = (Ord ref, Show ref, MonadNIA (Symbol ref) m)
type MonadNIAIdSize m = MonadNIASize Id m

sizeNoBranchLEq :: (MonadNIASize ref m) => Size ref -> Size ref -> ContM m (Maybe BOrdering) 
sizeNoBranchLEq (toPol -> szl) (toPol -> szr) = polLEq szl szr

sizePredAdd :: (MonadNIASize ref m) => (Size ref, CmpOp, Size ref) -> ContM_ m
sizePredAdd (sz₁, op, sz₂) = polPredAdd (toPol sz₁, op, toPol sz₂)

sizePredsAdd :: (MonadNIASize ref m) => [(Size ref, CmpOp, Size ref)] -> ContM_ m
sizePredsAdd = List.foldl (.) id . map sizePredAdd
                                                        
sizePredProve :: (MonadNIASize ref m) => (Size ref, CmpOp, Size ref) -> ContM m (Maybe Bool)
sizePredProve (sz₁, op, sz₂) = polPredProve (toPol sz₁, op, toPol sz₂) 

sizeFromType :: (Ord ref) => Type' name ref -> Maybe (Size ref)
sizeFromType (T (SizeT s)) = return s
sizeFromType (T (Var _ s)) = return (sizeSymb s)
sizeFromType _             = Nothing

