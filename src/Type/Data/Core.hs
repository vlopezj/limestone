module Type.Data.Core where

import Type.Core

newtype DataType name ref = DT (Type'' (DataType name ref) (Size ref)  name ref)

