{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
module Type.Symbol where

import Prelude hiding (mapM)
import MetaVar
import Id
import Data.Traversable
import Data.Foldable
import Control.Applicative
import Control.Monad.Reader hiding (mapM)
import Data.List
import GHC.Exts
import Binders

-- | Type for symbols
data Symbol ref = Meta (MetaVar ref)
                | TVar Bool ref
                | MetaSyntactic
                  Bool -- it's actual polarity
                  Bool -- it's printed polarity (for the negative type varible N)
                  Name -- ^ string name
                  -- 'meta' type (just for the paper, not found in actual code)
                  deriving (Show, Read, Functor, Eq, Traversable, Foldable, Ord)

symbRefs :: Symbol ref -> [ref]
symbRefs (TVar _ ref) = [ref]
symbRefs (Meta _)     = []
symbRefs (MetaSyntactic{}) = []

symbolVar = TVar True

idxSymbol :: forall name . Eq name => Symbol name -> ReaderT [name] (Either name) (Symbol Int)
idxSymbol s0 = go s0
  where
    go s0 = case s0 of
      Meta (MetaVar b u sc i) -> (\ sc' -> Meta (MetaVar b u sc' i)) <$> mapM lookup_tv sc
      TVar b tv               -> TVar b <$> lookup_tv tv
      MetaSyntactic b bs s    -> return $ MetaSyntactic b bs s

    
    lookup_tv :: name -> ReaderT [name] (Either name) Int
    lookup_tv x = do
        mi <- asks (elemIndex x)
        case mi of
            Just i  -> return i
            Nothing -> lift (Left x)

getMeta :: (Show ref) => Symbol ref -> Maybe (MetaVar ref)
getMeta s = case s of
  Meta m   -> Just m
  _        -> Nothing

var :: (EmbedSymbol t) => Ref t -> t
var = symb . symbolVar 

instance HasRef (Symbol ref) where
  type Ref (Symbol ref) = ref

instance IsString ref => IsString (Symbol ref) where
  fromString = symbolVar . fromString

fromStringEmbed :: (IsString (Ref t), EmbedSymbol t) => String -> t
fromStringEmbed = var . fromString

class HasRef t => EmbedSymbol t where
  symb :: Symbol (Ref t) -> t
 
instance EmbedSymbol (Symbol ref) where
  symb = id
