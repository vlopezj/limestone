{-# LANGUAGE ViewPatterns,MultiParamTypeClasses,GeneralizedNewtypeDeriving,
             OverloadedStrings,ScopedTypeVariables,TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module DsErrors where

import Data.Generics.Geniplate

import Prelude hiding (print)

import Text.PrettyPrint

import MetaVar

import Control.Monad.Error

import LL hiding (choice,Choice,int)

import Id

import PrettyId (pretty)

import qualified Parser.AbsMx as Abs
import Parser.AbsMx (Pat(..))

import Parser.PrintMx (Print,printTree)

import Text.Show.Pretty (ppDoc)

import qualified DsErrors.TH as TH

type Err = Doc

instance Error Doc where
    strMsg = text

print :: Print a => a -> Doc
print = text . printTree

getLoc :: [(Int,Int)] -> Maybe ((Int,Int),(Int,Int))
getLoc univ = case univ of
    [] -> Nothing
    xs -> Just (minimum xs,maximum xs)

metaInfoLoc :: MetaInfo -> Maybe ((Int,Int),(Int,Int))
metaInfoLoc info = case info of
    FromExpr e -> exprLoc e
    FromPat p  -> patLoc p
    FromType t -> typeLoc t
    NoInfo     -> Nothing

exprLoc :: Abs.Expr -> Maybe ((Int,Int),(Int,Int))
exprLoc e = getLoc (univ e)
  where
    univ :: Abs.Expr -> [(Int,Int)]
    univ = $(genUniverseBi 'TH.exprLocUniv)

typeLoc :: Abs.Type -> Maybe ((Int,Int),(Int,Int))
typeLoc e = getLoc (univ e)
  where
    univ :: Abs.Type -> [(Int,Int)]
    univ = $(genUniverseBi 'TH.typeLocUniv)

patLoc :: Abs.Pat -> Maybe ((Int,Int),(Int,Int))
patLoc e = getLoc (univ e)
  where
    univ :: Abs.Pat -> [(Int,Int)]
    univ = $(genUniverseBi 'TH.patLocUniv)

ppInfo :: MetaInfo -> Doc
ppInfo info = case info of
    FromExpr e -> "In expression:" <+> print e
    FromPat p  -> "In pattern:" <+> print p
    FromType t -> "In type:" <+> print t
    NoInfo     -> empty


msgTypeVariableEscapes     :: Id -> [Id] -> IdType -> MetaInfo -> Doc
msgTypeVariableEscapes tv scope ty info = loc (metaInfoLoc info) $
    hang ("Type variable" <+> pretty tv <+> "escapes") 2
    $  "in type:" <+> pretty ty
    $$ "scope:" <+> ppDoc scope
    $$ ppInfo info

msgOccursCheck             :: MetaVarRef -> IdType -> Doc
msgOccursCheck u t = hang ("Cannot construct infinite type:") 2
    (text (show u) <+> "=" <+> pretty t)

msgUnresolvedMeta          :: MetaVarRef -> MetaInfo -> Doc
msgUnresolvedMeta u info = loc (metaInfoLoc info) $
    hang ("Unresolved meta variable" <+> text (show u) <> ":") 2 (ppInfo info)

msgUnificationError        :: MetaInfo -> IdType -> IdType -> Doc
msgUnificationError info t0 t1 = loc (metaInfoLoc info) $
    hang "Unification error:" 2 $
    "Left type:" <+> pretty t0 $$
    "Right type:" <+> pretty t1 $$
    ppInfo info

msgSizeUnificationError        :: MetaInfo -> IdSize -> IdSize -> Doc
msgSizeUnificationError info t0 t1 = loc (metaInfoLoc info) $
    hang "Size unification error:" 2 $
    "Left size:" <+> pretty t0 $$
    "Right size:" <+> pretty t1 $$
    ppInfo info

msgSizeSolvingError        :: MetaVar Id -> String -> MetaInfo -> IdSize -> IdSize -> Doc
msgSizeSolvingError m extra info t0 t1 = loc (metaInfoLoc info) $
    hang ("Can't solve linearly for " <+> pretty m <> ":") 2 $
    "Reason:" <+> text extra $$
    "Left size:" <+> pretty t0 $$
    "Right size:" <+> pretty t1 $$
    ppInfo info

msgSizeMultipleVariablesError info t0 t1 = loc (metaInfoLoc info) $
    hang ("Multiple metavariables not supported:") 2 $
    "Left size:" <+> pretty t0 $$
    "Right size:" <+> pretty t1 $$
    ppInfo info


msgPatTypeError            :: Pat -> IdType -> IdType -> Doc
msgPatTypeError p t t' = loc (patLoc p) $
    "Expected type:" <+> pretty t $$
    "Actual type:" <+> pretty t' $$
    "In pattern:" <+> print p

msgUnboundIdentifier       :: Abs.Id -> Doc
msgUnboundIdentifier x = abs_id x <+> "is not bound"

msgUnboundTyVar            :: Abs.Id -> Doc
msgUnboundTyVar x = "Intuitionistic variable" <+> abs_id x <+> "is not bound"

msgTyVarOutOfScope         :: Id -> Doc
msgTyVarOutOfScope x = "Intuitionistic variable" <+> prettyId x <+> "is out of scope"

msgAlreadyBound            :: Abs.Id -> Id -> Doc
msgAlreadyBound x y = abs_id x <+> "is already bound at" <+> prettyId y

msgIdentifiersEscape       :: [Id] -> Doc
msgIdentifiersEscape [x] = prettyId x <+> "is never used"
msgIdentifiersEscape xs  = ppDoc xs <+> "are never used"

msgHole                    :: IdType -> [(Id,IdType)] -> [(Id,IdType)] -> Doc
msgHole goal ictx ctx =
    hang "Goal:" 4 (pretty goal) $$
    hang "Intuitionistic context:" 4 (prettyCtx ictx) $$
    hang "Linear context:" 4 (prettyCtx ctx)

msgCaseMismatch            :: Abs.Expr -> Doc
msgCaseMismatch e = loc (exprLoc e) $
    print e <+> "deconstructed, but the case arms do not use the same variables"

msgIncorrectlyAppliedAlias :: Abs.Id -> [Abs.Id] -> [IdType] -> Doc
msgIncorrectlyAppliedAlias n ns ts =
    hang (abs_id n <> colon <+> "incorrectly applied alias:") 4
        $  "defined with these variables:" <+> sep (pc (map abs_id ns))
        $$ "invoked with these arguments:" <+> sep (pc (map pretty ts))


loc :: Maybe ((Int,Int),(Int,Int)) -> Doc -> Doc
loc (Just (p1,p2))
    | p1 == p2  = hang (ppPos p1) 4
    | otherwise = hang (ppPos p1 <+> "-" <+> ppPos p2) 4
loc Nothing        = id

pc :: [Doc] -> [Doc]
pc = punctuate comma

abs_id :: Abs.Id -> Doc
abs_id (Abs.Id (p,n)) = text n <> ppPos p

ppPos :: (Int, Int) -> Doc
ppPos (y,x) = parens (int y <> comma <> int x)

prettyCtx :: [(Id,IdType)] -> Doc
prettyCtx ctx = vcat [ prettyId x <+> colon <+> pretty t | (x,t) <- ctx ]

prettyId :: Id -> Doc
prettyId (Id n _ (Just p)) = text n <> ppPos p
prettyId (Id n _ Nothing)  = text n

