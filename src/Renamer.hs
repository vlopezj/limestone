{-# LANGUAGE FlexibleContexts #-}
-- Copied from HipSpec
-- TODO: Make this an own library?
-- fi'e la danr
-- | Rename references in a traversable structure (e.g. a Type, or a Deriv)
--   consistently, according to a (long enough) list of suggestions for each name.
module Renamer where

import Control.Monad.State
import Control.Monad.Reader

import Data.Traversable (Traversable)
import qualified Data.Traversable as T
import Data.Bitraversable (Bitraversable)
import qualified Data.Bitraversable as B

import Data.Map (Map)
import qualified Data.Map as M
import Data.Set (Set)
import qualified Data.Set as S

import Control.Arrow

type RenameM a b = ReaderT (Suggestor a b) (State (Map a b,Set b))

type Suggestor a b = a -> [b]

runRenameM :: Ord b => Suggestor a b -> [b] -> RenameM a b r -> r
runRenameM f block m = evalState (runReaderT m f) (M.empty,S.fromList block)

insert :: (Ord a,Ord b) => a -> RenameM a b b
insert n = go =<< asks ($ n)
  where
    go (s:ss) = do
        u <- gets snd
        if s `S.member` u then go ss else do
            modify (M.insert n s *** S.insert s)
            return s
    go [] = error "ran out of names!?"

insertMany :: (Ord a,Ord b) => [a] -> RenameM a b [b]
insertMany = mapM insert

lkup :: (Ord a,Ord b) => a -> RenameM a b b
lkup n = do
    m_s <- gets (M.lookup n . fst)
    case m_s of
        Just s  -> return s
        Nothing -> insert n

rename :: (Ord a,Ord b,Traversable t) => t a -> RenameM a b (t b)
rename = T.mapM lkup

renameWith :: (Ord a,Ord b,Traversable t) => Suggestor a b -> t a -> t b
renameWith f = runRenameM f [] . rename

renameBi :: (Ord a,Ord b,Bitraversable t) => t a a -> RenameM a b (t b b)
renameBi = B.bimapM lkup lkup

renameBiWith :: (Ord a,Ord b,Bitraversable t) => Suggestor a b -> t a a -> t b b
renameBiWith f = runRenameM f [] . renameBi
