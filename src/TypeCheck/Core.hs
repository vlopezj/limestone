{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
module TypeCheck.Core where

import FileLocation
import Data.Default
import Data.Monoid
import Type
import Type.Ctx
import Control.Lens
import Control.Monad.Reader
import Control.Monad.Writer
import Control.Monad.Except.Extra
import Fresh
import Id
import Control.Applicative
import ContM

data StatusType a = OkType a | WrongType [Type' Name Name] | WrongSize (Size Name)

instance Default (TypeCheckEnv name ref) where
  def = TypeCheckEnv{
    _codeGen        = False
   ,_typeCheckCtx   = def
   ,_typeSizePreds  = def
  }

type TypeCheckCtx name ref = Type.Ctx.SeqCtx (name ::: Sized' name ref)
data TypeCheckEnv name ref = TypeCheckEnv
  { _codeGen            :: Bool -- ^ Whether the target of the sequent is code generation
  , _typeCheckCtx       :: SeqCtx (name ::: Sized' name ref)
  , _typeSizePreds      :: SizePredicates ref
  } deriving (Show)
$(makeLenses ''TypeCheckEnv)

intuit, linear :: forall name ref f. Functor f => 
  ([name ::: Sized' name ref] -> f [name ::: Sized' name ref]) ->
  (TypeCheckEnv name ref      -> f (TypeCheckEnv name ref)) 
intuit = typeCheckCtx . Type.Ctx.intuit
linear = typeCheckCtx . Type.Ctx.linear

type TypeError = String

type TypeCheckM name ref = TypeCheckEnvM name ref
type TypeCheckEnvM name ref = ReaderT (TypeCheckEnv name ref) FreshM
type TypeCheckLogM name ref = WriterT [TypeError] (ReaderT (TypeCheckEnv name ref) FreshM)
type TypeCheckErrM = ExceptT TypeError (TypeCheckLogM Id Id)

newtype WrapTypeCheckNIA m a = WrapTypeCheckNIA { unwrapTypeCheckNIA :: m a } deriving (Monad,Functor,Applicative)

instance MonadReader (TypeCheckEnv name ref) m => MonadReader (SizePredicates ref) (WrapTypeCheckNIA m) where
  ask = WrapTypeCheckNIA $ view typeSizePreds
  local f = WrapTypeCheckNIA . local (typeSizePreds %~ f) . unwrapTypeCheckNIA

liftTypeCheckNIA :: (Applicative n, Ord ref, Show ref, MonadReader (TypeCheckEnv name ref) n) =>
                    (forall m . MonadNIASize ref m => ContMR m a c) -> ContMR n a c 
liftTypeCheckNIA k = unwrapTypeCheckNIA . k . (WrapTypeCheckNIA .)
 
liftTypeCheckNIA_ :: (Applicative n, Ord ref, Show ref, MonadReader (TypeCheckEnv name ref) n) =>
                     (forall m . MonadNIASize ref m => ContM_' m c) -> ContM_' n c 
liftTypeCheckNIA_ k = unwrapTypeCheckNIA . k . WrapTypeCheckNIA

-- wrongType = [e| WrongType $(do
--                                loc <- qLocation
--                                let loc_str = locationToString loc
--                                [| loc_str |]) |]
