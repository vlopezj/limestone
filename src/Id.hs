{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE FlexibleContexts #-}
module Id where

import Data.Function (on)
import Data.Char
import Numeric
import Data.Maybe
import Data.List
import Language.Haskell.TH hiding (Name)
import GHC.Exts
import Tagged
import Control.Monad.State

import Development.Placeholders

import qualified Data.Map as M
import Data.Map (Map)

type Name = String

newtype Unique = Unique { getUnique :: Int } deriving (Eq,Ord,Enum,Show,Read)

$(let
     alpha = map intToDigit [0..9] ++ ['a'..'z'] ++ ['A'..'Z']
     n = fromIntegral $ length alpha
  in 
   return
     [ValD (VarP (mkName "uniqueAlpha"))  (NormalB (LitE (StringL alpha))) [],
      ValD (VarP (mkName "uniqueAlphaN")) (NormalB (LitE (IntegerL n))) []])

uniqueToString (Unique (-1)) = ""
uniqueToString (Unique i) = showIntAtBase uniqueAlphaN at i ""
      where
        at = (uniqueAlpha !!)

idToString :: Id -> String
idToString Id{..} = 
  let eid_name = id_name >>= \c ->
        case c of
          '_'  -> '_':'_':[]
          c    -> return c
  in
  eid_name ++ case uniqueToString id_unique of
                ""  -> ""
                str -> "_" ++ str

idFromString :: String -> Maybe Id
idFromString = $(todo "Implement idFromString.")

data Id = Id
    { id_name   :: Name 
    , id_unique :: Unique
    , id_m_loc  :: Maybe (Int,Int)
    -- ^ Maybe a source location this identifier appeared at
    }

class HasId a where
  toId :: a -> Id 

instance HasId Id where
  toId = id

instance HasId (Id ::: a) where
  toId = erase

-- ^ Show will be valid haskell when overloaded strings is enabled.
--   (except for the source location, can be added as a record update)
instance (Show Id) where
  -- Show as string, which could potentially be valid Haskell syntax for an Id
  show = (\str -> ['"'] ++ str ++ ['"']) .
         (>>= \c -> case c of
             '\\' -> '\\':'\\':[]
             '"'  -> '\\':'"':[]
             _    -> [c]) .
             idToString

{-instance (Read Id) where
  readsPrec d buf = do
    (a, rest) <- readsPrec d buf
    a' <- maybeToList $ idFromString a
    return (a', rest) -}
{-     
instance IsString Id where
  fromString = fromJust . idFromString
-}

mkId :: Name -> Unique -> Id
mkId n u = Id n u Nothing

unsafeId :: Name -> Id
unsafeId n = Id n (Unique (error "unsafeId")) Nothing

unsafestId :: Name -> Id
unsafestId n = Id n (Unique (-1)) Nothing

instance Eq (Id) where
    (==) = (==) `on` id_unique

instance Ord (Id) where
    compare = compare `on` id_unique

idFromName ::  Unique -> Name -> Id
idFromName u n = Id n u Nothing

idFromTag ::  Unique -> TagId -> Id
idFromTag u (Tag (Id n _ mp)) = Id n u mp

newtype TagId = Tag { unTag :: Id } deriving Show

instance Eq TagId where
    Tag (Id n1 u1 _) == Tag (Id n2 u2 _) = u1 == u2 && n1 == n2

instance Ord TagId where
    Tag (Id n1 u1 _) `compare` Tag (Id n2 u2 _) = case u1 `compare` u2 of
        EQ -> n1 `compare` n2
        gl -> gl
        

disambiguateId :: Id -> State (Map Name (Map Unique (Maybe Int))) Id
disambiguateId x@Id{ id_name, id_unique } = do
    n <- gets (M.lookup id_name)
    j <- case n of
            Nothing -> do
                let i = Nothing
                modify (M.insert id_name (M.singleton id_unique i))
                return i
            Just m' -> do
                case M.lookup id_unique m' of
                    Nothing -> do
                        let i = Just$ 1 + maximum (0:(catMaybes (M.elems m')))
                        modify (M.adjust (M.insert id_unique i) id_name)
                        return i
                    Just i -> return i
    return x { id_name = id_name ++ maybe "" subscriptShow j }

scriptShow ::  (Integral a, Show a) => [Char] -> a -> [Char]
scriptShow []             _ = error "scriptShow on empty list"
scriptShow (minus:digits) x = if x < 0 then minus : sho (negate x) else sho x
  where sho z = showIntAtBase 10 (\i -> digits !! i) z []

subscriptShow :: Int -> String
subscriptShow  = scriptShow "-₀₁₂₃₄₅₆₇₈₉"

ignoreName = "???\\_"

disambiguateName_ :: (MonadState (Map Name Int) m) => Name → m Name
disambiguateName_ x | x == ignoreName = pure x
disambiguateName_ x | x == "_"        = pure x
disambiguateName_ x                   = disambiguateName x 

disambiguateName :: (MonadState (Map Name Int) m) => Name → m Name
disambiguateName s = 
  gets (M.lookup s) >>= \case
        Nothing → do
            modify (M.insert s 0)
            return s
        Just i → do
            modify (M.insert s (i + 1))
            return$ s ++ subscriptShow i
