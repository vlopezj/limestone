{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PatternGuards #-}
module Polarize where

import LL
import LL.Traversal
import Fresh
import Control.Lens
import Control.Monad.Writer hiding ((<>))
import qualified Data.MultiMap as MM
import Data.MultiMap(MultiMap)
import Data.Semigroup
import Data.Generics.Geniplate
import Data.Default
import Data.List
import Data.Maybe
import Data.Generics.Is
import Tagged
import Control.Applicative
import Control.Monad.Maybe.Extra
import Control.Monad.Except.Extra
import Control.Monad.State
import Ritchie.Polar.Type

data PolConstraint' b = AtMostOneNeg [PolVar' b] deriving (Ord,Show,Eq)
type PolConstraint = PolConstraint' Id

data Solver p a = Solver {
  -- Result, not used during the algorithm
  _know         :: [(p,Bool)]
 ,_used         :: [a] 

 -- Algorithm state
 ,_clauses      :: [PolConstraint' p ::: a]  -- ^ Tagged with some data
 }

$(makeLenses ''Solver)  

paramPolSeqn :: (MonadFresh m,
                 ty ~ Type' name ref,
                 tysz ~ Sized' name ref)
                => (ann -> m bnn)
                -> Seqn ann ty sz tysz name ref
                -> m (Seqn bnn ty sz tysz name ref)
paramPolSeqn f = foldSeqn defSeqnFold{sseq = sseq
                                     ,sty = sty
                                     ,stysz = stysz} 
  where
    sseq ann seq = do ann' <- f ann
                      return $ Seqn ann' seq
    sty _ = paramPolType
    stysz _ = paramPolSized

paramPolSized :: (MonadFresh m) => Sized' name ref -> m (Sized' name ref)
paramPolSized (ty :^ sz) = do ty' <- paramPolType ty
                              return $ ty' :^ sz


paramPolDeriv ::
  (MonadFresh m,
   ty ~ Type' name ref,
   tysz ~ Sized' name ref)
  => (seq -> m seq) -> Deriv'' seq name ref -> m (Deriv'' seq name ref)
paramPolDeriv f d = d &   derivCtx     %%~ paramPolCtx
                      >>= derivSequent %%~ f

paramPolSeqn1 :: (MonadFresh m) => Seqn1 name ref -> m (Seqn1 name ref)
paramPolSeqn1 = _Wrapping Seqn1 %%~ paramPolSeqn return


paramPolD1 :: (MonadFresh m) => Deriv1 name ref -> m (Deriv1 name ref)
paramPolD1 = _Wrapping D1 %%~ paramPolDeriv paramPolSeqn1

paramPolCtx :: (MonadFresh m) => TypedCtx name ref -> m (TypedCtx name ref)
paramPolCtx c = c &   linear . traverse . tag %%~ paramPolSized
                  >>= intuit . traverse . tag %%~ paramPolSized

-- This problem is 2-SAT; can be solved optimally in polynomial time.
atMostOneNeg = AtMostOneNeg
allPositive = atMostOneNeg . (PolConstNeg:)

atMostOneSeqNeg :: (Ord ref) => [a ::: Sized' name ref] -> [PolConstraint]
atMostOneSeqNeg refs =
  let seqs = [ (v,sizeIsInteger sz) | _ ::: T (Array (ANeutral v) _ _) :^ sz <- refs]
      (sz0,seqs') = partition $(isP [p| (_,Just 0) |]) seqs 
      (sz1,szN  ) = partition $(isP [p| (_,Just 1) |]) seqs'
  in
  [atMostOneNeg [v | (v,_) <- sz1]
  ,allPositive  [v | (v,_) <- szN]
  ]


polarizeDeriv :: (MonadError String m, Ord ref) => Deriv2 name ref -> m (DerivPol name ref)
polarizeDeriv d = do
  let cs = polConstraintsDeriv d
  case solvePolConstraints cs of
    Left cs'   -> throwError $ "Can't polarize derivation. Relevant constraints: " ++ show cs
    Right know -> return$ saDeriv$ substPolVarAllDeriv2 know d

polarizeDerivUnsafe :: (Ord ref) => Deriv2 name ref -> DerivPol name ref
polarizeDerivUnsafe = unsafeExcept . polarizeDeriv

polConstraintsDeriv :: (Ord ref) => Deriv2 name ref -> [PolConstraint]
polConstraintsDeriv = polConstraints . view (_Wrapping D2 . derivSequent)

polConstraints :: (Ord ref) => Seqn2 name ref -> [PolConstraint]
polConstraints (Seqn2 seq) = execWriter $ foldSeqn defSeqnFold{sseq} seq
  where
    sseq _ t = case t of
        Ax{..} ->
          zipWithTypeM match (dropSize $ zElim ^. tag) (dropSize $ wElim ^. tag)
          where
            match (Array (ANeutral p₁) _ _) (Array (ANeutral p₂) _ _) = tell [atMostOneNeg [p₁,p₂]]
            match _ _ = return ()                                                                                  

        BigSeq steps -> tell $ atMostOneSeqNeg (bigSeqRefs steps)
        What{..}     -> tell $ atMostOneSeqNeg [v | ElemLinear v <- zVars] 
        _ -> pure () 

substPolVar :: (Eq p) => [(p,Bool)] -> PolVar' p -> PolVar' p
substPolVar ps (PolVar s p') | Just b <- lookup p' ps = PolConst $ if s then b else not b
substPolVar ps v = v

-- | Substitute all variables, else use a default value
substPolVarAll :: (Eq p) => [(p,Bool)] -> PolVar' p -> PolVar' q
substPolVarAll ps (PolVar s p') | Just b <- lookup p' ps = PolConst$ if s then b else not b
substPolVarAll ps (PolVar s _) = PolConst $ if s then True else False
substPolVarAll ps (PolConst b) = PolConst b

substPolVarAllType :: [(Id,Bool)] -> Type' name ref -> Type' name ref
substPolVarAllType = flip $ transformBiM (flip substPolVarAll)

substPolVarAllDeriv2 :: forall name ref. [(Id,Bool)] -> Deriv2 name ref -> Deriv2 name ref
substPolVarAllDeriv2 = flip $ transformBiM $ flip f
                       where f :: [(Id,Bool)] -> Type' name ref -> Type' name ref
                             f = substPolVarAllType
  

instance Default (Solver p a) where
  def = Solver [] [] []


newtype SolverM p b a = SolverM { runSolverM :: MaybeT (State (Solver p b)) a }
                  deriving (Functor,Applicative,Monad)
deriving instance MonadState (Solver p b) (SolverM p b)

instance (Eq p, Eq b) => Alternative (SolverM p b) where
  empty = mzero
  (<|>) = mplus

instance (Eq p, Eq b) => MonadPlus (SolverM p b) where
  mzero         = SolverM (MaybeT (StateT (\s -> Identity (Nothing, s)))) 
  mplus (SolverM (MaybeT (StateT ((runIdentity .) -> f₁))))
        (SolverM (MaybeT (StateT ((runIdentity .) -> f₂)))) =
    SolverM$ MaybeT$ StateT$ \s -> Identity $
      case f₁ s of
        r₁@(Just _, _) -> r₁
        (Nothing, s₁) -> case f₂ s of
          r₂@(Just _, _) -> r₂
          (Nothing, s₂) -> (Nothing, s₁ <> s₂)

-- Combining error states
instance (Eq p, Eq a) => Semigroup (Solver p a) where
  x <> y = Solver {
    _know = (x ^. know) `intersect` (y ^. know)
   ,_used = (x ^. used) `union` (y ^. used)
   ,_clauses = [] 
   }
    
solvePolConstraints :: forall p. Ord p => [PolConstraint' p] -> Either [PolConstraint' p] [(p,Bool)]
solvePolConstraints ps =
  let (res, s) = flip runState def{_clauses = [p ::: p | p <- ps]} . runMaybeT . runSolverM $ go [] in
  case res of
    Just k  -> Right k
    Nothing -> Left (s ^. used)

  where
    -- Simplification fixpoint; idempotent as long as it starts with a cleaned-up set of clauses
    go₁ :: Eq b => SolverM p b Bool
    go₁ = do
      know₁ <- whileTrue $ use clauses >>= fmap concat . mapM unitPropagation >>= add_knowledge
      know₂ <- whileTrue $ use clauses >>= pureLiterals >>= add_knowledge
      return (know₁ || know₂)

    whileTrue :: (Monad m) => m Bool -> m Bool
    whileTrue m = do
      r <- m
      if r then m >> return True 
           else return False

    go :: Eq b => [(p,Bool)]  -- ^ initial knowledge (can be empty)
       -> SolverM p b [(p,Bool)]
    go vs = do
      add_knowledge vs >> whileTrue go₁
      v <- select_variable
      case v of
        Nothing -> use know -- finish search
        Just v  -> go [v] `mplus` go [second dual v]

    all_variables :: PolConstraint' p -> [PolVar' p]
    all_variables (AtMostOneNeg vs) = vs

    select_variable :: SolverM p a (Maybe (p,Bool))
    select_variable = do
     cs <- use clauses
     return $ listToMaybe [(p, s) | c ::: _ <- cs, PolVar s p <- all_variables c ]

    -- 1. Simplify constraints
    -- Idempotent on each variable
    substitute :: [(p,Bool)] -> PolConstraint' p -> PolConstraint' p
    substitute know (AtMostOneNeg l) = AtMostOneNeg $ [substPolVar know v | v <- l]

    -- Idempotent 
    simplify :: PolConstraint' p -> PolConstraint' p
    simplify (AtMostOneNeg ps) = AtMostOneNeg $ filter $(isNotP [p| PolConstPos |]) ps

    -- if q' ⊆ p, then:
    -- add_knowledge p >> add_knowledge q == add_knowledge p >> return False
    add_knowledge :: [(p,Bool)] -> SolverM p a Bool
    add_knowledge ks = do
      clauses . traverse . value %= substitute ks
      clauses . traverse . value %= simplify
      clauses %= filter (not . isTrivial . view value)
      know %= (ks ++)
      return $ $(isNot '[]) ks

    -- Remove trivial constraints (these are true regardless of the values of the variables)
    isTrivial = (||) <$> $(isP [p| AtMostOneNeg []  |]) 
                     <*> $(isP [p| AtMostOneNeg [_] |])

    -- Unit propagation
    unitPropagation :: (Eq a) => (PolConstraint' p ::: a) -> SolverM p a [(p,Bool)]
    unitPropagation (AtMostOneNeg ps ::: a) = do
      case partition $(isP [p| PolConstNeg |]) ps of
        ([],     _) -> return []
        ([_], rest) -> used %= (a:) >> return [(p, positiveVar) | PolVar positiveVar p <- rest] 
        (_:_:_,  _) -> used %= (a:) >> mzero -- Contradiction

    -- Pure literals
    -- Variables that never or always appear negated
    pureLiterals :: [PolConstraint' p ::: a] -> SolverM p a [(p,Bool)]
    pureLiterals cs = pure $ join 
     [ case nub bs of
         [b]   -> [(p, b)]
         [_,_] -> []
     | (p, bs) <- MM.assocs $ MM.fromList $
                  [(p,s) | AtMostOneNeg ps ::: a <- cs
                         , PolVar s p <- ps] ]

instance Erase (DerivPol name ref) (Deriv2 name ref) where
  erase = D2 . (derivSequent %~ erase) . derivPol

instance Erase (SeqnPol name ref) (Seqn2 name ref) where
  erase (SeqnPol Seqn{_ann,_seq}) =
    Seqn2 (Seqn _ann
                (fmapSeq'' defSeqFmap{fseq,fname,fref,fty,ftysz} _seq))
    where
      fseq  = seqn2 . erase . SeqnPol
      fty   (TyA (TyPol _ ty)) = ty
      ftysz (TyA (TyPol _ tysz)) = tysz
      fname (SAn _ (x ::: ty)) = x ::: ftysz (TyA ty)
      fref  (SAr _ (x ::: ty)) = x ::: ftysz (TyA ty)



