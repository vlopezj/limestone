{-# OPTIONS_GHC -XTypeFamilies -XTypeSynonymInstances -XOverloadedStrings #-}
-- | TeX combinators for general document structure
module Framework where

import MarXup()
import MarXup.Latex
import MarXup.Latex.Math
import MarXup.Tex
import Data.Monoid
import Data.List (intersperse)
import Control.Monad
import Data.String

return' :: a -> Tex a
return' = return

----------
-- Text

anon :: TeX -> TeX
anon x = "(Anonymised) "

specTab ::  [TeX] -> Tex ()
specTab x = env "tabularx" $ do
    braces $ tex "\\textwidth"
    braces $ tex ">{\\centering}X"
    mconcat $ intersperse (newline <> vspace "1ex") x

inMargin = cmd "marginpar"
-- todo :: TeX -> TeX
-- todo x = inMargin $ "(todo: " <> x <> ")"
todo = cmd "todo"

todo' :: TeX -> TeX -> TeX
todo' who x = emph $ "(todo by "<> who <> ": " <> x <> ")"

{-
-- acknowledgements :: ClassFile -> Tex a -> Tex a
acknowledgements SIGPlan body = cmd "acks" body
acknowledgements _ body = paragraph "Acknowlegdements" >> body
-}

kern :: String -> TeX
kern x = braces $ tex $ "\\kern " ++ x

comment :: Tex a -> TeX
comment _ = ""

nocomment :: Tex a -> Tex a
nocomment x = x

clearpage ::  Tex ()
clearpage = cmd0 "clearpage"

verbatim ::  TeX -> Tex ()
verbatim body = env "verbatim" $ body

verb :: String -> Tex ()
verb body
    | '~' `elem` body = error $ "Cannot delimit " ++ show body ++ " with tilde! :'("
    | otherwise       = tex $ "\\verb~" ++ body ++ "~"

noindent :: () -> Tex ()
noindent _ = cmd0 "noindent"

sideways :: TeX -> TeX -> TeX
sideways caption body = env "sidewaysfigure" $ do
  body
  cmd "caption" caption

footnote :: Tex a -> Tex a
footnote = cmd "footnote"

classFile ::  IsString a => ClassFile -> a
classFile EPTCS = "../Tools/latex/eptcs"
classFile IEEE = "../Tools/latex/IEEEtran"
classFile LNCS = "../Tools/latex/llncs"
classFile SIGPlan = "../Tools/latex/sigplanconf"
classFile EasyChair = "../Tools/latex/easychair"
classFile Beamer = "beamer"

texDef nm = cmd ("def\\" ++ nm)

medSpace = tex "\\:"


localStdPreamble :: ClassFile -> Tex ()
localStdPreamble classUsed = localStdPreamble' classUsed [] 

localStdPreamble' :: ClassFile -> [String] -> Tex ()
localStdPreamble' classUsed opts = do
  documentClass (classFile classUsed) ([] ++  ["preprint,compact" | classUsed == SIGPlan]
                                       ++ opts)
  -- when (inMetaPost && classUsed == Beamer) $
  --   tex "\\renewcommand{\\familydefault}{\\sfdefault}"
  stdPreamble
  usepackage "cmll" [] -- for the operator "par"
  mathpreamble
  usepackage "todonotes" []
  if classUsed == Beamer
    then cmd "input" (tex "../Paper/Tools/latex/unicodedefs")
    else cmd "input" (tex "../Tools/latex/unicodedefs")
  do
    usepackage "natbib" ["sectionbib" | classUsed == LNCS]
    usepackage "rotating" [] -- for sideways
    usepackage "tikz" []
    cmd "usetikzlibrary" $ tex "shapes,arrows"
    usepackage "tabularx" []
    usepackage "microtype" []

--------------
-- Math

-- Envs

newtheorem :: String -> TeX -> TeX
newtheorem ident txt = cmd "newtheorem" (tex ident) >> braces txt

dm ::  Tex a -> Tex a
dm = displayMath

space ::  TeX
space = tex "\\:"

mkIf ::  [Char] -> TeX
mkIf str = tex "\\newif" <> tex ("\\if" ++ str)

--------------------

