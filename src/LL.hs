{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TransformListComp #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE InstanceSigs #-}
module LL (module LL) where

import Type as LL hiding ((&))
import Type.Data as LL
import LL.Core as LL
import LL.Traversal
import LL.Traversal as LL(foldSeqn, traverseSeq'', fmapSeq'', fmapSeqStep' 
                         ,defSeqFmap, defSeqnFold, defSeqTraverse)
import Type.Ctx as LL
import Op as LL

import Id as LL (Name,Id)
import MetaVar
import Data.List ((\\),partition,inits,tails)
import Data.Maybe (fromMaybe)
import Fresh
import Control.Monad.State
import Control.Monad.Except.Extra
import Control.Applicative
import Data.Traversable as T
import Tagged
import qualified Data.Foldable as T

import Data.Semigroup
import Data.List
import Data.Generics.Geniplate
import Data.Generics.Genifunctors
import Data.Bitraversable
import Data.Bifoldable
import Data.Bifunctor
import qualified Data.Bifunctor as LL (second)
import Control.Arrow hiding (second)
import Text.Show.Pretty (ppShow)

import GHC.Exts

import Id
import qualified Language.Haskell.TH as TH
import Language.Haskell.TH hiding (Name)
import Data.Functor.Identity

import Development.Placeholders

tySubstSeq :: (MonadFresh m) => Id -> IdType -> IdSeq -> m IdSeq
tySubstSeq i t = tySubstsSeq [(i,t)]

tySubstsSeq :: forall m. (MonadFresh m) => [(Id,IdType)] -> IdSeq -> m IdSeq
tySubstsSeq su = transformBiM $ \ t0 -> fmap normType $ tySubstsCore su t0

tySubstsSeq2 :: forall m. (MonadFresh m) => [(Id,IdType)] -> IdSeq2 -> m IdSeq2
tySubstsSeq2 su = transformBiM $ \ t0 -> fmap normType $ tySubstsCore su t0

{-
case t0 of
    Var (TVar pos i) | Just t <- lookup i su -> (if pos then id else neg) <$> refreshType t
    _                                        -> return t0
-}

tyVarSubst :: Id -> Id -> IdSeq -> IdSeq
tyVarSubst r0 r1 = bimap id (\r -> if r == r0 then r1 else r)

-- | Refreshes all identifiers in a derivation
refreshDeriv :: Deriv' Id Id -> FreshM (Deriv' Id Id)
refreshDeriv = refreshAll

-- | Refreshes the /binders/ in a sequent
refreshBindersSeq :: (Functor m, MonadFresh m) => IdSeq -> m IdSeq
refreshBindersSeq = refreshBinders

-- | Refreshes all identifiers in a derivation
refreshDeriv2 :: (MonadFresh m) => Deriv2 Id Id -> m (Deriv2 Id Id)
refreshDeriv2 = refreshAll

-- | Renaming of references: rename x y s means s[y/x]
rename :: (Functor f,Eq ref) => ref -> ref -> f ref -> f ref
rename x y = renameList [(x,y)]

-- | Renaming of many references
renameList :: (Functor f,Eq ref) => [(ref,ref)] -> f ref -> f ref
renameList s = fmap $ \ x -> fromMaybe x (lookup x s)

-- | The free variables in a sequent
freeVars :: (Bifoldable t,Eq a) => t a a -> [a]
freeVars s = nub (refs s) \\ nub (names s)

-- | Is this reference used in this sequent?
referencedIn,usedIn :: (Eq ref,Bifoldable t) => ref -> t name ref -> Bool
referencedIn i s = i `elem` refs s
usedIn = referencedIn

ignoreAll :: Boson -> [ref] -> Seq' name ref -> Seq' name ref
ignoreAll β xs (S s) = S $ foldr ((S'.) . Ignore β) s xs

aliasAll :: Boson -> [(ref,name)] -> Seq' name ref -> Seq' name ref
aliasAll β xs (S s) = S $ foldr ((S'.) . uncurry (Alias β)) s xs

isBoson :: Seq' name ref -> Boson
isBoson (S0 (Ignore β _ _)) = β
isBoson _                   = False -- TODO

--------------
-- Extraction
-- rightChild,leftChild :: Deriv -> Deriv
-- leftChild  (Deriv who ts vs (Cut w _ ty a _)) = Deriv ("leftChild of " ++ who) ts ((w,neg ty):take x vs) a
-- leftChild  _                                = error "leftChild: not a cut!"
-- rightChild (Deriv who ts vs (Cut _ w ty _ a)) = Deriv ("rightChild of " ++ who) ts ((w,    ty):drop x vs) a
-- rightChild  _                               = error "rightChild: not a cut!"

----------------------------------

seqIdRefsUsed :: IdSeq -> [Id]
seqIdRefsUsed s = nub (freeVars s `intersect` f s) \\ exceptions
  where
    f = $(genUniverseBiT' [   [t| Size Id |]
                           , [t| MetaVar Id |]
                           , [t| Type' Id Id |] ] [t| IdSeq -> [Id] |])

    exceptions = spuriousSizeVars
    spuriousSizeVars = [fmSz | (S0 Foldmap{..}) <- seqSeqs s]

seqSeqs :: forall name ref. Seq' name ref -> [Seq' name ref]
seqSeqs = $(genUniverseBi' [t| Seq' name ref -> [Seq' name ref] |])
 
prune0 = prune . _seq . seqn0 
prune :: Seq'' seq ty sz tysz name ref -> Seq'' (:…) ty sz tysz name ref
prune = fmapSeq'' defSeqFmap{fseq = const (:…)} 
                                           
pruneStep :: SeqStep' seq sz name ref -> SeqStep' (:…) sz name ref
pruneStep = fmapSeqStep' defSeqFmap{fseq = const (:…)}


seqSizes :: Seq' name ref  -> [Size ref]
seqSizes = $(genUniverseBi' [t| forall name ref. Seq' name ref  -> [Size ref] |])

seqStepSizes :: SeqStep name ref -> [Size ref]
seqStepSizes = $(genUniverseBi' [t| forall name ref. SeqStep name ref -> [Size ref] |])

seqSizeRefs :: (Ord ref) => Seq' name ref -> [ref]
seqSizeRefs = seqSizes >=> szRefs

seqStepSizeRefs :: (Ord ref) => SeqStep name ref -> [ref]
seqStepSizeRefs = seqStepSizes >=> szRefs

seqPole refs = S0 $ SPole refs

freezeSeq1 cnt sz ty x y (S r) (S s) = S0 $ Sync { syncIntr = [(sz,ty,x, Nothing,  Just $ SyncRead { syncCopies = cnt, syncYIntr = y })]
                                                 , contL = r
                                                 , syncCont = Nothing
                                                 , syncContR = Just s
                                                 }
instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Deriv1 name ref) where
  transformBiM = transformBiMTypeDeriv

instance (Applicative m, Monad m) => TransformBiM m (Type' name ref) (Deriv2 name ref) where
  transformBiM = transformBiMTypeDeriv

instance (Monad m) => TransformBiM m IdSeq IdSeq where
    transformBiM :: (IdSeq -> m IdSeq) -> IdSeq -> m IdSeq
    transformBiM f = $(genTransformBiM' [t| Monad m => (IdSeqn0' -> m IdSeqn0') -> IdSeq -> m IdSeq |])
           (liftM seqn0 . f . Seqn0)

