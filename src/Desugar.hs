{-# LANGUAGE ViewPatterns,MultiParamTypeClasses,GeneralizedNewtypeDeriving, TupleSections,
             OverloadedStrings,ScopedTypeVariables,PatternGuards,CPP,
             PatternSynonyms #-}
#define REPORT report (__FILE__ ++ ":" ++ show (__LINE__ :: Integer))
module Desugar (desugar) where

import Text.PrettyPrint hiding (int)

import qualified Data.Map as M

import Control.Applicative hiding (empty)
import Control.Monad
import Control.Monad.Reader
import Control.Monad.State
import Control.Arrow (second)

import MetaVar

import PrettyId

import Data.Maybe (catMaybes)

import LL hiding (choice,Choice,second,trType)
import LL.Traversal

import Parser.PrintMx (printTree)

-- import AbsMxUtils

import Id

import DsErrors

import qualified Parser.AbsMx as Abs
import Parser.AbsMx (Prog(..),Pat(..),MultiPat(..),ManyPat(..),LamPat(..))

import DsMonad
import Unify

-- import Text.Show.Pretty (ppDoc)

mkTyHole :: Abs.Id -> Abs.Type
mkTyHole (Abs.Id (p,s)) = Abs.TyHole (Abs.Hole (p,s))

desugar :: Prog -> (Either Err [IdDeriv],String)
desugar (Prog (map unTopDecl -> decls))
    = runDsM M.empty (insertAliases decls $ catMaybes <$> mapM dsDecl decls)

type AbsCtxEntry = (Abs.Id,Abs.Type)
type AbsCtx = [AbsCtxEntry]

getCtxs :: Abs.Ctxs -> (AbsCtx,AbsCtx)
getCtxs (Abs.BothCtxs is ls) = (map unBinder is,map unBinder ls)
getCtxs (Abs.OnlyLinear ls)  = ([],map unBinder ls)

unBinder :: Abs.Binder -> AbsCtxEntry
unBinder (Abs.TypedBinder i t) = (i,t)
unBinder (Abs.Binder i)        = (i,mkTyHole i)

insertIntCtx :: AbsCtx -> DsM a -> DsM a
insertIntCtx []          m = m
insertIntCtx ((x,t0):xs) m = do
    t <- trType t0
    insertIVar x t $ \ _ -> insertIntCtx xs m

dsDecl :: Abs.Decl -> DsM (Maybe IdDeriv)
dsDecl Abs.AliasDecl{} = return Nothing
dsDecl (Abs.DerivDecl init_ctxs s) = cleanly $ do

    let (int_ctx,lin_ctx) = getCtxs init_ctxs

    deriv <- insertIntCtx int_ctx $ do

        int_ctx' <- gets (map snd . st_iscope)

        lin_ctx' <- forM lin_ctx $ \ (x,t) -> do
            t' <- trType t
            x' <- insert x t'
            return (x',t' :^ sizeOne)
        d <- D0 . Deriv "" (seqctx (map (second (:^ sizeOne)) int_ctx') lin_ctx') <$> dsSeq s

        resolveMetas d

    assertEmptyScope

    return (Just deriv)


namePat :: Pat -> Name
namePat p0 = case p0 of
    PVar x      -> name x
    PPair px py -> namePat px ++ namePat py
    PUnit       -> "u"
    PUnpack a p -> name a ++ "_" ++ namePat p
    PBuild p    -> "bld_" ++ namePat p
    PUnfold p   -> "unf" ++ namePat p
    PSig p _    -> namePat p
    PRight p    -> namePat p ++ "_r"
    PLeft p     -> namePat p ++ "_l"
    PMany i     -> name i ++ "_many"

dsPat :: IdType -> Pat -> DsM (Id,DsM IdSeq -> DsM IdSeq)
dsPat t p = case p of
    PVar x -> do
        x' <- insert x t
        return (x',id)

    PMany i -> do

        [mv] <- newMetas 1 (FromPat p)
        unify (FromPat p) (T (Bang mv)) t

        z <- makeTmp (namePat p)

        return
            ( z
            , \ s -> do
                i_id <- makeId i
                reallyInsertIVar (Just (name i)) i_id mv $ do
                    s0$ Save z i_id <$> rM s
            )

    PPair px py -> do
        [tx,ty] <- newMetas 2 (FromPat p)
        (x,cx) <- dsPat tx px
        (y,cy) <- dsPat ty py
        unify (FromPat p) (T (tx :*: ty)) t
        z <- makeTmp (namePat p)
        return (z,\ s -> s0$ Cross false z x y <$> rM (cx (cy s)))
    PUnit -> do
        unify (FromPat p) (T One) t
        z <- makeTmp (namePat p)
        return (z,\m -> s0$ SOne false z <$> rM m)
    PBuild p' -> do
        z <- makeTmp (namePat p)
        a <- makeTmp "c"
        f <- addTyVar a $ newMeta (FromPat p)
        unify (FromPat p) t (T (Rec Mu a f))
        (x,cx) <- dsPat (foldTy' a f) p'
        return (z,\ s -> s0$ Fold z x <$> rM (cx s))
    PUnfold _p -> error "Desugar: PUnfold TODO"

    PUnpack a p' -> do
        z <- makeTmp (namePat p)
        ma <- newMeta (FromPat p)
        (tv,mv) <- insertIVar' a ma $ \ _ -> newMeta (FromPat p)
        unify (FromPat p) t (T (Exists tv ma mv))
        (x,cx) <- reinsertIVar a tv ma $ dsPat mv p'
        return (z,\ s -> s0$ reinsertIVar a tv ma $ TUnpack z (T TType) tv x <$> rM (cx s))

    PSig p' tp -> do
        tp' <- trType tp
        unify (FromPat p) t tp'
        dsPat t p'

    PLeft p' -> do
        [ta,tb] <- newMetas 2 (FromPat p)
        z <- makeTmp (namePat p)
        unify (FromPat p) t (T (ta :&: tb))
        (x,cx) <- dsPat ta p'
        return (z,\ s -> s0$ With false z LL.Inl x <$> rM (cx s))

    PRight p' -> do
        [ta,tb] <- newMetas 2 (FromPat p)
        z <- makeTmp (namePat p)
        unify (FromPat p) t (T (ta :&: tb))
        (x,cx) <- dsPat tb p'
        return (z,\ s -> s0$ With false z LL.Inr x <$> rM (cx s))

    {-
  where
    rep :: DsM (Id,IdSeq -> DsM IdSeq) -> DsM (Id,IdSeq -> DsM IdSeq)
    rep m = do
        (i,k) <- m
        report' $ \ pretty -> hang (text (id_name i) <+> "=" <+> text (printTree p) <+> ":") 4 (pretty t)
        return (i,k)
        -}

dsPat' :: IdType -> Pat -> Id -> DsM IdSeq -> DsM IdSeq
dsPat' t p y m = do
    (x,cx) <- dsPat t p
    [x'] <- makeTmps 1
    cut1ds x' x t (S0$ Ax y x') <$> cx m

-- Construct this type by telling me how to eliminate its dual
-- The result is consumed by the last argument
construct :: IdType -> (Id -> DsM IdSeq) -> (Id -> DsM IdSeq) -> DsM IdSeq
construct ty elim k = do
    [x,x'] <- makeTmps 2
    l <- elim x
    r <- k x'
    return $ cut1ds x x' ty l r

dsTyLam :: Abs.Id -> Abs.Type -> [LamPat] -> MetaInfo -> Abs.Expr -> IdType -> (Id -> DsM IdSeq) -> DsM IdSeq
dsTyLam tv0 _tv_ty lps info e ty k = do
    [z'] <- makeTmps 1
    ma <- newMeta info
    insertIVar tv0 ma $ \ tv -> do
        mv <- newMeta info
        unify info ty (T (Forall tv ma mv))
        s <- dsLams lps e mv $ \ z -> return (S0$ Ax z z')
        construct ty (\ x -> return $ S0$ TUnpack z' (T TType) tv x (r0 s)) k

dsLam :: Pat -> [LamPat] -> MetaInfo -> Abs.Expr -> IdType -> (Id -> DsM IdSeq) -> DsM IdSeq
dsLam p lps info e ty k = do
    [ml,mr] <- newMetas 2 info
    unify info ty (ml ⊸ mr)
    (x,cx) <- dsPat ml p
    [y] <- makeTmps 1
    s <- cx (dsLams lps e mr $ \ y' -> return (S0$ Ax y y'))
    construct (ml ⊸ mr) (\ f -> return $ S0 $ Cross false f x y (r0 s)) k

dsLams :: [LamPat] -> Abs.Expr -> IdType -> (Id -> DsM IdSeq) -> DsM IdSeq
dsLams []       e ty k = dsExpr e ty k
dsLams (lp:lps) e ty k = case lp of
    ValPat p     -> dsLam   p                lps info e ty k
    TyPat tv     -> dsTyLam tv (mkTyHole tv) lps info e ty k
    TyTyPat tv t -> dsTyLam tv t             lps info e ty k
  where
    info = FromExpr (Abs.Lam (toMultiPat lp lps) e)

dsELit :: Integer -> IdType -> (Id -> DsM IdSeq) -> DsM IdSeq
dsELit i ty k = do
    unify (FromExpr (Abs.ELit (Abs.Lit i))) int ty
    [x] <- makeTmps 1
    s0 $ IntLit x i <$> rM (k x)

dsIntOp :: Abs.Expr -> IntOp -> Abs.Expr -> Abs.Expr -> IdType -> (Id -> DsM IdSeq) -> DsM IdSeq
dsIntOp e0 op e1 e2 ty k = do
    unify (FromExpr e0) ty int
    dsExpr e1 int $ \ z1 -> do
    dsExpr e2 int $ \ z2 -> do
    [x] <- makeTmps 1
    s0$ IntOp op z1 z2 x <$> rM (k x)

trCmpOp :: Abs.BinOp -> CmpOp
trCmpOp op = case op of
    Abs.Eq -> Eq
    Abs.Ne -> Ne
    Abs.Gt -> Gt
    Abs.Ge -> Ge
    Abs.Le -> Le
    Abs.Lt -> Lt

dsIntCmp :: Abs.Expr -> CmpOp -> Abs.Expr -> Abs.Expr -> IdType -> (Id -> DsM IdSeq) -> DsM IdSeq
dsIntCmp e0 op e1 e2 ty k = do
    unify (FromExpr e0) ty bool
    dsExpr e1 int $ \ z1 -> do
    dsExpr e2 int $ \ z2 -> do
    [x] <- makeTmps 1
    s0$ IntCmp op z1 z2 x <$> rM (k x)

dsExpr :: Abs.Expr -> IdType -> (Id -> DsM IdSeq) -> DsM IdSeq
dsExpr e0 ty {- contains the "true" quantifier -} k = rep $ case e0 of

    -------------------
    -- Vars and units

    Abs.Var x -> do
        (t,x') <- del x
        unify (FromExpr e0) t ty
        k x'


    Abs.Unit -> do
        unify (FromExpr e0) (T One) ty
        construct (T One) (\ x -> return (S0$ SBot x)) k
    {-
     ⊢ tiling s : BigPar s Index
    Abs.Tiling s -> do
      s' <- trSize (FromExpr e0) s
      let typ = bigPar s' index
      unify (FromExpr e0) typ ty
      [x] <- makeTmps 1
      Tile x s' <$> k x
    -}

    {-

    G, x : ~C |- e : _|_
    --------------------------
    G |- return { x -> e } : C

    -}

    Abs.Return px e -> do

        (z,cx) <- dsPat (neg ty) px

        [z'] <- makeTmps 1

        cut1ds z z' ty <$> cx (dsSeq e) <*> k z'


    -----------------
    -- Axiom

    Abs.Ax ex ey -> do
        [xr,yr] <- makeTmps 2

        mv <- newMeta (FromExpr e0)

        unify (FromExpr e0) ty (T Bot)

        sx <- dsExpr ex mv       $ \ x -> return (S0$ Ax x xr)
        sy <- dsExpr ey (neg mv) $ \ y -> return (S0$ Ax y yr)

        construct (T Bot) (\ x -> return $ S0$ SOne false x $ r0$ cut1ds xr yr mv sx sy) k

{-
    Abs.Cut [px] a [py] b -> do


        [tx',ty'] <- newMetas 2 (FromExpr e0)

        unify (FromExpr e0) (neg tx') ty'

        (x,cx) <- dsPat tx' px
        sx <- cx (dsSeq a)

        (y,cy) <- dsPat ty' py
        sy <- cy (dsSeq b)

        construct Bot (\ u -> return $ SOne false u $ Cut x y ty' sx sy) k
        -}

    Abs.Cut pxs a pys b -> do
        unify (FromExpr e0) ty (T Bot)

        when (length pxs /= length pys) $ throw "Cut with different lengths"

        left  <- newMetas (length pxs) (FromExpr e0)
        right <- newMetas (length pys) (FromExpr e0)

        zipWithM_ (unify (FromExpr e0) . neg) left right

        let go :: [(Abs.Pat,IdType)] -> DsM ([Id],DsM IdSeq -> DsM IdSeq)
            go []         = return ([],id)
            go ((p,t):ps) = do
                (x,cx) <- dsPat t p
                (xs,cxs) <- go ps
                return (x:xs,cx . cxs)

        (xs,cx) <- go (zip pxs left)
        sx <- cx (dsSeq a)

        (ys,cy) <- go (zip pys right)
        sy <- cy (dsSeq b)

        construct (T Bot) (\ u -> return $ S0$ SOne false u $ S'$ NCut Manual FanLeft sizeOne (zip3 xs ys right) (r0 sx) (r0 sy)) k

    Abs.Halt -> do
        unify (FromExpr e0) ty (T Bot)
        construct (T Bot) (\ u -> return $ S0$ SOne false u $ S'$ Halt []) k

    -------------------
    -- Tensor

    Abs.Pair a b -> do
        [x',y'] <- makeTmps 2
        [ta,tb] <- newMetas 2 (FromExpr e0)
        unify (FromExpr e0) (T (ta :*: tb)) ty
        dsExpr a ta $ \ va -> do
        dsExpr b tb $ \ vb -> do
        construct ty
            (\ z -> return $ S0$ Par false z x' (S'$ Ax va x') y' (S'$ Ax vb y'))
            k

    -----------------------
    -- Plus

    Abs.Case e px ex py ey -> do
        [tl,tr] <- newMetas 2 (FromExpr e)

        dsExpr e (T (tl :+: tr)) $ \ z -> do

        mv <- newMeta (FromExpr e0)
        unify (FromExpr e0) ty mv

        sc <- getScope
        (x,cx) <- dsPat tl px
        sx <- cx (dsExpr ex mv k)
        sc_after_1 <- getScope

        putScope sc
        (y,cy) <- dsPat tr py
        sy <- cy (dsExpr ey mv k)

        sc_after_2 <- getScope
        when (M.toList sc_after_1 /= M.toList sc_after_2) $ throw (msgCaseMismatch e0)


        return $ S0$ Plus z (Branch x (r0 sx)) (Branch y (r0 sy))


    Abs.MkPlus lr a -> do
        [x'] <- makeTmps 1
        [ta,tb] <- newMetas 2 (FromExpr e0)
        let (ch,ty') = case lr of
                Abs.Inl -> (LL.Inl,ta)
                Abs.Inr -> (LL.Inr,tb)
        unify (FromExpr e0) (T (ta :+: tb)) ty
        dsExpr a ty' $ \ v -> do
        construct ty
            (\ z -> return $ S0$ With false z ch x' (S'$ Ax v x'))
            k

    Abs.Choose lr e -> do
        [x'] <- makeTmps 1
        [ta,tb] <- newMetas 2 (FromExpr e0)
        let (ch,ty') = case lr of
                Abs.Fst -> (LL.Inl,ta)
                Abs.Snd -> (LL.Inr,tb)
        unify (FromExpr e0) ty' ty
        dsExpr e (T (ta :&: tb)) $ \ z -> s0$ With false z ch x' <$> rM (k x')

    Abs.Choices ex ey -> do

        tl <- newMeta (FromExpr ex)
        tr <- newMeta (FromExpr ey)

        [x,y] <- makeTmps 2

        unify (FromExpr e0) ty (T (tl :&: tr))

        sc <- getScope
        sx <- dsExpr ex tl $ \ x' -> return $ S0$ Ax x x'

        putScope sc
        sy <- dsExpr ey tr $ \ y' -> return $ S0$ Ax y y'

        construct (T (tl :&: tr)) (\ z -> return $ S0$ Plus z (Branch x (r0 sx)) (Branch y (r0 sy))) k

    ------------------------------
    -- Let
    --
    Abs.Let [] e' -> dsExpr e' ty k

    Abs.Let (Abs.LetDecl (Abs.PSig p t) e:ds) e' ->
        dsExpr (Abs.Let (Abs.LetDecl p (Abs.ExprSig e t):ds) e') ty k

    Abs.Let (Abs.LetDecl p e:ds) e' -> do
        tp <- newMeta (FromExpr e)
        dsExpr e tp $ \ z -> do
        dsPat' tp p z (dsExpr (abslet ds e') ty k)
      where
        abslet []    rest = rest
        abslet decls rest = Abs.Let decls rest


    -----------------------------
    -- Quantifier related

    Abs.TApp e t -> do
        t' <- trType t
        a <- makeTmp "a"

        ma <- newMeta (FromExpr e)
        mv <- addTyVar a $ newMeta (FromExpr e)

        dsExpr e (T (Forall a ma mv)) $ \ z -> do

            mv' <- follow mv
            ty' <- dsRunFresh (tySubst a t' mv')

            unify (FromExpr e0) ty ty'

            x <- makeTmp (id_name z ++ "'")
            s0$ TApp false z t' x <$> rM (k x)

    Abs.Pack t e -> do
        t' <- trType t

        ma <- newMeta (FromExpr e0)
        a <- makeTmp "a"

        mv <- addTyVar a $ newMeta (FromExpr e0)
        unify (FromExpr e0) ty (T (Exists a ma mv))
        mv' <- follow mv

        inner_ty <- dsRunFresh (tySubst a t' mv')

        dsExpr e inner_ty $ \ v -> do

            x' <- makeTmp (id_name v ++ "'")
            construct ty
                (\ z -> return $ S0$ TApp false z t' x' (S'$ Ax v x'))
                k

    ------------------
    -- Exponentials

{-
    Abs.Derelict e -> do
        dsExpr e (Bang ty) $ \ z -> do
            x <- makeTmp ("dmd_" ++ id_name z)
            Demand x z <$> k x
    -}

    Abs.Load i -> do
        -- lookup variable in the intuitionistic context
        isc <- gets st_iscope
        case lookup (Just (name i)) isc of
            Nothing -> throw (msgUnboundIdentifier i)
            Just (ix,ity)  -> do
                x <- makeTmp ("load_" ++ id_name ix)
                unify (FromExpr e0) ity ty
                s0$ Load ix 1 x <$> rM (k x)

    Abs.Promote e -> do
        -- assertEmptyScope
        [z'] <- makeTmps 1
        mv <- newMeta (FromExpr e0)
        unify (FromExpr e0) (T (Bang mv)) ty
        s <- dsExpr e mv $ \ z -> return (S0$ Ax z z')
        construct ty
            (\ x -> return $ S0$ Offer false x z' (r0 s))
            k

{-
    Abs.Ignore i e -> do
        (t,i') <- del i
        mv <- newMeta (FromExpr e0)
        unify (FromExpr e0) (Bang mv) t
        Ignore false i' <$> dsExpr e ty k
-}


    -- TODO: return an array instead of a big tensor
    {-
    Abs.Alias e -> do
        mv <- newMeta (FromExpr e0)
        unify (FromExpr e0) (Bang mv :*: Bang mv) ty
        dsExpr e (Bang mv) $ \ z -> do
            z' <- makeTmp (id_name z ++ "'")
            [f,g] <- makeTmps 2
            construct
                (Bang mv :*: Bang mv)
                (\ fg -> return $ Alias false z z' (Par false f g fg (Ax f z) (Ax g z')))
                k
                    -- ref first, the thing we want to copy
    -}


    ---------------
    -- Functions

    Abs.App ef ex -> do
        [ml] <- newMetas 1 (FromExpr e0)
        dsExpr ef (ml ⊸ ty) $ \ f -> do
            [r,x'] <- makeTmps 2
            s0$ Par false f
                <$> pure x' <*> rM (dsExpr ex ml $ \ x -> return (S0$ Ax x x'))
                <*> pure r  <*> rM (k r)

    Abs.Lam mp e -> dsLams (simpMultiPat mp) e ty k

    Abs.Crash e -> do
        dsExpr e (T Zero) $ \ z -> do
            construct ty
                (\ z' -> return (S0$ SZero z [z']))
                k

    Abs.Fix e -> do
        -- assertEmptyScope
        [t] <- newMetas 1 (FromExpr e0)
        unify (FromExpr e0) t ty
        [z'] <- makeTmps 1
        s <- dsExpr e (T (neg (T (Bang t)) :|: t)) $ \ z -> return (S0$ Ax z z')
        construct ty
            (\ u -> return (S0$ Fix z' u (neg ty) (r0 s)))
            k

    ---------------
    -- Arithmetic

    Abs.CaseLit e brs def_var def_expr -> do
        e' <- dsCaseLit e brs def_var def_expr
        REPORT $ hang "CaseLit" 4 (DsErrors.print e')
        dsExpr e' ty k

    Abs.ELit i        -> dsELit (dsLit i) ty k
    Abs.EPlus   e1 e2 -> dsIntOp e0 Add e1 e2 ty k
    Abs.EMinus  e1 e2 -> dsIntOp e0 Sub e1 e2 ty k
    Abs.EMul    e1 e2 -> dsIntOp e0 Mul e1 e2 ty k
    Abs.EDiv    e1 e2 -> dsIntOp e0 Div e1 e2 ty k
    Abs.EMod    e1 e2 -> dsIntOp e0 Mod e1 e2 ty k
    Abs.ENegate e1    -> dsExpr (Abs.ELit Abs.LitZero `Abs.EMinus` e1) ty k
    Abs.EBinOp e1 cmp_op e2 -> dsIntCmp e0 (trCmpOp cmp_op) e1 e2 ty k
    Abs.Ignore x -> do
        (t,x') <- del x
        unify (FromExpr e0) t int
        unify (FromExpr e0) (T One) ty
        construct (T One) (return . S0 . IntIgnore x' . S' . SBot) k
    Abs.Copy e -> do
        unify (FromExpr e0) (T (int :*: int)) ty
        dsExpr e int $ \ xy -> do
            [x,y,x',y'] <- makeTmps 4
            construct
                (T (int :*: int))
                (\ x'y' -> return $ S0$ IntCopy xy x y (S'$ Par false x'y' x' (S'$ Ax x' x) y' (S'$ Ax y' y)))
                k

    ----------------
    -- Recursive types

    Abs.Build e -> do
        [z'] <- makeTmps 1

        a <- makeTmp "b"

        f <- addTyVar a $ newMeta (FromExpr e0)

        REPORT $ hang ("build" <+> pretty ty) 4
                      (pretty (T (Rec Mu a f)) $$ pretty (foldTy' a f))

        unify (FromExpr e0) ty (T (Rec Mu a f))

        _ty <- follow ty
        REPORT $ "build" <+> pretty _ty

        s <- dsExpr e (foldTy' a f) $ \ z -> return (S0$ Ax z z')

        construct (T (Rec Mu a f)) (\ x -> return $ S0$ Unfold x z' (r0 s)) k

    ----------
    -- Arrays
{-

       G  |- e : a
    -------------------------
       ⊗G |- distribute e : ⊗a

-}

    {-
    Abs.Distribute sz0 e -> do

        [z'] <- makeTmps 1
        a <- newMeta (FromExpr e0)

        sz <- trSize (FromExpr e0) sz0

        unify (FromExpr e0) (bigTensor sz a) ty

        let bound = map name (idsOfExpr e)

            strip abs_name u@(id_ty,i)
                | abs_name `elem` bound = do
                    ms <- newMeta (FromExpr e0)
                    ti <- newMeta (FromExpr e0)
                    unify (FromExpr e0) (bigTensor (sz `sizeMul` ms) ti) id_ty
                    return (bigTensor ms ti,i)
                | otherwise = return u

        putScope =<< M.traverseWithKey strip =<< getScope

        s <- dsExpr e a $ \ z -> return (Ax z z')

        construct (bigTensor sz a)
            (\ x -> return $ Distribute z' x s)
            k
    -}

{-
    Abs.Foldmap sz0 e1 e2 e3 -> do
        a <- newMeta (FromExpr e0)
        sz <- trSize (FromExpr e0) sz0
        let sz_plus_one = sizeLit 1 `sizeAdd` sz
        [x1,x2,x3] <- makeTmps 3
        s1 <- dsExpr e1 (bigTensor sz_plus_one (a ⊸ ty))       (\ z -> return (Ax z x1))
        s2 <- dsExpr e2 (bigTensor sz          (ty ⊸ ty ⊸ ty)) (\ z -> return (Ax z x2))
        dsExpr       e3 (bigPar    sz_plus_one a) $ \ z ->
            Foldmap x1 x2 x3 (-1) z ty s1 s2 <$> k x3
            -}

    Abs.Unfold{} -> error "Desugar: Unfold TODO"

    --------------
    -- Par
    --
    -- This should only be applicable when the goal is _|_, otherwise
    -- you can just use pre- and post- compose.
    -- (a -o a') -o (b -o b') -o (a | b) -o (a' | b')

    Abs.Connect e px ex py ey -> do

        unify (FromExpr e0) ty (T Bot)

        [tx',ty'] <- newMetas 2 (FromExpr e0)

        dsExpr e (T (tx' :|: ty')) $ \ z -> do

            (x,cx) <- dsPat tx' px
            sx <- cx (dsSeq ex)

            (y,cy) <- dsPat ty' py
            sy <- cy (dsSeq ey)

            construct (T Bot) (\ u -> return $ S0$ SOne false u $ S'$ Par false z x (r0 sx) y (r0 sy)) k

    ----------------
    -- Type signatures

    Abs.ExprSig e t -> do
        t' <- trType t
        unify (FromExpr e0) ty t'
        dsExpr e t' k

    -----------------
    -- Holes
    Abs.ExprHole _ -> do
         xs <- M.elems <$> getScope
         ictx <- gets st_iscope
         f <- getFollower
         ty' <- f ty
         xs' <- sequence [ (,) x <$> f t | (t,x) <- xs ]
         ictx' <- sequence [ (,) x <$> f t | (_,(x,t)) <- ictx ]
         throw $ msgHole ty' ictx' xs'
         -- Alternatively: progress with typechecking to collect more
         -- information about unresolved meta-variables and eaten scope
         -- Register this as a crash, and try to catch errors, and if
         -- there is a registered hole (or many), follow them now
         --

  where
    rep m = do
        let hdr = text "dsExpr" <+> parens (text (printTree e0)) <+> parens (pretty ty)
        REPORT hdr
        -- xs <- M.elems <$> getScope
        -- tvs <- gets (map snd . st_iscope)
        r <- m
        -- -- This output is in general way too long to print:
        -- REPORT $ hang (hdr <+> "=") 4 (ppDoc r) {- contains metas so we can't use foldSeq -}
        return r


getFollower :: DsM (IdType -> DsM IdType)
getFollower = do
    su <- gets st_mgu
    return (applySubst su)

follow :: IdType -> DsM IdType
follow t = join (($ t) <$> getFollower)

dsSeq :: Abs.Expr -> DsM IdSeq
dsSeq e = dsExpr e (T Bot) (return . S0. SBot)

resolveMetas :: IdDeriv -> DsM IdDeriv
resolveMetas d = do
    su <- gets st_mgu
    REPORT "Resolve metas"
    let follow_metas t = case t of
            T (Var _ (Meta _)) -> applySubst su t
            _              -> return t
    travDerivM chk =<< travDerivM follow_metas d
  where
    chk t = case t of
        T (Var _ (Meta (MetaVar _ u _ i))) -> throw (msgUnresolvedMeta u i)
        _ -> return t

__ :: IdType
__ = wild

trSize :: MetaInfo -> Abs.Size -> DsM IdSize
trSize info = go
  where
    go s = case s of
        Abs.SizeLit i -> return (sizeLit (dsLit i))
        Abs.SizeSum  s1 s2 -> (+) <$> go s1 <*> go s2
        Abs.SizeProd s1 s2 -> (*) <$> go s1 <*> go s2
        Abs.SizeHole h -> newMeta (FromType (Abs.TyHole h))
        Abs.SizeVar (absx@(name -> x)) -> do
            tvs <- gets st_iscope
            case lookup (Just x) tvs of
                Just (ix,t) -> do
                    unify info index t
                    return (sizeVar ix)
                _ -> throw (msgUnboundTyVar absx)

trType :: Abs.Type -> DsM IdType
trType = go
  where
    go :: Abs.Type -> DsM IdType
    go t0 = case t0 of
        Abs.Type         -> tM$ return TType
        Abs.Index        -> return index
        Abs.Int          -> return int
        Abs.Tensor x y   -> tM$ (:*:) <$> go x <*> go y
        Abs.Par x y      -> tM$ (:|:) <$> go x <*> go y
        Abs.Plus x y     -> tM$ (:+:) <$> go x <*> go y
        Abs.With x y     -> tM$ (:&:) <$> go x <*> go y
        Abs.One          -> tM$ return One
        Abs.Bot          -> tM$ return Bot
        Abs.Top          -> tM$ return Top
        Abs.Zero         -> tM$ return Zero
        Abs.Lollipop x y -> (⊸) <$> go x <*> go y
        Abs.Bang x       -> tM$ Bang <$> go x
        Abs.Quest x      -> tM$ Quest <$> go x
        Abs.BigTensor s x -> bigTensor <$> trSize (FromType t0) s <*> go x
        Abs.BigPar s x    -> bigPar <$> trSize (FromType t0) s <*> go x
        Abs.Neg x        -> neg <$> go x
        Abs.Forall qbs t -> goQ Forall (qbList qbs) t
        Abs.Exists qbs t -> goQ Exists (qbList qbs) t
        Abs.Mu x t         -> insertTyVar x $ \ x' -> T . Rec Mu x' <$> go t
        Abs.Nu x t         -> insertTyVar x $ \ x' -> T . Rec Nu x' <$> go t
        Abs.TyHole{}       -> newMeta (FromType t0)
        Abs.TyVar x        -> go' t0 x []
        Abs.TyApp{}        -> uncurry (go' t0) (collectTyApp t0)
        Abs.TySize s       -> tM$ SizeT <$> trSize (FromType t0) s

    goQ _ []         r = go r
    goQ q ((x,t):xs) r = do
        t' <- trType t
        insertIVar x t' $ \ x' -> T . q x' t' <$> goQ q xs r

    go' t0 (absx@(name -> x)) ts = do
        tvs <- gets st_iscope
        case lookup (Just x) tvs of
            Just (ix,t) | null ts -> do
                unify (FromType t0) (T TType) t
                return (var ix)
            _ -> do
                -- type aliases:
                v <- asks (M.lookup x)
                case v of
                    Just (t,is) -> do
                        tys <- mapM go ts
                        when (length tys /= length is) $ throw (msgIncorrectlyAppliedAlias absx is tys)
                        insertTyVars is $ \ is' -> dsRunFresh . tySubsts (zip is' tys) =<< trType t
                    Nothing -> throw (msgUnboundTyVar absx)

qbList :: Abs.QBList -> [(Abs.Id,Abs.Type)]
qbList = go
  where
    go qbl = case qbl of
        Abs.OneUntyped x     -> [untyped x]
        Abs.OneTyped x t     -> [(x,t)]
        Abs.ConsUntyped x xs -> untyped x:go xs
        Abs.ConsTyped x t xs -> (x,t):go xs

    untyped x = (x,mkTyHole x) -- alternatively Abs.Type

collectTyApp :: Abs.Type -> (Abs.Id,[Abs.Type])
collectTyApp (Abs.TyVar x)    = (x,[])
collectTyApp (Abs.TyApp ts t') =
    let (t,ts') = collectTyApp ts
    in  (t,ts' ++ [t'])
collectTyApp _ = error "Types not applied to an alias"

simpMultiPat :: MultiPat -> [LamPat]
simpMultiPat (OnePat lp)     = [lp]
simpMultiPat (ConsPat lp mp) = lp : simpMultiPat mp
simpMultiPat (MultiPat mp t) = map (\ p -> ValPat (p `PSig` t)) (manyPatElems mp)

manyPatElems :: ManyPat -> [Pat]
manyPatElems (TwoManyPat p1 p2) = [p1,p2]
manyPatElems (ConsManyPat p mp) = p : manyPatElems mp

toMultiPat :: LamPat -> [LamPat] -> MultiPat
toMultiPat lp []       = OnePat lp
toMultiPat lp (lq:lqs) = ConsPat lp (toMultiPat lq lqs)

dsLit :: Abs.Lit -> Integer
dsLit Abs.LitZero = 0
dsLit Abs.LitOne  = 1
dsLit (Abs.Lit i) = i

dsCaseLit :: Abs.Expr -> [Abs.LitBranch] -> Abs.Id -> Abs.Expr -> DsM Abs.Expr
dsCaseLit e brs0 def_var def_expr = case brs0 of
    []                        -> return (Abs.Let [Abs.LetDecl (Abs.PVar def_var) e] def_expr)
    Abs.LitBranch lit rhs:brs -> do
        x <- makeAbsId
        y <- makeAbsId
        rest <- dsCaseLit (Abs.Var y) brs def_var def_expr
        return $
            Abs.Let
                [Abs.LetDecl (Abs.PPair (Abs.PVar x) (Abs.PVar y)) (Abs.Copy e)]
                (Abs.Case (Abs.EBinOp (Abs.Var x) Abs.Ne (Abs.ELit lit))
                    Abs.PUnit (Abs.Let [Abs.LetDecl Abs.PUnit (Abs.Ignore y)] rhs)
                    Abs.PUnit rest
                )

