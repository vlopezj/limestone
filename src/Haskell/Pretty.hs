{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE OverloadedStrings,RecordWildCards #-}
module Haskell.Pretty where

import Text.PrettyPrint hiding ((<>))
import Data.Monoid
import LL (Choice(..))

import Haskell
import PrettyId

instance (Pretty n,Pretty r) => Pretty (HExpr n r) where
    pretty = ppExpr 0

instance (Pretty n,Pretty r) => Pretty (HPat n r) where
    pretty = ppPat 0

instance (Pretty n,Pretty r) => Pretty (TyCon n r) where
    pretty Con{..} = hang
        ("newtype" <+> ppCon tc_name <+> sep ("r":map pretty tc_fvs) <+> "=") 2
        (hang (ppCon tc_name) 2
            (braces
                (hang (ppProject tc_name <+> "::") 2
                    (ppType 0 (Forall tc_quant tc_inner)))))

ppProgram :: (Pretty n,Pretty r) => HExpr n r -> [TyCon n r] -> Doc
ppProgram e nts = vcat
    ( "{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}"
    : map pretty nts ++
      [hang ("foo" <+> "=") 2 (pretty e)]
    )

ppChoice :: Choice -> Doc
ppChoice Inl = "Left"
ppChoice Inr = "Right"

ppCon :: Pretty n => n -> Doc
ppCon i = "T" <> pretty i

ppProject :: Pretty n => n -> Doc
ppProject i = "un" <> pretty i

ppExpr :: (Pretty n,Pretty r) => Int -> HExpr n r -> Doc
ppExpr i e0 = case e0 of
    Choice ch e   -> parensIf (i > 1) $ hang (ppChoice ch) 2 (ppExpr 1 e)
    Tuple e1 e2   -> csv (map (ppExpr 1) [e1,e2])
    Var x       -> pretty x
    App{} -> parensIf (i > 1) $
        let (fun,args) = collectArgs e0
            pp_args    = map (ppExpr 2) args
            pp_fun     = ppExpr 1 fun
        in  hang pp_fun 2 (sep pp_args)
    Lam{} -> parensIf (i > 0) $
        let (args,body) = collectBinders e0
            pp_body     = ppExpr 0 body
            pp_args     = map (ppPat 0) args
        in  hang ("\\" <+> sep pp_args <+> "->") 2 pp_body
    TyLam _ e -> ppExpr i e
    TyApp e _ -> ppExpr i e
    Case _b e _t alts -> parensIf (i > 0) $
        hang ("case" <+> ppExpr 0 e <+> "of") 2
             (inside "{ " "; " "}" (map ppAlt alts))
    Let _ p e _t b -> parensIf (i > 0) $
        hang ("let" <+> ppPat 0 p <+> "=") 6 (ppExpr 0 e <+> "in") $$
        ppExpr 0 b
    Project tc   -> ppProject tc
    Construct tc -> ppCon tc
    Load x -> pretty x
    Save e -> ppExpr 1 e
    Offer e -> ppExpr i e
    Unit -> "()"
    ZeroElim{} -> "undefined"
    MetaExpr n _ _ -> text n

    IntOp op -> parens (pretty op)
    CmpOp op -> parens (pretty op)
    IntLit l -> integer l

    Compose _ e1 e2 -> hang (ppExpr 1 e1 <+> "`mappend`") 0 (ppExpr 1 e2)
    NewRef n k -> hang ("newSTRef >>= \\ " <> pretty n <> "->") 0 (ppExpr 100 k)
    WriteRef n -> "writeSTRef " <> pretty n
    ReadRef source n k -> hang ("readSTRef " <> pretty source <> ">>= \\ " <> pretty n <> "->") 0 (ppExpr 100 k)

    Mempty -> "mempty"

    e ::: t -> parens (hang (parens (ppExpr 0 e) <+> "::") 2 (ppType 0 t))
    Delay e -> ppExpr i e
    What{} -> "what?"

ppAlt :: (Pretty n,Pretty r) => (HPat n r,HExpr n r) -> Doc
ppAlt (pat,rhs) = hang (ppPat 0 pat <+> "->") 2 (ppExpr 0 rhs)

ppPat :: (Pretty n,Pretty r) => Int -> HPat n r -> Doc
ppPat i pat = case pat of
    PVar x _t    -> pretty x {- parens (hang (pretty x <+> "::") 2 (ppType 0 t)) -}
    PChoice ch p -> parensIf (i > 0) $ ppChoice ch <+> ppPat 1 p
    PTuple p1 p2 -> csvPat (map (ppPat 0) [p1,p2])
    PUnit        -> "()"
    PTrue        -> "True"
    PFalse       -> "False"

ppType :: (Pretty n,Pretty r) => Int -> HType n r -> Doc
ppType i t0 = case t0 of
    TVar x      -> pretty x
    Pair t1 t2  -> csv (map (ppType 0) [t1,t2])
    Either t1 t2 -> parensIf (i > 1) $
        hang ("Either") 2 (sep (map (ppType 2) [t1,t2]))
    Neg t       -> parensIf (i > 0) $
        hang (ppType 1 t <+> "->") 2 "r"
    Pos t -> ppType i t
    Result -> "r"
    ArrTy t1 t2 -> parensIf (i > 0) (hang (ppType 1 t1) 2 (ppType 0 t2))

    Int -> "Int"
    Top -> "()"
    Bottom -> "Void"

    Bang t -> ppType i t

--    ArrTy t1 t2 -> parensIf (i > 0) $ hang (ppType 1 t1 <+> "->") 2 (ppType 0 t2)

    Forall{}    -> parensIf (i > 0) $
        let (tvs,t) = collectForalls t0
        in  hang ("forall" <+> sep (map pretty tvs) <+> ".") 2 (ppType 0 t)

    TyCon (Con tc free _ _) -> parensIf (i > 1) $
        hang (ppCon tc) 2 (sep ("r":map pretty free))

    MetaInject{} -> error "ppType on MetaInject!"
    SuspendedSubst{} -> error "ppType on SuspendedSubst!"
    Star{} -> error "ppType on Star!"
    Meta{} -> error "ppType on Meta!"


parensIf :: Bool -> Doc -> Doc
parensIf True  = parens
parensIf False = id

csv :: [Doc] -> Doc
csv = inside "(" "," ")"

csvPat :: [Doc] -> Doc
csvPat = parens . cat . punctuate ","

inside :: Doc -> Doc -> Doc -> [Doc] -> Doc
inside _ _ _ []     = empty
inside l p r (x:xs) = cat (go (l <> x) xs)
  where
    go y []     = [y,r]
    go y (z:zs) = y : go (p <> z) zs

