{-# LANGUAGE OverloadedStrings, TypeOperators, PatternGuards #-}
module Haskell.Tex where

import Haskell
import MarXup (Textual(..))
import MarXup.Tex
--import MarXup.Tikz (outline, noOutline)
import MarXup.Diagram (outline, noOutline)
import MarXup.Latex
import MarXup.Latex.Math
import TexPretty hiding (texType)
import qualified TexPretty
import Id
import LL (Choice(..),choice)
import Data.Monoid
import MarXup.DerivationTrees
import Data.List (intersperse,partition)
import qualified Data.Foldable as T
import Framework (medSpace)

compose :: TeX
compose = math $ cmd0 "circledast"
        -- can't use diamond, already used for units!

sep :: [TeX] -> TeX
sep = punctuate " "

sepp :: [TeX] -> TeX
sepp = punctuate space

(<+>) :: TeX -> TeX -> TeX
a <+> b = a <> " " <> b

space = medSpace

(<++>) :: TeX -> TeX -> TeX
a <++> b = a <> space <> b

texHsCtx' :: Bool -> Style -> [(Id,IdHType)] -> [TeX]
texHsCtx' negations st ctx = [ texVarT x (dn <> texType st 0 t) | (x,t) <- ctx ]
  where
    dn | negations = "¬¬"
       | otherwise = ""

texHsCtx :: Style -> [(Id,IdHType)] -> [TeX]
texHsCtx = texHsCtx' False

ifILL :: Style -> (TeX -> TeX) -> TeX -> TeX
ifILL ILL f = f
ifILL F2  _ = id

data ShowTypes = ShowTypes | HideTypes

stack'em :: [TeX] -> TeX
stack'em xs = array ["t"] "l" $ map (:[]) xs

texExpr :: ShowTypes -> Style -> Int -> Expr -> TeX
texExpr showBoundVariablesTypes st i e0 = case e0 of
    Delay e -> go i e
    What nm _ -> textual nm
    Choice ch e   -> parensIf (i > 0) (texChoice ch <+> go 2 e)
    -- Choose ch   -> texChosen ch
    Tuple e1 e2 -> paren (go 1 e1 <> "," <> go 1 e2)
    -- Choices e1 e2 -> paren (go 1 e1 <> ifILL st (const ";") "," <> go 1 e2)
    Var x       -> texVar x
    Haskell.Save e -> ifILL st (parensIf (i > 0) . (save_ <++>)) (go 1 e)
    Haskell.Load n -> ifILL st (parensIf (i > 0) . (load_ <++>)) (texVar n)
--    TyApp e t -> parensIf (i > 1) (go 1 e <++> brack (texType st 0 t))
    TyApp e t -> parensIf (i > 1) (go 1 e <++> "@" <++> texType st 2 t)
    App e1 e2 _ -> parensIf (i > 1) (go 1 e1 <++> go 2 e2)
    TyLam x e -> parensIf (i > 0) (keyword "Λ" <++> texVar x <++> keyword "." <++> go 0 e)
    Offer e
        | st == ILL -> parensIf (i > 0) (keyword "offer" <++> go 2 e)
        | otherwise -> go i e
    Lam{} -> parensIf (i > 0) $
        let (args,body) = collectBinders e0
            pp_body     = go 0 body
            pp_args     = map (texPat showBoundVariablesTypes st 0) args
        in  keyword "λ" <++> sepp pp_args <++> keyword "." <++> pp_body
    Case b e _ alts -> parensIf (i > 1) $
        case_ <> go 0 e <> _of_ <> stacky (map (texAlt showBoundVariablesTypes st) alts)
      where
        stacky = if b then stack'em . addBraces else brac . punctuate ";"
        addBraces (x:xs) = "{" <> x : map (";" <>) (toLast (<> "}") xs)
        toLast f xs = case reverse xs of
            y:ys -> reverse (f y:ys)

    Let True p e _ b ->
        stack'em [let_ <> texPat showBoundVariablesTypes st 0 p <> "=" <> go 0 e <++> in_, go 0 b]
    Let False p e _ b ->parensIf (i > 0) $
        let_ <> texPat showBoundVariablesTypes st 0 p <> "=" <> go 0 e <++> in_ <> go 0 b
    Unit -> diamond
    ZeroElim e xs -> keyword "absurd" <++> go 2 e <++> if st == ILL then sepp (map texVar xs) else mempty
    MetaExpr n vs es -> plupp (textual n <> mconcat [ brackets (texVar v <> texNeg False <> "/" <> texVar v) | v <- vs ] ) <>
                        if null es'
                            then mempty
                            else brack (punctuate "," (map (go 0) es'))
      where
        es' = [ e
              | e <- es
              , case e of
                    Var (Id{id_name = '?':_}) -> False
                    _                         -> True
              ]
    Compose True x y -> stack'em [go 0 x <> ";", go 0 y]
    Compose False x y -> go 0 x <> compose <> go 0 y
    WriteRef r -> keyword "writeRef " <> texVar r
    NewRef r k -> stack'em [texVar r <> "← " <> keyword "newRef" <> ";", go 0 k]
    ReadRef r n k -> stack'em [texVar n <> "← " <> keyword "readRef " <> texVar r <> ";", go 0 k]
    Mempty -> keyword "mempty"
    e ::: _ -> go i e
    _ -> error $ "texExpr: " ++ show e0
  where go = texExpr showBoundVariablesTypes st

rulName :: Style -> Expr -> TeX
rulName st e0 = case e0 of

    Lam{}                  -> arr <> "-intro"
    App{}                  -> arr <> "-elim"

    Tuple{}                -> times <> "-intro"
    Let _ (PTuple _ _) _ _ _ -> times <> "-elim"

    Let _ PUnit _ _ _      -> "1-elim"
    Unit                   -> "1-intro"

    Var{} | F2  <- st      -> "init"
          | ILL <- st      -> "init" <> (math $ tex "_" <> tex "L")
    Load{} | F2  <- st     -> "init"
           | ILL <- st     -> "init" <> (math $ tex "_" <> tex "J")

    Let _ _ Save{} _ _     -> "!-elim"
    Offer{}                -> "!-intro"

    ZeroElim{}             -> "0-elim"

    Choice c _             -> plus <> "-intro" <> case c of
      Inl -> "-l"
      Inr -> "-r"
    Case{}                 -> plus <> "-elim"

    TyLam{}                -> "∀-intro"
    TyApp{}                -> "∀-elim"

    MetaExpr{}             -> "IH"

    Let{}                  -> "cut"
    _                      -> "rulName"

  where
    [arr,times,plus] = case st of
        F2  -> ["→","×","+"]
        ILL -> ["⊸","⊗","⊕"]


texBind :: Style -> (Id,IdHType) -> TeX
texBind st (x,t) = case id_name x of
  ('?':_) -> texType st 0 t
  _ -> texVar x  <> ":" <> texType st 0 t

texHCtx :: Style -> [(Id,IdHType)] -> TeX
texHCtx st ctx = mconcat $ intersperse "," $ map (texBind st) ctx

texDeriv :: Bool -> Style -> Deriv -> TeX
texDeriv showPrograms st (Deriv ictx ctx res e) = derivationTree' $ v
  where _ ::> v = texPartDeriv showPrograms st ictx ctx res e

patCtx (PTuple p1 p2) = patCtx p1 ++ patCtx p2
patCtx (PVar x t) = [(x,t)]
patCtx _ = []

texPartDeriv :: Bool -> Style -> [(Id,IdHType)] -> [(Id,IdHType)] -> IdHType -> Expr -> (Link ::> Derivation)
texPartDeriv showPrograms st = go
  where
    restrict e1 ctx = case st of
      F2 -> ctx
      ILL -> filter ((`T.elem` e1) . fst) ctx
    part e1 ctx = case st of
      F2 -> (ctx,ctx)
      ILL -> partition ((`T.elem` e1) . fst) ctx
    go ictx ctx res e0 = case e0 of
      ZeroElim e xs -> rul [go' lctx Bottom e]
        where lctx = restrict e ctx
      Choice ch e | Either l r <- res -> rul [go' ctx (choice ch l r) e]
      Tuple e1 e2 | Pair t1 t2 <- res ->
                   rul [go' lctx t1 e1,go' rctx t2 e2]
        where
          (lctx,rctx) = part e1 ctx

      Let _ (PVar x t) (Save e1) _ e2 -> rul
          [ go ictx lctx (Haskell.Bang t) e1
          , go ((x,t):ictx) rctx res e2
          ]
        where
          (lctx,rctx) = part e1 ctx

--      Load z | Just t <- lookup z ictx ->
--        rul [go ictx ((x,t):ctx) res e]

      Let _ pat e1 t e2 -> rul [go' lctx t e1, go' (rctx++patCtx pat) res e2]
        where
          (lctx,rctx) = part e1 ctx
      Lam pat body | Neg _      <- res -> rul [go' (ctx++patCtx pat) Result body]
      Lam pat body | ArrTy _ t2 <- res -> rul [go' (ctx++patCtx pat) t2 body]
      TyLam x e | Forall _ t <- res -> rul [go' ctx t e]
      TyApp (e ::: t@(Forall w ta)) tb ->
        rul [ go' ctx t e ]
        {-
        delay_rul [ Delayed LeftA ::> (go' ctx t e)
             , defaultLink ::> Node
                (Rule () None mempty mempty
                    -- check that ta instantiated at tb is res
                    ("⊢" <> texType st 0 (hInstTVar w tb ta) <> "=" <> texType st 0 res)
                )
                []
            ]
                -}
      -- Unit: default
      -- zeroElim: default
      App e1 e2 t -> rul [go' lctx ty e1,go' rctx t e2]
        where
          ty = case res of
            Result -> Neg t
            _      -> ArrTy t res
          (lctx,rctx) = part e1 ctx

      Case _ e3 t3 [(PChoice Inl (PVar x t),e1),(PChoice Inr (PVar y u),e2)] ->
         rul [go' lctx t3 e3,go' ((x,t):rctx) res e1,go' ((y,u):rctx) res e2]
        where
          (lctx,rctx) = part e3 ctx
      -- Load{} | st == F2 ->  r
      -- Save r | st == ILL =
      --        | otherwise -> go' ctx t
      What _ _ -> final
--      Var _ -> final
      Offer e | st == F2 -> go' ctx res e
      Offer e | Haskell.Bang t <- res -> rul [go' ctx t e] -- check ctx is empty
      e ::: t -> go' ctx t e
      Delay e -> let _ ::> v = go' ctx res e in Delayed ::> v
      _ -> rul []

     where
     {-
      skipInF2 x = case st of
         F2 -> x
         ILL -> rul [x]
         -}
      go' = go ictx
      content = math $ (if null ictx then mempty else texHCtx st ictx <> semi)
                    <> texHCtx st ctx <> "⊢" <> program <> texType st 0 res
      program = if showPrograms then texExpr HideTypes st 0 e0 <> ":"
                                else mempty
      rul subs = defaultLink ::> Node (Rule (outline "black") mempty (textSize FootnoteSize $ rulName st e0) content) subs
      final    = defaultLink ::> Node (Rule noOutline mempty mempty content) []

      semi = case st of
        F2  -> ","
        ILL -> ";"

texAlt :: ShowTypes -> Style -> (Pat,Expr) -> TeX
texAlt bound_types st (pat,rhs) = texPat bound_types st 0 pat <+> cmd0 "mapsto" <+> texExpr bound_types st 0 rhs

texPat :: ShowTypes -> Style -> Int -> Pat -> TeX
texPat bound_types st i pat = case pat of
    PVar x t
        | ShowTypes <- bound_types -> parensIf (i >= 0) (texVar x <> ":" <> texType st 0 t)
        | otherwise                -> texVar x
    PChoice ch p -> parensIf (i > 0) $ texChoice ch <> texPat bound_types st 1 p
    PTuple p1 p2 -> paren (texPat bound_types st 0 p1 <> "," <> texPat bound_types st 0 p2)
    PUnit        -> diamond

texChoice :: Choice -> TeX
texChoice Inl = inl_
texChoice Inr = inr_

texChosen :: Choice -> TeX
texChosen Inl = fst_
texChosen Inr = snd_

texType :: Style -> Int -> IdHType -> TeX
texType st i t0 = case t0 of
    Meta x []       -> textual x
    Meta x vs       -> textual x <> brack (mconcat $ intersperse "," $ map (texType st 1) vs)
    Result       -> textual "r"
    TVar x       -> texVar x
    Pair t1 t2   -> parensIf (i > 3) $ texType st 3 t1 <> times <> texType st 3 t2
    Either t1 t2 -> parensIf (i > 0) $ texType st 1 t1 <> plus <> texType st 1 t2
    Neg (Neg t) | ILL <- st -> "¬¬" <> texType st 1 t
    Neg t        -> parensIf (i > 0) $ texType st 1 t <> arr <> "r"
    Int    -> "Int"
    Top    -> "1"
    Bottom -> "0"
    Bang t -> bang <> texType st 1 t

    ArrTy t1 t2 -> parensIf (i > 0) $ texType st 1 t1 <> arr <> texType st 0 t2

    Forall{}    -> parensIf (i > 0) $
        let (tvs,t) = collectForalls t0
        in  "∀" <> sep (map texVar tvs) <> "." <> texType st 1 t

    MetaInject t -> TexPretty.texType i t
    SuspendedSubst t0 x t -> parensIf (i > 0) $
        texType st 0 t0 <> brack (texType st 0 t <> "/" <> texVar x)
    Star -> "*"
    Pos t -> texType st i t
    _ -> error $ "Haskell.texType: " ++ show t0
  where
    [arr,times,plus,bang] = case st of
        F2  -> ["→","×","+",""]
        ILL -> ["⊸","⊗","⊕",tex "{}" <> "!"]

parensIf :: Bool -> TeX -> TeX
parensIf True  = paren
parensIf False = id
