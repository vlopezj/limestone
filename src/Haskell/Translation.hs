{-# LANGUAGE TemplateHaskell,RecordWildCards,PatternGuards, DeriveFunctor,ScopedTypeVariables, ViewPatterns #-}
{-# LANGUAGE DeriveFunctor, DeriveTraversable, DeriveFoldable, StandaloneDeriving #-}
{-# LANGUAGE PatternSynonyms, ViewPatterns #-}
{-# OPTIONS_GHC -w #-}
module Haskell.Translation where

import Haskell.Types as H

import Id
import PrettyId (prettyShow)
import LL hiding (meta,choice,trType,Var,(:::),intOp)
import LL.Para
import qualified LL
import Fresh
import Haskell.Eval (refreshLet)
import Control.Applicative

import Control.Applicative
import Control.Monad
import Control.Arrow ((***))

data Style = F2 | ILL deriving Eq

doubleNeg :: Style -> HType n r -> HType n r
doubleNeg F2  = id
doubleNeg ILL = Neg . Neg

{-
Notation:
A* is the translation of A
~A is A perp
A→r is a haskell type

For every type A, A* is either of the form

- A*; in which case we say that A is positive or of the form
- ~A* → r; in which case we say that A is negative.

Consequently, either: ~A* = A* -> r
                      A* = ~A* -> r

we define:

- (A ⊗ B)* = (A* × B*)
- (A ⅋ B)* = (~A* × ~B*) → r



We write C t for the translation of proof t. C t is an expression of type r.
For every variable x:A in the context we assume x:A* in the translations' context
Γ,x:A ⊢ s     y:B,Δ ⊢ t
-------------------------
    Γ,z:A ⅋ B,Δ ⊢ u

We then have the cases:

1. Both A and B negative
2. A and B of different sign. (We treat the case A positive)
3. Both A and B positive


1. C u = z (\x → C s,\y → C t)
2. C u = let x = \x' → z (x',(\y → C t)) in C s
3. C u = let x = \x' → let y = \y' → z (x',y')
                       in C t
         in C s

Remark about using polymorphic values.

Say we instanciate α with a negative type A. Thus we have A* = ~A* →
r. When we pass a concrete value of type ~α, the generated polymorphic
code expect the type α* → r, that is (~A* → r) → r, while we actually
have a value of type A* in our hands. So we must insead pass \k → k x.

In general, we need a translation function from any haskell type T(α)
to T((α → r) → r). This can probably be done by induction on the type
structure.

Alternative representation of multiplicatives:

- (A ⊗ B)* = (A* → B* → r) → r
- (A ⅋ B)* = ~A* → ~B* → r

Changes: the tensor elimination changes to cps; the par elimination
changes to curryfied style; the polarity of the multiplicative
fragment is swapped.

-}

data ExpandMetas = Expand | Don'tExpand

trTypeNoExpand :: Style -> IdType -> IdHType
trTypeNoExpand = trType' Don'tExpand

trType :: Style -> IdType -> IdHType
trType = trType' Expand

trType' :: ExpandMetas -> Style -> IdType -> IdHType
trType' metas st = go
  where
    go t0@(T t0') = case t0' of
        t :*: u                                -> Pair (go t) (go u)
        t :|: u                                -> Neg (Pair (go $ neg t) (go $ neg u))
        t :+: u                                -> Either (go t) (go u)
        t :&: u                                -> Neg (Either (go $ neg t) (go $ neg u))
        Zero                                   -> Bottom
        LL.Top                                 -> Neg Bottom
        One                                    -> H.Top
        Bot                                    -> Neg H.Top

        LL.Bang t                              -> H.Bang (doubleNeg st $ go t)
        Quest t                                -> Neg (go (T (LL.Bang (neg t))))

        LL.Forall x _ t                        -> H.Forall x (Neg (go (neg t) `Either` go (neg t')))
          where t' = runFreshM (tySubst x (neg (var x)) t)
        Exists n u t                           -> Neg (go (T (LL.Forall n u (neg t))))
        LL.Var _ (LL.TVar True r)                -> H.TVar r
        LL.Var _ (LL.TVar False r)               -> Neg $ H.TVar r
        TType                                   -> Star
        Rec{}                                  -> err
        LL.Var _ (MetaSyntactic{}) | Don'tExpand <- metas -> MetaInject (T (Plupp t0))
        LL.Var _ (MetaSyntactic True _ _)                 -> MetaInject (T (Plupp t0))
        LL.Var _ (MetaSyntactic False _ _)                -> Neg $ go (neg t0)
        LL.Var _ (LL.Meta{})                     -> err
        MetaNeg{}                              -> err
        Lollipop{}                             -> err

        Array (pol -> Positive) _ t                     -> Pos (Neg (Pair H.Int (go $ neg t)))
        Array (pol -> Negative) _ _                     -> Neg (go $ neg t0)

        Index{}                                -> err
        SizeT{}                                -> err
        Plupp{}                                -> err
        UnpushedNeg{}                          -> err
        LL.Int True                            -> H.Int
        LL.Int False                           -> Neg H.Int
      where
        err = error $ "H.trType " ++ prettyShow t0



trDeriv :: Style -> IdDeriv -> Expr
trDeriv st d =
    let e = foldr (\ (i,i_ty) -> case i_ty of
                Star -> TyLam i
                _    -> Lam (PVar i (doubleNeg st i_ty))) <$> inner_e <*> pure ictx
    in runFreshMFrom d $ refreshLet =<< e
  where
    (ictx,ctx,e) = trDerivWithCtx Expand st d
    inner_e      = foldr (\ (x,x_ty) -> Lam (PVar x x_ty)) <$> e <*> pure ctx

-- | /Cosmetic/ change of variable
--   This is in evalExpr fixed with fixHackyIds
--   Real solution: use freshM in trDerivWithCtx
bar :: Id -> Id
bar i = i { id_name = id_name i ++ "'" }

trDeriv' :: ExpandMetas -> Style -> IdDeriv -> H.Deriv
trDeriv' metas st d = H.Deriv ictx ctx Result $ runFreshMFrom d e
  where (ictx,ctx,e) = trDerivWithCtx metas st d

dr ::  HExpr name ref -> HExpr name ref
dr = Delay

dl ::  HExpr name ref -> HExpr name ref
dl = Delay

dc ::  HExpr name ref -> HExpr name ref
dc = Delay

trCtxItem :: Style -> IdSized -> IdHType
trCtxItem st (t :^ 1) = trType st t
trCtxItem st (t :^ s)       = trType st (T (Array APositive s t))

trDerivWithCtx :: ExpandMetas -> Style -> IdDeriv -> ([(Id,IdHType)],[(Id,IdHType)],FreshM Expr)
trDerivWithCtx metas st d@(D0 (LL.Deriv _ (seqctx_ -> (ictx, ctx)) _)) =
    ( map (second (dn . trCtxItem st)) ictx
    , map (second (trCtxItem st)) ctx
    , foldIdDeriv sf d []
    )
 where
  dn Star = Star
  dn x = case st of
    ILL -> Neg (Neg x)
    F2  -> x

--  sf :: IdDeriv -> SeqFinal'' Id Id ([Id] -> IdHType) ([Id] -> FreshM Expr)
  sf (D0 (LL.Deriv _ (seqctx_ -> (ts0, ctx)) (S0 s0))) = SeqFinal{..}
   where
    ssz _ = id
    stysz _ = id  
    sty _ t su = trType' metas st (runFreshM (tySubsts [ (tv,neg (var tv)) | tv <- su ] t))

    sax x y y_ty su
        | Neg a <- y_ty su     = return $ H.Var y :@ a @: H.Var x
        | otherwise            = return $ H.Var x :@ (y_ty su) @: H.Var y

    scross _ z x x_ty y y_ty s su = mkLet (PTuple (PVar x (x_ty su)) (PVar y (y_ty su))) (Var z) <$> s su

    spar _ z x x_ty y y_ty s t su = do
        x' <- refreshId x
        y' <- refreshId x
        s' <- s su
        t' <- t su
        return $ case () of
            ()  | Neg x'_ty <- x_ty su, Neg y'_ty <- y_ty su
                -> mkLet
                    px (dl $ Lam (PVar x' x'_ty)
                        (mkLet
                            py (Lam (PVar y' y'_ty) (Var z :@ Pair x'_ty y'_ty @: (dr $ Tuple (Var x') (Var y'))))
                            t'
                        )
                    )
                    s'

                | Neg x'_ty <- x_ty su
                -> mkLet px (Lam (PVar x' x'_ty) (Var z :@ Pair x'_ty (Neg $ y_ty su) @: (dr $ Tuple (Var x') (Lam py t')))) (dr s')

                | Neg y'_ty <- y_ty su
                -> mkLet py (Lam (PVar y' y'_ty) (Var z :@ Pair (Neg $ x_ty su) y'_ty @: (dr $ Tuple (Lam px s') (Var y')))) (dr t')

                | otherwise
                -> Var z :@ Pair (Neg $ x_ty su) (Neg $ y_ty su) @: (dr $ Tuple (Lam px s') (Lam py t'))
      where
        px = PVar x (x_ty su)
        py = PVar y (y_ty su)
        x' = bar x
        y' = bar y

    szero x xs _su = return $ ZeroElim (Var x) (map (\(r,_,_) -> r) xs)

    splus z x x_ty y y_ty s t su = do
        s' <- s su
        t' <- t su
        return $ Case False (Var z) (Either (x_ty su) (y_ty su))
            [ (PChoice Inl (PVar x (x_ty su)),dc s')
            , (PChoice Inr (PVar y (y_ty su)),t')
            ]

    -- NOTE: x is rebound here and shadowed! (Alt. solution: use FreshM)
    swith _ z_ty ch z x x_ty s su = do
        x' <- refreshId x
        s' <- s su
        return $ case () of
            ()  | Neg x'_ty <- x_ty su
                -> mkLet px (Lam (PVar x' x'_ty) (Var z :@ z_ty_su @: (dr (Choice ch (Var x'))))) (dr s')

                | otherwise -> Var z :@ z_ty_su @: dr (Choice ch (Lam px s'))
      where
        Neg z_ty_su = z_ty su
        px = PVar x (x_ty su)

    sone _ z s su = mkLet PUnit (Var z) <$> s su

    sbot z _su = return $ Var z :@ H.Top @: H.Unit

    swhat n xs su = return $ MetaExpr n su (map Var xs)

    ssave z t x s su
        = Let False (PVar x (doubleNeg st $ t su)) (save z)
                    (H.Bang $ doubleNeg st $ t su) <$> s su

    sload z t x s su
        | F2  <- st = mkLet (PVar x (t su)) (Var z) <$> s su
        | otherwise = do
            s' <- s su
            return $ H.Load z :@ Neg (t su) @: (Lam (PVar x (t su)) s')

    soffer _ z x t s su = do
        x' <- refreshId x
        s' <- s su
        return $ case () of
            ()
                | Neg t_su <- t su, ILL <- st
                    -> Var z :@ (H.Bang $ Neg $ Neg $ t_su) @: dr (H.Offer (Lam (PVar x (t su)) s'))

                | Neg t_su <- t su, F2 <- st
                    -> mkLet (PVar x (Neg t_su)) (Var z) s'

                | ILL <- st
                    -> Var z :@ H.Bang (Neg $ Neg $ Neg $ t su) @: dr (H.Offer (Lam (PVar x' (Neg $ Neg $ t su)) (Var x' :@ (Neg $ t su) @: Lam (PVar x (t su)) s')))

                | otherwise
                    -> Var z :@ Neg (t su) @: dr (Lam (PVar x (t su)) s')

    stapp _ z z_ty inst_ty x ty s su = do
        x' <- refreshId x
        s' <- s su
        return $ case (z_ty su,summary) of
            (H.Forall w (Neg a@(Neg _ `Either` _)),Inl)

                -> choice :@ hInstTVar w tr_b_ty a @: (Choice Inl $ Lam (PVar x (inst_ty su)) s')

            (H.Forall w (Neg a@(_ `Either` Neg _)),Inr)

                -> choice :@ hInstTVar w tr_b_ty a @: (Choice Inr $ Lam (PVar x (inst_ty su)) s')

            (H.Forall w (Neg a@(_ `Either` _)),_) -> case inst_ty su of
                Neg i_ty ->
                    mkLet (PVar x (Neg i_ty))
                          (Lam (PVar x' i_ty) (choice :@ hInstTVar w tr_b_ty a @: Choice summary (Var x')))
                          s'
                bad_ty -> error $ "Haskell stapp inst_ty su not negative: " ++ show bad_ty
            bad_ty -> error $ "Haskell stapp inst_ty on weird type: " ++ show bad_ty
      where
        TApp _ _ tyB _ _ = s0

        (summary,choice,tr_b_ty) = case ty su of
            Neg _ty' -> (Inr,dl $ (Var z ::: z_ty su) `TyApp` (sty ts0 (neg tyB) su),(sty ts0 (neg tyB) su))
            _        -> (Inl,dl $ (Var z ::: z_ty su) `TyApp` (ty su),ty su)

    stunpack tyvar z z_ty x xty s su = do
        x' <- refreshId x
        brs <- sequence [ go Inl su , go Inr su' ]
        return $ Var z :@ (H.Forall tyvar $ Neg $ (xty su `Either` xty su'))  @:
            (dr $ (TyLam tyvar
                (Lam (PVar x' (xty su `Either` xty su'))
                     (Case True (Var x') (xty su `Either` xty su') brs)
                )) ::: i_z_ty)
      where
        Neg i_z_ty = z_ty su
        go :: Choice -> [Id] -> FreshM (Pat,Expr)
        go ch@Inl csu = do { s' <- dc <$> s csu ; return (PChoice ch (PVar x (xty csu)),s') }
        go ch@Inr csu = do { s' <- s csu        ; return (PChoice ch (PVar x (xty csu)),s') }
        su' = su ++ [tyvar]

    shalt _su = return $ Mempty


    sncut [(x,x_ty,y,y_ty)] s t su = do
        s_lam <- Lam (PVar x (x_ty su)) <$> (s su)
        t_lam <- Lam (PVar y (y_ty su)) <$> (t su)
        return $ case () of 
          () | isNeg (y_ty su) -> t_lam :@ Neg (x_ty su) @: s_lam
          () | otherwise       -> s_lam :@ Neg (y_ty su) @: t_lam
      where

    sncut [] s t su = Compose False <$> (s su) <*> (t su)
    sncut [(v,vty,v',_v'ty),(w,wty,w',_w'ty)] s t su = do
        locOf_v <- locOf v
        locOf_w <- locOf w
        s' <- s su
        t' <- t su
        return $ NewRef (locOf_v) $
          NewRef (locOf_w) $
          mkLet' True (PVar v (vty su)) (WriteRef (locOf_v)) $
          mkLet' True (PVar w (wty su)) (WriteRef (locOf_w)) $
          Compose True s' $
          ReadRef (locOf_v) v' $
          ReadRef (locOf_w) w' $
          t'
      where
        locOf = refreshId

    sncut _ _ _ _ = error "Untranslatable n-cut"

    sintop x op z1 z2 s su = mkLet (PVar x H.Int) (H.IntOp op :@ dunno @: Var z1 :@ dunno @: Var z2) <$> s su

    sintlit x i s su = mkLet (PVar x H.Int) (H.IntLit i) <$> s su

    sintcopy x y z s su = mkLet (PVar x H.Int) (Var z) . mkLet (PVar y (H.Int)) (Var z) <$> s su

    sintcmp x op z1 z2 s su =
        mkLet (PVar x (Either H.Top H.Top))
            (Case False (H.CmpOp op :@ dunno @: Var z1 :@ dunno  @: Var z2) dunno
                [ (PFalse, Choice Inl H.Unit)
                , (PTrue,  Choice Inr H.Unit)
                ])
            <$> (s su)

    sintignore _z s su = s su

    sBigTensor x z t a sz s su
        | 1 <- sz = case a su of
            Neg a' -> do
              s' <- s su
              return $ hcomment "BT1: NEG" $ Var z :@ Pair H.Int (Neg a') @: Tuple (H.IntLit 0) (Lam (PVar x a') s')
            a'     -> do
              s' <- s su
              x' <- refreshId x
              return $ hcomment "BT1: POS" $ mkLet (PVar x a') (Lam (PVar x' a') (Var z :@ Pair H.Int (Neg a') @: Tuple (H.IntLit 0) (Var x'))) s'
        | otherwise     = mkLet (PVar x (t su)) (Var z) <$> (s su)
      where

    sSplit x sx y sy z tz s su = do
      ix <- freshFrom "ix"
      v  <- freshFrom "v"
      hcomment "sSplit Top" .
        mkLet (PVar x (tz su)) (Var z) .
        mkLet (PVar y (tz su)) (Lam (PTuple (PVar ix H.Int) (PVar v a')) (Var z :@ Pair H.Int a' @: Tuple (intOp Sub (Var ix) (trSize sx)) (Var v))) <$>
        (sBigTensor x x tz ak sx (\ su' ->
        sBigTensor y y tz ak sy s su') su)
      where
        Just (a :^ _) = lookup z ctx
        ak = sty (error "ssplit sty") a
        a' = ak su

    sBigPar z a brs su = do
        ix <- freshFrom "ix"
        z' <- refreshId z
        let go_br [] = return $ undef
            go_br ((x,s,sz):xs) = do
               gone <- go_br xs
               s'   <- s su
               let rfs  = refs s'
                   ctx' = filter (\ (n,_) -> n /= z && n `elem` rfs) ctx
               goInner <- go ix ctx' s'
               return $ iff (CmpOp Lt :@ H.Int @: Var ix :@ H.Int @: trSize sz)
                                      (mkLet (PVar x (a su)) (Var z') goInner)
                                      (mkLet (PVar ix H.Int) (intOp Sub (Var ix) (trSize sz)) gone)
        gone <- go_br brs
        return $ Var z :@ Neg (Pair H.Int (a su)) @: Lam (PTuple (PVar ix H.Int) (PVar z' (a su)))
          (hcomment "sBigPar top" $ gone)
        {-
        [
            case sz of
                SizeOne -> s su
                _       -> go (filter (\ (n,_) -> n /= z) ctx) (s su))
        -}
      where
        undef = MetaExpr "undefined" [] []
        iff a b c | Let _ _ _ _ u <- c, u == undef = b
                  | otherwise  = H.if_ a b c

        go ix []                 e = return e
        go ix ((g,t :^ 1):gs) e = go ix gs e
        go ix ((g,t :^ _gsz):gs) e = do
          gone <- go ix gs e
          v <- freshFrom "v"
          return $ case sty (error "sbigpar sty") t su of
            Neg a' -> hcomment "NEG" $ Var g :@ Pair H.Int (Neg a') @: Tuple (Var ix) (Lam (PVar g a') gone)
            a'     -> hcomment "POS" $ mkLet (PVar g (Neg a')) (Lam (PVar v a') (Var g :@ Pair H.Int a' @: Tuple (Var ix) (Var v))) gone



    sMerge _ _ _ _ _      = return $ MetaExpr "trDeriv: merge not supported" [] []
    sfreeze _ _ _ _ = return $ MetaExpr "trDeriv: freeze not supported" [] []
    sfoldmap _ _ _ _ _ _  = return $ MetaExpr "trDeriv: foldmap not supported" [] []
    ~[sfold, sunfold, sdemand,
      signore, salias, smem, stile, snatrec, sfix, sPermute]
     = error "trDeriv: deriv. rule not supported yet"

hcomment x y = y
-- hcomment x y = MetaExpr ("{-"++ x ++"-}") [] [] :@ H.Int @: y

trSize :: IdSize -> Expr
trSize (sizeToList -> []) = H.IntLit 0
trSize (sizeToList -> vs) = foldl1 (intOp Add) $ fmap (uncurry $ foldl (intOp Mul)) $ fmap (H.IntLit *** fmap trSymbol) vs

trSymbol :: Symbol Id -> Expr
trSymbol (LL.TVar pos v) = Var v
trSymbol v = error $ "Invalid variable " ++ show v ++ " during translation."
{- flip map ((a,rs) 
trSize 0   = H.IntLit 0
trSize 1   = H.IntLit 1
trSize (LL.TVar _ x) = Var x
trSize (SizeAdd a b) = intOp Add (trSize a) (trSize b)
trSize (SizeSub a b) = intOp Sub (trSize a) (trSize b)
trSize (SizeMul a b) = intOp Mul (trSize a) (trSize b)
-}
{-
storeSingletonMaybe :: IdSize -> Id -> Id -> HType -> Expr -> Expr
storeSingletonMaybe sz z x a k
    | SizeOne <- sz = case a of
        Neg a' -> Var z :@ Pair H.Int (Neg a') @: Tuple (H.IntLit 0) (Lam (PVar x a') k)
        a'     -> mkLet (PVar x a') (Lam (PVar x' a') (Var z :@ Pair H.Int (Neg a') @: Tuple (H.IntLit 0) (Var x))) k
    | otherwise     = mkLet (PVar x a) (Var z) k
      where
        x' = bar x

-}






intOp :: IntOp -> Expr -> Expr -> Expr
intOp i e1 e2 = H.IntOp i :@ H.Int @: e1 :@ H.Int @: e2

dunno :: t
dunno = error " Ints not supported in System F"

