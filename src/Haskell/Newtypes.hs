module Haskell.Newtypes where

import Haskell.Types as H

import Id
import qualified LL
import Fresh

import Control.Monad.State
import Data.Map (Map)
import qualified Data.Map as M

insertNewtypes :: Expr -> (Expr,[TyCon Id Id])
insertNewtypes e1 = (e3,M.elems st)
  where
    (e2,st) = LL.runFreshMFrom e1 (exprTravTyM new_ty e1 `runStateT` M.empty)

    e3 = exprTrav mod_expr e2

    new_ty ::
        IdHType ->
        StateT (Map (HType () Int) (TyCon Id Id)) FreshM IdHType
    new_ty t0 = case t0 of
        H.Forall x t -> do
            let (nt,free) = normalise t0
            d <- gets (M.lookup nt)
            case d of
                Just tc -> return (TyCon tc)
                Nothing -> do
                    name <- lift freshId
                    let tc = Con name free x t
                    modify (M.insert nt tc)
                    return (TyCon tc)
        _ -> return t0

    mod_expr e0 = case e0 of
        TyLam tv e ::: TyCon (Con tc _free x t) ->
            let a = H.Forall tv (LL.rename x tv t)
            in Construct tc :@ a @: (e ::: a)
        (e ::: a@(TyCon (Con tc _free x t))) `TyApp` tA ->
            (Project tc :@ a @: e) ::: hInstTVar x tA t
        _ -> e0

