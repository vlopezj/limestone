module Haskell.Loop where

import Haskell.Types as H
import Haskell.Translation hiding (trType)

import qualified Id as H
import qualified LL as LL
import Parser.AbsMx
import qualified Parser.AbsMx as Abs
import Data.Maybe
import Fresh

result,r,dum :: Abs.Id
result = Abs.Id ((0,0),"result")
r      = Abs.Id ((0,0),"r")
dum    = Abs.Id ((0,0),"x")

trId :: H.Id -> Abs.Id
trId i = Abs.Id (fromMaybe (0,0) (H.id_m_loc i),show i)

trType :: IdHType -> Type
trType = go
  where
    go t0 = case t0 of
        H.Either a b -> Plus (go a) (go b)
        H.Pair   a b -> Tensor (go a) (go b)
        H.Bang t     -> Abs.Bang (go t)
        H.Neg t      -> go t `Lollipop` TyVar r
        H.Top        -> Abs.One
        H.Bottom     -> Abs.Zero
        H.Forall x t -> Abs.Forall (OneTyped (trId x) Type) (go t)
        TVar x       -> Abs.TyVar (trId x)
        H.Int        -> Abs.Int
        MetaInject{} -> error "trType MetaInject"
        Star         -> Type
        _            -> error "Loop: trType, untranslatable type"

trLoop :: LL.IdDeriv -> Prog
trLoop d = trTopExpr ictx ctx (runFreshM e)
  where (ictx,ctx,e) = trDerivWithCtx Expand ILL d

trTopExpr :: [(H.Id,IdHType)] -> [(H.Id,IdHType)] -> H.Expr -> Prog
trTopExpr ictx ctx e =
    Prog [TopDecl (DerivDecl
        (BothCtxs
            (TypedBinder r Type : trCtx (reverse (map dn ictx)))
            (TypedBinder result (Abs.Neg (TyVar r)) : trCtx ctx))
        (Ax (Abs.Var result) (trExpr e)))]
  where
    dn (a,Star) = (a,Star)
    dn (a,t)    = (a,H.Neg (H.Neg t))

trCtx :: [(H.Id,IdHType)] -> [Binder]
trCtx ctx = [ TypedBinder (trId i) (trType t) | (i,t) <- ctx ]

trChoice :: LL.Choice -> InlInr
trChoice LL.Inl = Abs.Inl
trChoice LL.Inr = Abs.Inr

trPat :: H.Pat -> Abs.Pat
trPat p0 = case p0 of
    H.PVar x t   -> PSig (Abs.PVar (trId x)) (trType t)
    PTuple p1 p2 -> PPair (trPat p1) (trPat p2)
    H.PUnit      -> Abs.PUnit
    _            -> error "Loop: trPat: untranslatable pattern"

trExpr :: H.Expr -> Abs.Expr
trExpr = go
  where
    go e0 = case e0 of
        Choice ch e -> MkPlus (trChoice ch) (go e)
        Tuple e1 e2 -> Abs.Pair (go e1) (go e2)
        H.Let _(H.PVar x _) (H.Save e) t b -> Abs.Let [LetDecl (PMany (trId x)) (Abs.ExprSig (go e) (trType t))] (go b)
        H.Let _ p e t b -> Abs.Let [LetDecl (trPat p) (go (e ::: t))] (go b)
        H.Lam p e   -> Abs.Lam (OnePat (ValPat (trPat p))) (go e)
        H.TyLam x e -> Abs.Lam (OnePat (TyTyPat (trId x) Type)) (go e)
        H.Unit      -> Abs.Unit
        H.ZeroElim ez xs -> foldl (\ e u -> e `Abs.App` Abs.Var (trId u)) (Abs.Crash (go ez)) xs
        H.Offer e   -> Abs.Promote (go e)
        H.App e1 e2 t -> go e1 `Abs.App` go (e2 ::: t)
        H.TyApp e t -> Abs.TApp (go e) (trType t)
        H.Var x     -> Abs.Var (trId x)
        H.Load x    -> Abs.Load (trId x)
        H.Case _ e t [(PChoice LL.Inl p1,e1),(PChoice LL.Inr p2,e2)] ->
            Abs.Case (go (e ::: t)) (trPat p1) (go e1) (trPat p2) (go e2)
        H.Case{} -> error "HaskelLoop: Only case with Inl/Ir supported"
        e ::: t    -> Abs.ExprSig (go e) (trType t)
        Delay e  -> go e
        MetaExpr{}  -> error "Loop: trExpr MetaExpr"
        _           -> error "Loop: trExpr on untranslatable expression"



