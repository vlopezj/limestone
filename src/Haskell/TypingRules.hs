module Haskell.TypingRules where

import Haskell.Types
import Haskell.Translation (Style(..))
import Type (fromNames)
import LL (Choice(..))
import Fresh
import Control.Monad.Except.Extra

typingRules :: Style -> [Deriv]
typingRules st = [funElim, funIntro
                 , pairElim, pairIntro
                 , sumElim, sumIntro Inl, sumIntro Inr
                 , typeAppl, typeAbst
                 , oneElim, oneIntro
                 , zeroElim, start
                 , cutRule
                 ] ++ illRules
  where
       illRules = case st of
          F2 -> []
          ILL -> [loadRule
                 ,saveRule
                 ,offerRule
                 ]
       gd = case st of
          ILL -> [("?Γ",meta "Γ"),("?Δ",meta "Δ")]
          F2 -> [("?Γ",meta "Γ")]
       th = case st of
          ILL -> [("?Θ",meta "Θ")]
          F2 -> []
       whatg x = what x ["?Γ"]
       whatd x = case st of
          ILL -> what x ["?Δ"]
          F2 -> what x []
       mk_deriv i l d = runFreshM $ unsafeExceptT $ fromNames (Deriv th i l d)
       start      = mk_deriv ([("?Γ",meta "Γ") | st == F2]++[("x",meta "A")]) (meta "A") (Var "x")
       funElim    = mk_deriv gd (meta "B") (App (whatg "f") (whatd "a") (meta "A"))
       funIntro   = mk_deriv [("?Γ",meta "Γ")] (meta "A" `ArrTy` meta "B") (Lam (PVar "x" $ meta "A") (what "b" []))
       pairElim   = mk_deriv gd (meta "C") $ mkLet (PTuple (PVar "x" $ meta "A") (PVar "y"$ meta "B")) (whatg "e") (whatd "c")
       pairIntro  = mk_deriv gd (Pair (meta "A")(meta "B")) $ Tuple (whatg "a") (whatd "b")
       sumElim    = mk_deriv gd (meta "C") $ Case False (whatg "e") (Either (meta "A") (meta "B")) [(PChoice Inl (PVar "x" (meta "A")),whatd "a"),(PChoice Inr (PVar "y" (meta "B")),whatd "b")]
       sumIntro c = mk_deriv [("?Γ",meta "Γ")] (Either (meta "A") (meta "B")) $ Choice c (whatg "e")
       typeAbst   = mk_deriv [("?Γ",meta "Γ")] (Forall "α" (meta "A")) $ TyLam "α" $ whatg "e"
       typeAppl   = mk_deriv [("?Γ",meta "Γ")] (Meta "A" [meta "B"]) $ TyApp (whatg "e" ::: (Forall "α" (Meta "A" [TVar "α"]))) (meta "B")
       oneIntro   = mk_deriv [] Top Unit
       oneElim    = mk_deriv [("?Γ",meta "Γ")] (meta "C") $ mkLet PUnit (whatg "e") (whatd "c")
       zeroElim   = mk_deriv gd (meta "A") $ ZeroElim (whatg "e") (if st == ILL then ["?Δ"] else [])
       offerRule  = mk_deriv [] (Bang $ meta "A") (Offer $ what "e" [])
       cutRule    = mk_deriv gd (meta "B") $ mkLet (PVar "x" $ meta "A") (whatg "e") (whatd "c")

       saveRule   = mk_deriv gd (meta "C") $
                      Let False (PVar "z" (meta "A")) (Save (whatg "e")) (Bang $ meta "A") (whatd "c")

       loadRule   = runFreshM $ unsafeExceptT $ fromNames $ Deriv (th ++ [("x",meta "A")]) [] (meta "A") (Load "x")



