{-# LANGUAGE TemplateHaskell,RecordWildCards,PatternGuards, DeriveFunctor,ScopedTypeVariables #-}
{-# LANGUAGE DeriveFunctor, DeriveTraversable, DeriveFoldable, StandaloneDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
module Haskell.Types(module Haskell.Types) where

import Haskell.Core as Haskell.Types
import qualified Haskell.TH as TH
import Id
import qualified LL
import Type (names,refs)

import Data.List ((\\),elemIndex)
import Control.Monad.State

import Data.Generics.Genifunctors
import Data.Generics.Geniplate
import Data.Bitraversable
import Data.Bifoldable
import Data.Bifunctor
import Data.Traversable (Traversable)
import Data.Foldable (Foldable)

exprTravTyM :: Monad m =>
    (HType name ref -> m (HType name ref)) ->
    (HExpr name ref -> m (HExpr name ref))
exprTravTyM = $(genTransformBiM 'TH.exprTravTyM)

exprTravTy ::
    (HType name ref -> HType name ref) ->
    (HExpr name ref -> HExpr name ref)
exprTravTy = $(genTransformBi 'TH.exprTravTy)

exprTravM :: Monad m =>
    (HExpr name ref -> m (HExpr name ref)) ->
    (HExpr name ref -> m (HExpr name ref))
exprTravM = $(genTransformBiM 'TH.exprTravM)

exprTrav ::
    (HExpr name ref -> HExpr name ref) ->
    (HExpr name ref -> HExpr name ref)
exprTrav = $(genTransformBi 'TH.exprTrav)

tyTrav ::
    (HType name ref -> HType name ref) ->
    (HType name ref -> HType name ref)
tyTrav = $(genTransformBi 'TH.tyTrav)

-- for names
normalise :: forall n t . (Ord n,Bitraversable t) => t n n -> (t () Int,[n])
normalise t =
    let nt :: t () Int
        ids :: [n]
        (nt,ids) =
            bitraverse (\ x -> lk x >> return ()) lk t `runState` []
    in  (nt,ids \\ names t)
  where
    lk x = do
        mi <- gets (elemIndex x)
        case mi of
            Just i  -> return i
            Nothing -> modify (x:) >> return 0

type IdHType = HType Id Id

-- | Extract all universal quantifiers starting a type.
collectForalls :: HType n r -> ([n],HType n r)
collectForalls (Forall x t) =
    let (xs,t') = collectForalls t
    in  (x:xs,t')
collectForalls t = ([],t)

isNeg :: HType name ref -> Bool
isNeg Neg{}       = True
isNeg _           = False


save :: ref -> HExpr name ref
save = Save . Var

if_ :: HExpr name ref -> HExpr name ref -> HExpr name ref -> HExpr name ref
if_ s t f = Case True s HBool [(PTrue,t),(PFalse,f)]


-- no refreshing yet
substVal :: Eq r => r -> HExpr n r -> HExpr n r -> HExpr n r
substVal x e = exprTrav $ \ e0 -> case e0 of
    Load y | x == y -> e
    _                       -> e0

-- no refreshing yet
substExpr :: Eq r => r -> HExpr n r -> HExpr n r -> HExpr n r
substExpr x e = exprTrav $ \ e0 -> case e0 of
    Var y | x == y -> e
    _              -> e0

hInstTVar :: Eq ref => ref -> HType name ref -> HType name ref -> HType name ref
hInstTVar x t = tyTrav $ \ t0 -> case t0 of
    TVar y   | x == y -> t
    MetaInject ll_t0 | x `elem` refs ll_t0 -> SuspendedSubst t0 x t
    _                         -> t0

substTyInExpr :: Eq r => r -> HType n r -> HExpr n r -> HExpr n r
substTyInExpr x t = exprTravTy $ \ t0 -> case t0 of
    TVar y   | x == y -> t
    MetaInject ll_t0 | x `elem` refs ll_t0 -> SuspendedSubst t0 x t
    _                         -> t0


patType :: HPat name ref -> HType name ref
patType p0 = case p0 of
    PVar _ t -> t
    PTuple p1 p2 -> Pair (patType p1) (patType p2)
    PChoice{} -> error "patType on PChoice"
    PUnit     -> Top
    PTrue     -> bool
    PFalse    -> bool
  where
    bool = Either Top Top


collectArgs :: HExpr n r -> (HExpr n r,[HExpr n r])
collectArgs (App e1 e2 _) =
    let (e,es) = collectArgs e1
    in  (e,es ++ [e2])
collectArgs e           = (e,[])

collectBinders :: HExpr n r -> ([HPat n r],HExpr n r)
collectBinders (Lam x e) =
    let (xs,e') = collectBinders e
    in  (x:xs,e')
collectBinders e         = ([],e)

data Appl n r = HExpr n r :@ HType n r

(@:) :: Appl n r -> HExpr n r -> HExpr n r
(f :@ t) @: a = App f a t

infixl 6 @:
infixl 6 :@

type Expr = HExpr Id Id
type Pat  = HPat Id Id

instance Bifoldable HType    where bifoldMap  = bifoldMapDefault
instance Bifunctor HType     where bimap      = bimapDefault
instance Bitraversable HType where bitraverse = $(genTraverse ''HType)

instance Bifoldable HExpr    where bifoldMap  = bifoldMapDefault
instance Bifunctor HExpr     where bimap      = bimapDefault
instance Bitraversable HExpr where bitraverse = $(genTraverse ''HExpr)

instance Bifoldable HPat    where bifoldMap  = bifoldMapDefault
instance Bifunctor HPat     where bimap      = bimapDefault
instance Bitraversable HPat where bitraverse = $(genTraverse ''HPat)

instance Bifoldable TyCon    where bifoldMap  = bifoldMapDefault
instance Bifunctor TyCon     where bimap      = bimapDefault
instance Bitraversable TyCon where bitraverse = $(genTraverse ''TyCon)

instance Bifoldable Deriv'    where bifoldMap  = bifoldMapDefault
instance Bifunctor Deriv'     where bimap      = bimapDefault
instance Bitraversable Deriv' where bitraverse = $(genTraverse ''Deriv')

mkLet' :: Bool -> HPat name ref -> HExpr name ref  -> HExpr name ref -> HExpr name ref
mkLet' multiLine p e b = Let multiLine p e (patType p) b

mkLet :: HPat name ref -> HExpr name ref  -> HExpr name ref -> HExpr name ref
mkLet = mkLet' False

what :: Name -> [ref] -> HExpr name ref
what = What

meta :: Name -> HType name ref
meta n = Meta n []

type Deriv = Deriv' Id Id







