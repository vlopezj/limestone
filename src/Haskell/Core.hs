{-# LANGUAGE DeriveFunctor, DeriveTraversable, DeriveFoldable #-}
module Haskell.Core where

import LL
import Data.Traversable
import Data.Foldable

data HExpr name ref
    = Choice LL.Choice (HExpr name ref)   -- +-intro
    -- | Choose Choice -- &-elim
    | Tuple (HExpr name ref) (HExpr name ref)   -- ×-intro
    -- | Choices (HExpr name ref) (HExpr name ref) -- &-intro
    | Let Bool (HPat name ref) (HExpr name ref) (HType name ref) (HExpr name ref)
    | Lam (HPat name ref) (HExpr name ref)      -- -o intro
    | TyLam ref (HExpr name ref)                -- exists intro
    | Unit                                      -- 1 intro
    | ZeroElim (HExpr name ref) [ref]                                   -- 0 elim
    | App (HExpr name ref) (HExpr name ref) (HType name ref)     -- -o elim
    | TyApp (HExpr name ref) (HType name ref) -- forall elim
    | Case Bool {- pretty print on one or two lines in TeX -}
           (HExpr name ref) (HType name ref) [(HPat name ref,HExpr name ref)] -- +-elim
    | Var ref
    | MetaExpr Name [ref] {- pending type variable flippings -}
                    [HExpr name ref] -- for the CPS article
    | Load ref                   -- copy from intuitionistic context
    | Save (HExpr name ref)      -- !-elim
    | Offer (HExpr name ref)     -- !-elim (requires empty linear context)
    | IntOp LL.IntOp
    | CmpOp LL.CmpOp
    | IntLit Integer
    | Compose Bool (HExpr name ref) (HExpr name ref) -- mix/bicut
    | NewRef name (HExpr name ref)
    | WriteRef ref
    | ReadRef ref name (HExpr name ref)
    | Mempty                    -- halt
    | HExpr name ref ::: HType name ref -- explicit type, needed around TyApp and TyLam
    | What Name [ref]

    | Project   ref {- ^ identifier of the constructor -}
    | Construct ref {- ^ identifier of the constructor -}
    | Delay (HExpr name ref)
  deriving (Eq,Show,Functor,Traversable,Foldable)

data HType name ref
    = Either (HType name ref) (HType name ref)
    | Pair (HType name ref) (HType name ref)
    -- | With (HType name ref) (HType name ref)
    | Bang (HType name ref)
    | Neg (HType name ref)

    | Pos (HType name ref)
    -- ^ To "hide" the top level Neg constructor in positive types.
    --   Translation just ignores this constructor and proceeds with the
    --   wrapped type.

    | Result
    | Top
    | Bottom
    | Forall name (HType name ref)
    | TVar ref
    | Int
    | MetaInject (LL.Type' name ref)         -- for the CPS article
    | SuspendedSubst (HType name ref) ref (HType name ref)
        -- for the CPS article, when you want to substitute inside a MetaInject
    | Star -- the type of types
    | Meta Name [HType name ref]
    | TyCon (TyCon name ref)
    -- Introducing type constructors for Haskell compilation of quantifiers

    | ArrTy (HType name ref) (HType name ref)
    -- for the CPS article...

    | HBool
  deriving (Show,Eq,Ord,Functor,Traversable,Foldable)

data TyCon name ref = Con
    { tc_name  :: Id
    -- ^ Name of type (and data) constructor
    , tc_fvs   :: [ref]
    -- ^ Free variables
    , tc_quant :: name
    -- ^ One quantified variable
    , tc_inner :: HType name ref
    -- ^ The inner type
    }
  deriving (Show,Eq,Ord,Functor,Traversable,Foldable)

data HPat name ref
    = PVar name (HType name ref)
    | PTuple (HPat name ref) (HPat name ref) -- ×-elim
    | PChoice LL.Choice (HPat name ref)      -- used in case
    | PUnit                                  -- 1-elim
    | PTrue
    | PFalse
  deriving (Eq,Show,Functor,Traversable,Foldable)

infixl 6 `TyApp`

infix 5 :::

data Deriv' n r  = Deriv [(n,HType n r)] [(n,HType n r)] (HType n r) (HExpr n r)
