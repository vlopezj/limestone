{-# LANGUAGE PatternGuards #-}
module Haskell.Eval (evalExprs,refreshLet,evalExprSome) where

import Haskell.Types as H

import LL hiding (meta, (:::))
import Fresh
import Id

import Control.Applicative
import Control.Monad

import Control.Monad.State
import Control.Monad.Identity
import Control.Monad.Except.Extra

import Safe (lastMay)

import Fuel

instance Alternative Identity where
  empty = mzero
  (<|>) = mplus

instance MonadPlus Identity where
  mzero = fail "Identity mzero"
  mplus = const


refreshLet :: Expr -> FreshM Expr
refreshLet = exprTravM $ \ e0 -> case e0 of
    Let bl (PVar x t) ty e b -> do
        x' <- refreshId x
        return (Let bl (PVar x' t) ty e (rename x x' b))
    _ -> return e0

-- no refreshing yet
evalExpr :: (MonadPlus m,Eq n) => HExpr n n -> FuelT m (HExpr n n)
evalExpr = exprTravM $ \ e0 ->
  let fuelled e = consume e0 (evalExpr e) `mplus` return e0
  in  case e0 of
        App (Lam (PVar x _) b) e _ -> fuelled (substExpr x e b)

        App (Lam (PTuple (PVar x1 _) (PVar x2 _)) b) (Tuple e1 e2) _ -> fuelled (substExpr x1 e1 (substExpr x2 e2 b))

        Let _ (PUnit) H.Unit _ b       -> fuelled b

        Let _ (PVar x _) (H.Save se) _ b -> case se of
            H.Offer e -> fuelled (substVal x e b)
            _               -> return e0

        Let _ (PVar _x _) H.Load{} _ _b -> return e0

        Let _ (PVar x _) e _ b       -> fuelled (substExpr x e b)
        Let _ (PTuple (PVar x1 _) (PVar x2 _)) (Tuple e1 e2) _t b -> fuelled (substExpr x1 e1 (substExpr x2 e2 b))
        Case _ (Choice ch e) _ ps ->
            let [(PChoice Inl (PVar xl _),el),(PChoice Inr (PVar xr _),er)] = ps
            in  case ch of
                   Inl -> fuelled (substExpr xl e el)
                   Inr -> fuelled (substExpr xr e er)
        TyApp (TyLam b e) t -> fuelled (substTyInExpr b t e)

        -- eta contraction:
        Lam (PVar x _) (App e (H.Var x') _)
            | x == x'
            , x' `notElem` (names e ++ refs e) -> fuelled e

        _ -> return e0


-- Remove type sigs & delays, they are in the way for evaluation
rmObstacles :: HExpr n r -> HExpr n r
rmObstacles = exprTrav $ \ e0 -> case e0 of
    e H.::: _       -> e
    Delay e       -> e
    Case _ e t ps -> Case False e t ps -- write cases on one line
    _             -> e0

evalExprSome :: Int -> Expr -> Expr
evalExprSome i = runIdentity . runFuelT i . evalExpr .
                 runFreshM . unsafeExceptT . fixHackyIds . rmObstacles

-- Evaluates to a fixed point!
evalExprs :: Expr -> [Expr]
evalExprs = go . runFreshM . unsafeExceptT . fixHackyIds . rmObstacles
        -- need to fix hacky ids because trDerivWithCtx does some cosmetic changes
        -- need to remove type signatures because they are in the way for some redexes
  where
    -- We use last(Maybe) to get the topmost redex
    go e = e : case lastMay [ e' | (e',0) <- runAndGetFuelT 1 (evalExpr e) ] of
        Just e' -> go e'
        _       -> []

