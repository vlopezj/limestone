module Haskell.TH where

import Haskell.Core

thOnly = error "do not use! only for template haskell to get the type."

exprTravTyM :: Monad m =>
    (HType name ref -> m (HType name ref)) ->
    (HExpr name ref -> m (HExpr name ref))
exprTravTyM = thOnly 

exprTravTy ::
    (HType name ref -> HType name ref) ->
    (HExpr name ref -> HExpr name ref)
exprTravTy = thOnly

exprTravM :: Monad m =>
    (HExpr name ref -> m (HExpr name ref)) ->
    (HExpr name ref -> m (HExpr name ref))
exprTravM = thOnly 

exprTrav ::
    (HExpr name ref -> HExpr name ref) ->
    (HExpr name ref -> HExpr name ref)
exprTrav = thOnly

tyTrav ::
    (HType name ref -> HType name ref) ->
    (HType name ref -> HType name ref)
tyTrav = thOnly 

