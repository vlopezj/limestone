{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ImpredicativeTypes #-}
-- | A continuation-monad transformer without using wrappers
--   or a toolkit for writing in delimited continuations
module ContM where

import FileLocation
import Language.Haskell.TH

type ContM' n m c a = (a -> n c) -> m c
type ContMR m c a = ContM' m m c a
type ContM  m a = forall c. ContM' m m c a 
type ContM_' m c = m c -> m c
type ContM_ m = forall c. ContM_' m c

pureContM :: a -> ContM' m m c a
pureContM a = ($ a)

fmapContM   :: (a -> b) -> ContM' n m c a -> ContM' n m c b
fmapContM   f g h = g (h . f) 

sequenceContM :: [ContM' m m c a] -> ContM' m m c [a]
sequenceContM []     cont = cont []
sequenceContM (x:xs) cont = x $ \x' -> sequenceContM xs $ \xs' -> cont (x':xs')

voidContM :: ContM m a -> ContM_ m
voidContM m cont = m $ \_ -> cont

sequenceContM_ :: [ContM_' m c] -> ContM_' m c
sequenceContM_ = foldl (.) id

type Cont' c a = (a -> c) -> c
type Cont a = forall c. (a -> c) -> c

recurse :: (a -> Cont' r b) -> [a] -> Cont' r [b]
recurse f [] μ = μ []
recurse f (v:vs) μ =
  f v $ \t ->
  recurse f vs $ \ts -> 
  μ (t:ts) 

recurse1 :: (a -> Cont' r b) -> Maybe a -> Cont' r (Maybe b)
recurse1 _ Nothing  μ = μ Nothing
recurse1 f (Just a) μ = f a $ μ . Just

recurse_ :: (a -> ContM_' m r) -> [a] -> ContM_' m r
recurse_ f [] μ = μ
recurse_ f (a:as) μ = f a $ recurse_ f as μ

recurseFold :: (s -> a -> (s -> b -> r) -> r) -> s -> [a] -> (s -> [b] -> r) -> r
recurseFold f s [] μ = μ s []
recurseFold f s (v:vs) μ =
  f s v $ \s' t ->
  recurseFold f s' vs $ \s'' ts -> 
  μ s'' (t:ts)

bindContM :: (a -> (b -> m r) -> m r) -> (b -> m r) -> a -> m r
bindContM = flip

bind2ContM :: (a -> b -> (c -> m r) -> m r) -> b -> (c -> m r) -> a -> m r
bind2ContM f b c a = f a b c



