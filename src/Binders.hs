{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
-- | Models binding structures.
--
--   A binding structure is a bitraversable bifunctor such that names are
--   always mentioned before references to those names.
module Binders where

import Data.Map (Map)
import qualified Data.Map as M
import Control.Monad.State
import Control.Monad.Except.Extra
import Data.Bitraversable
import Data.Bifoldable
import Data.Bifunctor
import Data.Maybe
import Control.Applicative
import Data.Traversable as T
import Fresh
import Id
import Control.Lens


class HasRef a where
  type Ref a

instance HasRef a => HasRef [a] where
  type Ref [a] = Ref a

instance HasRef a => HasRef (b,a) where
  type Ref (b,a) = Ref a
  
-- | The references in a sequent
refs  :: Bifoldable t => t name ref -> [ref]
refs   = biconcatMap (const []) (:[])

-- | The introduced names in a sequent
names :: Bifoldable t => t name ref -> [name]
names  = biconcatMap (:[]) (const [])

runFreshMFrom :: Bifoldable t => t Id Id -> FreshM a -> a
runFreshMFrom = runFreshMFromUnique . nextUnique

nextUnique :: Bifoldable t => t Id Id -> Unique
nextUnique = succ . maximum . map Id.id_unique . biList

-- Alpha-equivalence with unique identifiers

fromNames ::  (MonadFresh m, Bitraversable p, MonadError String m) => p Name Name -> m (p Id Id)
fromNames = fromAnythingStrict idFromName

fromNamesLiberal ::  (Bitraversable p, MonadFresh m) => p Name Name -> m (p Id Id)
fromNamesLiberal = fromAnythingLiberal idFromName

fixHackyIds :: (MonadFresh m, Bitraversable p, MonadError String m) => p Id Id -> m (p Id Id)
fixHackyIds = fromAnythingLiberal idFromTag . bimap Tag Tag

-- | Rename all bound variables to unique identifiers, preserving α-equivalence.
--   If False, free variables will also be renamed.
fromAnythingLiberal :: (Bitraversable p,Show a,Ord a,MonadFresh m)
                    => (Unique -> a -> Id) -> p a a -> m (p Id Id)
fromAnythingLiberal mk u = evalStateT (bitraverse fresh_name look u) []
  where
    fresh_name s = do
       x <- lift fresh
       let r = mk x s
       -- Important: new bindings supersede old ones.
       modify ((s,r):)
       return r
    look s = do
      m <- get
      case lookup s m of
        Nothing -> fresh_name s
        Just x -> return x

-- | Rename all bound variables to unique identifiers, preserving α-equivalence.
fromAnythingStrict :: (Bitraversable p,Show a,Ord a,MonadFresh m, MonadError String m)
                    => (Unique -> a -> Id) -> p a a -> m (p Id Id)
fromAnythingStrict mk u = evalStateT (bitraverse fresh_name look u) []
  where
    fresh_name s = do
       x <- lift fresh
       let r = mk x s
       -- Important: new bindings supersede old ones.
       modify ((s,r):)
       return r
    look s = do
      m <- get
      case lookup s m of
        Nothing -> throwError $ "Unknown identifier : " ++ show s
        Just x -> return x

-- Refreshing binders

refreshBinders' :: (Bifunctor t,Bifoldable t, MonadFresh m) => (t Id Id) -> [Id] -> m ([Id], t Id Id)
refreshBinders' s extraNames = do
    freshenedExtra <- T.mapM refreshId extraNames
    freshened <- T.sequence [ (,) b <$> refreshId b | b <- names s ]
    let f = zip extraNames freshenedExtra ++ freshened
    let lk x = fromMaybe x (lookup x f)
    return$ (freshenedExtra, bimap lk lk s)

refreshBinders s = snd <$> refreshBinders' s []

{-
refreshBindersWith :: (Bifunctor t,Bifoldable t,Bitraversable t,
                       Applicative m, Monad m, Eq a, Show a) => (a -> m b) -> (t a a) ->
                      ExceptT String m (t b b)
refreshBindersWith f s = do
    freshened <- lift $ T.sequence [ (,) b <$> f b | b <- names s ]
    let lk x = case lookup x freshened of
          Just a  -> return a
          Nothing -> throwError $ "Unbound reference " ++ show x
    bitraverse lk lk $ s
-}
refreshAll :: forall m t. (MonadFresh m, Bitraversable t) => t Id Id -> m (t Id Id)
refreshAll d = evalStateT (bitraverse ref_id ref_id d) M.empty
  where
    ref_id :: Id -> StateT (Map Id Id) m Id
    ref_id x = do
        m_y <- gets (M.lookup x)
        case m_y of
            Nothing -> do
                y <- lift (refreshId x)
                modify (M.insert x y)
                return y
            Just y -> return y

makeBindersSafe :: (Bifunctor t,Bitraversable t) => (t Id Id) -> ExceptT String FreshM (t Id Id)
makeBindersSafe = fromAnythingStrict (\u unsafeId -> unsafeId & unique .~ u)

-- | Disambiguate binders is idempotent
disambiguateBinders :: (Bitraversable t) => (t Id Id) -> (t Id Id)
disambiguateBinders d = evalState (bitraverse disambiguateId disambiguateId d) M.empty
    

refreshFromNames :: forall m t. (MonadFresh m, Bitraversable t) => t Id Id -> m (t Id Id)
refreshFromNames d = evalStateT (bitraverse ref_id ref_id d) M.empty
  where
    ref_id :: Id -> StateT (Map Id Id) m Id
    ref_id x = do
        m_y <- gets (M.lookup x)
        case m_y of
            Nothing -> do
                modify (M.insert x x)
                return x
            Just y -> return y
