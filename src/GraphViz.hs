{-# LANGUAGE RecordWildCards, PatternSynonyms #-}
-- | Render derivations using GraphViz
module GraphViz (couplingDiag) where

import LL
import TexPretty
import PrettyId
import Control.Monad.RWS
import MarXup
import MarXup.Tex
import MarXup.Diagram.Graphviz
import Control.Applicative
import qualified Data.Map as M
import Data.Bifunctor
import Eval (contextOf)
import Data.GraphViz.Types.Generalised
import Data.GraphViz.Attributes.Complete
import Data.GraphViz.Attributes.Colors.SVG
import qualified Data.Text.Lazy as T
import qualified Data.Sequence as S

type GGen a = RWS () (S.Seq (DotStatement NodeRef)) (M.Map Id NodeRef,Int) a
type NodeRef = String

tell1 :: DotStatement NodeRef -> GGen ()
tell1 = tell . S.singleton

node :: Attributes -> GGen NodeRef
node extraAttrs  = do
  let attrs = FontSize 10 : Margin (DVal 0):Width 0:extraAttrs 
  r <- snd <$> get
  modify $ second $ (+1)
  let ref = "nd" ++ show r
  tell1 $ DN $ DotNode ref attrs
  return ref

fermion s = node [Shape Circle,getNodeLab s]
boson s = node [Shape PlainText,getNodeLab s]
-- hypothesis = node [Shape Circle,Color [toWC (SVGColor White)],emptyLab]
             -- for some reason this is more compact than "shape=none"
hypothesis = node [Shape PlainText,emptyLab]

specialEdge n n' t = edgeAutoDir ([Style [SItem Dotted []]],n,t,n')
edge n n' t = edgeAutoDir ([],n,t,n')


type Edge = (Attributes,NodeRef,IdType,NodeRef)

orientEdge :: Edge -> Edge
-- orientEdge (as,s,ty@(MetaSyntactic True _ "Ξ" []),t) = (as,t,neg ty,s)
orientEdge (as,s,ty@(T (Var _ (MetaSyntactic False _  _ ))),t) = (as,t,neg ty,s)
orientEdge (as,s,T (RevNeg ty),t) = (-- [("arrowhead","none"),("arrowtail","vee")]++
                                 (Dir Back):
                                 as',t',ty',s')
   where (as',s',ty',t') = orientEdge (as,s,neg ty,t)
-- orientEdge (as,s,ReverseNeg ty,t) = (as,t,neg ty,s)
orientEdge e = e


edgeAutoDir, edgeFixedDirection :: Edge -> GGen ()

edgeAutoDir = edgeFixedDirection . orientEdge

edgeFixedDirection (attrs,sn,ty,tn) = do
  -- comment $ "  edge of type " ++ P.render (pType 0 ty)
  tell1 $ DE $ DotEdge sn tn  (Label (StrLabel $ T.pack typ) : attrs)
  where typ = dotQuote $ renderSimple Plain $ texType 0 ty

dotQuote = id
-- dotQuote x = '"': concatMap esc x ++ "\""
--   where esc '\\' = "\\\\"
--         esc x = [x]

emptyLab :: Attribute
emptyLab = Label $ StrLabel $ T.pack $ dotQuote " "


look :: Id -> GGen NodeRef
look x = do
  (m,_) <- get
  case M.lookup x m of
    Nothing -> error $ "variable not found in context:" ++ show x
    Just n -> return n

insert :: Id -> NodeRef -> GGen ()
insert x v = modify $ first $ M.insert x v

toGraphPart s0 = case s0 of
  (S0 NCut {ncut_cs = [(v,v',ty)],..}) -> do
    toGraphPart (Seqn0 ncut_left)
    toGraphPart (Seqn0 ncut_right)
    n <- look v
    n' <- look v'
    edge n n' ty
  _ -> do
      -- add a node to the output
    thisNode <- fermion s0
    forM_ (contextOf s0) $ \v ->
      insert v thisNode

-- toGraphPart :: [Name] -> [(Name,Type)] -> Seq -> GGen [NodeRef]
-- toGraphPart te e (NCut _info [(v,v',ty)] s t) = do
--   let (e0,e1) = splitAt γ e
--   (s':sn) <- toGraphPart te ((v,neg ty):e0) s
--   (t':tn) <- toGraphPart te ((v', ty)  :e1) t
--   edge s' t' ty
--   return $ sn ++ tn
-- toGraphPart te e this@(With True _ v b x s) = do
--   let (v0,(_,~(vt :&: vt')):v1) = splitAt x e
--       wt = case b of Inl -> vt; Inr -> vt'
--   (n0,n:n1) <- splitAt x <$> toGraphPart te (v0++(v,wt):v1) s
--   thisNode <- boson this
--   specialEdge thisNode n wt
--   return (n0++thisNode:n1)
-- toGraphPart te e this@(SOne True x s) = do
--   let (v0,(_,~One):v1) = splitAt x e
--   (n0,n1) <- splitAt x <$> toGraphPart te (v0++v1) s
--   thisNode <- boson this
--   return (n0++thisNode:n1)
-- toGraphPart te e this@(TApp True _ w x tyB s) = do
--   let (v0,(_,~(Forall _ tyA)):v1) = splitAt x e
--       ty = subst0 tyB ∙ tyA
--   (n0,n:n1) <- splitAt x <$> toGraphPart te (v0++(w,ty):v1) s
--   thisNode <- boson this
--   specialEdge thisNode n ty
--   return (n0++thisNode:n1)
-- toGraphPart te e this@(Offer True v x s) = do
--   let (v0,(_,~(Quest tyA)):v1) = splitAt x e
--   (n0,n:n1) <- splitAt x <$> toGraphPart te (v0++(v,tyA):v1) s
--   thisNode <- boson this
--   specialEdge thisNode n tyA
--   return (n0++thisNode:n1)
-- toGraphPart te e this@(Mem _ _ ty x n t u) = do
--    let (t',u') = splitAt x e
--    (tn:tns) <- toGraphPart te (("_x",neg ty):t') t
--    (un,uns) <- splitAt n <$> toGraphPart te (replicate n ("x",Bang ty) ++ u') u
--    thisNode <- boson this
--    specialEdge thisNode tn ty
--    forM un $ \un' ->
--      specialEdge thisNode un' (Bang ty)
--    return (tns++uns)
-- toGraphPart te e this@(Alias True x w' s) = do
--   let (v0,(w,~(Bang tyA)):v1) = splitAt x e
--   (n:n1,n':n2) <- splitAt (x+1) <$> toGraphPart te ((w,Bang tyA):v0++(w',Bang tyA):v1) s
--   thisNode <- boson this
--   specialEdge thisNode n (Bang tyA)
--   specialEdge thisNode n' (Bang tyA)
--   return (n1++thisNode:n2)
-- toGraphPart te e this@(Par True _ v v' x s t) = do
--   let (v0,(_w,(vt :|: vt')):v1) = splitAt x e
--   sn0 <- toGraphPart te (v0++[(v,vt)]) s
--   let sn = init sn0
--       s' = last sn
--   (t':tn) <- toGraphPart te ((v',vt'):v1) t
--   thisNode <- boson this
--   specialEdge thisNode s' vt
--   specialEdge thisNode t' vt'
--   return (sn++thisNode:tn)
-- toGraphPart te e s@(Cross True _ v v' x t) = do
--   let (v0,(_w,(vt :*: vt')):v1) = splitAt x e
--   tn <- toGraphPart te (v0++(v,vt):(v',vt'):v1) t
--   let (ts0,(t1:t2:ts3)) = splitAt x tn
--   thisNode <- boson s
--   specialEdge thisNode t1 vt
--   specialEdge thisNode t2 vt'
--   return (ts0++thisNode:ts3)
-- toGraphPart _te e s = do
--   comment $ "  fermion"
--   thisNode <- fermion s
--   return $ replicate (length e) thisNode


-- toGraphMain :: IdDeriv -> GGen ()
toGraphMain d@(Deriv _ (SeqCtx te e) s) = do
  toGraphPart s
  forM_ e $ \(x ::: ty :^ _) -> do
    n <- hypothesis
    n' <- look x
    edge n n' ty

-- getNodeLab :: IdSeq -> Attribute
getNodeLab (S0 (What (Huh x) _ _)) = Label $ StrLabel $ T.pack $ dotQuote x
getNodeLab s = Label $ StrLabel $ T.pack $ dotQuote (seqLab s)
-- getNodeLab _ = emptyLab

-- toGraph :: IdDeriv -> 
toGraph d = DotGraph False True Nothing w

  where (_,_,w) = runRWS tg () (M.empty,1)
        tg = do
          tell1 $ GA $ GraphAttrs [RankSep [0.1], RankDir FromLeft]
          tell1 $ GA $ EdgeAttrs [Len 0.1]

                --       ,"node[fixedsize=True,width=0.5]"
                --        ,"graph[start=2];" -- Seed
--               ,"minlen=0.1;"
                --      ,  "size=5;"
               -- ,"edge [arrowhead=\"vee\",dir=\"forward\"];"]
          toGraphMain d

-- couplingDiag :: IdDeriv -> TeX
couplingDiag = element . graph Dot . toGraph . deriv0

-- (jyp-set-file-local-variable 'flycheck-haskell-ghc-executable "nix-ghc")
-- (flycheck-compile 'haskell-ghc)

-- Local Variables:
-- flycheck-haskell-ghc-executable: "nix-ghc"
-- End:
