{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TransformListComp #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternGuards, OverloadedStrings, RecordWildCards #-}
module Lint (checkCtx, checkCtxDeriv) where

import LL hiding ((&))
import LL.Traversal
import Type.Ctx
import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import Data.Default
import Control.Monad.State
import Control.Monad.Writer
import Control.Monad.RWS
import Text.PrettyPrint hiding ((<>))
import GHC.Exts
import Data.Traversable as T
import Control.Monad.Except.Extra
import Development.Placeholders
import Control.Lens hiding (Fold)
import Text.Show.Pretty

type CtxM = Either String 
type CtxRM = ExceptT String (Writer [String]) 

runCtxRM :: CtxRM a -> CtxM a
runCtxRM m =
  let res = runWriter $ runExceptT $ m  in
  runExcept $ withExcept (intercalate "\n") $ case res of
    (Right x, [])   -> return x
    (Left msg, log) -> throwError $ ("FATAL:" ++ msg):log
    (_, log)        -> throwError $ log 


checkCtx :: (name ~ ref, Ord name, Show name) => Seq' name ref -> CtxM (Seqn1 name ref)
checkCtx = runCtxRM . checkCtx' funRemoveIfPossible 
           where

funRemoveIfPossible currentCtx introducedNames = 
               let rest = [ i | i <- introducedNames, i `notElem` currentCtx ] in
               (currentCtx \\ introducedNames, rest)


checkCtxDeriv :: (name ~ ref, Ord name, Show name) => Deriv0 name ref -> CtxM (Deriv1 name ref)
checkCtxDeriv (D0 d) = runCtxRM $ D1 <$> do
  seq1 <- checkCtx' funRemoveIfPossible (d ^. derivSequent)

  let expectedLinear = map erase $ d ^. derivCtx . linear
  let requiredLinear = seq1 ^. _Wrapping Seqn1 . ann 

  assertEq expectedLinear requiredLinear

  return $ d & derivSequent .~ seq1

assertEq :: (Ord ref, Show ref) => [ref] -> [ref] -> CtxRM [ref]
assertEq rs rs' = do
    if not (sort rs == sort rs') then (tellError (nub (rs ++ rs')) $
      "Expected context " ++ show rs ++ ", sequent consumes " ++ show rs' ++ ".")
      else return rs

-- | Error with recovery
tellError :: a -> String -> CtxRM a
tellError recoveryValue msg = do 
    tell [msg]
    return recoveryValue


checkCtx' :: forall name ref. (Ord ref, Show ref, Ord name, Show name) =>
             ([ref] -> [name] -> ([ref],[name])) -> Seq' name ref -> CtxRM (Seqn1 name ref)
checkCtx' symmDiff (Seqn0 seqn0) = foldSeqn defSeqnFold{sseq} $ seqn0
  where
    sseq :: () -> Seq'' (Seqn1 name ref) (Type' name ref) (Size ref) (Sized' name ref) name ref -> CtxRM (Seqn1 name ref)
    sseq () = annotate $ \u -> case u of
      Ax x y           -> return [x,y]
      NCut _ _ _ nms s t -> ((++) <$> (ctx s .- as) <*> (ctx t .- bs))
         where (as,bs,_tys) = unzip3 nms
      Sync{..} -> mconcat <$> T.sequence ([ctx contL .- [xIntr | (_,_,xIntr,_,_) <- syncIntr] ] ++
                                          (flip fmap (maybeToList syncCont) $ \syncCont ->
                                            ctx syncCont  .- [syncWIntr | (_,_,_,Just SyncUpdate{..},_) <- syncIntr]) ++
                                          (flip fmap (maybeToList syncContR) $ \syncContR ->
                                            ctx syncContR .- [syncYIntr | (_,_,_,_,Just SyncRead{..})   <- syncIntr]))

      Halt refs  -> return refs
      Cross _ x a b s -> (x :) <$> (ctx s .- [a,b])
      Par {..} -> (zElim :) <$>
                               ((++) <$> (ctx contL .- [xIntr])
                                     <*> (ctx contR .- [yIntr]))
      Plus {..} -> do
        lCtx <- checkBranch inl
        rCtx <- checkBranch inr
        if not (sort lCtx == sort rCtx)
          then tellError (zElim : (nub $ lCtx ++ rCtx)) $ "case: context uniformity broken (ctx " ++ show lCtx ++ "≠ ctx " ++ show rCtx ++ ")"
          else return $ (zElim : lCtx)
      With _ x _ a s  -> (x :) <$> (ctx s .- [a])
      SOne _ x s        -> return (x : ctx s)
      SZero x xs        -> pure $ x : xs
      SBot x            -> pure [x]
      What{..}          -> pure [ref | ElemLinear ref <- zVars]
      TApp{..}          -> (zElim :) <$>  (ctx cont .- [xIntr])
      TUnpack{..}       -> (zElim :) <$>  (ctx cont .- [xIntr])
      Offer _ x a s     -> ctx s .- [a] >>= assertEmpty >> return [x]
      Demand x a s    -> (x :) <$>  (ctx s .- [a])
      Fold x a s        -> (x :) <$>  (ctx s .- [a])
      Unfold x a s      -> (x :) <$>  (ctx s .- [a])
      Ignore _ x s      -> (x :) <$>  (return $ ctx s)
      IntOp _ z1 z2 x s  -> ([z1,z2] ++) <$> (ctx s .- [x])
      IntCmp _ z1 z2 x s -> ([z1,z2] ++) <$> (ctx s .- [x])
      IntCopy z x y s    -> (z :) <$> (ctx s .- [x,y])
      IntIgnore z s      -> return $ z : ctx s
      IntLit x _ s       -> ctx s .- [x]
      Fix x z _ s        -> ctx s .- [x] >>= assertEmpty >> return [z]
      Alias _ _x a s    -> ctx s .- [a] -- This is why this function cannot be made generic.
      Load _int_x _ a s   -> ctx s .- [a] -- This too!
      Save x _int_a s   -> return $ x : ctx s
      Mem{} -> throwError "checkCtx': Mem"
      Tile a _x s         -> (ctx s .- [a])
      BigPar {..} ->
        ((zElim :) <$> (concat <$> traverse checkBranch (map snd branches))) >>=
            case branches of
              [] -> flip tellError $ "0 branches in BigPar"
              _  -> return
      BigTensor x a s -> (x :) <$> (ctx s .- [a])
      Foldmap {..} -> do
        assertEq fmArr =<< (ctx fmMap .- [fmMapTrg])
        assertEq [] =<< ctx fmMix .- [fmMixL, fmMixR, fmMixTrg]
        (fmArr ++) <$> (ctx fmCont .- [fmMon])
      Foldmap2 {..} -> do
        assertEq fmArr =<< (ctx fmMap .- [fmMapTrg])
        assertEq [] =<< ctx fmMix .- [fmMixL, fmMixR, fmMixTrg]
        assertEq [] =<< ctx fmUnit .- [fmUnitTrg]
        (fmArr ++) <$> (ctx fmCont .- [fmMon])
      -- (Mem a b _ x _ s t) -> x : (ctx s ++ ctx t) .- [a,b] -- CHECK ???
      Split x _sz a b s -> (x :) <$> (ctx s .- [a,b]) 
      Merge x y _sz a s -> ([x,y]++) <$> (ctx s .- [a])
      Permute {..} -> (zElim :) <$> (ctx cont .- [xIntr])
  
      BigSeq steps ->
         do
            -- Consume sequences. Each sequence is consumed only once in each step
            ctxSequenceRefs <- (nub . join) <$> T.forM steps (\t -> T.sequence
                             [ do
                                when (length elem > 1) $
                                   throwError $ "Sequence " ++ show (the ref) ++ " used more than once in step " ++
                                                ppShow (pruneStep t) ++ "."
                                return (the ref)
                             | ref <- seqStepRefs t
                             , let elem = ()
                             , then group by ref using groupWith 
                             ])

             -- Steps may consume elements from the context 
            ctxStep <- join <$> T.sequence [ ctx seqProg .- seqStepNames t
                                           | t <- steps
                                           , seqProg <- maybeToList (seqStepSeq t) ]

            return $ ctxSequenceRefs ++ ctxStep
 
      -- SizeCmp requires complete coverage of all cases, in one way or the other.
      SizeCmp{bCmp = [],..} -> 
        tellError [] $ "sizeCmp: Incomplete coverage. Missing case: " ++ show CmpTrue ++ "."

      SizeCmp{..} -> do
        let ctxs = [(op,sort $ ctx p) | (op,p) <- bCmp]
        
        let ctx_recovery = foldl1 union [ctx | (_,ctx) <- ctxs]
        let f (o₁,ctx₁) (o₂,ctx₂) = if (ctx₁ == ctx₂) then return (o₁ `joinCmp` o₂,ctx₁)
              else tellError (o₁ `joinCmp` o₂, ctx₁ `union` ctx₂) $
                 "sizeCmp: context uniformity broken (ctx " ++ show o₁ ++ "≠ ctx " ++ show o₂ ++ ")." 
 
        (\(y:ys) -> do
           (op,ctx) <- foldM f y ys
           case op of
             CmpTrue -> return ctx 
             _       -> tellError ctx $ "sizeCmp: Incomplete coverage. Missing case: " ++ show (negCmp op) ++ "."
         ) ctxs
      _ -> 
         tellError [] ("lint: unsupported constructor : " ++ show u)
  
    annotate fctx u' = Seqn1 <$> do
      let u = fmapSeq'' defSeqFmap{ fseq = seqn1 } u'
      ctx <- fctx u
      return $ Seqn ctx u

    checkBranch Branch{..} = ctx branchProg .- [branchName]
    ctx = view ann
    x .- y = removeAndCheck x y
    -- eq :: [ref] -> [ref] -> CtxM ()
    assertEmpty r =
      case r of
        [] -> return r
        is -> throwError $ "Offer or fix linear ctx:\n" ++ unwords (map (show) is)
    
    withPartitionedContext :: (MonadState [e] m) => (e -> Bool) -> m a -> m a
    withPartitionedContext pred m = do
      rest <- state $ partition (not . pred)
      a <- m
      modify (++ rest)
      return a
    
    ref `elemNames` names = case [ref] `removeIfPossible` names of
      ([], _) -> True
      _       -> False

    removeIfPossible = symmDiff 

    removeAndCheck' :: [name] -> [ref] -> CtxRM [name]
    names' `removeAndCheck'` refs' = do
      let (refs, names) = refs' `removeIfPossible` names'
      case refs of
        [] -> return names
        is -> tellError names $ "Context linearity broken:\n" ++ unwords (map (show) is)

      
    removeAndCheck :: [ref] -> [name] -> CtxRM [ref]
    refs' `removeAndCheck` names'  = do
      let (refs, names) = refs' `removeIfPossible` names'
      case names of
        [] -> return refs
        is -> tellError refs $ "Context linearity broken:\n" ++ unwords (map (show) is)

instance Erase (Seqn1 name ref) (Seqn0 name ref) where
  erase (Seqn1 Seqn{_seq}) = S0 (fmapSeq'' defSeqFmap{fseq = seqn0 . erase . Seqn1} _seq)

instance Erase (Deriv1 name ref) (Deriv0 name ref) where
  erase = D0 . (derivSequent %~ erase) . deriv1
