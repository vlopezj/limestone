{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE TupleSections #-}
-- | Renderings of the derivation rules with metavariables
module Rules where

import MarXup()
-- import MarXup.Tex
-- import MarXup.Latex
import LL
-- import Pretty
-- import Symheap
import Numeric (showIntAtBase)
import Fresh
import Data.Bitraversable
import Tagged
import Control.Lens
import Control.Applicative
import Data.List
import Data.Default

mkDeriv a b c seq = runFreshM $ mkDerivM a b c seq

mkDerivM :: (MonadFresh m) => Name -> [(Name,Sized' Name Name)] -> [(Name, Sized' Name Name)] -> Seq' Name Name -> m (Deriv' Id Id)
mkDerivM a b c seq = fromNamesLiberal $ D0 $ Deriv a (seqctx b c) seq

{-
axCut = mkDeriv "cutAx"
    theta [("i",neg $ meta "A"),gamma]
        (cut1 "x" "y" (neg (meta "A"))
             (Ax "i" "x")
             (whatB `using` gamma `using'` "y")
        )


cutParTensor f g = mkDeriv "cutParTensor"
    theta [gamma,delta,xi] (cut1 "x" "y" (neg (f (meta "A") :*: g (meta "B")))
                             (Cross False "i" "j" "x" (whatA `using` gamma))
                             (Par False "u" "v" "y"
                                (whatB `using` delta)
                                (whatC `using` xi)
                             )
                           )

cutPlusWith f g b = mkDeriv "cutPlusWith"
    theta [gamma,delta,xi] (cut1 "x" "y" (neg (f (meta "A") :+: g (meta "B")))
                             (With False "i" b "x" (whatA `using` gamma))
                             (Plus "u" "v" "y"
                                (whatB `using` delta)
                                (whatC `using` xi)
                             )
                           )
                           -}

axRule        = mkDeriv "axRule"     theta [("x",unit tA),("y",unit $ neg tA)] (S0 $ Ax "x" "y")

axPosRule        = mkDeriv "axRule"     theta [("x",unit tP),("y",unit $ neg tP)] (S0 $ Ax "x" "y")



cutRuleSz sz       = mkDeriv "cutRule"    (theta ++ sizeCtx [sz]) [gamma,deltaSz sz] (cutNr sz "x" "y" (neg tA)
                                                            (whatA `using` gamma `using'` "x")
                                                            (whatB `using` delta `using'` "y")
                                                                   )

cutRulePosSz sz     = mkDeriv "cutRule"    (theta ++ sizeCtx [sz]) [gammaSz sz,delta] (cutNl sz "x" "y" (tP)
                                                            (whatA `using` gamma `using'` "x")
                                                            (whatB `using` delta `using'` "y")
                                                                   )

cutRuleNegSz sz     = mkDeriv "cutRule"    (theta ++ sizeCtx [sz]) [gammaSz sz,delta] (cutNl sz "x" "y" (tN)
                                                            (whatA `using` gamma `using'` "x")
                                                            (whatB `using` delta `using'` "y")
                                                                   )

fuseRuleSz sz       = mkDeriv "cutRule"    (theta ++ sizeCtx [sz])
                        [gamma,deltaSz sz] (fuseNr sz "x" "y" (neg tA)
                                                            (whatA `using` gamma `using'` "x")
                                                            (whatB `using` delta `using'` "y")
                                                                   )

commutCut sz       = mkDeriv "cutRule"    (theta ++ sizeCtx [sz]) [gamma,("w", tC :^ sz),deltaSz sz] (fuseNr sz "x" "y" (neg tA)
                                                            (whatA `using` gamma `using'` "x")
                                                            (whatB `using` delta `using'` "y"  `using'` "w")

                                                                   )
cutRule = cutRuleSz sizeOne
cutRuleN = cutRuleSz szN
cutRulePosN = cutRulePosSz szN
cutRuleNegN = cutRuleNegSz szN
fuseRule = fuseRuleSz sizeOne
fuseRuleN = fuseRuleSz szN
mixRule       = mkDeriv "mixRule"    theta [gamma,delta]
                (S0$ NCut Manual FanLeft sizeOne []
                   (r0$ whatA `using` gamma)
                   (r0$ whatB `using` delta))
bicutRule     = mkDeriv "bicutRule"    theta [gamma,delta]  $
                (S0$ NCut Manual FanLeft sizeOne [("x", "u", meta "A"),("y","v",meta "B")]
                 (r0$ whatA `using` gamma `using'` "x" `using'` "y")
                 (r0$ whatB `using` delta `using'` "u" `using'` "v"))

haltRule      = mkDeriv "haltRule" [] [] (S0$ Halt [])

crossRule' β = mkDeriv "crossRule"  theta [gamma, ("z",(T (meta "A" :*: meta "B")) :^ sz)] (S0$ Cross β "z" "x" "y" (r0$ whatA `using` gamma `usings` ["x","y"]))
  where sz = sizeOne
crossRule = crossRule' False

parRule'' β a b = mkDeriv "parRule" theta [gammaSz sz, ("z",(T (a :|: b)) :^ sz),delta]
                                             (S0$ Par β "z"
                                                    "x" (r0$ whatA `using` gamma `using'` "x")
                                                    "y" (r0$ whatB `using` delta `using'` "y")
                                             )
 where sz = sizeOne

parRule'      = parRule'' False
 
parRule       = parRule' tA tB

parRuleX = mkDeriv "parRule" theta [gamma, ("g",tX :^ 1), ("z",(T (tA :|: tB)) :^ 1),delta]
                                             (S0$ Par False "z"
                                                    "x" (r0$ whatA `using` gamma `using'` "x" `using'` "g")
                                                    "y" (r0$ whatB `using` delta `using'` "y")
                                             )


withRule'' β a b ch = mkDeriv "withRule"   theta [gamma,("z",(T (a :&: b)) :^ sizeOne)]
                      (S0$ With β "z" ch "x" (r0$ whatA `using` gamma `using'` "x"))

withRule'     = withRule'' False

withRule      = withRule' tA tB

plusRule      = mkDeriv "plusRule"   theta [gamma,("z",(T (meta "A" :+: meta "B")) :^ sizeOne)]
                (S0$
                 Plus "z" (Branch "x" (r0$ whatA `using` gamma `using'` "x"))
                          (Branch "y" (r0$ whatB `using` gamma `using'` "y")))

sizeCmpRule      = mkDeriv "sizeCmpRule"   (theta ++ sizeCtx [szN, szM]) [gamma]
                    (S0$
                     SizeCmp szN szM  [(Ge, r0$ whatA `using` gamma)
                                      ,(Le, r0$ whatB `using` gamma)])

oneRule' β    = mkDeriv "oneRule"    theta [gamma,("x",T One :^ sizeOne)] (S0$ SOne β "x" (r0$ whatA `using` gamma))
oneRule       = oneRule' False

zeroRule      = mkDeriv "zeroRule"   theta [gamma,("x",T Zero :^ sizeOne)] (S0$ SZero "x" [fst gamma])

botRule       = mkDeriv "botRule"    theta [("x",T Bot :^ sizeOne)] (S0$ SBot "x")

-- forallRuleVar f g  = mkDeriv "forallRule" theta [gamma,("z",Forall "α" Type (f (var "α")))] $ TApp True "x" "z" (g (meta "B")) whatA
forallRule'' β a b = mkDeriv "forallRule-2" theta [gamma,("z",T (Forall "α" (T TType) a):^sizeOne)] $ (S0$ TApp β "z" b "x" (r0 $ whatA `using` gamma `using'` "x"))
forallRule'        = forallRule'' False
forallRule         = forallRule' tAofAlpha tB

forallRuleSz = mkDeriv "forallRule-n" (theta++sizeCtx [szN]) [gamma,("z",T (Forall "α" (T TSize) tAofAlpha):^sizeOne)] $ (S0$ TApp False "z" (T$ SizeT szN) "x" (r0 $ whatA `using` gamma `using'` "x"))
existsRuleSz = mkDeriv "existsRule-n" (theta++sizeCtx []) [gamma,("z",T (Exists "α" (T TSize) tAofAlpha):^sizeOne)] $ (S0$ TUnpack "z" (T TSize) "n" "x" (r0$ whatA `using` gamma `using'` "x"))
                  
forallRulePred = mkDeriv "forallRule-p" (theta++sizeCtx [szN]++[("q",T (TProp (szN,Le,szM)) :^ 1)]) [gamma,("z",T (Forall "p" (T (TProp (szN,Le,szM))) tA):^sizeOne)] $ (S0$ TApp False "z" (T$ Prop) "x" (r0 $ whatA `using` gamma `using'` "x"))
existsRulePred = mkDeriv "existsRule-p" (theta++sizeCtx [szN,szM]) [gamma,("z",T (Exists "p" (T (TProp (szN,Le,szM))) tA):^sizeOne)] $ (S0$ TUnpack "z" (T (TProp (szN,Le,szM))) "p" "x" (r0$ whatA `using` gamma `using'` "x"))
 
existsRule' f   = mkDeriv "existsRule'" theta [gamma,("z",T (Exists "α" (T TType) (f tAofAlpha)):^sizeOne)] $ (S0$ TUnpack "z" (T TType) "β" "x" (r0$ whatA `using` gamma `using'` "x"))
existsRule      = existsRule' id

existsRuleVar   = mkDeriv "existsRuleVar" theta [gamma,("z",T (Exists "α" (T TType) (var "α")):^sizeOne)] $ (S0$ TUnpack "z" (T TType) "β" "x" (r0$ whatA `using` gamma `using'` "x"))

questRule'' β a = mkDeriv "questRule"  theta [("z",T (Quest a) :^ sizeOne)] $ (S0$ Offer β "z" "x" (r0 whatA))
questRule'      = questRule'' False
questRule       = questRule' tA

saveRule      = mkDeriv "saveRule"   theta                       [gamma,("z",T (Bang (meta "A")):^sizeOne)] $ S0$ Save "z" "x" (r0$ whatA `using` gamma)
loadRule      = mkDeriv "loadRule"   (theta ++ [("z",meta "A":^sizeOne)]) [gamma]                       $ S0$ Load "z" sizeOne "x" (r0$ whatA `using` gamma `using'` "x")

splitRule = mkDeriv "splitRule" (theta ++ sizeCtx [szN,szM]) [gamma,("z",(meta "A"):^(sizeAdd szN szM))] $ S0$ Split "z" szN "x" "y" $ (r0$ whatA `using` gamma `usings` ["x","y"] )
mergeRule = mkDeriv "mergeRule" (theta ++ sizeCtx [szN,szM]) [gamma,("z",(meta "A"):^szM),("y",(meta "A"):^szN)] $ S0$ Merge "z" "y" szM "x" $ (r0$ whatA `using` gamma `using'` "x")

bigTensorRule = mkDeriv "bigTensorRule" (theta ++ sizeCtx [szM]) [gamma,("z",bigTensor szM (meta "A"):^sizeOne)] $ S0$ BigTensor "z" "x" $ (r0$ whatA `using` gamma `using'` "x") 
bigParRuleSimpl = mkDeriv "bigParRuleSimpl" (theta ++ sizeCtx [szN]) [gammaN,("z",bigPar szN (meta "A"):^ sizeOne)] $ S0$ BigPar
               { zElim = "z", branches = [(szN,Branch "x" (r0$ whatA `using` gammaN `using'` "x"))]}
bigParRule = mkDeriv "bigParRule" (theta ++ sizeCtx [szN,szM]) [gammaN,deltaM,("z",bigPar (sizeAdd szN szM) (meta "A"):^ sizeOne)] $ S0$ BigPar
               { zElim = "z", branches = [(szN,Branch "x" (r0$ whatA `using` gammaN `using'` "x")),
                                          (szM,Branch "y" (r0$ whatB `using` deltaN `using'` "y"))]}

bigParRuleX = mkDeriv "bigParRuleX" (theta ++ sizeCtx [szN,szM])
              [gammaN, ("g",tX :^ 1), ("z",bigPar (szN + szM + 1) tA :^ 1), deltaM]
                                             (S0$ BigPar {
                                                    zElim = "z"
                                                   ,branches = [(szN, Branch "w" (r0$ whatA `using'` "w" `using` gamma))
                                                               ,(1,   Branch "x" (r0$ whatB `using'` "x" `using'`  "g"))
                                                               ,(szM, Branch "y" (r0$ whatC `using` delta `using'` "y"))
                                                               ]
                                                   })


freezeRule' ty = mkDeriv "freezeRule" (theta ++ sizeCtx [szN]) [gamma,delta] $ freezeSeq1 1 szN ty "x" "y" (whatA `using` gamma `using'` "x") (whatB `using` delta `using'` "y")

freezeRule = freezeRule' tD
             
freezeKRule = mkDeriv "freezeRule" (theta ++ sizeCtx [szK,szN]) [gamma,delta] $ freezeSeq1 szK szN tD "x" "y" (whatA `using` gamma `using'` "x") (whatB `using` delta `using'` "y")

freezeKRule1 = mkDeriv "freezeRule" (theta ++ sizeCtx [szK]) [gamma,delta] $ freezeSeq1 szK 1 tD "x" "y" (whatA `using` gamma `using'` "x") (whatB `using` delta `using'` "y")

freezeCopiesRuleSz cnt sz = mkDeriv "freezeRule" (theta ++ sizeCtx [cnt,sz]) [gamma,delta] $ freezeSeq1 cnt sz tD "x" "y" (whatA `using` gamma `using'` "x") (whatB `using` delta `using'` "y")


szT :: Size String
szT = var "t"
updateRule' ty = mkDeriv "freezeRule" (theta ++ sizeCtx [szN,szM]) [gamma,delta] $ S0$ Sync
               [(1, ty, "x", Just (SyncUpdate szM "y"), Nothing)]
               (r0$ whatA `using` gamma `using'` "x")
               (Just (r0$ whatB `using` delta `using'` "y"))
               Nothing
updateRule = updateRule' tD                                                                                      

{-
foldmapRule = mkDeriv "foldmapRule" (("n",Index True:^sizeOne):theta) [gamma,("?Δ", meta "Δ":^(SizeExp szN))] $
              Foldmap "n" (metaSyntactic True True "B" [var "n"]) ["?Δ"] "y" (whatA) "x" "y" "w" (whatB) "z" (whatC `using` gamma)
-}
foldmap2Rule = mkDeriv "foldmap2Rule" (theta ++ sizeCtx [szN]) [gamma,deltaN] $
              S0$ Foldmap2  ["?δ"] szN tB "v" (r0$ whatE `using'` "v") "u" (r0$ whatA `using` delta `using'` "u") "x" "y" "w" (r0$ whatB `usings` ["x","y","w"]) "z" (r0$ whatD `using` gamma `using'` "z")

bigSeqRule = runFreshM$ do
  seqAN <- bigSeqPol szN tA
  seqBM <- bigSeqPol szM tB
  seqC <- bigSeqPol (szN + szM) tC

  mkDerivM "bigSeqRule" (theta ++ sizeCtx [szN,szM])
                                                  [gammaN,deltaM
                                                  ,("x₁", seqAN :^ 1)
                                                  ,("x₂", seqBM :^ 1)
                                                  ,("y₁", seqC :^ 1)
                                                  ] $
    S0$ BigSeq $ 
       [SeqStep {
          seqSz = szN,
          seqChunk = [("y₁","y"),("x₁","x")],
          seqProg = r0$ whatA `using'` "y" `using'` "x" `using` gamma
        },
        SeqStep {
          seqSz = szM,
          seqChunk = [("y₁","y"),("x₂","x")],
          seqProg = r0$ whatB `using'` "y" `using'` "x" `using` delta
        }
       ]

bigSeqRulePos = runFreshM$ do
  let seqAN = bigSeqPlus szN tA
  let seqAM = bigSeqPlus szM tA
  let seqB = bigSeqPlus (szN + szM) tB

  mkDerivM "bigSeqRule" (theta ++ sizeCtx [szN,szM])
                                                  [gammaN,deltaM
                                                  ,("x₁", seqAN :^ 1)
                                                  ,("x₂", seqAM :^ 1)
                                                  ,("y₁", seqB :^ 1)
                                                  ] $
    S0$ BigSeq $ 
       [SeqStep {
          seqSz = szN,
          seqChunk = [("y₁","y"),("x₁","x")],
          seqProg = r0$ whatA `using'` "y" `using'` "x" `using` gamma
        },
        SeqStep {
          seqSz = szM,
          seqChunk = [("y₁","y"),("x₂","x")],
          seqProg = r0$ whatB `using'` "y" `using'` "x" `using` delta
        }
       ]

bigSeqRuleNeg = runFreshM$ do
  let seqAN = bigSeqPlus szN tA
  let seqAM = bigSeqMinus szM tA
  let seqB = bigSeqPlus (szN + szM) tB

  mkDerivM "bigSeqRule" (theta ++ sizeCtx [szN,szM])
                                                  [gammaN,deltaM
                                                  ,("x₁", seqAN :^ 1)
                                                  ,("x₂", seqAM :^ 1)
                                                  ,("y₁", seqB :^ 1)
                                                  ] $
    S0$ BigSeq $ 
       [SeqStep {
          seqSz = szN,
          seqChunk = [("y₁","y"),("x₁","x")],
          seqProg = r0$ whatA `using'` "y" `using'` "x" `using` gamma
        },
        SeqStep {
          seqSz = szM,
          seqChunk = [("y₁","y"),("x₂","x")],
          seqProg = r0$ whatB `using'` "y" `using'` "x" `using` delta
        }
       ]



bigSeqSimpleRule = runFreshM$ do
  seqA <- bigSeqPol szN tA
  seqB <- bigSeqPol szN tB
  mkDerivM "bigSeqRule" (theta ++ sizeCtx [szN])
                                                  [gammaN
                                                  ,("x", seqA :^ 1)
                                                  ,("y", seqB :^ 1)
                                                  ] $
    S0$ BigSeq $ 
       [SeqStep {
          seqSz = szN,
          seqChunk = [("x","x'"),("y","y'")],
          seqProg = r0$ whatA `using'` "x'" `using'` "y'" `using` gamma
        }
       ]

bigSeqSimpleRulePos = runFreshM$ do
  let seqA = bigSeqPlus szN tA
  let seqB = bigSeqPlus szN tB
  mkDerivM "bigSeqRule" (theta ++ sizeCtx [szN])
                                                  [gammaN
                                                  ,("x", seqA :^ 1)
                                                  ,("y", seqB :^ 1)
                                                  ] $
    S0$ BigSeq $ 
       [SeqStep {
          seqSz = szN,
          seqChunk = [("x","x'"),("y","y'")],
          seqProg = r0$ whatA `using'` "x'" `using'` "y'" `using` gamma
        }
       ]

bigSeqSimpleRuleNeg = runFreshM$ do
  let seqA = bigSeqMinus szN tA
  let seqB = bigSeqPlus szN tB
  mkDerivM "bigSeqRule" (theta ++ sizeCtx [szN])
                                                  [gammaN
                                                  ,("x", seqA :^ 1)
                                                  ,("y", seqB :^ 1)
                                                  ] $
    S0$ BigSeq $ 
       [SeqStep {
          seqSz = szN,
          seqChunk = [("x","x'"),("y","y'")],
          seqProg = r0$ whatA `using'` "x'" `using'` "y'" `using` gamma
        }
       ]

bigSeqSimpleRulePol3 = runFreshM$ do
  let seqA = bigSeqMinus szN tA
  let seqB = bigSeqPlus szN tB
  let seqC = bigSeqPlus szN tC
  mkDerivM "bigSeqRule" (theta ++ sizeCtx [szN])
                                                  [gammaN
                                                  ,("x", seqA :^ 1)
                                                  ,("y", seqB :^ 1)
                                                  ,("z", seqC :^ 1)
                                                  ] $
    S0$ BigSeq $ 
       [SeqStep {
          seqSz = szN,
          seqChunk = [("x","x'"),("y","y'"),("z","z'")],
          seqProg = r0$ whatA `using'` "x'" `using'` "y'" `using'` "z'" `using` gamma
        }
       ]




dataCrossRule = mkDeriv "data⊗" theta [("a", unit $ T (tA :*: tB)),
                                       ("b", unit $ dual $ bigTensor szK $ T (tA :*: tB))] $
               (cut1 "w" "z" (T (bigTensor szK tA :*: bigTensor szK tB)) 
                  (S0$ Cross False "a" "a₁" "a₂"
                    (S'$ Par False "w"
                      "w₁" (S'$ What "data" (ElemLinear <$> ["a₁","w₁"]) [])
                      "w₂" (S'$ What "data" (ElemLinear <$> ["a₂","w₂"]) [])))

                  (S0$ Cross False "z" "z₁" "z₂" $
                     S'$ BigTensor "z₁" "z₁'" $
                     S'$ BigTensor "z₂" "z₂'" $
                     S'$ BigPar "b" [(szK, Branch "b'" $
                                       S'$ Par False "b"
                                           "b₁'" (S'$ Ax "z₁'" "b₁'")
                                           "b₂'" (S'$ Ax "z₂'" "b₂'"))]))

dataPlusRule = mkDeriv "data⊕" theta [("a", unit $ T (tA :+: tB)),
                                       ("b", unit $ dual $ bigTensor szK $ T (tA :+: tB))] $
               (cut1 "w" "z" (T (bigTensor szK tA :+: bigTensor szK tB))
                  (S0$ Plus "a"
                     (Branch "a₁" (S'$ With False "w" Inl "w₁" (S'$ What "data" (ElemLinear <$> ["a₁","w₁"]) [] )))
                     (Branch "a₂" (S'$ With False "w" Inr "w₂" (S'$ What "data" (ElemLinear <$> ["a₂","w₂"]) [] ))))
                  (S0$ Plus "z"
                     (Branch "z₁"
                       (S'$ BigTensor "z₁" "z₁'" $
                         (S'$ BigPar "b" [(szK, Branch "b₁" (S'$ With False "b₁'" Inl "b₁" (S'$ Ax "b₁'" "z₁'")))])))

                     (Branch "z₂"
                        (S'$ BigTensor "z₂" "z₂'" $
                         (S'$ BigPar "b" [(szK, Branch "b₂" (S'$ With False "b₂'" Inr "b₂" (S'$ Ax "b₂'" "z₂'")))])))))



dataOneRule = mkDeriv "data1" theta [("a", unit $ T One),
                                     ("b", unit $ dual $ bigTensor szK (T One))] $
              S0$ SOne False "a" $
              S'$ BigPar "b" [(szK, Branch "b'" $ S'$ SBot "b'")]


permutRule = mkDeriv "permutRule" theta [gamma,("x",meta "A" :^ szN)] $ S0$
             Permute "x" szM "x" $ r0$ whatA `using'` "x" `using` gamma
gamma,delta ::  (Name, Sized' name ref)
gammaSz sz = ("?γ",tgamma :^ sz)
gammaSz' :: Int -> Size ref -> (Name, Sized' name ref)
gammaSz' i sz = ("?γ" ++ show i,tgamma' i :^ sz)
gamma = ("?γ",tgamma :^ sizeOne)
gamma' i = gammaSz' i sizeOne
gamma1 = gamma' 1
gamma2 = gamma' 2
gammaN1 = gammaSz' 1 szN
gammaN2 = gammaSz' 2 szN

gammaMN = ("?γ",tgamma :^ (sizeMul szM szN))
gammaMpN = ("?γ",tgamma :^ (sizeAdd szM szN))
gamma2N = ("?γ",tgamma :^ SizeExp szN)
gammaN = ("?γ",tgamma :^ szN)
gammaM = ("?γ",tgamma :^ szM)
gammaM' i = gammaSz' i szM
gammaM1 = gammaM' 1
gammaM2 = gammaM' 2
gammaNM = ("?γ",tgamma :^ (sizeMul szN szM))
gammaNpP = ("?γ",tgamma :^ (sizeAdd szN szM))
deltaSz sz = ("?δ",tdelta :^ sz)
deltaSz' i sz = ("?δ" ++ show i,tdelta' i :^ sz)
delta = deltaSz sizeOne
delta' i = deltaSz' i sizeOne
delta1 = delta' 1
delta2 = delta' 2
delta2N = ("?δ",tdelta :^ SizeExp szN)
deltaN = ("?δ",tdelta :^ szN)
deltaN1 = deltaN' 1
deltaN2 = deltaN' 2
deltaM1 = deltaM' 1
deltaM2 = deltaM' 2
deltaN' i = ("?δ" ++ show i,tdelta' i :^ szN)
deltaM' i = ("?δ" ++ show i,tdelta' i :^ szM)
deltaM = ("?δ",tdelta :^ szM)
deltaNM' i = deltaSz' i (szN * szM)
deltaNM1 = deltaNM' 1 
deltaNM2 = deltaNM' 2 
deltaNM = ("?δ",tdelta :^ (sizeMul szN szM))
deltaMN = ("?δ",tdelta :^ (sizeMul szM szN))
deltaNpM = ("?δ",tdelta :^ (sizeAdd szN szM))
deltaMpN = ("?δ",tdelta :^ (sizeAdd szM szN))

gammaBang = ("?γ",Bang tgamma)
xi    = ("?ξ",txi:^sizeOne)
xi1 = xi' 1
xi2 = xi' 2
xiN    = ("?ξ",txi:^ szN)
xiSz sz    = ("?ξ",meta "Ξ":^ sz)
xiSz' i sz = ("?ξ" ++ show i,txi' i :^ sz)
xiM = xiSz szM
xiM' i = xiSz' i szM
xiM1 = xiM' 1
xiM2 = xiM' 2
xiN' i = xiSz' i szN
xiN1 = xiN' 1
xiN2 = xiN' 2
xiNM    = ("?ξ",meta "Ξ" :^ (sizeMul szN szM))
xiMN    = ("?ξ",meta "Ξ" :^ (sizeMul szM szN))
gamma', delta', xi' :: Int -> CtxElem Name (Sized' name ref)
xi' i = xiSz' i sizeOne

eta    = ("?η",meta "Η":^sizeOne)
etaN   = ("?η",meta "Η":^szN)

theta = [("?Θ",ttheta :^ sizeOne)]

tgamma ::  Type' name ref
tgamma = meta "Γ"
tgamma', tdelta', txi' :: Int -> Type' name ref
tgamma' i = meta $ "Γ" ++ subscriptShow i 
tgamma1 = tgamma' 1
tgamma2 = tgamma' 2
tdelta ::  Type' name ref
tdelta = meta "Δ"
tdelta' i = meta $ "Δ" ++ subscriptShow i
tdelta1 = tdelta' 1
tdelta2 = tdelta' 2
ttheta :: Type' name ref
ttheta = meta "Θ"
txi =  meta "Ξ"
txi' i =  meta $ "Ξ" ++ subscriptShow i
txi1 = txi' 1
txi2 = txi' 2

whatA,whatB,whatC,whatD,whatE,whatF,whatG,whatH,whatI,whatJ :: Seqn0 name ref
whatA = Seqn0 whatA'
whatA' = Seqn def (What "a" [] [])
whatB = S0$ What "b" [] []
whatC = S0$ What "c" [] []
whatD = S0$ What "d" [] []
whatE = S0$ What "e" [] []
whatF = S0$ What "f" [] []
whatG = S0$ What "g" [] []
whatH = S0$ What "h" [] []
whatI = S0$ What "i" [] []
whatJ = S0$ What "j" [] []



infixl `using`
infixl `using'`

s `using` (x,_t) = s `using'` x

s `usings'` xs = s `usings` [x | (x,_t) <- xs]
x `using'` y = x `usings` [y]

S0 (What nm xs ys) `usings` xs' = S0$ What nm (xs ++ [ElemLinear x | x <- xs']) ys
s                  `usings` _ = s



szN,szM,szP,szK :: Size String
szN = sizeVar "n"
szN1 = sizeVar "n₁"
szN2 = sizeVar "n₂"
szM1 = sizeVar "m₁"
szM2 = sizeVar "m₂"
szM = sizeVar "m"
szP = sizeVar "p"
szK = sizeVar "k"
szNM = szN * szM
szNP = szN * szP

tA,tB,tC,tD,tD1,tD2 :: Type' n r
tD' :: Int -> Type' n r
tA = meta "A"
tB = meta "B"
tC = meta "C"
tD = meta "D"
tX = meta "X"
tD' i = meta $ "D" ++ subscriptShow i
tD1 = tD' 1
tD2 = tD' 2

tN,tM,tP,tQ :: Type' n r
tN = metaSyntactic False True "N" []
tM = metaSyntactic False True "M" []
tP = meta "P"
tQ = meta "Q"

tN' :: IdType
tN' = tN

tR :: IdType
tR = meta "r"

tA',tB' :: IdType
tA' = tA
tB' = tB

tPofAlpha    = metaSyntactic True True "P" [var "α"]
tPofAlphaNeg = metaSyntactic True True "P" [neg $ var "α"]
tNofAlpha    = metaSyntactic False True "N" [var "α"]
tNofAlphaNeg = metaSyntactic False True "N" [neg $ var "α"]
tAofAlpha    = metaSyntactic True True "A" [var "α"]
tAofAlphaNeg = metaSyntactic True True "A" [neg $ var "α"]
tAofB        = metaSyntactic True True "A" [tB]
tAofN    = metaSyntactic True True "A" [var "n"]
tAofNNeg = metaSyntactic True True "A" [neg $ var "n"]

scriptShow ::  (Integral a, Show a) => [Char] -> a -> [Char]
scriptShow []             _ = error "scriptShow on empty list"
scriptShow (minus:digits) x = if x < 0 then minus : sho (negate x) else sho x
  where sho z = showIntAtBase 10 (\i -> digits !! i) z []

subscriptShow :: Int -> String
subscriptShow  = scriptShow "-₀₁₂₃₄₅₆₇₈₉"

{-
bangRule = Deriv "bangRule" ["Θ"] [gamma,("z",Bang (meta "A"))] $ Demand "x" 1 whatB
questRule β  = Deriv "questRule" ["Θ"] [("?γ",meta "!Δ"),("z",Quest (meta "A"))] $ Offer β "_x" 1 whatA
-}


{-

axRule,botRule,parRule,crossRule,plusRule,zeroRule,existsRule,bangRule,cutRule :: Deriv
axRule     = Deriv "axRule" ["Θ"] [("x",meta "A"),("y",neg (meta "A"))] (Ax 0 1)
cutRule    = Deriv "cutRule" ["Θ"] [gamma,xi] (Cut "x" "y" (meta "A") 1 whatA whatB)
crossRule' β = Deriv "crossRule" ["Θ"] [gamma, ("z",meta "A" :*: meta "B"),delta] (Cross β "x" "y" 1 whatA)
parRule' β  = Deriv "parRule" ["Θ"] [gamma, ("z",meta "A" :|: meta "B"),delta] (Par β "x" "y" 1 whatA whatB)
crossRule = crossRule' False
parRule = parRule' False
withRule :: Boson -> Choice -> Deriv
withRule β b = Deriv "withRule" ["Θ"] [gamma,("z",meta "A" :&: meta "B")] (With  β "x" b 1 whatA)
plusRule   = Deriv "plusRule" ["Θ"] [gamma,("z",meta "A" :+: meta "B")] (Plus "x" "y" 1 whatA whatB)
oneRule  β   = Deriv "oneRule" ["Θ"] [gamma,("x",One)] (SOne  β 1 whatA)
zeroRule   = Deriv "zeroRule" ["Θ"] [gamma,("x",Zero)] (SZero 1 [])
botRule    = Deriv "botRule" ["Θ"] [("x",Bot)] (SBot 0)
forallRule  β = Deriv "forallRule" ["Θ"] [gamma,("z",Forall "α" tAofAlpha)] $ TApp β "x" 1 (meta "B") whatA
existsRule  = Deriv "existsRule" ["Θ"] [gamma,("z",Exists "α" tAofAlpha)] $ TUnpack "β" "x" 1 whatA

-- Note ugly hack: to display diagram properly I make the whole !Δ a string.
questRule,weakenRule,contractRule,oneRule,forallRule,parRule',crossRule' :: Boson -> Deriv
weakenRule β = Deriv "weakenRule" ["Θ"] [gamma,("z",Bang (meta "A"))] $ Ignore β 1 whatA
contractRule β  = Deriv "contractRule" ["Θ"] [gamma,("z",Bang (meta "A"))] $ Alias β 1 "z'" whatA

chanCrossRule = crossRule' True
chanParRule = parRule' True

memRule :: Int -> Deriv
memRule n = Deriv "memRule" ["θ"] [gammaBang,xi] $ Mem "x" "w" tA 1 n whatA whatB
-}


tPrim s = T (Primitive True s)
tPrimA = tPrim "𝔸"

axAtomRule        = mkDeriv "axAtomRule"     theta [("x",unit (tPrimA)),("y",unit $ neg (tPrim "𝔸"))] (S0 $ Ax "x" "y")

freezePrimRule = freezeRule' tPrimA
updatePrimRule = updateRule' tPrimA
