{-# OPTIONS_GHC -w #-} -- suppress errors for now
{-# LANGUAGE OverloadedStrings, GeneralizedNewtypeDeriving, RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternSynonyms #-}
module LLVM where

import LL
import LL.Para
import Type

import Text.PrettyPrint.HughesPJ hiding ((<>))
import Control.Applicative
import Control.Monad.Error
import Control.Monad.State
import Control.Monad.Writer
import Data.List
import Id
import Fresh
import Data.Either

import qualified Data.Map as M

type Err = String
type MyState = ()
newtype LLVM a = LLVM (WriterT [Either Doc Doc] (ErrorT String FreshM) a) deriving
  ( Functor, Applicative, Monad
  , MonadWriter [Either Doc Doc]
  , MonadError Err
  )

prelude :: String
prelude = " \
\declare void @llvm.memcpy.p0i32.p0i32.i32(i32*,i32*,i32,i32,i1) \n\
\ "


make :: IdDeriv -> Either String String
make = runLLVM . compileTop

runLLVM :: LLVM () -> Either String String
runLLVM (LLVM x) = case runFreshM $ runErrorT $ runWriterT x of
  Left err -> Left $ err
  Right ((),code) ->
    let definitions = vcat [x | Left x <- code ]
        stmts = vcat [x | Right x <- code ]
    in Right $ render $ definitions


tellDecl,tellStmt :: Doc -> LLVM ()
tellDecl x = tell [Left x]
tellStmt x = tell [Right x]

isData :: IdType -> Bool
isData (T t) = case t of
   (a :+: b) -> isData a && isData b
   (a :*: b) -> isData a && isData b
   (a :|: b) -> isData a && isData b
   (a :&: b) -> False -- must send a bit.
   (Bang a)  -> True -- encapsulated protocol.
   Zero -> True
   One -> True
   Top -> True
   Bot -> True
   Var _ (TVar b _) -> b -- for now we just assume all variables are > 0
   Exists x _ t -> isData t
   Rec _mu x t -> isData t
   _ -> False

makeTmpReg :: LLVM Doc
makeTmpReg = do
  u <- LLVM $ lift $ lift $ fresh
  return $ nameToReg $ "tmpReg" <> text (show u)

makeLabels :: Int -> LLVM [Doc]
makeLabels 0 = return []
makeLabels n = flip (:) <$> makeLabels (n-1) <*> do
  u <- LLVM $ lift $ lift $ fresh
  return $ "label" <> text (show u)


voidOp :: Doc -> [Doc] -> LLVM ()
voidOp op args = tellStmt $ op <> " " <> (sep $ punctuate comma args)

tellOp :: Doc -> Maybe Doc -> [Doc] -> LLVM Doc
tellOp op target0 args = do
  target <- case target0 of
    Nothing -> makeTmpReg
    Just x -> return x
  voidOp (target <> " = " <> op) args
  return target

binop :: Doc {- ^ operation -} -> Maybe Doc {- ^ target -} -> Doc -> Doc -> LLVM Doc
binop op z x y = tellOp op z [x,y]

add = binop "add i32"
mul = binop "mul i32"

alloca :: Maybe Doc -> Doc -> Doc -> LLVM Doc
alloca target typ sz = do
  tellOp "alloca" target [typ,"i32 " <> sz]

branch :: Doc -> Doc -> Doc -> LLVM ()
branch cond x y = voidOp "br i1" [cond, nameToLabel x, nameToLabel y]

jump :: Doc -> LLVM ()
jump lab = voidOp "br" [nameToLabel lab]

shiftPtr target source shift = do
  tellOp "getelementptr inbounds i32*" (Just $ dataLocation target) [dataLocation source, shift]

x <-- y = shiftPtr x y "i32 0"

size :: Type' a b -> LLVM Doc
size (T t) = case t of
   (a :+: b) -> do x <- size a
                   y <- size b
                   add Nothing "1" =<< (add Nothing x y)
   (a :*: b) -> do x <- size a
                   y <- size b
                   add Nothing x y
   (a :|: b) -> do x <- size a
                   y <- size b
                   add Nothing x y
   Bang a  -> pure "1"
   Zero ->pure "0"
   One -> pure "0"
   Top -> pure "0"
   Bot -> pure "0"
   -- TVar b _ -> -- load ... register
   Exists x _ t -> pure "1"

mkChoice :: Choice -> Doc
mkChoice Inl = "0"
mkChoice Inr = "1"

nameToReg :: Doc -> Doc
nameToReg x = "%" <> x

nameToLabel :: Doc -> Doc
nameToLabel x = "label %" <> x

nameToAddr :: Doc -> Doc
nameToAddr x = "@" <> x

dataLocation :: Id -> Doc
dataLocation x = nameToReg $ text $ "t_" ++ (show $ id_unique x)

{-
staticZone :: Id -> LLVM Doc
staticZone = error "TODO"

saveContext :: [(Id,Type' Id Int)] -> Id -> LLVM ()
saveContext xs target = error "TODO"

restoreContext :: [(Id,Type' Id Int)] -> Id -> LLVM ()
restoreContext xs target = error "TODO"
-}

emitFunction :: Doc -> Doc -> [Doc] -> LLVM Doc -> LLVM ()
emitFunction returnType funName arguments body = do
  (ret,w) <- listen body
  let (decls,stmts) = partitionEithers w
  tell (map Left decls)
  tellDecl ("define "<> returnType <> " " <> funName <> parens (sep $ punctuate comma arguments) <> "{" $$
            nest 2 (vcat stmts) $$
            "ret " <> ret $$
            "}"
           )



emitFunction' :: IdType -> Id -> IdType -> LLVM Doc -> LLVM Doc
emitFunction' returnType argumentName argumentType body = do
  {-
  (ret,b) <- listen body
  fctName <- makeTmpName
  outputFunction $ braces (hcat [body] $$ "ret " <> ret)
  return fctName
  -}
  error "ARSt"
  -- reg <- body
  -- tell ["ret " <> reg]
  -- return newName

call :: Doc {- ^ function -} -> [Doc] -> LLVM Doc
call f args = do
  tmp <- makeTmpReg
  tellStmt $ tmp <> " = call " <> f <> parens (sep $ punctuate comma args)
  return tmp

callVoid :: Doc {- ^ function -} -> [Doc] -> LLVM Doc
callVoid f args = do
  tellStmt $ "call void " <> f <> parens (sep $ punctuate comma args)
  return "void"

label :: Doc -> LLVM ()
label l = tellStmt $ l <> ":"

load :: Id -> LLVM Doc
load x = tellOp "load i32*" Nothing [dataLocation x]

store :: Id -> Doc -> LLVM ()
store x val = voidOp "store i32" [val,"i32* " <> dataLocation x]


copy :: Type' a b -> Id -> Id -> LLVM ()
copy ty source target = do
  let target' = dataLocation target
      source' = dataLocation source
  sz <- mul Nothing "4" =<< size ty
  callVoid "@llvm.memcpy.p0i32.p0i32.i32" ["i32* " <> target',
                                           "i32* " <> source',
                                           "i32 " <> sz,"i32 4","i1 0"]
  return ()

compileTop :: IdDeriv -> LLVM ()
compileTop d@(D0 (Deriv who (seqctx_ -> (ts, vs)) _)) = do
  emitFunction "void" (nameToAddr $ text who) (["i32* " <> dataLocation x | (x,_) <- vs]) $ do
    compile d
    return "void"

compile = foldIdDeriv sf where
  sf :: IdDeriv -> SeqFinal''' Id Id IdType IdSize IdSized (LLVM ())
  sf (D0 (Deriv _who (seqctx_ -> (_ts, vs)) s0)) = SeqFinal {..} where
    ssz _ = id
    stysz _ = id
    sty _ = id
    sxchg _π s = s
    sone False _ t = t
    sbot _ = return ()
    szero _ ctx  = return () -- FIXME: call exit
    sax x y t | isData t  = copy t       x y
              | otherwise = copy (neg t) y x

    scut v v' vs s vt t | isData vt = do
      sz <- size vt
      alloca (Just $ dataLocation v) "i32" sz
      s
      v' <-- v
      t
                        | isData vs = scut v' v vt t vs s
                        | otherwise = throwError "Cut on non-data type"
    swith False zt choice z x xt s = do
      store z (mkChoice choice)
      shiftPtr x z "i32 1"
      s
    splus z x xt y yt sx sy = do
      z' <- load z
      [inl_,inr_,done] <- makeLabels 3
      cmp <- tellOp "icmp eq i32" Nothing [z',"0"]
      branch cmp inl_ inr_

      label inr_
      shiftPtr x z "i32 1"
      sy
      jump done

      label inl_
      shiftPtr y z "i32 1"
      sx
      jump done

      label done



      {-
    scross False z x xt y yt s target
--      | isData xt && isData yt = produce a pair.
      | isData xt = do
        emitFunction (neg yt) x xt $ do
          s y
    spar False z x xt y yt s t target = do
      s x -- TODO: check that the target is in t's context
      call z x y
      t target
-}
