module DsErrors.TH where

import qualified Parser.AbsMx as Abs

thOnly = error "do not use! only for template haskell to get the type."

exprLocUniv :: Abs.Expr -> [(Int,Int)]
exprLocUniv = thOnly

typeLocUniv :: Abs.Type -> [(Int,Int)]
typeLocUniv = thOnly

patLocUniv :: Abs.Pat -> [(Int,Int)]
patLocUniv = thOnly

