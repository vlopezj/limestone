{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TransformListComp #-}
module Ritchie(genCodeC
              ,mkCFile,mkHFile
              ,LangM(..)
              ,CompileEnv(..)
              ,EnvFlags(..)
              ,IdDerivPol(..)
              ,TopLevel) where

import Ritchie.LL
import Ritchie.Polar
import Ritchie.C
import Ritchie.C.Pretty
import Ritchie.Stash
import GHC.Exts

import LL.Core
import Text.PrettyPrint
import Fuel
import Control.Monad.Reader
import Control.Monad.Except.Extra
import Data.Default
import Fresh
import Development.Placeholders
import Control.Monad.Writer
import Data.List

import Text.Heredoc

genCodeC :: IdDerivPol -> FuelT LangM Doc
genCodeC d = lift$ fmap asDoc$ stash$ 
             (translateDeriv d
              (\_ -> unstash sNopA) :: EnvM TVoid)


mkCFile :: [(Doc,[TopLevel])] -> Doc
mkCFile prog = scafoldC $
               vcatBlank $ [
                 text lscc_core
                ,vcat$ map asDoc $ nub
                            [ decl
                            | (_,decls) <- prog 
                            , TopLevelDecl decl <- decls
                            ]
                ,vcatBlank [fun | (fun,_) <- prog]
                ]


mkHFile :: [(Doc,[TopLevel])] -> Doc
mkHFile prog = scafoldH $
               vcat$ map asDoc $ nub
                          [ decl
                          | (_,decls) <- prog 
                          , TopLevelDecl decl <- decls
                          ]

lscc_core = [str|#define alignof(t) offsetof( struct { char x; t dummy; }, dummy)
                |#define size_t_alignof(len,t) size_t_align(len, alignof(t))
                |#define failure(s) do { fprintf (stderr, "Failure: %s at %s (%d)\n", s, __FILE__, __LINE__); } while(false)
                |
                |static inline size_t size_t_max(size_t a, size_t b)        { return (a >= b)?a:b; }
                |static inline size_t size_t_align(size_t len, size_t mask) { return (len + mask) & ~mask; }
                |]
