{-# LANGUAGE TemplateHaskell #-}
module Assoc where

import Control.Monad.Except.Extra
import Data.List
import GHC.Exts
import Is

withFields :: (IsString s, MonadError s m, Show a, Eq a) => [(a,b)] -> [a] -> m ([(a,b)],[b],[(a,b)])
withFields all₀ fields = do
  let (prelude , all₁) = span (not . flip elem fields . fst) all₀
  let (slice   , all₂) = span (flip elem fields . fst)       all₁
  let (epilogue, all₃) = partition (not . flip elem fields . fst) all₂
  
  let nonContiguous = map fst all₃

  when (not $ $(is '[]) nonContiguous) $ throwError $ fromString $ "Fields " ++ show nonContiguous ++ " are non-contiguous."
  
  let sliceFields = map fst slice

  when (sliceFields /= fields) $ throwError $ fromString $ "Expected contiguous fields " ++ show fields ++ 
                                                " in record, got " ++ show sliceFields ++ "."

  return (prelude, map snd slice, epilogue) 
  
  
  


    
