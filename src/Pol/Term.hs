{-# LANGUAGE GeneralizedNewtypeDeriving, TupleSections #-}
module Pol.Term where

import Data.Function
import Data.Monoid
import Data.MultiSet (MultiSet)
import qualified Data.MultiSet as MultiSet
import qualified Data.Map as Map
import Control.Applicative
import Data.Bifunctor
import Data.Biapplicative
import Data.Either

newtype Term ref = Term { getTerm :: MultiSet ref }
                 deriving (Show,
                           Read,
                           Eq,
                           Monoid) 

-- ^ The scalar in a Monomial is non-zero
type Monomial a ref = (a, Term ref)

map :: (Ord s, Ord r) => (r -> s) -> Term r -> Term s
map = liftTerm . MultiSet.map

liftTerm :: (MultiSet r -> MultiSet s) -> Term r -> Term s
liftTerm = (Term .) . (. getTerm) 

liftTermM :: (Functor m) => (MultiSet r -> m (MultiSet s)) -> Term r -> m (Term s)
liftTermM = (fmap Term .) . (. getTerm) 

liftTerm2 :: (MultiSet ref -> MultiSet ref -> MultiSet ref) -> (Term ref -> Term ref -> Term ref)
liftTerm2 = ((Term.) .) . flip on getTerm 

fromList :: (Ord ref) => [ref] -> Term ref
fromList = Term . MultiSet.fromList

toList :: Term ref -> [ref] 
toList = MultiSet.toList . getTerm

fromList' :: (Ord ref) => [(ref, Int)] -> Term ref
fromList' = Term . MultiSet.fromOccurList

toList' :: Term ref -> [(ref, Int)] 
toList' = MultiSet.toOccurList . getTerm

termOne :: Term ref
termOne = Term MultiSet.empty

termVar :: ref -> Term ref
termVar = Term . MultiSet.singleton

termProd :: (Ord ref) => Term ref -> Term ref -> Term ref
termProd = mappend

termProds :: (Ord ref) => [Term ref] -> Term ref
termProds = mconcat

termDegree :: Term ref -> Int
termDegree = MultiSet.size . getTerm

termDivide :: (Ord ref) => Term ref -> Term ref -> Maybe (Term ref)
termDivide (Term s) (Term t) | t `MultiSet.isSubsetOf` s = Just $ Term $ s `MultiSet.difference` t
termDivide _ _                                           = Nothing

monomialProd :: (Num a, Eq a, Ord ref) => Monomial a ref -> Monomial a ref -> Monomial a ref
monomialProd = biliftA2 (*) termProd

monomialDivide :: (Fractional a, Ord ref) => Monomial a ref -> Monomial a ref -> Maybe (Monomial a ref)
(a,s) `monomialDivide` (b,t) = (a/b,) <$> s `termDivide` t  

monomialIntegralDivide :: (Integral a, Ord ref) => Monomial a ref -> Monomial a ref -> Maybe (Monomial a ref, Monomial a ref)
_ `monomialIntegralDivide` (0,_) = Nothing
(a,s) `monomialIntegralDivide` (b,t) =
  case a `quotRem` b of
    (0, _) -> Nothing
    (k, r) -> do
      mq <- s `termDivide` t
      return ((k,mq), (r * a, s))   


instance Ord ref => Ord (Term ref) where
  -- | Implement reverse lexicographical ordering, as it's the fastest for
  --   Gröbner basis computation.
  compare = degrevlex


degrevlex :: (Ord ref) => Term ref -> Term ref -> Ordering
a `degrevlex` b = case (compare `on` termDegree) a b of
  LT -> LT
  GT -> GT
  EQ -> (compare `on` (Map.toDescList . MultiSet.toMap . getTerm)) a b

deglex :: (Ord ref) => Term ref -> Term ref -> Ordering
a `deglex` b = case (compare `on` termDegree) a b of
  LT -> LT
  GT -> GT
  EQ -> (compare `on` (Map.toList . MultiSet.toMap . getTerm)) a b

revdeglex :: (Ord ref) => Term ref -> Term ref -> Ordering
a `revdeglex` b = case (compare `on` termDegree) a b of
  LT -> GT
  GT -> LT
  EQ -> (compare `on` (Map.toList . MultiSet.toMap . getTerm)) a b


termRefs :: Term ref -> [ref]
termRefs = MultiSet.distinctElems . getTerm


rebaseMapMonomial :: (Ord s0, Ord s1, Num a) => (r -> Either s0 s1) -> Monomial a r -> Monomial (Monomial a s0) s1
rebaseMapMonomial f (a, term) =
  let (t0,t1) = partitionEithers $ fmap f $ toList term in
  ((a, fromList t0), fromList t1)
  

termLinear :: (Ord ref) => ref -> Term ref -> Maybe (Either (Term ref) (Term ref))
termLinear ref t@(Term t') = 
  case MultiSet.occur ref t' of
    0          -> Just $ Right t
    1          -> Just $ Left (Term (MultiSet.deleteAll ref t'))
    n          -> Nothing

monomialLinear :: (Ord ref) => ref -> Monomial a ref -> Maybe (Either (Monomial a ref) (Monomial a ref))
monomialLinear ref (a,t) = bimap (a,) (a,) <$> termLinear ref t
  
  
  
