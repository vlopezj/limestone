{-# LANGUAGE DoAndIfThenElse #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
-- | Non-linear arithmetic reasoning for naturals
--   The library is written in continuation-passing style; the idea being
--   that lemmas can be carried further into the scope as the solver "learns"
--   them.
module Pol.NIA(module Op
              ,module Pol.NIA)
       where

import Pol.Core as Pol
import qualified Pol.Term
import Data.Default
import Control.Monad.Reader
import Data.Semigroup
import Data.Map(Map)
import qualified Data.Map as Map
import Data.List
import Data.Maybe
import Op
import Development.Placeholders
import ContM
import Data.SBV.Bridge.CVC4
import Control.Monad.Trans.State
import System.IO.Unsafe

type PolZ = Pol Integer

type MonadNIA v m = (Ord v, Show v, MonadReader (PolPredicates v) m)

data PolPredicates v = PP' {
  bounds :: [(PolZ v, CmpOp, PolZ v)]
 } deriving (Show)


data BOrdering = BEq | BGEq | BLEq deriving (Show, Eq)

bOrdToCmpOp :: BOrdering -> CmpOp
bOrdToCmpOp BEq = Eq
bOrdToCmpOp BGEq = Ge
bOrdToCmpOp BLEq = Le

bReverse :: BOrdering -> BOrdering
bReverse BGEq = BLEq
bReverse BEq  = BEq
bReverse BLEq = BGEq

bImplies :: BOrdering -> BOrdering -> Bool
bImplies BEq  x    = True
bImplies BGEq BGEq = True
bImplies BLEq BLEq = True
bImplies _    _    = False

-- Combine knowledge
BGEq `bMeet` BGEq  = BGEq
BLEq `bMeet` BLEq  = BLEq
_    `bMeet` _     = BEq

mMeet :: Maybe BOrdering -> Maybe BOrdering -> Maybe BOrdering
mMeet (Just BEq) _ = Just BEq
mMeet Nothing a = a
mMeet a Nothing = a
mMeet (Just x) (Just y) = Just $ x `bMeet` y

ppRaw :: (Ord v) => [(PolZ v, CmpOp, PolZ v)] -> PolPredicates v
ppRaw v = PP' { bounds = v }

polPredAdd :: (MonadNIA v m) => (PolZ v, CmpOp, PolZ v) -> ContM_ m
polPredAdd (p, op, q) = local (flip mappend (ppRaw [(p,op,q)]))

type MemoizeT a b = StateT (Map a b)

memoized :: (Ord a, Monad m) => (a -> m b) -> (a -> MemoizeT a b m b)
memoized f a = do
  fa <- gets (Map.lookup a)
  case fa of
    Nothing -> do
      fa <- lift$ f a
      modify (Map.insert a fa)
      return fa
    Just fa -> return fa

runMemoizeT :: (Monad m) => MemoizeT a b m r -> m r
runMemoizeT = flip evalStateT def    

sbvCmpOp :: (OrdSymbolic a) => CmpOp -> (a -> a -> SBool)
sbvCmpOp Le = (.<=)
sbvCmpOp Ge = (.>=)
sbvCmpOp Lt = (.<) 
sbvCmpOp Gt = (.>) 
sbvCmpOp Eq = (.==) 
sbvCmpOp Ne = (./=)
sbvCmpOp CmpFalse = \_ _ -> false
sbvCmpOp CmpTrue = \_ _ -> true 

sbvThmResult :: ThmResult -> Maybe Bool
sbvThmResult (ThmResult (Unsatisfiable _)) = Just True
sbvThmResult (ThmResult (Satisfiable _ _)) = Just False
sbvThmResult (ThmResult (Unknown _ _))     = Nothing
sbvThmResult e@(ThmResult _) = error (show e)

-- | This will use a Quantifier-Free Linear Integer Arithmetic solver.
--   This theory is decidable. 
--   Polynomials are handled by normalizing them into monomials, and then
--   treating each monomial as an independent variable.
polPredProve :: forall v m. (MonadNIA v m) => (PolZ v, CmpOp, PolZ v) -> ContM m (Maybe Bool)
polPredProve (p, op, q) κ = do
  preds <- asks bounds
  let res = unsafePerformIO$ proveWith theProver$ runMemoizeT$ do
        -- Declare empty monomial as equivalent one
        one'    <- evalSymb (1 :: PolZ v)
        lift$ constrain$ one' .== 1

        -- Evaluate constraints
        forM_ preds$ \(a,op₀,b) -> do
          a'      <- evalSymb a
          let op₀' = sbvCmpOp op₀
          b'      <- evalSymb b
          lift$ constrain$ a' `op₀'` b'

        p'      <- evalSymb p
        let op'  = sbvCmpOp op
        q'      <- evalSymb q

        return$ p' `op'` q'

  κ (sbvThmResult res)

  where
    theProver = sbvCurrentSolver { useLogic = Just (PredefinedLogic QF_LIA) }

    evalSymb :: PolZ v -> StateT (Map (Pol.Term.Term v) (SInteger)) Symbolic (SInteger)
    evalSymb p = 
      fmap sum $ forM (toMonomials p) $ \(coef, monomial) -> do
        monomial' <- mv monomial
        return$ fromInteger coef * monomial'

    mv :: Pol.Term.Term v -> MemoizeT (Pol.Term.Term v) (SInteger) Symbolic (SInteger)
    mv = memoized$ \v -> do
      v' <- forall (show v)
      constrain $ v' .>= 0
      return v'

emptyPolPredicates :: PolPredicates v
emptyPolPredicates = PP' {
  bounds        = []
 }

instance (Ord v) => Monoid (PolPredicates v) where
  mempty = emptyPolPredicates
  a `mappend` b = PP' {
    bounds        = (bounds a ++ bounds b)
   } 

instance (Ord v) => Semigroup (PolPredicates v) where
  (<>) = mappend

type PP v = (->) (PolPredicates v)

{-
alwaysNonNegative :: (Ord v) => PolZ v -> Bool
alwaysNonNegative sz = $notImplemented

alwaysGEq0 :: (Ord v) => PolZ v -> Bool
alwaysGEq0 δ = alwaysNonNegative δ

alwaysLEq0 :: (Ord v) => PolZ v -> Bool
alwaysLEq0 = alwaysGEq0 . negate
-}

instance Default (PolPredicates v) where
  def = emptyPolPredicates

polLEq :: (MonadNIA v m) => PolZ v -> PolZ v -> ContM m (Maybe BOrdering)
polLEq p q κ =
  polPredProve (p, Le, q) $ \a ->
  polPredProve (p, Ge, q) $ \b ->
  case (a,b) of
    (Just True, Just True) -> κ (Just BEq)
    (Just True, _        ) -> κ (Just BLEq)
    (_        , Just True) -> κ (Just BGEq)
    (_        , _        ) -> κ Nothing

simplify :: (MonadNIA v m) => (PolZ v, BOrdering) -> PolZ v -> ContM m (Maybe BOrdering)
simplify (p, op) q κ = polPredAdd (p, bOrdToCmpOp op, 0) $ polLEq q 0 κ
