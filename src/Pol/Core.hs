{-# LANGUAGE ViewPatterns #-}
module Pol.Core where

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import Pol.Term hiding (toList, fromList, map)
import qualified Pol.Term as Term
import Data.List.NonEmpty(NonEmpty(..))
import qualified Data.List.NonEmpty as NE

import Data.Tuple(swap)
import Data.Void
import Data.Bifunctor
import Data.Function
import Control.Monad

-- ^ Pol always is normalized; that is, none of the "a" are 0
data Pol a v = Pol { getPol :: Map (Term v) a } deriving (Show, Eq, Ord)


instance (Num a, Eq a, Ord ref) => Num (Pol a ref) where
  (+) = add
  (*) = times
  (-) = sub

  abs p = case signumNum p of
     0    -> 0
     1    -> p
     (-1) -> negate p
     s    -> s `timesNum` p

  signum = lit . signumNum 

  fromInteger = lit . fromInteger
  negate = liftPolRaw $ fmap negate

signumNum :: (Num a, Eq a, Ord ref) => Pol a ref -> a
signumNum = signum . headCoef

lit :: (Num a, Eq a) => a -> Pol a ref
lit 0 = zero
lit 1 = one
lit n = Pol $ Map.singleton Term.termOne $ n

add :: (Num a, Eq a, Ord ref) => Pol a ref -> Pol a ref -> Pol a ref
add = liftPol2Raw $ Map.mergeWithKey (const $ ((mfilter (/= 0) . Just) .) . (+)) id id

times :: (Num a, Eq a, Ord ref) => Pol a ref -> Pol a ref -> Pol a ref
times = (fromMonomials .) . (liftM2 monomialProd `on` monomials)

sub :: (Num a, Eq a, Ord ref) => Pol a ref -> Pol a ref -> Pol a ref
sub = liftPol2Raw $ Map.mergeWithKey (const $ ((mfilter (/= 0) . Just) .) . (-)) id (fmap negate)

monomials :: Pol a ref -> [Monomial a ref]
monomials = fmap swap . Map.toDescList . getPol

zero :: Pol a ref
zero = Pol Map.empty

one :: (Num a) => Pol a ref
one = Pol $ Map.singleton Term.termOne 1

timesNum :: (Num a, Eq a, Ord ref) => a -> Pol a ref -> Pol a ref
timesNum 0 = const 0
timesNum n = liftPolRaw $ fmap (n *)

liftPolRaw :: (Map (Term r) a -> Map (Term s) b) -> Pol a r -> Pol b s
liftPolRaw = (Pol .) . (. getPol)

liftPolRawM :: (Functor m) => (Map (Term r) a -> m (Map (Term s) b)) -> Pol a r -> m (Pol b s)
liftPolRawM = (fmap Pol .) . (. getPol)

liftPol2Raw = ((Pol .) .) . (`on` getPol)

liftPol :: (Num a, Eq a) => (Map (Term ref) a -> Map (Term ref) a) -> Pol a ref -> Pol a ref
liftPol = (normalize .) . (. getPol)

liftPol2 :: (Num a, Eq a) => (Map (Term ref) a -> Map (Term ref) a -> Map (Term ref) a)
         -> Pol a ref -> Pol a ref -> Pol a ref
liftPol2 = ((normalize .) .) . (`on` getPol)

normalize :: (Num a, Eq a) => Map (Term ref) a -> Pol a ref
normalize = Pol . Map.filter (/= 0)

fromMonomials :: (Num a, Eq a, Ord ref) => [Monomial a ref] -> Pol a ref
fromMonomials = normalize . Map.fromListWith (+) . fmap swap

toMonomials :: Pol a ref -> [Monomial a ref]
toMonomials = fmap swap . Map.toList . getPol

fromList :: (Num a, Eq a, Ord v) => [(a, [v])] -> Pol a v 
fromList = fromMonomials . fmap (second Term.fromList) 

toList :: Pol a v -> [(a, [v])]
toList = fmap (second Term.toList) . fmap swap . Map.toList . getPol

asScalar' :: (Num a) => Pol a v -> Either (NonEmpty v) a
asScalar' (toList -> p) =
  case p of
    [(b, [])] -> Right b
    []        -> Right 0
    _         -> Left$ NE.fromList [v | (_,vs) <- p , v <- vs]

bindList' :: (Num b) => [(b,[r])] -> (r -> b) -> b
s `bindList'` f = foldl (+) 0 $ fmap (uncurry (*) . second ((foldl (*) 1) . fmap f)) s

bindList :: (Ord s, Num a, Eq a) => [(a,[r])] -> (r -> Pol a s) -> Pol a s
s `bindList` f = foldl (+) zero $ fmap (uncurry timesNum . second ((foldl times one) . fmap f)) s

bind :: (Ord s, Num a, Eq a) => Pol a r -> (r -> Pol a s) -> Pol a s
s `bind` f = toList s `bindList` f

eval :: (Num r) => (a -> r) -> (v -> r) -> Pol a v -> r
eval g f s = (map (first g) $ toList s) `bindList'` f

evalM :: (Num r, Applicative m) => (a -> m r) -> (v -> m r) -> Pol a v -> m r
evalM g f s = flip bindList' id <$> sequenceA [(,) <$> g a <*> traverse f v | (a,v) <- toList s]

-- ^ Partial function
headCoefNonZero :: Pol a ref -> a
headCoefNonZero = fst . headMonomial

headCoef :: (Num a, Eq a, Ord ref) => Pol a ref -> a
headCoef 0 = 0
headCoef p = headCoefNonZero p

-- ^ Partial function
headMonomial :: Pol a ref -> Monomial a ref
headMonomial = head . monomials 


-- | A polynomial is posic if it's leading monomial has a positive (or zero)
--   The leading monomial is defined according to the ordering.
--   For every polynomial, either
--     + p or -p is posic
--     + If both p and -p are posic, then  p ≡ -p ≡ 0
posic :: (Num a, Ord a, Ord v) => Pol a v -> (Bool, Pol a v)
posic p =
  if (signum $ headCoef p) >= 0
  then (True, p)
  else (False , negate p)

isPosic :: (Num a, Ord a, Ord v) => Pol a v -> Bool
isPosic = fst . posic

mapCoef :: (Num b, Eq b) => (a -> b) -> Pol a r -> Pol b r
mapCoef f = liftPolRaw $ Map.mapMaybe (mfilter (/= 0) . Just . f)

mapRef :: (Ord s, Ord r, Num a) => (r -> s) -> Pol a r -> Pol a s
mapRef f = liftPolRaw $ Map.mapKeysWith (+) (Term.map f)

mapRefCoef :: (Ord s, Ord r, Num b, Eq b) => (r -> s) -> (a -> b) -> Pol a r -> Pol b s 
mapRefCoef f g = mapRef f . mapCoef g
