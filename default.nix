{ mkDerivation, alex, array, base, bifunctors, bound, cmdargs
, cmdlib, comonad, constraints, containers, contravariant
, data-default, data-fin, deepseq, directory, file-location
, filepath, genifunctors, geniplate-mirror, graphviz, happy
, heredoc, HUnit, is, lens, marxup, mtl, multimap, multiset
, placeholders, prelude-extras, pretty, pretty-show, process
, QuickCheck, quickcheck-assertions, random, safe, sbv, semigroups
, singletons, stdenv, tagged, template-haskell, temporary
, test-framework, test-framework-hunit, test-framework-quickcheck2
, text, transformers, unix, void
}:
mkDerivation {
  pname = "limestone";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    array base bifunctors bound cmdargs cmdlib comonad constraints
    containers contravariant data-default data-fin directory
    file-location filepath genifunctors geniplate-mirror graphviz
    heredoc HUnit is lens marxup mtl multimap multiset placeholders
    prelude-extras pretty pretty-show process QuickCheck
    quickcheck-assertions random safe sbv semigroups singletons tagged
    template-haskell test-framework test-framework-hunit
    test-framework-quickcheck2 text transformers unix void
  ];
  libraryToolDepends = [ alex happy ];
  executableHaskellDepends = [ base lens ];
  testHaskellDepends = [
    base containers data-default deepseq directory HUnit lens mtl
    QuickCheck quickcheck-assertions temporary test-framework
    test-framework-hunit test-framework-quickcheck2 unix
  ];
  description = "Limestone: a language with linear types";
  license = "GPL";
}
