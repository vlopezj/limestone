module TestLSCC where

import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit
import Reference.Core
import System.Directory
import System.IO.Temp

import qualified Limestone.Examples as Col
import LSCC
import Control.Arrow.Pretty
import Control.Lens
import Control.DeepSeq
import Tagged
import Data.Default
import LL

-- | Tests of the full compiler pipeline
testsLSCC =  testsLSCCStep 0
          ++ testsLSCCStep 10000

testsLSCCStep step = [ testCase (name ++ ":steps=" ++ show step) $ timeoutLong $ testLSCC name step
             | name <- testDerivNames ]

testLSCC' :: String -> Int -> Bool -> IO ()
testLSCC' name steps ir = do
  let param = def {
        evalFuel = steps
       ,progDerivNames = [name]
       ,noIR = ir
       ,outputBaseName = name
       }
  withSystemTempDirectory (name ++ ".lscc") $ \dir -> do
    setCurrentDirectory dir
    runCompilerM param (compilerPipeline allDeriv)

testLSCC name steps = testLSCC' name steps True >>= return . rnf 
  
  
allDeriv = Col.allDeriv
testDerivNames = [d ^. derivName | D0 d ::: True <- Col.allProgs ]
