{-# LANGUAGE ForeignFunctionInterface #-}
module Reference.Core(module Core,
                      module Reference.Core) where

import Test.Framework as Core
import Test.Framework.Providers.HUnit as Core
import Test.Framework.Providers.QuickCheck2 as Core
import Control.DeepSeq
import Test.QuickCheck as Core((==>),Property)

import Control.Concurrent
import Foreign.C( CInt(..) )
import System.IO
import System.Posix.Process
import System.Exit
import System.Posix.Signals

import qualified System.Timeout as TO

rnfIO :: (NFData a) => a -> IO ()
rnfIO a = do
  () <- return $ rnf a
  return ()

rnfShowIO :: (Show a) => a -> IO ()
rnfShowIO a = rnfIO $ show a

type Microseconds = Int

seconds :: Double -> Microseconds
seconds x = ceiling $ x * 1000 * 1000

timeout :: IO a -> IO a
timeout m = do
  res <- TO.timeout (seconds 10) m
  case res of
    Just x  -> return x
    Nothing -> error "timeout"

timeoutLong :: IO a -> IO a 
timeoutLong m = do
  res <- TO.timeout (seconds 120) m
  case res of
    Just x  -> return x
    Nothing -> error "timeout"

waitForTermination a = a {-do
  forkIO a
  threadDelay 10000000-}
