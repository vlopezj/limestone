module Main where

import Test.Framework
import Test.Framework.Providers.HUnit
import Test.Framework.Providers.QuickCheck2 (testProperty)

import Test.QuickCheck
import Test.HUnit

import TestPol
import TestPolNIA
import TestLSCC
import TestLL

import Reference.Core

main :: IO ()
main = defaultMain tests
  
tests = [ testGroup "LSCC"  testsLSCC
        , testGroup "TypeCheck"  testsTypeCheck
        , testGroup "Eval"  testsEval
        , testGroup "Pol" testsPol
        , testGroup "PolNIA" testsPolNIA
        ]

