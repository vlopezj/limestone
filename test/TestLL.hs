module TestLL where

import LL
import CLL
import LSCC
import Control.Lens
import Reference.Core
import Test.HUnit
import Data.Default
import Eval
import Data.List as List

import Rules
import Reductions
-- Tests for the typechecker (and, implicitly, the linearity checker)

testsTypeCheck = [ testCase name (timeout$ testTypeCheck d0)
                 | d0@(D0 d) ::: _ <- goodDeriv
                 , let name = d ^. derivName ]

testsEval = [ testCase name (timeout$ testEval d0 fuel)
            | d0@(D0 d) ::: fuels <- goodDeriv
            , fuel <- fuels
            , let name = d ^. derivName ]

testTypeCheck :: IdDeriv0 -> Assertion
testTypeCheck d0 = rnfShowIO $ analyseDeriv d0

testEval :: IdDeriv0 -> Int -> Assertion
testEval d0 fuel = 
  let d0' = runEvalEnv evaluateFDeriv fuel def{ _branchingAllowed = False } d0 in
  testTypeCheck d0'


goodDeriv =  rules
          ++ reductions
          ++ cll

rules = List.map (::: []) [ axRule
        , cutRule , cutRuleN, fuseRule, fuseRuleN, mixRule, haltRule
        , crossRule , parRule, withRule Inl, withRule Inr, plusRule
        , oneRule, zeroRule, botRule
        , forallRule, existsRule
        , saveRule, loadRule
        , splitRule, mergeRule
        , bigTensorRule, bigParRule
        , freezeRule, freezeKRule
        , foldmap2Rule
        , bigSeqRule
        ]

reductions = List.map (::: [100]) [ cutAssoc1
             , cutAssoc1R
             , cutSplit
             , cutSplitR
             , cutAx
             , cutParCross 1 id id
             , cutParCross szN id id
             , cutWithPlus Inl id id
             , cutWithPlus Inr id id
             , cutUnit
             , cutBigL
             , cutBigSimpl
             , cutFoldmap2
             , cutSaveOffer
             , cutQuant id id
             , commutPar 1 id id
             , commutPar szN id id
             , commutCross 1
             , commutPlus
             , commutWith Inl id id
             , commutWith Inr id id
             , commutForall id id
             , commutExists
             , commutSave
             , commutLoad
             , commutBigPar
             , commutBigTensor
             , commutGen
             , commutSync
             , commutMix1, commutMix2, commutMix3, commutMix4
             , fuseMBigSeq
             , mergeFoldmap
             , mergeBigPar
             ] 


cll = concat
 [[zipWithDeriv ::: d
  ,mapDeriv  ::: d  
  ,mapNDeriv ::: d 
  ,bareScheduleDeriv ::: d
  ,schedule2Deriv ::: d
  ]
 ,[schedule3Deriv b 0 ::: d | b <- [True,False]
  ]
 ,[freezeDerivK ::: d
  ,freezeDeriv2 ::: d
  --,fftStepDeriv 0 ::: d
  ,fftStep2Deriv 0 ::: d
  --,fftStep2DerivEval ::: []
  ,sumDeriv ::: d
  ]
 ,[dotProduct2Deriv b 0 ::: d | b <- [True,False]
  ]
 ,[matrixProductDeriv ::: d
  ,saxpyNDeriv 0 ::: d
  ,rotateDeriv 0 ::: d
  ]
 ,[stonestampDeriv kGen 0 ::: d | kGen <- [True,False]
  ]
 ,[circStencilDeriv kGen 0 ::: d | kGen <- [True,False]
  ]
 ,[circStencilMemDeriv 0 ::: d
  ,stonestampDeriv2 0 ::: d
  ,circStencilDeriv2 0 ::: d
  ,circStencilDerivEval ::: [] 
--  ,stencilDeriv ::: d
--  ,stencilDeriv2 ::: d
  ,extendLDeriv ::: d
  ,extendRDeriv ::: d
--  ,duplDeriv ::: d
  ,waveBigSeqDeriv ::: d
  ,waveBigSeqSymmDeriv ::: d
  ]
 ,[waveBigSeqSymmComposeDeriv count ::: d | count <- [2,3]
  ]
 ,[waveBigSeqSymm2Deriv ::: d
--  ,waveBigSeqSymm2FusedDeriv ::: d
  ]
 ,[foldDeriv ::: d
  ,mapSeqDeriv ::: d
  ,mapSeq1Deriv ::: d
  ,splitDeriv ::: d
  ,foldDeriv ::: d
  ,bestDeriv ::: d
  ,quickhullStep1Deriv ::: d
  ,quickhullStep2Deriv ::: d
  ,quickhullStep3Deriv ::: d
  ,lineDistanceDeriv ::: d
  ,dotDeriv ::: d
  ,dotSeqDeriv ::: d
  ,best1Deriv ::: d
  ,splitTopAboveDeriv ::: d
  ]
 ]
      where
        d = [10000] -- default steps

