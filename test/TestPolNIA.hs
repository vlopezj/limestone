{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE DoAndIfThenElse #-}
module TestPolNIA where

import TestPol
import Pol
import Pol.NIA
import Data.Default
import Reference.Core
import Test.QuickCheck(Arbitrary(..),oneof)
import Test.QuickCheck.Assertions  
import qualified Data.Map as M
import Control.Applicative
import Test.QuickCheck.Property
import Control.Monad.Reader
import Data.Default

instance Arbitrary BOrdering where
  arbitrary = oneof [return x | x <- [BEq, BGEq, BLEq]]

instance Arbitrary CmpOp where
  arbitrary = oneof $ fmap return enumCmpOp


testsPolNIA = [ testProperty "addGe" $ addGe
              , testProperty "addLe" $ addLe
              , testProperty "addEq"  $ addEq
              , testProperty "testAdd"  $ testAdd
              , testProperty "meetOrdering"  $ meetOrdering
              , testProperty "polLEqScalar"  $ polLEqScalar
              , testProperty "cmpRev"  $ cmpRev
              , testProperty "niaRecall" $ niaRecall
              , testProperty "niaRecallRev" $ niaRecallRev
              ]

pp :: (PolPredicates Ref -> a) -> a
pp = ($ def)


testAdd :: S -> BOrdering -> Property
testAdd p op = (not $ isScalar p) ==> (pp $ do
            polPredAdd (p, bOrdToCmpOp op, 0) $ do
              polLEq p 0 $ \res -> return$ res `assertImplies` Just op)
             
testSimplifyGEq :: S -> Integer -> Bool -> Property
testSimplifyGEq p i pos = do
  let f = simplify (p, BGEq)
  (not$ isScalar p) ==> 
     if pos then
      (pp$
        f (p + fromIntegral i) $ \res -> return$ case i `compare` 0 of
                                EQ -> res `assertImplies` Just BGEq
                                LT -> succeeded
                                GT -> res `assertImplies` Just BGEq)
      else
      (pp$
        f (negate p + fromIntegral i) $ \res -> return$ case i `compare` 0 of
                                EQ -> res `assertImplies` Just BLEq
                                LT -> res `assertImplies` Just BLEq
                                GT -> succeeded)

testSimplifyEq :: S -> Integer -> Bool -> Property
testSimplifyEq p i pos = do
  let f = simplify (p, BEq)
  (not$ isScalar p) ==> 
     if pos then
      (pp$
        f (p + fromIntegral i) $ \res -> return$ case i `compare` 0 of
                                EQ -> res `assertImplies` Just BEq
                                LT -> res `assertImplies` Just BLEq
                                GT -> res `assertImplies` Just BGEq)
      else
      (pp$
        f (negate p + fromIntegral i) $ \res -> return$ case i `compare` 0 of
                                EQ -> res `assertImplies` Just BEq
                                LT -> res `assertImplies` Just BLEq
                                GT -> res `assertImplies` Just BGEq)

testSimplifyLEq :: S -> Integer -> Bool -> Property
testSimplifyLEq p i pos = do
  let f = simplify (p, BLEq)
  (not$ isScalar p) ==> 
     if pos then
      (pp$
        f (p + fromIntegral i) $ \res -> return$ case i `compare` 0 of
                                EQ -> res `assertImplies` Just BLEq
                                GT -> succeeded
                                LT -> res `assertImplies` Just BLEq
        )
      else
      (pp$
        f (negate p + fromIntegral i) $ \res -> return$ case i `compare` 0 of
                                EQ -> res `assertImplies` Just BGEq
                                GT -> res `assertImplies` Just BGEq
                                LT -> succeeded
        )


assertImplies :: Maybe BOrdering -> Maybe BOrdering -> Result
assertImplies _ Nothing = succeeded
assertImplies (Just x) (Just y) | x `bImplies` y = succeeded
assertImplies x y = failed { reason = "Result " ++ show x ++ " does not imply " ++ show y ++ "." }


polLEqScalar :: Integer -> Result
polLEqScalar i = pp $ polLEq (fromIntegral i) 0 $ \res ->
  return $ case i `compare` 0 of
    EQ -> res ?== Just BEq
    LT -> res ?== Just BLEq
    GT -> res ?== Just BGEq

meetOrdering :: BOrdering -> BOrdering -> Bool
meetOrdering a b = (a `bMeet` b) `bImplies` a

addGe :: S -> Integer -> Property
addGe p (fromIntegral -> q) = (not$ isScalar p) ==> (pp $ 
  polPredAdd (p, Ge, q) $
  polLEq p (q - 1) $ \res ->
  return$ res `assertImplies` Just BGEq
  )
  
addLe :: S -> Integer -> Property
addLe p (fromIntegral -> q) = (not$ isScalar p) ==> (pp $ 
  polPredAdd (p, Le, q) $
  polLEq p (q + 1) $ \res ->
  return$ res `assertImplies` Just BLEq
  )

addEq :: S -> Integer -> Property
addEq p (fromIntegral -> q) = (not$ isScalar p) ==> (pp $ 
  polPredAdd (p, Eq, q) $
  p `polLEq` q $ \res_eq ->
  p `polLEq` (q-1) $ \res_geq ->
  p `polLEq` (q+1) $ \res_leq ->
  return $      res_eq `assertImplies` Just BEq
           .&&. res_leq `assertImplies` Just BLEq
           .&&. res_geq `assertImplies` Just BGEq
  )

cmpRev :: S -> CmpOp -> S -> Property
cmpRev p op q =
  (not $ op `elem` [Ne]) ==> (pp$
    polPredAdd (p, op, q) $
    polPredProve (q, flipCmp op, p) $ \res ->
    return$ res ?== Just True
    )

niaRecall :: (S, CmpOp, S) -> Property
niaRecall (x,op,y) = (not$ op `elem` [CmpFalse,Ne]) ==>
                       ((flip runReader def$
                         polPredAdd (x,op,y) $
                         polPredProve (x,op,y) $ \res ->
                         return (res))  ?== Just True)

niaRecallRev :: (S, CmpOp, S) -> Property
niaRecallRev (x,op,y) = (not$ op `elem` [CmpFalse,Ne]) ==>
                       ((flip runReader def$
                         polPredAdd (x,flipCmp op,y) $
                         polPredProve (y,op,x) $ \res ->
                         return (res))) ?== Just True
