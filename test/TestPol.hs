{-# LANGUAGE ViewPatterns #-}
module TestPol where

import Pol
import Test.QuickCheck
import Test.QuickCheck.Property
import Test.Framework (defaultMain, testGroup)
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Control.Applicative
import Pol.Term as Term
import Control.Monad
import Data.Monoid

import Test.Framework
import Test.Framework.Providers.QuickCheck2 (testProperty)

testsPol = [ testProperty "commutesAddition" $ commutes (+)
           , testProperty "commutesProduct" $ commutes (*)
           , testProperty "associatesAddition" $ associates (+)
           , testProperty "associatesProduct" $ associates (*)
           , testProperty "consistent1" consistent1
           , testProperty "consistent2" consistent2
           , testProperty "divisionCorrect" divisionCorrect
           , testProperty "divisionTerminates" divisionTerminates
           , testProperty "zeroPosic" $ zeroPosic
           , testProperty "projPosic" $ projPosic
           , testProperty "bipartitePosic" $ bipartitePosic
           , testProperty "negateId" $ negateId
           , testProperty "negateInvolution" $ negateInvolution
           ]


newtype Ref = Ref String deriving (Show, Eq, Ord)

instance Arbitrary Ref where
  arbitrary = sized $ \n -> do
    v <- (++) <$> growingElements vars <*> (idx <$> choose (0, n `div` 3))
    return $ Ref v

    where
        vars = ["x","y","z"] 

        idx :: Int -> String
        idx 0 = ""
        idx i | i > 0 = show (i-1) 

instance (Ord ref, Arbitrary ref) => Arbitrary (Term ref) where
  arbitrary = sized $ \n ->
    Term.fromList <$> do
      deg <- choose (0,n)
      let var = resize (1 + ((2*n) `div` (1 + deg))) arbitrary
      forM [1..n] $ const var


instance (Ord ref, Eq a, Num a, Arbitrary ref, Arbitrary a) => Arbitrary (Pol a ref) where
  arbitrary = fromMonomials <$> sized (\n -> listOf (resize (squareRoot n) arbitrary)) 

type S = Pol Integer Ref
type T = Term Ref

commutes :: (S -> S -> S) -> S -> S -> Bool
commutes op a b = (a `op` b) == (b `op` a)

associates :: (S -> S -> S) -> S -> S -> S -> Bool
associates op a b c = ((a `op` b) `op` c) == (a `op` (b `op` c))

divisionCorrect :: S -> S -> Property 
divisionCorrect a b = (b /= 0) ==> ((a * b) `divSafeIntegral` b) == (Just a)

divisionTerminates :: S -> S -> Property 
divisionTerminates a b = (b /= 0) ==> (a `divSafeIntegral` b) `seq` True 

substraction :: S -> S -> Bool
substraction a b = (a - b) == (a + (negate b))

(^!) :: Num a => a -> Int -> a
(^!) x n = x^n

consistent1 :: T -> T -> T -> Property
consistent1 s t u = s <= t ==> (u <> s) <= (u <> t)

consistent2 :: T -> T -> Bool
consistent2 s t = t <= (t <> s)


squareRoot :: (Integral a) => a -> a
squareRoot 0 = 0
squareRoot 1 = 1
squareRoot n =
   let twopows = iterate (^!2) 2
       (lowerRoot, lowerN) =
          last $ takeWhile ((n>=) . snd) $ zip (1:twopows) twopows
       newtonStep x = div (x + div n x) 2
       iters = iterate newtonStep (squareRoot (div n lowerN) * lowerRoot)
       isRoot r  =  r^!2 <= n && n < (r+1)^!2
   in  head $ dropWhile (not . isRoot) iters



bipartitePosic :: S -> Bool
bipartitePosic p = (isPosic p) || (isPosic (negate p))

zeroPosic :: S -> Bool
zeroPosic p = not ((isPosic p) && (isPosic (negate p))) || p == 0

projPosic :: S -> Bool
projPosic p@(posic -> (flag, p')) = (p == p') == flag

negateId :: S -> Bool
negateId p = (p + negate p) == 0

negateInvolution :: S -> Bool
negateInvolution p = (negate (negate p)) == p

