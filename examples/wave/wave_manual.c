#include <stddef.h>
#include <stdbool.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void wave0 (size_t, double, double *, double *, double *);
void wave1 (size_t, double, double *, double *, double *, double *);
void wave2 (size_t, double, double *, double *, double *, double *);
void wave3 (size_t, double, double *, double *, double *, double *);
void wave6 (size_t, double, double *, double *, double *, double *);

static inline double waveL (double tau, double low, double a, double b) { return 0; }
static inline double waveR (double tau, double low, double a, double b) { return 0; }
static inline double waveC (double tau, double a, double b, double c, double prev) {
  double e = 2*b;
  return e - prev + tau * (c - e + a);
}

void wave0 (size_t n, double tau, double *a, double *b, double *d) {
    d[0] = waveL(tau, a[0], b[0], b[1]);
    for(int i = 1; i < n-1; i++) {
      d[i] = waveC(tau, b[i-1], b[i], b[i+1], a[i]);
    }
    d[n-1] = waveR(tau, a[0], b[0], b[1]);
}

void wave1 (size_t n, double tau, double *a, double *b, double *c, double *d) {
  d[0] = waveL(tau, a[0], b[0], b[1]);
  c[0] = b[0];
  for(int i = 1; i < n-1; i++) {
    d[i] = waveC(tau, b[i-1], b[i], b[i+1], a[i]); 
    c[i] = b[i];
  }
  d[n-1] = waveR(tau, a[n-1], b[n-2], b[n-1]);
  c[n-1] = b[n-1];
}

void wave2 (size_t n, double tau, double *a, double *b, double *c, double *d) {
  double c0, c1, c2;

  c[0] = c0 = waveL(tau, a[0], b[0], b[1]);
  c[1] = c1 = waveC(tau, b[0], b[1], b[2], a[1]);
  d[0] = waveL(tau, b[0], c0, c1);
  for(int i = 1; i < n-2; i++) {
    int j = i+1; c[j] = c2 = waveC(tau, b[j-1], b[j], b[j+1], a[j]);
    d[i] = waveC(tau, c0, c1, c2, b[i]); 
    c0 = c1;  
    c1 = c2;
  }
  c[n-1] = c2 = waveR(tau, a[n-1], b[n-2], b[n-1]);
  d[n-2] = waveC(tau, c0, c1, c2, b[n-2]);
  d[n-1] = waveR(tau, b[n-1], c1, c2);
}

void wave3 (size_t n, double tau, double *a, double *b, double *c, double *d) { return; }

void wave6 (size_t n, double tau, double *a, double *b, double *c, double *d) { return; }
