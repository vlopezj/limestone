#!/bin/bash

function prog() {
    ./sim$4 $1 $2 $3 s 2>/dev/null
}

echo -e "alg\tt\tx\tfuse\ttime\ttimeEl"

for repeat in `seq 1`; do
    for i in 0 1 2 3 6; do
    #        prog 10000000 10       $i
    #        prog 1000000  100      $i
    #        prog 100000   1000     $i
    #        prog 10000    10000    $i
    #        prog 1000     100000   $i
    #       prog 200      10000000 $i
    echo -n -e "UNFUSED\t";    prog 10   6000000    $i _unfused
    echo -n -e "FUSED\t";   prog 100      6000000  $i
    echo -n -e "HAND\t";    prog 100      6000000  $i _manual
    echo -n -e "PHYSIS\t";    prog 100      6000000  $i _physis
    done
done
