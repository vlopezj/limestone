#!/usr/bin/env python

from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np
import sys

yy = np.genfromtxt(sys.argv[1])

fig = plt.figure()
ax = plt.axes(xlim=(0,1), ylim=(-2,2))
line, = ax.plot([], [], lw=2)

def init():
    line.set_data([],[])
    return line,

def animate(i):
    y = yy[i,:]

    x = np.linspace(0,1, len(y))

    line.set_data(x,y)
    return line,

anim = animation.FuncAnimation(fig, animate, init_func = init,
                               frames = min(100,len(yy)), interval=12, blit = True)

#anim.save('wave.mp4', fps=12, extra_args=['-vcodec', 'libx264'])
anim.save('imgs/wave.png', writer='imagemagick', dpi=300)

plt.show()



