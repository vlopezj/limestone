#!/bin/bash

D=$(mktemp -d)
make

error=0

for a in 0 1 2 3 6; do
    for t in output_manual output output_unfused; do
        if [ $t == "output_manual" -a '(' $a == 3 -o $a == 6 ')' ]; then
            continue
        fi
        ./sim_$t 1000 1000 $a 1 2>/dev/null > $D/$t.txt
        diff ./test_reference.txt $D/$t.txt >"$t-diff.txt" || (echo "$t:$a" 1>&2 && exit 1)
        if [ $? -ne 0 ]; then error=1; fi
        rm $D/$t.txt
    done
done

exit $error
