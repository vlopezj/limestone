#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>
#include <stdio.h>

#include "wave.h"

// Number of repeats for warmup
#define REPEATS 3
const double dt = 0.1;                                                              
const double dx = 0.1;                                                              
const double c  = 0.5;                                                               

double** mkarray2(int m, int n) {
  double* block = (double *)malloc(sizeof(double) * m * n);
  double** index = (double**)malloc(sizeof(double*) * m);

  for(int i = 0; i < m; i++) {
    index[i] = block + i * n;
  }
  return index;
}

void freearray2(double** index) {
  free(index[0]);
  free(index);
}



double v_init(double x) {
  return 0;
}

void output_line(int x_num, double *y) {
  #ifdef OUTPUT
  for(int j=0; j<x_num; j++) {
    printf("%lf ",y[j]);
  }
  printf("\n");
  #endif
}

double clock_diff(struct timespec b, struct timespec a) {
  double sec_delta  = b.tv_sec - a.tv_sec;
  long   nano_delta = b.tv_nsec - a.tv_nsec;
  return sec_delta + (1.0E-9 * nano_delta);
}

int main(int argc, char * argv[]) {
  // Reading arguments
  long x_num, t_num;                                                             
  int fuse;
  char output;

  switch(argc-1) {                                                              
  case 4:                                                                       
    if(sscanf(argv[1], "%ld", &t_num) >= 1 &&                                    
       sscanf(argv[2], "%ld", &x_num) >= 1 &&
       sscanf(argv[3], "%d", &fuse) >= 1  &&
       sscanf(argv[4], "%c", &output) >= 1 
       )
      break;                                                                    
  default:                                                                      
    assert(argc > 0);                                                           
    fprintf(stderr, "Usage: %s [#t] [#x] [#fuse] [#output y|n]\n", argv[0]);
    exit(1);                                                                    
  }    

  // Initialization
  double x_max = dx*x_num;
  double y_init(double x) {
      return
        sin(2 * M_PI * x/x_max) +
        0.5*sin(3 * M_PI * x/x_max) +
        0.33*sin(5 * M_PI * x/x_max) +
        0.25*sin(7 * M_PI * x/x_max);
  }

  double **y, ** b;
  y = mkarray2(20, x_num);
  b = mkarray2(20, x_num);
  double tau = c * dt / dx;
  assert(tau <= 1);

  fprintf(stderr, "Tau: %lf\n", tau);
  fprintf(stderr, "%ld steps\n", t_num);
  fprintf(stderr, "%ld elements\n", x_num);
  fprintf(stderr, "%d steps fused together\n", fuse);
  
  double diff;
  struct timespec start, end;
#ifndef OUTPUT
  for(int repeat = 0; repeat < REPEATS; repeat++){
#endif

      // Set contour conditions
      for (int i = 1; i < (x_num-1); i++) {
        double sx = ((double) i)/x_num * x_max;
        b[0][i] = y_init(sx);
        y[0][i] = b[0][i] + v_init(sx) * dt;
      }
      b[0][0] = b[0][x_num-1] = 0;
      y[0][0] = y[0][x_num-1] = 0;

      clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
      // For comparability, each step simulates 6 timesteps
      switch(fuse){
        case 0:
          for(int i=0;i<t_num;i++) {
            wave0(x_num, tau, b[0]  , y[0]  , b[1]);
            wave0(x_num, tau, y[0]  , b[1]  , b[2]);
            wave0(x_num, tau, b[1]  , b[2]  , b[3]);
            wave0(x_num, tau, b[2]  , b[3]  , b[4]);
            wave0(x_num, tau, b[3]  , b[4]  , b[0]);
            wave0(x_num, tau, b[4]  , b[0]  , y[0]);
	    output_line(x_num, y[0]);
          }
        break;

        case 1:
          for(int i=0;i<t_num;i++) {
            wave1(x_num, tau, b[0], y[0], b[1], b[2]);
            wave1(x_num, tau, b[1], b[2]  , b[3], b[4]);
            wave1(x_num, tau, b[3], b[4]  , b[5], b[6]);
            wave1(x_num, tau, b[5], b[6]  , b[7], b[8]);
            wave1(x_num, tau, b[7], b[8]  , b[9], b[10]);
            wave1(x_num, tau, b[9], b[10] , b[0], y[0]);
	    output_line(x_num, y[0]);
          }
        break;

        case 2:
          for(int i=0;i<t_num;i++) {
            wave2(x_num, tau, b[0]  , y[0] , b[1], b[2]);
            wave2(x_num, tau, b[1]  , b[2] , b[3], b[4]);
            wave2(x_num, tau, b[3]  , b[4] , b[0], y[0]);
	        output_line(x_num, y[0]);
          }
        break;

        case 3:
          for(int i=0;i<t_num;i++) {
            wave3(x_num, tau, b[0]  , y[0], b[1], b[2]);
            wave3(x_num, tau, b[1]  , b[2], b[0], y[0]);
	    output_line(x_num, y[0]);
          }
        break;

        case 6:
          for(int i=0;i<t_num;i+=2) {
            wave6(x_num, tau, b[0], y[0], b[1], y[1]);
            wave6(x_num, tau, b[1], y[1], b[0], y[0]);
	    output_line(x_num, y[1]);
	    output_line(x_num, y[0]);
          }
        break;

        default: assert(false);
      }
      clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
      diff = clock_diff(end,start);
#ifndef OUTPUT
  }
#endif
  double cpu = diff;
  double cpu_per_element = cpu / (6 * t_num * (x_num - 2));
 
  switch(output) {
    case 's':
      printf("%ld\t%ld\t%d\t%lg\t%lg\n", t_num, x_num, fuse, cpu, cpu_per_element);
      break;
  }

  freearray2(y); freearray2(b);
}
