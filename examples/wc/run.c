#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>
#include <stdio.h>

#include "wc_driver.h"

void y_init(long len, long i, double * x, double * y) {
  double t = 2*M_PI*((double)i)/len;
  *x = sin(3*t);
  *y = sin(2*t + 1);
}

double clock_diff(struct timespec b, struct timespec a) {
  double sec_delta  = b.tv_sec - a.tv_sec;
  long   nano_delta = b.tv_nsec - a.tv_nsec;
  return sec_delta + (1.0E-9 * nano_delta);
}

void output_points(size_t len, point * hull) {
    for(int i = 0; i < len; i++) {
      printf("%lf\t", hull[i].X);
    }
    printf("\n");
    for(int i = 0; i < len; i++) {
      printf("%lf\t", hull[i].Y);
    }
    printf("\n");
}

int main(int argc, char * argv[]) {
  // Reading arguments
  long len;                                                             
  long repeats;
  char output;
  FILE* f;

  switch(argc-1) {                                                              
  case 3:                                                                       
    if(((f = fopen(argv[1],"r")) || (fprintf(stderr, "Can't open %s\n", argv[1]) &&  0)) &&
       sscanf(argv[2], "%ld", &len) >= 1 &&
       sscanf(argv[3], "%ld", &repeats) >= 1 &&                                    
       sscanf(argv[4], "%c", &output) >= 1 
       )
      break;                                                                    
  default:                                                                      
    assert(argc > 0);                                                           
    fprintf(stderr, "Usage: %s file len repeats [#output y|n]\n", argv[0]);
    exit(1);                                                                    
  }    


  point * all, * hull;
  size_t hull_len;

  all  = malloc(sizeof(*all)*len);
  hull = malloc(sizeof(*hull)*len);
  
  for(size_t i = 0; i < len; i++) {
    y_init(len,i,&all[i].X,&all[i].Y);
  }
  
  double diff;
  struct timespec start, end;

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
  for(int r = 0; r < repeats; r++){
    quickhull(len, all, &hull_len, hull);
  }        

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
  diff = clock_diff(end,start);

  double cpu = diff / repeats ;
  double cpu_per_element = cpu / len;

  switch(output) {
      case 'y': 
        output_points(len, all);
	output_points(hull_len, hull);
	break;
      case 's':
        printf("%ld\t%ld\t%lg\t%lg\n", len, repeats, cpu, cpu_per_element);
        break;
  }

  free(all);
  free(hull);
}
