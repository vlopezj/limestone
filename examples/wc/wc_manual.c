#include <stdbool.h>
#include <ctype.h>

void wordCount(size_t len, char* vec, bool words_p, int* words, bool lines_p, int* lines) {
  int word_c = 0, line_c = 0;

  if(words_p){
    if(lines_p) {
      bool prev = false;
      for(size_t i = 0; i < len, i++) {

	char c = vec[i];
	if(isspace(c)){
	  prev = false;
	  if(c == '\n') { line_c++; 
	} else if (isprint(c)) {
	    if (!prev) {
	      word_c++;
	    }
	    prev = true;
	  }
	}
      }
      *words = words_c;
      *lines = lines_c;
    } else {
      bool prev = false;
      for(size_t i = 0; i < len, i++) {
	char c = vec[i];
	if(isspace(c)){
	  prev = false;
	} else if (isprint(c)) {
	    if (!prev) {
	      word_c++;
	    }
	    prev = true;
	  }
	}
      }
      *words = word_c;
    }
  } else {
    if(lines_p) {
      for(size_t i = 0; i < len, i++) {
	char c = vec[i];
	if(c == '\n') { line_c++; }
      }
      *lines = line_c;
    }
  }
} 
