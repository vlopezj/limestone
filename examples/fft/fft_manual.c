#include <stddef.h>
#include <stdbool.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


void fftMulti (size_t n, size_t d, double complex *source, double complex *dest) {
    double complex twiddle = _Complex_I * 2.0 * M_PI / (double)(n);
    for (size_t k = 0; k < d; k++){
        for (size_t i = 0; i < n; i++) {
            double complex x, y;
            x = source[n * (2 * k) + i];
            y = cexp((double)i * twiddle) * source[n * (2 * k + 1) + i];
            dest[n * 2 * k + i] = x + y;
            dest[n * (2 * k + 1) + i] = x - y;
        }
    }
}

