#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>
#include <stdio.h>

#include "fft.h"

// Number of repeats for warmup
#define REPEATS 3
const double dt = 0.1;                                                              
const double dx = 0.1;                                                              
const double c  = 0.5;                                                               

double clock_diff(struct timespec b, struct timespec a) {
  double sec_delta  = b.tv_sec - a.tv_sec;
  long   nano_delta = b.tv_nsec - a.tv_nsec;
  return sec_delta + (1.0E-9 * nano_delta);
}

int main(int argc, char * argv[]) {
  // Reading arguments
  int log_len;                                                             
  long repeats;
  char output;

  switch(argc-1) {                                                              
  case 3:                                                                       
    if(sscanf(argv[1], "%d", &log_len) >= 1 &&                                    
       sscanf(argv[2], "%ld", &repeats) >= 1 &&                                    
       sscanf(argv[3], "%c", &output) >= 1 
       )
      break;                                                                    
  default:                                                                      
    assert(argc > 0);                                                           
    fprintf(stderr, "Usage: %s log₂(len) repeats [#output y|n]\n", argv[0]);
    exit(1);                                                                    
  }    

  // Initialization
  const long len = ((long)1) << log_len;
  double complex y_init(long i) {
      if (len/4 <= i && i <= 3*len/4 ) {
          return 1;
      } else {
          return 0;
      }
  }

  double complex *y1, *y2;
  y1 = malloc(sizeof(*y1)*len);
  y2 = malloc(sizeof(*y2)*len);

  for(size_t i = 0; i < len; i++) {
    y1[i] = y_init(i);
  }
  fprintf(stderr, "Lenght: 2^%d = %ld\n", log_len, len);
  
  double diff;
  struct timespec start, end;

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
  for(int r = 0; r < repeats; r++){
    for(int log_d = 0, log_n = (log_len - 1);
        log_d <= log_len - 1 && log_n >= 0;
        log_d++, log_n --) {
          fftMulti(1L << log_n, 1L << log_d, y1, y2); 
          { void *swp = y1; y1 = y2; y2 = swp; }
    }
  }        

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
  diff = clock_diff(end,start);

  double cpu = diff / repeats ;
  double cpu_per_element = cpu / (log_len * len);
 
  switch(output) {
      case 'y': 
          for(int i = 0; i < len; i++) {
              printf("%lf\t",creal(y1[i]));
          }
          printf("\n");
          for(int i = 0; i < len; i++) {
              printf("%lf\t",cimag(y1[i]));
          }
          printf("\n");
          break;
      case 's':
        printf("%d\t%ld\t%lg\t%lg\n", log_len, repeats, cpu, cpu_per_element);
        break;
  }

  free(y1); free(y2);
}
