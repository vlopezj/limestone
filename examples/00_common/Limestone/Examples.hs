{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ViewPatterns #-}
module Limestone.Examples where

import CLL.Prelude as CLL hiding (fftStepDeriv,bff)
import Control.Applicative
import Tagged
import Data.List
import Control.Lens

-- ^ The tag indicates whether the programs are suitable for compilation, or
--   just intermediate results.
allProgs = []
           ++ [dotDeriv ::: True]
           ++ [dotSeqInlinedDeriv ::: True]
           ++ fftMultiDeriv
           ++ wave0
           ++ wave1
           ++ wave2
           ++ wave3
           ++ wave6
           ++ [diffDeriv ::: True]
           ++ [diff2Deriv ::: True]
           ++ quickhull
           ++ wc
           
allDeriv = fmap dropTag allProgs

main = compilerMain allDeriv

bffDeriv = D0 $ runFreshM $ do
  n <- freshFrom "n"; let szN = var n :: IdSize
  [f] <- mapM freshFrom ["f"]
  Deriv "bff" SeqCtx { _intuit =  [n      :::.  T TSize]
                     , _linear =  [f      :::.   dual (bigTensor szN ((tComplex ⊗ tComplex)  ⊸  (tComplex |: tComplex)))]
                     } <$> (
    -- factor 2π/n
    ffiLit ("_Complex_I" ::: tComplex) $ \im -> 
    ffiLit ("M_PI" ::: tComplex)       $ \pi -> 
    ffiLit ("2.0" ::: tComplex)     $ \two ->

    fTimes tComplex two pi $ \r₁ ->
    fTimes tComplex im  r₁ $ \r₂ ->
    sizeAsDouble szN $ \szND ->
    fDiv   tComplex r₂ szND $ \factor ->
    syncK szN (tComplex :^ 1) (axiom factor) $ \factor ->

    putIndex szN $ \i ->
    coslice f szN $ \f ->
    getIndex i szN $ \i ->
    unapply 1 f $ \[input] output ->

    tensor input $ \a b ->
    tensor output $ \c d ->

    copyData 2 tComplex a $ \[a₁, a₂] ->

    sizeAsDouble i $ \i ->

    fTimes tComplex i factor $ \tw ->
    ffiCall tComplex "cexp" [tw] $ \twexp -> 
    fTimes tComplex twexp b $ \b -> 

    copyData 2 (T PComplex) b $ \[b₁, b₂] ->
    CLL.mix' [fPlus  tComplex a₁ b₁ (axiom c)
             ,fMinus tComplex a₂ b₂ (axiom d)
             ]
    )

diffDeriv = D0$ runFreshM $ do
  n <- freshFrom "n"; let szN = var n :: IdSize
  let ty = tDouble
  [input,output] <- mapM freshFrom ["input","output"]
  Deriv "diff" SeqCtx { _intuit = [ n :::. tSize
                                  ]
                      , _linear = [ input :::. bigSeq szN ty
                                  , output :::. dual (bigSeq szN ty)
                                  ]
                      } <$> diffSeq szN ty input output

diff2Deriv = D0$ runFreshM $ do
  n <- freshFrom "n"; let szN = var n :: IdSize
  p <- freshFrom "p"
  let ty = tDouble
  [input,output] <- mapM freshFrom ["input","output"]
  Deriv "diff2" SeqCtx { _intuit = [ n :::. tSize
                                   ]
                       , _linear = [ input :::. bigSeq szN ty
                                   , output :::. dual (bigSeq szN ty)
                                   ]
                       } <$> diff2Seq szN ty input output



fftStepDeriv = [
  bffDeriv ::: False
 ,(::: True) . D0 $ runFreshM $ do
    [input,result] <- freshIds 2
    n <- freshFrom "n"
    let sz  = var n
        sz2 = 2 * sz
    -- c <- var <$> freshFrom "C"
    let c = tComplex
    let fftStep = {- evaluateF evalSteps =<< -}
          cut (bigTensor sz ((c ⊗ c) ⊸ (c ⅋ c))) (\bff ->
            (saveContext [("n",T (SizeT sz) ::: \[] -> T TSize)]
                         (\[sizeFromType -> Just sz] -> [bff :::. dual (bigTensor sz ((c ⊗ c) ⊸ (c ⅋ c)))]) (derivCallD bffDeriv)))
          $ \bff -> slice bff $ \bff -> 
            (compose (bigTensor sz c ⊗ bigTensor sz c) (CLL.halve sz) $
                 compose (bigTensor sz (c ⊗ c)) (CLL.zip sz) $
                 compose (bigTensor sz (c ⅋ c)) (CLL.mapN sz bff) $
                 compose (bigPar sz (c ⅋ c)) (CLL.schedule2 sz) $
                 compose (bigPar sz c ⅋ bigPar sz c) (flip (CLL.zip sz)) $
                 compose (bigPar sz2 c) (flip (CLL.halve sz)) $
                 axiom
                ) input result
    Deriv "fftStep" SeqCtx { _intuit = [{-bff ::: (c ⊗ c) ⊸ (c ⅋ c) :^ sizeOne-}
                                        n :::. T TSize ]
                           , _linear = [input  ::: bigTensor sz2 c :^ sizeOne
                                       ,result ::: bigTensor sz2 (dual c) :^ sizeOne
                                    ] } <$> fftStep 
  ]

fftMultiDeriv = fftStepDeriv ++ [
  (::: True) . D0 $ runFreshM $ do
     [input,output] <- mapM freshFrom ["input","output"]
     n <- freshFrom "n"; let szN = var n
     d <- freshFrom "d"; let szD = var d
     Deriv "fftMulti" (SeqCtx 
       [n     :::. tSize
       ,d     :::. tSize
       ]
       [input  :::. bigTensor (2 * szN * szD) tComplex
       ,output :::. bigTensor (2 * szN * szD) (dual tComplex)
       ]) <$> (
       slice input $ \input ->
       slice output $ \output ->
       seqSchedule szD $
        fuse (bigTensor (2*szN) tComplex) (\v -> coslice v (2*szN) $ axiom input) $ \input ->
        fuse (bigTensor (2*szN) (dual tComplex)) (\v -> coslice v (2*szN) $ axiom output) $ \output ->
        saveContext
          [("n",T (SizeT szN) ::: \[] -> T TSize)]
          (\[sizeFromType -> Just szN] -> 
                     [input :::. bigTensor (2 * szN) tComplex
                     ,output :::. bigTensor (2 * szN) (dual tComplex)
                     ])
          (derivCall "fftStep")
       )
  ]

{-
fftMultiDeriv = fftStepDeriv ++ [
  (::: True) . D0 $ runFreshM $ do
     [input,output] <- mapM freshFrom ["input","output"]
     n <- freshFrom "n"; let szN = var n
     d <- freshFrom "d"; let szD = var d
     Deriv "fftMulti" (SeqCtx 
       [n     :::. tSize
       ,d     :::. tSize
       ]
       [input  :::. bigTensor (2 * szN * szD) tComplex
       ,output :::. bigTensor (2 * szN * szD) (dual tComplex)
       ]) <$> (
       slice input $ \input ->
       slice output $ \output ->
       cut' "sched" (neg $ bigTensor szD (T Bot))
                    (\x -> s0 $ pure$ What (Huh "bar") [ElemLinear x] [])
                    (\s -> coslice s szD $ \ss -> ignore ss $
                                                  return$ S0$ What {what = (Huh "foo"), zVars = [ElemLinear input,
                                                        ElemLinear output], disp = []}))

     {-
        fuse (bigTensor (2*szN) tComplex) (\v -> coslice v (2*szN) $ axiom input) $ \input ->
        fuse (bigTensor (2*szN) (dual tComplex)) (\v -> coslice v (2*szN) $ axiom output) $ \output ->
        saveContext
          [("n",T (SizeT szN) ::: \[] -> T TSize)]
          (\[sizeFromType -> Just szN] -> 
                     [input :::. bigTensor (2 * szN) tComplex
                     ,output :::. bigTensor (2 * szN) (dual tComplex)
                     ])
          (derivCall "fftStep") -}
  ]
-}


waveL = D0$ runFreshM$ do
  a_d1 <- freshFrom "a_d1"
  a_0  <- freshFrom "a_0"
  a_r1 <- freshFrom "a_r1"
  b_0  <- freshFrom "b_0"
  tausq  <- freshFrom "tau"
  Deriv "waveL" SeqCtx { _intuit = []
                       , _linear = [ tausq :::. T PDouble
                                   , a_d1 :::. T PDouble
                                   , a_0  :::. T PDouble
                                   , a_r1  :::. T PDouble
                                   , b_0  :::. dual (T PDouble)
                                   ]
                       } <$>
   (dump1 (T PDouble) tausq  $
    dump1 (T PDouble) a_d1 $
    dump1 (T PDouble) a_0  $
    dump1 (T PDouble) a_r1 $
    ffiDouble 0 (axiom b_0)
    )

waveR = D0$ runFreshM$ do
  a_d1 <- freshFrom  "a_d1"
  a_0  <- freshFrom  "a_0"
  a_l1  <- freshFrom "a_l1"
  b_0 <- freshFrom   "b_0"
  tausq <- freshFrom   "tau"
  Deriv "waveR" SeqCtx { _intuit = []
                       , _linear = [ tausq :::. T PDouble
                                   , a_d1  :::. T PDouble
                                   , a_0   :::. T PDouble
                                   , a_l1  :::. T PDouble
                                   , b_0   :::. dual (T PDouble)
                                   ]
                       } <$>
   (dump1 (T PDouble) tausq  $
    dump1 (T PDouble) a_d1 $
    dump1 (T PDouble) a_0  $
    dump1 (T PDouble) a_l1  $
    ffiDouble 0 (axiom b_0)
    )


waveC = D0$ runFreshM$ do
  a_d1  <- freshFrom "a_d1"
  a_0   <- freshFrom "a_0"
  a_l1  <- freshFrom "a_l1"
  a_r1  <- freshFrom "a_l1"
  b_0   <- freshFrom "b_0"
  tausq <- freshFrom "τ²"
  Deriv "waveC" SeqCtx { _intuit = []
                       , _linear = [ tausq  :::. T PDouble
                                   , a_r1   :::. T PDouble
                                   , a_0    :::. T PDouble
                                   , a_l1   :::. T PDouble
                                   , a_d1   :::. T PDouble
                                   , b_0    :::. dual (T PDouble)
                                   ]
                       } <$>
   (ffiDouble 2 $ \two ->
    two `fTimesD` a_0 $ \r₁ ->
    copyData 2 (T PDouble) r₁ $ \[r₁',r₁''] ->
    a_l1 `fMinusD` r₁'' $ \r₃ ->
    r₃   `fPlusD`  a_r1 $ \r₄ ->
    tausq `fTimesD` r₄  $ \r₅ ->
    r₁'  `fMinusD` a_d1 $ \r₂ ->
    r₂   `fPlusD`  r₅   $ \r₆ ->
    axiom b_0 r₆
    )

wave0 = waveWrapOne    "wave0" "wave1'" (waveWrapDouble "wave1'" (CLL.waveBigSeqSymmDeriv))
wave1 = waveWrapDouble "wave1" (CLL.waveBigSeqSymmComposeDeriv 1)
wave2 = waveWrapDouble "wave2" (CLL.waveBigSeqSymmComposeDeriv 2)
wave3 = waveWrapDouble "wave3" (CLL.waveBigSeqSymmComposeDeriv 3)
wave6 = waveWrapDouble "wave6" (CLL.waveBigSeqSymmComposeDeriv 6)

waveWrapDouble :: Name -> IdDeriv0 -> [IdDeriv0 ::: Bool]
waveWrapDouble name d = [d ::: False, waveR ::: True, waveL ::: True, waveC ::: True,
  (::: True) $ D0 $ runFreshM $ do
    n <- freshFrom "n"
    p <- freshFrom "p"
    tau <- freshFrom "tau"
    [input₁,input₂,result₁,result₂] <- freshIds 4
    let wsz = var n
    Deriv name (SeqCtx { _intuit = [n :::. T TSize
                                   ,p :::. T (TProp (wsz, Ge, 10)) ]
                       , _linear = [tau     :::. T PDouble
                                   ,input₁  :::. bigTensor wsz (T PDouble)
                                   ,input₂  :::. bigTensor wsz (T PDouble)
                                   ,result₁ :::. bigTensor wsz (dual (T PDouble))
                                   ,result₂ :::. bigTensor wsz (dual (T PDouble))] }) <$> (
       slice input₁  $ \input₁ -> 
       slice input₂  $ \input₂ -> 
       slice result₁ $ \result₁ -> 
       slice result₂ $ \result₂ -> 
       cut (bigSeq wsz (T PDouble ⊗ T PDouble))
         (\seq -> bigseq [seqstep wsz [seq] $ (\[seq] ->
                            par seq (axiom input₁) (axiom input₂))])
         (\input_seq ->
            cut (dual$ bigSeq wsz (T PDouble ⊗ T PDouble))

                 (\seq -> bigseq [seqstep wsz [seq] $ \[seq] ->
                            tensor seq (\a b -> 
                                           mix' [axiom result₁ a,
                                                 axiom result₂ b])])


                 (\output_seq ->
                     saveContext [("a", T PDouble     ::: \[] -> T TType)
                                 ,("n", T (SizeT wsz) ::: \[_] -> T TSize)
                                 ,("p", T Prop        ::: \[_,n] -> T (TProp (var n, Ge, 10)))]
                                 (\[tyA, sizeFromType -> Just sz, _] ->
                                      [tau        :::. (T$ PDouble)
                                      ,input_seq  :::. (bigSeq sz (tyA ⊗ tyA))
                                      ,output_seq :::. (dual$ bigSeq sz (tyA ⊗ tyA))
                                      ])
                       $ 
                            derivCall (_derivName $ deriv0 d)
                   )

            ))]


waveWrapOne :: Name -> Name -> [IdDeriv0 ::: Bool] -> [IdDeriv0 ::: Bool]
waveWrapOne name dname ds =
  let ((d ::: _):_, rest) = partition ((dname ==) . view (value . _Wrapped' . derivName)) ds in
  rest ++ [d ::: False] ++ [ 
  (::: True) $ D0 $ runFreshM $ do
    n <- freshFrom "n"
    p <- freshFrom "p"
    tau <- freshFrom "tau"
    [input₁,input₂,result₂] <- freshIds 3
    let wsz = var n
    Deriv name (SeqCtx { _intuit = [n :::. T TSize
                                   ,p :::. T (TProp (wsz, Ge, 10)) ]
                       , _linear = [tau     :::. T PDouble
                                   ,input₁  :::. bigTensor wsz (T PDouble)
                                   ,input₂  :::. bigTensor wsz (T PDouble)
                                   ,result₂  :::. bigTensor wsz (dual (T PDouble))] }) <$> (
       cut (bigTensor wsz (dual (T PDouble)))
            (\result₁' -> coslice result₁' wsz (\result -> dump1 (T PDouble) result $ halt))
            (\result₁  ->
                   saveContext [("n", T (SizeT wsz) ::: \[] -> T TSize)
                               ,("p", T Prop        ::: \[n] -> T (TProp (var n, Ge, 10)))]
                               (\[sizeFromType -> Just sz, _] ->
                                    [tau      :::. (T$ PDouble)
                                    ,input₁   :::. bigTensor wsz (T PDouble)
                                    ,input₂   :::. bigTensor wsz (T PDouble)
                                    ,result₁  :::. bigTensor wsz (dual (T PDouble))
                                    ,result₂  :::. bigTensor wsz (dual (T PDouble))
                                    ])
                     $   derivCall (_derivName $ deriv0 d)
                  )
           )
        
      ]


                         

quickhull = [quickhullStep1Deriv ::: True
            ,quickhullStep2Deriv ::: True
            ,quickhullStep3Deriv ::: True
            ,splitDeriv ::: False
            ,mapSeqDeriv ::: False
            ,mapSeq1Deriv ::: False
            ,dotSeqInlinedDeriv ::: True
            ,lineDistanceDeriv ::: True
            ,bestDeriv ::: False
            ,foldDeriv ::: False
            ,splitTopAboveDeriv ::: True
            ,best1Deriv ::: False
            ]

wc        = [countTrueDeriv ::: True
            ,countNewLineDeriv ::: True
            ,mapSeqDeriv ::: False
            ,countWordDeriv ::: True
            ,wordCountDeriv ::: True
            ]


