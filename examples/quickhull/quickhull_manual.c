#include <stddef.h>
#include <stdbool.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "quickhull_driver.h"

typedef point vector;

/* we don't divide by the norm of v, since we only care about > 0, and comparison */
/* Distance should be exactly zero if both vectors are equal, or one of them is zero */
static inline double distance(vector v, vector pv) {
    return pv.Y * v.X - v.Y * pv.X;
}

static inline vector diff(point b, point a) {
  vector result = { { b.X - a.X, b.Y - a.Y } };
  return result;
}

void quickhullStep1Manual (size_t n, const point * all, point* left_, point* right_) {
  point left, right;
  left  = right = all[0];

  for(int i = 1; i < n; i++) {
    const point p = all[i];
    if (p.X < left.X)       { left  = p; }
    else if (p.X > right.X) { right = p; }
  }

  *right_ = right;
  *left_  = left;
}

void quickhullStep2Manual (size_t n, const point * all,
			   point left, point right,
			   size_t* r_, point * above,
			   size_t* s_, point * below,
			   point* max_, point* min_) {
  point min; point max; size_t r = 0, s = 0;

  vector v = diff(right, left);

  double dmin = 0; double dmax = 0;

  for(int i = 0; i < n; i++) {
    const point p = all[i];
    point pv = diff (p, left);
    double d = distance(v, pv);
    if (d > 0) {
      above[r] = p; r++;
      if (d > dmax) { dmax = d; max = p; };
    } else if (d < 0) {
      below[s] = p; s++;
      if (d < dmin) { dmin = d; min = p; };
    }
  }

  *max_ = max;
  *min_ = min;
  *r_ = r;
  *s_ = s;
}

void quickhullStep3Manual (size_t n, const point * all, point left, point right, point top,
			   size_t *r_, point *lefts,
			   size_t *s_, point *rights,
			   point* top_left_, point* top_right_) {

  point top_left; point top_right; size_t r = 0, s = 0;

  double dleft = 0, dright = 0;

  vector v1 = { { top.X - left.X, top.Y - left.Y } };
  vector v2 = { { right.X - top.X, right.Y - top.Y } };

  for(int i = 0; i < n; i++) {
    const point p = all[i];
    point pv1 = diff(p, left);
    double d  = distance(v1, pv1);
    if (d > 0) {
      lefts[r] = p; r++;
      if (d > dleft) { dleft = d ; top_left = p ; }
    } else {
      point pv2 = diff(p, top);
      double d  = distance(v2, pv2);
      if(d > 0) {
        rights[s] = p; s++;
        if (d > dright) { dright = d ; top_right = p; }
      }
    }
  }

  *top_left_  = top_left;
  *top_right_ = top_right;
  *r_ = r;
  *s_ = s;
}
