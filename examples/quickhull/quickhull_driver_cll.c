#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "quickhull_manual.h"
#include "quickhull_driver.h"

typedef vecSumN_bvec_b2_sprim_bdouble_d_d_svoid_d pointSeq;
typedef sum_bvec_b2_sprim_bdouble_d_d_svoid_d pointOption;

#define LENGTH_INL(A) ((A).skip[((A).n - 1) & ~((size_t)1L)])

void halfQuickhull(size_t n, point left, point top, point right, point* all, size_t *hull_n, point* hull, point * inl1, point * inl2, size_t* skip1, size_t* skip2); 

void quickhull(size_t n, point *all, size_t *hull_n, point* hull) {
  point* hull_orig = hull;

  point *inl1, *inl2, *inl3;
  size_t *skip1, *skip2;
  inl3 = (inl2 = (inl1 = malloc(3*n * sizeof(point))) + n) + n;
  size_t skip_buff_size = 2*n + 4;
  skip2 = (skip1 = malloc(3*skip_buff_size * sizeof(size_t))) + skip_buff_size;
  skip1[0] = skip2[0] = 0;

  if (n <= 3) { memcpy ((void *restrict)hull, (const void *restrict)all, sizeof(point) * n); *hull_n = n; return; }


  point left, right; 
  quickhullStep1(n, all, &left, &right);

  pointOption top, bottom;
  //pointSeq buffer1 = { 0, skip1 + 1, hull }, buffer2 = { 0, skip2 + 1, inl2 };
  pointSeq buffer1 = { 0, skip1 + 1, inl1 }, buffer2 = { 0, skip2 + 1, inl2 };
  quickhullStep2(n, all, left, right, &top, &buffer1, &bottom, &buffer2);
  size_t r = LENGTH_INL(buffer1);
  size_t s = LENGTH_INL(buffer2);

  size_t r1 = 0, r2 = 0;
  *hull = left;  hull++;
  halfQuickhull(r, left,  top.val.inl,    right, buffer1.inl, &r1, hull, inl2 + s, inl3, skip1, skip2); hull+=r1;
  *hull = right; hull++;
  halfQuickhull(s, right, bottom.val.inl, left,  buffer2.inl, &r2, hull, inl1 + r, inl3 + r, skip1, skip2); hull+=r2;
  *hull_n = hull - hull_orig;
  
  free(inl1); 
  free(skip1);
}

/* Use recursion, since we are not measuring the performance of the driver */
/* Assumptions: the caller has already put the values right and left in the hull */
/* Assumptions: the all array could be modified up to the given length */
void halfQuickhull(size_t n, point left, point top, point right, point* all, size_t *hull_n, point* hull, point * inl1, point * inl2, size_t* skip1, size_t* skip2) {

  point * hull_orig = hull;

  if (n <= 1) {
    memcpy ((void *)hull, (const void *)all, sizeof(point) * n);
    *hull_n = n;
    return;
  }

  size_t r, s; pointOption top_left, top_right;
  pointSeq buffer1 = { 0, skip1 + 1, inl1 }, buffer2 = {0, skip2 + 1, inl2 };
  quickhullStep3 (n, all, left, top, right,
			   &buffer1,
                           &top_left,
			   &buffer2,
                           &top_right);
  r = LENGTH_INL(buffer1);
  s = LENGTH_INL(buffer2);

  size_t r1, s1;
  halfQuickhull(r, left, top_left.val.inl, top, inl1,  &r1, hull, inl2 + s, all, skip1, skip2); hull += r1;
  *hull = top; hull++;
  halfQuickhull(s, top, top_right.val.inl, right, inl2, &s1, hull, inl1 + r, all, skip1, skip2); hull += s1;
  *hull_n = hull - hull_orig;
}
