#!/bin/bash

echo -e "variant\tlen\trepeats\ttime\ttimeEl"

LEN=1000000
echo -n -e "MEM\t" ; ./run 100000 100 s 2>/dev/null
echo -n -e "FUSED\t" ; ./run_fused $LEN 100 s 2>/dev/null
echo -n -e "HAND\t" ; ./run_manual $LEN 100 s 2>/dev/null

