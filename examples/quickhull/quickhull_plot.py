#!/usr/bin/env python

from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np
import sys

lines = open(sys.argv[1])

x = np.fromstring(next(lines), sep="\t")
y = np.fromstring(next(lines), sep="\t")
hx = np.fromstring(next(lines), sep="\t")
hy = np.fromstring(next(lines), sep="\t")

plt.plot(x,y,'.')
plt.plot(hx,hy,'o')

plt.axis([-1.2,1.2,-1.2,1.2])

plt.savefig('quickhull.png',dpi=300)

plt.show()



