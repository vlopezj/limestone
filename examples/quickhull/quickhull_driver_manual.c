#include <string.h>
#include <stdlib.h>

#include "quickhull_manual.h"
#include "quickhull_driver.h"


void halfQuickhull(size_t n, point left, point top, point right, point* all, size_t *hull_n, point* hull, point * buffer1, point * buffer2);
void quickhull(size_t n, point *all, size_t *hull_n, point* hull) {
  point *buffer1, *buffer2, *buffer3;
  buffer3 = (buffer2 = (buffer1 = malloc(3*n * sizeof(point))) + n) + n;

  if (n <= 3) { memcpy ((void *restrict)hull, (const void *restrict)all, sizeof(point) * n); *hull_n = n; return; }

  point left, right; 
  quickhullStep1Manual(n, all, &left, &right);

  size_t r, s; point top, bottom;
  quickhullStep2Manual(n, all, left, right, &r, buffer1, &s, buffer2, &top, &bottom);

  /* solve top problem */
  size_t r1, r2;
  *hull = left; hull++;
  halfQuickhull(r, left,  top,    right, buffer1, &r1, hull, buffer2 + s, buffer3);
  hull+=r1; *hull = right; hull++;
  halfQuickhull(s, right, bottom, left,  buffer2, &r2, hull, buffer1 + r, buffer3 + r);
  *hull_n = 2 + r1 + r2; 
  
  free(buffer1);
}

/* Use recursion, since we are not measuring the performance of the driver */
/* Assumptions: the caller has already put the values right and left in the hull */
/* Assumptions: the all array could be modified up to the given length */
void halfQuickhull(size_t n, point left, point top, point right, point* all, size_t *hull_n, point* hull, point * buffer1, point * buffer2) {

  size_t r, s; point top_left, top_right;
  if (n <= 1) {
    memcpy ((void *)hull, (const void *)all, sizeof(point) * n);
    *hull_n = n;
    return;
  }

  quickhullStep3Manual (n, all, left, right, top,
			 &r, buffer1,
			 &s, buffer2,
			 &top_left, &top_right);

  size_t r1, s1;
  halfQuickhull(r, left, top_left, top, buffer1,  &r1, hull, buffer2 + s, all);
  hull += r1; *hull = top; hull++;
  halfQuickhull(s, top, top_right, right, buffer2, &s1, hull, buffer1 + r, all);
  *hull_n = r1 + s1 + 1;
}
