#include <stddef.h>
#include <stdbool.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "quickhull_driver.h"
void quickhullStep1Manual (size_t n, point * all, point* left_, point* right_);
void quickhullStep2Manual (size_t n, point * all, point left, point right, size_t* r_, point * above, size_t* s_, point * below, point* max_, point* min_);
void quickhullStep3Manual (size_t n, point * all, point left, point right, point top,
			   size_t *r_, point *lefts,
			   size_t *s_, point *rights,
			   point* top_left_, point* top_right_);
