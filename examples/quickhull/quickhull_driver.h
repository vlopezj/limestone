#ifndef QUICKHULL_DRIVER
#define QUICKHULL_DRIVER
#include "quickhull.h"

typedef vec_b2_sprim_bdouble_d_d point;

#define X vec[0]
#define Y vec[1]


void quickhull(size_t n, point *all, size_t *hull_n, point* hull);
#endif
