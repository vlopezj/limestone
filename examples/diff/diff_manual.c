#include <stddef.h>
#include <stdbool.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void diff (size_t arg_1g, double *arg_1h, double *arg_1i);
void diff2 (size_t arg_2L, double *arg_2M, double *arg_2N);

void diff (size_t n, double *a, double *b) {
    for (size_t i = 0; i < n - 1; i++) {
        b[i] = a[i+1] - a[i];
    }
}

void diff2 (size_t n, double *a, double *b) {
    for (size_t i = 0; i < n - 2; i++) {
        b[i] = a[i+2] + 2*a[i+1] - a[i]; 
    }
}
