#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>
#include <stdio.h>

#include "dot.h"

void init(long len, long i, double * a, double * b) {
  double t = 2*M_PI*((double)i)/len;
  *x = sin(t);
  *y = sin(2*t);
}

double clock_diff(struct timespec b, struct timespec a) {
  double sec_delta  = b.tv_sec - a.tv_sec;
  long   nano_delta = b.tv_nsec - a.tv_nsec;
  return sec_delta + (1.0E-9 * nano_delta);
}

int main(int argc, char * argv[]) {
  // Reading arguments
  long len;                                                             
  long repeats;
  char output;

  switch(argc-1) {                                                              
  case 3:                                                                       
    if(sscanf(argv[1], "%ld", &len) >= 1 &&                                    
       sscanf(argv[2], "%ld", &repeats) >= 1 &&                                    
       sscanf(argv[3], "%c", &output) >= 1 
       )
      break;                                                                    
  default:                                                                      
    assert(argc > 0);                                                           
    fprintf(stderr, "Usage: %s len repeats [#output y|n]\n", argv[0]);
    exit(1);                                                                    
  }    

  double * all, * hull;
  a  = malloc(sizeof(*a)*len);
  b  = malloc(sizeof(*b)*len);
  
  for(size_t i = 0; i < len; i++) {
    init(len,i,&a[i],&b[i]);
  }
  
  double diff;
  struct timespec start, end;

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
  for(int r = 0; r < repeats; r++){
    res = dotSeq(len, a, b);
  }        

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
  diff = clock_diff(end,start);

  double cpu = diff / repeats ;
  double cpu_per_element = cpu / len;

  switch(output) {
      case 'y': 
        output_points(len, res);
	break;
      case 's':
        printf("%ld\t%ld\t%lg\t%lg\n", len, repeats, cpu, cpu_per_element);
        break;
  }

  free(a), free(b);
}
