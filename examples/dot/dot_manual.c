#include <stddef.h>
#include <stdbool.h>
#include <complex.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double dotSeq (size_t n, double *a, double *b);

double dotSeq (size_t n, double *a, double *b) {
    double r = 0.0;
    for (size_t i = 0; i < n; ++i) {
        r += a[i] * b[i];
    }
    return r;
}
