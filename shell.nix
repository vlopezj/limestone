let 
    pkgs = (import <nixpkgs> {});
    haskellPackages = pkgs.recurseIntoAttrs (pkgs.haskell.packages.ghc7102.override {
        overrides = self : super :
        let callPackage = self.callPackage;
            dontTest = pkgs.haskell.lib.dontCheck;
        in {
           cabal-install = super.cabal-install; # .override{ enableLibraryProfiling = true; };
           cake = callPackage ./nix/cake.nix {};
           cmdlib = callPackage ./nix/cmdlib.nix {};
           either = callPackage ./nix/either.nix {};
           genifunctors = callPackage ./nix/genifunctors.nix {};
           geniplate-mirror = callPackage ./nix/geniplate-mirror.nix {};
           is = callPackage ./nix/is.nix {};
           labeledTree = callPackage ./nix/labeled-tree.nix {};
           # marxup = callPackage ../MarXup {};
           marxup = callPackage ./nix/marxup.nix {};
           multiset = callPackage ./nix/multiset.nix {};
           parsek = callPackage ./nix/parsek.nix {};
           polynomials-bernstein = callPackage ./nix/polynomials-bernstein.nix {};
           vector =  dontTest (callPackage ./nix/vector.nix {});
           sbv =  dontTest (callPackage ./nix/sbv.nix {});
           QuickCheck = callPackage ./nix/QuickCheck.nix {};
           typography-geometry = callPackage ./nix/typography-geometry.nix {};
           singletons = callPackage ./nix/singletons.nix {};
           th-desugar = callPackage ./nix/th-desugar.nix {};
           thisPackage = callPackage (import ./default.nix) {};
        };});
in pkgs.stdenv.mkDerivation {
    name = haskellPackages.thisPackage.name;
    buildInputs = 
       [(haskellPackages.ghcWithPackages (hs: ([
         hs.cabal-install
         hs.hdevtools
         hs.alex
         hs.happy
         hs.hscolour
         hs.marxup
       ] ++ haskellPackages.thisPackage.propagatedNativeBuildInputs)))] ++
       [ pkgs.glibcLocales
         pkgs.cvc4
         pkgs.gcc
       ];
    shellHook = ''
      export LANG=en_US.UTF-8
      '';
     }
